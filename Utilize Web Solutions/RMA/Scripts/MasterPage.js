﻿function clickRow() {
    $('.table').bootstrapTable({
    }).on('click-row.bs.table', function (e, row, $element) {     
        __doPostBack('CallReturn', $(row[0]).text());        
    });
}


function set_caret_links() {
    $(".image-caret").each(function () {
        $(this).attr("data-target", "#" + $(this).parent().parent().next().find(".collapse").attr("id"));
    });
}

function set_status_width() {
    if ($(".label.rounded").length) {
        var widest = 0;
        $(".label.rounded").each(function () {
            var current_width = $(this).css("width");
            if (current_width.substring(0, current_width.length - 2) > widest) {
                widest = parseInt(current_width.substring(0, current_width.length - 2));
                widest += parseInt($(this).css("padding-left").substring(0, $(this).css("padding-left").length - 2));
                widest += parseInt($(this).css("padding-right").substring(0, $(this).css("padding-right").length - 2));
                widest = widest.toString() + "px";
            }            
        });

        $(".label.rounded").each(function () {
            var current_width = $(this).css("width");
            if (current_width.substring(0, current_width.length - 2) != widest.substring(0, widest.length - 2)) {
                var difference = widest.substring(0, widest.length - 2) - current_width.substring(0, current_width.length - 2);
                var padding = difference / 2;
                console.log(widest);
                $(this).css("padding-left", padding + "px");
                $(this).css("padding-right", padding + "px");
            }
        });
    }
}

function increment(control) {
    var input = $("#" + control.id).parent().prev();
    var max = input.attr("data-max");
    var current = +input.val();
    if (!(current >= max)) {
        input.val(current + 1);        
    }
}

function decrement(control) {
    var input = $("#" + control.id).parent().next();
    var current = +input.val();
    if (current > 0) {
        input.val(current - 1);
    }
}