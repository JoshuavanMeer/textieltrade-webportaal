﻿
Partial Class Webshop_Account_RMAForm
    Inherits Utilize.Web.Solutions.RMA.rma_page_base

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim loPDFGenerate As New ExpertPdf.HtmlToPdf.PdfConverter
        loPDFGenerate.LicenseKey = "Apxofk9W45ihltLQtKYelQ7h9+0Jm3o+D/mGrPycnO7rHozfB9KwzbnQ7M6ZjIGE"

        Dim loByte() As Byte = loPDFGenerate.GetPdfBytesFromHtmlString(Me.generate_rma_form(Me.get_page_parameter("rmacode"), Me.global_ws.Language))

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-length", loByte.Length.ToString())
        Response.AddHeader("Content-Disposition", "attachment;filename=rma#" + Me.get_page_parameter("rmacode") + ".pdf")

        Response.BinaryWrite(loByte)
        Response.End()
    End Sub

    Public Function generate_rma_form(rmacode As String, language As String) As String
        Dim rma_form_html As String = ""

        Dim qsc_rma_form As New Utilize.Data.QuerySelectClass
        qsc_rma_form.select_fields = "form_body_" + language
        qsc_rma_form.select_from = "uws_rma_form"
        Dim dt_rma_form As System.Data.DataTable = qsc_rma_form.execute_query()

        rma_form_html = dt_rma_form.Rows(0).Item("form_body_" + language)

        rma_form_html = rma_form_html.Replace("==RMACODE==", rmacode)

        If rma_form_html.Contains("==RMALINES==") Then
            rma_form_html = rma_form_html.Replace("==RMALINES==", create_rma_lines(rmacode, language))
        End If

        Return rma_form_html
    End Function

    Private Function create_rma_lines(rmacode As String, _language As String) As String
        Dim lc_lines As String = ""

        Dim qsc_rma_form As New Utilize.Data.QuerySelectClass
        qsc_rma_form.select_fields = "*"
        qsc_rma_form.select_from = "uws_rma_form"
        Dim dt_rma_form As System.Data.DataTable = qsc_rma_form.execute_query()

        lc_lines += "<table style='" + dt_rma_form.Rows(0).Item("line_style") + "'> "

        'bouw de header op
        lc_lines += "<thead>"

        lc_lines += "<tr>"
        lc_lines += "<th style=""text-align: left;"">" + Utilize.Web.Solutions.Translations.global_trans.translate_label("col_prod_code1", "Productcode", _language) + "</th>"
        lc_lines += "<th style=""text-align: left;"">" + Utilize.Web.Solutions.Translations.global_trans.translate_label("col_prod_desc1", "Product beschrijving", _language) + "</th>"
        lc_lines += "<th style=""text-align: center;"">" + Utilize.Web.Solutions.Translations.global_trans.translate_label("col_amount1", "Aantal", _language) + "</th>"
        lc_lines += "</tr>"

        lc_lines += "</thead>"

        lc_lines += "<tbody>"

        Dim qsc_rma_lines As New Utilize.Data.QuerySelectClass
        qsc_rma_lines.select_fields = "*"
        qsc_rma_lines.select_from = "uws_rma_line"
        qsc_rma_lines.select_where = "hdr_id = @rma_code"
        qsc_rma_lines.add_parameter("@rma_code", rmacode)
        Dim dt_rma_lines As System.Data.DataTable = qsc_rma_lines.execute_query()

        For Each row In dt_rma_lines.Rows
            lc_lines += "<tr>"

            lc_lines += "<td style=""text-align: left;"">" + System.Web.HttpUtility.HtmlEncode(row.Item("prod_code")) + "</td>"
            lc_lines += "<td style=""text-align: left;"">" + System.Web.HttpUtility.HtmlEncode(row.Item("prod_desc")) + "</td>"
            lc_lines += "<td style=""text-align: center;"">" + System.Web.HttpUtility.HtmlEncode(Decimal.Round(row.Item("return_qty"), 0)) + "</td>"

            lc_lines += "</tr>"
        Next

        lc_lines += "</tbody>"

        lc_lines += "</table>"

        Return lc_lines
    End Function


End Class
