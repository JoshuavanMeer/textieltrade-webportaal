﻿
Partial Class RMA_UserControls_uc_image_overview
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Private _dt_images As New System.Data.DataTable
    Public Property dt_images As System.Data.DataTable
        Get
            Return _dt_images
        End Get
        Set(value As System.Data.DataTable)
            _dt_images = value
        End Set
    End Property

    Private Sub RMA_UserControls_uc_image_overview_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_image_overview.css", Me)
        set_images()

    End Sub

    Private Sub set_images()

        If dt_images.Rows.Count > 0 Then
            rpt_images.DataSource = _dt_images
            rpt_images.DataBind()

            Me.ph_image_overview.Visible = True
        End If
    End Sub
    
End Class
