﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_image_overview.ascx.vb" Inherits="RMA_UserControls_uc_image_overview" %>

<utilize:placeholder runat="server" id="ph_image_overview" Visible="false">
    <tr class="uc_image_overview">
        <td colspan="6">
            <div class="collapse" runat="server" id="div_collapasble">
                <utilize:repeater runat="server" ID="rpt_images">
                    <ItemTemplate>
                        <div class="col-md-3 col-xs-6 col-sm-6">
                            <img class="img-responsive" src="<%#Me.ResolveUrl(DataBinder.Eval(Container.DataItem, "image"))%>" alt="image"/>
                        </div>
                    </ItemTemplate>
                </utilize:repeater>
            </div>
        </td>
    </tr>
</utilize:placeholder>