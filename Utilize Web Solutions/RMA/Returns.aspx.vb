﻿
Partial Class Webshop_Account_Returns
    Inherits Utilize.Web.Solutions.RMA.rma_page_base

    Private status_id As String = ""
    Private reason_id As String = ""
    Private rmacode_id As String = ""

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Me.get_page_parameter("status") Is Nothing Then
            status_id = Request.Params("status")
        End If

        If Not Me.get_page_parameter("reason") Is Nothing Then
            reason_id = Request.Params("reason")
        End If

        If Not Me.get_page_parameter("rmacode") Is Nothing Then
            rmacode_id = Request.Params("rmacode")
        End If

        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_returns_overview", "Overzicht Retouren", Me.global_ws.Language)
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim uc_return_login As RMA.rma_user_control = LoadUserControl("PageBlocks/uc_returns.ascx")
        uc_return_login.set_property("status", status_id)
        uc_return_login.set_property("reason", reason_id)
        uc_return_login.set_property("rmacode", rmacode_id)
        Me.ph_controls.Controls.Add(uc_return_login)
    End Sub


End Class
