﻿<%@ WebHandler Language="VB" Class="UploadImages" %>

Imports System
Imports System.Web
Imports System.IO
Imports Utilize.Data


<Serializable()>
Public Class UploadImages : Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim lc_prod_code As String = HttpContext.Current.Request.QueryString("prodcode")
        Dim lc_rma_code As String = HttpContext.Current.Request.QueryString("rmacode")
        Dim lc_ord_nr As String = HttpContext.Current.Request.QueryString("ordnr")
        Dim fullpath As String = HttpContext.Current.Server.MapPath("~/Documents/RMA/tempImages/")
        Dim rmapath As String = HttpContext.Current.Server.MapPath("~/Documents/RMA/")

        If Not Directory.Exists(rmapath) Then
            Directory.CreateDirectory(rmapath)
        End If

        If Not Directory.Exists(fullpath) Then
            Directory.CreateDirectory(fullpath)
        End If

        For Each s As String In context.Request.Files
            Dim file As HttpPostedFile = context.Request.Files.Get(s)
            Dim filename As String = file.FileName
            Dim newFilname As String = ""

            If lc_ord_nr = "" Then
                newFilname = filename.Substring(0, filename.LastIndexOf("."c)) + "[" + lc_rma_code + "]" + lc_prod_code + filename.Substring(filename.LastIndexOf("."c))
            Else
                newFilname = filename.Substring(0, filename.LastIndexOf("."c)) + "[" + lc_rma_code + "]" + lc_prod_code + "{" + lc_ord_nr + filename.Substring(filename.LastIndexOf("."))
            End If

            If Not filename = Nothing And Not filename = "" Then
                Dim pathAndName As String = fullpath + newFilname
                file.SaveAs(pathAndName)
            End If
        Next
        context.Response.Write("done")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class