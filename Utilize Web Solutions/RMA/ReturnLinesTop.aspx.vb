﻿
Partial Class RMA_ReturnLinesTop
    Inherits Utilize.Web.Solutions.RMA.rma_page_base

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_returns_lines_top", "Retour regels selectie", Me.global_ws.Language)
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim uc_return_dash As RMA.rma_user_control = LoadUserControl("PageBlocks/uc_return_lines_top.ascx")
        Me.ph_controls.Controls.Add(uc_return_dash)
    End Sub

End Class
