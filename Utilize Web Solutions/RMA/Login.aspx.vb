﻿
Partial Class RMA_Login
    Inherits Utilize.Web.Solutions.RMA.rma_page_base



    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim uc_return_login As RMA.rma_user_control = LoadUserControl("PageBlocks/uc_return_login.ascx")
        Me.ph_controls.Controls.Add(uc_return_login)
    End Sub

    Protected Sub Page_PreInit1(sender As Object, e As EventArgs) Handles Me.PreInit
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_rma_login", "RMA login", Me.global_ws.Language)
    End Sub
End Class
