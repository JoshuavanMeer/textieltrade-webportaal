﻿
Partial Class uc_change_overview
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Private _rma_code As String = ""
    Public Property rma_code As String
        Get
            Return _rma_code
        End Get
        Set(ByVal value As String)
            _rma_code = value
            set_change_overview()
        End Set
    End Property

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_change_overview.css", Me)

    End Sub

    Protected Sub set_change_overview()
        Dim qsc_changes As New Utilize.Data.QuerySelectClass
        qsc_changes.select_fields = "*"
        qsc_changes.select_from = "uws_rma_changes"
        qsc_changes.select_where = "rma_code = @rma_code"
        qsc_changes.add_parameter("@rma_code", _rma_code)
        qsc_changes.select_order = "change_date DESC"

        Dim dt_changes As System.Data.DataTable = qsc_changes.execute_query()

        rpt_changes.DataSource = dt_changes
        rpt_changes.DataBind()

        'loop door alle elementen van de repeater heen en set hier de juiste dingen van.
        For Each item In rpt_changes.Items
            Dim label_who As label = item.findcontrol("label_who")
            Dim label_status As label = item.findcontrol("label_status")
            Dim label_rec_id As label = item.findcontrol("label_rec_id")
            Dim label_image_url As label = item.findcontrol("label_image_url")

            Dim lt_title As literal = item.findcontrol("lt_title")
            Dim lt_date As literal = item.findcontrol("lt_date")
            Dim lt_name As literal = item.findcontrol("lt_name")
            Dim lt_text As literal = item.findcontrol("lt_text")
            Dim lt_comp As literal = item.findcontrol("lt_comp")
            Dim lt_image As literal = item.findcontrol("lt_image")

            Dim ph_text As placeholder = item.findcontrol("ph_text")
            Dim ph_comp As placeholder = item.findcontrol("ph_comp")
            Dim ph_image As placeholder = item.findcontrol("ph_image")

            ph_text.Visible = False

            Dim div As HtmlGenericControl = item.findcontrol("div_alert")
            Dim div_date As HtmlGenericControl = item.findcontrol("div_date")
            Dim div_title As HtmlGenericControl = item.findcontrol("div_title")

            'Set naming of customer or rma user
            If label_who.Text.StartsWith("klant") Then
                lt_name.Text = label_who.Text.Substring(5)
            Else
                lt_name.Text = label_who.Text.Substring(9)
            End If

            'set de styling van de elementen hier aan de hand van het type dat ze zijn.
            Select Case label_status.Text
                Case "verzoek"
                    div.Attributes.Add("class", "alert alert-info fade in grey pull-left")
                    lt_title.Text = global_trans.translate_label("label_request_submitted", "Verzoek ingediend", Me.global_ws.Language)
                    div_date.Attributes.Add("class", "pull-right")
                    div_title.Attributes.Add("class", "pull-left")
                Case "goedgekeurd"
                    div.Attributes.Add("class", "alert alert-info fade in pull-right")
                    lt_title.Text = global_trans.translate_label("label_request_accepted", "Verzoek goedgekeurd", Me.global_ws.Language)
                    div_date.Attributes.Add("class", "pull-left")
                    div_title.Attributes.Add("class", "pull-right")
                Case "afgerond"
                    div.Attributes.Add("class", "alert alert-success fade in pull-right")
                    lt_title.Text = global_trans.translate_label("label_request_completed", "Verzoek afgerond", Me.global_ws.Language)
                    div_date.Attributes.Add("class", "pull-left")
                    div_title.Attributes.Add("class", "pull-right")
                Case "meer informatie"
                    'als het een bericht is die van de klant uit komt dan moet het bericht aan de linker kant uitgelijnd worden.
                    If label_who.Text.StartsWith("klant") Then
                        div.Attributes.Add("class", "alert alert-warning fade in pull-left")
                        lt_title.Text = global_trans.translate_label("label_request_more_info", "Extra informatie", Me.global_ws.Language)

                        If Not label_image_url.Text = "" Then
                            ph_image.Visible = True
                            lt_image.Text = "<img class='img-responsive' src='" + Me.ResolveCustomUrl("~/" + label_image_url.Text) + "'/>"
                        End If

                        ph_text.Visible = True
                        ph_comp.Visible = False
                        div_date.Attributes.Add("class", "pull-right")
                        div_title.Attributes.Add("class", "pull-left")

                    Else
                        div.Attributes.Add("class", "alert alert-warning fade in pull-right")
                        lt_title.Text = global_trans.translate_label("label_request_more_info", "Extra informatie", Me.global_ws.Language)

                        ph_text.Visible = True
                        Dim qsc_comp As New Utilize.Data.QuerySelectClass
                        qsc_comp.select_fields = "comp_desc_" + Me.global_ws.Language
                        qsc_comp.select_from = "uws_rma_compensation"
                        qsc_comp.select_where = "comp_code in (select change_comp from uws_rma_changes where rec_id = @rec_id)"
                        qsc_comp.add_parameter("@rec_id", label_rec_id.Text)

                        Dim dt_comp As System.Data.DataTable = qsc_comp.execute_query()

                        If dt_comp.Rows.Count > 0 Then
                            ph_comp.Visible = True
                            lt_comp.Text = global_trans.translate_label("label_change_comp", "De nieuwe toegewezen compensatie:", Me.global_ws.Language) + " " + dt_comp.Rows(0).Item("comp_desc_" + Me.global_ws.Language)
                        Else
                            ph_comp.Visible = False
                        End If

                        div_date.Attributes.Add("class", "pull-left")
                        div_title.Attributes.Add("class", "pull-right")
                    End If
                Case "afgekeurd"
                    div.Attributes.Add("class", "alert alert-danger fade in pull-right")
                    lt_title.Text = global_trans.translate_label("label_request_rejected", "Verzoek is afgekeurd", Me.global_ws.Language)
                    lt_text.Visible = True

                    div_date.Attributes.Add("class", "pull-left")
                    div_title.Attributes.Add("class", "pull-right")
                Case Else
                    Exit Select
            End Select
        Next
    End Sub
End Class
