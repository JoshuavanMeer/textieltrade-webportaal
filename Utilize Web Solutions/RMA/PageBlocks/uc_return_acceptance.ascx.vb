﻿
Imports System.Web.UI.WebControls

Partial Class Webshop_Account_AccountBlocks_uc_return_acceptance
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Private uc_change_overview As Utilize.Web.Solutions.RMA.rma_user_control = Nothing

    Private Function get_returns(Optional search As Boolean = False) As System.Data.DataTable
        Dim qsc_returns As New Utilize.Data.QuerySelectClass
        With qsc_returns
            .select_fields = "uws_rma.*,uws_customers.*,uws_rma_compensation.*, uws_history.*"
            .select_from = "uws_rma"
            .select_where = "(req_status = @req_status or req_status = @req_status2)"
            .select_order = "rma_code DESC"
        End With
        qsc_returns.add_join("left join", "uws_customers", "uws_rma.cust_id = uws_customers.rec_id")
        qsc_returns.add_join("left join", "uws_rma_compensation", "uws_rma.comp_code = uws_rma_compensation.comp_code")
        qsc_returns.add_join("left join", "uws_history", "uws_rma.ord_nr = uws_history.crec_id")
        qsc_returns.add_parameter("@req_status", "Verzoek")
        qsc_returns.add_parameter("@req_status2", "In behandeling")

        'als de optionele parameter true is wordt er gecheckt op de search functies
        If search Then
            'filter op rma code
            If Not String.IsNullOrEmpty(Me.txt_rma_code.Text) Then
                qsc_returns.select_where += " AND RMA_CODE like @rma_code"
                qsc_returns.add_parameter("@rma_code", Me.txt_rma_code.Text + "%")
            End If

            'check op customer id
            If Not String.IsNullOrEmpty(Me.txt_cust_nr.Text) Then
                qsc_returns.select_where += " AND uws_rma.cust_id like @cust_id"
                qsc_returns.add_parameter("@cust_id", Me.txt_cust_nr.Text + "%")
            End If

            'check op customer name
            If Not String.IsNullOrEmpty(Me.txt_cust_name.Text) Then

                Dim fullName() As String = Split(Me.txt_cust_name.Text, " ")
                Dim firstName As String = ""
                Dim infix As String = ""
                Dim lastName As String = ""

                For i As Integer = 0 To fullName.Length - 1
                    If i = 0 Then
                        firstName = fullName(i)
                    End If
                    If i >= 1 Then
                        If fullName.Length > 2 Then
                            If Not i = fullName.Length - 1 Then
                                If i = 1 Then
                                    infix = fullName(i)
                                Else
                                    infix += " " + fullName(i)
                                End If
                            Else
                                lastName = fullName(i)
                            End If
                        Else
                            lastName = fullName(i)
                        End If
                    End If
                Next

                qsc_returns.select_where += " AND uws_rma.cust_id in (select rec_id from uws_customers where fst_name like @fst_name and infix like @infix and lst_name like @lst_name)"
                qsc_returns.add_parameter("@fst_name", firstName + "%")
                qsc_returns.add_parameter("@infix", infix + "%")
                qsc_returns.add_parameter("@lst_name", lastName + "%")
            End If

            'check op factuurnummer
            If Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
                qsc_returns.select_where += " AND rma_code in (select hdr_id from uws_rma_line where doc_nr = @doc_nr) OR ord_nr in (select crec_id from uws_history where doc_nr = @doc_nr)"
                qsc_returns.add_parameter("@doc_nr", Me.txt_invoice_nr.Text)
            End If

            'check op ordernummer
            If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Then
                qsc_returns.select_where += " AND uws_rma.ord_nr like @ord_nr OR uws_rma.rma_code in (select hdr_id from uws_rma_line where ord_nr like @ord_nr)"
                qsc_returns.add_parameter("@ord_nr", "%" + Me.txt_order_code.Text + "%")
            End If

            'check op datum
            If Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                Dim req_date As String = ""

                'bouw de string voor de search op de request date hier op
                If String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                    req_date += "%-"
                Else
                    req_date += Me.txt_request_year.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_month.Text) Then
                    req_date += "%-"
                Else
                    req_date += "%" + Me.txt_request_month.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_day.Text) Then
                    req_date += "%"
                Else
                    req_date += "%" + Me.txt_request_day.Text + "%"
                End If

                'check of er al een optie gebruikt is of niet
                qsc_returns.select_where += " AND CONVERT(VARCHAR, req_date, 120) like @req_date"
                qsc_returns.add_parameter("@req_date", req_date)
            End If
        End If

        Dim dt_returns As New System.Data.DataTable
        dt_returns = qsc_returns.execute_query()

        'set de status van het verzoek naar de juiste taal
        For Each row In dt_returns.Rows
            Dim lc_req_status As String = row.item("req_status")
            Select Case lc_req_status
                Case "Verzoek"
                    row.item("req_status") = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "In behandeling"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select
        Next

        Return dt_returns
    End Function

#Region "Page subs"
    Protected Sub pageinit1(ByVal sender As Object, e As System.EventArgs) Handles Me.Init
        'check of de gebruiker ingelogd is
        If Not Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If

        If Not Me.global_rma.user_information.user_accept Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/home.aspx", True)
        End If

        Me.set_style_sheet("uc_return_acceptance.css", Me)

        ' Load the change overview user control and add it to the page in a placeholder
        uc_change_overview = LoadUserControl("~/RMA/PageBlocks/uc_change_overview.ascx")
        Me.ph_change_overview.Controls.Add(uc_change_overview)

        Me.rpt_returns.next_page_text = global_trans.translate_label("LABEL_LIST_NEXT", "Volgende pagina", Me.global_cms.Language)
        Me.rpt_returns.previous_page_text = global_trans.translate_label("LABEL_LIST_PREVIOUS", "Vorige pagina", Me.global_cms.Language)

        Dim dt_returns As New System.Data.DataTable
        dt_returns = Me.get_returns()

        If Not Me.get_page_parameter("rmacode") = "" Then
            Me.set_return_details(Me.get_page_parameter("rmacode"))
        End If

        'check of er retouren aanwezig zijn
        If dt_returns.Rows.Count() > 0 Then
            rpt_returns.DataSource = dt_returns
            rpt_returns.DataBind()
        Else
            Me.ph_no_returns.Visible = True
            Me.ph_returns.Visible = False
        End If
        Me.set_status_styling()
        Me.button_dashboard.OnClientClick = "document.location='home.aspx'; return false"

        Me.check_assigend_comp.Text = global_trans.translate_label("label_check_assigned_comp", "Een compensatie toewijzen aan dit verzoek", Me.global_ws.Language)

        'Set reasons
        Dim qsc_reasons As New Utilize.Data.QuerySelectClass
        With qsc_reasons
            .select_fields = "distinct reason_desc_" + Me.global_ws.Language
            .select_from = "uws_rma_reason"
        End With

        Dim dt_reason As New System.Data.DataTable
        dt_reason = qsc_reasons.execute_query()

        'Insert reason selection
        Dim top_row_default As System.Data.DataRow = dt_reason.NewRow()
        top_row_default("reason_desc_" + Me.global_ws.Language) = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language)
        dt_reason.Rows.InsertAt(top_row_default, 0)
    End Sub

    Protected Sub show_assign_comp()
        If Not ph_assigned_compensation.Visible Then
            ph_assigned_compensation.Visible = True
        Else
            ph_assigned_compensation.Visible = False
        End If
    End Sub

    Private Sub set_status_styling()
        'geef de styling van de status aan de elementen aan de hand van wat voor iets het is
        For Each item In Me.rpt_returns.Items
            Dim cb_status As Utilize.Web.UI.WebControls.label
            cb_status = item.findcontrol("row_status")

            Select Case cb_status.Text
                Case global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-default")
                Case global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-warning")
                Case Else
                    Exit Select
            End Select
        Next
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_width", "$(document).ready(function () {set_status_width();});", True)
    End Sub

    Protected Sub set_return_details(rma_code As String)
        'verbergd de onderdelen die op het moment zichtbaar waren en maakt een paar onderdelen zichtbaar
        Me.ph_assigned_comp.Visible = False
        Me.ph_return_details.Visible = True
        Me.ph_returns.Visible = False
        Me.button_close_details.Visible = True
        Me.txt_reject_reason.Text = ""
        Me.ph_choice_buttons.Visible = True
        Me.ph_reject_error.Visible = False

        'haal alle rma_lines op
        Dim qsc_return_detail_lines As New Utilize.Data.QuerySelectClass
        With qsc_return_detail_lines
            .select_fields = "*"
            .select_from = "uws_rma_line"
            .select_where = "hdr_id = @hdr_id"
        End With
        qsc_return_detail_lines.add_parameter("@hdr_id", rma_code)

        Dim dt_rma_lines As System.Data.DataTable = qsc_return_detail_lines.execute_query()

        'haal alle rma's op
        Dim qsc_return_details As New Utilize.Data.QuerySelectClass
        With qsc_return_details
            .select_fields = "uws_rma.*,uws_history.doc_nr,uws_customers.bus_name"
            .select_from = "uws_rma, uws_history,uws_customers"
            .select_where = "rma_code = @rma_code and uws_rma.cust_id = uws_customers.rec_id "
            .select_order = "rma_code DESC"
        End With
        qsc_return_details.add_parameter("@rma_code", rma_code)

        Dim dt_rma_details As System.Data.DataTable = qsc_return_details.execute_query()

        'get de compensatie omschrijving van de compensatie die aan het retour gekoppeld is
        Dim qsc_comp_text As New Utilize.Data.QuerySelectClass
        With qsc_comp_text
            .select_fields = "*"
            .select_from = "uws_rma_compensation"
            .select_where = "comp_code = @comp_code"
        End With
        qsc_comp_text.add_parameter("@comp_code", dt_rma_details.Rows(0).Item("comp_code"))

        Dim dt_comp_text As System.Data.DataTable = qsc_comp_text.execute_query()

        Dim lc_comp_text As String = dt_comp_text.Rows(0).Item("comp_desc_" + Me.global_ws.Language)

        'check of de compensatie een alternatief product is
        If dt_comp_text.Rows(0).Item("comp_type") = "1" Then
            Me.ph_alt_prod_table.Visible = True
            Dim qsc_alt_prods As New Utilize.Data.QuerySelectClass
            With qsc_alt_prods
                .select_fields = "*"
                .select_from = "uws_rma_alt_prod"
                .select_where = "rma_code = @rma_code"
            End With
            qsc_alt_prods.add_parameter("@rma_code", rma_code)

            Dim dt_alt_prods As System.Data.DataTable = qsc_alt_prods.execute_query()

            Me.rpt_alt_prod.DataSource = dt_alt_prods
            Me.rpt_alt_prod.DataBind()
        Else
            Me.ph_alt_prod_table.Visible = False
        End If

        'check of er een compensatie toegewezen is door een medewerker
        If Not dt_rma_details.Rows(0).Item("asgn_comp_code") = "" Then
            Me.ph_assigned_comp.Visible = True

            Dim qsc_assigned As New Utilize.Data.QuerySelectClass
            With qsc_assigned
                .select_fields = "comp_desc_" + Me.global_ws.Language
                .select_from = "uws_rma_compensation"
                .select_where = "comp_code = @asgn_comp_code"
            End With
            qsc_assigned.add_parameter("@asgn_comp_code", dt_rma_details.Rows(0).Item("asgn_comp_code"))

            Dim dt_asgn_compensation As System.Data.DataTable = qsc_assigned.execute_query()

            Me.lbl_assigned_compensation.Text = dt_asgn_compensation.Rows(0).Item("comp_desc_" + Me.global_ws.Language)
        End If

        'set de label waardes naar de informatie uit de database
        Me.lbl_return_number.Text = dt_rma_details.Rows(0).Item("rma_code")
        Me.lbl_return_customer.Text = dt_rma_details.Rows(0).Item("bus_name")
        Me.lbl_return_order_number.Text = dt_rma_details.Rows(0).Item("doc_nr")
        Me.lbl_compensation.Text = lc_comp_text
        Me.lbl_req_date.Text = dt_rma_details.Rows(0).Item("req_date")

        Dim lc_req_status As String = dt_rma_details.Rows(0).Item("req_status")
        Select Case lc_req_status
            Case "Verzoek"
                Me.lbl_req_status.Text = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
            Case "In behandeling"
                Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
            Case Else
                Exit Select
        End Select

        Me.lbl_reason_description.Text = dt_rma_details.Rows(0).Item("reason_comm")

        Me.rpt_return_detail_lines.DataSource = dt_rma_lines
        Me.rpt_return_detail_lines.DataBind()

        Me.uc_change_overview.set_property("rma_code", rma_code)
        Me.ph_change_overview.Visible = True
    End Sub

    Protected Sub rpt_returns_PreRender(sender As Object, e As EventArgs) Handles rpt_returns.PreRender
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "clickRow", "$(document).ready(function () {clickRow();});", True)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Me.set_status_styling()

            'dit is code die door javascript uitgevoerd wordt als iemand op een row klikt in het overzicht
            Dim eventTarget As String
            Dim eventArgument As String

            If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                eventTarget = String.Empty
            Else
                eventTarget = Me.Request("__EVENTTARGET")
            End If
            If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                eventArgument = String.Empty
            Else
                eventArgument = Me.Request("__EVENTARGUMENT")
            End If

            If eventTarget = "CallReturn" Then
                Dim rma_code = eventArgument
                set_return_details(rma_code)
            End If
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_data_targets" + Me.UniqueID, "$(document).ready(function () {set_caret_links();});", True)
    End Sub

#End Region

#Region "Page buttons"
    Protected Sub button_close_details_click() Handles button_close_details.Click
        Me.ph_returns.Visible = True
        Me.ph_return_details.Visible = False
        Me.button_close_details.Visible = False
        Me.ph_accept_field.Visible = False
        Me.ph_reject_field.Visible = False
        Me.ph_change_overview.Visible = False
        Me.ph_more_information_field.Visible = False
    End Sub

    Protected Sub button_reject_confirm_click() Handles button_reject_confirm.Click
        Dim ll_testing = True

        If ll_testing Then
            If Me.txt_reject_reason.Text = "" Then
                ll_testing = False
            End If
        End If

        If Not ll_testing Then
            Me.ph_reject_error.Visible = True
        End If

        If ll_testing Then
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)
            'set de afkeuringsreden en de status
            If ll_testing Then
                ll_testing = ubo_bus_obj.field_set("uws_rma.reject_reason", Me.txt_reject_reason.Text)
                If ll_testing Then
                    ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "Afgekeurd")
                End If

                If ll_testing Then
                    ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
                End If

                'maak een change aan voor de afkeuring
                If ll_testing Then
                    Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
                    ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

                    ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
                    ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
                    ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
                    ubo_change.field_set("uws_rma_changes.change_status", "afgekeurd")
                    ubo_change.field_set("uws_rma_changes.change_text", Me.txt_reject_reason.Text)

                    ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
                End If

                If ll_testing Then
                    Dim dt_returns As New System.Data.DataTable
                    dt_returns = Me.get_returns()

                    'check of er retouren aanwezig zijn
                    If dt_returns.Rows.Count() > 0 Then
                        rpt_returns.DataSource = dt_returns
                        rpt_returns.DataBind()

                        Me.set_status_styling()

                        'maak de velden zichtbaar voor als er nog wel retouren zijn
                        Me.ph_no_returns.Visible = False
                        Me.ph_returns.Visible = True
                        Me.ph_return_details.Visible = False
                        Me.ph_reject_field.Visible = False
                        Me.button_close_details.Visible = False
                        Me.ph_change_overview.Visible = False
                    Else
                        'maar de velden zichtbaar voor als er geen retouren zijn
                        Me.ph_no_returns.Visible = True
                        Me.ph_returns.Visible = False
                        Me.ph_return_details.Visible = False
                        Me.ph_reject_field.Visible = False
                        Me.button_close_details.Visible = False
                        Me.ph_change_overview.Visible = False
                    End If
                End If
            End If
            Me.set_status_styling()
        End If
    End Sub

    Protected Sub button_reject_click() Handles button_reject_rma_request.Click
        Me.ph_reject_field.Visible = True
        Me.ph_choice_buttons.Visible = False
    End Sub

    Protected Sub button_reject_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_reject_cancel.Click
        Me.txt_reject_reason.Text = ""
        Me.ph_reject_field.Visible = False
        Me.ph_choice_buttons.Visible = True
    End Sub

    Protected Sub button_accept_clicked(sender As Object, e As System.EventArgs) Handles button_accept_rma_request.Click
        Me.ph_accept_field.Visible = True
        Me.ph_choice_buttons.Visible = False
    End Sub

    Protected Sub button_accept_confirm_click() Handles button_accept_confirm.Click
        Dim ll_testing = True

        'maak een business object aan voor het goedkeuren
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")

        ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)
        If ll_testing Then
            ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "Goedgekeurd")
            ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            Dim loPDFGenerate As New ExpertPdf.HtmlToPdf.PdfConverter
            With loPDFGenerate
                .LicenseKey = "Apxofk9W45ihltLQtKYelQ7h9+0Jm3o+D/mGrPycnO7rHozfB9KwzbnQ7M6ZjIGE"
            End With

            Dim loByte() As Byte = loPDFGenerate.GetPdfBytesFromHtmlString(Me.global_rma.generate_rma_form(Me.lbl_return_number.Text, Me.global_ws.Language))

            If Not System.IO.File.Exists(Server.MapPath("~/documents/RMA")) Then
                System.IO.Directory.CreateDirectory(Server.MapPath("~/documents/RMA"))
                System.IO.Directory.CreateDirectory(Server.MapPath("~/documents/RMA/RMA forms"))
            End If

            If Not System.IO.File.Exists(Server.MapPath("~/documents/RMA/RMA forms")) Then
                System.IO.Directory.CreateDirectory(Server.MapPath("~/documents/RMA/RMA forms"))
            End If

            loPDFGenerate.SavePdfFromHtmlStringToFile(Me.global_rma.generate_rma_form(Me.lbl_return_number.Text, Me.global_ws.Language), Server.MapPath("~/documents/RMA/RMA forms/rma" + Me.lbl_return_number.Text + ".pdf"))
        End If

        If ll_testing Then
            'maak een business object aan voor de rma_changes zodat er een status opgeslagen kan worden voor het retour
            Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
            ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

            ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
            ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
            ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
            ubo_change.field_set("uws_rma_changes.change_status", "goedgekeurd")

            ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            Dim dt_returns As New System.Data.DataTable
            dt_returns = Me.get_returns()

            'check of er retouren aanwezig zijn
            If dt_returns.Rows.Count() > 0 Then
                rpt_returns.DataSource = dt_returns
                rpt_returns.DataBind()

                Me.set_status_styling()

                Me.ph_no_returns.Visible = False
                Me.ph_accept_field.Visible = False
                Me.ph_returns.Visible = True
                Me.ph_return_details.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_change_overview.Visible = False
            Else
                Me.ph_no_returns.Visible = True
                Me.ph_accept_field.Visible = False
                Me.ph_returns.Visible = False
                Me.ph_return_details.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_change_overview.Visible = False
            End If
            Me.set_status_styling()
        End If
    End Sub

    Protected Sub button_accept_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_accept_cancel.Click
        Me.ph_accept_field.Visible = False
        Me.ph_choice_buttons.Visible = True
    End Sub

    Protected Sub button_more_information_clicked(sender As Object, e As System.EventArgs) Handles button_more_information.Click
        Me.ph_more_information_field.Visible = True
        Me.ph_choice_buttons.Visible = False

        'set de de compensaties voor het mogelijke toewijzen van een compensatie
        Dim qsc_comps As New Utilize.Data.QuerySelectClass
        With qsc_comps
            .select_fields = "*"
            .select_from = "uws_rma_compensation"
        End With
        Dim dt_comp As System.Data.DataTable = qsc_comps.execute_query()

        'voeg een regel toe die aangeeft dat er eerst een reden geselecteerd moet worden voordat er een compensatie geselecteerd worden
        Dim top_row_default_comp As System.Data.DataRow = dt_comp.NewRow()
        top_row_default_comp("comp_desc_" + Me.global_ws.Language) = global_trans.translate_label("label_compensation_rma", "-Wijs een compensatie toe-", Me.global_ws.Language)
        top_row_default_comp("comp_code") = "0000"
        dt_comp.Rows.InsertAt(top_row_default_comp, 0)

        Me.dd_assigned_compensation.DataSource = dt_comp
        Me.dd_assigned_compensation.DataTextField = "comp_desc_" + Me.global_ws.Language
        Me.dd_assigned_compensation.DataValueField = "comp_code"
        Me.dd_assigned_compensation.DataBind()
    End Sub

    Protected Sub button_more_information_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_more_information_cancel.Click
        Me.ph_choice_buttons.Visible = True
        Me.ph_more_information_field.Visible = False
    End Sub

    Protected Sub button_more_information_send_clicked(sender As Object, e As System.EventArgs) Handles button_more_information_send.Click
        Dim ll_testing = True

        If ll_testing Then
            If String.IsNullOrEmpty(Me.txt_more_information_message.Text) Then
                ll_testing = False
                Me.ph_more_information_error.Visible = True
            End If
        End If

        If ll_testing Then
            'maak een business object aan voor de rma_changes 
            Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
            ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

            'set de informatie voor een meer informatie bericht
            ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
            ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
            ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
            ubo_change.field_set("uws_rma_changes.change_status", "meer informatie")
            ubo_change.field_set("uws_rma_changes.change_text", Me.txt_more_information_message.Text)
            If Me.check_assigend_comp.Checked Then
                ubo_change.field_set("uws_rma_changes.change_comp", Me.dd_assigned_compensation.SelectedValue)
            End If
            ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            'set de status van het rma naar in behandeling
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)
            If ll_testing Then
                ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "In behandeling")
            End If
            If ll_testing Then
                If Me.check_assigend_comp.Checked Then
                    ll_testing = ubo_bus_obj.field_set("uws_rma.asgn_comp_code", Me.dd_assigned_compensation.SelectedValue)
                End If
            End If
            If ll_testing Then
                ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
            End If
        End If

        If ll_testing Then
            Dim dt_returns As New System.Data.DataTable
            dt_returns = Me.get_returns()

            'check of er retouren aanwezig zijn
            If dt_returns.Rows.Count() > 0 Then
                rpt_returns.DataSource = dt_returns
                rpt_returns.DataBind()

                Me.ph_no_returns.Visible = False
                Me.ph_accept_field.Visible = False
                Me.ph_returns.Visible = True
                Me.ph_return_details.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_more_information_field.Visible = False
                Me.ph_more_information_error.Visible = False
                Me.ph_change_overview.Visible = False
            Else
                Me.ph_no_returns.Visible = True
                Me.ph_accept_field.Visible = False
                Me.ph_returns.Visible = False
                Me.ph_return_details.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_more_information_error.Visible = False
                Me.ph_more_information_field.Visible = False
                Me.ph_change_overview.Visible = False
            End If
            Me.set_status_styling()
        End If
    End Sub

#Region "search subs"
    Protected Sub button_search_clicked(sender As Object, e As System.EventArgs) Handles button_search.Click
        'open de search
        Me.ph_search.Visible = True
        Me.button_search.Visible = False
        Me.button_filter_remove.Visible = False
        Me.button_close_search.Visible = True
    End Sub

    Protected Sub button_search_close_clicked(sender As Object, e As System.EventArgs) Handles button_close_search.Click
        'sluit de search box
        Me.ph_search.Visible = False
        Me.button_search.Visible = True
        Me.button_close_search.Visible = False


        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()
        Me.rpt_returns.DataSource = dt_returns
        Me.rpt_returns.DataBind()

        Me.set_status_styling()

        If Me.ph_no_found.Visible Then
            Me.ph_no_found.Visible = False
            Me.ph_return_table.Visible = True
        End If

        'reset de search fields zodat ze weer leeg zijn
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""
        Me.button_filter_remove.Visible = False
    End Sub

    Protected Sub button_filter_clicked(sender As Object, e As System.EventArgs) Handles button_filter.Click
        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        'reset de pagina zodat alles weer zichtbaar is
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            Me.set_status_styling()

            'als het no found zichtbaar is dan wordt deze weer ontzichtbaar gemaakt en de normale weergave wordt weer zichtbaar
            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_filter_remove_clicked(sender As Object, e As System.EventArgs) Handles button_filter_remove.Click
        'reset de search fields zodat ze leeg zijn
        Me.button_filter_remove.Visible = False
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""

        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()

        'rebind de repeater zodat de table weer alle retouren weer geeft
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()

            Me.set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        End If
    End Sub

    Private Sub rpt_return_detail_lines_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_return_detail_lines.ItemCreated
        If Not IsNothing(e.Item.DataItem) Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                ' Load the image overview user control and add it to the current item in a placeholder
                Dim ph_image_overview As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_image_overview")
                Dim uc_image_overview As Utilize.Web.Solutions.RMA.rma_user_control = LoadUserControl("~/RMA/UserControls/uc_image_overview.ascx")
                uc_image_overview.ID = "uc_image_overview_" + e.Item.DataItem("rec_id")
                ph_image_overview.Controls.Add(uc_image_overview)

                Dim qsc_images As New Utilize.Data.QuerySelectClass
                With qsc_images
                    .select_fields = "image"
                    .select_from = "uws_rma_line_images"
                    .select_where = "hdr_id = @hdr_id"
                End With
                qsc_images.add_parameter("@hdr_id", e.Item.DataItem("rec_id"))

                Dim dt_images As System.Data.DataTable = qsc_images.execute_query()

                ' Load the images in the lines image overview user control
                uc_image_overview.set_property("dt_images", dt_images)

                If dt_images.Rows.Count > 0 Then
                    If Not rpt_return_detail_lines.Controls(0).FindControl("col_caret_down").Visible Then
                        rpt_return_detail_lines.Controls(0).FindControl("col_caret_down").Visible = True
                    End If
                    Dim td_caret_down As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("td_caret_down")
                    td_caret_down.Visible = True
                End If
            End If
        End If
    End Sub

#End Region

#End Region

End Class
