﻿
Partial Class RMA_PageBlocks_uc_return_dashboard
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'controle of de user ingelogd is
        If Not Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If

        Me.set_javascript("uc_return_dashboard.js", Me)
        Me.set_style_sheet("uc_return_dashboard.css", Me)

        'maak een datatable voor de laatste 10 retouren
        Dim qsc_last_ten As New Utilize.Data.QuerySelectClass
        qsc_last_ten.select_fields = "top 10 uws_rma.*,uws_customers.bus_name"
        qsc_last_ten.select_from = "uws_rma,uws_customers"
        qsc_last_ten.select_where = "uws_rma.cust_id = uws_customers.rec_id"
        qsc_last_ten.select_order = "rma_code desc"

        Dim dt_last_ten As System.Data.DataTable = qsc_last_ten.execute_query()

        'set de namen van de elementen naar de juiste taal
        For Each row In dt_last_ten.Rows
            dim lc_req_status as string = row.item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "Verzoek"
                    row.item("req_status") = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "Afgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                Case "Afgerond"
                    row.item("req_status") = global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                Case "In behandeling"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select
        Next

        rpt_returns.DataSource = dt_last_ten
        rpt_returns.DataBind()

        set_status_styling()
    End Sub

    Private Sub set_status_styling()
        'geef de styling van de status aan de elementen aan de hand van wat voor iet het is
        For Each item In Me.rpt_returns.Items
            Dim cb_status As System.Web.UI.WebControls.Label
            cb_status = item.findcontrol("row_status")

            Select Case cb_status.Text
                Case global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-default")
                Case global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-danger")
                Case global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-primary")
                Case global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-success")
                Case global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-warning")
            End Select
        Next
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_width", "$(document).ready(function () {set_status_width();});", True)
    End Sub

    Sub set_percentages()
        Dim qsc_returns As New Utilize.Data.QuerySelectClass
        qsc_returns.select_fields = "*"
        qsc_returns.select_from = "uws_rma"

        Dim dt_return As New System.Data.DataTable
        dt_return = qsc_returns.execute_query()

        Dim done As Integer = 0
        Dim accepted As Integer = 0
        Dim rejected As Integer = 0
        Dim request As Integer = 0
        Dim more_information As Integer = 0

        'loopt door alle rma's heen en telt hoeveel er van elke status zijn
        For i As Integer = 0 To dt_return.Rows.Count - 1

            dim lc_req_status as string = dt_return.Rows(i).Item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    accepted += 1
                Case "Afgekeurd"
                    rejected += 1
                Case "Afgerond"
                    done += 1
                Case "Verzoek"
                    request += 1
                Case "In behandeling"
                    more_information += 1
                Case "In behandeling na ontvangst"
                    more_information += 1
                Case Else
                    Exit Select
            End Select
        Next

        'bouw de javascript string op met de waardes die nodig zijn
        Dim javascript_total As String = "google.setOnLoadCallback(drawChart);"
        javascript_total += "function drawChart() {"
        javascript_total += "var data = google.visualization.arrayToDataTable(["
        javascript_total += "['Status', 'Hoeveelheid', 'extra'],"
        javascript_total += "[""" + global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language) + """," + done.ToString() + ", 'Afgerond'],"
        javascript_total += "[""" + global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language) + """," + rejected.ToString() + ", 'Afgekeurd'],"
        javascript_total += "[""" + global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language) + """," + accepted.ToString() + ", 'Goedgekeurd'],"
        javascript_total += "[""" + global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language) + """," + more_information.ToString() + ", 'In behandeling'],"
        javascript_total += "[""" + global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language) + """," + request.ToString() + ", 'Verzoek']"
        javascript_total += "]);"
        javascript_total += "var options = {title: '" + global_trans.translate_label("label_total_overview", "Totaal overzicht", Me.global_ws.Language) + "', backgroundColor:  '#f1f1f1', colors: ['#5cb85c', '#d9534f', '#337ab7', '#f0ad4e', '#777'], legend: {position: 'left'}};"
        javascript_total += "var chart = new google.visualization.PieChart(document.getElementById('piechart'));"
        javascript_total += "chart.draw(data, options);"
        javascript_total += "$(window).resize( function () {chart.draw(data, options);});"
        javascript_total += "google.visualization.events.addListener(chart, 'select', function() {"
        javascript_total += "var row = chart.getSelection()[0].row;"
        javascript_total += "window.location.href = window.location.href.replace('home.aspx','returns.aspx') + '?status=' + data.getValue(row, 2);"
        javascript_total += "});"
        javascript_total += "}"

        System.Diagnostics.Debug.WriteLine(javascript_total)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "pie_chart", javascript_total, True)

        'code voor de donut chart met reasons
        Dim javascript As String = "google.setOnLoadCallback(drawChart);"
        javascript += "var data1 = google.visualization.arrayToDataTable([['Reden', 'Hoeveelheid'],"

        Dim qsc_reasons As New Utilize.Data.QuerySelectClass
        qsc_reasons.select_fields = "reason_code, reason_desc_" + Me.global_ws.Language
        qsc_reasons.select_from = "uws_rma_reason"

        Dim dt_reasons As System.Data.DataTable = qsc_reasons.execute_query()

        'voeg een nieuwe colom toe aan de datatable waar de hoeveelheden opgeslagen worden
        Dim col As System.Data.DataColumn = New System.Data.DataColumn("Quantity", GetType(Int32))
        dt_reasons.Columns.Add(col)
        For Each row As System.Data.DataRow In dt_reasons.Rows
            row.Item("Quantity") = 0
        Next

        Dim qsc_return_rows As New Utilize.Data.QuerySelectClass
        qsc_return_rows.select_fields = "*"
        qsc_return_rows.select_from = "uws_rma_line"

        Dim dt_return_lines As System.Data.DataTable = qsc_return_rows.execute_query()

        'hier worden de per return row gekeken welke redenen er zijn en het aantal wordt +1
        For Each row_return As System.Data.DataRow In dt_return_lines.Rows
            For Each row_reason As System.Data.DataRow In dt_reasons.Rows
                If row_return.Item("reason_code") = row_reason.Item("reason_code") Then
                    row_reason.Item("Quantity") += row_return.Item("return_qty")
                End If
            Next
        Next

        'maak een nieuwe datatabel aan die gebruikt kan worden om data op te halen
        Dim dt_reason_amounts As New System.Data.DataTable
        Dim name As System.Data.DataColumn = New System.Data.DataColumn("Reason_desc")
        Dim qty As System.Data.DataColumn = New System.Data.DataColumn("Quantity", GetType(Int32))
        dt_reason_amounts.Columns.Add(name)
        dt_reason_amounts.Columns.Add(qty)

        'maak een firstline aan zodat er door de lines heen geloopt kan worden
        Dim dr_row As System.Data.DataRow = dt_reason_amounts.NewRow()
        dr_row("Reason_desc") = "firstline"
        dr_row("Quantity") = 0
        dt_reason_amounts.Rows.Add(dr_row)

        'loop door alle reasons heen en vul de dt_reason_amounts
        For Each row As System.Data.DataRow In dt_reasons.Rows
            Dim found As Boolean = False

            'loop om te kijken of de reden al bestaat in de tabel, als dit waar is dan wordt de hoeveelheid opgeted bij het element
            For i As Integer = 0 To dt_reason_amounts.Rows.Count - 1
                If row.Item("reason_desc_" + Me.global_ws.Language).ToString() = dt_reason_amounts.Rows(i).Item("Reason_desc").ToString() Then
                    dt_reason_amounts.Rows(i).Item("Quantity") += row.Item("Quantity")
                    found = True
                End If
            Next
            'als hij niet gevonden is dan wordt hij toegevoegd aan de datatable
            If Not found Then
                Dim dr_new As System.Data.DataRow = dt_reason_amounts.NewRow()
                dr_new("Reason_desc") = row.Item("reason_desc_" + Me.global_ws.Language)
                dr_new("Quantity") = Integer.Parse(row.Item("Quantity"))
                dt_reason_amounts.Rows.Add(dr_new)
                found = True
            End If

        Next

        'verwijder de eerste dummy regel
        dt_reason_amounts.Rows(0).Delete()

        'bouw hier de javascript op met de elementen uit de datatable
        For Each row As System.Data.DataRow In dt_reason_amounts.Rows
            javascript += "[""" + row.Item("Reason_desc") + """," + row.Item("Quantity").ToString() + "],"
        Next

        'haal de laatste comma weg uit de string
        javascript = javascript.Substring(0, javascript.Length - 1)

        javascript += "]);"

        'de settings van het reden overzicht
        javascript += "var options1 = {title: '" + global_trans.translate_label("label_reasons", "Retour redenen", Me.global_ws.Language) + "', backgroundColor:  '#f1f1f1', colors: ['#5cb85c', '#d9534f', '#337ab7', '#f0ad4e', '#777'], legend: { position: 'left' }};"

        javascript += "var chart = new google.visualization.PieChart(document.getElementById('donutchart')); "
        javascript += "chart.draw(data1, options1);"
        javascript += "$(window).resize( function () {chart.draw(data1, options1);});"
        javascript += "google.visualization.events.addListener(chart, 'select', function() {"
        javascript += "var row = chart.getSelection()[0].row;"
        javascript += "window.location.href = window.location.href.replace('home.aspx','returnlinestop.aspx') + '?reason=' + data1.getValue(row, 0);"
        javascript += "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "donut_chart", javascript, True)
    End Sub

    Protected Sub Page_InitComplete() Handles Me.InitComplete
        set_percentages()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Me.set_status_styling()

            'dit is code die door javascript uitgevoerd wordt als iemand op een row klikt in het overzicht
            Dim eventTarget As String
            Dim eventArgument As String

            If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                eventTarget = String.Empty
            Else
                eventTarget = Me.Request("__EVENTTARGET")
            End If
            If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                eventArgument = String.Empty
            Else
                eventArgument = Me.Request("__EVENTARGUMENT")
            End If

            If eventTarget = "CallReturn" Then
                Dim rma_code = eventArgument
                Dim qsc_rma As New Utilize.Data.QuerySelectClass
                qsc_rma.select_from = "uws_rma"
                qsc_rma.select_fields = "req_status"
                qsc_rma.select_where = "rma_code = @rma_code"
                qsc_rma.add_parameter("@rma_code", rma_code)

                Dim dt_rma As System.Data.DataTable = qsc_rma.execute_query()

                Select Case dt_rma.Rows(0).Item("req_status").ToString().ToLower()
                    Case "goedgekeurd"
                        If Me.global_rma.user_information.user_incoming Then
                            Response.Redirect(Me.ResolveUrl("~/RMA/ReturnIncoming.aspx") + "?rmacode=" + rma_code)
                        Else
                            Response.Redirect(Me.ResolveUrl("~/RMA/Returns.aspx") + "?rmacode=" + rma_code)
                        End If
                    Case "verzoek"
                        If Me.global_rma.user_information.user_accept Then
                            Response.Redirect(Me.ResolveUrl("~/RMA/ReturnAcceptance.aspx") + "?rmacode=" + rma_code)
                        Else
                            Response.Redirect(Me.ResolveUrl("~/RMA/Returns.aspx") + "?rmacode=" + rma_code)
                        End If
                    Case "in behandeling"
                        If Me.global_rma.user_information.user_accept Then
                            Response.Redirect(Me.ResolveUrl("~/RMA/ReturnAcceptance.aspx") + "?rmacode=" + rma_code)
                        Else
                            Response.Redirect(Me.ResolveUrl("~/RMA/Returns.aspx") + "?rmacode=" + rma_code)
                        End If
                    Case "in behandeling na ontvangst"
                        If Me.global_rma.user_information.user_incoming Then
                            Response.Redirect(Me.ResolveUrl("~/RMA/ReturnIncoming.aspx") + "?rmacode=" + rma_code)
                        Else
                            Response.Redirect(Me.ResolveUrl("~/RMA/Returns.aspx") + "?rmacode=" + rma_code)
                        End If
                    Case Else
                        Response.Redirect(Me.ResolveUrl("~/RMA/Returns.aspx") + "?rmacode=" + rma_code)
                End Select
            End If
        End If
    End Sub

    Protected Sub rpt_returns_PreRender(sender As Object, e As EventArgs) Handles rpt_returns.PreRender
        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "clickRow", "$(document).ready(function () {clickRowDashboard(""" + Me.ResolveUrl("~/rma/returns.aspx") + """,""" + Me.ResolveUrl("~/rma/return_acceptance.aspx") + """,""" + Me.ResolveUrl("~/rma/return_incoming.aspx") + ""","");});", True)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "clickRow", "$(document).ready(function () {clickRow();});", True)
    End Sub
End Class
