﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_navigation.ascx.vb" Inherits="RMA_PageBlocks_uc_navigation" %>

<%@ Register Src="~/RMA/PageBlocks/uc_languages_block.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>

<div id="uc_navigation">
    <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	    <div class="navbar-header">
		    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar_content" aria-expanded="false">
			    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
		    </button>
            <a class="navbar-brand" href="<%= Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/rma/home.aspx" %>">
                <img class="logo" alt="Logo" src="<%= Me.ResolveCustomUrl("~/") + Me.global_rma.get_rma_setting("rma_logo")%>" />
            </a>
	    </div>
	    <div class="collapse navbar-collapse" id="navbar_content" style="margin-right: 10px">
		    <ul class="nav navbar-nav navbar-left">
                <utilize:placeholder runat="server" ID="ph_menu">
                    <li>
                        <utilize:placeholder runat="server" ID="ph_returns"><utilize:translatelink runat="server" ID="link_account_rma_returns" text="Retouren overzicht"></utilize:translatelink></utilize:placeholder>
                    </li>
                    <li>
                        <utilize:placeholder runat="server" ID="ph_acceptance"><utilize:translatelink runat="server" ID="link_account_return_acceptance" text="Retouren beoordelen"></utilize:translatelink></utilize:placeholder>
                    </li>
                    <li>
                        <utilize:placeholder runat="server" ID="ph_incoming"><utilize:translatelink runat="server" ID="link_account_incomming_return" text="Binnengekomen retouren verwerken"></utilize:translatelink></utilize:placeholder>
                    </li>
                </utilize:placeholder>
            </ul>
            <ul class="nav navbar-nav navbar-right">                         
                <uc1:uc_languages_block runat="server" ID="uc_languages_block1" />                     
                <utilize:placeholder runat="server" ID="ph_signed_in">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <utilize:translatelabel runat="server" id="lt_signed_in_as" Text="Ingelogd als" />&nbsp;<utilize:label runat="server" id="lbl_user_name" Text="" CssClass="user_name"/>
                            <span class="caret"></span>
				        </a>				  
				        <ul class="dropdown-menu">
					        <li>
                                <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
					        </li>
				        </ul>
			        </li>
                </utilize:placeholder>
		    </ul>
	    </div>
    </nav>
    <div class="current-date pull-right">
        <asp:Literal runat="server" ID="lt_current_date" />
    </div>
     <div class="clearfix"></div>
</div>
