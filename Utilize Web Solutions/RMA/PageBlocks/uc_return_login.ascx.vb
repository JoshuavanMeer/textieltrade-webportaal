﻿Imports System.Linq

Partial Class RMA_PageBlocks_uc_return_login
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        If Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/home.aspx", True)
        End If

        Me.set_style_sheet("uc_return_login.css", Me)

        Me.txt_logon_name.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_name", "Gebruikersnaam", Me.global_cms.Language))
        Me.txt_logon_password.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_password", "Wachtwoord", Me.global_cms.Language))
        Me.txt_logon_name.Focus()
    End Sub

    Protected Sub button_login_clicked(sender As Object, e As System.EventArgs) Handles button_login.Click
        Dim ll_resume As Boolean = True
        Dim lcUrlReferrer As String = Server.HtmlDecode(Me.get_page_parameter("RedirectUrl"))
        Try
            If ll_resume Then
                ' Probeer in te loggen
                ll_resume = Me.global_rma.user_information.login(Me.txt_logon_name.Text, Me.txt_logon_password.Text)

                If Not ll_resume Then
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "login_error", "alert('" + Me.global_rma.user_information.error_message + "');", True)
                End If
            End If

            If ll_resume Then
                Response.Redirect("~/" + Me.global_ws.Language + "/rma/home.aspx", False)
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class