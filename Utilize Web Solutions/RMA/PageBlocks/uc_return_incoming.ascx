﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_return_incoming.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_return_incoming" %>

<div class="uc_return_incoming add-padding">
    <utilize:placeholder runat="server" ID="ph_no_incoming" Visible="false">
        <h3>
            <utilize:translatetitle runat="server" ID="title_no_incoming" Text="Geen retouren"></utilize:translatetitle></h3>
        <utilize:translatetext runat="server" ID="text_no_incoming" Text="Er zijn op het moment geen retouren die goedgekeurd zijn en nog binnen moeten komen"></utilize:translatetext>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_incoming">
        <h3>
            <utilize:translatetitle runat="server" ID="title_return_incoming" Text="Retouren overzicht"></utilize:translatetitle></h3>
        <utilize:translatetext runat="server" ID="text_return_incoming" Text="Onderstaand vind u een overzicht van alle retour verzoeken die goedgekeurd zijn en naar uw bedrijf toe gestuurd zijn. Door op het retournummer te klikken kunt u het overzicht zien van het retour."></utilize:translatetext>

        <div class="buttons margin-bottom-10">
            <utilize:translatebutton runat='server' ID="button_search" CssClass="right btn btn-primary" Text="Zoeken" />
            <utilize:translatebutton runat="server" ID="button_close_search" CssClass="btn btn-primary" Text="Zoeken sluiten" Visible="false" />
        </div>
        <utilize:placeholder runat="server" ID="ph_search" Visible="false">
            <div class="input-border">
                <div class="">
                    <utilize:translatetext runat="server" ID="text_search" Text="Vul onderstaand uw zoekcriteria in."></utilize:translatetext>
                </div>

                <utilize:placeholder runat="server" ID="ph_no_filter" Visible="false">
                    <div class="row">
                        <div class="col-md-6 alert alert-danger">
                            <utilize:translateliteral runat="server" ID="lt_no_filter_error" Text="U heeft geen filter opgegeven"></utilize:translateliteral>
                        </div>
                    </div>
                </utilize:placeholder>

                <div class="row margin-bottom-20">
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_return_code" Text="Retournummer"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_rma_code" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_customer_number" Text="klantnummer"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_cust_nr" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_customer_name" Text="klantnaam"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_cust_name" CssClass="form-control"></utilize:textbox>
                    </div>
                </div>
                <div class="row margin-bottom-20">
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_order_code" Text="Ordernummer"></utilize:translatelabel></b>
                        <utilize:textbox runat="server" ID="txt_order_code" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_invoice_nr" Text="Factuurnummer"></utilize:translatelabel></b>
                        <utilize:textbox runat="server" ID="txt_invoice_nr" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-1">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_day" Text="Dag"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_day" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                    </div>
                    <div class="col-md-1">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_month" Text="Maand"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_month" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                    </div>
                    <div class="col-md-2">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_year" Text="Jaar"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_year" CssClass="form-control" MaxLength="4" type="number" onkeypress="return this.value.length<=3"></utilize:textbox>
                    </div>
                </div>                
                <div class="buttons">
                    <utilize:translatebutton runat='server' ID="button_filter" CssClass="right btn btn-primary margin-right-5" Text="Zoeken" />
                    <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="left btn btn-primary" Text="Filter opheffen" Visible="false" />
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_no_found" Visible="false">
            <utilize:translatetext runat="server" ID="txt_no_found_returns" Text="Er zijn geen retouren gevonden met deze zoek waardes"></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_return_table">
            <utilize:repeater runat="server" ID="rpt_returns" pager_enabled="true" pagertype="bottom" page_size="20">
                <HeaderTemplate>
                    <div class="table-responsive">
                        <table class="table overview">
                            <thead>
                                <tr>
                                    <th class="col-md-2">
                                        <utilize:translatelabel runat="server" ID="col_return_code" Text="Retournummer"></utilize:translatelabel></th>
                                    <th class="col-md-2">
                                        <utilize:translatelabel runat="server" ID="col_cust_name" Text="Klantnaam"></utilize:translatelabel></th>
                                    <th class="col-md-2 text-center">
                                        <utilize:translatelabel runat="server" ID="col_req_date" Text="Verzoek datum"></utilize:translatelabel></th>                                   
                                    <th class="col-md-2">
                                        <utilize:translatelabel runat="server" ID="col_compensation" Text="Compensatie"></utilize:translatelabel></th>
                                    <th class="col-md-2">
                                        <utilize:translatelabel runat="server" ID="col_status" Text="Status"></utilize:translatelabel></th>
                                </tr>
                            </thead>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <utilize:label runat="server" ID="row_return_code" Text='<%# DataBinder.Eval(Container.DataItem, "rma_code")%>'></utilize:label></td>
                        <td>
                            <utilize:label runat="server" ID="row_cust_name" Text='<%# DataBinder.Eval(Container.DataItem, "bus_name")%>'></utilize:label></td>
                        <td class="text-center">
                            <utilize:label runat="server" ID="row_req_date" Text='<%# Format(DataBinder.Eval(Container.DataItem, "req_date"),"d")%>'></utilize:label></td>                        
                        <td>
                            <utilize:label runat="server" ID="row_compensation" Text='<%# DataBinder.Eval(Container.DataItem, "comp_desc_" + Me.global_ws.Language)%>'></utilize:label></td>
                        <td>
                            <utilize:label runat="server" ID="row_status" Text='<%# DataBinder.Eval(Container.DataItem, "req_status")%>'></utilize:label></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </div>
                </FooterTemplate>
            </utilize:repeater>
        </utilize:placeholder>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_incoming_detail" Visible="false">
        <h3>
            <utilize:translatetitle runat="server" ID="title_return_details" Text="Overzicht retour"></utilize:translatetitle></h3>
        <utilize:translatetext runat="server" ID="text_retour_incoming_detail" Text="Onderstaand is een overzicht van het geselecteerde retour, vanuit hier is het mogelijk om het retour goed te keuren en deze te laten verwerken, of hem alsnog af te keuren omdat het verzonden product niet overeenkomt met wat er opgegeven was in het verzoek."></utilize:translatetext>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_return_number" Text="Retournummer: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_number"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_return_customername" Text="Klantnaam: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_customer"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_return_order_nummer" Text="Ordernummer: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_order_number"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_req_date" Text="Aanvraagdatum: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_req_date"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_req_status" Text="Status: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_req_status"></utilize:label>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-md-4">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_compensation" Text="Compensatie: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_compensation"></utilize:label>
                    </div>
                </div>
                <utilize:placeholder runat="server" ID="ph_assigned_comp" Visible="false">
                    <div class="row">
                        <div class="col-md-4">
                            <b>
                                <utilize:translatelabel runat="server" ID="label_assigned_compensation" Text="Toegewezen compensatie:"></utilize:translatelabel></b>
                        </div>
                        <div class="col-md-4">
                            <b>
                                <utilize:label runat="server" ID="lbl_assigned_compensation"></utilize:label></b>
                        </div>
                    </div>
                </utilize:placeholder>
                <div class="row">
                    <div class="col-md-4">
                        <b><utilize:translatelabel runat="server" ID="label_wrong_return_amount" Text="Verkeerde retour hoeveelheden"></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:checkbox runat="server" ID="check_wrong_return_amounts" AutoPostBack="true" OnCheckedChanged="show_wrong_return_amounts"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <b>
                                    <utilize:translatelabel runat="server" ID="label_reason_description" Text="Opmerkingen: "></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-8">
                        <utilize:label runat="server" ID="lbl_reason_description"></utilize:label>
                    </div>
                </div>
            </div>
        </div>
        <utilize:repeater runat="server" ID="rpt_return_detail_lines">
            <HeaderTemplate>
                                        <div class="table-responsive">
                    <table class="table overview">
                        <thead>
                                                <tr>
                                                <th style="width: 10%">
                                    <utilize:translatelabel runat="server" ID="col_prod_code" Text="Productcode"></utilize:translatelabel></th>
                                <th style="width: 30%">
                                    <utilize:translatelabel runat="server" ID="col_prod_desc" Text="Product beschrijving"></utilize:translatelabel></th>
                                <th class="col-md-2 text-center">
                                    <utilize:translatelabel runat="server" ID="label_order_number" Text="Ordernummer"></utilize:translatelabel>
                                </th>
                                <th class="col-md-2 text-center">
                                    <utilize:translatelabel runat="server" ID="label_return_reason_single" Text="Reden"></utilize:translatelabel>
                                </th>

                                <th style="width: 5%" class="text-center">
                                    <utilize:translatelabel runat="server" ID="col_amount" Text="Aantal"></utilize:translatelabel></th>
                                <th style="width: 5%" class="text-right">
                                    <utilize:translatelabel runat="server" ID="col_row_amt" Text="Regelbedrag"></utilize:translatelabel></th>
                                <th style="width: 7.5%" class="text-right">
                                    <utilize:translatelabel runat="server" ID="col_btw_amt" Text="btw bedrag"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" ID="col_caret_down" Visible="false"><th style="width: 5%"><utilize:translateliteral runat="server" ID="lt_click_here" Text="Foto's weergeven"></utilize:translateliteral></th></utilize:placeholder>
                            </tr>
                        </thead>
                        <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <utilize:label runat="server" ID="label_prodcode" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label>
                    </td>
                    <td>
                        <utilize:label runat="server" ID="label_proddesc" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label>
                    </td>
                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "doc_nr") %></td>
                    <td class="text-center"><%# Utilize.Data.DataProcedures.GetValue("uws_rma_reason", "reason_desc_" + Me.global_cms.Language, DataBinder.Eval(Container.DataItem, "reason_code"))%></td>
                    <td class="text-center">
                        <utilize:label runat="server" ID="label_return_amt" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0)%>'></utilize:label></td>
                    <td class="text-right">
                        <utilize:label runat="server" ID="label_row_amt" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:label></td>
                    <td class="text-right">
                        <utilize:label runat="server" ID="label_btw_amt" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "vat_amt"), 2), "c")%>'></utilize:label></td>
                    <utilize:placeholder runat="server" ID="td_caret_down" Visible="false">
                        <td class="text-center"><a data-toggle="collapse" aria-expanded="false" id="link_caret" runat="server" class="image-caret"><i class="fa fa-caret-down"></i></a></td>
                    </utilize:placeholder>
                </tr>
                <utilize:placeholder runat="server" ID="ph_wrong_return_amount" Visible="false">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="4">
                            <asp:HiddenField runat="server" id="hidden_rec_id" Value='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' />
                            <div class="form-group">
                                <label class="control-label col-lg-5"><utilize:translatelabel runat="server" ID="label_returned_amount" Text="Binnengekomen hoeveelheid"></utilize:translatelabel></label>                            
                                <div class="col-lg-7">
                                    <div class="input-group custom-input">
                                        <span class="input-group-btn">
                                            <utilize:button runat="server" ID="img_min_unit" CssClass="btn btn-secondary" imageurl="~/DefaultImages/button_min.jpg" text="-" OnClientClick="decrement(this); return false;"/>
                                        </span>
                                        <utilize:textbox runat="server" ID="txt_quantity_unit" MaxLength="4" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0)%>' CssClass="form-control quantity" data-max='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0)%>' OnTextChanged="check_amount" AutoPostBack="true"></utilize:textbox>
                                        <span class="input-group-btn">
                                            <utilize:button runat="server" ID="img_plus_unit" CssClass="btn btn-secondary" imageurl="~/DefaultImages/button_plus.jpg" text="+" OnClientClick="increment(this); return false;"/>
                                        </span>
                                    </div>                                                                       
                                </div>                                
                            </div>
                        </td>
                    </tr>
                </utilize:placeholder>                
                <utilize:placeholder runat="server" ID="ph_image_overview"></utilize:placeholder>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                    </table>
                </div>
            </FooterTemplate>
        </utilize:repeater>

        <utilize:placeholder runat="server" ID="ph_alt_prod_table" Visible="false">
            <div class="row">
                <div class="col-md-12">
                    <h4>
                        <utilize:translatetitle runat="server" ID="title_alt_products" Text="De alternatieve producten"></utilize:translatetitle></h4>
                    <div class="table-responsive">
                        <table class="table overview">
                            <thead>
                                <tr>
                                    <th class="col-md-1">
                                        <utilize:translatelabel runat="server" ID="col_prod_code1" Text="Productcode"></utilize:translatelabel></th>
                                    <th class="col-md-3">
                                        <utilize:translatelabel runat="server" ID="col_prod_desc1" Text="Product beschrijving"></utilize:translatelabel></th>
                                    <th class="col-md-2 text-center">
                                        <utilize:translatelabel runat="server" ID="col_amount1" Text="Aantal"></utilize:translatelabel></th>
                                    <th class="col-md-1 text-right">
                                        <utilize:translatelabel runat="server" ID="col_row_amt1" Text="Regelbedrag"></utilize:translatelabel></th>
                                    <th class="col-md-1 text-right">
                                        <utilize:translatelabel runat="server" ID="col_btw_amt1" Text="btw bedrag"></utilize:translatelabel></th>
                                </tr>
                            </thead>
                            <tbody>
                                <utilize:repeater runat="server" ID="rpt_alt_prod">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                                            <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%></td>
                                            <td class="text-center"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "prod_qty"), 0)%></td>
                                            <td class="text-right"><%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%></td>
                                            <td class="text-right"><%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_vat"), 2), "c")%></td>
                                        </tr>
                                    </ItemTemplate>
                                </utilize:repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_choice_buttons">
            <div class="row  margin-bottom-10">
                <div class="col-md-12">
                    <div class="buttons pull-right">
                        <utilize:translatebutton runat="server" ID="button_more_information" Text="Meer informatie" CssClass="request btn btn-primary" />
                        <utilize:translatebutton runat="server" ID="button_accept_rma_request" Text="Goedkeuren" CssClass="request btn right btn-primary" />
                        <utilize:translatebutton runat="server" ID="button_reject_rma_request" Text="Afkeuren" CssClass="request right btn btn-primary" />
                    </div>
                </div>
            </div>
        </utilize:placeholder>
    </utilize:placeholder>
    <utilize:modalpanel runat="server" ID="ph_reject_field" Visible="false">
        <div class="modal-content">
            <div class="modal-body">
                <utilize:placeholder runat="server" ID="ph_reject_error" Visible="false">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="alert alert-danger">
                                <utilize:translateliteral runat="server" ID="lt_reject_error" Text="U heeft geen reden opgegeven"></utilize:translateliteral>
                            </div>
                        </div>
                    </div>
                </utilize:placeholder>
                <div class="row">
                    <div class="col-md-6">
                        <utilize:translatetext runat="server" ID="text_reject_reason" Text="Geef hieronder aan wat de reden is voor de afkeuring"></utilize:translatetext>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <utilize:textarea runat="server" ID="txt_reject_reason"></utilize:textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_reject_confirm" Text="Bevestigen" CssClass="account btn btn-primary" />
                <utilize:translatebutton runat="server" ID="button_reject_cancel" Text="Annuleren" CssClass="right btn btn-primary" />
            </div>
        </div>

    </utilize:modalpanel>


    <utilize:modalpanel runat="server" ID="ph_accept_field" Visible="false">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 ">
                        <utilize:translatetext runat="server" ID="text_incomming_confirm" Text="Weet u zeker dat u dit retour goed wilt keuren?"></utilize:translatetext>
                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_coupon_field" Visible="false">
                    <div class="row">
                        <div class="col-md-6">
                            <utilize:translatetext runat="server" ID="text_rma_coupon_info" Text="Vul onderstaand de gegevens in voor de coupon"></utilize:translatetext>
                        </div>
                    </div>
                    <utilize:placeholder runat="server" ID="ph_error_fields_not_filled" Visible="false">
                        <div class="row">
                            <div class="alert alert-danger col-md-6">
                                <utilize:translatetext runat="server" ID="text_fields_not_filled" Text="Een van de onderstaande velden is niet gevult"></utilize:translatetext>
                            </div>
                        </div>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_amount_error" Visible="false">
                        <div class="row">
                            <div class="alert alert-danger col-md-6">
                                <utilize:translatetext runat="server" ID="text_amount_error" Text="De content van het bedrag veld is niet in het correcte format"></utilize:translatetext>
                            </div>
                        </div>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_rma_setting_error" Visible="false">
                        <div class="row">
                            <div class="alert alert-danger col-md-6">
                                <utilize:translatetext runat="server" ID="text_rma_setting_error" Text="Er is geen product vastgesteld als voor de coupon in de rma settings"></utilize:translatetext>
                            </div>
                        </div>
                    </utilize:placeholder>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <utilize:translatelabel runat="server" ID="label_coupon_code" Text="Couponcode" CssClass="control-label col-md-2"></utilize:translatelabel>
                            <div class="col-md-4">
                                <utilize:textbox runat="server" ID="text_coupon_code" CssClass="form-control"></utilize:textbox>
                            </div>
                        </div>
                        <div class="form-group">
                            <utilize:translatelabel runat="server" ID="label_coupon_amount" Text="Couponbedrag" CssClass="control-label col-md-2"></utilize:translatelabel>
                            <div class="col-md-4">
                                <utilize:textbox runat="server" ID="text_coupon_amount" CssClass="form-control"></utilize:textbox>
                            </div>
                        </div>
                        <div class="form-group">
                            <utilize:translatelabel runat="server" ID="label_expire_date" Text="Vervaldatum (format: dd-mm-yyyy)" CssClass="control-label col-md-2"></utilize:translatelabel>
                            <div class="col-md-4">
                                <utilize:textboxdate type="date" runat="server" ID="textdate_expire_date" CssClass="form-control"></utilize:textboxdate>
                            </div>
                        </div>
                        <div class="form-group">
                            <utilize:translatelabel runat="server" ID="label_description" Text="Omschrijving" CssClass="control-label col-md-2"></utilize:translatelabel>
                            <div class="col-md-4">
                                <utilize:textbox runat="server" ID="text_description" CssClass="form-control"></utilize:textbox>
                            </div>
                        </div>
                    </div>
                </utilize:placeholder>

            </div>
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_accept_confirm" Text="Bevestigen" CssClass="btn btn-primary request" />
                <utilize:translatebutton runat="server" ID="button_accept_cancel" Text="Annuleren" CssClass="right btn btn-primary" />
            </div>
        </div>
    </utilize:modalpanel>

    <utilize:modalpanel runat="server" ID="ph_more_information_field" Visible="false">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 margin-bottom-10">
                        <utilize:translatetext runat="server" ID="text_more_information" Text="Stel in het onderstaande veld een bericht op met de vraag/vragen die U aan de klant hebt"></utilize:translatetext>
                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_more_information_error" Visible="false">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="alert alert-danger">
                                <utilize:translateliteral runat="server" ID="txt_more_information_error" Text="U heeft geen bericht opgegeven"></utilize:translateliteral>
                            </div>
                        </div>
                    </div>
                </utilize:placeholder>

                <div class="row">
                    <div class="col-md-6 margin-bottom-10">
                        <utilize:textarea runat="server" ID="txt_more_information_message" CssClass="form-control"></utilize:textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_more_information_send" Text="Versturen" CssClass="btn btn-primary request" />
                <utilize:translatebutton runat="server" ID="button_more_information_cancel" Text="Annuleren" CssClass="btn btn-primary right" />
            </div>
        </div>
    </utilize:modalpanel>

    <div class="buttons margin-bottom-20">
        <utilize:translatebutton runat="server" ID="button_dashboard" Text="Dashboard" CssClass="btn btn-primary" />
        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="account btn btn-primary" Visible="false" />
    </div>

    <utilize:placeholder runat="server" ID="ph_change_overview" Visible="false"></utilize:placeholder>
</div>

<utilize:label runat="server" ID="lbl_foutmelding"></utilize:label>