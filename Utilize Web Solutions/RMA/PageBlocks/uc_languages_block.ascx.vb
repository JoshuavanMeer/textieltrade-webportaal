﻿
Partial Class uc_languages_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Dim udt_languages As System.Data.DataTable

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim qsc_languages As New Utilize.Data.QuerySelectClass
            qsc_languages.select_fields = "*"
            qsc_languages.select_from = "ucm_languages"

            udt_languages = qsc_languages.execute_query()
        End If

        If ll_resume Then
            Me.rpt_links.DataSource = udt_languages
            Me.rpt_links.DataBind()
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_languages_block.css", Me)
        End If

        Me.Visible = True
    End Sub

    Protected Sub rpt_links_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_links.ItemCommand
        Me.global_cms.Language = e.CommandArgument

        Response.Redirect(Me.ResolveUrl("~/" + Me.global_cms.Language.ToLower + "/rma/home.aspx"))
    End Sub
End Class
