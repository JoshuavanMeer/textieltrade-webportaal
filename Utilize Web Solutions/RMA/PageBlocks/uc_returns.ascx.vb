﻿
Imports System.Web.UI.WebControls

Partial Class uc_returns
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    'Status of "retour"
    Private _status_id As String = ""
    Public Property status As String
        Get
            Return _status_id
        End Get
        Set(value As String)
            _status_id = value
        End Set
    End Property

    'Rejection reason of "retour"
    Private _reason_id As String = ""
    Public Property reason As String
        Get
            Return _reason_id
        End Get
        Set(value As String)
            _reason_id = value
        End Set
    End Property

    'RMA code
    Private _rmacode_id As String = ""
    Public Property rmacode As String
        Get
            Return _rmacode_id
        End Get
        Set(value As String)
            _rmacode_id = value
        End Set
    End Property


    Public Function get_returns(Optional search As Boolean = False) As System.Data.DataTable
        Dim qsc_returns As New Utilize.Data.QuerySelectClass
        qsc_returns.select_fields = "uws_rma.*,uws_customers.*,uws_rma_compensation.*, uws_history.*"
        qsc_returns.select_from = "uws_rma"
        qsc_returns.add_join("left join", "uws_customers", "uws_rma.cust_id = uws_customers.rec_id")
        qsc_returns.add_join("left join", "uws_rma_compensation", "uws_rma.comp_code = uws_rma_compensation.comp_code")
        qsc_returns.add_join("left join", "uws_history", "uws_rma.ord_nr = uws_history.crec_id")
        qsc_returns.select_where = "1=1"
        qsc_returns.select_order = "rma_code DESC"

        'als de pagina geladen wordt met een specifieke reden of status, dan deze toevoegen aan de where clausule 
        If Not String.IsNullOrEmpty(_status_id) And Me.ddl_search_status.SelectedIndex < 1 Then
            qsc_returns.select_where += " AND req_status = @status"
            qsc_returns.add_parameter("@status", _status_id)
        End If

        'If a status is selected in the search filter, then add it
        If Me.ddl_search_status.SelectedIndex > 0 Then
            qsc_returns.select_where += " AND req_status = @status"
            qsc_returns.add_parameter("@status", Me.ddl_search_status.SelectedValue)
        End If

        'als de optionele parameter true is wordt er gecheckt op de search functies
        If search Then
            'filter op rma code
            If Not String.IsNullOrEmpty(Me.txt_rma_code.Text) Then
                qsc_returns.select_where += " AND RMA_CODE like @rma_code"
                qsc_returns.add_parameter("@rma_code", Me.txt_rma_code.Text + "%")
            End If

            'check op customer id
            If Not String.IsNullOrEmpty(Me.txt_cust_nr.Text) Then
                qsc_returns.select_where += " AND uws_rma.cust_id like @cust_id"
                qsc_returns.add_parameter("@cust_id", Me.txt_cust_nr.Text + "%")
            End If

            'check op customer name
            If Not String.IsNullOrEmpty(Me.txt_cust_name.Text) Then

                Dim fullName() As String = Split(Me.txt_cust_name.Text, " ")
                Dim firstName As String = ""
                Dim infix As String = ""
                Dim lastName As String = ""

                For i As Integer = 0 To fullName.Length - 1
                    If i = 0 Then
                        firstName = fullName(i)
                    End If
                    If i >= 1 Then
                        If fullName.Length > 2 Then
                            If Not i = fullName.Length - 1 Then
                                If i = 1 Then
                                    infix = fullName(i)
                                Else
                                    infix += " " + fullName(i)
                                End If
                            Else
                                lastName = fullName(i)
                            End If
                        Else
                            lastName = fullName(i)
                        End If
                    End If
                Next

                qsc_returns.select_where += " AND uws_rma.cust_id in (select rec_id from uws_customers where fst_name like @fst_name and infix like @infix and lst_name like @lst_name)"
                qsc_returns.add_parameter("@fst_name", firstName + "%")
                qsc_returns.add_parameter("@infix", infix + "%")
                qsc_returns.add_parameter("@lst_name", lastName + "%")
            End If

            'check op factuurnummer
            If Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
                qsc_returns.select_where += " AND rma_code in (select hdr_id from uws_rma_line where doc_nr = @doc_nr) OR ord_nr in (select crec_id from uws_history where doc_nr = @doc_nr)"
                qsc_returns.add_parameter("@doc_nr", Me.txt_invoice_nr.Text)
            End If

            'check op ordernummer
            If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Then
                qsc_returns.select_where += " AND uws_rma.ord_nr like @ord_nr OR uws_rma.rma_code in (select hdr_id from uws_rma_line where ord_nr like @ord_nr)"
                qsc_returns.add_parameter("@ord_nr", "%" + Me.txt_order_code.Text + "%")
            End If

            'check op datum
            If Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                Dim req_date As String = ""

                'bouw de string voor de search op de request date hier op
                If String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                    req_date += "%-"
                Else
                    req_date += Me.txt_request_year.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_month.Text) Then
                    req_date += "%-"
                Else
                    req_date += "%" + Me.txt_request_month.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_day.Text) Then
                    req_date += "%"
                Else
                    req_date += "%" + Me.txt_request_day.Text + "%"
                End If

                qsc_returns.select_where += " AND CONVERT(VARCHAR, req_date, 120) like @req_date"
                qsc_returns.add_parameter("@req_date", req_date)
            End If
        End If

        Dim dt_returns As New System.Data.DataTable
        dt_returns = qsc_returns.execute_query()

        'vertaal de status naar de juiste taal
        For Each row In dt_returns.Rows
            dim lc_req_status as string = row.item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "Verzoek"
                    row.item("req_status") = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "Afgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                Case "Afgerond"
                    row.item("req_status") = global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                Case "In behandeling"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select
        Next

        Return dt_returns
    End Function

#Region "Page subs"

    Protected Sub pageInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If

        Me.rpt_returns.next_page_text = global_trans.translate_label("LABEL_LIST_NEXT", "Volgende pagina", Me.global_cms.Language)
        Me.rpt_returns.previous_page_text = global_trans.translate_label("LABEL_LIST_PREVIOUS", "Vorige pagina", Me.global_cms.Language)

        'als er een page parameter is ga naar de rma met de matchende rma code
        If Not _rmacode_id = "" Then
            set_return_details(_rmacode_id)
        End If

        Me.set_style_sheet("uc_returns.css", Me)

        Dim dt_returns As New System.Data.DataTable
        dt_returns = Me.get_returns()

        'check of er retouren aanwezig zijn
        If dt_returns.Rows.Count() > 0 Then
            rpt_returns.DataSource = dt_returns
            rpt_returns.DataBind()
        Else
            Me.ph_no_returns.Visible = True
            Me.ph_returns.Visible = False
        End If

        Me.button_dashboard.OnClientClick = "document.location='home.aspx'; return false"
        set_status_styling()

        'Set status
        Dim qsc_status As New Utilize.Data.QuerySelectClass
        qsc_status.select_fields = "distinct req_status"
        qsc_status.select_from = "uws_rma"

        Dim dt_status As New System.Data.DataTable
        dt_status = qsc_status.execute_query()

        'Insert status selection
        Dim top_row_default_comp As System.Data.DataRow = dt_status.NewRow()
        top_row_default_comp("req_status") = global_trans.translate_label("label_status_select", "-selecteer een status-", Me.global_ws.Language)
        dt_status.Rows.InsertAt(top_row_default_comp, 0)

        Me.ddl_search_status.DataSource = dt_status
        Me.ddl_search_status.DataTextField = "req_status"
        Me.ddl_search_status.DataValueField = "req_status"
        Me.ddl_search_status.DataBind()

        'Set reasons
        Dim qsc_reasons As New Utilize.Data.QuerySelectClass
        qsc_reasons.select_fields = "distinct reason_desc_" + Me.global_ws.Language
        qsc_reasons.select_from = "uws_rma_reason"

        Dim dt_reason As New System.Data.DataTable
        dt_reason = qsc_reasons.execute_query()

        'Insert reason selection
        Dim top_row_default As System.Data.DataRow = dt_reason.NewRow()
        top_row_default("reason_desc_" + Me.global_ws.Language) = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language)
        dt_reason.Rows.InsertAt(top_row_default, 0)

    End Sub

    Private Sub set_status_styling()
        'geef de styling van de status aan de elementen aan de hand van wat voor iet het is
        For Each item In Me.rpt_returns.Items
            Dim cb_status As System.Web.UI.WebControls.Label
            cb_status = item.findcontrol("row_status")

            Select Case cb_status.Text
                Case global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-default")
                Case global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-danger")
                Case global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-primary")
                Case global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-success")
                Case global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-warning")
                Case Else
                    Exit Select
            End Select
        Next
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_width", "$(document).ready(function () {set_status_width();});", True)
    End Sub

    Private Sub set_return_details(rma_code As String)

        Me.ph_assigned_comp.Visible = False

        'get het retour wat het meegegeven rma code heeft
        Dim qsc_return_details As New Utilize.Data.QuerySelectClass
        qsc_return_details.select_fields = "uws_rma.*,uws_history.doc_nr,uws_customers.bus_name"
        qsc_return_details.select_from = "uws_rma, uws_history,uws_customers"
        qsc_return_details.select_where = "rma_code = @rma_code and uws_rma.cust_id = uws_customers.rec_id "
        qsc_return_details.add_parameter("@rma_code", rma_code)
        qsc_return_details.select_order = "rma_code DESC"
        Dim dt_return_details As New System.Data.DataTable
        dt_return_details = qsc_return_details.execute_query()

        If dt_return_details.Rows.Count > 0 Then

            Me.ph_return_detail.Visible = True
            Me.ph_returns.Visible = False
            Me.button_close_details.Visible = True
            Me.ph_reject_reason.Visible = False

            'get de compensatie omschrijving van de compensatie die aan het retour gekoppeld is
            Dim qsc_comp_text As New Utilize.Data.QuerySelectClass
            qsc_comp_text.select_fields = "*"
            qsc_comp_text.select_from = "UWS_RMA_COMPENSATION"
            qsc_comp_text.select_where = "comp_code = @comp_code"
            qsc_comp_text.add_parameter("@comp_code", dt_return_details.Rows(0).Item("comp_code"))
            Dim dt_comp_text As New System.Data.DataTable
            dt_comp_text = qsc_comp_text.execute_query()

            Dim lc_comp_text = dt_comp_text.Rows(0).Item("comp_desc_" + Me.global_ws.Language)

            'set de label waardes naar de informatie uit de database
            Me.lbl_return_number.Text = dt_return_details.Rows(0).Item("rma_code")
            Me.lbl_return_customer.Text = dt_return_details.Rows(0).Item("bus_name")
            Me.lbl_return_order_number.Text = dt_return_details.Rows(0).Item("doc_nr")
            Me.lbl_compensation.Text = lc_comp_text
            Me.lbl_req_date.Text = dt_return_details.Rows(0).Item("req_date")

            Dim lc_req_status As String = dt_return_details.Rows(0).Item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "Verzoek"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "Afgekeurd"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                Case "Afgerond"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                Case "In behandeling"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select

            Me.lbl_reason_description.Text = dt_return_details.Rows(0).Item("reason_comm")

            'check of de status afgewezen is
            If dt_return_details.Rows(0).Item("req_status") = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language) Then
                Me.ph_reject_reason.Visible = True
                Me.lbl_reject_reason.Text = dt_return_details.Rows(0).Item("reject_reason")
            End If

            'check of de compensatie een alternatief product is
            If dt_comp_text.Rows(0).Item("comp_type") = "1" Then
                Me.ph_alt_prod_table.Visible = True
                Dim qsc_alt_prods As New Utilize.Data.QuerySelectClass
                qsc_alt_prods.select_fields = "*"
                qsc_alt_prods.select_from = "uws_rma_alt_prod"
                qsc_alt_prods.select_where = "rma_code = @rma_code"
                qsc_alt_prods.add_parameter("@rma_code", rma_code)
                Dim dt_alt_prods As System.Data.DataTable = qsc_alt_prods.execute_query()

                Me.rpt_alt_prod.DataSource = dt_alt_prods
                Me.rpt_alt_prod.DataBind()
            Else
                Me.ph_alt_prod_table.Visible = False
            End If

            'check of er een compensatie toegewezen is door een medewerker
            If Not dt_return_details.Rows(0).Item("asgn_comp_code") = "" Then
                Me.ph_assigned_comp.Visible = True

                Dim qsc_assigned As New Utilize.Data.QuerySelectClass
                qsc_assigned.select_fields = "comp_desc_" + Me.global_cms.Language
                qsc_assigned.select_from = "uws_rma_compensation"
                qsc_assigned.select_where = "comp_code = @asgn_comp_code"
                qsc_assigned.add_parameter("@asgn_comp_code", dt_return_details.Rows(0).Item("asgn_comp_code"))

                Dim dt_asgn_compensation As System.Data.DataTable
                dt_asgn_compensation = qsc_assigned.execute_query()

                Me.lbl_assigned_compensation.Text = dt_asgn_compensation.Rows(0).Item("comp_desc_" + Me.global_cms.Language)
            End If

            'get de bij behorende detail gegevens van het retour
            Dim qsc_return_lines As New Utilize.Data.QuerySelectClass
            qsc_return_lines.select_fields = "*"
            qsc_return_lines.select_from = "uws_rma_line"
            qsc_return_lines.select_where = "hdr_id = @hdr_id"
            qsc_return_lines.add_parameter("@hdr_id", rma_code)
            Dim dt_return_lines As New System.Data.DataTable
            dt_return_lines = qsc_return_lines.execute_query()

            Me.rpt_return_detail.DataSource = dt_return_lines
            Me.rpt_return_detail.DataBind()

            Me.uc_change_overview.set_property("rma_code", rma_code)
            Me.ph_change_overview.Visible = True
        Else
            Me.ph_return_detail.Visible = False
            Me.ph_returns.Visible = False
            Me.ph_wrong_page_param.Visible = True
            Me.button_dashboard.Visible = False
            Me.button_close_details.Visible = False
            Me.ph_reject_reason.Visible = False
        End If
    End Sub

    Protected Sub rpt_returns_PreRender(sender As Object, e As EventArgs) Handles rpt_returns.PreRender
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "clickRow", "$(document).ready(function () {clickRow();});", True)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Me.set_status_styling()

            'code die wordt aangeroepen als iemand op een row klikt in het overzicht, dit wordt met javascript aangeroepen
            Me.set_page_parameter("page", rpt_returns.page_nr.ToString())
            Dim eventTarget As String
            Dim eventArgument As String

            If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                eventTarget = String.Empty
            Else
                eventTarget = Me.Request("__EVENTTARGET")
            End If
            If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                eventArgument = String.Empty
            Else
                eventArgument = Me.Request("__EVENTARGUMENT")
            End If

            If eventTarget = "CallReturn" Then
                Dim rma_code = eventArgument
                set_return_details(rma_code)
            End If
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_data_targets" + Me.UniqueID, "$(document).ready(function () {set_caret_links();});", True)
    End Sub

#End Region

    Protected Sub button_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_close_details.Click
        'sluit de retour detail pagina
        Me.ph_returns.Visible = True
        Me.ph_return_detail.Visible = False
        Me.button_close_details.Visible = False
        Me.ph_change_overview.Visible = False

        Dim dt_returns As New System.Data.DataTable
        'dit zorgt er voor dat als er een search gedaan was voordat het de details geopend waren weer terug keert
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If

        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_search_clicked(sender As Object, e As System.EventArgs) Handles button_search.Click

        'open de search
        Me.ph_search.Visible = True
        Me.button_search.Visible = False
        Me.button_filter_remove.Visible = False
        Me.button_close_search.Visible = True

        Me.ddl_search_status.SelectedIndex = 0

        'Check if reason or status is pre-filled. If so, then set dropdown to correct one
        If Not String.IsNullOrEmpty(status) Then
            Me.ddl_search_status.SelectedValue = status
        End If
    End Sub

    Protected Sub button_search_close_clicked(sender As Object, e As System.EventArgs) Handles button_close_search.Click
        'sluit de search box
        Me.ph_search.Visible = False
        Me.button_search.Visible = True
        Me.button_close_search.Visible = False


        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()
        Me.rpt_returns.DataSource = dt_returns
        Me.rpt_returns.DataBind()
        set_status_styling()

        If Me.ph_no_found.Visible Then
            Me.ph_no_found.Visible = False
            Me.ph_return_table.Visible = True
        End If

        'reset de search fields zodat ze weer leeg zijn
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""
        Me.button_filter_remove.Visible = False
    End Sub

    Protected Sub button_filter_clicked(sender As Object, e As System.EventArgs) Handles button_filter.Click
        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        'reset de pagina zodat alles weer zichtbaar is
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            'als het no found zichtbaar is dan wordt deze weer ontzichtbaar gemaakt en de normale weergave wordt weer zichtbaar
            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If

    End Sub

    Protected Sub button_rma_overview_clicked(senders As Object, e As System.EventArgs) Handles button_rma_overview.Click
        'sluit de retour detail pagina
        Me.ph_returns.Visible = True
        Me.ph_wrong_page_param.Visible = False
        Me.button_dashboard.Visible = True

        Me.remove_page_parameter("rmacode")

        Dim dt_returns As New System.Data.DataTable
        'dit zorgt er voor dat als er een search gedaan was voordat het de details geopend waren weer terug keert
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If

        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_filter_remove_clicked(sender As Object, e As System.EventArgs) Handles button_filter_remove.Click
        'reset de search fields zodat ze leeg zijn
        Me.button_filter_remove.Visible = False
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""
        Me.ddl_search_status.SelectedIndex = 0

        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()

        'rebind de repeater zodat de table weer alle retouren weer geeft
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        End If
    End Sub

    Private Sub rpt_return_detail_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_return_detail.ItemCreated

        If Not IsNothing(e.Item.DataItem) Then

            If Not e.Item.DataItem("org_return_qty") = e.Item.DataItem("return_qty") Then
                Me.ph_col_org_amt.Visible = True
                e.Item.FindControl("ph_row_org_amount").Visible = True
            Else
                Me.ph_col_org_amt.Visible = False
                e.Item.FindControl("ph_row_org_amount").Visible = False
            End If

            Dim uc_image_overview As ASP.rma_usercontrols_uc_image_overview_ascx = e.Item.FindControl("uc_image_overview")
            Dim qsc_images As New Utilize.Data.QuerySelectClass
            With qsc_images
                .select_fields = "image"
                .select_from = "uws_rma_line_images"
                .select_where = "hdr_id = @hdr_id"
            End With
            qsc_images.add_parameter("@hdr_id", e.Item.DataItem("rec_id"))

                Dim dt_images As System.Data.DataTable = qsc_images.execute_query()
                uc_image_overview.dt_images = dt_images

                If dt_images.Rows.Count > 0 Then
                    If Not Me.col_caret_down.Visible Then
                        Me.col_caret_down.Visible = True
                    End If
                    Dim td_caret_down As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("td_caret_down")
                    td_caret_down.Visible = True
                End If
            End If
    End Sub
End Class
