﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_return_dashboard.ascx.vb" Inherits="RMA_PageBlocks_uc_return_dashboard" %>
<div class="uc_return_dashboard add-padding">

    <h3>Dashboard</h3>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div id="piechart" style="width: 100%; height: 400px;"></div>
            </div>
            <div class="row">
                <div id="donutchart" style="width: 100%; height: 400px;"></div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table overview">
                    <thead>
                        <tr runat="server" id="tr_row">
                            <th class="col-md-2"><utilize:translatelabel runat="server" ID="col_rma_code" Text="Retournummer"></utilize:translatelabel></th>
                            <th class="col-md-2"><utilize:translatelabel runat="server" ID="col_cust_name" Text="Klantnaam"></utilize:translatelabel></th>
                            <th class="col-md-2 text-center"><utilize:translatelabel runat="server" ID="col_req_date" Text="Aanvraag datum"></utilize:translatelabel></th>                        
                            <th class="col-md-2"><utilize:translatelabel runat="server" ID="col_status" Text="Status"></utilize:translatelabel></th>
                        </tr>
                    </thead>
                    <tbody>               
                    <utilize:repeater runat="server" id="rpt_returns">
                        <itemtemplate>
                            <tr>
                                <td><utilize:label runat="server" ID="row_rma_code" Text='<%# DataBinder.Eval(Container.DataItem, "rma_code")%>'></utilize:label></td>
                                <td><utilize:label runat="server" ID="row_cust_name" Text='<%# DataBinder.Eval(Container.DataItem, "bus_name")%>'>'></utilize:label></td>
                                <td class="text-center"><utilize:label runat="server" ID="row_req_date" Text='<%# Format(DataBinder.Eval(Container.DataItem, "req_date"), "d")%>'>'></utilize:label></td>
                                <td><utilize:label runat="server" ID="row_status" Text='<%# DataBinder.Eval(Container.DataItem, "req_status")%>'>'></utilize:label></td>
                            </tr>
                        </itemtemplate>
                    </utilize:repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>