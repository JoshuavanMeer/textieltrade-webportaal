﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_languages_block.ascx.vb" Inherits="uc_languages_block" %>

    <li class="uc_languages_blockd dropdown">        
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="lang-img" src='<%= Me.Page.ResolveUrl("~/" + Utilize.Data.DataProcedures.GetValue("ucm_languages", "lng_image", Me.global_cms.Language))%>' alt=""> <%= Utilize.Data.DataProcedures.GetValue("ucm_languages", "lng_desc_" + Me.global_cms.Language, Me.global_cms.Language)  %></a>
        <ul class="dropdown-menu">
            <utilize:repeater runat="server" ID="rpt_links">
                <ItemTemplate>
                    <li><utilize:linkbutton runat="server" ID="ib_switch_languages" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'><img class="lang-img" src='<%# Me.Page.ResolveUrl("~/" + DataBinder.Eval(Container.DataItem, "lng_image"))%>' alt='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'> <%# DataBinder.Eval(Container.DataItem, "lng_desc_" + Me.global_cms.Language) %></utilize:linkbutton></li>
                </ItemTemplate>
            </utilize:repeater>
        </ul>                
    </li>