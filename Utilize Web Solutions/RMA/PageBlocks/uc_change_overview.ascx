﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_change_overview.ascx.vb" Inherits="uc_change_overview" %>
<div class="uc_change_overview">
    <utilize:repeater runat="server" ID="rpt_changes">
        <ItemTemplate>
            <div runat="server" id="div_alert">
                <utilize:label runat="server" ID="label_who" Text='<%# DataBinder.Eval(Container.DataItem, "change_user")%>' Visible="false"></utilize:label>
                <utilize:label runat="server" ID="label_status" Text='<%# DataBinder.Eval(Container.DataItem, "change_status")%>' Visible="false"></utilize:label>
                <utilize:label runat="server" ID="label_rec_id" Text='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' Visible="false"></utilize:label>
                <utilize:label runat="server" ID="label_image_url" Text='<%# DataBinder.Eval(Container.DataItem, "user_image_url")%>' Visible="false"></utilize:label>

                <div class="row title-row">
                    <div class="col-md-12">
                        <div runat="server" id="div_title">
                            <b><utilize:literal runat="server" ID="lt_title"></utilize:literal></b>
                        </div>
                        <div runat="server" id="div_date">
                            <div runat="server" id="div_name">
                                <b><utilize:literal runat="server" ID="lt_name"></utilize:literal></b>
                            </div>
                            <utilize:literal runat="server" ID="lt_date" Text='<%# String.Format("{0:dd\/MM\/yyyy HH:mm}", DataBinder.Eval(Container.DataItem, "change_date"))%>'></utilize:literal>
                        </div>
                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_text" Visible="false">
                    <div class="row">
                        <div class="col-md-9">
                            <utilize:literal runat="server" ID="lt_text" Text='<%# DataBinder.Eval(Container.DataItem, "change_text")%>'></utilize:literal>
                        </div>
                    </div>
                </utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_image" Visible="false">
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:literal runat="server" ID="lt_image"></utilize:literal>
                        </div>
                    </div>
                </utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_comp" Visible="false">
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:literal runat="server" ID="lt_comp"></utilize:literal>
                        </div>
                    </div>
                </utilize:placeholder>

            </div>
        </ItemTemplate>
    </utilize:repeater>
</div>
