﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_return_login.ascx.vb" Inherits="RMA_PageBlocks_uc_return_login" %>
<div class="uc_return_login">
    <div class="login container" id="login_container">
        <div class="panel panel-default">
			<div class="panel-body">
                <div class="login-logo form-group text-center">
                    <img class="img-responsive" src='<%= Me.ResolveCustomUrl("~/") + Me.global_rma.get_rma_setting("rma_logo")%>' />
                </div>
                <div class="login-title form-group text-center">
                    <label><utilize:translatetitle runat="server" ID="title_rma_login" Text="Retouren login"></utilize:translatetitle></label>
                </div>
                <div class="login-controls form">
                    <div class="form-group">
                        <utilize:textbox ID="txt_logon_name" runat="server" CssClass="txt_name form-control"></utilize:textbox>
                    </div>
                    <div class="form-group">
                        <utilize:textbox ID="txt_logon_password" TextMode="Password" runat="server" CssClass="txt_password form-control"></utilize:textbox>
                    </div>
                    <div class="form-group">
                        <utilize:translatebutton ID="button_login" UseSubmitBehavior="true" CssClass="btn btn-default btn-primary pull-right" Text="Inloggen" runat="server" />
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>