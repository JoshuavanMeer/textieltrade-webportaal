﻿
Partial Class RMA_PageBlocks_uc_return_lines_top
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Private Sub RMA_PageBlocks_uc_return_lines_top_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_return_lines_top.css", Me)

        If Not Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If

        Dim lt_reason As String = Me.get_page_parameter("reason")

        Dim qsc_top As New Utilize.Data.QuerySelectClass
        qsc_top.select_fields = "top 25 uws_rma_line.prod_code, uws_rma_line.prod_desc, uws_rma_line.reason_code, sum(uws_rma_line.return_qty) as return_qty "
        qsc_top.select_from = "uws_rma_line"

        If Not lt_reason = "" Then
            qsc_top.add_join("left join", "uws_rma_reason", "uws_rma_line.reason_code = uws_rma_reason.reason_code")
            qsc_top.select_where = "reason_desc_" + Me.global_cms.Language + " = @reason "

            qsc_top.add_parameter("reason", lt_reason)
            qsc_top.add_parameter("reason_desc", "reason_desc_" + Me.global_cms.Language)
            qsc_top.select_where += "GROUP BY prod_code, prod_desc, uws_rma_line.reason_code "
        Else
            qsc_top.select_where = "1 = 1 GROUP BY prod_code, prod_desc, uws_rma_line.reason_code "
        End If

        qsc_top.select_order = "return_qty DESC "

        Me.global_rma.logger.log(qsc_top.select_fields)
        Me.global_rma.logger.log(qsc_top.select_from)
        Me.global_rma.logger.log(qsc_top.select_where)
        Me.global_rma.logger.log(qsc_top.select_order)

        Dim dt_top As System.Data.DataTable = qsc_top.execute_query()

        Me.rpt_return_lines.DataSource = dt_top
        Me.rpt_return_lines.DataBind()
    End Sub
End Class
