﻿
Imports System.Web.UI.WebControls

Partial Class Webshop_Account_AccountBlocks_uc_return_incoming
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Private uc_change_overview As Utilize.Web.Solutions.RMA.rma_user_control = Nothing

#Region "Page functions"
    Private Function get_returns(Optional search As Boolean = False) As System.Data.DataTable
        Dim qsc_returns As New Utilize.Data.QuerySelectClass
        With qsc_returns
            .select_fields = "uws_rma.*,uws_customers.*,uws_rma_compensation.*, uws_history.*"
            .select_from = "uws_rma"
            .select_where = "(req_status = @req_status or req_status = @req_status2)"
            .select_order = "rma_code DESC"
        End With
        qsc_returns.add_join("left join", "uws_customers", "uws_rma.cust_id = uws_customers.rec_id")
        qsc_returns.add_join("left join", "uws_rma_compensation", "uws_rma.comp_code = uws_rma_compensation.comp_code")
        qsc_returns.add_join("left join", "uws_history", "uws_rma.ord_nr = uws_history.crec_id")
        qsc_returns.add_parameter("@req_status", "Goedgekeurd")
        qsc_returns.add_parameter("@req_status2", "In behandeling na ontvangst")

        'als de optionele parameter true is wordt er gecheckt op de search functies
        If search Then
            'filter op rma code
            If Not String.IsNullOrEmpty(Me.txt_rma_code.Text) Then
                qsc_returns.select_where += " AND RMA_CODE like @rma_code"
                qsc_returns.add_parameter("@rma_code", Me.txt_rma_code.Text + "%")
            End If

            'check op customer id
            If Not String.IsNullOrEmpty(Me.txt_cust_nr.Text) Then
                qsc_returns.select_where += " AND uws_rma.cust_id like @cust_id"
                qsc_returns.add_parameter("@cust_id", Me.txt_cust_nr.Text + "%")
            End If

            'check op customer name
            If Not String.IsNullOrEmpty(Me.txt_cust_name.Text) Then

                Dim fullName() As String = Split(Me.txt_cust_name.Text, " ")
                Dim firstName As String = ""
                Dim infix As String = ""
                Dim lastName As String = ""

                For i As Integer = 0 To fullName.Length - 1
                    If i = 0 Then
                        firstName = fullName(i)
                    End If
                    If i >= 1 Then
                        If fullName.Length > 2 Then
                            If Not i = fullName.Length - 1 Then
                                If i = 1 Then
                                    infix = fullName(i)
                                Else
                                    infix += " " + fullName(i)
                                End If
                            Else
                                lastName = fullName(i)
                            End If
                        Else
                            lastName = fullName(i)
                        End If
                    End If
                Next

                qsc_returns.select_where += " AND uws_rma.cust_id in (select rec_id from uws_customers where fst_name like @fst_name and infix like @infix and lst_name like @lst_name)"
                qsc_returns.add_parameter("@fst_name", firstName + "%")
                qsc_returns.add_parameter("@infix", infix + "%")
                qsc_returns.add_parameter("@lst_name", lastName + "%")
            End If

            'check op factuurnummer
            If Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
                qsc_returns.select_where += " AND rma_code in (select hdr_id from uws_rma_line where doc_nr = @doc_nr) OR ord_nr in (select crec_id from uws_history where doc_nr = @doc_nr)"
                qsc_returns.add_parameter("@doc_nr", Me.txt_invoice_nr.Text)
            End If

            'check op ordernummer
            If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Then
                qsc_returns.select_where += " AND uws_rma.ord_nr like @ord_nr OR uws_rma.rma_code in (select hdr_id from uws_rma_line where ord_nr like @ord_nr)"
                qsc_returns.add_parameter("@ord_nr", "%" + Me.txt_order_code.Text + "%")
            End If

            'check op datum
            If Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                Dim req_date As String = ""

                'bouw de string voor de search op de request date hier op
                If String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                    req_date += "%-"
                Else
                    req_date += Me.txt_request_year.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_month.Text) Then
                    req_date += "%-"
                Else
                    req_date += "%" + Me.txt_request_month.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_day.Text) Then
                    req_date += "%"
                Else
                    req_date += "%" + Me.txt_request_day.Text + "%"
                End If

                'check of er al een optie gebruikt is of niet
                qsc_returns.select_where += " AND CONVERT(VARCHAR, req_date, 120) like @req_date"
                qsc_returns.add_parameter("@req_date", req_date)
            End If
        End If

        Dim dt_returns As New System.Data.DataTable
        dt_returns = qsc_returns.execute_query()

        For Each row In dt_returns.Rows
            Dim lc_req_status As String = row.item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select
        Next

        Return dt_returns
    End Function
#End Region

#Region "Page subs"
    Protected Sub pageinit1(sender As Object, e As System.EventArgs) Handles Me.Init

        If Not Me.global_rma.user_information.user_logged_on Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If

        If Not Me.global_rma.user_information.user_incoming Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/home.aspx", True)
        End If

        Me.set_style_sheet("uc_return_incoming.css", Me)

        ' Load the change overview user control and add it to the page in a placeholder
        uc_change_overview = LoadUserControl("~/RMA/PageBlocks/uc_change_overview.ascx")
        Me.ph_change_overview.Controls.Add(uc_change_overview)

        If Not Me.get_page_parameter("rmacode") = "" Then
            Me.set_return_details(Me.get_page_parameter("rmacode"))
        End If

        Me.rpt_returns.next_page_text = global_trans.translate_label("LABEL_LIST_NEXT", "Volgende pagina", Me.global_cms.Language)
        Me.rpt_returns.previous_page_text = global_trans.translate_label("LABEL_LIST_PREVIOUS", "Vorige pagina", Me.global_cms.Language)

        Dim dt_returns As New System.Data.DataTable
        dt_returns = Me.get_returns()

        'check of er retouren aanwezig zijn
        If dt_returns.Rows.Count() > 0 Then
            rpt_returns.DataSource = dt_returns
            rpt_returns.DataBind()
        Else
            Me.ph_no_incoming.Visible = True
            Me.ph_incoming.Visible = False
        End If
        Me.set_status_styling()

        Me.button_dashboard.OnClientClick = "document.location='home.aspx'; return false"

        'Set reasons
        Dim qsc_reasons As New Utilize.Data.QuerySelectClass
        With qsc_reasons
            .select_fields = "distinct reason_desc_" + Me.global_ws.Language
            .select_from = "uws_rma_reason"
        End With
        Dim dt_reason As System.Data.DataTable = qsc_reasons.execute_query()

        'Insert reason selection
        Dim top_row_default As System.Data.DataRow = dt_reason.NewRow()
        top_row_default("reason_desc_" + Me.global_ws.Language) = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language)
        dt_reason.Rows.InsertAt(top_row_default, 0)
    End Sub

    Protected Sub set_return_details(rma_code As String)
        Me.ph_incoming_detail.Visible = True
        Me.ph_incoming.Visible = False
        Me.button_close_details.Visible = True
        Me.ph_assigned_comp.Visible = False
        Me.ph_choice_buttons.Visible = True

        'get de informatie van het retour
        Dim qsc_return_details As New Utilize.Data.QuerySelectClass
        With qsc_return_details
            .select_fields = "uws_rma.*, uws_history.doc_nr,uws_customers.bus_name"
            .select_from = "uws_rma, uws_history,uws_customers"
            .select_where = "rma_code = @rma_code and uws_rma.cust_id = uws_customers.rec_id "
            .select_order = "rma_code DESC"
        End With
        qsc_return_details.add_parameter("@rma_code", rma_code)

        Dim dt_return_detail As System.Data.DataTable = qsc_return_details.execute_query()

        'get de retour lines
        Dim qsc_return_detail_lines As New Utilize.Data.QuerySelectClass
        With qsc_return_detail_lines
            .select_fields = "*"
            .select_from = "uws_rma_line"
            .select_where = "hdr_id = @hdr_id"
        End With
        qsc_return_detail_lines.add_parameter("@hdr_id", rma_code)

        Dim dt_rma_lines As System.Data.DataTable = qsc_return_detail_lines.execute_query()

        'get de compensatie naam die bij het retour hoort
        Dim qsc_comp_text As New Utilize.Data.QuerySelectClass
        With qsc_comp_text
            .select_fields = "*"
            .select_from = "uws_rma_compensation"
            .select_where = "comp_code = @comp_code"
        End With
        qsc_comp_text.add_parameter("@comp_code", dt_return_detail.Rows(0).Item("comp_code"))

        Dim dt_comp_text As System.Data.DataTable = qsc_comp_text.execute_query()

        'check of de compensatie een alternatief product is
        If dt_comp_text.Rows(0).Item("comp_type") = "1" Then
            Me.ph_alt_prod_table.Visible = True

            Dim qsc_alt_prods As New Utilize.Data.QuerySelectClass
            With qsc_alt_prods
                .select_fields = "*"
                .select_from = "uws_rma_alt_prod"
                .select_where = "rma_code = @rma_code"
            End With
            qsc_alt_prods.add_parameter("@rma_code", rma_code)

            Dim dt_alt_prods As System.Data.DataTable = qsc_alt_prods.execute_query()

            Me.rpt_alt_prod.DataSource = dt_alt_prods
            Me.rpt_alt_prod.DataBind()
        Else
            Me.ph_alt_prod_table.Visible = False
        End If

        'check of er een compensatie toegewezen is door een medewerker
        If Not dt_return_detail.Rows(0).Item("asgn_comp_code") = "" Then
            Me.ph_assigned_comp.Visible = True

            Dim qsc_assigned As New Utilize.Data.QuerySelectClass
            With qsc_assigned
                .select_fields = "comp_desc_" + Me.global_ws.Language
                .select_from = "uws_rma_compensation"
                .select_where = "comp_code = @asgn_comp_code"
            End With
            qsc_assigned.add_parameter("@asgn_comp_code", dt_return_detail.Rows(0).Item("asgn_comp_code"))

            Dim dt_asgn_compensation As System.Data.DataTable = qsc_assigned.execute_query()

            Me.lbl_assigned_compensation.Text = dt_asgn_compensation.Rows(0).Item("comp_desc_" + Me.global_ws.Language)
        End If

        'set de compensaties en de redenen van het retour
        Me.lbl_compensation.Text = dt_comp_text.Rows(0).Item("comp_desc_" + Me.global_ws.Language)
        Me.lbl_reason_description.Text = dt_return_detail.Rows(0).Item("reason_comm")
        Me.lbl_req_date.Text = dt_return_detail.Rows(0).Item("req_date")

        Dim lc_req_status As String = dt_return_detail.Rows(0).Item("req_status")
        Select Case lc_req_status
            Case "Goedgekeurd"
                Me.lbl_req_status.Text = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
            Case "In behandeling na ontvangst"
                Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
            Case Else
                Exit Select
        End Select

        Me.lbl_return_customer.Text = dt_return_detail.Rows(0).Item("bus_name")
        Me.lbl_return_number.Text = dt_return_detail.Rows(0).Item("rma_code")
        Me.lbl_return_order_number.Text = dt_return_detail.Rows(0).Item("doc_nr")

        Me.rpt_return_detail_lines.DataSource = dt_rma_lines
        Me.rpt_return_detail_lines.DataBind()

        Me.uc_change_overview.set_property("rma_code", rma_code)
        Me.ph_change_overview.Visible = True
    End Sub

    Protected Sub rpt_returns_PreRender(sender As Object, e As EventArgs) Handles rpt_returns.PreRender
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "clickRow", "$(document).ready(function () {clickRow();});", True)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Me.set_status_styling()

            'code die door javascript aageroepen wordt op het moment dat iemand op een row klikt
            Dim eventTarget As String
            Dim eventArgument As String

            'checks of de event elementen niet leeg zijn
            If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                eventTarget = String.Empty
            Else
                eventTarget = Me.Request("__EVENTTARGET")
            End If
            If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                eventArgument = String.Empty
            Else
                eventArgument = Me.Request("__EVENTARGUMENT")
            End If

            If eventTarget = "CallReturn" Then
                Dim rma_code = eventArgument
                set_return_details(rma_code)
            End If
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_data_targets" + Me.UniqueID, "$(document).ready(function () {set_caret_links();});", True)
    End Sub

    Private Sub set_status_styling()
        'geef de styling van de status aan de elementen aan de hand van wat voor iets het is
        For Each item In Me.rpt_returns.Items
            Dim cb_status As System.Web.UI.WebControls.Label
            cb_status = item.findcontrol("row_status")

            Select Case cb_status.Text
                Case global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-primary")
                Case global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-warning")
                Case Else
                    Exit Select
            End Select
        Next
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_width", "$(document).ready(function () {set_status_width();});", True)
    End Sub

    Public Sub show_wrong_return_amounts()
        Dim wrong_amount_checked As Boolean = Me.check_wrong_return_amounts.Checked
        For Each item As RepeaterItem In rpt_return_detail_lines.Items
            Dim ph_wrong_return_amount As PlaceHolder = item.FindControl("ph_wrong_return_amount")
            ph_wrong_return_amount.Visible = wrong_amount_checked
        Next
    End Sub

    Public Sub check_amount(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim max As Integer = sender.attributes("data-max")
        Dim current As Integer

        If Integer.TryParse(sender.text, current) Then
            If current > max Or current < 0 Then
                sender.text = max
            End If
        Else
            sender.text = max
        End If
    End Sub

#End Region

#Region "Page buttons"
    Protected Sub button_close_detail_click(sender As Object, e As System.EventArgs) Handles button_close_details.Click
        Me.ph_incoming.Visible = True
        Me.ph_incoming_detail.Visible = False
        Me.button_close_details.Visible = False
        Me.ph_change_overview.Visible = False
    End Sub

    Protected Sub button_reject_click(sender As Object, e As System.EventArgs) Handles button_reject_rma_request.Click
        Me.ph_reject_field.Visible = True
        Me.txt_reject_reason.Text = ""
        Me.ph_choice_buttons.Visible = False
    End Sub

    Protected Sub button_search_clicked(sender As Object, e As System.EventArgs) Handles button_search.Click
        'open de search
        Me.ph_search.Visible = True
        Me.button_search.Visible = False
        Me.button_filter_remove.Visible = False
        Me.button_close_search.Visible = True
    End Sub

    Protected Sub button_search_close_clicked(sender As Object, e As System.EventArgs) Handles button_close_search.Click
        'sluit de search box
        Me.ph_search.Visible = False
        Me.button_search.Visible = True
        Me.button_close_search.Visible = False

        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()
        Me.rpt_returns.DataSource = dt_returns
        Me.rpt_returns.DataBind()

        Me.set_status_styling()

        If Me.ph_no_found.Visible Then
            Me.ph_no_found.Visible = False
            Me.ph_return_table.Visible = True
        End If

        'reset de search fields zodat ze weer leeg zijn
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""
        Me.button_filter_remove.Visible = False
    End Sub

    Protected Sub button_filter_clicked(sender As Object, e As System.EventArgs) Handles button_filter.Click
        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        'reset de pagina zodat alles weer zichtbaar is
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            Me.set_status_styling()

            'als het no found zichtbaar is dan wordt deze weer ontzichtbaar gemaakt en de normale weergave wordt weer zichtbaar
            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_filter_remove_clicked(sender As Object, e As System.EventArgs) Handles button_filter_remove.Click
        'reset de search fields zodat ze leeg zijn
        Me.button_filter_remove.Visible = False
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.txt_cust_nr.Text = ""
        Me.txt_order_code.Text = ""
        Me.txt_invoice_nr.Text = ""

        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()

        'rebind de repeater zodat de table weer alle retouren weer geeft
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()

            Me.set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        End If
    End Sub

    Protected Sub button_reject_cancel_Click(sender As Object, e As EventArgs) Handles button_reject_cancel.Click
        Me.ph_choice_buttons.Visible = True
        Me.ph_reject_field.Visible = False
        Me.txt_reject_reason.Text = ""
    End Sub

    Protected Sub button_reject_confirm_click(sender As Object, e As System.EventArgs) Handles button_reject_confirm.Click
        Dim ll_testing = True

        If ll_testing Then
            ll_testing = Not Me.txt_reject_reason.Text = ""
        End If

        If Not ll_testing Then
            Me.ph_reject_error.Visible = True
        End If

        If ll_testing Then
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            'zoek de juiste rma
            ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)
            If ll_testing Then
                'set de afwijzingsreden
                ll_testing = ubo_bus_obj.field_set("uws_rma.reject_reason", Me.txt_reject_reason.Text)

                If ll_testing Then
                    'set de status
                    ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "Afgekeurd")
                End If

                If ll_testing Then
                    ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
                End If

                'maak een change aan
                If ll_testing Then
                    Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
                    ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

                    ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
                    ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
                    ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
                    ubo_change.field_set("uws_rma_changes.change_status", "afgekeurd")
                    ubo_change.field_set("uws_rma_changes.change_text", Me.txt_reject_reason.Text)

                    ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
                End If

                If ll_testing Then
                    Dim dt_returns As New System.Data.DataTable
                    dt_returns = Me.get_returns(True)

                    'check of er retouren aanwezig zijn
                    If dt_returns.Rows.Count() > 0 Then
                        rpt_returns.DataSource = dt_returns
                        rpt_returns.DataBind()

                        Me.set_status_styling()

                        Me.ph_no_incoming.Visible = False
                        Me.ph_incoming.Visible = True
                        Me.ph_incoming_detail.Visible = False
                        Me.ph_reject_field.Visible = False
                        Me.button_close_details.Visible = False
                        Me.ph_change_overview.Visible = False
                        Me.ph_choice_buttons.Visible = True
                    Else
                        Me.ph_no_incoming.Visible = True
                        Me.ph_incoming.Visible = False
                        Me.ph_incoming_detail.Visible = False
                        Me.ph_reject_field.Visible = False
                        Me.button_close_details.Visible = False
                        Me.ph_change_overview.Visible = False
                        Me.ph_choice_buttons.Visible = True
                    End If
                End If
            End If
        End If

    End Sub

    Protected Sub button_accept_rma_request_Click(sender As Object, e As EventArgs) Handles button_accept_rma_request.Click
        Me.ph_choice_buttons.Visible = False
        Me.ph_accept_field.Visible = True

        ' Haal de geselecteerde RMA op
        Dim qsc_rma As New Utilize.Data.QuerySelectClass
        With qsc_rma
            .select_fields = "*"
            .select_from = "uws_rma"
            .select_where = "rma_code = @rma_code"
        End With
        qsc_rma.add_parameter("@rma_code", Me.lbl_return_number.Text)

        Dim dt_rma As System.Data.DataTable = qsc_rma.execute_query()

        Dim qsc_comp As New Utilize.Data.QuerySelectClass
        With qsc_comp
            .select_fields = "comp_type"
            .select_from = "uws_rma_compensation"
            .select_where = "comp_code = @comp_code"
        End With
        qsc_comp.add_parameter("@comp_code", dt_rma.Rows(0).Item("comp_code"))

        Dim dt_comp As System.Data.DataTable = qsc_comp.execute_query()

        If dt_comp.Rows(0).Item("comp_type") = 4 Or dt_comp.Rows(0).Item("comp_type") = 1 Then
            Me.ph_coupon_field.Visible = True

            Dim lcGuid As String = Guid.NewGuid().ToString.Replace("-", "").Substring(0, 6)

            Me.text_coupon_code.Text = lcGuid

            If check_wrong_return_amounts.Checked Then
                Dim total As Decimal = 0.0
                For Each item As RepeaterItem In rpt_return_detail_lines.Items
                    Dim label_return_amt As Label = item.FindControl("label_return_amt")
                    Dim txt_quantity_unit As TextBox = item.FindControl("txt_quantity_unit")
                    Dim label_row_amt As Label = item.FindControl("label_row_amt")
                    Dim quantity_unit_amount As Integer

                    If Not txt_quantity_unit.Text = "" And Integer.TryParse(txt_quantity_unit.Text, quantity_unit_amount) Then
                        If quantity_unit_amount < Integer.Parse(label_return_amt.Text) Then
                            Dim addition As Decimal = Decimal.Parse(label_row_amt.Text.Substring(1))
                            addition = (addition / Integer.Parse(label_return_amt.Text)) * quantity_unit_amount
                            total += addition
                        End If
                    End If
                Next
                Me.text_coupon_amount.Text = Decimal.Round(total, 2).ToString()
            Else
                Me.text_coupon_amount.Text = Decimal.Round(dt_rma.Rows(0).Item("return_amount"), 2).ToString()
            End If

        Else
            Me.ph_coupon_field.Visible = False
        End If
    End Sub

    Protected Sub button_accept_cancel_Click(sender As Object, e As EventArgs) Handles button_accept_cancel.Click
        Me.ph_choice_buttons.Visible = True
        Me.ph_accept_field.Visible = False
    End Sub

    Protected Sub button_accept_confirm_Click(sender As Object, e As EventArgs) Handles button_accept_confirm.Click
        Dim ll_testing = True
        If check_wrong_return_amounts.Checked Then
            Dim ubo_rma As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            ubo_rma.table_locate("uws_rma", "rma_code = '" + Me.lbl_return_number.Text + "'")

            For Each item As RepeaterItem In rpt_return_detail_lines.Items
                Dim label_return_amt As Label = item.FindControl("label_return_amt")
                Dim txt_quantity_unit As TextBox = item.FindControl("txt_quantity_unit")
                Dim quantity_unit_amount As Integer

                If Not txt_quantity_unit.Text = "" And Integer.TryParse(txt_quantity_unit.Text, quantity_unit_amount) Then
                    If quantity_unit_amount < Integer.Parse(label_return_amt.Text) Then
                        Dim hidden_rec_id As HiddenField = item.FindControl("hidden_rec_id")
                        Dim found As Boolean = ubo_rma.table_locate("uws_rma_line", "rec_id = '" + hidden_rec_id.Value + "'")

                        If found Then
                            ubo_rma.field_set("uws_rma_line.return_qty", quantity_unit_amount)

                            Dim row_amount As Decimal = ubo_rma.field_get("uws_rma_line.row_amt")
                            row_amount = (row_amount / ubo_rma.field_get("uws_rma_line.org_return_qty")) * quantity_unit_amount
                            ubo_rma.field_set("uws_rma_line.row_amt", row_amount)

                            Dim vat_amount As Decimal = ubo_rma.field_get("uws_rma_line.vat_amt")
                            vat_amount = (vat_amount / ubo_rma.field_get("uws_rma_line.org_return_qty")) * quantity_unit_amount
                            ubo_rma.field_set("uws_rma_line.vat_amt", vat_amount)

                            ubo_rma.table_update(Me.global_ws.user_information.user_id)
                        End If
                    End If
                Else
                    ll_testing = False
                End If
            Next
        End If

        ' Haal de geselecteerde RMA op
        Dim qsc_rma As New Utilize.Data.QuerySelectClass
        With qsc_rma
            .select_fields = "*"
            .select_from = "uws_rma"
            .select_where = "rma_code = @rma_code"
        End With
        qsc_rma.add_parameter("@rma_code", Me.lbl_return_number.Text)

        Dim dt_rma As System.Data.DataTable = qsc_rma.execute_query()

        ' Haal de geselecteerde RMA regels op
        Dim qsc_rma_lines As New Utilize.Data.QuerySelectClass
        With qsc_rma_lines
            .select_fields = "*"
            .select_from = "uws_rma_line"
            .select_where = "hdr_id = @hdr_id"
        End With
        qsc_rma_lines.add_parameter("@hdr_id", Me.lbl_return_number.Text)

        Dim dt_rma_lines As System.Data.DataTable = qsc_rma_lines.execute_query()

        Dim qsc_comp_type As New Utilize.Data.QuerySelectClass
        With qsc_comp_type
            .select_fields = "comp_type"
            .select_from = "uws_rma_compensation"
            .select_where = "comp_code = @comp_code"
        End With

        'check of er een toegewezen compensatie aanwezig is, als dit zo is wordt deze gebruikt anders de gene die door de klant aangegeven is.
        If Not dt_rma.Rows(0).Item("asgn_comp_code") = "" Then
            qsc_comp_type.add_parameter("@comp_code", dt_rma.Rows(0).Item("asgn_comp_code"))
        Else
            qsc_comp_type.add_parameter("@comp_code", dt_rma.Rows(0).Item("comp_code"))
        End If
        Dim dt_comp_type As System.Data.DataTable = qsc_comp_type.execute_query()

        If ll_testing Then
            If dt_comp_type.Rows(0).Item("comp_type") = "1" Then
                'code voor een alternatief product als compensatie methode
                Dim ubo_orders_old As Utilize.Data.BusinessObject = Nothing

                If ll_testing Then

                    ll_testing = IsDate(Me.textdate_expire_date.Text)

                    If Not ll_testing Then
                        Me.global_rma.logger.alert("test")
                    End If
                End If

                'check of een van de velden niet leeg is
                If ll_testing Then
                    If Me.text_description.Text = "" Or Me.text_coupon_amount.Text = "" Or Me.text_coupon_code.Text = "" Then
                        ll_testing = False
                    End If

                    If Not ll_testing Then
                        Me.ph_error_fields_not_filled.Visible = True
                    Else
                        Me.ph_error_fields_not_filled.Visible = False
                    End If
                End If

                'check of het ingevulde bedrag wel een decimal is
                If ll_testing Then
                    Dim lc_test As New Decimal
                    If Not Decimal.TryParse(Me.text_coupon_amount.Text, lc_test) Then
                        ll_testing = False
                    End If

                    If Not ll_testing Then
                        Me.ph_amount_error.Visible = True
                    Else
                        Me.ph_amount_error.Visible = False
                    End If
                End If

                If ll_testing Then
                    ll_testing = Not Me.global_rma.get_rma_setting("COUPON_PRODUCT") = ""

                    If Not ll_testing Then
                        Me.ph_rma_setting_error.Visible = True
                    Else
                        Me.ph_rma_setting_error.Visible = False
                    End If
                End If

                If ll_testing Then
                    ubo_orders_old = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                    ' Initialiseer en laad het business object
                    ubo_orders_old.bus_init()
                    ubo_orders_old.bus_load()

                    ubo_orders_old.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                    ubo_orders_old.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                    ubo_orders_old.field_set("order_description", global_trans.translate_message("message_old_return", "Retour producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                    ubo_orders_old.field_set("lng_code", Me.global_ws.Language)
                    ubo_orders_old.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                    Dim order_total As Decimal = 0
                    Dim total_vat As Decimal = 0
                    For Each dr_rma_line As System.Data.DataRow In dt_rma_lines.Rows
                        ubo_orders_old.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                        ubo_orders_old.field_set("uws_order_lines.prod_code", dr_rma_line.Item("prod_code"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_desc", dr_rma_line.Item("prod_desc"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_price", (dr_rma_line.Item("row_amt")) / (dr_rma_line.Item("return_qty") * -1), "/no_api")
                        'set de quantity naar een negatief getal
                        ubo_orders_old.field_set("uws_order_lines.ord_qty", dr_rma_line.Item("return_qty") * -1, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_code", dr_rma_line.Item("vat_code"), "/no_api")
                        'set het row amount van de order naar 0                        
                        ubo_orders_old.field_set("uws_order_lines.row_amt", 0, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_amt", 0, "/no_api")
                        total_vat += dr_rma_line.Item("vat_amt")
                        order_total += dr_rma_line.Item("row_amt")
                    Next

                    'set het btw totaal naar 0
                    ubo_orders_old.field_set("order_costs", 0, "/no_api")
                    ubo_orders_old.field_set("shipping_costs", 0, "/no_api")
                    ubo_orders_old.field_set("vat_total", 0, "/no_api")
                    ubo_orders_old.field_set("order_total", 0, "/no_api")

                    ll_testing = ubo_orders_old.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_orders_old.api_execute_method("create_purchase_order", ubo_orders_old, "uws_orders")

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                'maak hier de nieuwe order aan van de alternatieve producten
                If ll_testing Then
                    'haal alle alternatieve producten op
                    Dim qsc_alt_prods As New Utilize.Data.QuerySelectClass
                    With qsc_alt_prods
                        .select_fields = "*"
                        .select_from = "uws_rma_alt_prod"
                        .select_where = "rma_code = @rma_code"
                    End With
                    qsc_alt_prods.add_parameter("@rma_code", Me.lbl_return_number.Text)

                    Dim dt_alt_prods As System.Data.DataTable = qsc_alt_prods.execute_query()

                    ll_testing = dt_alt_prods.Rows.Count > 0

                    Dim ubo_orders As Utilize.Data.BusinessObject = Nothing

                    If ll_testing Then
                        ubo_orders = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                        ' Initialiseer en laad het business object
                        ubo_orders.bus_init()
                        ubo_orders.bus_load()

                        'maak een nieuwe order aan en geef deze wat van de customer information
                        ubo_orders.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                        ubo_orders.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                        ubo_orders.field_set("order_description", global_trans.translate_message("message_alt_products", "Alternatieve producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                        ubo_orders.field_set("lng_code", Me.global_ws.Language)
                        ubo_orders.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                        'maak een orderline aan voor elke alternatief product wat besteld wordt

                        For Each row As System.Data.DataRow In dt_alt_prods.Rows
                            ubo_orders.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                            ubo_orders.field_set("uws_order_lines.prod_code", row.Item("prod_code"), "/no_api")
                            ubo_orders.field_set("uws_order_lines.prod_desc", row.Item("prod_desc"), "/no_api")
                            ubo_orders.field_set("uws_order_lines.prod_price", 0, "/no_api")
                            'set de quantity naar een negatief getal
                            ubo_orders.field_set("uws_order_lines.ord_qty", row.Item("prod_qty"), "/no_api")
                            ubo_orders.field_set("uws_order_lines.vat_code", row.Item("vat_code"), "/no_api")
                            'set het row amount van de order naar 0                        
                            ubo_orders.field_set("uws_order_lines.row_amt", 0, "/no_api")
                            ubo_orders.field_set("uws_order_lines.vat_amt", 0, "/no_api")
                        Next
                        'set het btw totaal naar 0
                        ubo_orders_old.field_set("order_costs", 0, "/no_api")
                        ubo_orders_old.field_set("shipping_costs", 0, "/no_api")
                        ubo_orders.field_set("vat_total", 0, "/no_api")
                        ubo_orders.field_set("order_total", 0, "/no_api")

                        ll_testing = ubo_orders.table_update(Me.global_ws.user_information.user_id)

                        If Not ll_testing Then
                            lbl_foutmelding.Text = ubo_orders.error_message()
                        End If
                    End If

                    If ll_testing Then
                        'maak de purchase order aan
                        ll_testing = ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")

                        If Not ll_testing Then
                            lbl_foutmelding.Text = ubo_orders.error_message()
                        End If
                    End If
                End If

                Dim ubo_coupon As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_coupons")

                'genereer een coupon
                If ll_testing Then
                    ll_testing = ubo_coupon.table_insert_row("uws_coupons", Me.global_ws.user_information.user_id)
                    If ll_testing Then
                        ubo_coupon.field_set("uws_coupons.cpn_type", "1")
                        ubo_coupon.field_set("uws_coupons.disc_type", "2")
                        ubo_coupon.field_set("uws_coupons.cpn_desc", Me.text_description.Text)
                        ubo_coupon.field_set("uws_coupons.exp_date", Me.textdate_expire_date.Text)
                        ubo_coupon.field_set("uws_coupons.prod_code", Me.global_rma.get_rma_setting("coupon_product"))
                        ubo_coupon.field_set("uws_coupons.disc_amt", Me.text_coupon_amount.Text)
                        ubo_coupon.field_set("uws_coupons.cpn_code", Me.text_coupon_code.Text)

                        ll_testing = ubo_coupon.table_update(Me.global_ws.user_information.user_id)
                    End If
                End If
            End If

            If dt_comp_type.Rows(0).Item("comp_type") = "2" Then
                'code voor een nieuw product als compensatie methode
                Dim ubo_orders_old As Utilize.Data.BusinessObject = Nothing

                If ll_testing Then
                    ubo_orders_old = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                    ' Initialiseer en laad het business object
                    ubo_orders_old.bus_init()
                    ubo_orders_old.bus_load()

                    ubo_orders_old.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                    ubo_orders_old.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                    ubo_orders_old.field_set("order_description", global_trans.translate_message("message_old_return", "Retour producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                    ubo_orders_old.field_set("lng_code", Me.global_ws.Language)
                    ubo_orders_old.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                    Dim order_total As Decimal = 0
                    Dim total_vat As Decimal = 0
                    For Each dr_rma_line As System.Data.DataRow In dt_rma_lines.Rows
                        ubo_orders_old.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                        ubo_orders_old.field_set("uws_order_lines.prod_code", dr_rma_line.Item("prod_code"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_desc", dr_rma_line.Item("prod_desc"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_price", 0, "/no_api")
                        'set de quantity naar een negatief getal
                        ubo_orders_old.field_set("uws_order_lines.ord_qty", dr_rma_line.Item("return_qty") * -1, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_code", dr_rma_line.Item("vat_code"), "/no_api")
                        'set het row amount van de order naar 0                        
                        ubo_orders_old.field_set("uws_order_lines.row_amt", 0, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_amt", 0, "/no_api")
                        total_vat += dr_rma_line.Item("vat_amt")
                        order_total += dr_rma_line.Item("row_amt")
                    Next

                    'set het btw totaal naar 0
                    ubo_orders_old.field_set("order_costs", 0, "/no_api")
                    ubo_orders_old.field_set("shipping_costs", 0, "/no_api")
                    ubo_orders_old.field_set("vat_total", 0, "/no_api")
                    ubo_orders_old.field_set("order_total", 0, "/no_api")

                    ll_testing = ubo_orders_old.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_orders_old.api_execute_method("create_purchase_order", ubo_orders_old, "uws_orders")

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                If ll_testing Then
                    Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                    ' Initialiseer en laad het business object
                    ubo_orders.bus_init()
                    ubo_orders.bus_load()

                    ubo_orders.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                    ubo_orders.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                    ubo_orders.field_set("order_description", global_trans.translate_message("message_new_return", "Nieuwe producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                    ubo_orders.field_set("lng_code", Me.global_ws.Language)
                    ubo_orders.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                    Dim order_total As Decimal = 0
                    Dim total_vat As Decimal = 0
                    For Each dr_rma_line As System.Data.DataRow In dt_rma_lines.Rows
                        ubo_orders.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                        ubo_orders.field_set("uws_order_lines.prod_code", dr_rma_line.Item("prod_code"), "/no_api")
                        ubo_orders.field_set("uws_order_lines.prod_desc", dr_rma_line.Item("prod_desc"), "/no_api")
                        ubo_orders.field_set("uws_order_lines.prod_price", 0, "/no_api")
                        'set de quantity naar een negatief getal
                        ubo_orders.field_set("uws_order_lines.ord_qty", dr_rma_line.Item("return_qty"), "/no_api")
                        ubo_orders.field_set("uws_order_lines.vat_code", dr_rma_line.Item("vat_code"), "/no_api")
                        'set het row amount van de order naar 0                        
                        ubo_orders.field_set("uws_order_lines.row_amt", 0, "/no_api")
                        ubo_orders.field_set("uws_order_lines.vat_amt", 0, "/no_api")

                        total_vat += dr_rma_line.Item("vat_amt")
                        order_total += dr_rma_line.Item("row_amt")
                    Next

                    'set het btw totaal naar 0
                    ubo_orders_old.field_set("order_costs", 0, "/no_api")
                    ubo_orders_old.field_set("shipping_costs", 0, "/no_api")
                    ubo_orders.field_set("vat_total", 0, "/no_api")
                    ubo_orders.field_set("order_total", 0, "/no_api")

                    ll_testing = ubo_orders.table_update(Me.global_ws.user_information.user_id)

                    ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")
                End If
            End If

            If dt_comp_type.Rows(0).Item("comp_type") = "3" Then
                'code voor geld terug
                Dim ubo_orders As Utilize.Data.BusinessObject = Nothing

                If ll_testing Then
                    ubo_orders = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                    ' Initialiseer en laad het business object
                    ubo_orders.bus_init()
                    ubo_orders.bus_load()

                    ubo_orders.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                    ubo_orders.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                    ubo_orders.field_set("order_description", global_trans.translate_message("message_old_return", "Retour producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                    ubo_orders.field_set("lng_code", Me.global_ws.Language)
                    ubo_orders.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                    Dim total_vat As Decimal = 0
                    Dim order_total As Decimal = 0
                    For Each dr_rma_line As System.Data.DataRow In dt_rma_lines.Rows
                        ubo_orders.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                        ubo_orders.field_set("uws_order_lines.prod_code", dr_rma_line.Item("prod_code"), "/no_api")
                        ubo_orders.field_set("uws_order_lines.prod_desc", dr_rma_line.Item("prod_desc"), "/no_api")
                        'set de quantity naar een negatief getal
                        ubo_orders.field_set("uws_order_lines.ord_qty", dr_rma_line.Item("return_qty") * -1, "/no_api")
                        ubo_orders.field_set("uws_order_lines.prod_price", (dr_rma_line.Item("row_amt")) / (dr_rma_line.Item("return_qty")), "/no_api")
                        ubo_orders.field_set("uws_order_lines.vat_code", dr_rma_line.Item("vat_code"), "/no_api")
                        'set het row amount van de order naar een negatief getal
                        ubo_orders.field_set("uws_order_lines.vat_amt", dr_rma_line.Item("vat_amt") * -1, "/no_api")
                        ubo_orders.field_set("uws_order_lines.row_amt", dr_rma_line.Item("row_amt") * -1, "/no_api")

                        order_total += dr_rma_line.Item("row_amt")
                        total_vat += dr_rma_line.Item("vat_amt")
                    Next

                    ubo_orders.field_set("order_costs", 0, "/no_api")
                    ubo_orders.field_set("shipping_costs", 0, "/no_api")
                    ubo_orders.field_set("order_total", order_total * -1, "/no_api")
                    ubo_orders.field_set("vat_total", total_vat * -1, "/no_api")

                    ll_testing = ubo_orders.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders.error_message()
                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders.error_message()
                    End If
                End If
            End If

            If dt_comp_type.Rows(0).Item("comp_type") = "4" Then
                'code voor een coupon als compensatie methode
                Dim ubo_orders_old As Utilize.Data.BusinessObject = Nothing

                'test of de datum in het correcte format is
                If ll_testing Then

                    ll_testing = IsDate(Me.textdate_expire_date.Text)

                    If Not ll_testing Then
                    End If
                End If

                'check of een van de velden niet leeg is
                If ll_testing Then
                    If Me.text_description.Text = "" Or Me.text_coupon_amount.Text = "" Or Me.text_coupon_code.Text = "" Then
                        ll_testing = False
                    End If

                    If Not ll_testing Then
                        Me.ph_error_fields_not_filled.Visible = True
                    Else
                        Me.ph_error_fields_not_filled.Visible = False
                    End If
                End If

                'check of het ingevulde bedrag wel een decimal is
                If ll_testing Then
                    Dim lc_test As New Decimal
                    If Not Decimal.TryParse(Me.text_coupon_amount.Text, lc_test) Then
                        ll_testing = False
                    End If

                    If Not ll_testing Then
                        Me.ph_amount_error.Visible = True
                    Else
                        Me.ph_amount_error.Visible = False
                    End If
                End If

                If ll_testing Then
                    ll_testing = Not Me.global_rma.get_rma_setting("COUPON_PRODUCT") = ""

                    If Not ll_testing Then
                        Me.ph_rma_setting_error.Visible = True
                    Else
                        Me.ph_rma_setting_error.Visible = False
                    End If
                End If

                If ll_testing Then
                    ubo_orders_old = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                    ' Initialiseer en laad het business object
                    ubo_orders_old.bus_init()
                    ubo_orders_old.bus_load()

                    ubo_orders_old.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)
                    ubo_orders_old.field_set("order_description", global_trans.translate_message("message_old_return", "Retour producten van RMA #", Me.global_ws.Language) + Me.lbl_return_number.Text)
                    ubo_orders_old.field_set("cust_id", dt_rma.Rows(0).Item("cust_id"))
                    ubo_orders_old.field_set("lng_code", Me.global_ws.Language)
                    ubo_orders_old.field_set("email_address", Utilize.Data.DataProcedures.GetValue("uws_customers", "email_address", dt_rma.Rows(0).Item("cust_id")))

                    Dim order_total As Decimal = 0
                    Dim total_vat As Decimal = 0
                    For Each dr_rma_line As System.Data.DataRow In dt_rma_lines.Rows
                        ubo_orders_old.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)
                        ubo_orders_old.field_set("uws_order_lines.prod_code", dr_rma_line.Item("prod_code"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_desc", dr_rma_line.Item("prod_desc"), "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.prod_price", (dr_rma_line.Item("row_amt")) / (dr_rma_line.Item("return_qty")), "/no_api")

                        'set de quantity naar een negatief getal
                        ubo_orders_old.field_set("uws_order_lines.ord_qty", dr_rma_line.Item("return_qty") * -1, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_code", dr_rma_line.Item("vat_code"), "/no_api")

                        'set het row amount van de order naar 0                        
                        ubo_orders_old.field_set("uws_order_lines.row_amt", 0, "/no_api")
                        ubo_orders_old.field_set("uws_order_lines.vat_amt", 0, "/no_api")
                        total_vat += dr_rma_line.Item("vat_amt")
                        order_total += dr_rma_line.Item("row_amt")
                    Next

                    'set het btw totaal naar 0
                    ubo_orders_old.field_set("order_costs", 0, "/no_api")
                    ubo_orders_old.field_set("shipping_costs", 0, "/no_api")
                    ubo_orders_old.field_set("vat_total", 0, "/no_api")
                    ubo_orders_old.field_set("order_total", 0, "/no_api")

                    ll_testing = ubo_orders_old.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_orders_old.api_execute_method("create_purchase_order", ubo_orders_old, "uws_orders")

                    If Not ll_testing Then
                        lbl_foutmelding.Text = ubo_orders_old.error_message()
                    End If
                End If

                Dim ubo_coupon As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_coupons")

                If ll_testing Then
                    ll_testing = ubo_coupon.table_insert_row("uws_coupons", Me.global_ws.user_information.user_id)
                    If ll_testing Then
                        ubo_coupon.field_set("uws_coupons.cpn_type", "1")
                        ubo_coupon.field_set("uws_coupons.disc_type", "2")
                        ubo_coupon.field_set("uws_coupons.cpn_desc", Me.text_description.Text)
                        ubo_coupon.field_set("uws_coupons.exp_date", Me.textdate_expire_date.Text)
                        ubo_coupon.field_set("uws_coupons.prod_code", Me.global_rma.get_rma_setting("coupon_product"))
                        ubo_coupon.field_set("uws_coupons.disc_amt", Me.text_coupon_amount.Text)
                        ubo_coupon.field_set("uws_coupons.cpn_code", Me.text_coupon_code.Text)

                        ll_testing = ubo_coupon.table_update(Me.global_ws.user_information.user_id)
                    End If
                End If
            End If
        End If

        If ll_testing Then

            'maak een business object aan voor het veranderen van de status van de rma
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")

            ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)

            If ll_testing Then
                'set de status naar afgerond
                ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "Afgerond")
                If dt_comp_type.Rows(0).Item("comp_type") = "4" Or dt_comp_type.Rows(0).Item("comp_type") = "1" Then
                    ll_testing = ubo_bus_obj.field_set("uws_rma.coupon_code", Me.text_coupon_code.Text)
                End If

                ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
            End If
        End If
        'maak een change aan
        If ll_testing Then
            Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
            ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

            ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
            ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
            ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
            ubo_change.field_set("uws_rma_changes.change_status", "afgerond")

            ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            Dim dt_returns As New System.Data.DataTable
            dt_returns = Me.get_returns(True)

            'check of er retouren aanwezig zijn
            If dt_returns.Rows.Count() > 0 Then
                rpt_returns.DataSource = dt_returns
                rpt_returns.DataBind()

                Me.set_status_styling()
                'laat het retouren overzicht weer zien
                Me.ph_no_incoming.Visible = False
                Me.ph_accept_field.Visible = False
                Me.ph_incoming.Visible = True
                Me.ph_incoming_detail.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_change_overview.Visible = False
                Me.ph_choice_buttons.Visible = True
            Else
                'laat de velden zien voor als er geen retouren meer zijn
                Me.ph_no_incoming.Visible = True
                Me.ph_accept_field.Visible = False
                Me.ph_incoming.Visible = False
                Me.ph_incoming_detail.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_change_overview.Visible = False
                Me.ph_choice_buttons.Visible = True
            End If
        End If
    End Sub

    Protected Sub button_more_information_clicked(sender As Object, e As System.EventArgs) Handles button_more_information.Click
        Me.ph_more_information_field.Visible = True
        Me.ph_choice_buttons.Visible = False
    End Sub

    Protected Sub button_more_information_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_more_information_cancel.Click
        Me.ph_choice_buttons.Visible = True
        Me.ph_more_information_field.Visible = False
    End Sub

    Protected Sub button_more_information_send_clicked(sender As Object, e As System.EventArgs) Handles button_more_information_send.Click
        Dim ll_testing = True

        If ll_testing Then
            If String.IsNullOrEmpty(Me.txt_more_information_message.Text) Then
                ll_testing = False
                Me.ph_more_information_error.Visible = True
            End If
        End If

        If ll_testing Then
            'maak een business object aan voor de rma_changes
            Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
            ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)

            'set alle data die nodig is voor een meer informatie change
            ubo_change.field_set("uws_rma_changes.change_user", "gebruiker" + Me.global_rma.user_information.name_of_user)
            ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
            ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
            ubo_change.field_set("uws_rma_changes.change_status", "meer informatie")
            ubo_change.field_set("uws_rma_changes.change_text", Me.txt_more_information_message.Text)

            ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            'maak een business object aan om de status te veranderen naar In behandeling na ontvangst
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            ll_testing = ubo_bus_obj.table_seek("uws_rma", Me.lbl_return_number.Text)
            If ll_testing Then
                ll_testing = ubo_bus_obj.field_set("uws_rma.req_status", "In behandeling na ontvangst")
            End If
            If ll_testing Then
                ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
            End If
        End If

        If ll_testing Then
            Dim dt_returns As New System.Data.DataTable
            dt_returns = Me.get_returns()

            'check of er retouren aanwezig zijn
            If dt_returns.Rows.Count() > 0 Then
                rpt_returns.DataSource = dt_returns
                rpt_returns.DataBind()

                Me.ph_no_incoming.Visible = False
                Me.ph_accept_field.Visible = False
                Me.ph_incoming.Visible = True
                Me.ph_incoming_detail.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_more_information_field.Visible = False
                Me.ph_more_information_error.Visible = False
                Me.ph_change_overview.Visible = False
            Else
                Me.ph_no_incoming.Visible = True
                Me.ph_accept_field.Visible = False
                Me.ph_incoming.Visible = False
                Me.ph_reject_field.Visible = False
                Me.ph_reject_field.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_more_information_error.Visible = False
                Me.ph_more_information_field.Visible = False
                Me.ph_change_overview.Visible = False
            End If
            Me.set_status_styling()
        End If
    End Sub

    Private Sub rpt_return_detail_lines_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_return_detail_lines.ItemCreated
        If Not IsNothing(e.Item.DataItem) Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                ' Load the image overview user control and add it to the current item in a placeholder
                Dim ph_image_overview As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_image_overview")
                Dim uc_image_overview As Utilize.Web.Solutions.RMA.rma_user_control = LoadUserControl("~/RMA/UserControls/uc_image_overview.ascx")
                uc_image_overview.ID = "uc_image_overview_" + e.Item.DataItem("rec_id")
                ph_image_overview.Controls.Add(uc_image_overview)

                Dim qsc_images As New Utilize.Data.QuerySelectClass
                With qsc_images
                    .select_fields = "image"
                    .select_from = "uws_rma_line_images"
                    .select_where = "hdr_id = @hdr_id"
                End With
                qsc_images.add_parameter("@hdr_id", e.Item.DataItem("rec_id"))

                Dim dt_images As System.Data.DataTable = qsc_images.execute_query()

                ' Load the images in the lines image overview user control
                uc_image_overview.set_property("dt_images", dt_images)

                If dt_images.Rows.Count > 0 Then
                    If Not rpt_return_detail_lines.Controls(0).FindControl("col_caret_down").Visible Then
                        rpt_return_detail_lines.Controls(0).FindControl("col_caret_down").Visible = True
                    End If
                    Dim td_caret_down As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("td_caret_down")
                    td_caret_down.Visible = True
                End If
            End If
        End If
    End Sub

#End Region
End Class