﻿
Partial Class RMA_PageBlocks_uc_navigation
    Inherits Utilize.Web.Solutions.RMA.rma_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_navigation.css", Me)

        'set de links die in het menu staan
        Me.link_account_rma_returns.NavigateUrl = "~/rma/returns.aspx"
        Me.link_account_return_acceptance.NavigateUrl = "~/rma/returnacceptance.aspx"
        Me.link_account_incomming_return.NavigateUrl = "~/rma/returnincoming.aspx"

        'set de zichtbaarheid van de elementen naar de informatie die er voor nodig is
        Me.ph_menu.Visible = Me.global_rma.user_information.user_logged_on
        Me.ph_acceptance.Visible = Me.global_rma.user_information.user_accept
        Me.ph_incoming.Visible = Me.global_rma.user_information.user_incoming


        Me.ph_signed_in.Visible = Me.global_rma.user_information.user_logged_on
        If Me.global_rma.user_information.user_logged_on Then
            Me.lbl_user_name.Text = Me.global_rma.user_information.username.Substring(0, 1) + Me.global_rma.user_information.username.Substring(1).ToLower()
        End If

        Dim lcCurrentDate As String = global_trans.translate_label("label_rma_current_date", "Huidige datum: [0] (week [1])", Me.global_ws.Language)
        Me.lt_current_date.Text = lcCurrentDate
        Me.lt_current_date.Text = Me.lt_current_date.Text.Replace("[0]", Date.Now().ToString("d"))
        Me.lt_current_date.Text = Me.lt_current_date.Text.Replace("[1]", DatePart(DateInterval.WeekOfYear, Date.Now, FirstDayOfWeek.Monday, FirstWeekOfYear.System))
    End Sub

    Protected Sub button_logout_clicked(sender As Object, e As System.EventArgs) Handles link_logout.Click
        If Me.global_rma.user_information.logout() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/rma/login.aspx", True)
        End If
    End Sub
End Class
