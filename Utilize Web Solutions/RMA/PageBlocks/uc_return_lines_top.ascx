﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_return_lines_top.ascx.vb" Inherits="RMA_PageBlocks_uc_return_lines_top" %>

<div class="uc_return_lines_top add-padding">
    <div class="table-responsive">
        <table class="table overview">
            <thead>
                <tr>
                    <th class="col-md-3">
                        <utilize:translatelabel runat="server" ID="label_prod_code" Text="Artikelnummer"></utilize:translatelabel>
                    </th>
                    <th class="col-md-3">
                        <utilize:translatelabel runat="server" ID="label_description" Text="Omschrijving"></utilize:translatelabel>
                    </th>
                    <th class="col-md-3">
                        <utilize:translatelabel runat="server" ID="label_reason_line" Text="Reden"></utilize:translatelabel>
                    </th>
                    <th class="col-md-3 text-center">
                        <utilize:translatelabel runat="server" ID="label_quantity" Text="Aantal"></utilize:translatelabel>
                    </th>
                </tr>
            </thead>
            <tbody>
                <utilize:repeater runat="server" ID="rpt_return_lines">
                    <ItemTemplate>
                        <tr>
                            <td><utilize:label runat="server" ID="lbl_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label></td>
                            <td><utilize:label runat="server" ID="lbl_description" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label></td>
                            <td><utilize:label runat="server" ID="lbl_reason_line" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_rma_reason", "reason_desc_" + Me.global_cms.Language, DataBinder.Eval(Container.DataItem, "reason_code"))%>'></utilize:label></td>
                            <td class="text-center"><utilize:label runat="server" ID="lbl_quantity" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0)%>'></utilize:label></td>
                        </tr>
                    </ItemTemplate>
                </utilize:repeater>
            </tbody>        
        </table>

</div>
