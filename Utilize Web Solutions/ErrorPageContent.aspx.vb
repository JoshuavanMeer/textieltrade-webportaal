﻿
Partial Class ErrorPageContent
    Inherits CMSPageTemplate

    Private rec_id As String = ""

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Sla de code van de template op
        Me.cms_template_code = global_cms.get_cms_setting("news_template")

        ' Zet hier de titel van de pagina
        Dim lc_website_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Dim lc_page_title As String = global_trans.translate_title("title_error_occured", "Er is een fout opgetreden", Me.global_cms.Language)

        Me.Page.Title = IIf(lc_website_title = "", "", lc_website_title + " : ") + lc_page_title
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Try
            Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
            Dim uc_control As CMS.cms_user_control = LoadUserControl("CMS/Modules/Error/uc_error.ascx")
            ph_zone.Controls.Add(uc_control)
        Catch ex As Exception
            ' In certain cases there is an exception, so don't show other error page as expected
        End Try
    End Sub
End Class
