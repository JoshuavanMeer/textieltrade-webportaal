﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AppendCMSData.aspx.vb" Inherits="TestPages_AppendCMSData" EnableViewState="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        * {
            font-size: 12px;
            font-family: Arial;
        }

        h1 {
            font-size: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <utilize:placeholder runat="server" ID="ph_content">
            <div>
                <h1>Maak altijd een backup van de doel database</h1>
                <table>
                    <tr>
                        <td>Bron database</td>
                        <td>
                            <utilize:textbox runat="server" ID="txt_source_database" Text="[Bron database]"></utilize:textbox></td>
                    </tr>
                    <tr>
                        <td>Doel database</td>
                        <td>
                            <utilize:textbox runat="server" ID="txt_target_database" Text="[Doel database]"></utilize:textbox></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <utilize:button runat="server" ID="button_generate_script" Text="Genereer script" /></td>
                    </tr>
                </table>

                <utilize:literal runat="server" ID="lt_script"></utilize:literal>
            </div>
        </utilize:placeholder>
    </form>
</body>
</html>
