﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SizeBar.aspx.vb" Inherits="TestPages_SizeBar" %>

<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_sizeview_matrix.ascx" tagname="uc_sizeview_matrix" tagprefix="uc1" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_sizeview_group.ascx" tagname="uc_sizeview_group" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--
        <script type="text/javascript">

        function myFunction(elmnt, clr) {
        elmnt.style.color = clr;
    }

    function increment(fld, e) {
        fld.value = parseFloat(fld.value) + 1;

        if (isNaN(fld.value))
            fld.value = 1
    }

    function increment(fld) {
        fld.value = parseFloat(fld.value) + 1;

        if (isNaN(fld.value))
            fld.value = 1
    }

    function show(fld) {
        fld.value = 'hoi';
    }

    function whichElement(e) {
        var targ;
        if (!e) {
            var e = window.event;
        }
        if (e.target) {
            targ = e.target;
        }
        else if (e.srcElement) {
            targ = e.srcElement;
        }
        var tname;
        tname = targ.tagName;
        alert("You clicked on a " + tname + " element.");
    }

    function changeThisText() {
        if (isNaN(document.getElementById('uc_sizeview_matrix1_tc_size_maat_17_1005258920').innerHTML))
            document.getElementById('uc_sizeview_matrix1_tc_size_maat_17_1005258920').innerHTML = 0

        document.getElementById('uc_sizeview_matrix1_tc_size_maat_17_1005258920').innerHTML = parseFloat(document.getElementById('uc_sizeview_matrix1_tc_size_maat_17_1005258920').innerHTML) + 1;
    }

    function changeThisText2() {
        if (isNaN(document.getElementById('uc_sizeview_matrix1_txt_quantity_maat_18_1005258920').value))
            document.getElementById('uc_sizeview_matrix1_txt_quantity_maat_18_1005258920').value = 0

        document.getElementById('uc_sizeview_matrix1_txt_quantity_maat_18_1005258920').value = parseFloat(document.getElementById('uc_sizeview_matrix1_txt_quantity_maat_18_1005258920').value) + 1;
    }

    function changeThisText3() {
        if (isNaN(document.getElementById('Text1').value))
            document.getElementById('Text1').value = 0

        document.getElementById('Text1').value = parseFloat(document.getElementById('Text1').value) + 1;
    }


    function changeText(obj) {

        if (isNaN(obj.innerHTML))
            obj.innerHTML = 0
    
        obj.innerHTML = parseFloat(obj.innerHTML) + 1
    }


    function changeText(obj,e) {

        if (isNaN(e.srcElement.value))
            e.srcElement.value = 0

        e.srcElement.value = parseFloat(e.srcElement.value) + 1
    }

    function increment(e) {
        e.srcElement.value = parseFloat(e.srcElement.value) + 1

        if (isNaN(e.srcElement.value))
            e.srcElement.value = 0
    }


    </script>

    --%>
        <script language="javascript" type="text/javascript">
        // <![CDATA[
            function Text1_onmouseout() {
                alert("Hello! I am an alert box!!");
            }

            function Text1_onkeyup() {
                alert("Hello! I am an alert box!!");
            }
        // ]]>
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
        <div>
            <uc2:uc_sizeview_group ID="uc_sizeview_group1" runat="server" />
        </div>
        <input id="Text1" type="text" onmouseout="return Text1_onmouseout()" onkeyup="return Text1_onkeyup()" />

        <div>
            <uc1:uc_sizeview_matrix ID="uc_sizeview_matrix1" runat="server" />
        </div>

        </form>
    </body>
</html>