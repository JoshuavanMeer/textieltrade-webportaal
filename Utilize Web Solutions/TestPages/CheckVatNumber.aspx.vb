﻿
Partial Class TestPages_CheckVatNumber
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Voorbeeld: BE123456789
            ll_testing = Webshop.ws_procedures.check_vat_number("BE", "BE0123456789") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: CY12345678X
            ll_testing = Webshop.ws_procedures.check_vat_number("CY", "CY12345678X") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: DK12345678
            ll_testing = Webshop.ws_procedures.check_vat_number("DK", "DK12345678") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: DE123456789
            ll_testing = Webshop.ws_procedures.check_vat_number("DE", "DE123456789") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: EE123456789
            ll_testing = Webshop.ws_procedures.check_vat_number("EE", "EE123456789") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: FI12345678
            ll_testing = Webshop.ws_procedures.check_vat_number("FI", "FI12345678") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: FR12345678901 
            ll_testing = Webshop.ws_procedures.check_vat_number("FR", "FR12345678901") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: FRXX345678901
            ll_testing = Webshop.ws_procedures.check_vat_number("FR", "FRXX345678901") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: FR1X345678901
            ll_testing = Webshop.ws_procedures.check_vat_number("FR", "FR1X345678901") = 1
        End If

        If ll_testing Then
            ' Voorbeeld: FRX2345678901
            ll_testing = Webshop.ws_procedures.check_vat_number("FR", "FRX2345678901") = 1
        End If

        ' Voorbeeld: EL012345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("EL", "EL012345678") = 1
        End If

        ' Voorbeeld: HU12345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("HU", "HU12345678") = 1
        End If

        ' Voorbeeld: IE1234567X 
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("IE", "IE1234567X") = 1
        End If

        ' Voorbeeld: IE1X34567X
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("IE", "IE1X34567X") = 1
        End If

        ' Voorbeeld: IT12345678901
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("IT", "IT12345678901") = 1
        End If

        ' Voorbeeld: LV12345678901
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("LV", "LV12345678901") = 1
        End If

        ' Voorbeeld: LT123456789
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("LT", "LT123456789") = 1
        End If

        ' Voorbeeld: LT123456789012
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("LT", "LT123456789012") = 1
        End If

        ' Voorbeeld: LU12345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("LU", "LU12345678") = 1
        End If

        ' Voorbeeld: MT12345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("MT", "MT12345678") = 1
        End If

        ' Voorbeeld: NL123456789B01
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("NL", "NL123456789B01") = 1
        End If

        ' Voorbeeld: PL1234567890
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("PL", "PL1234567890") = 1
        End If

        ' Voorbeeld: PT123456789
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("PT", "PT123456789") = 1
        End If

        ' Voorbeeld: SI12345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("SI", "SI12345678") = 1
        End If

        ' Voorbeeld: ESX12345678
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("ES", "ESX12345678") = 1
        End If

        ' Voorbeeld: GB123456789 
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("GB", "GB123456789") = 1
        End If

        ' Voorbeeld: GB123456789001
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("GB", "GB123456789001") = 1
        End If

        ' Voorbeeld: SE123456789001
        If ll_testing Then
            ll_testing = Webshop.ws_procedures.check_vat_number("SE", "SE123456789001") = 1
        End If

    End Sub
End Class
