﻿
Partial Class TestPages_AppendCMSData
    Inherits System.Web.UI.Page

    Private Sub button_generate_script_Click(sender As Object, e As EventArgs) Handles button_generate_script.Click
        Dim lcQuery As String = "USE " + Me.txt_target_database.Text + "<br>"
        Dim lcSourceDataBase As String = Me.txt_source_database.Text

        Dim dtTables As System.Data.DataTable = getTables()

        For Each drTable As System.Data.DataRow In dtTables.Rows
            lcQuery += "delete from " + drTable.Item("dbf_name") + "<br>"
            lcQuery += "delete from utlz_dd_keys where keys_dbf_name = '" + drTable.Item("dbf_name") + "'<br>"

            Dim dtFields As System.Data.DataTable = getFields(drTable.Item("dbf_name"))

            Dim lcInsert As String = ""
            Dim lcValues As String = ""

            For Each drField In dtFields.Rows
                lcInsert += drField.item("fld_name") + ","
                lcValues += drField.item("fld_name") + ","
            Next

            lcInsert = lcInsert.Substring(0, lcInsert.Length - 1)
            lcValues = lcValues.Substring(0, lcValues.Length - 1)

            lcQuery += "insert into " + drTable.Item("dbf_name") + " (" + lcInsert + ") select " + lcValues + " from " + lcSourceDataBase + ".dbo." + drTable.Item("dbf_name") + "<br>"
        Next

        Me.lt_script.Text = lcQuery
    End Sub

    Private Function getTables() As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "dbf_name"
        qsc_select.select_from = "utlz_dd_tables"
        qsc_select.select_where = "sol_code = 'UTLZ_CMS'"
        qsc_select.select_order = "dbf_name"

        Return qsc_select.execute_query()
    End Function

    Private Function getFields(ByVal dbfName As String) As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "fld_name"
        qsc_select.select_from = "utlz_dd_fields"
        qsc_select.select_where = "dbf_name = @dbf_name"
        qsc_select.add_parameter("dbf_name", dbfName)

        qsc_select.select_order = "fld_name"

        Return qsc_select.execute_query()
    End Function

    Private Sub TestPages_AppendCMSData_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.ph_content.Visible = Utilize.Web.UI.SessionManager.Login.FrameWorkAdminLoggedIn = True
    End Sub
End Class
