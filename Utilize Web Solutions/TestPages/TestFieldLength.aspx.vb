﻿
Partial Class TestPages_TestFieldLength
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", "", "rec_id")
        ubo_bus_obj.table_insert_row("uws_customers", "testpages_testfieldlength")
        ubo_bus_obj.field_set("bus_name", "123456789tien123456789twintig123456789dertig123456789veertig123456789vijftig123456789zestig123456789zeventig123456789tachtig")
        ubo_bus_obj.table_update("testpages_testfieldlength")
    End Sub
End Class
