﻿
Partial Class ogone_test
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private lc_querystring As String = ""

    Private dt_payment_methods As System.Data.DataTable = Nothing
    Private qc_payment_methods As New Utilize.Data.QueryCustomSelectClass
    Protected PARAMVAR As String = "http://www.soulcycle.com"

    'ogone locatie
    Protected pay_url As String = ""
    Protected pay_code As String = ""

    ' bestellinggegevens
    Protected PSPID As String = "Soulcyclecom"
    Protected orderID As String = ""
    Protected amount As Decimal = 0
    Protected currency As String = ""
    Protected SHASign As String = ""
    Protected language As String = "en_US"

    ' locaties voor terug sturen
    Protected accepturl As String = ""
    Protected exceptionurl As String = ""
    Protected declineurl As String = ""
    Protected cancelurl As String = ""
    Protected catalogurl As String = ""
    Protected homeurl As String = ""

    Protected CN As String = ""
    Protected OWNERADDRESS As String = ""
    Protected OWNERZIP As String = ""
    Protected OWNERTOWN As String = ""
    Protected OWNERCTY As String = ""
    Protected OWNERTELNO As String = ""

    ' betaal methode en betaal brand
    Protected PM As String = ""
    Protected BRAND As String = ""

    Private Sub set_order_form()
        orderID = "SC0000678032"

        CN = "Jana Mayer"
        OWNERADDRESS = "Kaiserstraße 27"
        OWNERCTY = "Germany"
        OWNERTELNO = ""
        OWNERTOWN = "Weil am Rhein"
        OWNERZIP = "79576"


        Dim ln_amount As Decimal = 29.4
        amount = Decimal.Round(ln_amount * 100)

        currency = "EUR"
        PM = "IDEAL"
        BRAND = "IDEAL"

        If PM.ToUpper() = "ONLINE" Then
            PM = ""
        End If

        If BRAND.ToUpper() = "ONLINE" Then
            BRAND = ""
        End If

        Dim pay_signature = "soulcycle@soulcycle@"
        Dim hash_object As System.Security.Cryptography.HashAlgorithm = New System.Security.Cryptography.SHA1Managed()
        Dim hash_signature As String = ""

        accepturl = "http://www.soulcycle.com/Webshop/PaymentProcess/order_confirmation.aspx?status=SUCCESS"
        exceptionurl = "http://www.soulcycle.com/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        declineurl = "http://www.soulcycle.com/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        cancelurl = "http://www.soulcycle.com/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        catalogurl = "http://www.soulcycle.com/Webshop/product_list.aspx"
        homeurl = "http://www.soulcycle.com/home.aspx"

        ' hier word de hash samengesteld
        If Not accepturl = "" Then
            hash_signature &= "ACCEPTURL=" & accepturl & pay_signature
        End If

        hash_signature &= "AMOUNT=" & amount & pay_signature

        If Not BRAND = "" Then
            hash_signature &= "BRAND=" & BRAND & pay_signature
        End If

        If Not cancelurl = "" Then
            hash_signature &= "CANCELURL=" & cancelurl & pay_signature
        End If

        If Not catalogurl = "" Then
            hash_signature &= "CATALOGURL=" & catalogurl & pay_signature
        End If

        If Not CN = "" Then
            hash_signature &= "CN=" & CN & pay_signature
        End If

        hash_signature &= "CURRENCY=" & currency & pay_signature

        If Not declineurl = "" Then
            hash_signature &= "DECLINEURL=" & declineurl & pay_signature
        End If

        If Not exceptionurl = "" Then
            hash_signature &= "EXCEPTIONURL=" & exceptionurl & pay_signature
        End If

        If Not homeurl = "" Then
            hash_signature &= "HOMEURL=" & homeurl & pay_signature
        End If

        hash_signature &= "LANGUAGE=" & language & pay_signature
        hash_signature &= "ORDERID=" & orderID & pay_signature

        If Not OWNERADDRESS = "" Then
            hash_signature &= "OWNERADDRESS=" & OWNERADDRESS & pay_signature
        End If

        If Not OWNERCTY = "" Then
            hash_signature &= "OWNERCTY=" & OWNERCTY & pay_signature
        End If

        If Not OWNERTELNO = "" Then
            hash_signature &= "OWNERTELNO=" & OWNERTELNO & pay_signature
        End If

        If Not OWNERTOWN = "" Then
            hash_signature &= "OWNERTOWN=" & OWNERTOWN & pay_signature
        End If

        If Not OWNERZIP = "" Then
            hash_signature &= "OWNERZIP=" & OWNERZIP & pay_signature
        End If

        hash_signature &= "PARAMVAR=" & PARAMVAR & pay_signature

        If Not PM = "" Then
            hash_signature &= "PM=" & PM & pay_signature
        End If

        If Not PSPID = "" Then
            hash_signature &= "PSPID=" & PSPID & pay_signature
        End If

        ' de hash string omzetten in een 40 tekens lange code
        Dim Password() As Byte = Encoding.Default.GetBytes(hash_signature)

        Dim HashPassword() As Byte = hash_object.ComputeHash(Password)
        SHASign = BitConverter.ToString(HashPassword).Replace("-", "")

        ' Me.lt_string.Text = hash_signature
        ' Me.lt_hash.Text = SHASign

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_order_form()
    End Sub
End Class
