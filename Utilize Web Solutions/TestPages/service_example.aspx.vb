﻿
Partial Class TestPages_service_example
    Inherits System.Web.UI.Page

    Private _property_list(0) As generic_service.property_info

    Public Sub set_property(ByVal property_name As String, ByVal property_value As Object)
        ' Maak een nieuwe property aan
        Dim pi_property_info As New generic_service.property_info
        pi_property_info.property_name = property_name.ToUpper()
        pi_property_info.property_value = property_value

        ' Redim de array
        If Not _property_list(0) Is Nothing Then
            ReDim Preserve _property_list(_property_list.Length)
        End If

        _property_list.SetValue(pi_property_info, _property_list.Length - 1)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.update_customer()
        'Me.update_customer_user()
    End Sub

    Private Function update_customer() As Boolean
        ' Geef aan welk object je gegevens wilt toevoegen
        Dim lc_obj_code As String = "uws_customers"

        ' Geef aan welke tabel je gegevens aan wilt toevoegen
        Dim lc_main_table As String = "uws_customers"

        ' Zet de verschillende properties 
        Me.set_property("rec_id", "0000089094") ' Deze is leeg wanneer het een nieuw gegeven betreft
        Me.set_property("bus_name", "Utilized - Voorbeeld Daan")
        Me.set_property("type_code", "1")

        ' Maak de service aan
        Dim lo_service As New generic_service


        ' Voer de actie uit, de nieuwe of meegegeven sleutelwaarde wordt geretourneerd
        Dim lc_rec_id As String = lo_service.update_data(lc_obj_code, lc_main_table, _property_list)

        Response.Write("<script>alert('" + lc_rec_id + "');</script>")

        Return True
    End Function

    Private Sub update_customer_user()
        ' Geef aan welk object je gegevens wilt toevoegen
        Dim lc_obj_code As String = "uws_customers"

        ' Geef aan welke tabel je gegevens aan wilt toevoegen
        Dim lc_main_table As String = "uws_customers"
        Dim lc_details_table As String = "uws_customers_users"

        ' Zet de verschillende properties 
        Me.set_property("fst_name", "Daan")
        Me.set_property("lst_name", "van Manen")
        Me.set_property("login_code", "daan.vanmanen@utilize.nl")
        Me.set_property("login_password", "Daan123")

        ' Maak de service aan
        Dim lo_service As New generic_service

        ' Voer de actie uit, de nieuwe of meegegeven sleutelwaarde wordt geretourneerd
        Dim lc_rec_id As String = lo_service.update_data_related(lc_obj_code, lc_main_table, "0000089094", lc_details_table, "rec_id = ''", _property_list)
    End Sub

End Class
