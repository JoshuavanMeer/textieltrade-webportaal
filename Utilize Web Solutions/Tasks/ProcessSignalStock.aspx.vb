﻿Imports System.Threading

Partial Class ProcessSignalStock
    Inherits Utilize.Web.Solutions.Base.FrontEnd.frontend_page_base

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Not Me.get_page_parameter("Language") = "" Then
            Me.global_cms.Language = Me.get_page_parameter("Language")
        Else
            If Not Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("language") = "" Then
                Me.global_cms.Language = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("language")
            End If
        End If

        ' Start Process Signal Stock thread
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_stocksignal") Then
            ProcessSignalStock(Me.global_ws.get_webshop_setting("signal_stock_remove"))
        End If

    End Sub

    Private Sub ProcessSignalStock(remove_requests_after_weeks As String)
        ' Check if automatic update is available in database
        Dim qsc_signal_stock_time As New Utilize.Data.QuerySelectClass
        qsc_signal_stock_time.select_fields = "*"
        qsc_signal_stock_time.select_from = "uws_signal_st_time"
        qsc_signal_stock_time.select_where = "abs(datediff(minute,GETDATE(),check_date)) > 14"

        ' Execute thread as long as there is a row with information in the database
        Dim dt_signal_stock_time As System.Data.DataTable = qsc_signal_stock_time.execute_query()
        'Dim url_base As String = Me.Request.Url.AbsoluteUri.Replace(Me.Request.Url.AbsolutePath, "")

        'A fix to remove language parameter from url
        Dim url_base As String = Me.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped)

        Do While dt_signal_stock_time.Rows.Count > 0

            ' Execute the webservice
            Dim losStockSignalService As New signal_stock

            losStockSignalService.ProcessSignalStock(remove_requests_after_weeks, url_base)

            losStockSignalService.Dispose()

            ' Sleep for in total of 15.5 minutes
            Dim count_to_minutes_times2 As Integer = 31
            While count_to_minutes_times2 > 0
                ' Sleep for 30 seconds
                Thread.Sleep(30000)
                count_to_minutes_times2 = count_to_minutes_times2 - 1
            End While

            'Update the data table
            dt_signal_stock_time = qsc_signal_stock_time.execute_query()
        Loop
    End Sub
End Class
