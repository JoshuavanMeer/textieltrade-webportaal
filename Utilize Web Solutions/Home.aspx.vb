﻿Imports System.Threading

Partial Class Home
    Inherits Utilize.Web.Solutions.Base.FrontEnd.frontend_page_base

    Private thread_regular_signal_stock_process As Thread

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.update_dd_fields()

        Me.delete_translations()
        Me.delete_dd_tables()
        Me.delete_dd_fields()
        Me.delete_bus_obj()
        Me.delete_menu_options()
        Me.delete_files()
        Me.delete_directories()

        If Not Me.get_page_parameter("Language") = "" Then
            Me.global_cms.Language = Me.get_page_parameter("Language")
        Else
            If Not Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("language") = "" Then
                Me.global_cms.Language = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("language")
            End If
        End If

        Dim udt_data_table As New Utilize.Data.DataTable
        udt_data_table.table_name = "ucm_structure"
        udt_data_table.select_fields = "top 1 row_ord, us_code, us_url"
        udt_data_table.select_order = "row_ord"

        udt_data_table.table_init()
        udt_data_table.add_where("and us_parent = ''")

        ' Als de gebruiker niet is ingelogd, dan willen we niet de items tonen die beschikbaar mogen zijn wanneer de gebruiker is ingelogd
        If Not Me.global_ws.user_information.user_logged_on Then
            udt_data_table.add_where("and (show_type = 0 or show_type = 2)")
        Else
            udt_data_table.add_where("and (show_type = 0 or show_type = 1)")
        End If

        ' Als de module meerdere menu's aan staat, dan moet er een filter gezet worden.
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_menu") = True Then
            Dim lc_menu_group As String = Me.global_cms.get_cms_setting("menu_group_id")

            udt_data_table.add_where("and menu_group_id = @menu_group_id")
            udt_data_table.add_where_parameter("menu_group_id", lc_menu_group)
        End If

        udt_data_table.table_load()

        If udt_data_table.data_row_count > 0 Then
            ' Als de website structuur plannings module aan staat, dan een extra filter toevoegen
            For Each dr_data_row As System.Data.DataRow In udt_data_table.data_table.Rows
                Dim lc_location As String = ""
                Dim lc_us_page_type As String = udt_data_table.field_get("us_page_type")

                Select Case lc_us_page_type
                    Case "TEXT"
                        lc_location = Me.ResolveCustomUrl("~/" + Me.ResolveCustomUrl(udt_data_table.field_get("tgt_url_" + Me.global_cms.Language)))
                    Case "DEFAULT"
                        lc_location = Me.ResolveCustomUrl("~/" + Me.ResolveCustomUrl(udt_data_table.field_get("tgt_url_" + Me.global_cms.Language)))
                    Case "LINK_EXTERNAL"
                        lc_location = udt_data_table.field_get("us_page_url")
                    Case "UWS_CATEGORIES"
                        lc_location = Me.ResolveCustomUrl("~/Webshop/product_list.aspx?cat_code=" & udt_data_table.field_get("us_prod_cat"))
                    Case "LINK_INTERNAL"
                        Dim lc_page_url As String = udt_data_table.field_get("us_page_url")

                        ' Controleer hoe de pagina gestart wordt
                        If lc_page_url.ToLower().StartsWith("http://") Then
                            ' Het is een volledige URL
                            lc_location = udt_data_table.field_get("us_page_url")
                        Else
                            ' Het is een URL zonder domeinnaam
                            lc_location = Me.ResolveCustomUrl("~/" + udt_data_table.field_get("us_page_url"))
                        End If
                    Case Else
                        Exit Select
                End Select

                Response.Status = "302 Moved Temporarily"
                Response.AddHeader("Location", lc_location)

                Exit For
            Next

            udt_data_table.data_table.AcceptChanges()
        End If
    End Sub

    Private Sub update_dd_tables()
        ' Delete functies
        Dim lo_update_command As Utilize.Data.QueryBaseClass = Nothing

        ' Verwijder de type code bij de uws_customers_users.
        ' Deze wordt niet meer gebruikt.
        lo_update_command = New Utilize.Data.QueryBaseClass
        lo_update_command.QueryString = "update utlz_dd_tables set "
        lo_update_command.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub update_dd_fields()
        ' Delete functies
        Dim lo_update_command As Utilize.Data.QueryBaseClass = Nothing

        ' Verwijder de type code bij de uws_customers_users.
        ' Deze wordt niet meer gebruikt.
        lo_update_command = New Utilize.Data.QueryBaseClass
        lo_update_command.QueryString = "update utlz_dd_fields set fld_mandatory = 0 where dbf_name = 'uws_prod_relations' and fld_name = 'prod_code' "
        lo_update_command.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub delete_translations()
        ' Delete functies
        Dim lo_delete_commando As Utilize.Data.QueryBaseClass = Nothing

        ' Verwijder de type code bij de uws_customers_users.
        ' Deze wordt niet meer gebruikt.
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from ucm_translate_msg where field_code = 'message_password_saved'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from ucm_translate_labels where field_code like 'lnk_%'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub delete_dd_tables()
        ' Delete functies
        Dim lo_delete_commando As Utilize.Data.QueryBaseClass = Nothing

        ' Verwijder de type code bij de uws_customers_users.
        ' Deze wordt niet meer gebruikt.
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_tables where dbf_name = 'UWS_MAILINGS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de order entry klanten
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_tables where dbf_name = 'UWS_ORDERENTRY_CUSTS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de order entry bestellingen
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_tables where dbf_name = 'UWS_ORDERENTRY_OFF'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de order entry offertes
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_tables where dbf_name = 'UWS_ORDERENTRY_ORDS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

    End Sub

    Private Sub delete_dd_fields()
        ' Delete functies
        Dim lo_delete_commando As Utilize.Data.QueryBaseClass = Nothing

        ' Verwijder niet gebruikte velden van kalenderlines        
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'ucm_calendar_lines' and fld_name like 'SRC_URL_%'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'ucm_calendar_lines' and fld_name like 'TGT_URL_%'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDER_ENTRY' and fld_name like 'MAIL_QUOTE_%'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDER_ENTRY' and fld_name like 'MAIL_ORDER_%'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDER_ENTRY' and fld_name like '%_SAVE_DIR'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDERENTRY_CUSTS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDERENTRY_ORDS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder vervuling Order Entry
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_ORDERENTRY_OFF'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        'Verwijder vervuiling in de API Pages
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'TEST' and dbf_name = 'UTLZ_API_CUST_PAGES'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de type code bij de uws_customers_users, Deze wordt niet meer gebruikt.
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'TYPE_CODE' and dbf_name = 'UWS_CUSTOMERS_USERS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Deze wordt niet meer gebruikt.
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where dbf_name = 'UWS_MAILINGS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder het type categorie
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'CAT_TYPE' and dbf_name = 'UWS_SETTINGS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder het type categorie
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'ART_PRIO' and dbf_name = 'UWS_PRODUCTS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder het pagina template block "Productenoverzicht op basis van merk"
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_field_values where fv_code = 'BRAND' and dbf_name = 'UCM_PAGE_TEMP_BLOCKS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande api class uit de oplossing
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_api_pages where page_class = 'menu_page'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande api class uit de oplossing
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_modules where mod_code = 'UTLZ_CM_BLOGGER'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande api class uit de oplossing
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_modules where mod_code = 'UTLZ_CM_SOCIAL_MEDIA'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande api class uit de oplossing
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_modules where mod_code = 'UTLZ_WS_MAILING'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande database veld waarde
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_field_values where fv_code = 'CONTENTSEARCH'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' Verwijder de onderstaande database veld waarde
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from ucm_page_temp_blocks where block_type = 'CONTENTSEARCH'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'US_BLOCK_COLUMN' and dbf_name = 'UCM_STRUCTURE_BLOCKS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_dd_fields where fld_name = 'return_amount' and dbf_name = 'UWS_RMA_LINE'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub delete_bus_obj()
        ' Delete functies
        Dim lo_delete_commando As Utilize.Data.QueryBaseClass = Nothing

        ' De mailings worden niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_obj where obj_code = 'UWS_MAILINGS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' De order entry customers wordne niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_obj where obj_code = 'UWS_ORDERENTRY_ORDS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' De order entry customers wordne niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_obj where obj_code = 'UWS_ORDERENTRY_USERS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' De order entry customers wordne niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_obj_lin where obj_code = 'UWS_ORDERENTRY_USERS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' De coupons used moet geen api hebben
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_api_objects where obj_code = 'UWS_COUPONS_USED'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub delete_menu_options()
        ' Delete functies
        Dim lo_delete_commando As Utilize.Data.QueryBaseClass = Nothing

        ' De mailings worden niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_menu_options where men_code = 'UWS_MAILINGS'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")

        ' De order entry customers wordne niet meer gebruikt
        lo_delete_commando = New Utilize.Data.QueryBaseClass
        lo_delete_commando.QueryString = "delete from utlz_fw_menu_options where men_code = 'UTLZ_ORDERENTRY_CUST'"
        lo_delete_commando.execute_query("home.aspx_setup_calls")
    End Sub

    Private Sub delete_files()
        ' Verwijder de product_details.css uit de styles director
        Try
            System.IO.File.Delete(Server.MapPath("~/Styles/ProductDetailsStyle.css"))
        Catch ex As Exception

        End Try

        ' Verwijder de product_details.css uit de styles director
        Try
            System.IO.File.Delete(Server.MapPath("~/Styles/ControlsTemplate.aspx"))
            System.IO.File.Delete(Server.MapPath("~/Styles/ControlsTemplate.aspx.vb"))
        Catch ex As Exception

        End Try

        ' Verwijder de gastenboek onderdelen
        Try
            System.IO.File.Delete(Server.MapPath("~/CMS/guestbook.aspx"))
            System.IO.File.Delete(Server.MapPath("~/CMS/guestbook.aspx.vb"))
        Catch ex As Exception

        End Try

        ' Verwijder de payment services
        Try
            System.IO.File.Delete(Server.MapPath("~/Webshop/PaymentProcess/Blocks/uc_payment_services.ascx"))
            System.IO.File.Delete(Server.MapPath("~/Webshop/PaymentProcess/Blocks/uc_payment_services.ascx.vb"))
            System.IO.File.Delete(Server.MapPath("~/Webshop/PaymentProcess/Blocks/uc_payment_services.css"))

        Catch ex As Exception

        End Try

        ' Verwijder de product comments bestanden, deze heeft Dave onterecht aangemaakt
        Try
            System.IO.File.Delete(Server.MapPath("~/Webshop/Modules/ProductInformation/uc_product_comment.ascx"))
            System.IO.File.Delete(Server.MapPath("~/Webshop/Modules/ProductInformation/uc_product_comment.ascx.vb"))
            System.IO.File.Delete(Server.MapPath("~/Webshop/Modules/ProductInformation/uc_product_comment.css"))

        Catch ex As Exception

        End Try

        ' Verwijder jquery.js, wij gebruiken jquerylibrary.js
        Try
            System.IO.File.Delete(Server.MapPath("~/_FWScripts/jquery.js"))
        Catch ex As Exception

        End Try

    End Sub

    Private Sub delete_directories()
        Try
            System.IO.Directory.Delete(Server.MapPath("~/Webshop/Modules/SearchBasic"), True)
            System.IO.Directory.Delete(Server.MapPath("~/Webshop/Modules/SearchAdvanced"), True)
        Catch ex As Exception

        End Try
    End Sub

End Class
