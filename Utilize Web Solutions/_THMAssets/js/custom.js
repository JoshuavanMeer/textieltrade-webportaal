/* Write here your custom javascript codes */

/* 
    Custom navbar dropdown trigger for mobile devices:
    If the screen is less wide than 991px a button is added to the menu to open and close the drop down lists
*/

function collapse_dropdown() {
    if (window.innerWidth < 991) {
        if ($('.dropdown-button').length == 0) {
            $('.dropdown').children("a").after('<div class="dropdown-button fa fa-angle-down"></div>');
        }
    }
    $('.dropdown-button').on("click", function (e) {
        if ($(this).parent().hasClass("open")) {
            $(this).parent().removeClass("open");
            $(this).removeClass("fa-angle-up").addClass("fa-angle-down");
        } else {
            $(this).parent().addClass("open");
            $(this).removeClass("fa-angle-down").addClass("fa-angle-up");
        };
    });
    if (window.innerWidth > 991) {
        $('div.dropdown-button').remove();
    };
};

function show_list_custom(who) {
    var size = $(who + " .hidden_item_size").val();
    $(who + " .uc_product_block").addClass("uc_product_row col-xs-6 col-md-12 col-sm-12");
    $(who + " .uc_product_block").removeClass("uc_product_block " + size);

    $(who + " .full-width").addClass("sm-margin-bottom-20");
    $(who + " .full-width").removeClass("full-width");

    $(who + " .image-block").addClass("col-sm-4");
    $(who + " .image-block").removeClass("product-img product-img-brd");

    $(who + " .information-block").addClass("col-sm-8");
    $(who + " .information-block").removeClass("product-description-brd margin-bottom-30");

    $(who + " .content-placeholder").addClass("content");
    $(who + " .content-placeholder").removeClass("illustration-v2 col-xs-12");

    $(who + " .item-border").addClass("list-product-description product-description-brd margin-bottom-30");
    $(who + " .containter-placeholder").addClass("container-fluid");
    $(who + " .row-placeholder").addClass("row");

    $(who + " .product_info_order").css("display", "none");

    $(who + " .list-only").css("display", "block");

}


function show_blocks_custom(who) {
    var size = $(who + " .hidden_item_size").val();
    $(who + " .uc_product_row").addClass("col-xs-6 col-sm-6 uc_product_block " + size);
    $(who + " .uc_product_row").removeClass("uc_product_row");

    $(who + " .sm-margin-bottom-20").addClass("full-width");
    $(who + " .sm-margin-bottom-20").removeClass("sm-margin-bottom-20");

    $(who + " .image-block").addClass("product-img product-img-brd");
    $(who + " .image-block").removeClass("col-sm-4");

    $(who + " .information-block").addClass("product-description-brd margin-bottom-30");
    $(who + " .information-block").removeClass("col-sm-8");

    $(who + " .content-placeholder").addClass("illustration-v2 col-xs-12");

    $(who + " .item-border").removeClass("list-product-description product-description-brd margin-bottom-30");
    $(who + " .containter-placeholder").removeClass("container-fluid");
    $(who + " .row-placeholder").removeClass("row");

    $(who + " .list-only").css("display", "none");
}

function move_category() {
    if ($(".uc_category_menu_collapse").length) {
        if (window.innerWidth >= 992) {
            if (!$(".mobile-category").children(".uc_category_menu_collapse").length) {
                $(".mobile-category").append($(".uc_category_menu_collapse"));
            }
        } else {
            if ($(".uc_header_v1").length) {
                if (!$(".header-menu").children(".uc_category_menu_collapse").length) {
                    $(".header-menu").append($(".uc_category_menu_collapse"));
                }
            } else if ($(".uc_header_v2").length) {
                if (!$(".header").children(".uc_category_menu_collapse").length) {
                    $(".header").append($(".uc_category_menu_collapse"));
                }
            } else if ($(".uc_header_v3").length) {
                if (!$(".header-menu > .container").children(".uc_category_menu_collapse").length) {
                    $(".header-menu > .container").append($(".uc_category_menu_collapse"));
                }
            } else if ($(".uc_header_v4").length) {
                if (!$(".header-menu > .menu > .container").children(".uc_category_menu_collapse").length) {
                    $(".header-menu > .menu > .container").append($(".uc_category_menu_collapse"));
                }
            }
        }
    }
}

function show_webpage() {
    $("#upd_update_panel_div").css("visibility", "visible");

}

$(window).resize(function () {
    move_category();
    collapse_dropdown();
});

window.onload = function () {
    show_webpage();
    collapse_dropdown();
};

var counter = 0
function pageLoad() {
    //move the category menu on mobile
    move_category();

    // Repeater fixes
    $(".pager_next > a").addClass('btn-u');
    $(".pager_previous > a").addClass('btn-u');
    $(".pager_numbers > a").addClass('btn-u btn-numbers');

    counter++;
    if (counter > 1) {
        show_webpage();
    }

    //code voor de uc_shopping_cart_control
    if ($(".uc_shopping_cart_control").length) {
        $("#header_shopping_cart").hover(
            function () {
                $('body').on('scroll touchmove mousewheel', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                })
            },
            function () {
                $('body').off('scroll touchmove mousewheel');
            })
    }

    //code voor de uc_product_selection
    if ($(".uc_product_selection").length) {
        $(".uc_product_selection .image-block").each(function () {
            $(this).append($(".label-first").clone().removeClass("label-first"));
        });
        $(".label-first").remove();
    }

    //code voor de uc_header_v2
    if ($(".uc_header_v2").length) {
        if (window.innerWidth > 991) {
            var logo_width = $(".navbar-brand-logo").width();
            $(".uc_menu_flyout").css("margin-left", (logo_width + 10) + "px");
        }
    }

    //fix voor de search bar die niet meer werkt na een postback in de headerv1
    jQuery('.search-button').click(function () {
        jQuery('.search-open').slideDown();
    });

    jQuery('.search-close').click(function () {
        jQuery('.search-open').slideUp();
    });

    if ($(".uc_product_list").length) {
        $(".uc_placeholder").each(function () {
            var price_actual = $(this).find(".uc_product_information .value.actual.actualdefault").text();
            $(this).find(".target-actualdefault").text(price_actual);

            if ($(this).find("div.uc_product_information div.prod_price span.original").css("display") != "none") {
                var price_original = $(this).find(".uc_product_information .value.original.originaldefault").text();
                $(this).find(".target-originaldefault").text(price_original);
            }
            var prod_code = $(this).find(".uc_product_information .prod_code > ul li.value").text();
            $(this).find(".prod_code_target").text(prod_code);
        });
    }

    if ($("#use-async").length) {
        set_async_order_buttons();
        set_shopping_cart_delete_buttons();
        setupMessages();
    }
}

$(window).resize(function () {
    if ($(".uc_header_v2").length) {
        if ($("body").width() > 960) {
            var logo_width = $(".navbar-brand-logo").width();
            $(".uc_menu_flyout").css("margin-left", (logo_width + 10) + "px");
        } else {
            $(".uc_menu_flyout").css("margin-left", "0px");
        }
    }
    handle_sticky_header();
});

// This function will be executed when the user scrolls the page.
$(window).scroll(function () {
    handle_sticky_header();
});

function handle_sticky_header() {

    var screenWidth = $(window).width();
    var header_v1 = false;
    if ($(".uc_header_v1").length) {
        header_v1 = true;
    }
    var header_v2 = false;
    if ($(".uc_header_v2").length) {
        header_v2 = true;
    }

    if ($("#scroller").length && $(window).width() > 991) {
        // Get the position of the location where the scroller starts.
        //var scroller_anchor = $(".header-height").last().height() - $(".header-menu").height();
        var scroller_anchor = $(".header-height").first().height()

        // Check if the user has scrolled and the current position is after the scroller's start location and if its not already fixed at the top 
        if ($(window).scrollTop() >= scroller_anchor && $('.header-menu').css('position') != 'fixed') {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.            
            if (header_v1) {
                $('.header-menu').css({
                    top: "0px",
                    "z-index": "1000000",
                    height: "78px"
                });
                $('.uc_menu_flyout_horizontal').css({
                    "padding-left": "0px"
                });
                $('.hide-scroll').css({
                    visibility: "hidden"
                });
                $("#floating-header").css({

                    position: "fixed",
                    top: 0,
                    width: "100%",
                    left: 0,
                    height: "auto",
                    "border-bottom": "solid 2px #eee",
                });
                $(".header-menu").addClass("container");
            } else {
                $('.header-menu').css({
                    position: "fixed",
                    top: "0px",
                    width: "100%",
                    "z-index": "1000000",
                    "border-bottom": "solid 2px #eee"
                });
                if (header_v2) {
                    $(".uc_menu_flyout").css("margin-left", "0px");
                }
                $('.hide-scroll').css({
                    visibility: "hidden"
                });
            }
        }
        else if ($(window).scrollTop() < scroller_anchor && ($('.header-menu').css('position') != 'relative' || header_v1)) {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.            
            disable_sticky_header(header_v1, header_v2);
        }
    }
    else if (screenWidth <= 991 && $('.header-menu').css('position') != 'relative' || header_v1) {
        disable_sticky_header(header_v1, header_v2);
    }

    function disable_sticky_header(header_v1, header_v2) {
        var zindex = 99;
        if ($(".uc_header_v2").length) {
            zindex = 0;
        }

        if (header_v1) {
            $(".header-menu").removeClass("container");
            $('.uc_menu_flyout_horizontal').css({
                "padding-left": "210px"
            });
            $("#floating-header").css({

                position: "",
                top: "",
                width: "100%",
                left: "",
                height: "auto",
                "border-bottom": "0px",
            });
            $('.header-menu').css({
                top: "",
                "z-index": -1,
                height: "78px"
            });
            $('.hide-scroll').css({
                visibility: "visible"
            });
        } else {
            $('.header-menu').css({
                position: "relative",
                top: "",
                "z-index": zindex,
                "border-bottom": "0px"
            });
            if (header_v2) {
                var logo_width = $(".navbar-brand-logo").width();
                $(".uc_menu_flyout").css("margin-left", (logo_width + 10) + "px");
            }
            $('.hide-scroll').css({
                visibility: "visible"
            });
        }
    }
}

//voer de show_webpage() functie uit voor elke alert die gedaan word
// dit is zodat er tijdens de alert geen wit scherm zichtbaar is
(function (proxied) {
    window.alert = function () {
        show_webpage();
        return proxied.apply(this, arguments);
    };
})(window.alert);

function set_shopping_cart_delete_buttons() {
    $(".shopping-product-li").each(function () {
        var element = $(this);

        element.find(".close").click(function () {
            var button = $(this);
            var obj = {};
            obj.recId = button.data("rec-id");
            element.remove();

            $.ajax({
                type: "POST",
                url: "/Webshop/WebshopAPI/ProductListWebmethods.aspx/RemoveFromShoppingcart",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var returnValue = r.d;
                    var productCount = returnValue.product_count;
                    var orderTotal = returnValue.orderTotal;
                    var shoppingCartHtml = returnValue.shoppingcartHtml;

                    updateShoppingCart(productCount, orderTotal, shoppingCartHtml, false);
                    set_shopping_cart_delete_buttons();

                    return r.d;
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#error_message').html(err.Message);
                    $('#error_message').fadeIn();
                    updateShoppingCart(productCount, orderTotal, shoppingCartHtml);
                    set_shopping_cart_delete_buttons();
                }
            });
        });
    });
}

function set_async_order_buttons(location) {
    var location = location ? location : $('body');
    // Get all the order buttons within the given location
    var orderControls = location.find('[data-order-target="order-prod-control"]');

    orderControls.each(function (orderControls) {
        $(this).find('[data-order-target="order-prod-button"]').click(function () {
            var qtyfields = $(this).parent().find('[data-order-target="order-prod-quantity"]');
            var orderLines = createPayload(qtyfields);
            if (orderLines) {
                orderProducts(orderLines);
                return false;
            }            
        });
    });
}

// Takes a list of $(textboxes) and converts
// it's values to an array of objects
function createPayload(fieldElemArr) {
    var payload = [];
    fieldElemArr.each(function () {
        var prodCode = $(this).data('prod-code').toString();
        var qtyFieldVal = $(this).val();
        var qty = qtyFieldVal === undefined ? 0 : parseFloat(qtyFieldVal.replace(",","."));
        var unitCode = $(this).data('unit-code') === undefined ? '' : $(this).data('unit-code');
        var prodComment = $(this).parents(".uc_order_product").find(".product_comment").first().val();
      //  var prodComment = $('#<%= this.txt_product_comment %>');
        
        if (qty > 0) {            
            payload.push({ prod_code: prodCode, ord_qty: qty, unit_code: unitCode, prod_comment: prodComment });
        }        
    });
    if (payload.length) {
        return payload;
    } else {
        return false;
    }
}

function orderProducts(orderLines, orderControls) {

    bespokeAsyncHelper.addedToCart(orderLines, orderControls);

    var obj = {};
    obj.products = orderLines;

    $.ajax({
        type: "POST",
        url: "/Webshop/WebshopAPI/ProductListWebmethods.aspx/OrderProduct",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var returnValue = r.d;
            var productCount = returnValue.product_count;
            var orderTotal = returnValue.orderTotal;
            var orderErrors = r.errorMessages;            

            var message = returnValue.order_message;

            var shoppingCartHtml = returnValue.shoppingcartHtml;


            updateShoppingCart(productCount, orderTotal, shoppingCartHtml);

            if (returnValue.errorMessages) {
                updateErrorMessage(returnValue.errorMessages);
            }

            if (returnValue.totalProductsAdded > 0) {
                updateOrderMessage(message);
            }

            set_shopping_cart_delete_buttons();           

            return r.d;
        },
        error: function (xhr, status, error, errors) {
            var err = eval("(" + xhr.responseText + ")");
            updateErrorMessage(err.Message);
        }
    });
}

function updateShoppingCart(productCount, orderTotal, shoppingCartHtml, updateShoppingCartHtml) {
    var updateHtml = updateShoppingCartHtml ? updateShoppingCartHtml : true;
    $('.cart_total_amount').text(orderTotal);
    $('.shopping_cart_badge').text(productCount);
    $('.product_count').text(productCount);
    var additionElement = $("#shopping_cart_content_ul").parents(".content_li");

    // Set the shopping_cart_content
    if (additionElement.length && updateHtml) {
        additionElement.empty();
        additionElement.append(shoppingCartHtml);
        $("#shopping_cart_content_ul").mCustomScrollbar();
    }
}

// Wraps notifications so they can be handled better
function setupMessages() {
    $("#error_message").wrapAll("<div id='async-notifications' />");
    $('#async-notifications').append($("#order_message"));
}

function updateOrderMessage(message) {
    $('#order_message').html(message);    
    $('#order_message').fadeIn().delay(2000).fadeOut();
}

function updateErrorMessage(message) {
    if ($('#error_message').css('display') === 'block') {
        hideErrorMessage();
    }
    // Attach close button
    message += '<span class="fa fa-close" onclick="hideErrorMessage()" }">';
    $('#error_message').html(message);
    $('#error_message').fadeIn();
}

function hideErrorMessage() {
    $('#error_message').hide();
}


var llState = {
    llTarget: '#lazy-target',   // Element to which newly rendered product rows are appended
    productEndpoint: '',
    rowSize: 0,              
    rowsAdded: 0,     
    rowAmount: 0,
    rowTotal: 0,
    pageSize: 0,
    initPageSize: 2,
    language: 'NL',
    bespokeVariables: '',
    renderQueue: [],
    orderedRowsVisible: 0           // Number of rows rendered in the correct order
};

function initiate_product_load(productCodesJson, pageSize, language, rowAmountSetting, url, mainElement, locationId, spinnerId, bespokeVariables) {

    // Check for bespoke js functions tapping into product load changes
    bespokeAsyncHelper.init();        

    // Init state
    llState.llTarget            = locationId ? '#' + locationId : '';
    llState.mainElement         = mainElement;                  
    llState.spinnerId           = '#' + spinnerId;
    llState.rowSize             = rowAmountSetting;
    llState.pageSize            = pageSize;                 
    llState.productEndpoint     = url + '/GetProductList';  
    llState.language            = language;
    llState.productCodes        = productCodesJson;
    llState.bespokeVariables    = bespokeVariables;
    llState.rowAmount           = rowAmountSetting;
    llState.rowTotal            = Math.ceil(llState.productCodes.length / rowAmountSetting);
    llState.renderQueue         = Array.apply(null, Array(llState.rowTotal)).map(function () { }); // Creates an iterable array of certain length
    llState.initPageSize        = llState.initPageSize >= llState.rowTotal ? llState.rowTotal : llState.initPageSize;
    
    // Create row element containers with indexes and attach them to the DOM
    // These act as placeholder in which ajax responses are appended 
    var productRowEl = '';
    for (var j = 0; j < llState.rowTotal; j++) {
        productRowEl += "<div data-row-count='" + j + "'></div>";        
    }
    $(llState.llTarget).append(productRowEl);    

    // Hide pagination
    $("#lower-paging").css("display", "none");

    // Initial productload
    
    for (var i = 0; i < llState.initPageSize; i++) {
        renderNextRow();
        handleView(); 
    }

    // Get the rest of the products, timeout gives faster initial load
    setTimeout(function () {        
        for (var i = llState.initPageSize; i < llState.rowTotal; i++) {
            renderNextRow();
        }
    }, 1000);

    // Setup scroll events
    var scrollTimer, lastScrollFireTime = 0;
    $(window).on('scroll', function () {

        var minScrollTime = 100;
        var now = new Date().getTime();

        function processScroll() {
            var offset = window.innerHeight + window.pageYOffset + $('.uc_footer').height();
            if (offset >= document.body.offsetHeight) {
                if (llState.orderedRowsVisible !== llState.rowTotal) {
                    handleView();
                } else {
                    $(llState.spinnerId).fadeOut(100);
                }
            }
        }

        if (!scrollTimer) {
            if (now - lastScrollFireTime > (3 * minScrollTime)) {
                processScroll();   
                lastScrollFireTime = now;
            }
            scrollTimer = setTimeout(function () {
                scrollTimer = null;
                lastScrollFireTime = new Date().getTime();
                processScroll();
            }, minScrollTime);
        }
    });
}

function handleView(recursive) {
    var next = updateView();
    // If render failed because it is not on the queue yet retry again
    var lastRowAdded = llState.orderedRowsVisible === llState.rowTotal;
    if (!next && !lastRowAdded) {
        setTimeout(handleView, 500);
        $(llState.spinnerId).fadeIn(100);
    } else {
        $(llState.spinnerId).fadeOut(100);
    }
}

// Checks all elements on the queue and checks if they are visible in the view
function updateView() {
    var queue = llState.renderQueue;
    var startIndex = llState.orderedRowsVisible;

    // If no ajax requested product has been set on this index of the queue
    if (queue[startIndex] == null) {
        return false;
    } else {
        queue[llState.orderedRowsVisible].elemRef.fadeIn(500);
        llState.orderedRowsVisible++;
        if (llState.orderedRowsVisible === llState.rowTotal) {
            // Display lower pagination when last row has been loaded in
            $("#lower-paging").css("display", "");
        }
        return true;
    }
}

// Attaches product row html to the DOM and adds it element ref to a queue
function renderRowToQueue(productRowHtml, rowIndex, data) {
    var productRowTarget = $('[data-row-count="' + rowIndex + '"]');
   
    productRowTarget.append(productRowHtml).hide();
    llState.renderQueue[rowIndex] = { elemRef: productRowTarget, visible: false };

    // Pass reference data for bespoke helper
    bespokeAsyncHelper.loadedProducts(data, productRowTarget);

    // Set layout for product blocks
    // set the overview of the product list    
    if (llState.mainElement === undefined || llState.mainElement === '') {
        setOverview();
    } else {
        show_blocks_custom(llState.mainElement);
    }

    // Setup quantity buttons on elements that have been added to the DOM
    for (i = 0; i < Object.keys(data.decimals).length; i++) {
        var prod_code = Object.keys(data.decimals)[i];
        var decimals = data.decimals[prod_code];
        setup_order_buttons(decimals, prod_code);
    }
    // Setup async order buttons to elements that just have been added to the queue
    set_async_order_buttons(productRowTarget);    
}


// Each time this function is called a new rows is rendered
function renderNextRow() {
    // Setup products codes with a config
    var productCodes = [];
    var startIndex = llState.rowsAdded * llState.rowSize;

    for (var j = startIndex; j < llState.rowSize + startIndex; j++) {
        productCodes.push(llState.productCodes[j]);
    }

    var config = {
        language: llState.language,
        addedCount: llState.rowsAdded,
        bespokeVariables: llState.bespokeVariables,
        prod_codes: productCodes
    };    

    llState.rowsAdded += 1; 

    $.ajax({
        rowIndex: llState.rowsAdded - 1,
        type: "POST",
        url: llState.productEndpoint,
        data: JSON.stringify(config),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {

            var data = res.d;

            // Substring the ASP elements from the returned html because these cause issues
            var substringedHtml = data.html.substring(data.html.indexOf('<div class="product-row">'), data.html.indexOf('<div class="end-anchor">'));                  

            renderRowToQueue(substringedHtml, this.rowIndex, data);
            
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            $('#error_message').html(err.Message);
            $('#error_message').fadeIn().delay(1500).fadeOut();
        }
    });
}


function setup_order_buttons(decimals, prod_code) {
    var fld = $("input[data-prod-code='" + prod_code + "']");

    var incrementBtn = fld.parent().children("input.quantity-plus");
    var decrementBtn = fld.parent().children("input.quantity-min");
    incrementBtn.attr("onclick", "return false;");
    decrementBtn.attr("onclick", "return false;");

    incrementBtn.click(function () {
        var fld = $(this).parent().children("input[type=text]");
        var changeToComma = fld.val().indexOf(".") === -1;
        var fldVal = fld.val();

        if (fldVal === '-' || fldVal === '') {
            var newValue = 1;
            newValue = newValue.toFixed(decimals);
            if (changeToComma) {
                newValue = newValue.replace(".", ",");
            }
            fld.val(newValue);
        }
        else {
            var newVal = fldVal.replace(",", ".");
            newVal = parseFloat(newVal) + 1;
            newVal = newVal.toFixed(decimals);
            if (changeToComma) {
                newVal = newVal.replace('.', ',');
            }
            fld.val(newVal);
        }
    });

    decrementBtn.click(function () {
        var fld = $(this).parent().children("input[type=text]");
        var changeToComma = fld.val().indexOf(".") === -1;
        var fldVal = fld.val();

        if (fldVal === '-' || fldVal === '1' || fldVal === '' || fldVal === '0') {
            var newValue = 1;
            newValue = newValue.toFixed(decimals);
            if (changeToComma) {
                newValue = newValue.replace(".", ",");
            }
            fld.val(newValue);
        }
        else {
            var newVal = fldVal.replace(",", ".");
            newVal = parseFloat(newVal) - 1;
            newVal = newVal.toFixed(decimals);
            if (changeToComma) {
                newVal = newVal.replace('.', ',');
            }
            fld.val(newVal);
        }
    });
}

// This function checks for bespoke functions
// and fires them if either a product row is 
// lazyloaded or a product is ordered asynchronously
var bespokeAsyncHelper = (function () {

    var useOrderProductHelper = false;
    var useLazyLoadingHelper = false;

    function init() {
        // Check if bespoke functions exist before calling them
        useOrderProductHelper = typeof bespokeAsyncOrder === 'function' ? true : false;
        useLazyLoadingHelper = typeof bespokeLazyLoading === 'function' ? true : false;
    }

    function addedToCart(productData) {
        if (useOrderProductHelper) {
            bespokeAsyncOrder(productData);
        }
    }

    function loadedProducts(productData, htmlElement) {
        if (useLazyLoadingHelper) {
            bespokeLazyLoading(productData, htmlElement);
        }        
    }

    return {
        init: init,
        addedToCart: addedToCart,
        loadedProducts: loadedProducts
    };
}());

var setupVueGlobalScope = (function (w) {

    // Todo, freeze and lock object, determine 
    // Defines global modules
    w.vue = {
        globalWs: {
        },
        modules: {
        },
        translations: {
            label: {},
            title: {},
            button: {},
            text: {}
        }
    };

    // Takes a configuration object from the masterpage    
    function setGlobalWs(globalWsObj) {
        w.vue.globalWs = globalWsObj;
    }

    function addTranslations(translations) {
        var types = Object.keys(translations);
        types.forEach(function (type) {
            var typeValKeys = Object.keys(translations[type]);
            typeValKeys.forEach(function (key) {
                w.vue.translations[type][key] = translations[type][key];
            });
        });
    }

    function addProperty(module, key, value) {
        w.vue[module][key] = value;
    }

    // Registers an array of vue modules that can be loaded in
    function useModule(moduleName, initConfig) {
        w.vue.modules[moduleName] = {
            config: initConfig
        };
    }

    return {
        setGlobalWs: setGlobalWs,
        addProperty: addProperty,
        useModule: useModule,
        addTranslations: addTranslations
    };

})(window);