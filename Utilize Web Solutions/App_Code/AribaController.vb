﻿Imports System.Net
Imports System.Net.Http
Imports System.Web.Http
Imports System.Xml
Imports Newtonsoft.Json

Public Class AribaController
    Inherits ApiController

    <HttpPost>
    Public Function SetupRequest() As HttpResponseMessage
        Dim body = Request.Content.ReadAsStringAsync.Result
        Dim parsed = Ariba.AribaXmlSerializer.Deserialize(Of Ariba.PunchoutSetupRequest.CXML)(body)
        Dim from_credentials = JsonConvert.SerializeObject(parsed.Header.From.Credential)
        Dim to_credentials = JsonConvert.SerializeObject(parsed.Header.To.Credential)
        Dim punchout_url = parsed.Request.PunchOutSetupRequest.BrowserFormPost.URL
        Dim buyercookie = parsed.Request.PunchOutSetupRequest.BuyerCookie
        Dim global_ws As New Utilize.Web.Solutions.Webshop.global_ws
        Dim uws_default_customer = global_ws.get_webshop_setting("arb_customer_id")

        'Create Webshop user where the login code = BuyerCookie value
        Dim ubo_customer As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_customers_users")
        Dim pwd As String = Guid.NewGuid().ToString("N")

        If Not ubo_customer.table_locate("uws_customers_users", "login_code = '" + buyercookie + "' and hdr_id = '" + uws_default_customer + "'") Then
                ubo_customer.table_insert_row("uws_customers_users", global_ws.user_information.user_id)
                ubo_customer.field_set("hdr_id", uws_default_customer)
            End If

            ubo_customer.field_set("login_code", buyercookie)
            ubo_customer.field_set("fst_name", "Ariba first name")
            ubo_customer.field_set("lst_name", "Ariba last name")
            ubo_customer.field_set("login_password", pwd)
        'add punchout URL to webshop user
        ubo_customer.field_set("arb_punchout_url", punchout_url)
        ubo_customer.field_set("arb_credential_from", from_credentials)
        ubo_customer.field_set("arb_credential_to", to_credentials)
        ubo_customer.table_update(global_ws.user_information.user_id)


        'Çheck ariba session table for current buyercokie
        Dim qsc_ariba_session As New Utilize.Data.QuerySelectClass
                With qsc_ariba_session
                    .select_fields = "timestamp, session_string"
                    .select_from = "uws_ariba_session"
                    .select_where = "login_code = @login_code "
                End With
                qsc_ariba_session.add_parameter("@login_code", buyercookie)
                Dim dt_ariba_session As System.Data.DataTable = qsc_ariba_session.execute_query()
                Dim dt_single_timestamp As Date = DateTime.Now()

        Dim sesssion_token = String.Empty

        If dt_ariba_session.Rows.Count = 1 Then

            dt_single_timestamp = dt_ariba_session.Rows(0)("timestamp")
            If dt_single_timestamp.AddHours(24) > DateTime.Now() Then
                sesssion_token = Convert.ToString(dt_ariba_session.Rows(0)("session_string"))
            Else
                Dim qsc_delete_session As New Utilize.Data.QueryCustomSelectClass
                With qsc_delete_session
                    .QueryString = "delete from uws_ariba_session where login_code = @login_code"
                End With
                qsc_delete_session.add_parameter("login_code", buyercookie)
                qsc_delete_session.execute_query("ariba_service")
            End If
        End If

        If dt_ariba_session.Rows.Count > 1 Then
            Dim qsc_delete_session As New Utilize.Data.QueryCustomSelectClass
            With qsc_delete_session
                .QueryString = "delete from uws_ariba_session where login_code = @login_code"
            End With
            qsc_delete_session.add_parameter("login_code", buyercookie)
            qsc_delete_session.execute_query("ariba_service")
        End If


        If String.IsNullOrEmpty(sesssion_token) Then

            'Create punchout session in database
            'Renew the token
            sesssion_token = SessionId(80)
            Dim qic_ariba_session As New Utilize.Data.QueryInsertClass
            qic_ariba_session.insert_fields = "login_code, timestamp, session_id, session_string"
            qic_ariba_session.insert_table = "uws_ariba_session"
            qic_ariba_session.insert_values = "@login_code, @timestamp, @session_string, @session_string"
            qic_ariba_session.add_parameter("login_code", buyercookie)
            qic_ariba_session.add_parameter("timestamp", DateTime.Now())
            qic_ariba_session.add_parameter("session_string", sesssion_token)
            qic_ariba_session.execute_insert("Ariba_service")
        End If


        Dim response = Ariba.PunchoutSetupResponse.PunchOutSetupResponseBuilder.SuccessResponse(sesssion_token)



        Dim httpResponse As HttpResponseMessage = New HttpResponseMessage With {
            .StatusCode = 200,
            .Content = New StringContent(Ariba.AribaXmlSerializer.Serialize(response))
        }
        Return httpResponse
    End Function

    Private Function SessionId(ByRef Length As String) As String
        'Create random strings for use in session_id creation
        Dim str As String = Nothing
        Dim rnd As New Random
        For i As Integer = 1 To Length
            Dim chrInt As Integer = 0
            Do
                chrInt = rnd.Next(30, 122)
                If (chrInt >= 48 And chrInt <= 57) Or (chrInt >= 65 And chrInt <= 90) Or (chrInt >= 97 And chrInt <= 122) Then
                    Exit Do
                End If
            Loop
            str &= Chr(chrInt)
        Next
        Return str
    End Function

End Class
