﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Utilize.Web.Solutions.Translations

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://www.utilize.nl/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class ImportMultiOrderService
    Inherits System.Web.Services.WebService

    Private loImportObject As ImportObject

    <WebMethod()>
    Public Function ImportOrders(ByVal SessionId As String, ByVal cust_id As String, ByVal user_id As String, ByVal CSVContent As String) As Boolean
        ' UWS-436: Switch generic service on or not
        Dim ll_service_on As Boolean = False
        Boolean.TryParse(Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("import_multiorder_service_on"), ll_service_on)
        If Not ll_service_on Then
            'Throw New Exception("Import MultiOrder service is uitgezet in de web.config")
            Return False
        End If
        ' END UWS-436: Switch generic service on or not

        loImportObject = New ImportObject(SessionId)

        Try
            Dim lo_datasecurity_decryptor As Utilize.Secure.WebEncryption.Decryptor = Application("lo_datasecurity_decryptor")
            Dim CSVContentArray As String() = lo_datasecurity_decryptor.Decrypt(CSVContent)

            For Each csv_file_content As String In CSVContentArray
                import_orders(cust_id, user_id, csv_file_content)
            Next
        Catch ex As Exception
            loImportObject.ErrorText = global_trans.translate_message("message_multi_import_error", "Er is een fout opgetreden bij het importeren van gegevens, controleer uw bestand", loImportObject.Language)
            loImportObject.ErrorCount = loImportObject.RowCount - loImportObject.SuccessCount
        End Try

        loImportObject.Finished = True

        Return True
    End Function

    Private Function create_data_table(ByVal header_list As List(Of String), ByVal csv_lines As List(Of String)) As System.Data.DataTable
        Dim dt_data_table As New System.Data.DataTable
        dt_data_table.Columns.Add("productcode")
        dt_data_table.Columns.Add("productdescription")
        dt_data_table.Columns.Add("quantity")
        dt_data_table.Columns.Add("ordernumber")
        dt_data_table.Columns.Add("email")
        dt_data_table.Columns.Add("name")
        dt_data_table.Columns.Add("address")
        dt_data_table.Columns.Add("postalcode")
        dt_data_table.Columns.Add("city")
        dt_data_table.Columns.Add("countrycode")
        dt_data_table.Columns.Add("phonenumber")
        dt_data_table.AcceptChanges()

        For Each loCsvLine As String In csv_lines
            Dim order_line_split_str As String() = loCsvLine.Split(";")
            Dim order_line_split As List(Of String) = New List(Of String)(order_line_split_str)

            Dim dr_data_row As System.Data.DataRow = dt_data_table.NewRow()

            If Not order_line_split(header_list.IndexOf("productcode")).Trim() = "" Then
                dr_data_row.Item("productcode") = order_line_split(header_list.IndexOf("productcode")).Trim()
                dr_data_row.Item("productdescription") = order_line_split(header_list.IndexOf("productdescription")).Trim()
                dr_data_row.Item("quantity") = order_line_split(header_list.IndexOf("quantity")).Trim()
                dr_data_row.Item("ordernumber") = order_line_split(header_list.IndexOf("ordernumber")).Trim()
                dr_data_row.Item("email") = order_line_split(header_list.IndexOf("email")).Trim()
                dr_data_row.Item("name") = order_line_split(header_list.IndexOf("name")).Trim()
                dr_data_row.Item("address") = order_line_split(header_list.IndexOf("address")).Trim()
                dr_data_row.Item("postalcode") = order_line_split(header_list.IndexOf("postalcode")).Trim()
                dr_data_row.Item("city") = order_line_split(header_list.IndexOf("city")).Trim()
                dr_data_row.Item("countrycode") = order_line_split(header_list.IndexOf("countrycode")).Trim()
                dr_data_row.Item("phonenumber") = order_line_split(header_list.IndexOf("phonenumber")).Trim()
                dt_data_table.Rows.Add(dr_data_row)

                dt_data_table.AcceptChanges()
            End If
        Next

        Dim dv_sorted_table As System.Data.DataView = dt_data_table.DefaultView
        dv_sorted_table.Sort = "ordernumber"

        Return dv_sorted_table.ToTable()
    End Function

    ''' <summary>
    ''' Import content of csv file to add orders to database
    ''' 
    ''' First line should be Dutch header of field names
    ''' 
    ''' CSV is choosen instead of for example XLSX to make sure that it always works and not depends on oledb installed
    ''' </summary>
    ''' <param name="cust_id">customer id of user uploading the file</param>
    ''' <param name="user_id">user id of user uploading the file</param>
    ''' <param name="csv_file_content">csv file content as string</param>
    ''' <returns></returns>
    Private Function import_orders(cust_id As String, user_id As String, csv_file_content As String) As Boolean
        Dim ll_testing As Boolean = True
        Dim ll_error_occured As Boolean = False

        'Is there any content in csv file?
        If Not String.IsNullOrEmpty(csv_file_content) Then
            'Split content into lines
            Dim csv_file_lines_srt As String() = csv_file_content.Replace(vbLf, "").Split(vbNewLine)
            Dim csv_file_lines As List(Of String) = New List(Of String)(csv_file_lines_srt)

            'Get header fields and remove
            Dim header_list_str As String() = csv_file_lines(0).ToLowerInvariant.Replace(" ", "").Split(";")
            Dim header_list As List(Of String) = New List(Of String)(header_list_str)
            csv_file_lines.RemoveAt(0)

            Dim header_complete As Boolean = header_list.Contains("productcode") And header_list.Contains("productdescription")
            header_complete = header_complete And header_list.Contains("quantity") And header_list.Contains("ordernumber") And header_list.Contains("email")
            header_complete = header_complete And header_list.Contains("name") And header_list.Contains("address") And header_list.Contains("postalcode")
            header_complete = header_complete And header_list.Contains("city") And header_list.Contains("countrycode") And header_list.Contains("phonenumber")

            'Set known fields per line and add to table uws_orders and uws_order_lines  if all columns are available
            If header_complete Then
                Dim ubo_order As Utilize.Data.BusinessObject = Nothing
                Dim llNewRow As Boolean = True

                Dim dtOrderLines As System.Data.DataTable = create_data_table(header_list, csv_file_lines)
                Dim lcOrderNumber As String = ""
                Dim lnRowCount As Integer = 1

                ' Bepaal hier het aantal orders
                For I As Integer = 0 To dtOrderLines.Rows.Count - 1
                    If dtOrderLines.Rows(I).Item("ordernumber") <> lcOrderNumber And I > 0 Then
                        lcOrderNumber = dtOrderLines.Rows(I).Item("ordernumber")
                        lnRowCount += 1
                    End If
                Next

                lcOrderNumber = ""
                ' zet hier de property van het import object
                loImportObject.RowCount += lnRowCount

                ' Loop door de regels heen en maak de orders incl. orderregels aan 
                For I As Integer = 0 To dtOrderLines.Rows.Count - 1
                    Dim dr_data_row As System.Data.DataRow = dtOrderLines.Rows(I)
                    lcOrderNumber = dr_data_row.Item("ordernumber")

                    If llNewRow Then
                        loImportObject.CurrentRow += 1

                        ubo_order = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", "", "rec_id")

                        ubo_order.table_insert_row("uws_orders", user_id)
                        ubo_order.field_set("cust_id", cust_id)
                        ubo_order.field_set("user_id", user_id)
                        ubo_order.field_set("order_description", lcOrderNumber)

                        ubo_order.field_set("del_email_address", dr_data_row.Item("email").Trim())
                        ubo_order.field_set("del_address_custom", True)

                        Dim order_busname As String = dr_data_row.Item("name").Trim()
                        Dim order_name As String = ""
                        If order_busname.Contains(" - ") Then
                            order_name = order_busname.Split("-")(1)
                            order_busname = order_busname.Split("-")(0)
                        End If

                        ubo_order.field_set("del_bus_name", order_busname)

                        If Not String.IsNullOrEmpty(order_name) Then
                            ubo_order.field_set("del_fst_name", order_name.Substring(0, order_name.IndexOf(" ")))
                            ubo_order.field_set("del_lst_name", order_name.Substring(order_name.LastIndexOf(" ")))

                            If order_name.Contains(" ") Then
                                ubo_order.field_set("del_infix", order_name.Substring(order_name.IndexOf(" "), order_name.LastIndexOf(" ") - order_name.IndexOf(" ")))
                            End If
                        End If

                        ubo_order.field_set("del_address", dr_data_row.Item("address").Trim())
                        ubo_order.field_set("del_zip_code", dr_data_row.Item("postalcode").Trim())
                        ubo_order.field_set("del_city", dr_data_row.Item("city").Trim())

                        ubo_order.field_set("del_cnt_code", dr_data_row.Item("countrycode").Trim())
                        ubo_order.field_set("del_phone_nr", dr_data_row.Item("phonenumber").Trim())
                        ubo_order.field_set("payment_online", False)
                    End If


                    ubo_order.table_insert_row("uws_order_lines", user_id)
                    ubo_order.field_set("uws_order_lines.prod_code", dr_data_row.Item("productcode").Trim(), "/prog")
                    ubo_order.field_set("uws_order_lines.unit_qty", dr_data_row.Item("quantity").Trim(), "/no_api")

                    ubo_order.api_field_updated("uws_order_lines.ord_qty", ubo_order, "/prog")

                    If I = dtOrderLines.Rows.Count - 1 Then
                        llNewRow = True
                    Else
                        llNewRow = lcOrderNumber <> dtOrderLines.Rows(I + 1).Item("ordernumber")
                    End If

                    If llNewRow Then
                        ubo_order.api_field_updated("uws_orders.order_total_tax", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.order_total", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.vat_total", ubo_order, "/prog")

                        ubo_order.api_field_updated("uws_orders.order_costs", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.vat_code_ord", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.vat_ord", ubo_order, "/prog")

                        ubo_order.api_field_updated("uws_orders.shipping_costs", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.vat_code_ship", ubo_order, "/prog")
                        ubo_order.api_field_updated("uws_orders.vat_ship", ubo_order, "/prog")

                        ll_testing = ubo_order.table_update(user_id)

                        ' When something went wrong, the error booleans is raissed
                        If Not ll_testing Then
                            ll_error_occured = True
                            loImportObject.ErrorCount = loImportObject.ErrorCount + 1
                            loImportObject.ErrorText = loImportObject.ErrorText + "<br/>" + ubo_order.error_message
                        Else
                            loImportObject.SuccessCount = loImportObject.SuccessCount + 1
                        End If
                    End If
                Next
            End If
        End If

        Return ll_error_occured
    End Function


End Class