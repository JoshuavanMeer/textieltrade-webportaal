﻿Imports System.Web
Imports Microsoft.VisualBasic


<Serializable()>
Public Class ImportObject
    Private _sessionId As String = ""

    Public Sub New(ByVal sessionId As String)
        _sessionId = sessionId
    End Sub

    ''' <summary>
    ''' The recid of the order
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RecId As String
        Get
            Return HttpRuntime.Cache("RecId" + _sessionId)
        End Get
        Set(value As String)
            HttpRuntime.Cache("RecId" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' The language of the process
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Language As String
        Get
            Return HttpRuntime.Cache("Language" + _sessionId)
        End Get
        Set(value As String)
            HttpRuntime.Cache("Language" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' The URL of the webservice
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Url As String
        Get
            Return HttpRuntime.Cache("relativeUrl" + _sessionId)
        End Get
        Set(value As String)
            HttpRuntime.Cache("relativeUrl" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' Property to indicate the total rows of the process
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RowCount As Integer
        Get
            Return HttpRuntime.Cache("RowCount" + _sessionId)
        End Get
        Set(value As Integer)
            HttpRuntime.Cache("RowCount" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' Property to indicate the current row of the process
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CurrentRow As Integer
        Get
            Return HttpRuntime.Cache("CurrentRow" + _sessionId)
        End Get
        Set(value As Integer)
            HttpRuntime.Cache("CurrentRow" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' Property to indicate the percentage of progress
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Percentage As Integer
        Get
            Dim lnCurrentRow As Integer = Me.CurrentRow
            Dim lnRowCount As Integer = Me.RowCount

            If lnCurrentRow > 0 And lnRowCount > 0 Then
                Return (100 / lnRowCount) * lnCurrentRow
            Else
                Return 0
            End If
        End Get
    End Property

    Public Property ErrorCount As Integer
        Get
            Return HttpRuntime.Cache("ErrorCount" + _sessionId)
        End Get
        Set(value As Integer)
            HttpRuntime.Cache("ErrorCount" + _sessionId) = value
        End Set
    End Property
    Public Property SuccessCount As Integer
        Get
            Return HttpRuntime.Cache("SuccessCount" + _sessionId)
        End Get
        Set(value As Integer)
            HttpRuntime.Cache("SuccessCount" + _sessionId) = value
        End Set
    End Property

    Public Property ErrorText As String
        Get
            Return HttpRuntime.Cache("ErrorText" + _sessionId)
        End Get
        Set(value As String)
            HttpRuntime.Cache("ErrorText" + _sessionId) = value
        End Set
    End Property
    Public Property SuccesText As String
        Get
            Return HttpRuntime.Cache("SuccesText" + _sessionId)
        End Get
        Set(value As String)
            HttpRuntime.Cache("SuccesText" + _sessionId) = value
        End Set
    End Property

    ''' <summary>
    ''' Property to determine wether the process is finished
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Finished As Boolean
        Get
            Return HttpRuntime.Cache("Finished" + _sessionId)
        End Get
        Set(value As Boolean)
            HttpRuntime.Cache("Finished" + _sessionId) = value
        End Set
    End Property
End Class