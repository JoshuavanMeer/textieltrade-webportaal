﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Utilize.Web.Solutions.Translations

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://www.utilize.nl/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class ImportOrderService
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function ImportOrder(ByVal SessionId As String, ByVal dt_order_lines As System.Data.DataTable) As Boolean
        ' UWS-436: Switch generic service on or not
        Dim ll_service_on As Boolean = False
        Boolean.TryParse(Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("import_order_service_on"), ll_service_on)
        If Not ll_service_on Then
            'Throw New Exception("Import Order service is uitgezet in de web.config")
            Return False
        End If
        ' END UWS-436: Switch generic service on or not

        Dim loImportObject As New ImportObject(SessionId)

        Dim lcRecId As String = loImportObject.RecId
        Dim lc_language As String = loImportObject.Language
        Dim lc_relative_url As String = loImportObject.Url

        Dim ubo_order As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", lcRecId, "rec_id")

        Dim lc_cust_id As String = ubo_order.field_get("uws_orders.cust_id")

        Dim ln_row_current As Integer = 0
        Dim ln_row_succesful As Integer = 0
        Dim ln_row_failed As Integer = 0

        Dim lc_error_string As String = ""
        Dim lc_error As String = ""

        ' Zet hier het aantal nodes
        loImportObject.RowCount = dt_order_lines.Rows.Count

        ' Loop door de regels heen
        For Each dr_data_row As System.Data.DataRow In dt_order_lines.Rows
            ln_row_current += 1

            ' Zet het aantal geïmporteerde regels
            loImportObject.CurrentRow = ln_row_current

            Dim ll_testing As Boolean = True

            If ll_testing Then
                ll_testing = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_show", dr_data_row.Item(0))

                If Not ll_testing Then
                    ln_row_failed += 1

                    lc_error_string += global_trans.translate_message("message_product_not_found", "Het product met code [1] is niet gevonden", lc_language).Replace("[1]", dr_data_row.Item(0)) + "<br>"
                End If
            End If

            If ll_testing Then
                ubo_order.table_insert_row("uws_order_lines", lc_cust_id)
                ubo_order.field_set("uws_order_lines.prod_code", dr_data_row.Item(0), "/prog")
                ubo_order.field_set("uws_order_lines.unit_qty", dr_data_row.Item(1), "/no_api")
                ubo_order.api_field_updated("uws_order_lines.ord_qty", ubo_order, "/prog")


                If Not ubo_order.field_get("uws_order_lines.cpn_product") = True Then
                    ubo_order.api_field_updated("uws_order_lines.prod_price", ubo_order, "/prog")
                    ubo_order.api_field_updated("uws_order_lines.row_amt", ubo_order, "/prog")
                    ubo_order.api_field_updated("uws_order_lines.vat_amt", ubo_order, "/prog")
                End If

                ln_row_succesful += 1
            End If
        Next

        ubo_order.api_field_updated("uws_orders.order_total_tax", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.order_total", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.vat_total", ubo_order, "/prog")

        ubo_order.api_field_updated("uws_orders.order_costs", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.vat_code_ord", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.vat_ord", ubo_order, "/prog")

        ubo_order.api_field_updated("uws_orders.shipping_costs", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.vat_code_ship", ubo_order, "/prog")
        ubo_order.api_field_updated("uws_orders.vat_ship", ubo_order, "/prog")

        ubo_order.table_update(lc_cust_id)

        loImportObject.Finished = True
        loImportObject.ErrorText = lc_error_string
        loImportObject.ErrorCount = ln_row_failed
        loImportObject.SuccessCount = ln_row_succesful

        Return True
    End Function

End Class