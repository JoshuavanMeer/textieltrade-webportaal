﻿Imports Microsoft.VisualBasic
Imports ExpertPdf.HtmlToPdf
Imports System.Drawing


<Serializable()>
Public Class PDFGenerator
    Private _input As String
    Public Property input As String
        Get
            Return _input
        End Get
        Set(ByVal value As String)
            _input = value
        End Set
    End Property

    Private pdfConverter As PdfConverter
    Public Sub New(ByVal input As String, ByVal path_img_footer As String)
        Me.input = input

        Dim ll_testing As Boolean = True

        If ll_testing Then
            pdfConverter = New PdfConverter

            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
            pdfConverter.PdfDocumentOptions.ShowHeader = False
            pdfConverter.PdfDocumentOptions.LeftMargin = 0
            pdfConverter.PdfDocumentOptions.RightMargin = 0
            pdfConverter.PdfDocumentOptions.TopMargin = 20
            pdfConverter.PdfDocumentOptions.BottomMargin = 20
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = True
            pdfConverter.AvoidImageBreak = True
            pdfConverter.AvoidTextBreak = True

            pdfConverter.PdfDocumentOptions.ShowFooter = True
            pdfConverter.PdfFooterOptions.DrawFooterLine = False
            pdfConverter.PdfFooterOptions.FooterText = ""
            pdfConverter.PdfFooterOptions.FooterTextFontSize = 8
            pdfConverter.PdfFooterOptions.FooterTextColor = Color.DarkGray
            pdfConverter.PdfFooterOptions.FooterTextFontType = PdfFontType.TimesBoldItalic
            pdfConverter.PdfFooterOptions.ShowPageNumber = False

            If System.IO.File.Exists(path_img_footer) Then
                Dim lo_footer_image As System.Drawing.Image = Image.FromFile(path_img_footer)
                Using G As Graphics = Graphics.FromImage(lo_footer_image)
                    G.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    G.DrawImage(lo_footer_image, 0, 0, 110, 65)
                End Using

                'pdfConverter.PdfFooterOptions.FooterImage = lo_footer_image
                pdfConverter.PdfFooterOptions.ImageArea = New ImageArea(235, 0, 110, 65, lo_footer_image)
                pdfConverter.PdfFooterOptions.FooterImageLocation = New System.Drawing.PointF(100, 0)
                pdfConverter.PdfFooterOptions.FooterHeight = 70
            End If

            pdfConverter.LicenseKey = "Apxofk9W45ihltLQtKYelQ7h9+0Jm3o+D/mGrPycnO7rHozfB9KwzbnQ7M6ZjIGE"
        End If
    End Sub

    Public Sub save_pdf(ByVal lc_file_name As String)
        pdfConverter.SavePdfFromHtmlStringToFile(input, lc_file_name)
    End Sub
End Class
