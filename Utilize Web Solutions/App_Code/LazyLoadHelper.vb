﻿Imports Microsoft.VisualBasic
Imports Utilize.Web.Solutions.Webshop

Public Class LazyLoadModel
    Public Property Products As List(Of String)
    Public Property Url As String
    Public Property Control As ws_user_control
    Public Property MainElement As String
    Public Property LocationId As String
    Public Property SpinnerId As String
    Public Property PageSize As Integer
    Public Property RowAmount As Integer
    Public Property BespokeFrontEndFiles As List(Of String)
    Public Property BespokeVariables As New Dictionary(Of String, Object)
End Class

Public Class LazyLoadHelper
    Public Shared Sub InitializeLazyLoading(lazyLoadModel As LazyLoadModel)
        Dim products_json As String = Newtonsoft.Json.JsonConvert.SerializeObject(lazyLoadModel.Products)
        Dim bespokeVariableJson As String = Newtonsoft.Json.JsonConvert.SerializeObject(lazyLoadModel.BespokeVariables)
        Dim row_amount As Integer = 12 / lazyLoadModel.RowAmount
        Dim lc_url As String = lazyLoadModel.Url

        ScriptManager.RegisterClientScriptBlock(lazyLoadModel.Control, lazyLoadModel.Control.GetType, "initiate_product_load" + lazyLoadModel.Control.UniqueID,
                                                "$(document).ready(function() {initiate_product_load(" + products_json +
                                                ", " + lazyLoadModel.PageSize.ToString +
                                                ", '" + lazyLoadModel.Control.global_ws.Language + "'" +
                                                ", " + row_amount.ToString +
                                                ", '" + lc_url + "'" +
                                                ", '" + lazyLoadModel.MainElement + "'" +
                                                ", '" + lazyLoadModel.LocationId + "'" +
                                                ", '" + lazyLoadModel.SpinnerId + "'" +
                                                ", " + bespokeVariableJson +
                                                ");});", True)

        ScriptManager.RegisterClientScriptBlock(lazyLoadModel.Control, lazyLoadModel.Control.GetType, "setup_buttons", "$(document).ready(function () {setup_order_buttons(); });", True)

        Dim lo_front_end_files As New List(Of String)(New String() {"/modules/overviewcontrols/uc_product_row.css",
                                                      "/modules/productinformation/uc_order_product.css",
                                                      "/modules/productinformation/uc_product_information.css",
                                                      "/modules/productinformation/uc_product_price.css",
                                                      "/modules/ProductInformation/uc_product_group_matrix.css",
                                                      "/modules/productinformation/uc_order_product.js",
                                                      "/modules/productinformation/uc_product_stock.css"})

        If lazyLoadModel.BespokeFrontEndFiles IsNot Nothing Then
            lo_front_end_files.AddRange(lazyLoadModel.BespokeFrontEndFiles)
        End If

        For Each file In lo_front_end_files
            Dim lc_prefix_folder As String = "~/Webshop"
            If System.IO.File.Exists(lazyLoadModel.Control.Server.MapPath("/bespoke" + file)) Then
                lc_prefix_folder = "~/bespoke"
            End If
            If file.EndsWith(".css") Then
                lazyLoadModel.Control.set_style_sheet(lc_prefix_folder + file, lazyLoadModel.Control)
            ElseIf file.EndsWith(".js") Then
                lazyLoadModel.Control.set_javascript(lc_prefix_folder + file, lazyLoadModel.Control)
            End If
        Next
    End Sub
End Class
