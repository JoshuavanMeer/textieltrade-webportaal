﻿Imports System.IO


Public Class RequestHandler
    Implements IHttpHandler

    Public ReadOnly Property IsReusable As Boolean Implements IHttpHandler.IsReusable
        Get
            Return True
        End Get
    End Property

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If context.Request.Url.AbsoluteUri.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase) Or context.Request.Url.AbsoluteUri.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase) Or context.Request.Url.AbsoluteUri.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase) Then
            Dim path = context.Server.MapPath(context.Request.Url.AbsolutePath)

            If context.Request.UserAgent.IndexOf("Chrome/", StringComparison.InvariantCultureIgnoreCase) >= 0 Then
                context.Response.ClearHeaders()
                context.Response.ClearContent()
                Dim webpPath = System.IO.Path.ChangeExtension(path, ".webp")
                If File.Exists(path) Then
                    Dim content = File.ReadAllBytes(path)
                    context.Response.OutputStream.Write(content, 0, content.Length)
                    context.Response.OutputStream.Flush()
                    context.Response.AppendHeader("Content-type", "image/webp")
                Else
                    ImageFallback(context)
                End If
            Else
                ImageFallback(context)
            End If

        End If
    End Sub
    Private Sub ImageFallback(ByVal context As HttpContext)
        Dim found As Boolean = False
        Dim imagePath = context.Server.MapPath(context.Request.Url.AbsolutePath)

        If File.Exists(imagePath) Then
            found = true
            Dim content = File.ReadAllBytes(imagePath)
            context.Response.OutputStream.Write(content, 0, content.Length)
            context.Response.OutputStream.Flush()
            context.Response.AppendHeader("Content-type", "image" + "/" + Path.GetExtension(imagePath).Replace(".", ""))
        End If

        If Not found Then
            context.Response.ClearContent()
            context.Response.ClearHeaders()
            context.Response.StatusCode = 404
        End If
    End Sub
End Class
Friend Class ImageCacheModel
    Public Property WebpContent As Byte()
    Public Property FallbackImage As String
End Class
