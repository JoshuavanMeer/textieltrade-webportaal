﻿Imports Microsoft.VisualBasic

Public Class TearsheetProduct
    Private _title As String = ""
    Private _code As String = ""
    Private _productSpecs As List(Of KeyValuePair(Of String, String))
    Private _description As String = ""
    Private _mainImgUrl As String = ""
    Private _subImageUrls As List(Of String)
    Private _defaultPrice As String = ""
    Private _customerPrice As String = ""


    Public Property Title As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public Property Code As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    Public Property Description As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Public Property MainImageUrl As String
        Get
            Return _mainImgUrl
        End Get
        Set(ByVal value As String)
            _mainImgUrl = value
        End Set
    End Property

    Public Property ProductSpecs As List(Of KeyValuePair(Of String, String))
        Get
            Return _productSpecs
        End Get
        Set(ByVal value As List(Of KeyValuePair(Of String, String)))
            _productSpecs = value
        End Set
    End Property

    Public Property SubImagesUrl As List(Of String)
        Get
            Return _subImageUrls
        End Get
        Set(ByVal value As List(Of String))
            _subImageUrls = value
        End Set
    End Property

    Public Property DefaultPrice As String
        Get
            Return _defaultPrice
        End Get
        Set(ByVal value As String)
            _defaultPrice = value
        End Set
    End Property

    Public Property CustomerPrice As String
        Get
            Return _customerPrice
        End Get
        Set(ByVal value As String)
            _customerPrice = value
        End Set
    End Property

End Class
