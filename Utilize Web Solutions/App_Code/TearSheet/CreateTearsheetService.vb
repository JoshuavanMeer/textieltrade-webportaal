﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Net
Imports System.Web.Services
Public Class CreateTearsheetService
    Inherits WebService

    Private Sub PostRequest(ByVal postUrl As String, ByVal sessionid As String, ByVal HtmlString As String)
        Dim request As HttpWebRequest = TryCast(WebRequest.Create(postUrl), HttpWebRequest)

        Dim json_data = Encoding.UTF8.GetBytes(HtmlString)

        request.Method = "POST"
        request.ContentLength = json_data.Length
        request.ContentType = "application/json"

        Dim stream = request.GetRequestStream()
        stream.Write(json_data, 0, json_data.Length)
        stream.Close()


        Dim myHttpWebResponse As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)
        Dim responseCode = request.GetResponse()
        Dim responseStream = request.GetResponse().GetResponseStream()
        Dim ms As New MemoryStream
        responseStream.CopyTo(ms)
        Dim loByte() As Byte = ms.ToArray

        HttpContext.Current.Cache(sessionid) = loByte
    End Sub

    Public Sub CreateTearsheet(ByVal sessionid As String, ByVal HtmlString As String, ByVal postUrl As String)
        Dim lc_post_url As String = postUrl
        Try
            PostRequest(lc_post_url, sessionid, HtmlString)

        Catch ex As Exception
            HttpContext.Current.Cache(sessionid + "status") = "Er is iets fout gegaan, probeer het later opnieuw"
            Throw New Exception("Error in CreatTearsheet: " + ex.Message, ex)
        End Try
    End Sub
End Class
