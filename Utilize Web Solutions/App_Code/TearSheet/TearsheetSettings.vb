﻿Imports Microsoft.VisualBasic

Public Class TearsheetSettings
    Private _fontSizeSmall As String = ""
    Private _fontSizeMedium As String = ""
    Private _fontSizeLarge As String = ""

    Public Property FontSizeSmall As String
        Get
            Return _fontSizeSmall
        End Get
        Set(ByVal value As String)
            _fontSizeSmall = value
        End Set
    End Property

    Public Property FontSizeMedium As String
        Get
            Return _fontSizeMedium
        End Get
        Set(ByVal value As String)
            _fontSizeMedium = value
        End Set
    End Property

    Public Property FontSizeLarge As String
        Get
            Return _fontSizeLarge
        End Get
        Set(ByVal value As String)
            _fontSizeLarge = value
        End Set
    End Property
End Class
