﻿Imports Microsoft.VisualBasic

Public Class CompanyInfo
    Private _logoUrl As String = ""
    Private _footerInformation As String = ""

    Public Property LogoUrl As String
        Get
            Return _logoUrl
        End Get
        Set(ByVal value As String)
            _logoUrl = value
        End Set
    End Property

    Public Property FooterInformation As String
        Get
            Return _footerInformation
        End Get
        Set(ByVal value As String)
            _footerInformation = value
        End Set
    End Property
End Class
