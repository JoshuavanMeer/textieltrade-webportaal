﻿Imports Microsoft.VisualBasic
Imports CompanyInfo

Public Class TearsheetDto

    Private _product_name As String = ""
    Private _product_brand As String = ""
    Private _companyInfo As CompanyInfo
    Private _tearsheetProduct As TearsheetProduct
    Private _tearsheetSettings As TearsheetSettings

    Public Property CompanyInfo As CompanyInfo
        Get
            Return _companyInfo
        End Get
        Set(ByVal value As CompanyInfo)
            _companyInfo = value
        End Set
    End Property

    Public Property TearsheetProduct As TearsheetProduct
        Get
            Return _tearsheetProduct
        End Get
        Set(ByVal value As TearsheetProduct)
            _tearsheetProduct = value
        End Set
    End Property

    Public Property TearsheetSettings As TearsheetSettings
        Get
            Return _tearsheetSettings
        End Get
        Set(ByVal value As TearsheetSettings)
            _tearsheetSettings = value
        End Set
    End Property

End Class


