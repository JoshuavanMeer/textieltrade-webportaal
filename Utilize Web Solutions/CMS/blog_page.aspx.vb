﻿
Partial Class blog_page
    Inherits CMSPageTemplate

    Private blog_id As String = ""

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Request.Params("RecId") Is Nothing Then
            blog_id = Request.Params("RecId")
        End If

        ' Sla de code van de template op
        Me.cms_template_code = Me.global_cms.get_cms_setting("blog_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_blog_posts", "blog_title_" + Me.global_cms.Language, blog_id)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not blog_id = "" Then
            ' Me.meta_description = Utilize.Data.DataProcedures.GetValue("ucm_blog", "meta_desc_" + Me.global_cms.Language, blog_id)
            ' Me.meta_keywords = Utilize.Data.DataProcedures.GetValue("ucm_blog", "meta_keyw_" + Me.global_cms.Language, blog_id)

            Dim ph_blog_zone As Utilize.Web.UI.WebControls.placeholder = cms_template_zone
            Dim uc_blog_text As CMS.cms_user_control = LoadUserControl("Modules/Blog/uc_blog_text.ascx")
            uc_blog_text.set_property("block_id", blog_id)

            ph_blog_zone.Controls.Add(uc_blog_text)
        End If
    End Sub
End Class
