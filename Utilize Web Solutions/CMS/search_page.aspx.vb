﻿
Partial Class search_page
    Inherits CMSPageTemplate

    Private news_id As String = ""

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Sla de code van de template op
        Me.cms_template_code = Me.global_cms.get_cms_setting("SEARCH_TEMPLATE")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_search_results", "Zoekresultaten", Me.global_cms.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_search_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim h1_title As New HtmlGenericControl("h1")
        h1_title.InnerText = global_trans.translate_title("title_search_results", "Zoekresultaten", Me.global_cms.Language)
        ph_search_zone.Controls.Add(h1_title)

        Dim uc_search_results As CMS.cms_user_control = LoadUserControl("Modules/ContentSearch/uc_content_search_results.ascx")
        ph_search_zone.Controls.Add(uc_search_results)
    End Sub
End Class
