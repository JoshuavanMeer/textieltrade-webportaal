﻿
Partial Class calendar
    Inherits CMSPageTemplate

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Sla de code van de template op
        Me.cms_template_code = Me.global_cms.get_cms_setting("SEARCH_TEMPLATE")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_search_results", "Zoekresultaten", Me.global_cms.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        For I As Integer = 1 To 8
            Dim lo_content As System.Web.UI.WebControls.ContentPlaceHolder = Me.Page.Master.FindControl("column_" + I.ToString())
            lo_content.Visible = False
        Next

        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = cms_template_zone
        ph_zone.Parent.Visible = True

        Dim uc_calendar As CMS.cms_user_control = LoadUserControl("Modules/Calendar/uc_calendar.ascx")
        ph_zone.Controls.Add(uc_calendar)
    End Sub
End Class
