﻿
Partial Class MasterPage_1
    Inherits Utilize.Web.Solutions.CMS.cms_masterpage_base

    Public PageCorrectionStyles As String

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        ' UWS-619: Password AVG
        If Me.global_ws.user_information.user_logged_on Then
            If Not Me.IsPostBack Then
                If Me.global_ws.check_password_valid(Me.Request) Then
                    Session.Item("password_validity") = True
                Else
                    Session.Item("password_validity") = False
                    Response.Redirect("~/" + me.global_ws.language + "/account/changepassword.aspx")
                End If
            End If
        End If
        ' END UWS-610 

        GetPageCorrection()

        Dim header As Integer = Convert.ToInt32(Me.global_cms.get_cms_setting("header_id"))
        header += 1

        Me.autocomplete_address_via_google_map()

        Dim uc_header As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/Header/uc_header_v" + header.ToString() + ".ascx")
        uc_header.ID = "uc_header"
        Me.ph_header.Controls.Add(uc_header)

        Dim uc_footer As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/footer/uc_footer.ascx")
        uc_footer.ID = "uc_footer"
        Me.ph_footer.Controls.Add(uc_footer)

        Select Case Utilize.Data.DataProcedures.GetValue("utlz_fw_settings", "run_mode", "0000000001")
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Development
                Me.lt_watermark.Text = "DEV"
                Me.div_watermark.Attributes.Add("class", "utilize-watermark watermark-development")
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Test
                Me.lt_watermark.Text = "TEST"
                Me.div_watermark.Attributes.Add("class", "utilize-watermark watermark-test")
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Production
                Me.ph_watermark.Visible = False
        End Select
    End Sub

    Public Sub GetPageCorrection()
        Dim stylesheetText As String = "<link href='"
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True And
            Me.global_ws.user_information.user_logged_on And
            Me.global_ws.user_information.ubo_customer.field_get("uws_customers.use_colors") Then

            stylesheetText += Fingerprint.tag(ResolveUrl("~/Styles/PageCorrections.css?rec=" + Me.global_ws.user_information.user_logged_on.ToString + "|" + global_ws.user_information.user_customer_id_original))
        Else
            Dim pageCorrectionsFile As String = "~/Styles/PageCorrectionsActual.css"
            If Not System.IO.File.Exists(Server.MapPath(pageCorrectionsFile)) Then
                Utilize.Data.API.CMS.PageCorrectionsHelper.CreateCssFile()
            End If

            stylesheetText += Fingerprint.tag(ResolveUrl(pageCorrectionsFile))
        End If

        stylesheetText += "' rel='stylesheet' type='text/css' />"

        Me.lt_page_corrections.Text = stylesheetText
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Me.ph_corrections.Visible = System.IO.File.Exists(Server.MapPath("~/Styles/CustomerCorrections.css"))
        Me.ph_fixfooter.Visible = Request.Browser.Browser <> "IE"
        Me.ph_minimalist.Visible = Me.global_ws.get_webshop_setting("minimalist_theme")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "carousel_script", "jQuery(document).ready(function () {App.init();App.initScrollBar();App.initParallaxBg();OwlCarousel.initOwlCarousel();RevolutionSlider.initRSfullWidth();});", True)

        ' Initiate async order buttons
        If Me.global_ws.get_webshop_setting("use_async_order") Then
            Me.lt_async_avail.Text = "<div id='use-async'></div>"
        End If

        Dim lcScript As String = Me.global_ws.get_webshop_setting("zopimchat_script")
        If Not String.IsNullOrEmpty(lcScript) Then
            Me.bdy_master.Controls.Add(New LiteralControl(lcScript))
        End If

        ' Check marketing accept cookie to see if the scripts need to be loaded
        If Not HttpContext.Current.Request.Cookies("utlz_accept_cookie") Is Nothing Then
            If HttpContext.Current.Request.Cookies("utlz_accept_cookie").Value = "True" Then
                Me.lt_marketing.Text = Me.global_cms.get_cms_setting("marketing_scripts")
            End If
        End If
    End Sub

    Private Sub autocomplete_address_via_google_map()
        Dim ll_autocomplete As Boolean = Me.global_ws.get_webshop_setting("autocomplete")

        If ll_autocomplete Then
            Dim lc_key As String = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("google_api_key")
            Dim lc_src As String = "https://maps.googleapis.com/maps/api/js?key=" + lc_key + "&libraries=places&language=" + Me.global_ws.Language
            Me.lt_google_map_script.Text = "<script type=""text/javascript"" src=""" + lc_src + """></script>"
        End If
    End Sub
End Class