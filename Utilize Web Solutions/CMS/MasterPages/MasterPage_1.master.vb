﻿
Partial Class MasterPage_1
    Inherits Utilize.Web.Solutions.CMS.cms_masterpage_base
    Private ll_vuejs As Boolean = False

    Private Class VueGlobalWs
        Public Property userInformation As VueUserInformation
        Public Property webshopSettings As VueWebshopSettings
        Public Property endpoints As VueEndpoints
    End Class

    Private Class VueWebshopSettings
        Public Property language As String
        Public Property showStock As Boolean
        Public Property stockType As Integer
        Public Property showPrices As Boolean
        Public Property showOrderProduct As Boolean
    End Class

    Private Class VueUserInformation
        Public Property clientCode As String
        Public Property customerId As String
        Public Property userLoggedOn As String
        Public Property customerPriceList As String
    End Class

    Private Class VueEndpoints
        Public Property elasticEndpoint As String
    End Class

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init

        ll_vuejs = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_vuejs")

        ' Set Vue global settings
        If ll_vuejs Then
            Dim globalWs As VueGlobalWs = New VueGlobalWs With {
                .userInformation = New VueUserInformation With {
                    .clientCode = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("intermediate_client"),
                    .customerId = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr"),
                    .userLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .customerPriceList = Me.global_ws.user_information.ubo_customer.field_get("pl_code")
                },
                .webshopSettings = New VueWebshopSettings With {
                    .language = Me.global_ws.Language,
                    .stockType = Me.global_ws.get_webshop_setting("stock_type"),
                    .showStock = Me.global_ws.user_information.user_logged_on,
                    .showPrices = Me.global_ws.user_information.user_logged_on,
                    .showOrderProduct = Me.global_ws.user_information.user_logged_on
                },
                .endpoints = New VueEndpoints With {
                    .elasticEndpoint = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("elastic_host")
                }
            }
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupVueGlobalSettings", "setupVueGlobalScope.setGlobalWs(" + Newtonsoft.Json.JsonConvert.SerializeObject(globalWs) + ");", True)
        End If

        ' UWS-619: Password AVG
        If Me.global_ws.user_information.user_logged_on Then
            If Not Me.IsPostBack Then
                If Me.global_ws.check_password_valid(Me.Request) Then
                    Session.Item("password_validity") = True
                Else
                    Session.Item("password_validity") = False
                    Response.Redirect("~/" + me.global_ws.language + "/account/changepassword.aspx")
                End If
            End If
        End If
        ' END UWS-619        

        GetPageCorrection()

        Dim header As Integer = Convert.ToInt32(Me.global_cms.get_cms_setting("header_id"))
        header += 1

        Select Case header
            Case 1
                Me.header_v1.Visible = True
            Case 2
                Me.header_v2.Visible = True
            Case Else
                Exit Select
        End Select

        Me.AddGoogleRecaptchaScript()
        Me.autocomplete_address_via_google_map()

        Dim uc_cookies As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/Cookies/uc_cookies.ascx")
        uc_cookies.ID = "uc_cookies"
        Me.ph_cookies.Controls.Add(uc_cookies)

        Dim uc_header As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/Header/uc_header_v" + header.ToString() + ".ascx")
        uc_header.ID = "uc_header"
        Me.ph_header.Controls.Add(uc_header)

        Dim uc_footer As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/footer/uc_footer.ascx")
        uc_footer.ID = "uc_footer"
        Me.ph_footer.Controls.Add(uc_footer)

        Select Case Utilize.Data.DataProcedures.GetValue("utlz_fw_settings", "run_mode", "0000000001")
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Development
                Me.lt_watermark.Text = "DEV"
                Me.div_watermark.Attributes.Add("class", "utilize-watermark watermark-development")

                ' Set up vue development scripts
                If ll_vuejs Then
                    Me.ph_development_vue_scripts.Visible = True
                    Me.ph_development_vue_styles.Visible = True
                End If
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Test
                Me.lt_watermark.Text = "TEST"
                Me.div_watermark.Attributes.Add("class", "utilize-watermark watermark-test")

                ' Set up vue production scripts
                If ll_vuejs Then
                    Me.ph_production_vue_scripts.Visible = True
                    Me.ph_production_vue_styles.Visible = True
                End If
            Case Utilize.Data.API.Webshop.EnvironmentConfigurationType.Production
                Me.ph_watermark.Visible = False

                ' Set up vue production scripts
                If ll_vuejs Then
                    Me.ph_production_vue_scripts.Visible = True
                    Me.ph_production_vue_styles.Visible = True
                End If
        End Select
    End Sub

    Public Sub GetPageCorrection()
        Dim stylesheetText As String = "<link href='"
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True And
            Me.global_ws.user_information.user_logged_on And
            Me.global_ws.user_information.ubo_customer.field_get("uws_customers.use_colors") Then

            stylesheetText += Fingerprint.tag(ResolveUrl("~/Styles/PageCorrections.css?rec=" + Me.global_ws.user_information.user_logged_on.ToString + "|" + global_ws.user_information.user_customer_id_original))
        Else
            Dim pageCorrectionsFile As String = "~/Styles/PageCorrectionsActual.css"
            If Not System.IO.File.Exists(Server.MapPath(pageCorrectionsFile)) Then
                Utilize.Data.API.CMS.PageCorrectionsHelper.CreateCssFile()
            End If

            stylesheetText += Fingerprint.tag(ResolveUrl(pageCorrectionsFile))
        End If

        stylesheetText += "' rel='stylesheet' type='text/css' />"

        Me.lt_page_corrections.Text = stylesheetText
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ph_corrections.Visible = System.IO.File.Exists(Server.MapPath("~/Styles/CustomerCorrections.css"))
        Me.ph_fixfooter.Visible = Request.Browser.Browser <> "IE"
        Me.ph_minimalist.Visible = Me.global_ws.get_webshop_setting("minimalist_theme")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "carousel_script", "jQuery(document).ready(function () {App.init();App.initScrollBar();App.initParallaxBg();OwlCarousel.initOwlCarousel();RevolutionSlider.initRSfullWidth();});", True)

        Dim lcScript As String = Me.global_ws.get_webshop_setting("zopimchat_script")

        ' Initiate async order buttons
        If Me.global_ws.get_webshop_setting("use_async_order") Then
            Me.lt_async_avail.Text = "<div id='use-async'></div>"
        End If

        If Not String.IsNullOrEmpty(lcScript) Then
            Me.bdy_master.Controls.Add(New LiteralControl(lcScript))
        End If

        ' Check marketing accept cookie to see if the scripts need to be loaded
        If Not HttpContext.Current.Request.Cookies("utlz_accept_cookie") Is Nothing Then
            If HttpContext.Current.Request.Cookies("utlz_accept_cookie").Value = "True" Then
                Me.lt_marketing.Text = Me.global_cms.get_cms_setting("marketing_scripts")
            End If
        End If
    End Sub

    ''' <summary>
    ''' reCAPTCHA v2
    ''' https://developers.google.com/recaptcha/intro
    ''' </summary>
    Private Sub AddGoogleRecaptchaScript()
        Dim lc_recaptcha_api_key As String = ""
        Dim lc_recaptcha_api_secret As String = ""

        Dim ll_resume As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_CM_GO_RCAP")

        If ll_resume And String.IsNullOrEmpty(System.Web.HttpContext.Current.Session("RecaptchaApi")) Then
            ' Get the reCAPTCHA api key and secret
            Dim qsc_google_key As New Utilize.Data.QuerySelectClass
            qsc_google_key.select_fields = "*"
            qsc_google_key.select_from = "UCM_API_KEYS"
            qsc_google_key.select_where = "key_type = 4" ' 4 = Google Recaptcha

            Dim dt_google_data As System.Data.DataTable = qsc_google_key.execute_query()

            ' Save the recaptha key and secret in a public variable
            If dt_google_data.Rows.Count > 0 Then
                lc_recaptcha_api_key = dt_google_data.Rows.Item(0).Item("API_KEY")
                lc_recaptcha_api_secret = dt_google_data.Rows.Item(0).Item("API_SECRET")

                System.Web.HttpContext.Current.Session("RecaptchaApi") = "True"
                System.Web.HttpContext.Current.Session("RecaptchaApiKey") = lc_recaptcha_api_key
                System.Web.HttpContext.Current.Session("RecaptchaApiSecret") = lc_recaptcha_api_secret
            End If
        End If

        If ll_resume Then
            lc_recaptcha_api_key = System.Web.HttpContext.Current.Session("RecaptchaApiKey")
            lc_recaptcha_api_secret = System.Web.HttpContext.Current.Session("RecaptchaApiSecret")

            ' Only load the scripts when the key and secret are available
            ll_resume = Not String.IsNullOrEmpty(lc_recaptcha_api_key) And Not String.IsNullOrEmpty(lc_recaptcha_api_secret)
        End If

        If ll_resume Then
            Dim lc_language_parameter As String = "&hl=" + Me.global_cms.Language
            Dim lc_src As String = "https://www.google.com/recaptcha/api.js?render=explicit" + lc_language_parameter
            Me.lt_google_recaptcha_script.Text = "<script src=""" + lc_src + """></script>"
        End If
    End Sub

    Private Sub autocomplete_address_via_google_map()
        Dim ll_autocomplete As Boolean = Me.global_ws.get_webshop_setting("autocomplete")

        If ll_autocomplete Then
            Dim lc_key As String = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("google_api_key")
            Dim lc_src As String = "https://maps.googleapis.com/maps/api/js?key=" + lc_key + "&libraries=places&language=" + Me.global_ws.Language
            Me.lt_google_map_script.Text = "<script type=""text/javascript"" src=""" + lc_src + """></script>"
        End If
    End Sub
End Class