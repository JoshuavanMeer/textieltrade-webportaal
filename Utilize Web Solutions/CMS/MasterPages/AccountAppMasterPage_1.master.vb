﻿
Partial Class AccountAppMasterPage_1
    Inherits Utilize.Web.Solutions.CMS.cms_masterpage_base

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Dim header = Convert.ToInt32(Me.global_cms.get_cms_setting("header_id"))
        header = header + 1

        Dim uc_header As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/Header/uc_header_v" + header.ToString() + ".ascx")
        uc_header.ID = "uc_header"
        uc_header.Visible = False

        'Dim uc_footer As CMS.cms_user_control = CType(Me.Page, Utilize.Web.Solutions.Base.base_page_base).LoadUserControl("~/CMS/Modules/footer/uc_footer.ascx")
        'uc_footer.ID = "uc_footer"

        'Me.ph_header.Controls.Add(uc_header)
        'Me.ph_footer.Controls.Add(uc_footer)

        Me.Login()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ph_corrections.Visible = System.IO.File.Exists(Server.MapPath("~/Styles/CustomerCorrections.css"))
        Me.ph_fixfooter.Visible = Request.Browser.Browser <> "IE"
    End Sub

    Private Sub Login()
        
        Dim ll_resume As Boolean = True
        Dim lc_encrypted_login_code As String = Session("log_in_app_code")
        Dim lc_login_code As String = ""
        If Not String.IsNullOrEmpty(lc_encrypted_login_code) Then
            lc_login_code = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_encrypted_login_code))
            lc_login_code = lc_login_code.Replace("""", "")
        End If

        Dim lc_encrypted_login_password As String = Session("log_in_app_pass")
        Dim lc_login_password As String = ""
        If Not String.IsNullOrEmpty(lc_encrypted_login_code) Then
            lc_login_password = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_encrypted_login_password))
            lc_login_password = lc_login_password.Replace("""", "")
        End If

        Me.global_ws.user_information.logout()

        If ll_resume Then
            ' Controleer hier of de klanten voor klanten module beschikbaar is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_C4C_APP")

            If Not ll_resume Then
                Dim lc_module_no_available As String = global_trans.translate_message("message_c4c_module_not_available", "De module 'Klanten voor klant app' is niet beschikbaar.", Me.global_ws.Language)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('" + lc_module_no_available + "');", True)
            End If
        End If

        If ll_resume Then
            ' Probeer in te loggen
            ll_resume = Me.global_ws.user_information.login(0, lc_login_code, lc_login_password)

            If Not ll_resume Then
                If String.IsNullOrEmpty(lc_login_code) And Not String.IsNullOrEmpty(lc_login_password) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('code is empty');", True)
                ElseIf Not String.IsNullOrEmpty(lc_login_code) And String.IsNullOrEmpty(lc_login_password) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('pass is empty');", True)
                ElseIf String.IsNullOrEmpty(lc_login_code) And String.IsNullOrEmpty(lc_login_password) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('code and pass are empty');", True)
                End If
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('" + Me.global_ws.user_information.error_message + "');", True)
            End If
        End If

        If ll_resume Then
            ' Hier controleren we of een gebruiker geblokkeerd is of niet.
            ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers", "blocked", Me.global_ws.user_information.user_customer_id))

            If Not ll_resume Then
                Me.global_ws.user_information.logout()

                Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
            End If
        End If

        If ll_resume Then
            ' Hier controleren we of een gebruiker geblokkeerd is of niet.
            ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "blocked", global_ws.user_information.user_id))

            If Not ll_resume Then
                Me.global_ws.user_information.logout()

                Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
            End If
        End If
    End Sub
End Class

'http://webshop.utilizewebsolutions.nl/nl/account/account.aspx?LoginApp=True&LoginCode=ingrid&LoginPassword=dejong'
'http://webshop.utilizewebsolutions.nl/nl/account/account.aspx?LoginApp=True&LoginCode=ImluZ3JpZCI=&LoginPassword=ImRlam9uZyI=