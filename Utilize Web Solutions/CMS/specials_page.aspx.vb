﻿
Partial Class specials_page
    Inherits CMSPageTemplate

    Private specials_id As String = ""

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Request.Params("RecId") Is Nothing Then
            specials_id = Request.Params("RecId")
        End If

        Me.cms_template_code = Me.global_cms.get_cms_setting("specials_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_specials", "specials_title_" + Me.global_cms.Language, specials_id)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        If Not specials_id = "" Then
            Dim ph_specials_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

            Dim uc_specials_text As CMS.cms_user_control = LoadUserControl("Modules/Specials/uc_specials_text.ascx")
            uc_specials_text.set_property("block_id", specials_id)

            ph_specials_zone.Controls.Add(uc_specials_text)
        End If
    End Sub
End Class
