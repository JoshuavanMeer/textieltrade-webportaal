﻿
Partial Class photogallery_page
    Inherits CMSPageTemplate

    Private photogallery_id As String = ""
    Private number_of_columns_per_item As Integer = 0

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Me.get_page_parameter("RecId") Is Nothing Then
            photogallery_id = Request.Params("RecId")
        End If

        If Not Me.get_page_parameter("ColumnsPerItem") Is Nothing Then
            number_of_columns_per_item = CInt(Request.Params("ColumnsPerItem"))

            If number_of_columns_per_item = 0 Then
                number_of_columns_per_item = 2
            End If
        End If

        ' Sla de code van de template op
        Me.cms_template_code = Me.global_cms.get_cms_setting("news_template")

        ' Set the title of this page to the name of the photogallery title
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_photogallery", "title_" + Me.global_cms.Language, photogallery_id)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' When there is a photogallery id, then load photogallery details
        If Not photogallery_id = "" Then
            Dim ph_photogallery_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

            Dim uc_photogallery_details As CMS.cms_user_control = LoadUserControl("Modules/ImageGallery/uc_image_gallery_details.ascx")
            uc_photogallery_details.set_property("block_id", Utilize.Data.DataProcedures.GetValue("ucm_photogall_photos", "photogall_dir", photogallery_id))
            uc_photogallery_details.set_property("photogallery_id", photogallery_id)
            uc_photogallery_details.set_property("object_code", "uc_photogallery")

            ' Set the correct column tag of bootstrap grid
            uc_photogallery_details.set_property("item_size_int", number_of_columns_per_item)

            ph_photogallery_zone.Controls.Add(uc_photogallery_details)
        End If
    End Sub
End Class
