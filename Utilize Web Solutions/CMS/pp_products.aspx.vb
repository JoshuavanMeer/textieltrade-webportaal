﻿

<Serializable()>
Public Class pp_products
    Inherits CMSPageTemplate

    Private Function get_page_template(ByVal current_cat As String) As String
        Dim lc_page_template As String = ""

        If Not current_cat = "" Then
            Dim qc_select As New Utilize.Data.QuerySelectClass
            qc_select.select_fields = "rec_id, cat_parent, page_template"
            qc_select.select_from = "ucm_categories"
            qc_select.select_where = "rec_id = @rec_id"

            qc_select.add_parameter("rec_id", current_cat)


            Dim dt_data_table As System.Data.DataTable = qc_select.execute_query()

            If dt_data_table.Rows.Count > 0 Then
                Dim lc_cat_code As String = dt_data_table.Rows(0).Item("rec_id")
                Dim lc_parent_cat As String = dt_data_table.Rows(0).Item("cat_parent")

                If dt_data_table.Rows(0).Item("page_template").ToString.Trim() = "" Then
                    lc_page_template = Me.get_page_template(lc_parent_cat)
                Else
                    lc_page_template = dt_data_table.Rows(0).Item("page_template").ToString.Trim()
                End If
            End If
        Else
            lc_page_template = Me.global_cms.get_cms_setting("categories_template")
        End If

        Return lc_page_template
    End Function

    Private Function get_template_code() As String
        Dim lc_template_code As String = Me.global_cms.get_cms_setting("categories_template")
        Dim lc_prod_code As String = Me.get_page_parameter("prod_code")

        Dim lc_cat_code As String = Utilize.Data.DataProcedures.GetValue("ucm_categories_prod", "catcode", lc_prod_code, "prod_code")

        If Not lc_cat_code = "" Then
            lc_template_code = Me.get_page_template(lc_cat_code)
        End If

        Return lc_template_code
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_cat_code As String = Me.get_page_parameter("catcode")
        Dim lc_template_code As String = ""

        If Not lc_cat_code.Trim() = "" Then
            ' Vul de pagina template uit de categorieën
            lc_template_code = Me.get_page_template(lc_cat_code)
        End If

        If lc_template_code.Trim() = "" Then
            ' Gebruik de standaard pagina template
            lc_template_code = Me.global_cms.get_cms_setting("categories_template")
        End If

        ' Vul de pagina template
        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")

        If Me.get_page_parameter("catcode") = "" Then
            Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_label("label_product_presentation", "Productpresentatie", Me.global_ws.Language)
        Else
            Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_desc_" + Me.global_cms.Language, Me.get_page_parameter("catcode").Trim())
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim lc_cat_code As String = Me.get_page_parameter("CatCode")

        If Utilize.Data.DataProcedures.GetValue("ucm_categories", "page_type", lc_cat_code) = 2 Or Utilize.Data.DataProcedures.GetValue("ucm_categories", "rec_id", lc_cat_code, "cat_parent") = "" Then
            If global_ws.get_webshop_setting("product_list_type") = 2 Then
                ' Maak een nieuwe tekst blok aan.

                Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Cms/Modules/ProductPresentation/Products/uc_pp_products_overview.ascx")
                uc_block.set_property("block_id", lc_cat_code)
                uc_block.set_property("object_code", "ucm_categories")

                ph_target_block.Controls.Add(uc_block)
            Else
                ' Maak een nieuwe tekst blok aan.
                Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Cms/Modules/ProductPresentation/Products/uc_pp_products_overview.ascx")
                uc_block.set_property("block_id", lc_cat_code)
                uc_block.set_property("object_code", "ucm_categories")

                ph_target_block.Controls.Add(uc_block)
            End If

            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "setOverview", "setOverview();", True)
        Else
            ' Maak een nieuwe tekst blok aan.
            Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Cms/Modules/ProductPresentation/Categories/uc_pp_categories_overview.ascx")
            uc_block.set_property("block_id", lc_cat_code)
            uc_block.set_property("object_code", "ucm_categories")
            uc_block.set_property("item_size_int", 3)

            Try
                uc_block.set_property("item_size_int", CInt(Me.global_cms.get_cms_setting("cat_item_layout")))
            Catch ex As Exception

            End Try


            ph_target_block.Controls.Add(uc_block)
        End If

        If Not lc_cat_code = "" Then
            Dim lc_meta_desc As String = Utilize.Data.DataProcedures.GetValue("ucm_categories", "meta_desc_" + Me.global_cms.Language, lc_cat_code)

            Me.meta_description = IIf(String.IsNullOrEmpty(lc_meta_desc), Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_desc_" + Me.global_cms.Language, lc_cat_code), lc_meta_desc)
            Me.meta_keywords = Utilize.Data.DataProcedures.GetValue("ucm_categories", "meta_keyw_" + Me.global_cms.Language, lc_cat_code)
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Session("ProductPresentationPage") = Request.RawUrl.ToString()
        End If
    End Sub
End Class
