﻿
Partial Class utilize_calendar_form
    Inherits Utilize.Web.Solutions.Base.base_page_base

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_calendar") = False Then
            Return
        End If

        Dim header_block_id = Me.Request.Params("hdr")

        ' Voeg hier het formulier blok toe
        Dim control_form_block As Utilize.Web.Solutions.CMS.cms_user_control = loadcontrol("/CMS/Modules/Forms/uc_form_block.ascx")
        control_form_block.set_property("block_id", header_block_id)
        control_form_block.set_property("block_css", False)

        ' Haal de skin id op en zet deze wanneer deze niet leeg is
        Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "skin_id", Me.global_ws.get_webshop_setting("change_company_form"))
        control_form_block.set_property("skin", lc_skin_id)

        ph_blocks_1.Controls.Add(control_form_block)
    End Sub
End Class
