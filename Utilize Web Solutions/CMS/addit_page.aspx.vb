﻿
Partial Class CMS_addit_page
    Inherits CMSStructurePage

    Private rec_id As String = ""

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        rec_id = Me.get_page_parameter("RecId")

        ' Sla de code van de template op
        Me.cms_template_code = Utilize.Data.DataProcedures.GetValue("ucm_pages", "template_code", rec_id)

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_pages", "page_desc_" + Me.global_cms.Language, rec_id)
    End Sub

    
End Class
