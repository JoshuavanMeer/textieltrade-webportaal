﻿Partial Class CMS_utilize_calendar
    Inherits Utilize.Web.Solutions.Base.base_page_base

    Private Function GetResults(ByVal start_date As String, ByVal end_date As String, ByVal header_block_id As String) As String
        ' Fetch calendar rules
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "*,Format(start_date,'yyyy-MM-ddTHH:mm:ss') as startdate,Format(end_date,'yyyy-MM-ddTHH:mm:ss') as enddate"
        qsc_select.select_from = "ucm_calendar_lines"
        qsc_select.select_where = "CONVERT(date, start_date) >= @start_date And @end_date >= CONVERT(date, end_date) And hdr_id = @hdr_id"
        qsc_select.select_order = "start_date asc"
        qsc_select.add_parameter("start_date", start_date)
        qsc_select.add_parameter("end_date", end_date)
        qsc_select.add_parameter("hdr_id", header_block_id)

        Dim dt_calendar_lines As System.Data.DataTable = qsc_select.execute_query()

        ' Create calendar return result, if any available
        Dim str_result_list As String = ""
        str_result_list = str_result_list + "["

        Dim counter As Integer = 0
        Dim max_lines As Integer = dt_calendar_lines.Rows.Count - 1
        For Each calendar_line As System.Data.DataRow In dt_calendar_lines.Rows
            str_result_list = str_result_list + "{""title"": """ + calendar_line.Item("desc_" + Me.global_ws.Language).ToString + """, ""start"": """ + calendar_line.Item("startdate").ToString
            str_result_list = str_result_list + """, ""end"": """ + calendar_line.Item("enddate").ToString

            ' Set colors, if anything is set
            If Not String.IsNullOrEmpty(calendar_line.Item("bkg_color").ToString) Then
                str_result_list = str_result_list + """, ""color"": """ + calendar_line.Item("bkg_color").ToString
            End If
            If Not String.IsNullOrEmpty(calendar_line.Item("text_color").ToString) Then
                str_result_list = str_result_list + """, ""textColor"": """ + calendar_line.Item("text_color").ToString
            End If

            ' Fetch selected option
            Dim option_selected As Decimal = 0
            Try
                option_selected = Decimal.Parse(calendar_line.Item("opt_select").ToString)
            Catch ex As Exception
                option_selected = 0
            End Try

            ' Set correct details depending on selected option
            Dim calendar_line_content As String = calendar_line.Item("content_" + Me.global_ws.Language).ToString

            ' Sanitize html code (might need more in the future!)
            calendar_line_content = calendar_line_content.Replace(vbCrLf, "<br/>")
            calendar_line_content = calendar_line_content.Replace(vbLf, "")
            calendar_line_content = calendar_line_content.Replace(vbNewLine, "<br/>")
            calendar_line_content = calendar_line_content.Replace("""", "'")

            ' Set correct content or url
            Select Case option_selected
                Case 1
                    Dim form_url As String = Request.Url.AbsoluteUri.Split(".aspx")(0) + "_form.aspx?hdr=" + calendar_line.Item("form_id").ToString


                    ' If there is no content, then go direct to formfield
                    If String.IsNullOrEmpty(calendar_line_content) Then
                        str_result_list = str_result_list + """, ""urltoopen"": """ + form_url
                    Else
                        ' Otherwise incorporate it below the content.
                        str_result_list = str_result_list + """, ""content"": """ + calendar_line_content '+ "<br/>"
                        str_result_list = str_result_list + "<p>"
                        str_result_list = str_result_list + " <a href='" + form_url + "' target='_blank'>"
                        str_result_list = str_result_list + Utilize.Web.Solutions.Translations.global_trans.translate_label("label_calendar_goto_form", "Klik hier om naar het formulier te gaan.", Me.global_cms.Language).ToString
                        str_result_list = str_result_list + "</a>"
                        str_result_list = str_result_list + "</p>"
                    End If
                Case 2
                    Dim prod_code_url As String = Me.ResolveUrl("~/webshop/product_details.aspx?prod_code=" + calendar_line.Item("prod_code").ToString)

                    ' If there is no content, then go direct to product
                    If String.IsNullOrEmpty(calendar_line_content) Then
                        str_result_list = str_result_list + """, ""urltoopen"": """ + prod_code_url
                    Else
                        ' Otherwise incorporate it below the content.
                        str_result_list = str_result_list + """, ""content"": """ + calendar_line_content '+ "<br/>"
                        str_result_list = str_result_list + "<p>"
                        str_result_list = str_result_list + " <a href='" + prod_code_url + "' target='_blank'>"
                        str_result_list = str_result_list + Utilize.Web.Solutions.Translations.global_trans.translate_label("label_calendar_goto_prod_detail", "Klik hier om naar bestelscherm te gaan.", Me.global_cms.Language).ToString
                        str_result_list = str_result_list + "</a>"
                        str_result_list = str_result_list + "</p>"
                    End If
                Case 3
                    Dim external_url As String = calendar_line.Item("ext_url").ToString
                    ' If there is no content, then go direct to external url
                    If String.IsNullOrEmpty(calendar_line_content) Then
                        str_result_list = str_result_list + """, ""urltoopen"": """ + external_url
                    Else
                        ' Otherwise incorporate it below the content.
                        str_result_list = str_result_list + """, ""content"": """ + calendar_line_content '+ "<br/>"
                        str_result_list = str_result_list + "<p>"
                        str_result_list = str_result_list + Utilize.Web.Solutions.Translations.global_trans.translate_label("label_calendar_ext_url", "Externe url voor meer info:", Me.global_cms.Language).ToString
                        str_result_list = str_result_list + " <a href='" + external_url + "' target='_blank'>" + external_url + "</a>"

                        str_result_list = str_result_list + "</p>"
                    End If

                Case Else
                    ' Like 0 or others not mentioned above
                    str_result_list = str_result_list + """, ""content"": """ + calendar_line_content
            End Select

            If counter < max_lines Then
                str_result_list = str_result_list + """},"
            Else
                str_result_list = str_result_list + """}"
            End If

            counter = counter + 1
        Next

        str_result_list = str_result_list + "]"

        Return str_result_list
    End Function

    Protected Overloads Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim start_date = Me.Request.Params("start")
        Dim end_date = Me.Request.Params("end")
        Dim header_block_id = Me.Request.Params("hdr")

        Dim return_result As String = "[]"

        Try
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_calendar") = True Then
                If Not String.IsNullOrEmpty(start_date) And Not String.IsNullOrEmpty(end_date) Then
                    return_result = GetResults(start_date, end_date, header_block_id)
                End If
            End If
        Catch ex As Exception
            return_result = "[]"
        End Try

        Response.ContentType = "application/json"
        Response.Write(return_result)
        Response.End()
    End Sub

End Class
