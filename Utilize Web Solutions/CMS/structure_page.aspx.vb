﻿Imports System.Web.Services

Partial Class structure_page
    Inherits CMSStructurePage

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.global_ws.user_information.user_logged_on Then
            If Utilize.Data.DataProcedures.GetValue("ucm_structure", "show_type", Me.get_page_parameter("UsCode")) = 1 Then
                Response.Redirect("~/Home.aspx", True)
            End If
        End If
    End Sub
End Class
