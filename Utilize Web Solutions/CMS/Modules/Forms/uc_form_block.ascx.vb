﻿Imports System.Net.Mail
Imports System.Net.WebRequestMethods

Partial Class uc_form_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private captcha_image As New image
    Private captcha_textbox As New textbox

    Private dt_from_block As System.Data.DataTable
    Private dt_form_block_fields As System.Data.DataTable

    Private div_form As New HtmlGenericControl("div")
    Private span_error As New HtmlGenericControl("div")
    Private lt_content As New Utilize.Web.UI.WebControls.literal
    Private lt_response As New Utilize.Web.UI.WebControls.literal

    Private WithEvents button_send_form As New Utilize.Web.UI.WebControls.button

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            '  Controleer of de module banners beschikbaar is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_forms")
        End If

        If ll_resume Then
            Dim lc_title As String = Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "form_title_" + Me.global_cms.Language, Me.block_id)
            Me.ph_title.Visible = Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "form_title_avail", Me.block_id)

            If Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "form_title_h", Me.block_id) = 1 Then
                Me.lt_form_title.Text = lc_title
                Me.ph_h1.Visible = True
            Else
                Me.lt_form_title_2.Text = lc_title
                Me.ph_h2.Visible = True
            End If
        End If

            If ll_resume Then
            div_form.Attributes.Add("class", "")

            ' Voeg de controls toe
            Me.ph_mail_form.Controls.Add(lt_content)
            Me.ph_mail_form.Controls.Add(lt_response)
            Me.ph_mail_form.Controls.Add(div_form)

            ' Maak de controls zichtbaar
            Me.lt_content.Visible = True
            Me.lt_response.Visible = False
            Me.div_form.Visible = True
        End If

        If ll_resume Then
            ll_resume = Not Me.block_id = ""
        End If

        If ll_resume Then
            dt_from_block = New System.Data.DataTable

            ' Haal het formulier op
            Dim qsc_from_block As New Utilize.Data.QuerySelectClass
            With qsc_from_block
                .select_fields = "*"
                .select_from = "ucm_form_blocks"
                .select_order = "form_desc"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_from_block.add_parameter("rec_id", Me.block_id)

            dt_from_block = qsc_from_block.execute_query()

            ll_resume = dt_from_block.Rows.Count > 0
        End If

        If ll_resume Then
            Dim lc_content As String = dt_from_block.Rows(0).Item("form_content_" + Me.global_cms.Language)
            lt_content.Text = lc_content

            Dim lc_response As String = dt_from_block.Rows(0).Item("form_response_" + Me.global_cms.Language)
            lt_response.Text = lc_response

            Me.skin = dt_from_block.Rows(0).Item("skin_id")
        End If

        If ll_resume Then
            ' Haal de velden van het formulier op
            Dim qsc_form_block_fields As New Utilize.Data.QuerySelectClass
            With qsc_form_block_fields
                .select_fields = "*"
                .select_from = "ucm_form_fields"
                .select_order = "row_ord"
                .select_where = "hdr_id = @hdr_id"
            End With
            qsc_form_block_fields.add_parameter("hdr_id", Me.block_id)

            dt_form_block_fields = qsc_form_block_fields.execute_query()

            ll_resume = dt_form_block_fields.Rows.Count > 0
        End If

        Dim lo_fieldset As New HtmlGenericControl("div")
        lo_fieldset.Attributes.Add("class", "input-border")

        If ll_resume Then
            Me.div_form.Controls.Add(lo_fieldset)

            ' Loop door de regels heen
            For Each dr_form_block_fields As System.Data.DataRow In dt_form_block_fields.Rows
                Dim lc_field_type As String = dr_form_block_fields.Item("form_field_type")
                Dim ll_mandatory As Boolean = dr_form_block_fields.Item("form_field_mandatory")
                Dim ll_show_text As Boolean = dr_form_block_fields.Item("form_show_text")
                Dim lc_field_desc As String = dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language)
                Dim lc_rec_id As String = dr_form_block_fields.Item("rec_id")
                Dim ln_field_width As String = dr_form_block_fields.Item("form_field_width")
                Dim ln_text_width As String = dr_form_block_fields.Item("form_text_width")
                Dim lc_field_align As String = dr_form_block_fields.Item("form_field_align")

                Dim lo_section As New HtmlGenericControl("div")
                lo_section.Attributes.Add("class", "row sky-space-20")
                lo_fieldset.Controls.Add(lo_section)

                Dim lo_col As New HtmlGenericControl("div")
                lo_col.Attributes.Add("class", "col-md-8 col-md-offset-0 margin-bottom-15")
                lo_section.Controls.Add(lo_col)

                ' Maak de cellen voor de omschrijving, invulveld en verplichtindicatie aan
                Dim span_description As New HtmlGenericControl("label")
                lo_col.Controls.Add(span_description)

                Dim div_field As New HtmlGenericControl("div")
                lo_col.Controls.Add(div_field)

                Dim span_mandatory As New HtmlGenericControl("div")
                span_mandatory.Attributes.Add("class", "input-group-addon")

                Select Case lc_field_align
                    Case "LEFT"
                        div_field.Style.Add("text-align", "left")
                    Case "RIGHT"
                        div_field.Style.Add("text-align", "right")
                    Case "CENTER"
                        div_field.Style.Add("text-align", "center")
                    Case Else
                        Exit Select
                End Select

                Select Case lc_field_type
                    Case "TEXTBOX"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.textbox
                        With loControl
                            .Width = ln_field_width
                            .CssClass = "textbox form-control"
                            .ID = lc_rec_id
                        End With

                        If ll_mandatory Then
                            'loControl.Attributes.Add("Required", "")
                            div_field.Attributes.Add("class", "input-group")
                            Dim i_mandatory As New HtmlGenericControl("i")
                            i_mandatory.Attributes.Add("class", "icon-prepend fa fa-asterisk req")
                            span_mandatory.Controls.Add(i_mandatory)
                        Else
                            div_field.Attributes.Add("class", "")
                            span_mandatory.Visible = False
                        End If

                        div_field.Controls.Add(loControl)
                        div_field.Controls.Add(span_mandatory)
                    Case "TEXTEMAIL"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.textbox
                        With loControl
                            .Width = ln_field_width
                            .CssClass = "textbox form-control"
                            .ID = lc_rec_id
                        End With

                        If ll_mandatory Then
                            'loControl.Attributes.Add("Required", "")
                            div_field.Attributes.Add("class", "input-group")
                            Dim i_mandatory As New HtmlGenericControl("i")
                            i_mandatory.Attributes.Add("class", "icon-prepend fa fa-asterisk req")
                            span_mandatory.Controls.Add(i_mandatory)
                        Else
                            div_field.Attributes.Add("class", "")
                            span_mandatory.Visible = False
                        End If

                        div_field.Controls.Add(loControl)
                        div_field.Controls.Add(span_mandatory)
                    Case "TEXT"
                        lo_col.Attributes.Add("class", "col-md-12 col-md-offset-0 margin-bottom-15")

                        Dim lt_text As New Utilize.Web.UI.WebControls.literal
                        With lt_text
                            .Text = dr_form_block_fields.Item("form_text_" + Me.global_cms.Language)
                        End With

                        lo_col.Controls.Add(lt_text)

                        span_mandatory.Visible = False
                        span_description.Visible = False
                        div_field.Visible = False
                    Case "TEXTAREA"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.textarea
                        With loControl
                            .CssClass = "textarea form-control"
                        End With
                        div_field.Attributes.Add("class", "input-group")
                        div_field.Attributes.Add("class", div_field.Attributes.Item("class") + " fieldcontrol_txtarea")
                        loControl.ID = lc_rec_id

                        If ll_mandatory Then
                            'loControl.Attributes.Add("Required", "")
                            Dim i_mandatory As New HtmlGenericControl("i")
                            i_mandatory.Attributes.Add("class", "icon-prepend fa fa-asterisk req")
                            span_mandatory.Controls.Add(i_mandatory)
                        Else
                            div_field.Attributes.Add("class", "fieldcontrol_txtarea")
                            span_mandatory.Visible = False
                        End If
                        div_field.Controls.Add(loControl)
                        div_field.Controls.Add(span_mandatory)
                    Case "DROPDOWNLIST"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.dropdownlist
                        With loControl
                            .Width = ln_field_width
                            .DataValueField = "code"
                            .DataTextField = "description"
                        End With

                        Try
                            Dim lo_reader As New System.IO.StringReader(dr_form_block_fields.Item("form_field_values_" + Me.global_cms.Language))
                            Dim dt_data_table As New System.Data.DataSet
                            dt_data_table.ReadXml(lo_reader)
                            loControl.DataSource = dt_data_table
                            loControl.DataBind()
                        Catch ex As Exception

                        End Try

                        loControl.CssClass = "dropdownlist form-control"
                        loControl.ID = lc_rec_id
                        If ll_mandatory Then
                            'loControl.Attributes.Add("Required", "")
                            div_field.Attributes.Add("class", "input-group")
                            Dim i_mandatory As New HtmlGenericControl("i")
                            i_mandatory.Attributes.Add("class", "icon-prepend fa fa-asterisk req")
                            span_mandatory.Controls.Add(i_mandatory)
                        Else
                            div_field.Attributes.Add("class", "")
                            span_mandatory.Visible = False
                        End If

                        div_field.Controls.Add(loControl)
                        div_field.Controls.Add(span_mandatory)
                    Case "OPTIONGROUP"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.optiongroup
                        loControl.Width = ln_field_width
                        loControl.RepeatLayout = UI.WebControls.RepeatLayout.Flow
                        loControl.DataValueField = "code"
                        loControl.DataTextField = "description"
                        loControl.CssClass = "optiongroup"

                        Try
                            Dim lo_reader As New System.IO.StringReader(dr_form_block_fields.Item("form_field_values_" + Me.global_cms.Language))
                            Dim dt_data_table As New System.Data.DataSet
                            dt_data_table.ReadXml(lo_reader)

                            loControl.DataSource = dt_data_table
                            loControl.DataBind()
                        Catch ex As Exception

                        End Try

                        loControl.ID = lc_rec_id

                        If loControl.Items.Count > 0 Then
                            loControl.SelectedIndex = 0
                        End If

                        If ll_mandatory Then
                            'loControl.Attributes.Add("Required", "")
                        Else
                            div_field.Attributes.Add("class", "")
                            span_mandatory.Visible = False
                        End If

                        div_field.Controls.Add(loControl)
                    Case "CHECKBOX"
                        Dim loControl As New Utilize.Web.UI.WebControls.checkbox
                        loControl.ID = lc_rec_id
                        loControl.Text = lc_field_desc
                        loControl.CssClass = "check-box"
                        div_field.Controls.Add(loControl)
                    Case "ROW"
                        span_description.Visible = False
                        div_field.Visible = False
                        span_mandatory.Visible = False
                    Case "UPLOAD"
                        If ll_show_text Then
                            span_description.InnerHtml = lc_field_desc
                        End If

                        Dim loControl As New Utilize.Web.UI.WebControls.fileupload
                        loControl.CssClass = "form-control"
                        loControl.ID = lc_rec_id

                        If ll_mandatory Then
                            div_field.Attributes.Add("class", "input-group")
                            Dim i_mandatory As New HtmlGenericControl("i")
                            i_mandatory.Attributes.Add("class", "icon-prepend fa fa-asterisk req")
                            span_mandatory.Controls.Add(i_mandatory)
                        Else
                            div_field.Attributes.Add("class", "")
                            span_mandatory.Visible = False
                        End If

                        'Make sure we get back the filename at the time of saving
                        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Me.button_send_form)

                        div_field.Controls.Add(loControl)
                        div_field.Controls.Add(span_mandatory)
                    Case Else
                        Exit Select
                End Select
                ' Ga naar de volgende regel
            Next
        End If

        If ll_resume Then
            Dim captcha_available As Boolean = dt_from_block.Rows(0).Item("CAPTCHA_AVAIL")

            If captcha_available Then
                captcha_image.ID = "captchaimage"
                captcha_image.ImageUrl = "CaptchaImage.jpg"

                Dim lo_section As New HtmlGenericControl("div")
                lo_section.Attributes.Add("class", "row sky-space-20")
                lo_fieldset.Controls.Add(lo_section)

                Dim lo_col As New HtmlGenericControl("div")
                lo_col.Attributes.Add("class", "col-md-8 col-md-offset-0 margin-bottom-15")
                lo_section.Controls.Add(lo_col)

                ' Maak de cellen voor de omschrijving, invulveld en verplichtindicatie aan
                Dim span_description As New HtmlGenericControl("label")
                lo_col.Controls.Add(span_description)

                Dim div_field As New HtmlGenericControl("div")
                lo_col.Controls.Add(div_field)

                div_field.Controls.Add(captcha_image)
            End If

            If captcha_available Then
                captcha_textbox.Width = New UI.WebControls.Unit(30)
                captcha_textbox.Attributes.Add("placeholder", global_trans.translate_label("label_fill_captcha", "Vul de uitkomst van de bovenstaande berekening In", Me.global_cms.Language))

                captcha_textbox.CssClass = "textbox form-control"
                captcha_textbox.ID = "captchatext"

                Dim lo_section As New HtmlGenericControl("div")
                lo_section.Attributes.Add("class", "row sky-space-20")
                lo_fieldset.Controls.Add(lo_section)

                Dim lo_col As New HtmlGenericControl("div")
                lo_col.Attributes.Add("class", "col-md-8 col-md-offset-0 margin-bottom-15")
                lo_section.Controls.Add(lo_col)

                ' Maak de cellen voor de omschrijving, invulveld en verplichtindicatie aan
                Dim span_description As New HtmlGenericControl("label")
                lo_col.Controls.Add(span_description)

                Dim div_field As New HtmlGenericControl("div")
                lo_col.Controls.Add(div_field)

                div_field.Controls.Add(captcha_textbox)
            End If
        End If


        If ll_resume Then
            Dim lo_div_buttons As New HtmlGenericControl("div")
            lo_div_buttons.Attributes.Add("class", "buttons")
            lo_div_buttons.Controls.Add(button_send_form)

            ' Maak de versturen button op
            button_send_form.Text = global_trans.translate_button("button_send", "Versturen", Me.global_cms.Language)
            button_send_form.Attributes.Add("class", "btn-u btn-u-sea-shop btn-u-lg")
            button_send_form.ID = "button_send_form"

            ' Maak de cellen voor de omschrijving, invulveld en verplichtindicatie aan
            Dim span_description As New HtmlGenericControl("span")
            span_description.Attributes.Add("class", "description")

            Dim span_field As New HtmlGenericControl("span")
            span_description.Attributes.Add("class", "fieldcontrol")

            div_form.Controls.Add(lo_div_buttons)

            span_error.Attributes.Add("class", "message")
            div_form.Controls.Add(span_error)

            Dim lo_break3 As New literal
            lo_break3.Text = "<br style=""clear: both;"" />"
            div_form.Controls.Add(lo_break3)
        End If

        Me.Visible = ll_resume
        Me.set_style_sheet("uc_form_block.css", Me)
    End Sub

    Protected Sub button_send_form_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_send_form.Click
        Me.span_error.Visible = False
        Me.span_error.InnerHtml = ""

        ' Determine wether the form must be saved.
        Dim ll_save_form As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_forms_exp") And dt_from_block.Rows(0).Item("form_save")
        Dim lc_error_message As String = ""
        Dim lc_sender As String = ""

        ' Flag to determine wether mandatory fields are filled
        Dim ll_resume As Boolean = True

        Dim ll_testing As Boolean = True

        ' Empty string to fill in the form information
        Dim lc_form_content As String = ""
        Dim lc_mail_message As String = ""
        Dim lc_mail_attachment As String = ""
        Dim lc_mail_attachment_filename As String = ""

        If ll_testing Then
            ' Loop door de regels heen
            For Each dr_form_block_fields As System.Data.DataRow In dt_form_block_fields.Rows
                Dim lc_field_type As String = dr_form_block_fields.Item("form_field_type")
                Dim ll_mandatory As Boolean = dr_form_block_fields.Item("form_field_mandatory")

                Select Case lc_field_type
                    Case "TEXTBOX"
                        Dim loControl As Utilize.Web.UI.WebControls.textbox = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.Text.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        lc_form_content += loControl.Text + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.Text + "<br>"
                    Case "TEXTEMAIL"
                        Dim loControl As Utilize.Web.UI.WebControls.textbox = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.Text.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        If ll_resume And Not String.IsNullOrEmpty(loControl.Text.Trim()) Then
                            ' Controleer hier of het ingevulde emailadres geldig is.
                            Try
                                Dim email_address_check As MailAddress = New MailAddress(loControl.Text.Trim())

                                ll_resume = True
                            Catch ex As Exception
                                ll_testing = False
                                ll_resume = False
                            End Try

                            ' Toon een melding 
                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_email_address_incorrect", "Het ingevulde emailadres is niet correct", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        lc_form_content += loControl.Text + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.Text + "<br>"

                        If dt_from_block.Rows(0).Item("form_sender") = dr_form_block_fields.Item("rec_id") Then
                            lc_sender = loControl.Text
                        End If
                    Case "TEXTAREA"
                        Dim loControl As Utilize.Web.UI.WebControls.textarea = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.Text.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        lc_form_content += loControl.Text + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.Text + "<br>"
                    Case "DROPDOWNLIST"
                        Dim loControl As Utilize.Web.UI.WebControls.dropdownlist = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.SelectedValue.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        lc_form_content += loControl.SelectedValue + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.SelectedItem.Text + "<br>"
                    Case "OPTIONGROUP"
                        Dim loControl As Utilize.Web.UI.WebControls.optiongroup = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.SelectedValue.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        lc_form_content += loControl.SelectedValue + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.SelectedItem.Text + "<br>"
                    Case "CHECKBOX"
                        Dim loControl As Utilize.Web.UI.WebControls.checkbox = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        lc_form_content += loControl.Checked.ToString() + Chr(9)
                        lc_mail_message += dr_form_block_fields.Item("form_field_desc_" + Me.global_cms.Language) + ": " + loControl.Checked.ToString() + "<br>"
                    Case "UPLOAD"
                        Dim loControl As Utilize.Web.UI.WebControls.fileupload = Me.ph_mail_form.FindControl(dr_form_block_fields.Item("rec_id"))

                        If ll_mandatory Then
                            ll_resume = Not String.IsNullOrEmpty(loControl.FileName.Trim())

                            If Not ll_resume Then
                                ll_testing = False

                                lc_error_message = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language)
                                loControl.CssClass += " error"
                            Else
                                loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                            End If
                        End If

                        If ll_resume Then
                            'Is there any file selected
                            If ll_resume Then
                                ll_resume = loControl.HasFile And Not String.IsNullOrEmpty(loControl.FileName.Trim())
                            End If

                            'Is it a picture, pdf or office document file?
                            'TODO: It might be of security value to scan files on the content in the future as well, but that should be done correctly on one source location
                            If ll_resume Then
                                Dim lowered_filename = loControl.FileName.ToLower()
                                ll_testing = lowered_filename.EndsWith(".png") Or lowered_filename.EndsWith(".jpg") Or lowered_filename.EndsWith(".jpeg") Or lowered_filename.EndsWith(".pdf")
                                ll_testing = ll_testing Or lowered_filename.EndsWith(".xls") Or lowered_filename.EndsWith(".xlsx") Or lowered_filename.EndsWith(".doc") Or lowered_filename.EndsWith(".docx")
                                ll_testing = ll_testing Or lowered_filename.EndsWith(".ps") Or lowered_filename.EndsWith(".ods") Or lowered_filename.EndsWith(".odt")

                                If Not ll_resume Then
                                    ll_testing = False

                                    lc_error_message = global_trans.translate_message("MESSAGE_FILE_IMPORT_INCORRECT_EXTENSION", "Het geselecteerde bestand is geen png, jpg of jpeg afbeelding, pdf bestand of office document.", Me.global_cms.Language)
                                    loControl.CssClass += " error"
                                Else
                                    loControl.CssClass = loControl.CssClass.Replace("error", "no-error")
                                End If
                            End If

                            If ll_resume Then
                                Try
                                    If Not System.IO.Directory.Exists(Server.MapPath("~/Documents/Upload/")) Then
                                        System.IO.Directory.CreateDirectory(Server.MapPath("~/Documents/Upload/"))
                                    End If

                                    lc_mail_attachment = Server.MapPath("~/Documents/Upload/" + loControl.FileName)
                                    lc_mail_attachment_filename = loControl.FileName
                                    IO.File.WriteAllBytes(lc_mail_attachment, loControl.FileBytes)
                                Catch ex As Exception
                                    ll_testing = False
                                    lc_mail_attachment = ""
                                    lc_mail_attachment_filename = ""
                                    lc_error_message = global_trans.translate_message("message_mail_send_error", Me.global_cms.Language)
                                    ll_resume = False
                                End Try
                            End If
                        End If

                    Case Else
                        Exit Select
                End Select

                ' Ga naar de volgende regel
            Next
        End If

        'Check Captcha if any
        If ll_testing Then
            Dim captcha_available As Boolean = dt_from_block.Rows(0).Item("CAPTCHA_AVAIL")

            If captcha_available Then
                Dim originalCaptcha As String = System.Web.HttpContext.Current.Cache.Item("Captcha")

                ' Check orginal sum result of captcha and result entered in textbox
                If Not String.Equals(originalCaptcha, captcha_textbox.Text) Then
                    ll_testing = False
                    ll_resume = False

                    lc_error_message = global_trans.translate_message("MESSAGE_CAPTCHA_INCORRECT", "De captcha oplossing komt niet overeen met de gegeven versie.", Me.global_cms.Language)
                    captcha_textbox.CssClass += " error"
                Else
                    captcha_textbox.CssClass = captcha_textbox.CssClass.Replace("error", "no-error")
                End If
            End If
        End If

        ' Verstuur het ingevulde formulier per email.
        If ll_testing Then
            Dim lo_send_mail As New SendMail

            If Not String.IsNullOrEmpty(lc_sender.Trim()) Then
                lo_send_mail.mail_from = lc_sender
                lo_send_mail.mail_from_name = lc_sender
            End If

            If Me.global_ws.user_information.user_logged_on Then
                If Me.block_id = Me.global_ws.get_webshop_setting("change_company_form") Then
                    lo_send_mail.mail_subject = Me.global_ws.user_information.ubo_customer.field_get("av_nr") + ": " + global_trans.translate_label("lbl_email_change_company_details", "Wijziging bedrijfsgegevens", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
                Else
                    lo_send_mail.mail_subject = Me.global_ws.user_information.ubo_customer.field_get("av_nr") + ": " + global_trans.translate_label("lbl_email_submit_form", "Invulformulier", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
                End If
        Else
        lo_send_mail.mail_subject = global_trans.translate_label("lbl_email_submit_form", "Invulformulier", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
        End If

        lo_send_mail.mail_body = lc_mail_message + "<br><br>" + Me.get_ip_address()

        ' Controleer of er een afwijkend emailadres is vastgelegd in het invulformulier.
        Dim lc_email_to As String = dt_from_block.Rows(0).Item("eml_to_address")

        If Not String.IsNullOrEmpty(lc_email_to.Trim()) Then
            ' Zet het afwijkende email adres
            lo_send_mail.email_receiver = lc_email_to
        End If

        If Not String.IsNullOrEmpty(lc_mail_attachment) Then
            lo_send_mail.add_attachment(lc_mail_attachment, lc_mail_attachment_filename)
        End If

        ll_testing = lo_send_mail.send_mail()

        If Not ll_testing Then
            lc_error_message = global_trans.translate_message("message_mail_send_error", Me.global_cms.Language)
        End If
        End If

        ' Verstuur een bevestings email naar de verzender voor ontvangst formulier per email, indien gewenst en beschikbaar
        If ll_testing Then
            Dim lo_send_mail As New SendMail

            If Not String.IsNullOrEmpty(lc_sender.Trim()) And dt_from_block.Rows(0).Item("send_response") = True Then
                lo_send_mail.email_receiver = lc_sender

                If Me.global_ws.user_information.user_logged_on Then
                    If Me.block_id = Me.global_ws.get_webshop_setting("change_company_form") Then
                        lo_send_mail.mail_subject = Me.global_ws.user_information.ubo_customer.field_get("av_nr") + ": " + global_trans.translate_label("lbl_email_change_company_details", "Wijziging bedrijfsgegevens", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
                    Else
                        lo_send_mail.mail_subject = Me.global_ws.user_information.ubo_customer.field_get("av_nr") + ": " + global_trans.translate_label("lbl_email_submit_form", "Invulformulier", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
                    End If
        Else
            lo_send_mail.mail_subject = global_trans.translate_label("lbl_email_submit_form", "Invulformulier", Me.global_cms.Language) + ": " + dt_from_block.Rows(0).Item("form_desc")
        End If

        lo_send_mail.mail_body = dt_from_block.Rows(0).Item("form_response_" + Me.global_cms.Language) + "<br><br>" + lc_mail_message

        If Not String.IsNullOrEmpty(lc_mail_attachment) Then
            lo_send_mail.add_attachment(lc_mail_attachment, lc_mail_attachment_filename)
        End If

        ll_testing = lo_send_mail.send_mail()

        If Not ll_testing Then
            lc_error_message = global_trans.translate_message("message_mail_send_error", Me.global_cms.Language)
        End If
        End If
        End If

        ' Als de module om formulieren te bewaren beschikbaar is, sla hem dan op.
        If ll_testing And ll_save_form Then
            ' Create a new business object
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("ucm_forms")
            ubo_bus_obj.table_seek("ucm_form_blocks", Me.block_id)

            ' Insert a new row in the business object for table ucm_structure
            If ll_testing Then
                ll_testing = ubo_bus_obj.table_insert_row("ucm_form_file", Me.global_ws.user_information.user_id)
            End If

            If ll_testing Then
                ' Set the different field values
                ubo_bus_obj.field_set("ucm_form_file.form_content", lc_form_content)

                ' Save the changes
                ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
            End If

            If Not ll_testing Then
                ll_resume = False
                lc_error_message = global_trans.translate_message("message_form_save_error", "Er is een fout opgetreden bij het opslaan van het formulier.", Me.global_cms.Language)
            End If
        End If

        If ll_testing Then
            Me.div_form.Visible = False
            Me.lt_content.Visible = False
            Me.lt_response.Visible = True
        End If

        Me.span_error.Attributes.Add("class", "alert alert-danger")
        Me.span_error.InnerHtml = lc_error_message
        Me.span_error.Visible = Not ll_resume

        If ll_resume Then
            Response.Redirect(Me.ResolveCustomUrl("~/cms/FormResponse.aspx?id=" + Me.block_id))
        End If
    End Sub

    Private Function get_ip_address() As String
        Dim lc_ip_adress As String = ""

        If Not HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
            lc_ip_adress = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        ElseIf (HttpContext.Current.Request.UserHostAddress.Length <> 0) Then
            lc_ip_adress = HttpContext.Current.Request.UserHostAddress
        End If
        Return "Requested by ipadress: " + lc_ip_adress
    End Function
End Class
