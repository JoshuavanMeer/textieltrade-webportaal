﻿
Partial Class CMS_Modules_Forms_uc_form_response
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private form_id As String
    Private form_information As System.Data.DataRow

    Private Sub CMS_Modules_Forms_uc_form_response_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_form_response.css", Me)

        form_id = Me.get_page_parameter("id")
        If Not String.IsNullOrEmpty(form_id) Then
            form_information = Utilize.Data.DataProcedures.GetDataRow("ucm_form_blocks", form_id)

            If Not form_information Is Nothing Then
                Dim lc_title As String = form_information.Item("form_title_" + Me.global_cms.Language)
                Me.ph_title.Visible = form_information.Item("form_title_avail")

                If form_information.Item("form_title_h") = 1 Then
                    Me.lt_form_title.Text = lc_title
                    Me.ph_h1.Visible = True
                Else
                    Me.lt_form_title_2.Text = lc_title
                    Me.ph_h2.Visible = True
                End If

                Me.lt_response_message.Text = form_information.Item("form_response_" + Me.global_cms.Language)
            End If
        End If
    End Sub
End Class
