﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_form_response.ascx.vb" Inherits="CMS_Modules_Forms_uc_form_response" %>
<div class="uc_form_response">
    <utilize:placeholder runat="server" ID="ph_title">
        <utilize:placeholder runat="server" ID="ph_h1" Visible="false">
            <h1><utilize:literal runat="server" ID="lt_form_title"></utilize:literal></h1>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_h2" Visible="false">
            <h2><utilize:literal runat="server" ID="lt_form_title_2"></utilize:literal></h2>
        </utilize:placeholder>
    </utilize:placeholder>

    <div class="form-response">
        <utilize:literal runat="server" ID="lt_response_message"></utilize:literal>
    </div>
</div>