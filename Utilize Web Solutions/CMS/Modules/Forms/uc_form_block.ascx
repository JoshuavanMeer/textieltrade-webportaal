﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_form_block" CodeFile="uc_form_block.ascx.vb" Inherits="uc_form_block" %>
<utilize:panel ID="pnl_form" CssClass="uc_form_block" runat="server">
    <div class="uc_form_block">
        <utilize:placeholder runat="server" ID="ph_title">
            <utilize:placeholder runat="server" ID="ph_h1" Visible="false">
                <h1><utilize:literal runat="server" ID="lt_form_title"></utilize:literal></h1>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_h2" Visible="false">
                <h2><utilize:literal runat="server" ID="lt_form_title_2"></utilize:literal></h2>
            </utilize:placeholder>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_mail_form"></utilize:placeholder>
    </div>
</utilize:panel>