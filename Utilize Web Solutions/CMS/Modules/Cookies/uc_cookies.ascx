﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_cookies" CodeFile="uc_cookies.ascx.vb" Inherits="uc_cookies" %>

<div runat="server" ID="div_cookies_wrapper" class="uc_cookies">
    <utilize:placeholder runat="server" ID="ph_cookies_general" Visible="false">
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 cookies-wrapper">
                <utilize:translatetext runat="server" ID="text_cookies_message_header" Text="Deze website maakt gebruik van cookies, als je doorgaat met het gebruik van deze website stem je hiermee in" />
                <div class="cookie-bar-btns">                    
                    <utilize:translatebutton runat="server" ID="button_cookies_settings" Text="Instellingen" CssClass="btn-u btn-u-sea-shop btn-u-lg settings" /> 
                    <utilize:translatebutton runat="server" ID="button_cookies_accept" Text="Ja, ik accepteer cookies" CssClass="btn-u btn-u-sea-shop btn-u-lg confirm" />                                               
                </div>
              
            </div>
        </div>
        </div>
    </utilize:placeholder>
        
        <utilize:placeholder runat="server" ID="ph_cookies_settings" Visible="false">
       <div class="container"> 
            <div class="modal-body">
                <utilize:placeholder runat="server" ID="ph_reject_error" Visible="false">
                    <div class="row">
                    </div>
                </utilize:placeholder>
                <div class="row">
                    <div class="col-md-12">
                        <div class="body-text">
                          <h2><utilize:translatetitle runat="server" ID="title_cookies_settings" Text="Instellingen"></utilize:translatetitle></h2>  
                          <%--<utilize:literal runat="server" ID="lt_message" />--%>
                           <utilize:translatetext runat="server" ID="text_cookies_body_text" Text="U heeft de keuze uit de onderstaande pakketten. Het pakket Functionele en Analytische bestaat uit functionele en analytische cookies die standaard worden gebruikt. Deze gebruiken wij om de website goed te kunnen laten werken en om te analyseren hoe onze website wordt gebruikt, zodat we kunnen proberen de functionaliteit en effectiviteit van onze website te verbeteren."></utilize:translatetext>
                        </div>

                    </div>
                </div>
                <div runat="server" ID="div_column_wrapper" class="row column-wrapper">
                    <div class="col-sm-6 functional column-type">
                        <img src="/DefaultImages/Cookies/desktop_icon.png" />
                        <utilize:checkbox runat="server" ID="chk_functional" Checked="true" Enabled="false"></utilize:checkbox>
                        <div class="summary">
                            <utilize:translatetext runat="server" ID="text_summary_fuctional_cookies" Text="Noodzakelijke cookies helpen een website bruikbaarder te maken door basis functionaliteit als navigatie en toegang tot beveiligde gedeeltes van de website beschikbaar te maken. Zonder cookies kan deze site niet naar behoren werken"></utilize:translatetext>
                        </div>
                    </div>
                    <utilize:placeholder runat="server" ID="ph_marketing_cookies">
                        <div class="col-sm-6 marketing column-type">
                            <img src="/DefaultImages/Cookies/marketing_icon.png" />
                            <span class="checkbox">
                                <utilize:checkbox runat="server" ID="chk_marketing" Checked="false"></utilize:checkbox>
                            </span>
                            <div class="summary">
                                <utilize:translatetext runat="server" ID="text_summary_marketing_cookies" Text="Met analytische cookies geeft u ons toestemming om cookies te plaatsen voor meet doeleinden. Hiervoor maken wij dan gebruik van scripts afkomstig van derden zoals Facebook en LinkedIn."></utilize:translatetext>
                            </div>
                        </div>
                    </utilize:placeholder>
                </div>
            </div>
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_cancel" Text="Annuleren" CssClass="btn-u btn-u-sea-shop btn-u-lg decline" />
                <utilize:translatebutton runat="server" ID="button_accept_selection" Text="Bevestigen" CssClass="btn-u btn-u-sea-shop btn-u-lg confirm" />                
            </div>
        </div>
    </utilize:placeholder>

</div>