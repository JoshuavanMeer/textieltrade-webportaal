﻿
Partial Class uc_cookies
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private show_marketing_chk As Boolean = False
    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If Not String.IsNullOrEmpty(Me.global_cms.get_cms_setting("marketing_scripts")) Then
            show_marketing_chk = True
        End If

        Me.chk_functional.Text = global_trans.translate_label("lbl_cookie1", "Functionele'cookies", Me.global_ws.Language)
        Me.chk_marketing.Text = global_trans.translate_label("lbl_cookie2", "Analytische cookies", Me.global_ws.Language)

        If Not show_marketing_chk Then
            Me.ph_marketing_cookies.Visible = False
            Me.div_column_wrapper.Attributes.Add("class", "single-column " + Me.div_column_wrapper.Attributes("class"))
        End If

        ' Load the style sheet
        Me.set_style_sheet("uc_cookies.css", Me)

        If ll_testing Then
            ' Check if the module is active
            ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_cookies")

            ' Hide the user control if de module is not active
            If Not ll_testing Then
                Me.Visible = False
            Else
                Me.ph_cookies_general.Visible = True
            End If
        End If

        ' Get webshop setting to show the cookies or not
        If ll_testing Then
            Dim ll_show_cookies As Boolean = False

            ll_show_cookies = Me.global_ws.get_webshop_setting("show_cookie_message")

            ll_testing = ll_show_cookies

            If Not ll_testing Then
                Me.Visible = False
            End If
        End If

        ' Check if the cookie is present client side
        If ll_testing Then
            Dim lc_cookie_value As String = ""

            lc_cookie_value = Me.ReadCookie("utlz_user_cookie")

            If Not String.IsNullOrEmpty(lc_cookie_value) Then
                Me.Visible = False
            End If
        End If

        'Default selection. Should be always the max index
        If Not IsPostBack And show_marketing_chk Then
            Me.chk_marketing.Checked = True
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "isPostBack", String.Format("var isPostback = {0};", IsPostBack.ToString().ToLower()), True)
    End Sub

    Private Sub button_cookies_accept_Click(sender As Object, e As EventArgs) Handles button_cookies_accept.Click
        Dim ll_testing As Boolean = True
        Dim ln_expire_days As Integer = 0

        ' Get webshop setting how long a cookie must me saved
        ln_expire_days = Me.global_ws.get_webshop_setting("cookie_expire_days")

        ' Get the webshop url and replace the query from the url
        ' AbsluteUri: http://localhost:80/page.aspx?key=value
        ' PathAndQuery: /page.aspx?key=value
        Dim lc_cookie_value As String = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "")

        ' Save the cookie with the url as value
        Me.WriteCookie("utlz_user_cookie", lc_cookie_value, ln_expire_days)
        ' Write accept cookie
        Me.WriteCookie("utlz_accept_cookie", "True", ln_expire_days)

        ' Hide the user control
        Me.Visible = False
        ' reload page to refresh cookies with marketing cookies
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "reload_page", "$(document).ready(function () {location.reload();});", True)

    End Sub
    Private Sub button_cookies_settings_Click(sender As Object, e As EventArgs) Handles button_cookies_settings.Click
        Me.ph_cookies_general.Visible = False
        Me.ph_cookies_settings.Visible = True
        Me.div_cookies_wrapper.Attributes.Add("class", "view-settings " + Me.div_cookies_wrapper.Attributes("class"))
    End Sub
    Private Sub button_cancel_Click(sender As Object, e As EventArgs) Handles button_cancel.Click
        Dim ll_testing As Boolean = True
        Dim ln_expire_days As Integer = 0

        ' Get webshop setting how long a cookie must me saved
        ln_expire_days = Me.global_ws.get_webshop_setting("cookie_expire_days")

        ' Get the webshop url and replace the query from the url
        ' AbsluteUri: http://localhost:80/page.aspx?key=value
        ' PathAndQuery: /page.aspx?key=value
        Dim lc_cookie_value As String = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "")

        ' Save the cookie with the url as value
        Me.WriteCookie("utlz_user_cookie", lc_cookie_value, ln_expire_days)
        Me.WriteCookie("utlz_accept_cookie", "False", ln_expire_days)

        ' Hide the user control
        Me.Visible = False

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "reload_page", "$(document).ready(function () {location.reload();});", True)
    End Sub

    Private Sub button_accept_selection_Click(sender As Object, e As EventArgs) Handles button_accept_selection.Click
        Dim ll_testing As Boolean = True
        Dim ln_expire_days As Integer = 0

        ' Get webshop setting how long a cookie must me saved
        ln_expire_days = Me.global_ws.get_webshop_setting("cookie_expire_days")

        ' Get the webshop url and replace the query from the url
        ' AbsluteUri: http://localhost:80/page.aspx?key=value
        ' PathAndQuery: /page.aspx?key=value
        Dim lc_cookie_value As String = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "")

        ' Save the cookie with the url as value
        Me.WriteCookie("utlz_user_cookie", lc_cookie_value, ln_expire_days)
        ' Write accept cookie
        If show_marketing_chk And Me.chk_marketing.Checked Then
            Me.WriteCookie("utlz_accept_cookie", "True", ln_expire_days)
        Else
            Me.WriteCookie("utlz_accept_cookie", "False", ln_expire_days)
        End If

        ' Hide the user control
        Me.Visible = False
        ' reload page to refresh cookies with marketing cookies
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "reload_page", "$(document).ready(function () {location.reload();});", True)

    End Sub



#Region " Cookie management"
    Private Function ReadCookie(ByVal CookieName As String) As String
        Dim lcCookieValue As String = ""

        If Not HttpContext.Current.Request.Cookies(CookieName) Is Nothing Then
            lcCookieValue = HttpContext.Current.Request.Cookies(CookieName).Value

            If lcCookieValue Is Nothing Then
                lcCookieValue = ""
            End If
        End If

        Return lcCookieValue
    End Function

    Private Function WriteCookie(ByVal CookieName As String, ByVal CookieValue As String, ByVal Days As Integer) As Boolean
        If Not HttpContext.Current.Request.Cookies(CookieName) Is Nothing Then
            HttpContext.Current.Response.Cookies.Remove(CookieName)
        End If

        Dim myCookie As New HttpCookie(CookieName)
        'myCookie.Domain = GetCookieDomain()
        myCookie.Path = "/"
        myCookie.Expires = System.DateTime.Today.AddDays(Days)
        myCookie.Value = CookieValue
        HttpContext.Current.Response.Cookies.Add(myCookie)

        ' Als in de browser is ingesteld dat cookies niet mogen worden opgeslagen dan worden ze wel opgeslagen, maar daarna verwijderd.
        ' Er komt geen foutmelding voorbij.
        Return True
    End Function

    Private Shared Function GetCookieDomain() As String
        Return HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (IIf(HttpContext.Current.Request.Url.Port <> 80, ":" + HttpContext.Current.Request.Url.Port.ToString(), ""))
    End Function

    Private Sub RemoveCookie(ByVal CookieName As String)
        If Not Request.Cookies(CookieName) Is Nothing Then
            Response.Cookies(CookieName).Expires = DateTime.Now.AddDays(-1)
        End If
    End Sub
#End Region

End Class
