﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_news_text.ascx.vb" Inherits="uc_news_text" ClassName="uc_news_text" %>
<utilize:placeholder runat="server" ID="ph_news_text">
    <div class="uc_news_text news-v3 margin-bottom-30">
        <div class="news-v3-in">
            <ul class="list-inline posted-info">
                <li><utilize:literal runat="server" ID="lt_date"></utilize:literal></li>
            </ul>
	        <h2><utilize:literal runat="server" ID="lt_title"></utilize:literal></h2>
            <utilize:placeholder runat="server" ID="ph_news_image">
	            <p style="float: left;"><utilize:image runat="server" ID="img_news_image" CssClass="img-responsive default-image" /></p>
            </utilize:placeholder>
	        <utilize:literal runat="server" ID="lt_content"></utilize:literal>
        </div>
    </div>
</utilize:placeholder>