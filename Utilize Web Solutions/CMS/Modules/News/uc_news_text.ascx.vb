﻿
Partial Class uc_news_text
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _news_id As String = ""
    Public Property news_id As String
        Get
            Return _news_id
        End Get
        Set(value As String)
            _news_id = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_news_text.css", Me)

        Dim qsc_news As New Utilize.Data.QuerySelectClass
        qsc_news.select_fields = "*"
        qsc_news.select_from = "ucm_news"
        qsc_news.select_order = "row_ord"
        qsc_news.select_where = "rec_id = @rec_id"
        qsc_news.add_parameter("@rec_id", Me._news_id)

        Dim dt_news As System.Data.DataTable = qsc_news.execute_query()

        ' Bepaal of de control zichtbaar is op basis van het aantal regels
        Me.Visible = dt_news.Rows.Count > 0

        If Me.Visible Then
            Me.ph_news_image.Visible = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_image_available", dt_news.Rows(0).Item("hdr_id"))
            Me.img_news_image.ImageUrl = Me.ResolveCustomUrl("~/") + dt_news.Rows(0).Item("news_image")
            Me.lt_date.Text = Format(dt_news.Rows(0).Item("release_date"), "dd MMMM yyyy")
            Me.lt_title.Text = dt_news.Rows(0).Item("title_" + Me.global_cms.Language)
            Me.lt_content.Text = dt_news.Rows(0).Item("content_" + Me.global_cms.Language)
            Me.lt_content.Text = Me.lt_content.Text.Replace("<img", "<img class=""img-responsive""")
        End If
    End Sub
End Class
