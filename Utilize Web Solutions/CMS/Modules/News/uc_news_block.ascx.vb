﻿

<Serializable()>
Public Class uc_news_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    ' how many of the most recent entries will be shown , the default is 1
    Private _item_count As Integer = 1
    Public Property item_count() As Integer
        Get
            Return _item_count
        End Get
        Set(ByVal value As Integer)
            _item_count = value
        End Set
    End Property

    ' news_image_available
    Private _news_image_available As Boolean = False

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me._news_image_available = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_image_available", Me.block_id)
        End If

        If ll_testing Then
            Me.lt_news_title.Text = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_title_" + Me.global_cms.Language, Me.block_id)
            Me.ph_title.Visible = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_title_available", Me.block_id)
        End If

        If ll_testing Then
            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            qsc_data_table.select_fields = "top " + _item_count.ToString() + " * "
            qsc_data_table.select_from = "ucm_news"
            qsc_data_table.select_order = "release_date desc"
            qsc_data_table.select_where = "hdr_id = @hdr_id and release_date <= @release_date"
            qsc_data_table.add_parameter("release_date", Date.Now)
            qsc_data_table.add_parameter("@hdr_id", Me.block_id)

            Dim dt_data_table As System.Data.DataTable = qsc_data_table.execute_query()

            Me.rpt_newsitems.DataSource = dt_data_table
            Me.rpt_newsitems.DataBind()

            ll_testing = dt_data_table.Rows.Count > 0

            If Not ll_testing Then
                Me.Visible = False
            End If
        End If

        If ll_testing Then
            Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "skin_id", Me.block_id)

            If Not lc_skin_id = "" Then
                Me.skin = lc_skin_id
            End If

            Me.set_style_sheet("uc_news_block.css", Me)
        End If
    End Sub

    Protected Sub rpt_newsitems_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_newsitems.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim ph_image As placeholder = e.Item.FindControl("ph_image")
            ph_image.Visible = Me._news_image_available
        End If
    End Sub
End Class