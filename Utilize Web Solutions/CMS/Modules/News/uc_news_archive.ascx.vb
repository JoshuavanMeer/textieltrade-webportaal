﻿

<Serializable()>
Public Class uc_news_archive
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Dim dt_source As System.Data.DataTable = Nothing
    Dim dt_target As System.Data.DataTable = Nothing

    Private _page_nr As Integer = 0 ' Huidig paginanummer
    Private _page_item_count As Integer = 18 ' Aantal items per pagina

    Private _page_count As Integer = 0 ' Aantal pagina's
    Private _page_nr_count As Integer = 10 ' Aantal pagina's om te tonen
    Private _page_nr_endcount As Integer = 2 ' Aantal pagina's op het eind

    Private ReadOnly Property _page_name As String
        Get
            If Me.lt_raw_url.Text.Contains("&") Then
                Return Me.lt_raw_url.Text.Trim() + "&"
            Else
                Return Me.lt_raw_url.Text.Trim() + "?"
            End If
        End Get
    End Property

    Private Function get_page_start(ByVal page_nr As Integer) As Integer
        Dim ln_page_start As Integer = 0

        If page_nr < _page_nr_count Then
            ln_page_start = 0
        Else
            ln_page_start = System.Math.Floor(page_nr / _page_nr_count) * _page_nr_count
        End If

        Return ln_page_start
    End Function
    Private Function get_page_end(ByVal page_nr As Integer) As Integer
        Dim ln_page_end As Integer = 0

        If page_nr = 0 Then
            ln_page_end = _page_nr_count

            If _page_count < _page_nr_count Then
                ln_page_end = _page_count
            End If

            If page_nr = _page_count - 1 Then
                ln_page_end = _page_count
            End If

            If _page_count = 0 Then
                ln_page_end = 0
            End If
        Else
            ln_page_end = System.Math.Ceiling((page_nr + 1) / _page_nr_count) * _page_nr_count

            If ln_page_end > System.Math.Ceiling(dt_source.Rows.Count / _page_item_count) Then
                ln_page_end = System.Math.Ceiling(dt_source.Rows.Count / _page_item_count)
            End If
        End If

        Return ln_page_end
    End Function

    Private Sub set_pager_variables()
        dt_target = dt_source.Clone

        ' Probeer het paginanummer uit te lezen
        If Not Me.get_page_parameter("page") = "" Then
            Try
                _page_nr = CInt(Me.get_page_parameter("page"))
            Catch ex As Exception
                _page_nr = 0
            End Try
        End If

        Dim ln_page_start As Integer = _page_nr * _page_item_count
        Dim ln_page_end As Integer = (_page_nr + 1) * _page_item_count

        If ln_page_end > dt_source.Rows.Count Then
            ln_page_end = dt_source.Rows.Count
        End If

        For i As Integer = ln_page_start To ln_page_end - 1
            dt_target.ImportRow(dt_source.Rows(i))
        Next

        dt_target.AcceptChanges()
        _page_count = System.Math.Ceiling(dt_source.Rows.Count / _page_item_count)
    End Sub
    Private Sub set_pager_navigation()
        Dim page_start As Integer = Me.get_page_start(_page_nr)
        Dim page_end As Integer = Me.get_page_end(_page_nr)

        Dim lc_pager_string As String = ""

        ' Bouw hier de standaard pagina's op
        For page_iterations As Integer = page_start To page_end - 1
            ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
            Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
            url_class.set_parameter("pagesize", Me._page_item_count)
            url_class.set_parameter("page", page_iterations)
            Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

            Me.set_page_parameter("page", page_iterations)

            Dim lc_css_class As String = ""
            If page_iterations = _page_nr Then
                lc_css_class = "active"
            End If
            lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url + """>" + (page_iterations + 1).ToString() + "</a></li>"
        Next

        ' Zet hier de laatste pagina's op het eind
        If page_end - _page_nr_endcount > _page_nr_endcount And Not (page_start + _page_nr_count) > _page_count Then
            For i As Integer = 1 To _page_nr_endcount
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", i)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                If i = 1 Then
                    lc_pager_string += " .. "
                End If

                lc_pager_string += "<li><a href=""" + lc_url + """>" + (_page_count - _page_nr_endcount + i).ToString() + "</a></li>"
            Next
        End If

        Me.lt_pager_1.Text = lc_pager_string
        Me.lt_pager_2.Text = lc_pager_string

        If _page_nr > 0 Then
            Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
            url_class.set_parameter("pagesize", Me._page_item_count)
            url_class.set_parameter("page", _page_nr - 1)
            Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

            Me.set_page_parameter("page", _page_nr - 1)

            Me.hl_pl_previous1.NavigateUrl = lc_url
            Me.hl_pl_previous1.CssClass = "previous"

            Me.hl_pl_previous2.NavigateUrl = lc_url
            Me.hl_pl_previous2.CssClass = "previous"
        Else
            Me.hl_pl_previous1.NavigateUrl = ""
            Me.hl_pl_previous1.CssClass = "previous hide_me"

            Me.hl_pl_previous2.NavigateUrl = ""
            Me.hl_pl_previous2.CssClass = "previous hide_me"
        End If

        If _page_nr < (_page_count - 1) Then
            Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
            url_class.set_parameter("pagesize", Me._page_item_count)
            url_class.set_parameter("page", _page_nr + 1)
            Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

            Me.hl_pl_next1.NavigateUrl = lc_url
            Me.hl_pl_next1.CssClass = "next"

            Me.hl_pl_next2.NavigateUrl = lc_url
            Me.hl_pl_next2.CssClass = "next"
        Else
            Me.hl_pl_next1.NavigateUrl = ""
            Me.hl_pl_next1.CssClass = "next hide_me"

            Me.hl_pl_next2.NavigateUrl = ""
            Me.hl_pl_next2.CssClass = "next hide_me"
        End If

        If Me._page_count = 1 Then
            lt_pager_1.Visible = False
            lt_pager_2.Visible = False
        End If
    End Sub

    ' news_image_available
    Private _news_image_available As Boolean = False

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If Not Me.IsPostBack() Then
            Me.lt_raw_url.Text = Request.RawUrl.ToString()
        End If

        ' Calculation how many items on a page depended on the item layout setting of the news block
        If ll_testing Then
            ' If item layout: no columns is selected
            If Me.item_size_int = 0 Then
                ' Set items to 12 because 1 item a row is shown
                Me.item_size_int = 12
            End If

            ' The number of items is 12 (max number of bootstrap rows) / item layout eg. 6 columns) * 4 rows max
            Me._page_item_count = 12 / Me.item_size_int * 4
        End If

        If ll_testing Then
            Me.li_title.Text = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_title_" + Me.global_cms.Language, Me.block_id)
            Me.ph_news_title.Visible = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_title_available", Me.block_id)
        End If

        If ll_testing Then
            Me._news_image_available = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "news_image_available", Me.block_id)
        End If

        If ll_testing Then
            Dim qsc_source As Utilize.Data.QuerySelectClass = New Utilize.Data.QuerySelectClass
            qsc_source.select_fields = "*"
            qsc_source.select_from = "ucm_news"
            qsc_source.select_where = "hdr_id = @hdr_id and release_date < @release_date"
            qsc_source.add_parameter("release_date", Date.Now)
            qsc_source.add_parameter("hdr_id", Me.block_id)
            qsc_source.select_order = "release_date desc"

            dt_source = qsc_source.execute_query()

            Me.set_pager_variables()
            Me.set_pager_navigation()

            Me.rpt_newsitems.DataSource = dt_target
            Me.rpt_newsitems.DataBind()

            ll_testing = Me.rpt_newsitems.Items.Count > 0
        End If

        If ll_testing Then
            Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_news_blocks", "skin_id", Me.block_id)

            If Not lc_skin_id = "" Then
                Me.skin = lc_skin_id
            End If

            Me.set_style_sheet("uc_news_archive.css", Me)
        End If

        If Not ll_testing Then
            Me.Visible = False
        End If
    End Sub

    Protected Sub rpt_newsitems_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_newsitems.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim ph_image As placeholder = e.Item.FindControl("ph_image")
            ph_image.Visible = Me._news_image_available
        End If
    End Sub
End Class