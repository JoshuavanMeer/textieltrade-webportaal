﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_news_archive.ascx.vb" ClassName="uc_news_archive" Inherits="uc_news_archive" %>
<utilize:literal runat="server" id="lt_raw_url" text="" visible="false"></utilize:literal>
<div class="row margin-bottom-20 uc_news_archive"> 
    <utilize:placeholder runat="server" ID="ph_news_title">
        <h2><utilize:literal runat="server" ID="li_title"></utilize:literal> </h2>
    </utilize:placeholder>
    <utilize:panel runat="server" ID="pnl_news_archive" CssClass="uc_news_archive">
        <span class="showing_count"><utilize:literal runat='server' ID="lt_product_count1"></utilize:literal></span>

        <div class="text-center">
            <ul class="pagination">
                <li><asp:HyperLink ID="hl_pl_previous1" CssClass="previous" runat="server"><utilize:translatelabel runat='server' ID="label_product_list_previous1"></utilize:translatelabel></asp:HyperLink></li>          
                <utilize:literal runat='server' ID="lt_pager_1"></utilize:literal>
                <li><asp:HyperLink ID="hl_pl_next1" CssClass="next"  runat="server"><utilize:translatelabel runat='server' ID="label_product_list_next1"></utilize:translatelabel></asp:HyperLink></li>
            </ul>
        </div>

        <div class="news-row row">
        <utilize:repeater runat='server' ID="rpt_newsitems">
            <ItemTemplate>
                <!-- News v3 -->
                <div class='<%# Me.item_size%>'>
		<div class="news_block" onclick="window.location='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_"+ Me.global_cms.Language)) %>'">
                    <div class="img-center">
                        <utilize:placeholder runat="server" ID="ph_image">
                            <a href='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_"+ Me.global_cms.Language)) %>'><utilize:image runat="server" ID="img_news_image" CssClass="img-responsive newsimage" ImageUrl='<%# Me.ResolveCustomUrl("~/") + DataBinder.Eval(Container.DataItem, "news_image")%>' /></a>
                        </utilize:placeholder>
                    </div>
                    <div>
                        <div class="desc">
                            <h4><a href='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_"+ Me.global_cms.Language)) %>'>
                                <%# DataBinder.Eval(Container.DataItem, "title_" + Me.global_cms.Language) %>
                            </a></h4>
                            <utilize:label runat="server" ID="lbl_news_date"><%# Format(DataBinder.Eval(Container.DataItem, "release_date"), "dd MMMM yyyy") %></utilize:label>
                            <p><utilize:literal runat="server" ID="lt_news_short_desc" Text='<%# DataBinder.Eval(Container.DataItem, "desc_"+ Me.global_cms.Language).replace(chr(10), "<br>") %>'></utilize:literal>&nbsp;<utilize:hyperlink runat="server" ID="hl_read_more" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_"+ Me.global_cms.Language)) %>'><utilize:translatelabel runat="server" id="link_read_more" text="Lees meer"></utilize:translatelabel></utilize:hyperlink></p>
                        </div>
                    </div>
                </div>
                </div>
                <!-- End News v3 -->
                <utilize:placeholder runat="server" ID="ph_clearfix" Visible="<%# me.clearfix() %>">
                    <div class="customclearfix margin-bottom-20"></div>
                </utilize:placeholder>            
            </ItemTemplate>

        </utilize:repeater>
        </div>

        <div class="text-center">
            <ul class="pagination">
                <li><asp:HyperLink ID="hl_pl_previous2" CssClass="previous" runat="server"><utilize:translatelabel runat='server' ID="label_product_list_previous"></utilize:translatelabel></asp:HyperLink></li>
                <utilize:literal runat='server' ID="lt_pager_2"></utilize:literal>
                <li><asp:HyperLink ID="hl_pl_next2" CssClass="next"  runat="server"><utilize:translatelabel runat='server' ID="label_product_list_next"></utilize:translatelabel></asp:HyperLink></li>
            </ul>
        </div>
    </utilize:panel>
</div>