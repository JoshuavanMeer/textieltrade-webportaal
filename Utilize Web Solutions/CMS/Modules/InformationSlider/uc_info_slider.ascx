﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_info_slider.ascx.vb" Inherits="CMS_Modules_Slider_uc_slider" %>

<utilize:panel runat="server" ID="pnl_infoslider" CssClass="uc_slider margin-bottom-20">
    <div id="slider_contentsdgsd" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#slider_content" data-slide-to="0" class="active"></li>
        <li data-target="#slider_content" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <utilize:repeater runat="server" ID="rpt_items">
            <ItemTemplate>
                <utilize:panel runat="server" ID="pnl_item" CssClass="item">
                    <%# DataBinder.Eval(Container.DataItem, "text_" + Me.global_cms.Language)%>
                </utilize:panel>
            </ItemTemplate>
        </utilize:repeater>
      </div>
    </div>
</utilize:panel>