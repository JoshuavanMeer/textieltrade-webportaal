﻿
Partial Class CMS_Modules_Slider_uc_slider
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _sliderwidth As Integer = 0
    Public Property sliderwidth() As Integer
        Get
            Return _sliderwidth
        End Get
        Set(value As Integer)
            _sliderwidth = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        Dim qsc_slider As New Utilize.Data.QuerySelectClass
        Dim dt_slider As New System.Data.DataTable

        If ll_testing Then

            qsc_slider.select_fields = "*"
            qsc_slider.select_from = "UCM_INFSLIDER_BLOCKS"
            qsc_slider.select_order = "slider_desc"
            qsc_slider.select_where = "rec_id = @rec_id"
            qsc_slider.add_parameter("rec_id", Me.block_id)

            dt_slider = qsc_slider.execute_query()

            ll_testing = dt_slider.Rows.Count > 0
        End If

        Dim qsc_banners As New Utilize.Data.QuerySelectClass
        Dim dt_banners As New System.Data.DataTable

        If ll_testing Then

            qsc_banners.select_fields = "rank() OVER (ORDER BY row_ord) as row_number, *"
            qsc_banners.select_from = "UCM_INFO_SLIDER"
            qsc_banners.select_order = "row_ord"
            qsc_banners.select_where = "hdr_id = @hdr_id"
            qsc_banners.add_parameter("hdr_id", Me.block_id)

            dt_banners = qsc_banners.execute_query()

            ll_testing = dt_banners.Rows.Count > 0
        End If

        If ll_testing Then
            ' De control is zichtbaar
            Me.Visible = True

            Me.set_style_sheet("uc_info_slider.css", Me)
        End If

        If ll_testing Then
            ' Bouw hier de html op
            Me.rpt_items.DataSource = dt_banners
            Me.rpt_items.DataBind()
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        ' When the carousel is loaded, the first item must be set to active for bootstrap to work
        If Me.rpt_items.Items.Count > 0 Then
            CType(Me.rpt_items.Items(0).FindControl("pnl_item"), panel).CssClass = "item active"
        End If
    End Sub
End Class
