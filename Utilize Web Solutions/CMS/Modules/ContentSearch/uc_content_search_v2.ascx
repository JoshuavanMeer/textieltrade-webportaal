﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search_v2.ascx.vb" Inherits="uc_content_search" %>

<div class="search-open uc_content_search_v2" style="display: none;">
    <utilize:panel runat="server" ID="pnl_search" DefaultButton="button_search" CssClass="uc_content_search">
        <h2><utilize:translatetitle runat="server" ID="title_basic_search" Text="Zoeken"></utilize:translatetitle></h2>     
            <div class="col-md-12 no-margin-right margin-bottom-10">
                <div class="animated fadeInDown">
                    <utilize:optiongroup runat="server" ID="opg_search_type" RepeatDirection="Horizontal" CssClass="searchtype input-group-addon">
                        <asp:ListItem Value="WEBSITE"></asp:ListItem>
                        <asp:ListItem Value="WEBSHOP"></asp:ListItem>
                    </utilize:optiongroup>
                </div>
            </div>
        <div class="col-md-12 no-margin-right">
            <div class="input-group animated fadeInDown">
                <utilize:textbox runat="server" ID="txt_search" CssClass="form-control"></utilize:textbox>
                <span class="input-group-btn">
                    <utilize:button ID="button_search" runat="server" class="search_button" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg" Text="Go"></utilize:button>
                </span>
            </div>           
        </div>        
            
            
    </utilize:panel>
</div>
