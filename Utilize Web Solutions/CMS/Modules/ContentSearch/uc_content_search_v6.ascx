﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search_v6.ascx.vb" Inherits="uc_content_search_v6" %>

    <utilize:panel runat="server" ID="pnl_search" DefaultButton="button_search" CssClass="show_search_box uc_content_search_v6">
        <utilize:panel runat="server" ID="pnl_search_type">
            <utilize:optiongroup runat="server" ID="opg_search_type" RepeatDirection="Horizontal" CssClass="searchtype input-group-addon">
                <asp:ListItem Value="WEBSITE"></asp:ListItem>
                <asp:ListItem Value="WEBSHOP"></asp:ListItem>
            </utilize:optiongroup>
        </utilize:panel>
                <utilize:textbox runat="server" ID="txt_search" CssClass="form-control"></utilize:textbox>
                <utilize:button ID="button_search" runat="server" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg searchbutton" Text="Go"></utilize:button>
        <i class="fi-search-icon"></i>
    </utilize:panel>