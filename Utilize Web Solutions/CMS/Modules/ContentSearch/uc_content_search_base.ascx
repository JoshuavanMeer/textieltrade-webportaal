﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search_base.ascx.vb" Inherits="uc_content_search" %>

<div class="uc_content_search_base">
    <utilize:panel runat="server" ID="pnl_search" DefaultButton="button_search">
        <div class="col-md-12">
            <utilize:optiongroup runat="server" ID="opg_search_type" RepeatDirection="Horizontal" CssClass="searchtype input-group-addon">
                <asp:ListItem Value="WEBSITE"></asp:ListItem>
                <asp:ListItem Value="WEBSHOP"></asp:ListItem>
            </utilize:optiongroup>
        </div>
        <div class="col-md-12">
            <div class="input-group">
                <utilize:textbox runat="server" ID="txt_search" CssClass="form-control"></utilize:textbox>

                <utilize:linkbutton ID="button_search" runat="server" class="input-group-addon">
                    <i class="glyphicon glyphicon-search"></i>
                </utilize:linkbutton>
            </div>
        </div>
                 
    </utilize:panel>
</div>