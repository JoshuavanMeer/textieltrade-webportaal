﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search_v5.ascx.vb" Inherits="uc_content_search_v5" %>

    <utilize:panel runat="server" ID="pnl_search" DefaultButton="button_search" CssClass="show_search_box uc_content_search_v5">
        <utilize:panel runat="server" ID="pnl_search_type" class="col-md-12 col-lg-4">
            <utilize:optiongroup runat="server" ID="opg_search_type" RepeatDirection="Horizontal" CssClass="searchtype input-group-addon">
                <asp:ListItem Value="WEBSITE"></asp:ListItem>
                <asp:ListItem Value="WEBSHOP"></asp:ListItem>
            </utilize:optiongroup>
        </utilize:panel>
        <div class="<%=Me.get_class() %>">
            <div class="input-group">
                <utilize:textbox runat="server" ID="txt_search" CssClass="form-control"></utilize:textbox>
                <span class="input-group-btn">
                    <utilize:button ID="button_search" runat="server" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg searchbutton" Text="Go"></utilize:button>
                </span>
            </div>           
        </div>
    </utilize:panel>
