﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search_results.ascx.vb" Inherits="CMS_Modules_ContentSearch_uc_content_search_results" %>

<div class="uc_content_search_results">
    <utilize:placeholder ID="pnl_results" runat="server" Visible="false">
        <utilize:repeater runat="server" ID="rpt_items"><ItemTemplate>
                <p><a class="result_link" href='<%# Me.ResolveCustomUrl( DataBinder.Eval(Container.DataItem, "url"))%>'><%# DataBinder.Eval(Container.DataItem, "title")%></a></p><span class="result_text"><%# DataBinder.Eval(Container.DataItem, "short_text")%></span>
            </ItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </utilize:repeater>
    </utilize:placeholder>
    <utilize:placeholder ID="pnl_no_results" runat="server" Visible="false">
        <utilize:translatetext ID="text_no_search_results" Text="Er zijn geen resultaten gevonden binnen uw zoekopdracht" runat="server"></utilize:translatetext>
    </utilize:placeholder>
</div>
