﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_content_search.ascx.vb" Inherits="uc_content_search" %>

<div class="search-open uc_content_search">
    <div class="container">
        <utilize:panel runat="server" ID="pnl_search" DefaultButton="button_search" CssClass="row uc_content_search">
            <h2><utilize:translatetitle runat="server" ID="title_basic_search" Text="Zoeken"></utilize:translatetitle></h2>

            <div class="row">
                <div class="col-md-3 removepadding" >
                    <utilize:optiongroup runat="server" ID="opg_search_type" RepeatDirection="Horizontal" CssClass="searchtype input-group-addon">
                        <asp:ListItem Value="WEBSITE"></asp:ListItem>
                        <asp:ListItem Value="WEBSHOP"></asp:ListItem>
                    </utilize:optiongroup>
                </div>
                <div class="col-md-9">
                    <utilize:textbox runat="server" ID="txt_search" CssClass="form-control"></utilize:textbox>
                    <div class="search-close"><i class="icon-close"></i></div>
                </div>
            </div>
            <utilize:linkbutton ID="button_search" runat="server" class="search_button"></utilize:linkbutton>
            
        </utilize:panel>
    </div>    
</div>