﻿Partial Class CMS_Modules_ContentSearch_uc_content_search_results
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private dt_search As System.Data.DataTable = Nothing
    Private qc_search As New Utilize.Data.QueryCustomSelectClass

    Private _minimum_characters As Integer = 1
    Public Property minimum_characters() As Integer
        Get
            Return _minimum_characters
        End Get
        Set(ByVal value As Integer)
            _minimum_characters = value
        End Set
    End Property

    Private _show_results As Boolean = False
    Public Property show_results() As Boolean
        Get
            Return _show_results
        End Get
        Set(ByVal value As Boolean)
            _show_results = value
        End Set
    End Property


    Private _max_length As Integer = 150
    Public Property max_length() As Integer
        Get
            Return _max_length
        End Get
        Set(ByVal value As Integer)
            _max_length = value
        End Set
    End Property

    Protected Function utilize_substring(ByVal input_string As String, ByVal start_position As Integer, Optional ByVal end_position As Integer = -1) As String
        Dim result As String = input_string
        If end_position = -1 Or end_position > result.Length Then
            end_position = result.Length
        End If
        If start_position > result.Length Then
            Return ""
        End If
        Return result.Substring(start_position, end_position)
    End Function
    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_content_search_results.css", Me)

        Dim keywords As String = ""
        If Not Request.Params("keywords") Is Nothing And Not Request.Params("keywords") = "" Then
            keywords = Request.Params("keywords")

            Dim ln_minimum_characters = Me.global_ws.get_webshop_setting("search_count")

            If ln_minimum_characters < 0 Then
                ln_minimum_characters = 0
            End If

            If Not (keywords.Length >= ln_minimum_characters) and Not String.IsNullOrEmpty(keywords) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "search_text", "alert('" + global_trans.translate_message("message_search_text", "Uw zoekterm moet uit minimaal [1] tekens bestaan.", Me.global_ws.Language).Replace("[1]", ln_minimum_characters.tostring()) + "');", True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "search_reopen", "$('.uc_content_search').css({display: 'block'});", True)
                keywords = ""
            End If
        End If

        ' Controle of er al iets gestuurd is
        If keywords.Trim() = "" Then
            dt_search = Nothing
        Else
            ' Eerst naar pagina titels zoeken in de website structuur, vervolgens naar tekstuele inhoud en uiteindelijk naar nieuwsberichten
            Dim search_query As String = ""

            ' De kolom opbouw: pagina type, blok/pagina type, url, title, categorie, short_text, page_url

            ' Structure pages
            search_query &= " SELECT us_page_type as page_type, 'page' as type , tgt_url_" + Me.global_cms.Language + " as url , us_prod_cat, us_desc_" & Me.global_cms.Language & " as title , us_content_" & Me.global_cms.Language & " as short_text, us_page_url FROM ucm_structure WHERE us_desc_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' or us_content_" + Me.global_cms.Language + " LIKE '%" & keywords & "%'"
            search_query &= " UNION ALL "

            ' Structure blocks
            search_query &= " SELECT 'DEFAULT' as page_type, 'text' as type , tgt_url_" + Me.global_cms.Language + " as url , '' as us_prod_cat, us_desc_" & Me.global_cms.Language & " as title , ucm_text_blocks.block_content_" & Me.global_cms.Language & " as short_text, '' as us_page_url FROM ucm_text_blocks INNER JOIN ucm_structure_blocks ON ucm_text_blocks.rec_id = ucm_structure_blocks.text_block_id INNER JOIN ucm_structure ON ucm_structure_blocks.us_code = ucm_structure.us_code WHERE ucm_text_blocks.block_content_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' "
            search_query &= " UNION ALL "

            ' Structure blocks formulieren
            search_query &= " SELECT 'DEFAULT' as page_type, 'form' as type , tgt_url_" + Me.global_cms.Language + " as url , '' as us_prod_cat, us_desc_" & Me.global_cms.Language & " as title , ucm_form_blocks.form_content_" & Me.global_cms.Language & " as short_text, '' as us_page_url FROM ucm_form_blocks INNER JOIN ucm_structure_blocks ON ucm_form_blocks.rec_id = ucm_structure_blocks.form_block_id INNER JOIN ucm_structure ON ucm_structure_blocks.us_code = ucm_structure.us_code WHERE ucm_form_blocks.form_content_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' "
            search_query &= " UNION ALL "

            ' News berichten
            ' search_query &= " SELECT 'DEFAULT' as page_type, 'news' as type , tgt_url_" + Me.global_cms.Language + " as url , '' as us_prod_cat, title_" & Me.global_cms.Language & " as title , content_" & Me.global_cms.Language & " as short_text, '' as us_page_url FROM ucm_news WHERE (title_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' or desc_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' or content_" & Me.global_cms.Language & " LIKE '%" & keywords & "%')) "
            ' search_query &= " UNION ALL "

            ' Producten
            search_query &= " SELECT 'DEFAULT' as page_type, 'presentation' as type , tgt_url_" + Me.global_cms.Language + " as url, '' as us_prod_cat, cat_desc_" & Me.global_cms.Language & " as title , cat_info_" & Me.global_cms.Language & " as short_text, '' as us_page_url FROM ucm_categories WHERE (cat_desc_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' or cat_info_" & Me.global_cms.Language & " LIKE '%" & keywords & "%') "
            search_query &= " UNION ALL "

            ' Overige pagina's
            search_query &= " SELECT 'DEFAULT' as page_type, 'additional' as type , tgt_url_" + Me.global_cms.Language + " as url, '' as us_prod_cat, page_desc_" & Me.global_cms.Language & " as title , page_content_" & Me.global_cms.Language & " as short_text, '' as us_page_url FROM ucm_pages WHERE (page_desc_" & Me.global_cms.Language & " LIKE '%" & keywords & "%' or page_content_" & Me.global_cms.Language & " LIKE '%" & keywords & "%') "

            ' Sortering
            search_query &= " ORDER BY title "

            'uitvoer
            qc_search.QueryString = search_query

            'Try todo search, but if fails with exception, then just show "no search results"
            Try
                dt_search = qc_search.execute_query(Me.global_ws.user_information.user_id)
            Catch ex As Exception
                dt_search = Nothing
            End Try
        End If

        If dt_search Is Nothing Then
            Me.pnl_results.Visible = False
            Me.pnl_no_results.Visible = True
        Else
            Dim short_text As String = ""
            For Each dr_data_row As System.Data.DataRow In dt_search.Rows
                Dim lc_page_type As String = dr_data_row.Item("page_type")
                Select Case lc_page_type
                    Case "LINK_INTERNAL"
                        dr_data_row.Item("url") = dr_data_row.Item("us_page_url")
                    Case "LINK_EXTERNAL"
                        dr_data_row.Item("url") = dr_data_row.Item("us_page_url")
                    Case "UWS_CATEGORIES"
                        dr_data_row.Item("url") = "~/Webshop/product_list.aspx?cat_code=" + dr_data_row.Item("us_prod_cat")
                    Case Else
                        dr_data_row.Item("url") = Me.ResolveCustomUrl("~/" + dr_data_row.Item("url"))
                End Select

                short_text = System.Text.RegularExpressions.Regex.Replace(dr_data_row.Item("short_text"), "\<[/]{0,1}[^>]{0,}\>", "")
                short_text = Regex.Replace(utilize_substring(short_text, 0, _max_length), "[\s]{1}[\S]*$", "...")

                dr_data_row.Item("short_text") = short_text
            Next

            dt_search.AcceptChanges()

            ''binden
            If dt_search.Rows.Count > 0 Then
                Me.rpt_items.DataSource = dt_search
                Me.rpt_items.DataBind()
                Me.pnl_results.Visible = True
            Else
                Me.pnl_results.Visible = False
                Me.pnl_no_results.Visible = True
            End If
        End If
    End Sub
End Class