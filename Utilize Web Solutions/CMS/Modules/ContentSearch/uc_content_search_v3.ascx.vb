﻿
Partial Class uc_content_search_v3
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private dt_search As System.Data.DataTable = Nothing
    Private qc_search As New Utilize.Data.QueryCustomSelectClass

    Private _minimum_characters As Integer = 5
    Public Property minimum_characters() As Integer
        Get
            Return _minimum_characters
        End Get
        Set(ByVal value As Integer)
            _minimum_characters = value
        End Set
    End Property

    Protected Sub button_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim ln_minimum_characters = Me.global_ws.get_webshop_setting("search_count")

            If ln_minimum_characters > 0 Then
                _minimum_characters = ln_minimum_characters
            End If
        End If

        If ll_resume Then
            ll_resume = Me.txt_search.Text.Length >= _minimum_characters

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "search_text", "alert('" + global_trans.translate_message("message_search_text", "Uw zoekterm moet uit minimaal [1] tekens bestaan.", Me.global_ws.Language).Replace("[1]", _minimum_characters.ToString()) + "');", True)
            End If
        End If

        If ll_resume Then
            Select Case opg_search_type.SelectedValue
                Case "WEBSHOP"
                    Response.Redirect("~/" + Me.global_ws.Language + "/productsearch/search.aspx?searchtext=" + Server.UrlEncode(Me.txt_search.Text), True)
                Case "WEBSITE"
                    Response.Redirect("~/" + Me.global_ws.Language + "/contentsearch/search.aspx?keywords=" + Server.UrlEncode(Me.txt_search.Text), True)
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Private _input_control_extender As New AjaxControlToolkit.AutoCompleteExtender
    Private _input_javascript As New Utilize.Web.UI.WebControls.literal

    Public Sub New()
        Me._input_control_extender.MinimumPrefixLength = "2"
        Me._input_control_extender.CompletionInterval = "100"
        Me._input_control_extender.CompletionListCssClass = "autocomplete_ws_search"
        Me._input_control_extender.CompletionListItemCssClass = "autocomplete_listItem"
        Me._input_control_extender.CompletionListHighlightedItemCssClass = "highlighted"
        Me._input_control_extender.ServiceMethod = "GetProducts"
        Me._input_control_extender.FirstRowSelected = False
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_content_search_v3.css", Me)

        Me.txt_search.Attributes.Add("placeholder", global_trans.translate_label("label_search", "Zoeken", Me.global_cms.Language))

        'Zorg dat de opties op de juiste wijze vertaald worden
        Me.opg_search_type.Items(0).Text = global_trans.translate_label("label_search_type_website", "Website", Me.global_cms.Language)
        Me.opg_search_type.Items(1).Text = global_trans.translate_label("label_search_type_webshop", "Webshop", Me.global_cms.Language)
        Me.opg_search_type.RepeatLayout = UI.WebControls.RepeatLayout.Flow

        Dim ln_search_type As Integer = Me.global_ws.get_webshop_setting("search_type")

        ' Zorg dat de juiste opties getoond worden
        Select Case ln_search_type
            Case 1
                Me.opg_search_type.SelectedIndex = 0
                Me.opg_search_type.Visible = False
            Case 2
                Me.opg_search_type.SelectedIndex = 1
                Me.opg_search_type.Visible = False
            Case 3
                Me.opg_search_type.SelectedIndex = 0
                Me.opg_search_type.Visible = True
            Case Else
                Me.opg_search_type.SelectedIndex = 1
                Me.opg_search_type.Visible = False
        End Select

        If Not Me.opg_search_type.Visible And Me.opg_search_type.SelectedIndex = 1 And Utilize.FrameWork.LicenseManager.CheckModule("utlz_search_extender") Then
            Me.Controls.Add(_input_control_extender)
            Me.Controls.Add(_input_javascript)

            Me._input_control_extender.TargetControlID = "txt_search"
            Me._input_control_extender.ID = "ext_product_code"

            Me._input_control_extender.ContextKey = "uws_products"
        End If

        ' Als er gezocht wordt, dan moet de zoekterm overgenomen worden
        Select Case True
            Case Request.Url.ToString.ToLower().Contains("product_search.aspx")
                If Not Me.get_page_parameter("searchtext") = "" Then
                    Me.txt_search.Text = Me.get_page_parameter("searchtext")
                    Me.opg_search_type.SelectedValue = "WEBSHOP"
                End If
            Case Request.Url.ToString.ToLower().Contains("search_page.aspx")
                If Not Me.get_page_parameter("keywords") = "" Then
                    Me.txt_search.Text = Me.get_page_parameter("keywords")
                    Me.opg_search_type.SelectedValue = "WEBSITE"
                End If
            Case Else
                Exit Select
        End Select
    End Sub

End Class
