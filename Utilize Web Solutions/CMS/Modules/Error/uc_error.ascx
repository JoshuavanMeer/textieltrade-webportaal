﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_error.ascx.vb" Inherits="uc_error" %>
<div class="uc_error">
    <h1><utilize:translatetitle runat="server" ID="title_error_occured"></utilize:translatetitle></h1>
    <p><utilize:literal runat="server" id="lt_error_message"></utilize:literal></p>
</div>