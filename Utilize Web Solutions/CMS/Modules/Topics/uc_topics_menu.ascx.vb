﻿
Partial Class uc_topics_menu
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        Dim dt_topic_blocks As System.Data.DataTable = Nothing
        Dim dt_topics As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_topic_blocks As New Utilize.Data.QuerySelectClass
            qsc_topic_blocks.select_fields = "*"
            qsc_topic_blocks.select_from = "ucm_topic_blocks"
            qsc_topic_blocks.select_order = "row_ord"
            qsc_topic_blocks.select_where = "rec_id = @rec_id"
            qsc_topic_blocks.add_parameter("rec_id", block_id)

            dt_topic_blocks = qsc_topic_blocks.execute_query()

            ll_resume = dt_topic_blocks.Rows.Count > 0
        End If

        If ll_resume Then
            Dim qsc_topics As New Utilize.Data.QuerySelectClass
            qsc_topics.select_fields = "*"
            qsc_topics.select_from = "ucm_topics"
            qsc_topics.select_order = "row_ord"
            qsc_topics.select_where = "hdr_id = @hdr_id"
            qsc_topics.add_parameter("hdr_id", block_id)

            dt_topics = qsc_topics.execute_query()
        End If

        If ll_resume Then
            Me.img_topic.ImageUrl = "~/" + dt_topic_blocks.Rows(0).Item("topic_image")
            Me.lbl_topic_title.Text = dt_topic_blocks.Rows(0).Item("topic_title_" + Me.global_cms.Language)
        End If

        If ll_resume Then
            Me.rpt_topic_links.DataSource = dt_topics
            Me.rpt_topic_links.DataBind()
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_topics_menu.css", Me)
        End If
    End Sub
End Class
