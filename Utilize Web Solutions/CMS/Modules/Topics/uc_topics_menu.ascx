﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_topics_menu.ascx.vb" Inherits="uc_topics_menu" ClassName ="uc_topics_menu" %>

<utilize:placeholder runat="server" ID="ph_topics_menu">
    <utilize:panel runat="server" ID="pnl_topics" CssClass="uc_topics_menu service-box-v1">
        <div class="service-block service-block-default no-margin-bottom">
            <utilize:image runat="server" ID="img_topic" />
            <div class="uc_topics_menu_item">
                <h2 class="heading-sm"><utilize:label runat="server" ID="lbl_topic_title" CssClass="topic_title"></utilize:label></h2>
                <ul class="list-unstyled">
                    <utilize:repeater runat="server" ID="rpt_topic_links">
                        <ItemTemplate>
                            <li>
                                <asp:hyperlink runat="server" ID="hl_topic" NavigateUrl='<%#"~/" + DataBinder.Eval(Container.DataItem, "topic_link_url_" + Me.global_cms.Language)%>'><%# DataBinder.Eval(Container.DataItem, "topic_link_desc_" + Me.global_cms.Language)%></asp:hyperlink>
                            </li>
                        </ItemTemplate>
                    </utilize:repeater>
                </ul>
            </div>
        </div>
    </utilize:panel>
</utilize:placeholder>