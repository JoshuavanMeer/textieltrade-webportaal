﻿
Partial Class uc_tag_cloud
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _block_id As String = ""
    Public Shadows Property block_id() As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_testing Then
            ll_testing = Not _block_id.Trim() = ""
        End If

        If ll_testing Then
            Me.lbl_link_desc.Text = Utilize.Data.DataProcedures.GetValue("ucm_tag_cloud_blocks", "cloud_desc_" + Me.global_cms.Language, _block_id)
        End If

        If ll_testing Then
            Me.ph_title.Visible = Utilize.Data.DataProcedures.GetValue("ucm_tag_cloud_blocks", "tag_title_avail", Me.block_id)
        End If

        If ll_testing Then
            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            With qsc_data_table
                .select_fields = "link_desc_" + Me.global_cms.Language + " as link_desc, link_url_" + Me.global_cms.Language + " as link_url, link_prio"
                .select_from = "ucm_tag_cloud_links"
                .select_where = " hdr_id = @hdr_id"
                .select_order = "row_ord asc"
            End With
            qsc_data_table.add_parameter("hdr_id", _block_id)

            dt_data_table = qsc_data_table.execute_query()

            ll_testing = dt_data_table.Rows.Count > 0

            If Not ll_testing Then
                Me.Visible = False
            End If
        End If

        If ll_testing Then
            Dim lo_ul As New HtmlGenericControl("ul")

            For Each dr_data_table As System.Data.DataRow In dt_data_table.Rows ''Do While ll_move_next
                Dim lo_li As New HtmlGenericControl("li")
                lo_li.Attributes.Add("class", "tag" + dt_data_table.Rows(0).Item("link_prio").ToString())

                Dim hl_hyperlink As New hyperlink
                With hl_hyperlink
                    .Text = dt_data_table.Rows(0).Item("link_desc")
                    .NavigateUrl = dt_data_table.Rows(0).Item("link_url")
                End With
                lo_li.Controls.Add(hl_hyperlink)
                lo_ul.Controls.Add(lo_li)
            Next

            Me.ph_tags.Controls.Add(lo_ul)
        End If

        If ll_testing And False Then
            Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_tag_cloud_blocks", "skin_id", _block_id)

            If Not lc_skin_id = "" Then
                Me.skin = lc_skin_id
            End If
        End If

        If ll_testing Then
            Me.set_style_sheet("uc_tag_cloud.css", Me)
        End If
    End Sub
End Class
