﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_tag_cloud.ascx.vb" Inherits="uc_tag_cloud" %>
<div class="block uc_tag_cloud">
    <utilize:placeholder runat="server" ID="ph_title">
        <h2><utilize:label runat='server' ID="lbl_link_desc" CssClass="link_desc"></utilize:label></h2>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_tags"></utilize:placeholder>
</div>
