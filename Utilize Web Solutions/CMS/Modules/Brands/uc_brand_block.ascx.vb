﻿
Partial Class uc_brand_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _block_id As String = ""
    Public Shadows Property block_id() As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    ' recordset object aanmaken
    Private dt_brand_block As System.Data.DataTable

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            '  Controleer of de module banners beschikbaar is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_brnd")
        End If

        If ll_resume Then
            ll_resume = Not _block_id = ""
        End If

        ' er is een id opgegeven dus doorgaan
        If ll_resume Then
            Dim lc_language As String = Me.global_cms.Language

            ' Set the different properties
            ' Set the where clause
            Dim qc_query_class As New Utilize.Data.QuerySelectClass
            With qc_query_class
                .select_fields = "uws_products_tran.*, uws_products.prod_price, uws_products.image_url, uws_products.prod_stock, uws_products.brand_code"
                .select_from = "uws_products"
                .select_where = "uws_products.brand_code = @brand_code and uws_products_tran.lng_code = @lng_code"
            End With
            qc_query_class.add_join("left outer join", "uws_products_tran", "uws_products.prod_code = uws_products_tran.prod_code")
            qc_query_class.add_parameter("brand_code", _block_id)
            qc_query_class.add_parameter("lng_code", lc_language)

            dt_brand_block = qc_query_class.execute_query()

            ll_resume = dt_brand_block.Rows.Count > 0
        End If

        If ll_resume Then
            Me.rpt_brand_products.DataSource = dt_brand_block
            Me.rpt_brand_products.DataBind()

            Me.set_style_sheet("uc_brand_block.css", Me)
        End If
    End Sub
End Class
