﻿<%@ Control Language="VB" AutoEventWireup="false" ClassName="uc_brand_block" CodeFile="uc_brand_block.ascx.vb" Inherits="uc_brand_block" %>

<div class="products_per_brand">
    <asp:Repeater runat="server" ID="rpt_brand_products">
        <ItemTemplate>
            <a class="product" href="#">
                <img alt="<%#DataBinder.Eval(Container.DataItem, "prod_desc")%>" src='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "image_url"))%>' />
                <div><%#DataBinder.Eval(Container.DataItem, "prod_desc")%></div>
            </a>
        </ItemTemplate>
    </asp:Repeater>
    <div class="navigate">
        <span class="previous">Vorige pagina</span>
        
        <div class="pages">
            <span>1</span>
            -
            <span>2</span>
            -
            <span>3</span>
            -
            <span>4</span>
        </div>
        
        <span class="next">Volgende pagina</span>
    </div>
</div>