﻿
Partial Class uc_product_details
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private dt_product_details As System.Data.DataTable = Nothing
    Private prod_code As String = ""

    Private Sub uc_product_details_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            prod_code = Me.get_page_parameter("prodcode")
            ll_testing = Not prod_code = ""
        End If

        If ll_testing Then
            Dim qsc_product_details As New Utilize.Data.QuerySelectClass
            qsc_product_details.select_fields = "*"
            qsc_product_details.select_from = "ucm_products"
            qsc_product_details.select_where = "prod_code = @prod_code"
            qsc_product_details.add_parameter("prod_code", prod_code)
            qsc_product_details.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())
            dt_product_details = qsc_product_details.execute_query()

            ll_testing = dt_product_details.Rows.Count > 0
        End If

        If ll_testing Then
            Me.lt_article_title.Text = dt_product_details.Rows(0).Item("prod_desc_" + Me.global_ws.Language)
            Me.lt_prod_desc.Text = dt_product_details.Rows(0).Item("prod_content_" + Me.global_ws.Language)

            Me.ph_product_description.Visible = Not lt_prod_desc.Text.Trim() = ""
        End If

        If ll_testing Then
            ' Laad hier de afbeeldingen van de productdetails
            ' ph_product_images_lightbox

            Dim uc_pp_products_images_lightbox As Utilize.Web.Solutions.CMS.cms_user_control = LoadUserControl("~/CMS/Modules/ProductPresentation/ProductDetails/uc_pp_product_images_lightbox.ascx")
            uc_pp_products_images_lightbox.ID = "uc_pp_products_images_lightbox"
            uc_pp_products_images_lightbox.set_property("product_code", prod_code)

            Me.ph_product_images_lightbox.Controls.Add(uc_pp_products_images_lightbox)
        End If

        If Not ll_testing Then
            Me.ph_product_details.Visible = False
            Me.ph_product_not_found.Visible = True
        End If
    End Sub
End Class
