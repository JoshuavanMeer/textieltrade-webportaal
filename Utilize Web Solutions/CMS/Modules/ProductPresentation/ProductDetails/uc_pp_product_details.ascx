﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_pp_product_details.ascx.vb" Inherits="uc_product_details" %>

<div class="uc_pp_product_details">
    <utilize:placeholder runat="server" ID="ph_product_details" Visible="true">
        <div class="shop-product margin-bottom-10">
            <div class="productcontent row">
                <div class="col-sm-6 col-md-6 image">
                    <utilize:placeholder runat="server" ID="ph_product_images_lightbox">

                    </utilize:placeholder>
                </div>
                <div class="col-sm-6 col-md-6 specs">
                    <utilize:placeholder runat="server" ID="ph_shop_product_heading">
                        <div class="shop-product-heading">
                            <h1><utilize:literal ID="lt_article_title" runat="server"></utilize:literal></h1>
                        </div>
                    </utilize:placeholder>
                    <div class="productspecs">

                    </div>
                </div>
            </div>
        </div>

        <div class="productcontent">
            <utilize:placeholder runat="server" ID="ph_product_description">
                <h2><utilize:translatetitle runat="server" ID="title_product_description" Text="Product omschrijving"></utilize:translatetitle></h2>
                <p><utilize:literal ID="lt_prod_desc" runat="server"></utilize:literal></p>
            </utilize:placeholder>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_product_not_found" Visible='false'>
        <h1><utilize:translatetitle runat="server" ID="title_product_not_found" Text="Het product is niet gevonden"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_product_not_found" Text="De pagina van het product is niet gevonden."></utilize:translatetext>
    </utilize:placeholder>
</div>
