﻿Imports System.Data

Partial Class uc_pp_product_images_lightbox
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _product_code As String = ""
    Private _product_row As System.Data.DataRow = Nothing

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property product_code() As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    ' datatable aanmaken
    Private udt_product_data As System.Data.DataTable = Nothing

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' licentie controle
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_pp_product_images_lightbox.css", Me)

            Me.set_style_sheet("~/_THMAssets/plugins/master-slider/quick-start/masterslider/style/masterslider.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/master-slider/quick-start/masterslider/skins/default/style.css", Me)

            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/jquery.fancybox.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5", Me)

            Me.set_javascript("~/_THMAssets/js/plugins/master-slider.js", Me)
            Me.set_javascript("~/_THMAssets/plugins/master-slider/quick-start/masterslider/masterslider.min.js", Me)

            ' Set the fancybox javascript files
            Me.Page.Master.FindControl("ph_fancybox").Visible = True

            ' huidige taalcode ophalen
            Dim lc_language = Me.global_cms.Language

            ' Maak een nieuwe datatable aan
            _product_row = Utilize.Data.DataProcedures.GetDataRow("ucm_products", product_code)

            Dim lc_image_file_path As String = System.Web.HttpContext.Current.Server.MapPath("~/") + _product_row.Item("prod_image_large")
            If _product_row.Item("prod_image_large") = "" Or Not System.IO.File.Exists(lc_image_file_path) Then
                _product_row.Item("prod_image_large") = Me.ResolveCustomUrl("~/Documents/ProductImages/not-available-large.jpg")
            End If

            _product_row.AcceptChanges()

            ' er mag maar een enkele product met deze code voorkomen
            Dim ll_resume_database As Boolean = Not _product_row Is Nothing

            If ll_resume_database Then
                ' Bouw de lijst met afbeeldingen op
                image_list(Me._product_code)
            End If
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox", "jQuery(document).ready(function() {$('.fancybox').fancybox({openEffect  : 'fade', closeEffect : 'fade', prevEffect:  'fade', nextEffect:  'fade'});});", True)

        If ll_resume And Me.rpt_image_list.Items.Count > 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox1", "jQuery(document).ready(function () {MasterSliderShowcase2.initMasterSliderShowcase2();});", True)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox-button", "jQuery(document).ready(function() {$('.button-lightbox-zoom-in').click(function() {$('.ms-sl-selected .fancybox').click();});});", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox-button", "jQuery(document).ready(function() {$('#enlarge-image').click(function() {$('#" + img_image.ClientID() + "').click();});});", True)
        End If
    End Sub

    Private Sub image_list(ByVal product_code As String)
        Dim ll_resume As Boolean = True

        ' datatable aanmaken
        Dim udt_data_table As Utilize.Data.DataTable = Nothing

        If ll_resume Then
            udt_data_table = New Utilize.Data.DataTable
            udt_data_table.table_name = "ucm_product_images"

            ' Initialiseren
            udt_data_table.table_init()

            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            udt_data_table.add_where("and prod_code = @prod_code")
            udt_data_table.add_where_parameter("prod_code", product_code)

            ' En laad de tabel
            udt_data_table.table_load()

            Dim dt_data_table As System.Data.DataTable = udt_data_table.data_table

            ' If dt_data_table.Rows.Count > 0 Then
            Dim dr_data_Row As System.Data.DataRow = dt_data_table.NewRow()
            dr_data_Row.Item("PrimaryKey") = Me.product_code
            dr_data_Row.Item("prod_code") = Me.product_code
            dr_data_Row.Item("img_desc_" + Me.global_cms.Language) = _product_row.Item("prod_desc_" + Me.global_cms.Language)
            dr_data_Row.Item("image_url") = _product_row.Item("prod_image_large")

            dt_data_table.Rows.InsertAt(dr_data_Row, 0)

            CheckImages(dt_data_table)

            Me.img_image.ImageUrl = "~/" + dt_data_table.Rows(0).Item("image_url")
            Me.img_image.AlternateText = dr_data_Row.Item("img_desc_" + Me.global_ws.Language).ToString().Replace("'", "").Replace("""", "")
            Me.img_fancybox.Attributes.Add("href", Me.ResolveCustomUrl("~/" + dt_data_table.Rows(0).Item("image_url")))

            Me.rpt_image_list.DataSource = dt_data_table
            Me.rpt_image_list.DataBind()

            Me.ph_image.Visible = Me.rpt_image_list.Items.Count = 1
            Me.rpt_image_list.Visible = Me.rpt_image_list.Items.Count > 1
        End If
    End Sub

    Private Sub CheckImages(ByRef dt As DataTable)
        Dim dt_copy As DataTable = dt.Copy()

        Dim ln_index As Integer = 0
        For Each dr As DataRow In dt_copy.Rows
            Dim dr_data_row As DataRow = dt.Rows(ln_index)

            Dim lc_image_file_path As String = System.Web.HttpContext.Current.Server.MapPath("~/") + dr_data_row.Item("image_url")
            If Not System.IO.File.Exists(lc_image_file_path) Then
                dr_data_row.Item("image_url") = "Documents/ProductImages/not-available-large.jpg"
            End If

            ln_index += 1
        Next

        dt.AcceptChanges()
    End Sub
End Class
