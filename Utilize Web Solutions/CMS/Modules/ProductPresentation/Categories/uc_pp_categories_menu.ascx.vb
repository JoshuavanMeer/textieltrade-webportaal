﻿
Partial Class uc_pp_categories_menu
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private rec_id As String = ""
    Private _category_string As String = "^"

    ''' <summary>
    ''' Sub om een string van bovenliggende categorieën te maken
    ''' </summary>
    ''' <param name="rec_id"></param>
    ''' <remarks></remarks>
    Private Sub create_category_string(ByVal rec_id As String)
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        qsc_categories.select_fields = "ucm_categories.cat_parent, ucm_categories.rec_id"
        qsc_categories.select_from = "ucm_categories"
        qsc_categories.select_where = "ucm_categories.rec_id = @rec_id and cat_show = 1 and shop_code = @shop_code"

        qsc_categories.add_parameter("rec_id", rec_id)
        qsc_categories.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

        Dim dt_data_Table As System.Data.DataTable = qsc_categories.execute_query()

        If dt_data_Table.Rows.Count > 0 Then
            _category_string += dt_data_Table.Rows(0).Item("rec_id") + "^"

            If Not dt_data_Table.Rows(0).Item("cat_parent") = "" Then
                create_category_string(dt_data_Table.Rows(0).Item("cat_parent"))
            End If
        End If
    End Sub

    Private _level_count As Integer = 5
    Public Property level_count As Integer
        Get
            Return _level_count
        End Get
        Set(value As Integer)
            _level_count = value
        End Set
    End Property

    Private _level As Integer = 1
    Private sub_menu_counter As Integer = 0

    Private Sub create_category_uls(ByVal rec_id As String, ByVal targetcontrol As System.Web.UI.Control)
        If _level <= _level_count Then
            ' Haal de categorieën op.
            Dim qsc_categories As New Utilize.Data.QuerySelectClass
            qsc_categories.select_fields = "rec_id, cat_desc_" + Me.global_cms.Language + " as cat_desc, tgt_url_" + Me.global_cms.Language + " as tgt_url"
            qsc_categories.select_from = "ucm_categories"
            qsc_categories.select_where = "ucm_categories.cat_parent = @cat_parent and cat_show = 1 and shop_code = @shop_code"
            qsc_categories.select_order = "row_ord asc"

            qsc_categories.add_parameter("cat_parent", rec_id)
            qsc_categories.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            ' Voer de query uit
            Dim dt_data_table As System.Data.DataTable = qsc_categories.execute_query()

            ' controle voor het toevoegen van de class dropdown
            If _level > 1 Then
                If CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Item("class") = "active" Then
                    If dt_data_table.Rows.Count > 0 Then
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown active")
                    Else
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width active")
                    End If
                Else
                    If dt_data_table.Rows.Count > 0 Then
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown")
                    Else
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width")
                    End If
                End If
            End If

            ' Als er resultaten gevonden zijn...
            If dt_data_table.Rows.Count > 0 Then
                Dim ln_level_number As Integer = _level.ToString()

                ' Maak een nieuwe ul aan
                Dim lo_ul As New HtmlGenericControl("ul")
                ' Zet de classname en zorg dat we het niveau ook weten 
                lo_ul.Attributes.Add("class", "level" + _level.ToString())

                If ln_level_number = 1 Then
                    lo_ul.Attributes.Add("class", "nav navbar-nav")
                End If

                If ln_level_number > 1 Then
                    If _category_string.Contains("^" + rec_id + "^") Then
                        lo_ul.Attributes.Add("class", "dropdown-menu active")
                    Else
                        lo_ul.Attributes.Add("class", "dropdown-menu inactive")
                    End If
                End If


                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    Dim lo_li As New HtmlGenericControl("li")

                    Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink

                    hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + dr_data_row.Item("tgt_url"))
                    hl_hyperlink.Text = dr_data_row.Item("cat_desc")

                    ' code
                    If _category_string.Contains("^" + dr_data_row.Item("rec_id") + "^") Then
                        lo_li.Attributes.Add("class", "active")
                    End If

                    lo_li.Controls.Add(hl_hyperlink)

                    _level += 1
                    Me.create_category_uls(dr_data_row.Item("rec_id"), lo_li)
                    _level -= 1

                    lo_ul.Controls.Add(lo_li)

                Next
                'check of the ul een active bevat
                If TypeOf targetcontrol Is System.Web.UI.HtmlControls.HtmlGenericControl Then
                    For Each Control In lo_ul.Controls
                        If Control.attributes.item("class") = "li-width active" Then
                            CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown active")
                        End If
                    Next
                End If

                targetcontrol.Controls.Add(lo_ul)
            Else
                If rec_id.Trim() = "" Then
                    Me.Visible = False
                End If
            End If
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_pp_categories_menu.css", Me)

            If rec_id = "" Then
                rec_id = Me.get_page_parameter("rec_id")

                Me.create_category_string(rec_id)
            End If

            ' Bouw hier de gegevens op
            Me.create_category_uls(Me.block_id, Me.ph_categories)
        End If

        Me.Visible = ll_resume
    End Sub

End Class
