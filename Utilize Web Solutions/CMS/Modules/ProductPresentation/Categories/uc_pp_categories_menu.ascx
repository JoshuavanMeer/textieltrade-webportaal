﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_pp_categories_menu.ascx.vb" Inherits="uc_pp_categories_menu" %>

<div class="uc_pp_categories_menu mobile-category">
    <utilize:panel runat="server" id="pnl_menu_flyout" class="collapse navbar-collapse mega-menu navbar-responsive-collapse panel-width">
        <div class="panel-group" id="accordion">
            <div class="panel panel-borderset">
                <div class="mobile-heading panel-heading">
                    <h2 class="panel-title"><utilize:translatetitle runat="server" ID="title_categories" Text="Categorieën"></utilize:translatetitle></h2>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body mobile-body">
                        <utilize:placeholder runat="server" ID="ph_categories">

                        </utilize:placeholder>
                    </div>
                 
                </div>
            </div>
        </div>
    </utilize:panel>
</div>