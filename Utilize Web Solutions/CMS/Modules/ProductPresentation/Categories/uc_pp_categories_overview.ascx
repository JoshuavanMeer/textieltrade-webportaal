﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_pp_categories_overview.ascx.vb" Inherits="uc_pp_categories_overview" %>

<utilize:panel ID="pnl_categories_overview" CssClass="uc_pp_categories_overview margin-bottom-20" runat="server">
    <utilize:placeholder runat="server" ID="ph_categories_title">
        <h1><utilize:translatetitle runat="server" id="title_categories_overview" text="Categorieën overzicht"></utilize:translatetitle></h1>
    </utilize:placeholder>
        <div class="row">
            <utilize:repeater runat='server' ID="rpt_categories">
                <ItemTemplate>
                    <div class='<%# Me.item_size%> col-xs-6'>
                        <div class="category-item">
                            <utilize:HyperLink CssClass="image" runat="server" ID="hl_products_overview1" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url"))%>'>
                                <utilize:image CssClass="img-responsive" runat='server' ID="img_cat_image" ImageUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "cat_image"))%>'/>
                                <div class="category-name size-<%= Me.item_size_int %>">
                                    <utilize:literal ID="hl_categories" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "cat_desc")%>'></utilize:literal>
                                </div>
                            </utilize:HyperLink>
                        </div>
                    </div>

                    <utilize:placeholder runat="server" ID="ph_clearfix_sm" Visible="False">
                        <div class="customclearfix-xs"></div>
                    </utilize:placeholder>
                        
                    <utilize:placeholder runat="server" ID="ph_clearfix_md" Visible="<%# me.clearfix() %>">
                        <div class="customclearfix"></div>
                    </utilize:placeholder>
                </ItemTemplate>
            </utilize:repeater>
        </div>
</utilize:panel>
