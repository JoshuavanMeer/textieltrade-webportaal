﻿
Partial Class uc_pp_categories_overview
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private cat_code As String = ""
    Private clearfix_sm As Integer = 0
    Private clearfix_md As Integer = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_resume Then
            Me.ph_categories_title.Visible = Request.Url.ToString().ToLower.Contains("pp_products.aspx")
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_pp_categories_overview.css", Me)

            If Me.block_id = "" Then
                If cat_code = "" Then
                    cat_code = Me.get_page_parameter("cat_code")
                End If
            Else
                cat_code = block_id
            End If

            Dim qsc_categories As New Utilize.Data.QuerySelectClass
            qsc_categories.select_fields = "ucm_categories.rec_id, ucm_categories.cat_desc_" + Me.global_cms.Language + " as cat_desc, ucm_categories.tgt_url_" + Me.global_cms.Language + " as tgt_url, CASE WHEN ucm_categories.cat_image = '' THEN 'Documents/ProductImages/not-available-category.jpg' ELSE ucm_categories.cat_image END as cat_image "
            qsc_categories.select_from = "ucm_categories"
            qsc_categories.select_where = "ucm_categories.cat_parent = @cat_parent and cat_show = 1 and shop_code = @shop_code"
            qsc_categories.select_order = "row_ord"

            qsc_categories.add_parameter("cat_parent", cat_code)
            qsc_categories.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            dt_data_table = qsc_categories.execute_query()

            Me.rpt_categories.DataSource = dt_data_table
            Me.rpt_categories.DataBind()
        End If

        If ll_resume Then
            ' Haal de bovenliggende categorie op
            Dim lc_parent_cat As String = Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_parent", cat_code)
        End If

        ' Only show the categories block menu when categories are available
        If ll_resume Then
            ll_resume = dt_data_table.rows.count > 0
        End If

        Me.Visible = ll_resume
    End Sub

    Protected Sub rpt_categories_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_categories.ItemCreated
        clearfix_sm += 1
        If clearfix_sm = 2 Then
            clearfix_sm = 0
            Dim clearfixsm As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_clearfix_sm")
            clearfixsm.Visible = True
        End If

        clearfix_md += CInt(Me.item_size_int)

        If clearfix_md = 12 Then
            clearfix_md = 0

            Dim clearfixsm As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_clearfix_md")
            clearfixsm.Visible = True
        End If
    End Sub
End Class
