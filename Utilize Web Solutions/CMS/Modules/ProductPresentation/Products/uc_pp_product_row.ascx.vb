﻿
Partial Class uc_pp_product_row
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _dr_product_row As System.Data.DataRow
    Public Property product_row As System.Data.DataRow
        Get
            Return _dr_product_row
        End Get
        Set(value As System.Data.DataRow)
            _dr_product_row = value
        End Set
    End Property

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.object_code = "ucm_products"
        Me.block_id = _dr_product_row.Item("prod_code")

        Me.set_style_sheet("uc_pp_product_row.css", Me)
    End Sub
End Class
