﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_pp_product_row.ascx.vb" Inherits="uc_pp_product_row" %>

<div class="uc_pp_product_row uc_placeholder">
    <div class="table-view">
        <div class="row">
            <div class="item-code-table col-md-2 col-sm-2 col-xs-4">
                <div class="prod_code">
                    <ul class="list-inline prod_code">
                        <li class="value"><span class="prod_code_target"></span></li>
                    </ul>
                </div>
            </div>
            <div class="item-desc1-table col-md-2 col-sm-2 col-xs-4">
                <div class="row">
                    <asp:HyperLink ID="Hyperlink3" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url_" + Me.global_cms.Language))%>'><%# Me.product_row.Item("prod_desc_" + Me.global_cms.Language)%></asp:HyperLink></div>
            </div>
            <div class="item-comm-short-table col-md-4 col-sm-6 col-xs-12">
                <%# Me.product_row.Item("prod_comm_" + Me.global_cms.Language) %>
                <utilize:translatelink ID="link_read_more_table" CssClass="readmore" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url_" + Me.global_cms.Language)) %>' Text="Lees meer..." Visible='<%# Not Me.product_row.Item("prod_comm_" + Me.global_cms.Language) = ""%>'></utilize:translatelink>
            </div>
            <div class="item-price-table col-md-2 col-sm-2 col-xs-5">
                <div class="prod_price">
                    <ul class="price list-inline shop-product-prices">
                        <li><span class="value actual actualdefault target-actualdefault"></span></li>
                        <li><span class="value original originaldefault target-originaldefault"></span></li>
                    </ul>
                </div>
            </div>
            <div class="item-order-product-table col-md-2 col-sm-6 col-xs-7">
                <utilize:placeholder runat="server" ID="ph_order_product2"></utilize:placeholder>
            </div>
        </div>
    </div>

    <div class="list-product-description product-description-brd margin-bottom-30 item-border">
        <div class="container-fluid containter-placeholder">
            <div class="row row-placeholder">
                <div class="col-sm-4 image-block">
                    <asp:HyperLink ID="Hyperlink1" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url_" + Me.global_cms.Language))%>'><img class="image-placeholder img-responsive sm-margin-bottom-20" alt='<%# Me.product_row.Item("prod_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "") %>' src='<%# Me.ResolveCustomUrl("~/" + Me.product_row.Item("prod_image"))%>' /></asp:HyperLink>
                </div>

                <div class="col-sm-8 product-description information-block">
                    <div class="overflow-h margin-bottom-5">
                        <div class="table-block1">
                            <h4 class="title-price">
                                <asp:HyperLink ID="Hyperlink2" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url_" + Me.global_cms.Language))%>'><%# Me.product_row.Item("prod_desc_" + Me.global_cms.Language)%></asp:HyperLink></h4>
                            <div>

                                <span class="gender list-only">
                                    <p class="margin-bottom-20">
                                        <%# Me.product_row.Item("prod_comm_" + Me.global_cms.Language) %>
                                        <br>
                                        <utilize:translatelink ID="link_read_more" CssClass="readmore" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" +  Me.product_row.Item("tgt_url_" + Me.global_cms.Language)) %>' Text="Lees meer..." Visible='<%# Not Me.product_row.Item("prod_comm_" + Me.global_cms.Language) = ""%>'></utilize:translatelink>
                                    </p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>