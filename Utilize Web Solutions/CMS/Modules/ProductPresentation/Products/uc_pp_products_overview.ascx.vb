﻿Partial Class uc_pp_products_overview
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _dtProducts As System.Data.DataTable = Nothing
    Private _page_nr As Integer = 0
    Private _page_item_count As Integer = 15

    Private _page_count As Integer = 0
    Private _page_nr_count As Integer = 8
    Private _page_nr_endcount As Integer = 2
    Private _sort_field As String = ""

    Protected Friend _page_name As String = ""

    Private _page_range_1 As Integer = 0
    Private _page_range_2 As Integer = 0
    Private _page_range_3 As Integer = 0

    ' Onderstaand object wordt ingelezen vanuit een sessie object welke gecreeerd wordt in Specifications/uc_product_spec_filter.ascx.vb
    ' Het sessie object is Session("ProductSpecifications")
    ' Deze wordt vanuit de sessie ingelezen wanneer er specificaties gezet worden
    Private _active_specifications As New System.Collections.Generic.List(Of KeyValuePair(Of String, String))

    Private Sub set_product_list()

        ' Probeer het paginanummer uit te lezen
        If Not Me.get_page_parameter("page") = "" Then
            Try
                _page_nr = CInt(Me.get_page_parameter("page"))
            Catch ex As Exception
                _page_nr = 0
            End Try
        End If

        Dim qscProducts As New Utilize.Data.QuerySelectClass
        qscProducts.select_fields = "ucm_products.*, case when prod_image_small = '' then 'Documents/ProductImages/not-available-small.jpg' else prod_image_small end as prod_image"
        qscProducts.select_from = "ucm_products"
        qscProducts.add_join("left outer join", "ucm_categories_prod", "ucm_products.prod_code = ucm_categories_prod.prod_code")
        qscProducts.select_where = "ucm_categories_prod.cat_code = @cat_code and shop_code = @shop_code"
        qscProducts.add_parameter("cat_code", Me.get_page_parameter("CatCode"))
        qscProducts.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

        For I As Integer = 1 To 5
            If Not Me.get_page_parameter("free_text" + I.ToString() + "_" + Me.global_cms.Language) = "" Then
                qscProducts.select_where += " and ucm_products.free_text" + I.ToString() + "_" + Me.global_ws.Language + " = @param" + I.ToString()

                qscProducts.add_parameter("param" + I.ToString(), Me.get_page_parameter("free_text" + I.ToString() + "_" + Me.global_cms.Language))
            End If
        Next

        Dim lc_sort_field As String = ""
        lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"

        _dtProducts = qscProducts.execute_query()

        ' Maak een nieuwe datatable aan
        Dim dt_target As System.Data.DataTable = _dtProducts.Clone()
        Dim _range_start As Integer = (_page_nr * _page_item_count)
        Dim _range_end As Integer = ((_page_nr + 1) * _page_item_count)

        ' En voeg de regels toe aan de nieuwe tabel
        For I As Integer = _range_start To _range_end - 1
            If I < _dtProducts.Rows.Count Then
                dt_target.ImportRow(_dtProducts.Rows(I))
            End If
        Next

        ' Voer de query uit en bouw de repeater op
        Me.rpt_product_list.DataSource = dt_target
        Me.rpt_product_list.DataBind()

        ' Sla het aantal pagina's op in een variabele
        _page_count = System.Math.Ceiling(_dtProducts.Rows.Count / _page_item_count)


        Me.ph_product_list.Visible = True
        Me.ph_no_items.Visible = False

        If Me.rpt_product_list.Items.Count = 0 Then
            Me.ph_product_list.Visible = True
            Me.ph_no_items.Visible = False
        End If
    End Sub
    Private Sub set_pager_text()
        ' Zorg hier dat het juiste aantal getoond wordt in de pager
        Dim ln_product_count As Integer = _dtProducts.Rows.Count

        Dim ln_from As Integer = IIf(_page_nr = 0, 1, _page_nr * _page_item_count)
        Dim ln_till As Integer = IIf(_page_nr = 0, _page_item_count, (_page_nr * _page_item_count) + _page_item_count)

        If ln_till > ln_product_count Then
            ln_till = ln_product_count
        End If

        If ln_from > ln_till Then
            ln_from = ln_till
        End If

        Dim lc_product_text As String = global_trans.translate_label("label_showing_results", "Product [1] van [2] in categorie [3].", Me.global_ws.Language)
        lc_product_text = lc_product_text.Replace("[1]", ln_from.ToString() + " - " + ln_till.ToString())
        lc_product_text = lc_product_text.Replace("[2]", _dtProducts.Rows.Count.ToString())
        lc_product_text = lc_product_text.Replace("[3]", Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_desc_" + Me.global_cms.Language, Me.get_page_parameter("cat_code")))

        Me.lt_product_count1.Text = lc_product_text

        Me.hl_pl_previous1.Text = global_trans.translate_label("label_product_list_previous", "Vorige pagina", Me.global_cms.Language)
        Me.hl_pl_next1.Text = global_trans.translate_label("label_product_list_next", "Volgende pagina", Me.global_cms.Language)

        Me.hl_pl_previous2.Text = global_trans.translate_label("label_product_list_previous", "Vorige pagina", Me.global_cms.Language)
        Me.hl_pl_next2.Text = global_trans.translate_label("label_product_list_next", "Volgende pagina", Me.global_cms.Language)

        Me.label_items_per_page1.Text = global_trans.translate_label("label_items_per_page", "Aantal resultaten per pagina", Me.global_cms.Language)
        Me.label_items_per_page2.Text = global_trans.translate_label("label_items_per_page", "Aantal resultaten per pagina", Me.global_cms.Language)

        Me.label_sort_by1.Text = global_trans.translate_label("label_sort_by", "Sorteer op", Me.global_cms.Language)
        Me.label_sort_by2.Text = global_trans.translate_label("label_sort_by", "Sorteer op", Me.global_cms.Language)
    End Sub

    Private Function get_page_start(ByVal page_nr As Integer) As Integer
        ' _page_nr => Huidige pagina
        ' _page_count => Aantal pagina's
        ' _page_nr_count => Aantal pagina's tonen
        ' _page_nr_endcount => Aantal pagina's op het eind

        Dim ln_page_start As Integer = (page_nr - System.Math.Floor(_page_nr_count / 2))

        If (ln_page_start + _page_nr_count) > _page_count Then
            ln_page_start = _page_count - _page_nr_count
        End If

        If ln_page_start < 0 Then
            ln_page_start = 0
        End If

        Return ln_page_start
    End Function

    Private Function get_page_end(ByVal page_nr As Integer) As Integer
        Dim ln_page_end As Integer = Me.get_page_start(page_nr) + _page_nr_count

        If ln_page_end > _page_count Then
            ln_page_end = _page_count
        End If

        Return ln_page_end
    End Function

    Private Sub set_pager_navigation(ByVal empty_page As Boolean)
        If Not empty_page Then
            Dim page_start As Integer = Me.get_page_start(_page_nr)
            Dim page_end As Integer = Me.get_page_end(_page_nr)

            Me.set_page_parameter("pagesize", Me._page_item_count)

            Dim lc_pager_string As String = ""

            ' Bouw hier de standaard pagina's op
            For page_iterations As Integer = page_start To page_end - 1
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", page_iterations)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Dim lc_css_class As String = ""
                If page_iterations = _page_nr Then
                    lc_css_class = "active"
                End If

                If page_iterations = page_start And Not page_iterations + 1 = 1 Then
                    Dim url_class_start As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                    url_class_start.set_parameter("pagesize", Me._page_item_count)
                    url_class_start.set_parameter("page", 0)
                    Dim lc_url_start As String = Me.ResolveCustomUrl(url_class_start.get_url())
                    If page_iterations = 1 Then
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_start + """>" + "1" + "</a></li>"
                    Else
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_start + """>" + "1" + "</a></li>" + "<li class=""" + lc_css_class + """><a href="""">" + "..." + "</a></li>"
                    End If
                End If

                lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url + """>" + (page_iterations + 1).ToString() + "</a></li>"

                If page_iterations = page_end - 1 And Not page_iterations = _page_count - 1 Then
                    Dim url_class_end As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                    url_class_end.set_parameter("pagesize", Me._page_item_count)
                    url_class_end.set_parameter("page", _page_count - 1)
                    Dim lc_url_end As String = Me.ResolveCustomUrl(url_class_end.get_url())
                    If page_iterations = _page_count - 2 Then
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_end + """>" + _page_count.ToString() + "</a></li>"
                    Else
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href="""">" + "..." + "</a></li>" + "<li class=""" + lc_css_class + """><a href=""" + lc_url_end + """>" + _page_count.ToString() + "</a></li>"
                    End If
                End If
            Next

            Me.lt_pager_1.Text = lc_pager_string
            Me.lt_pager_2.Text = lc_pager_string

            If _page_nr > 0 Then
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", _page_nr - 1)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Me.hl_pl_previous1.NavigateUrl = lc_url
                Me.hl_pl_previous1.CssClass = "previous"

                Me.hl_pl_previous2.NavigateUrl = lc_url
                Me.hl_pl_previous2.CssClass = "previous"
            Else
                Me.hl_pl_previous1.NavigateUrl = ""
                Me.hl_pl_previous1.CssClass = "previous hide_me"

                Me.hl_pl_previous2.NavigateUrl = ""
                Me.hl_pl_previous2.CssClass = "previous hide_me"
            End If

            If _page_nr < (_page_count - 1) Then
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", _page_nr + 1)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Me.hl_pl_next1.NavigateUrl = lc_url
                Me.hl_pl_next1.CssClass = "next"

                Me.hl_pl_next2.NavigateUrl = lc_url
                Me.hl_pl_next2.CssClass = "next"
            Else
                Me.hl_pl_next1.NavigateUrl = ""
                Me.hl_pl_next1.CssClass = "next hide_me"

                Me.hl_pl_next2.NavigateUrl = ""
                Me.hl_pl_next2.CssClass = "next hide_me"
            End If
        Else
            Me.hl_pl_previous1.CssClass = "previous hide_me"
            Me.hl_pl_next1.CssClass = "next hide_me"

            Me.hl_pl_previous2.CssClass = "previous hide_me"
            Me.hl_pl_next2.CssClass = "next hide_me"
        End If
        'als er maar 1 pagina aanwezig is hide de pager
        If _page_count = 1 Then
            Me.ph_pager1.Visible = False
            Me.ph_pager2.Visible = False
        End If
    End Sub
    Private Sub set_pager_ranges()
        Dim dt_data_table As New System.Data.DataTable

        ' Maak een nieuwe datacolumn aan
        Dim dc_column As New System.Data.DataColumn
        dc_column.ColumnName = "page_size"
        dc_column.DataType = System.Type.GetType("System.Int32")

        dt_data_table.Columns.Add(dc_column)

        Dim dr_data_row5 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row5.Item("page_size") = _page_range_1
        dt_data_table.Rows.Add(dr_data_row5)

        Dim dr_data_row10 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row10.Item("page_size") = _page_range_2
        dt_data_table.Rows.Add(dr_data_row10)

        Dim dr_data_row15 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row15.Item("page_size") = _page_range_3
        dt_data_table.Rows.Add(dr_data_row15)

        Me.cbo_items_per_page1.DataTextField = "page_size"
        Me.cbo_items_per_page1.DataValueField = "page_size"
        Me.cbo_items_per_page1.DataSource = dt_data_table
        Me.cbo_items_per_page1.DataBind()

        Me.cbo_items_per_page2.DataTextField = "page_size"
        Me.cbo_items_per_page2.DataValueField = "page_size"
        Me.cbo_items_per_page2.DataSource = dt_data_table
        Me.cbo_items_per_page2.DataBind()

        Try
            Me.cbo_items_per_page1.SelectedValue = Me.get_page_parameter("pagesize")
            Me.cbo_items_per_page2.SelectedValue = Me.get_page_parameter("pagesize")

            _page_item_count = Me.get_page_parameter("pagesize")
        Catch ex As Exception
            _page_item_count = _page_range_1
        End Try
    End Sub
    Private Sub set_sort_by_items()
        Dim dt_data_table As New System.Data.DataTable

        ' Maak een nieuwe datacolumn aan
        Dim dc_field As New System.Data.DataColumn
        dc_field.ColumnName = "sort_field"
        dc_field.DataType = System.Type.GetType("System.String")

        dt_data_table.Columns.Add(dc_field)

        ' Maak een nieuwe datacolumn aan
        Dim dc_text As New System.Data.DataColumn
        dc_text.ColumnName = "sort_text"
        dc_text.DataType = System.Type.GetType("System.String")

        dt_data_table.Columns.Add(dc_text)

        Dim dr_data_row1 As System.Data.DataRow = Nothing
        dr_data_row1 = dt_data_table.NewRow()
        dr_data_row1.Item("sort_field") = "prod_prio"
        dr_data_row1.Item("sort_text") = global_trans.translate_label("label_sort_priority", "Standaard", Me.global_ws.Language)
        dt_data_table.Rows.Add(dr_data_row1)

        Me.cbo_sort_by1.DataTextField = "sort_text"
        Me.cbo_sort_by1.DataValueField = "sort_field"
        Me.cbo_sort_by1.DataSource = dt_data_table
        Me.cbo_sort_by1.DataBind()

        Me.cbo_sort_by2.DataTextField = "sort_text"
        Me.cbo_sort_by2.DataValueField = "sort_field"
        Me.cbo_sort_by2.DataSource = dt_data_table
        Me.cbo_sort_by2.DataBind()

        Try
            Me.cbo_sort_by1.SelectedValue = Me.get_page_parameter("Sort")
            Me.cbo_sort_by2.SelectedValue = Me.get_page_parameter("Sort")

            _sort_field = Me.get_page_parameter("Sort")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _page_range_1 = CInt(global_trans.translate_label("label_product_count_1", "15", Me.global_ws.Language))
        _page_range_2 = CInt(global_trans.translate_label("label_product_count_2", "30", Me.global_ws.Language))
        _page_range_3 = CInt(global_trans.translate_label("label_product_count_3", "45", Me.global_ws.Language))

        Me.set_javascript("uc_pp_products_overview.js", Me)
        Me.set_style_sheet("uc_pp_products_overview.css", Me)

        Me.hidden_item_size.Value = "col-md-" + Me.global_cms.get_cms_setting("item_layout")

        If Me.global_cms.get_cms_setting("product_list_type") = 2 Then
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "set_blocks", "set_blocks();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "set_list", "set_list();", True)
        End If

        ' Sla de url op in een onzichtbare label
        If Not Me.Page.IsPostBack() Then
            Me.lbl_raw_url.Text = Request.RawUrl.ToString()
        End If

        Me._page_name = Me.Page.AppRelativeVirtualPath

        If Not Request.QueryString.Count > 1 Then
            Me.set_pager_ranges()
            Me.set_sort_by_items()
            Me.set_product_list()
            Me.set_pager_text()
            Me.set_pager_navigation(False)
        Else
            Me.set_pager_ranges()
            Me.set_sort_by_items()
            Me.set_product_list()
            Me.set_pager_text()
            Me.set_pager_navigation(False)
        End If

        If Me._dtProducts.Rows.Count = 0 Then
            Me.ph_no_items.Visible = True
            Me.ph_product_list.Visible = False
        End If

    End Sub

    Protected Sub cbo_items_per_page1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_items_per_page1.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("pagesize", Me.cbo_items_per_page1.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub
    Protected Sub cbo_items_per_page2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_items_per_page2.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("pagesize", Me.cbo_items_per_page2.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub

    Protected Sub cbo_sort_by1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_sort_by1.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("sort", Me.cbo_sort_by1.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub
    Protected Sub cbo_sort_by2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_sort_by2.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("sort", Me.cbo_sort_by2.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub

    Private clearfixcount As Integer = 0
    Private mobile_clearfix As Integer = 0
    Protected Sub rpt_product_list_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_product_list.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            clearfixcount += CInt(Me.global_cms.get_cms_setting("item_layout"))
            mobile_clearfix += 6

            If clearfixcount = 12 Then
                e.Item.FindControl("ph_clear_fix").Visible = True
                clearfixcount = 0
            End If

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clear_fix").Visible = True
                mobile_clearfix = 0
            End If

            Dim uc_product_row As Utilize.Web.Solutions.CMS.cms_user_control = LoadUserControl("~/Cms/Modules/ProductPresentation/Products/uc_pp_product_row.ascx")
            uc_product_row.ID = "uc_pp_product_row"
            uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)

            Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
            ph_product_row.Controls.Add(uc_product_row)
        End If
    End Sub
End Class

