﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_pp_freetext_filter.ascx.vb" Inherits="uc_product_freetext_filter" %>
<utilize:literal runat="server" ID="lt_url" Text="" Visible="false"></utilize:literal>

<div class="uc_pp_freetext_filter filter-by-block <%= Me.GetFilterType() %>">
    <h2><utilize:translatetitle runat="server" ID="title_pp_free_text_filters" Text="Filters"></utilize:translatetitle></h2>

    <div class="row">
        <utilize:placeholder runat="server" ID="ph_free_text1">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_1_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_1_div" href="#filter_div_1">
                                    <utilize:translatelabel runat="server" ID="label_pp_free_text1" Text="Vrije tekst 1" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_1" class="panel-collapse collapse <%=Me.GetToggle(1)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text1">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "free_text1")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>
                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text1" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text2">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_2_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_2_div" href="#filter_div_2">
                                    <utilize:translatelabel runat="server" ID="label_pp_free_text2" Text="Vrije tekst 2" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_2" class="panel-collapse collapse <%=Me.GetToggle(2)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text2">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "free_text2")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text2" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text3">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_3_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_3_div" href="#filter_div_3">
                                    <utilize:translatelabel runat="server" ID="label_pp_free_text3" Text="Vrije tekst 3" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_3" class="panel-collapse collapse <%=Me.GetToggle(3)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text3">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "free_text3")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text3" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>    

        <utilize:placeholder runat="server" ID="ph_free_text4">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_4_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_4_div" href="#filter_div_4">
                                    <utilize:translatelabel runat="server" ID="label_pp_free_text4" Text="Vrije tekst 4" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_4" class="panel-collapse collapse <%=Me.GetToggle(4)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text4">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "free_text4")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text4" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text5">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_5_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_5_div" href="#filter_div_5">
                                    <utilize:translatelabel runat="server" ID="label_pp_free_text5" Text="Vrije tekst 5" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_5" class="panel-collapse collapse <%=Me.GetToggle(5)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text5">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "free_text5")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text5" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
    </div>
</div>