﻿
Partial Class uc_product_freetext_filter
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Dim ll_ff1_avail As Boolean = True
    Dim ll_ff2_avail As Boolean = True
    Dim ll_ff3_avail As Boolean = True
    Dim ll_ff4_avail As Boolean = True
    Dim ll_ff5_avail As Boolean = True

    Public Enum FilterTypes
        Checkboxes = 0
        Dropdownlist = 1
    End Enum

    Private _FilterType As FilterTypes = FilterTypes.Checkboxes
    Public Property FilterType As FilterTypes
        Get
            Return _FilterType
        End Get
        Set(value As FilterTypes)
            _FilterType = value
        End Set
    End Property

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.IsPostBack() Then
            Me.lt_url.Text = Request.RawUrl.ToString()
        End If

        ' Variabele die bepaalt of de control getoond moet worden
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim ll_show As Boolean = False

            If ll_ff1_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(1)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text1.DataSource = udt_data_table
                    Me.rpt_free_text1.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text1.DataValueField = "free_text1"
                    Me.cbo_free_text1.DataTextField = "free_text1"
                    Me.cbo_free_text1.DataSource = udt_data_table
                    Me.cbo_free_text1.DataBind()

                    Me.cbo_free_text1.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))

                    If Not Me.IsPostBack() Then
                        Me.cbo_free_text1.SelectedValue = Me.get_page_parameter(("free_text1_" + Me.global_cms.Language))
                    End If
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text1.Visible = False
                End If
            End If

            If ll_ff2_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(2)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text2.DataSource = udt_data_table
                    Me.rpt_free_text2.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text2.DataValueField = "free_text2"
                    Me.cbo_free_text2.DataTextField = "free_text2"
                    Me.cbo_free_text2.DataSource = udt_data_table
                    Me.cbo_free_text2.DataBind()

                    Me.cbo_free_text2.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text2.SelectedValue = Me.get_page_parameter(("free_text2_" + Me.global_cms.Language))
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text2.Visible = False
                End If
            End If

            If ll_ff3_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(3)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text3.DataSource = udt_data_table
                    Me.rpt_free_text3.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text3.DataValueField = "free_text3"
                    Me.cbo_free_text3.DataTextField = "free_text3"
                    Me.cbo_free_text3.DataSource = udt_data_table
                    Me.cbo_free_text3.DataBind()

                    Me.cbo_free_text3.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text3.SelectedValue = Me.get_page_parameter("free_text3_" + Me.global_cms.Language)
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text3.Visible = False
                End If
            End If

            If ll_ff4_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(4)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text4.DataSource = udt_data_table
                    Me.rpt_free_text4.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text4.DataValueField = "free_text4"
                    Me.cbo_free_text4.DataTextField = "free_text4"
                    Me.cbo_free_text4.DataSource = udt_data_table
                    Me.cbo_free_text4.DataBind()

                    Me.cbo_free_text4.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text4.SelectedValue = Me.get_page_parameter("free_text4_" + Me.global_cms.Language)
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text4.Visible = False
                End If
            End If

            If ll_ff5_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(5)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text5.DataSource = udt_data_table
                    Me.rpt_free_text5.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text5.DataValueField = "free_text5"
                    Me.cbo_free_text5.DataTextField = "free_text5"
                    Me.cbo_free_text5.DataSource = udt_data_table
                    Me.cbo_free_text5.DataBind()

                    Me.cbo_free_text5.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text5.SelectedValue = Me.get_page_parameter("free_text5_" + Me.global_cms.Language)
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text5.Visible = False
                End If
            End If

            ll_resume = ll_show
        End If

        If Not ll_resume Then
            Me.Visible = False
        Else
            Me.set_style_sheet("uc_pp_freetext_filter.css", Me)
        End If
    End Sub

    Friend Function get_url(ByVal ff_field As String, ByVal free_text As String) As String
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter(ff_field, free_text)

        Return Me.ResolveCustomUrl(url_class.get_url())
    End Function

    Private Function get_freetext_values(ByVal list_nr As Integer) As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_from = "ucm_products"
        qsc_select.select_fields = "distinct CASE WHEN ISNUMERIC(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + ") = 1 Then CAST(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + " As FLOAT) When ISNUMERIC(LEFT(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + ",1)) = 0 Then ASCII(LEFT(LOWER(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + "),1)) Else 2147483647 End, free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + " as free_text" + list_nr.ToString()
        qsc_select.select_where = " prod_code In (Select prod_code from ucm_categories_prod where cat_code = @cat_code) "
        qsc_select.select_where += " And free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + " <> '' "
        qsc_select.select_where += " And shop_code = @shop_code"
        qsc_select.select_order = "CASE WHEN ISNUMERIC(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + ") = 1 Then CAST(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + " As FLOAT) When ISNUMERIC(LEFT(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + ",1)) = 0 Then ASCII(LEFT(LOWER(free_text" + list_nr.ToString() + "_" + Me.global_cms.Language + "),1)) Else 2147483647 End"

        qsc_select.add_parameter("cat_code", Me.get_page_parameter("catcode"))
        qsc_select.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())
        Return qsc_select.execute_query()
    End Function

    Protected Sub rpt_free_text1_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_free_text1.ItemCreated, _
                                                                                                                    rpt_free_text2.ItemCreated, rpt_free_text3.ItemCreated, _
                                                                                                                    rpt_free_text4.ItemCreated, rpt_free_text5.ItemCreated

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            ' Bepaal het nummer van de lijst
            Dim ln_list_nr As Integer = CInt(CType(sender, repeater).ID.Replace("rpt_free_text", ""))

            ' Referentie naar de filter en zet de checked
            Dim chk_filter As Utilize.Web.UI.WebControls.checkbox = e.Item.FindControl("chk_free_text1")

            Dim ll_active As Boolean = Me.get_page_parameter("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language) = e.Item.DataItem("free_text" + ln_list_nr.ToString())

            If ll_active Then
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")
                url_class.set_parameter("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language, "")

                chk_filter.Checked = True
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            Else
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")
                url_class.set_parameter("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language, Server.UrlEncode(e.Item.DataItem("free_text" + ln_list_nr.ToString())))

                chk_filter.Checked = False
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            End If
        End If
    End Sub

    Protected Sub cbo_free_text_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_free_text1.SelectedIndexChanged, _
                                                                                                cbo_free_text2.SelectedIndexChanged, cbo_free_text3.SelectedIndexChanged, _
                                                                                                cbo_free_text4.SelectedIndexChanged, cbo_free_text5.SelectedIndexChanged

        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
        url_class.set_parameter("page", "0")

        For ln_list_nr As Integer = 1 To 5
            Dim cbo_free_text As dropdownlist = Me.FindControl("cbo_free_text" + ln_list_nr.ToString())

            If cbo_free_text.SelectedValue = "" Then
                url_class.remove_parameter("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language)
            Else
                url_class.set_parameter("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language, Server.UrlEncode(cbo_free_text.SelectedValue))
            End If
        Next

        Dim lc_url As String = url_class.get_url()

        For ln_list_nr As Integer = 1 To 5
            Dim cbo_free_text As dropdownlist = Me.FindControl("cbo_free_text" + ln_list_nr.ToString())

            If Not cbo_free_text.SelectedValue = "" Then
                lc_url = lc_url.Replace("free_text" + ln_list_nr.ToString() + "_" + Me.global_ws.Language + "=" + Server.UrlEncode(cbo_free_text.SelectedValue).ToLower(), "free_text" + ln_list_nr.ToString() + "=" + Server.UrlEncode(cbo_free_text.SelectedValue))
            End If
        Next

        Response.Redirect(lc_url, True)
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.rpt_free_text1.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text2.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text3.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text4.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text5.Visible = Me._FilterType = FilterTypes.Checkboxes

        Me.cbo_free_text1.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text2.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text3.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text4.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text5.Visible = Me._FilterType = FilterTypes.Dropdownlist
    End Sub

    Friend Function GetToggle(ByVal list_nr As Integer) As String
        Dim lcReturnValue As String = ""

        If Not Me.get_page_parameter("free_text" + list_nr.ToString() + "_" + Me.global_ws.Language) = "" Then
            lcReturnValue = "in"
        Else
            If Me._FilterType = FilterTypes.Dropdownlist Then
                lcReturnValue = "in"
            End If

        End If

        Return lcReturnValue
    End Function

    Friend Function GetFilterType() As String
        Dim lcReturnValue As String = ""

        If FilterType = FilterTypes.Checkboxes Then
            lcReturnValue = "filterlist-checkbox"
        Else
            lcReturnValue = "filterlist-dropdown"
        End If

        Return lcReturnValue
    End Function
End Class
