﻿function setOverview() {
    if (sessionStorage.getItem("view_state") == null) {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("onload_state") == "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("onload_state") == "list") {
                show_list();
            }
        }
    } else {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("view_state") == "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("view_state") == "list") {
                show_list();
            }
            if (sessionStorage.getItem("view_state") == "table") {
                show_table();
            }
        }
    }
}
function set_start() {
    $(".uc_pp_products_overview .uc_placeholder").removeClass().addClass("uc_placeholder");

    $(".uc_pp_products_overview .image-placeholder").removeClass().addClass("image-placeholder img-responsive");

    $(".uc_pp_products_overview .image-block").css("display", "block");
    $(".uc_pp_products_overview .item-border").css("display", "block");


    $(".uc_pp_products_overview .image-block").removeClass().addClass("image-block");

    $(".uc_pp_products_overview .information-block").removeClass().addClass("information-block");


    $(".uc_pp_products_overview .item-border").removeClass().addClass("item-border");
    $(".uc_pp_products_overview .containter-placeholder").removeClass().addClass("containter-placeholder");
    $(".uc_pp_products_overview .row-placeholder").removeClass().addClass("row-placeholder");

    $(".uc_pp_products_overview .list-only").css("display", "block");

    $(".fa-th").removeClass("active");
    $(".fa-bars").removeClass("active");
    $(".fa-th-list").removeClass("active");
}

function show_list() {
    set_start();
    var size = $(".uc_pp_products_overview .hidden_item_size").val();
    $(".uc_pp_products_overview .uc_placeholder").addClass("uc_pp_product_row col-xs-6 col-md-12 col-sm-12");
    $(".uc_pp_products_overview .uc_pp_product_block").removeClass("col-xs-6 col-sm-6 uc_pp_product_block " + size);

    $(".uc_pp_products_overview .image-placeholder").addClass("sm-margin-bottom-20");

    $(".uc_pp_products_overview .image-block").addClass("col-sm-4");

    $(".uc_pp_products_overview .information-block").addClass("col-sm-8");


    $(".uc_pp_products_overview .content-placeholder").addClass("content col-xs-12");
    $(".uc_pp_products_overview .content-placeholder").removeClass("illustration-v2 row");

    $(".uc_pp_products_overview .item-border").addClass("list-product-description product-description-brd margin-bottom-30");
    $(".uc_pp_products_overview .containter-placeholder").addClass("container-fluid");
    $(".uc_pp_products_overview .row-placeholder").addClass("row");

    $(".uc_pp_products_overview .product_info_order").css("display", "none");

    $(".fa-th-list").addClass("active");

    sessionStorage.setItem("view_state", "list");
}

function show_blocks(mobile) {
    mobile = mobile || false;
    set_start();
    var size = $(".uc_pp_products_overview .hidden_item_size").val();
    $(".uc_pp_products_overview .uc_placeholder").addClass("col-xs-6 col-sm-6 uc_pp_product_block " + size);

    $(".uc_pp_products_overview .image-placeholder").addClass("full-width");

    $(".uc_pp_products_overview .image-block").addClass("product-img product-img-brd");

    $(".uc_pp_products_overview .information-block").addClass("product-description-brd margin-bottom-30");

    $(".uc_pp_products_overview .content-placeholder").addClass("illustration-v2 col-xs-12");

    $(".uc_pp_products_overview .list-only").css("display", "none");

    $(".fa-th").addClass("active");
    if (!mobile) {
        sessionStorage.setItem("view_state", "blocks");
    }
}

function show_table() {
    set_start();
    var size = $(".uc_pp_products_overview .hidden_item_size").val();
    $(".uc_pp_products_overview .uc_placeholder").addClass("uc_product_table");

    $(".uc_pp_products_overview .item-border").css("display", "none")
    $(".uc_pp_products_overview .uc_pp_product_block").removeClass("uc_pp_product_block " + size);
    $(".uc_pp_products_overview .content-placeholder").removeClass("illustration-v2 row");

    $(".fa-bars").addClass("active");

    sessionStorage.setItem("view_state", "table");
}

function set_blocks() {
    sessionStorage.setItem("onload_state", "blocks");
    setOverview();
}

function set_list() {
    sessionStorage.setItem("onload_state", "list");
    setOverview();
}

$(window).resize(function () {
    if ($(window).width() < 991) {
        show_blocks(true);
    } else {
        setOverview();
    }
});