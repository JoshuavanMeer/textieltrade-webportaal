﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_mailing.ascx.vb" Inherits="uc_mailing" %>

<utilize:panel runat="server" ID="pnl_search" DefaultButton="button_mailing_register" CssClass="uc_mailing">
    <div ID="div_mailing" class="uc_mailing" runat="server">
        <h2><utilize:translatetitle runat="server" ID="title_register_mailing" Text="Aanmelden nieuwsbrief"></utilize:translatetitle></h2>
        <div class="form-group">
            <utilize:textbox runat="server" ID="txt_email_address" CssClass="form-control"></utilize:textbox>
        </div>
        <utilize:translatebutton runat="server" ID="button_mailing_register" Text="Aanmelden" CssClass="btn-u btn-u-sea-shop btn-u-lg middle"/> 
    </div>                       
</utilize:panel>
