﻿
Imports System.Net.Mail
Imports Utilize.Data.API.CMS

Partial Class uc_mailing
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_mailing.css", Me)

            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

            'If advanced styling has been set, add the css classes according to the CMS settings for the current mailing block
            If dr_data_row.Item("mailing_advanced") Then
                Dim uc_mailing As System.Web.UI.HtmlControls.HtmlGenericControl = Me.div_mailing
                Dim classes As String = uc_mailing.Attributes.Item("class")
                uc_mailing.Attributes.Add("class", classes + " advanced " + Me.block_table + Me.block_rec_id)

                Dim advancedStylingLocation As String = "/Styles/AdvancedMailing/" + Me.block_table + Me.block_rec_id + ".css"
                If Not System.IO.File.Exists(Server.MapPath(advancedStylingLocation)) Then
                    Dim styleBuilder = StyleBuilderFactory.GetStyleBuilder("mailing", Me.block_rec_id, Me.block_table)
                    styleBuilder.GenerateCssFile(advancedStylingLocation)
                End If

                Me.set_style_sheet(advancedStylingLocation, Me)
            End If
        End If

        If ll_resume Then
            Me.txt_email_address.Attributes.Add("placeholder", global_trans.translate_label("label_mailing_email_address", "Uw email adres", Me.global_cms.Language))
        End If
    End Sub

    Protected Sub button_mailing_register_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_mailing_register.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.txt_email_address.Text.Trim() = ""

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "mailing_email_address_empty", "alert('" + global_trans.translate_message("message_email_address_empty", "Uw email adres is niet ingevuld.", Me.global_cms.Language) + "');", True)
            End If
        End If

        If ll_resume Then
            Try
                Dim email_address_check As MailAddress = New MailAddress(Me.txt_email_address.Text)
                ll_resume = True
            Catch ex As Exception
                ll_resume = False
            End Try

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "mailing_email_address_empty", "alert('" + global_trans.translate_message("message_email_address_incorrect", "Het ingevulde emailadres is niet correct.", Me.global_cms.Language) + "');", True)
            End If
        End If

        If ll_resume Then
            Dim lo_mailing_object As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("ucm_mailings", Me.txt_email_address.Text, "ml_email")

            ' Hier controleren of het emailadres bestaat, zo nee een regel toevoegen met het vinkje "Ja/Nee"
            If lo_mailing_object.data_tables("ucm_mailings").data_row_count = 0 Then
                lo_mailing_object.table_insert_row("ucm_mailings", Me.global_ws.user_information.user_id)
            End If

            lo_mailing_object.field_set("ml_email", Me.txt_email_address.Text)
            lo_mailing_object.field_set("ml_wants_mailing", True)

            ll_resume = lo_mailing_object.table_update(Me.global_ws.user_information.user_id)
        End If

        If ll_resume Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "mailing_registration_succeeded", "alert('" + global_trans.translate_message("message_mailing_registration_succeeded", "Hartelijk dank voor uw aanmelding voor de nieuwsbrief.", Me.global_cms.Language) + "');", True)
        End If
    End Sub
End Class
