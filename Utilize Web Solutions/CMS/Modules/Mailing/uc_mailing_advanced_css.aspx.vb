﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim rec_id = Request.Params("rec_id")
        Dim block_table = Request.Params("block_table")
        Dim clientid = Request.Params("clientid")

        If ll_resume Then
            ll_resume = Not rec_id Is Nothing And Not block_table Is Nothing
        End If

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(block_table, rec_id)
            Dim lc_style_string As New StringBuilder

            lc_style_string.Append("#" + clientid + ".uc_mailing_footer.advanced .inputbutton,").AppendLine()
            lc_style_string.Append("#" + clientid + ".uc_mailing.advanced .inputbutton").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-color: " + dr_data_row.Item("mailing_btn_backgr").ToString() + "!important;").AppendLine()
            lc_style_string.Append("color: " + dr_data_row.Item("mailing_btn_font_col").ToString() + "!important;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.Append("#" + clientid + ".uc_mailing_footer.advanced .form-control,").AppendLine()
            lc_style_string.Append("#" + clientid + ".uc_mailing.advanced .form-control").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-color: " + dr_data_row.Item("mailing_input_backgr").ToString() + "!important;").AppendLine()
            lc_style_string.Append("border: 1px solid " + dr_data_row.Item("mailing_input_border").ToString() + "!important;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.ToString()

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_style_string)
            Response.End()
        End If
    End Sub
End Class