﻿Partial Class uc_breadcrumbs
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_breadcrumb_block.css", Me)
        Me.set_javascript("uc_breadcrumb_block.js", Me)

        If Me.IsPostBack() Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "move_breadcrumbs", "$( document ).ready(function() {move_breadcrumbs();});", True)
        End If
    End Sub

    ''' <summary>
    ''' Dit is de arraylist met breadcrumbs
    ''' </summary>
    ''' <remarks></remarks>
    Private al_bread_crumbs As New ArrayList

    ''' <summary>
    ''' Dit is een class om breadcrumbs op te slaan.
    ''' </summary>
    ''' <remarks></remarks>

    <Serializable()>
    Private Class bc_bread_crumb
        Public page_title As String = ""
        Public page_url As String = ""
    End Class

#Region " Methoden om de breadcrumbs te zetten"
    ''' <summary>
    ''' Deze property retourneert de kale naam van de pagina.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Title As String
        Get
            Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
            lc_title = IIf(lc_title = "", Me.Page.Title, Me.Page.Title.Replace(lc_title + " : ", ""))

            Return lc_title
        End Get
    End Property

    ''' <summary>
    ''' Deze sub handelt de account breadcrumbs af
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_account_breadcrumbs()
        ' Er wordt altijd een standaard "Mijn account" breadcrumb toegevoegd
        Dim bc_account As New bc_bread_crumb
        bc_account.page_title = global_trans.translate_title("title_my_account", "Mijn account", Me.global_ws.Language)
        bc_account.page_url = "~/" + Me.global_ws.Language + "/account/account.aspx"
        al_bread_crumbs.Add(bc_account)

        If Not Me.url_rewritten.Contains("account.aspx") Then
            ' Als we niet in het mijn account gedeelte zitten, dan kunnen we de laatste breadcrumb toevoegen
            Dim bc_target As New bc_bread_crumb
            bc_target.page_title = Me.Title
            bc_target.page_url = Me.lbl_url_raw.Text

            al_bread_crumbs.Add(bc_target)
        End If
    End Sub

    ''' <summary>
    ''' Deze sub handelt de paymentprocess breadcrumbs af
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_paymentprocess_breadcrumbs()
        Dim bc_shopping_cart As New bc_bread_crumb
        bc_shopping_cart.page_title = global_trans.translate_title("title_shopping_cart", "Winkelwagen", Me.global_ws.Language)
        bc_shopping_cart.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/shopping_cart.aspx"

        Dim bc_identification As New bc_bread_crumb
        bc_identification.page_title = global_trans.translate_title("title_identification", "Identificatie", Me.global_ws.Language)
        bc_identification.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/identification.aspx"

        Dim bc_check_order As New bc_bread_crumb
        bc_check_order.page_title = global_trans.translate_title("title_check_order", "Overzicht bestelling", Me.global_ws.Language)
        bc_check_order.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/check_order.aspx"

        Dim bc_confirmation As New bc_bread_crumb
        bc_confirmation.page_title = global_trans.translate_title("title_confirmation", "Mijn account", Me.global_ws.Language)
        bc_confirmation.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/order_confirmation.aspx"

        Dim bc_request_password As New bc_bread_crumb
        bc_request_password.page_title = global_trans.translate_title("title_request_password", "Mijn account", Me.global_ws.Language)
        bc_request_password.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/request_password.aspx"

        Dim bc_registration As New bc_bread_crumb
        bc_registration.page_title = global_trans.translate_title("title_registration", "Registreren", Me.global_ws.Language)
        bc_registration.page_url = "~/" + Me.global_ws.Language + "/webshop/paymentprocess/register.aspx"

        ' Afhankelijk van de pagina waar we opzitten meoten een of meerdere pagina's toegevoegd worden.
        Select Case True
            Case Me.url_rewritten.Contains("request_password.aspx")
                Me.al_bread_crumbs.Add(bc_request_password)
            Case Me.url_rewritten.Contains("shopping_cart.aspx")
                Me.al_bread_crumbs.Add(bc_shopping_cart)
            Case Me.url_rewritten.Contains("identification.aspx")
                Me.al_bread_crumbs.Add(bc_shopping_cart)
                Me.al_bread_crumbs.Add(bc_identification)
            Case Me.url_rewritten.Contains("check_order.aspx")
                Me.al_bread_crumbs.Add(bc_shopping_cart)
                Me.al_bread_crumbs.Add(bc_identification)
                Me.al_bread_crumbs.Add(bc_check_order)
            Case Me.url_rewritten.Contains("register.aspx")
                Me.al_bread_crumbs.Add(bc_registration)
            Case Me.url_rewritten.Contains("order_confirmation.aspx")
                bc_confirmation.page_title = Me.Title

                Me.al_bread_crumbs.Add(bc_shopping_cart)
                Me.al_bread_crumbs.Add(bc_identification)
                Me.al_bread_crumbs.Add(bc_check_order)
                Me.al_bread_crumbs.Add(bc_confirmation)
            Case Else
                Exit Select
        End Select
    End Sub

    ''' <summary>
    ''' Deze sub handelt de breadcrumbs voor de categorieën pagina af.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_categories_breadcrumbs()
        Dim bub_url_class As New Utilize.Web.Solutions.base_url_base(FrameWork.FrameworkProcedures.GetRequestUrlAbsoluteUri())

        Dim lc_cat_selected As String = bub_url_class.get_parameter("cat_code")

        If lc_cat_selected = "" Then
            Dim bc_category As New bc_bread_crumb
            bc_category.page_title = global_trans.translate_label("label_webshop", "Webshop", Me.global_ws.Language)
            bc_category.page_url = Request.Url.AbsoluteUri.ToString()

            al_bread_crumbs.Add(bc_category)
        Else
            Dim lc_cat_list As String = bub_url_class.get_parameter("cat_parent")

            Dim la_cat_list As String() = lc_cat_list.Split("|")

            For I As Integer = la_cat_list.Length - 1 To 0 Step -1
                If Not la_cat_list(I).ToString() = "" Then
                    Dim bc_category As New bc_bread_crumb
                    bc_category.page_title = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", la_cat_list(I).ToString() + Me.global_ws.Language, "cat_code + lng_code")
                    bc_category.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", la_cat_list(I).ToString() + Me.global_ws.Language, "cat_code + lng_code")

                    al_bread_crumbs.Add(bc_category)
                End If
            Next

            Dim bc_category_selected As New bc_bread_crumb
            bc_category_selected.page_title = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", lc_cat_selected.ToString() + Me.global_ws.Language, "cat_code + lng_code")
            bc_category_selected.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", lc_cat_selected.ToString() + Me.global_ws.Language, "cat_code + lng_code")

            al_bread_crumbs.Add(bc_category_selected)
        End If
    End Sub

    ''' <summary>
    ''' Deze sub handelt de breadcrumbs voor de productenpresentatie categorieën pagina af.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_pp_categories_breadcrumbs()
        Dim bub_url_class As New Utilize.Web.Solutions.base_url_base(FrameWork.FrameworkProcedures.GetRequestUrlAbsoluteUri())

        Dim lc_cat_selected As String = bub_url_class.get_parameter("CatCode")

        If lc_cat_selected = "" Then
            Dim bc_category As New bc_bread_crumb
            bc_category.page_title = global_trans.translate_label("label_presentation", "Productpresentatie", Me.global_cms.Language)
            bc_category.page_url = FrameWork.FrameworkProcedures.GetRequestUrlAbsoluteUri()

            al_bread_crumbs.Add(bc_category)
        Else
            Dim lc_cat_list As String = bub_url_class.get_parameter("CatParent")

            Dim la_cat_list As String() = lc_cat_list.Split("|")

            For I As Integer = la_cat_list.Length - 1 To 0 Step -1
                If Not la_cat_list(I).ToString() = "" Then
                    Dim bc_category As New bc_bread_crumb
                    bc_category.page_title = Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_desc_" + Me.global_ws.Language, la_cat_list(I).ToString())
                    bc_category.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_categories", "tgt_url_" + Me.global_ws.Language, la_cat_list(I).ToString())

                    al_bread_crumbs.Add(bc_category)
                End If
            Next

            Dim bc_category_selected As New bc_bread_crumb
            bc_category_selected.page_title = Utilize.Data.DataProcedures.GetValue("ucm_categories", "cat_desc_" + Me.global_ws.Language, lc_cat_selected.ToString())
            bc_category_selected.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_categories", "tgt_url_" + Me.global_ws.Language, lc_cat_selected.ToString())

            al_bread_crumbs.Add(bc_category_selected)
        End If
    End Sub

    ''' <summary>
    ''' Deze sub handelt de breadcrumbs van de website structuur af.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_structure_breadcrumbs()
        Dim bub_url_class As New Utilize.Web.Solutions.base_url_base(FrameWork.FrameworkProcedures.GetRequestUrl())

        Dim lc_us_selected As String = bub_url_class.get_parameter("uscode")
        Dim lc_us_list As String = bub_url_class.get_parameter("usparent")

        Dim la_us_list As String() = lc_us_list.Split("|")

        For I As Integer = la_us_list.Length - 1 To 0 Step -1
            If Not la_us_list(I).ToString() = "" Then
                Dim bc_category As New bc_bread_crumb
                bc_category.page_title = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_desc_" + Me.global_cms.Language, la_us_list(I).ToString())

                Dim lc_us_page_type As String = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_page_type", la_us_list(I).ToString())

                Select Case lc_us_page_type
                    Case "TEXT"
                        bc_category.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_cms.Language, la_us_list(I).ToString())
                    Case "DEFAULT"
                        bc_category.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_cms.Language, la_us_list(I).ToString())
                    Case "LINK_EXTERNAL"
                        bc_category.page_url = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_page_url", la_us_list(I).ToString())
                    Case "UWS_CATEGORIES"
                        bc_category.page_url = "~/Webshop/product_list.aspx?cat_code=" & Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_prod_cat", la_us_list(I).ToString())
                    Case "LINK_INTERNAL"
                        Dim lc_page_url As String = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_page_url", la_us_list(I).ToString())

                        ' Controleer hoe de pagina gestart wordt
                        If lc_page_url.ToLower().StartsWith("http://") Then
                            bc_category.page_url = lc_page_url
                        Else
                            bc_category.page_url = "~/" + lc_page_url
                        End If

                    Case Else
                        Exit Select
                End Select

                ' De home link voegen we niet toe, dat gebeurt al eerder...
                If Not bc_category.page_title.ToLower() = "home" Then
                    al_bread_crumbs.Add(bc_category)
                End If
            End If
        Next

        Dim bc_category_selected As New bc_bread_crumb
        bc_category_selected.page_title = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_desc_" + Me.global_cms.Language, lc_us_selected)
        bc_category_selected.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_cms.Language, lc_us_selected)

        ' De home link voegen we niet toe, dat gebeurt al eerder...
        If Not bc_category_selected.page_title.ToLower() = "home" Then
            al_bread_crumbs.Add(bc_category_selected)
        End If
    End Sub

    ''' <summary>
    ''' Deze sub handelt de breadcrumbs van de nieuwsberichten af
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_news_breadcrumbs()
        Dim news_id As String = Me.get_page_parameter("recid")
        Dim news_hdr_id As String = Utilize.Data.DataProcedures.GetValue("ucm_news", "hdr_id", news_id)

        If Not news_hdr_id.Trim() = "" Then
            Dim qsc_select As New Utilize.Data.QueryCustomSelectClass
            qsc_select.QueryString = "select us_code from ucm_structure_blocks where news_block_id = @news_block_id and block_type = 'NEWS_ARCHIVE' "
            qsc_select.QueryString += "UNION "
            qsc_select.QueryString += "select us_code from ucm_structure_blocks where news_block_id = @news_block_id and block_type = 'NEWS' "
            qsc_select.add_parameter("news_block_id", news_hdr_id)

            Dim dt_news As System.Data.DataTable = qsc_select.execute_query(Me.global_ws.user_information.user_id)

            If dt_news.Rows.Count > 0 Then
                Dim lc_us_code As String = dt_news.Rows(0).Item("us_code")

                Dim bc_news_page As New bc_bread_crumb
                bc_news_page.page_title = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_desc_" + Me.global_cms.Language, lc_us_code)
                bc_news_page.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_cms.Language, lc_us_code)

                ' De home link voegen we niet toe, dat gebeurt al eerder...
                If Not bc_news_page.page_title.ToLower() = "home" Then
                    al_bread_crumbs.Add(bc_news_page)
                End If

                Dim bc_news_item As New bc_bread_crumb
                bc_news_item.page_title = Utilize.Data.DataProcedures.GetValue("ucm_news", "title_" + Me.global_cms.Language, news_id)
                bc_news_item.page_url = "~/" + Utilize.Data.DataProcedures.GetValue("ucm_news", "tgt_url_" + Me.global_cms.Language, news_id)

                al_bread_crumbs.Add(bc_news_item)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Dit onderdeel vult de overige breadcrumbs
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fill_other_breadcrumbs()
        ' Dim lc_link As String = Request.RawUrl.ToString()
        ' link_us_parent.NavigateUrl = "~/" + lc_link
        Dim bc_other As New bc_bread_crumb
        bc_other.page_title = Me.Title
        bc_other.page_url = Me.lbl_url_raw.Text

        al_bread_crumbs.Add(bc_other)
    End Sub

    ''' <summary>
    ''' In deze sub worden alle breadcrumbs op de pagina toegevoegd
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub create_breadcrumbs()
        Dim ln_teller As Integer = 0

        Dim ol_breadcrumb As New HtmlGenericControl("ul")
        ol_breadcrumb.Attributes.Add("class", "breadcrumb-v5 row")

        ' Voeg eventueel een seperator toe
        Dim span_breadcrumb_text As New HtmlGenericControl("span")
        span_breadcrumb_text.InnerText = global_trans.translate_label("label_you_are_here", "U bevindt zich hier", Me.global_cms.Language)
        Me.panel_breadcrumbs.Controls.Add(span_breadcrumb_text)

        For Each bc_bread_crumb As bc_bread_crumb In al_bread_crumbs
            ' Voeg eventueel een seperator toe
            Dim li_breadcrumb As New HtmlGenericControl("li")

            ' Maak het laatste item in de breadcrumbs active
            If al_bread_crumbs.IndexOf(bc_bread_crumb) = (al_bread_crumbs.Count - 1) Then
                li_breadcrumb.Attributes.Add("class", "active")
                ol_breadcrumb.Controls.Add(li_breadcrumb)
                li_breadcrumb.InnerHtml = bc_bread_crumb.page_title

            Else
                ol_breadcrumb.Controls.Add(li_breadcrumb)

                ' Voeg de breadcrumb toe
                Dim hl_bread_crumb As New Utilize.Web.UI.WebControls.hyperlink
                hl_bread_crumb.Text = bc_bread_crumb.page_title
                hl_bread_crumb.NavigateUrl = Me.ResolveCustomUrl(bc_bread_crumb.page_url)

                li_breadcrumb.Controls.Add(hl_bread_crumb)
            End If
        Next

        Me.panel_breadcrumbs.Controls.Add(ol_breadcrumb)
    End Sub
#End Region

    Private url_default As String = "'"
    Private url_rewritten As String = "'"

    Protected Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Sla hier de raw url op in een label.
        If Not Me.IsPostBack() Then
            Me.lbl_url_raw.Text = Request.RawUrl.ToString().ToLower()
        End If

        ' Sla hier gegevens in variabelen op
        Me.url_default = FrameWork.FrameworkProcedures.GetRequestUrl().ToLower()
        Me.url_rewritten = Me.lbl_url_raw.Text.ToLower()

        ' Er wordt altijd een standaard "Mijn account" breadcrumb toegevoegd
        Dim bc_home As New bc_bread_crumb
        bc_home.page_title = "Home"
        bc_home.page_url = "~/home.aspx?language=" + Me.global_cms.Language
        al_bread_crumbs.Add(bc_home)

        ' Bepaal in welk onderdeel van de webshop we zitten, we kennen: CMS, Webshop, Account, Paymentprocess
        Select Case True
            Case Me.url_rewritten.Contains("/account/")
                Me.fill_account_breadcrumbs()
            Case Me.url_rewritten.Contains("/paymentprocess/")
                Me.fill_paymentprocess_breadcrumbs()
            Case Else
                Select Case True
                    ' CMS
                    Case Me.url_default.Contains("structure_page.aspx")
                        Me.fill_structure_breadcrumbs()
                    ' CMS Productpresentatie
                    Case Me.url_default.Contains("pp_products.aspx")
                        Me.fill_pp_categories_breadcrumbs()
                    Case Me.url_default.Contains("product_search.aspx")
                        ' Het producten zoeken gedeelte
                        Dim bc_search As New bc_bread_crumb
                        bc_search.page_title = global_trans.translate_title("title_search_results", "Zoekresultaten", Me.global_cms.Language)
                        bc_search.page_url = Me.url_rewritten

                        al_bread_crumbs.Add(bc_search)
                    Case Me.url_default.Contains("product_list.aspx")
                        ' Het producten overzicht gedeelte
                        Me.fill_categories_breadcrumbs()
                    Case Me.url_default.Contains("news_page.aspx")
                        Me.fill_news_breadcrumbs()
                    Case Me.url_default.ToLower().Contains("pp_product_details.aspx")
                        ' De product details
                        ' Hier gaan we net wat anders mee om
                        ' We moeten namelijk de vorige url ook meenemen.
                        If Not Session("PPBreadCrumbs") Is Nothing Then
                            al_bread_crumbs = Session("PPBreadCrumbs").clone()
                        End If

                        Me.fill_other_breadcrumbs()
                    Case Me.url_default.ToLower().Contains("product_details.aspx")
                        ' De product details
                        ' Hier gaan we net wat anders mee om
                        ' We moeten namelijk de vorige url ook meenemen.
                        If Not Session("BreadCrumbs") Is Nothing Then
                            al_bread_crumbs = Session("BreadCrumbs").clone()
                        End If

                        Me.fill_other_breadcrumbs()
                    Case Else
                        Me.fill_other_breadcrumbs()
                End Select
        End Select

        If Not Me.url_default.ToLower().Contains("pp_product_details.aspx") Then
            Session("PPBreadCrumbs") = al_bread_crumbs
        End If

        If Not Me.url_default.ToLower().Contains("product_details.aspx") Then
            Session("BreadCrumbs") = al_bread_crumbs
        End If

        ' En maak de broodkruimels aan.
        Me.create_breadcrumbs()
    End Sub
End Class