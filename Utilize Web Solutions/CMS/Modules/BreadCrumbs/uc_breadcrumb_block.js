﻿$(window).load(function () {
    move_breadcrumbs();
});

function move_breadcrumbs() {
    $('.body').first().prepend($('.uc_breadcrumb_block'));

    $('.uc_breadcrumb_block .breadcrumb-v5').each(function () {
        var childCount = $(this).children().length;
        if (childCount > 2) {
            $("<li class='expand'><a href='#'>. . .</a></li>").insertBefore($(this).children()[--childCount]);

            $(".breadcrumb-v5 .expand").click(function () {
                $(".breadcrumb-v5").toggleClass("show");
            });
        }        
    });
}

