﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_breadcrumb_block.ascx.vb" Inherits="uc_breadcrumbs" %>

<utilize:panel ID="panel_breadcrumbs" CssClass="fluid-container uc_breadcrumb_block margin-bottom-10" runat="server">
</utilize:panel>

<utilize:label runat="server" ID="lbl_url_raw" Visible='false'></utilize:label>
<utilize:label runat="server" ID="lbl_url_referrer" Visible='false'></utilize:label>