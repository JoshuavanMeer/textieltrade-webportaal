﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_link_block.ascx.vb" Inherits="uc_link_block" %>
<utilize:panel runat="server" ID="pnl_link_block" CssClass="uc_link_block">
    <div>
    <utilize:placeholder runat="server" ID="ph_link_desc">
        <h2><utilize:literal runat='server' ID="lbl_link_desc"></utilize:literal></h2>
    </utilize:placeholder>
    <ul class="list-unstyled categories">
        <utilize:repeater runat="server" ID="rpt_links">
            <ItemTemplate>
                <li>
                    <utilize:hyperlink runat="server" ID="hl_link">
                        <utilize:Image ID="link_logo" AlternateText='<%# DataBinder.Eval(Container.DataItem, "link_desc_" + Me.global_cms.Language) %>' ImageUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "link_logo")) %>' runat="server" />
                        <utilize:literal ID="label_link_desc" Text='<%# DataBinder.Eval(Container.DataItem, "link_desc_" + Me.global_cms.Language)%>' Visible="false" runat="server"></utilize:literal>
                    </utilize:hyperlink>
                </li>
            </ItemTemplate>
        </utilize:repeater>
    </ul>
    </div>
</utilize:panel>