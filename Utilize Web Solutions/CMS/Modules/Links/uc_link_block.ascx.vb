﻿
Imports Utilize.Data.API.CMS

Partial Class uc_link_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Function target(ByVal inp As String) As String
        If inp = "YES" Then
            Return "_blank"
        End If
        Return ""
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim shopcode = Me.global_ws.shop.get_shop_code()
        Dim ll_resume As Boolean = True

        Dim dt_link_block As System.Data.DataTable = Nothing
        Dim dt_links As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_link_block As New Utilize.Data.QuerySelectClass
            qsc_link_block.select_fields = "*"
            qsc_link_block.select_from = "ucm_link_blocks"
            qsc_link_block.select_order = "link_block_desc"
            qsc_link_block.select_where = "rec_id = @rec_id"
            qsc_link_block.add_parameter("rec_id", Me.block_id)
            dt_link_block = qsc_link_block.execute_query()

            ll_resume = dt_link_block.Rows.Count > 0
        End If

        If ll_resume Then
            Dim qsc_links As New Utilize.Data.QuerySelectClass
            qsc_links.select_fields = "*"
            qsc_links.select_from = "ucm_links"
            qsc_links.select_order = "row_ord"
            qsc_links.select_where = "hdr_id = @hdr_id"
            qsc_links.add_parameter("hdr_id", Me.block_id)

            If Me.global_ws.user_information.user_logged_on Then
                qsc_links.select_where += " and (show_when = @show_when1 or show_when = @show_when2)"
                qsc_links.add_parameter("show_when1", 1)
                qsc_links.add_parameter("show_when2", 2)
            Else
                qsc_links.select_where += " and (show_when = @show_when1 or show_when = @show_when2)"
                qsc_links.add_parameter("show_when1", 1)
                qsc_links.add_parameter("show_when2", 3)
            End If

            dt_links = qsc_links.execute_query()

            ll_resume = dt_links.Rows.Count > 0
        End If

        If ll_resume Then
            ' Controleer of de omschrijving getoond moet worden
            Me.ph_link_desc.Visible = dt_link_block.Rows(0).Item("show_desc")
            ' Zet de omschrijving van het blok
            Me.lbl_link_desc.Text = dt_link_block.Rows(0).Item("link_block_desc_" + Me.global_cms.Language)
        End If

        If ll_resume Then
            If Not String.IsNullOrEmpty(dt_link_block.Rows(0).Item("skin_id")) Then
                Me.skin = dt_link_block.Rows(0).Item("skin_id")
            End If
        End If

        If ll_resume Then
            Me.rpt_links.DataSource = dt_links
            Me.rpt_links.DataBind()
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_link_block.css", Me)

        End If

        Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

        'If advanced styling has been set, add the css classes according to the CMS settings for the current link block
        If ll_resume And dr_data_row.Item("link_advanced") Then
            Dim uc_link_block As Utilize.Web.UI.WebControls.panel = Me.pnl_link_block
            uc_link_block.CssClass += " advanced"

            Select Case dr_data_row.Item("link_text_align")
                Case "1"
                    uc_link_block.CssClass += " align-left"
                Case "2"
                    uc_link_block.CssClass += " align-center"
                Case "3"
                    uc_link_block.CssClass += " align-right"
            End Select

            If dr_data_row.Item("link_set_background") Then
                uc_link_block.CssClass += " bg-color"
            End If

            If dr_data_row.Item("link_horiz_vertic") = "2" Then
                uc_link_block.CssClass += " horizontal-flow"
            End If

            If dr_data_row.Item("link_list_icons") = "1" Then
                uc_link_block.CssClass += " list-icons"
            End If

            uc_link_block.CssClass += " " + Me.block_table + Me.block_rec_id

            Dim advancedStylingLocation As String = "/Styles/AdvancedLink/" + Me.block_table + Me.block_rec_id + ".css"
            If Not System.IO.File.Exists(Server.MapPath(advancedStylingLocation)) Then
                Dim styleBuilder = StyleBuilderFactory.GetStyleBuilder("link", Me.block_rec_id, Me.block_table)
                styleBuilder.GenerateCssFile(advancedStylingLocation)
            End If

            Me.set_style_sheet(advancedStylingLocation, Me)
        End If

        If ll_resume Then
            Me.Visible = True
        End If
    End Sub

    Protected Sub rpt_links_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_links.ItemCreated
        Dim hl_link As System.Web.UI.WebControls.HyperLink = e.Item.FindControl("hl_link")

        If e.Item.DataItem("link_url_" + Me.global_cms.Language).ToString().StartsWith("/") Then
            hl_link.NavigateUrl = Me.ResolveCustomUrl("~/") + e.Item.DataItem("link_url_" + Me.global_cms.Language).ToString().Substring(1)
        Else
            hl_link.NavigateUrl = e.Item.DataItem("link_url_" + Me.global_cms.Language).ToString()
        End If

        Dim label_link_desc As System.Web.UI.WebControls.Literal = e.Item.FindControl("label_link_desc")
        Dim link_logo As System.Web.UI.WebControls.Image = e.Item.FindControl("link_logo")
        Dim row As System.Data.DataRowView = e.Item.DataItem

        label_link_desc.Visible = row.Item("show_desc")
        link_logo.Visible = row.Item("show_logo")

        If row.Item("external_link") = True Then
            hl_link.Target = "_blank"
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.float_right = True Then
            pnl_link_block.CssClass += " pull-right"
        End If
    End Sub
End Class
