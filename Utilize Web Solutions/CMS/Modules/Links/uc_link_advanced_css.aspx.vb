﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim rec_id = Request.Params("rec_id")
        Dim block_table = Request.Params("block_table")
        Dim clientid = Request.Params("clientid")

        If ll_resume Then
            ll_resume = Not rec_id Is Nothing And Not block_table Is Nothing
        End If

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(block_table, rec_id)

            Dim lc_style_string As New StringBuilder

            If dr_data_row.Item("link_set_background") Then
                lc_style_string.Append("#" + clientid + ".uc_link_block.advanced > div").AppendLine()
                lc_style_string.Append("{").AppendLine()
                lc_style_string.Append("background-color: " + dr_data_row.Item("link_backgr_color").ToString + ";").AppendLine()
                lc_style_string.Append("}").AppendLine()
            End If

            lc_style_string.Append("#" + clientid + ".uc_link_block.advanced h2").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("font-size: " + dr_data_row.Item("link_title_size").ToString + "px;").AppendLine()
            lc_style_string.Append("color: " + dr_data_row.Item("link_title_color").ToString + "!important;").AppendLine()

            If dr_data_row.Item("link_title_transform") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If

            lc_style_string.Append("}").AppendLine()

            lc_style_string.Append("#" + clientid + ".uc_link_block.advanced a").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("font-size: " + dr_data_row.Item("link_text_size").ToString + "px;").AppendLine()
            lc_style_string.Append("color: " + dr_data_row.Item("link_text_color").ToString + "!important;").AppendLine()

            If dr_data_row.Item("link_text_transform") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If

            lc_style_string.Append("}").AppendLine()

            Dim lc_css As String = lc_style_string.ToString()

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_css)
            Response.End()
        End If
    End Sub
End Class
