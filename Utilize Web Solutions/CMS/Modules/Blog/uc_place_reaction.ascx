﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_place_reaction.ascx.vb" Inherits="uc_place_reaction" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

<utilize:panel runat="server" cssclass="uc_place_reaction" DefaultButton="button_place_reaction">
    <div class="blog_reaction_place uc_place_reaction">
        <h2>Plaats een reactie <utilize:literal runat="server" ID="lt_reaction"></utilize:literal></h2>
        <p>Je emailadres wordt niet gepubliceerd.</p>

        <div class="sky-form">
            <fieldset>
                <section>
                    <label class="label">Naam<span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input"><utilize:textbox runat="server" ID="txt_name" MaxLength="80"></utilize:textbox></label>
                </section>
                <section>
                    <label class="label">Email<span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input"><utilize:textbox runat="server" ID="txt_email_address" MaxLength="80"></utilize:textbox></label>
                </section>
                <section>
                    <label class="label">Reactie<span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input"><utilize:textarea runat="server" ID="txt_reaction"></utilize:textarea></label>
                </section>
            </fieldset>
            <div class="buttons">
                <utilize:translatebutton runat="server" ID="button_place_reaction" Text="Plaats reactie" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
            </div>
            <br />
        </div>
    </div>



    <utilize:translatemessage ID="message_mandatory_fields_not_filled" runat="server" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message" Visible="false"></utilize:translatemessage>
</utilize:panel>