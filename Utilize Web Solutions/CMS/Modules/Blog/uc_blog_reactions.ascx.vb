﻿
Partial Class uc_blog_reactions
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _reaction_id As String = ""
    Public Property reaction_id As String
        Get
            Return _reaction_id
        End Get
        Set(value As String)
            _reaction_id = value
        End Set
    End Property

    Public Sub create_reactions()
        Dim qsc_blog_reactions As New Utilize.Data.QuerySelectClass
        qsc_blog_reactions.select_fields = "*"
        qsc_blog_reactions.select_from = "ucm_blog_reactions"
        qsc_blog_reactions.select_where = "hdr_id = @hdr_id and parent_id = @parent_id"
        qsc_blog_reactions.add_parameter("hdr_id", Me.block_id)
        qsc_blog_reactions.add_parameter("parent_id", Me.reaction_id)
        qsc_blog_reactions.select_order = "reaction_date desc"

        Dim dt_data_table As System.Data.DataTable = qsc_blog_reactions.execute_query()

        If dt_data_table.Rows.Count > 0 Then
            Me.rpt_reactions.DataSource = qsc_blog_reactions.execute_query()
            Me.rpt_reactions.DataBind()
        End If
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_blog_reactions.css", Me)
        Me.block_css = False

        If Not Me.IsPostBack() Then
            Me.lt_raw_url.Text = Request.RawUrl.ToString()
        End If
    End Sub

    Protected Sub rpt_reactions_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_reactions.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim ph_blog_reactions As placeholder = e.Item.FindControl("ph_blog_reactions")
            Dim uc_blog_reactions As uc_blog_reactions = LoadUserControl("~/Cms/Modules/Blog/uc_blog_reactions.ascx")
            uc_blog_reactions.block_id = Me.block_id
            uc_blog_reactions.reaction_id = e.Item.DataItem("rec_id")
            uc_blog_reactions.create_reactions()
            ph_blog_reactions.Controls.Add(uc_blog_reactions)

            Dim hl_hyperlink As hyperlink = e.Item.FindControl("hl_reply_to")

            Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
            url_class.set_parameter("reaction", e.Item.DataItem("rec_id"))

            Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())
            hl_hyperlink.NavigateUrl = lc_url
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.Visible = Me.rpt_reactions.Items.Count > 0
    End Sub
End Class
