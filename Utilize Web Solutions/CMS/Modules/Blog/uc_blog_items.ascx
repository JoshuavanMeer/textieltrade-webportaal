﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_blog_items.ascx.vb" Inherits="uc_blog_items" %>

<div class="uc_blog_items">
    <utilize:repeater runat="server" ID="rpt_blog_items">
        <HeaderTemplate>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="header"><h3><%# DataBinder.Eval(Container.DataItem, "blog_title_" + Me.global_cms.Language)%></h3><%# Format(DataBinder.Eval(Container.DataItem, "blog_date"), "dd MMMM yyyy") %></td>
            </tr>
            <tr>
                <td class="content">
                    <p><%# DataBinder.Eval(Container.DataItem, "blog_short_" + Me.global_cms.language).replace(chr(10), "<br>") %>&nbsp;<utilize:hyperlink runat="server" id="hl_read_more" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_"+ Me.global_cms.Language)) %>'><utilize:translatelabel runat="server" id="link_read_more" text="Lees meer"></utilize:translatelabel></utilize:hyperlink></p>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </utilize:repeater>
</div>
