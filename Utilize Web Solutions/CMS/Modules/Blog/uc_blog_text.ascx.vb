﻿
Partial Class uc_blog_text
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_blog_text.css", Me)

        'Maak een nieuwe datatable aan
        Dim qsc_news = New Utilize.Data.QuerySelectClass
        Dim dt_news = New System.Data.DataTable

        qsc_news.select_fields = "*"
        qsc_news.select_from = "ucm_blog_posts"
        qsc_news.select_order = "BLOG_DATE"
        qsc_news.select_where = "rec_id = @rec_id"
        qsc_news.add_parameter("@rec_id", Me.block_id)

        dt_news = qsc_news.execute_query()

        Me.Visible = dt_news.Rows.Count > 0
        If Me.Visible Then
            Me.lt_title.Text = dt_news.Rows(0).Item("blog_title_" + Me.global_cms.Language)
            Me.lt_blog_date.Text = Format(dt_news.Rows(0).Item("blog_date"), "dd MMMM yyyy")
            Me.lt_content.Text = dt_news.Rows(0).Item("blog_content_" + Me.global_cms.Language)
            Me.uc_blog_reactions.block_id = Me.block_id
            Me.uc_blog_reactions.create_reactions()
        End If
    End Sub
End Class
