﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_blog_reactions.ascx.vb" Inherits="uc_blog_reactions" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

<utilize:placeholder runat="server" ID="ph_reactions">
    <utilize:repeater runat="server" ID="rpt_reactions">
        <ItemTemplate>
            <div class="avatar uc_blog_reactions">
                <img src="<%# Me.ResolveCustomUrl("~/") + "DefaultImages/avatar.png" %>" class="avatar" /><br />
                <utilize:hyperlink runat="server" ID="hl_reply_to">Beantwoorden</utilize:hyperlink>
            </div>
            <div class="content">
                <h4><%# DataBinder.Eval(Container.DataItem, "blog_name")%></h4>
                <i><%# Format(DataBinder.Eval(Container.DataItem, "reaction_date"), "dd MMMM yyyy") + " - " + CType(DataBinder.Eval(Container.DataItem, "reaction_date"), DateTime).ToShortTimeString %></i><br />
                <%# DataBinder.Eval(Container.DataItem, "blog_reaction").replace(Chr(10), "<br>")%>
            </div>
            <br style="clear: both" />
            <div class="subreactions">
                <utilize:placeholder runat="server" ID="ph_blog_reactions">

                </utilize:placeholder>
            </div>
            <br style="clear: both" />
        </ItemTemplate>
    </utilize:repeater>
</utilize:placeholder>
