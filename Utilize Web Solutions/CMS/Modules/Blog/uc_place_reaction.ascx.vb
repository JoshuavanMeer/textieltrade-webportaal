﻿
Partial Class uc_place_reaction
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _reaction_id As String = ""
    Public Property reaction_id As String
        Get
            Return _reaction_id
        End Get
        Set(value As String)
            _reaction_id = value
        End Set
    End Property

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_place_reaction.css", Me)

        If Not Me.get_page_parameter("reaction") = "" And Not Me.IsPostBack() Then
            Me.lt_reaction.Text = " op het bericht van " + Utilize.Data.DataProcedures.GetValue("ucm_blog_reactions", "blog_name", Me.get_page_parameter("reaction"))

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "scrollBottom", "$(document).ready(function() {window.scrollTo(0,document.body.scrollHeight)});", True)

            Me.txt_name.Focus()
        End If

        If Not Me.IsPostBack() Then
            Me.lt_raw_url.Text = Request.RawUrl.ToString()
        End If
    End Sub

    Protected Sub button_place_reaction_Click(sender As Object, e As EventArgs) Handles button_place_reaction.Click
        Me.message_mandatory_fields_not_filled.Visible = False

        Dim ll_testing As Boolean = True

        If ll_testing Then
            ll_testing = Not Me.txt_name.Text.Trim() = ""

            If Not ll_testing Then
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_testing Then
            ll_testing = Not Me.txt_email_address.Text.Trim() = ""

            If Not ll_testing Then
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_testing Then
            ll_testing = Not Me.txt_reaction.Text.Trim() = ""

            If Not ll_testing Then
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_testing Then
            ' Hier gaan we de reactie toevoegen aan de lijst
            Dim ubo_reactions As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("ucm_blog_reactions", "TEST", "rec_id")
            ubo_reactions.table_insert_row("ucm_blog_reactions", Me.global_ws.user_information.user_id)
            ubo_reactions.field_set("reaction_date", System.DateTime.Now)
            ubo_reactions.field_set("blog_name", Server.HtmlEncode(Me.txt_name.Text))
            ubo_reactions.field_set("blog_email", Server.HtmlEncode(Me.txt_email_address.Text))
            ubo_reactions.field_set("blog_reaction", Server.HtmlEncode(Me.txt_reaction.Text))
            ubo_reactions.field_set("hdr_id", Me.get_page_parameter("RecId"))
            ubo_reactions.field_set("parent_id", Me.get_page_parameter("reaction"))
            ubo_reactions.table_update(Me.global_ws.user_information.user_id)

            Me.txt_name.Text = ""
            Me.txt_email_address.Text = ""
            Me.txt_reaction.Text = ""
        End If

        If ll_testing Then
            Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_raw_url.Text)
            url_class.set_parameter("reaction", "")

            Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())
            Response.Redirect(lc_url, True)
        End If
    End Sub
End Class
