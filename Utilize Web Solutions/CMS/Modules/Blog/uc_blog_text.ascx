﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_blog_text.ascx.vb" Inherits="uc_blog_text" %>

<%@ Register Src="~/CMS/Modules/Blog/uc_blog_reactions.ascx" tagname="uc_blog_reactions" tagprefix="uc1" %>
<%@ Register Src="~/CMS/Modules/Blog/uc_place_reaction.ascx" tagname="uc_place_reaction" tagprefix="uc2" %>

<div class="uc_blog_text">
    <utilize:placeholder runat="server" ID="ph_blog_content">
        <div class="blog_text">
            <table>
                <tr>
                    <td>
                        <h1><utilize:literal runat="server" ID="lt_title"></utilize:literal></h1>
                        <utilize:literal runat="server" ID="lt_blog_date"></utilize:literal>
                        <utilize:literal runat="server" ID="lt_content"></utilize:literal>
                    </td>
                </tr>
            </table>
        </div>
    </utilize:placeholder>
</div>

<div class="uc_blog_reactions block">
    <h2>Reacties</h2>
    <uc1:uc_blog_reactions ID="uc_blog_reactions" runat="server" />
    
    <utilize:placeholder runat="server" ID="ph_no_reactions" Visible="false">
    </utilize:placeholder>
</div>

<uc2:uc_place_reaction ID="uc_place_reaction1" runat="server" block_id="<%# Me.block_id%>" />