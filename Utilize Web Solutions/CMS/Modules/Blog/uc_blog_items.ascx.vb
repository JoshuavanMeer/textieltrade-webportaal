﻿
Partial Class uc_blog_items
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_blog_items.css", Me)

        Dim qsc_blog_posts As New Utilize.Data.QuerySelectClass
        qsc_blog_posts.select_fields = "*"
        qsc_blog_posts.select_from = "ucm_blog_posts"
        qsc_blog_posts.select_where = "hdr_id = @hdr_id"
        qsc_blog_posts.add_parameter("hdr_id", Me.block_id)
        qsc_blog_posts.select_order = "blog_date desc"

        Me.rpt_blog_items.DataSource = qsc_blog_posts.execute_query()
        Me.rpt_blog_items.DataBind()
    End Sub
End Class
