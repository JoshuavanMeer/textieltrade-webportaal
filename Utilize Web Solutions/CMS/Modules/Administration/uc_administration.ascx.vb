﻿
Partial Class uc_administration
    Inherits Utilize.Web.Solutions.Base.base_usercontrol_base

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_administration.css", Me)
        Me.set_javascript("uc_administration.js", Me)

        Me.opg_edit_type.Items.Add(New System.Web.UI.WebControls.ListItem("Niet geselecteerd", ""))
        Me.opg_edit_type.Items.Add(New System.Web.UI.WebControls.ListItem("Blokken aanpassen", "blocks"))
        Me.opg_edit_type.Items.Add(New System.Web.UI.WebControls.ListItem("Terminologie aanpassen", "translations"))
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Session("AdminPageEdit") Is Nothing Then
            Me.opg_edit_type.SelectedIndex = 0
        Else
            Me.opg_edit_type.SelectedValue = Session("AdminPageEdit")
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeTranslationBlocks", "$(document).ready(function (){setArrows();});", True)
    End Sub

    Protected Sub opg_edit_type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opg_edit_type.SelectedIndexChanged
        Session("AdminPageEdit") = Me.opg_edit_type.SelectedValue
    End Sub
End Class
