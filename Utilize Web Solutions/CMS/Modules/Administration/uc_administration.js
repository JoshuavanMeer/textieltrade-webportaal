﻿function setArrows() {
    var labelList = $('[data-url-id="translatelabel"]')

    labelList.each(function()
    {
        var url = $(this).data('url');

        var changeSpan = document.createElement('span')
        changeSpan.setAttribute('class', 'utlz_change_block');

        var changeDiv = document.createElement('div')
        changeDiv.setAttribute('class', 'utlz_change_block');
        changeSpan.appendChild(changeDiv)

        var changeA = document.createElement('a')
        changeA.setAttribute('href', '#');
        changeA.setAttribute('onclick', $(this).data('url'));
        changeA.innerHTML = 'Wijzigen'
        changeDiv.appendChild(changeA)

        $(this).before(changeSpan);
    }
    );
}