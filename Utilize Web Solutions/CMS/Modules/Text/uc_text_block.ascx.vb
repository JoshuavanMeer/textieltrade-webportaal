﻿
Imports Utilize.Data.API.CMS

Partial Class uc_text_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.block_id = ""
        End If

        Dim dt_text_block As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_text_block As New Utilize.Data.QuerySelectClass
            qsc_text_block.select_fields = "*"
            qsc_text_block.select_from = "ucm_text_blocks"
            qsc_text_block.select_order = "block_desc"

            ' Zorg dat de juiste filter getoond wordt.
            qsc_text_block.select_where = "rec_id = @rec_id"
            qsc_text_block.add_parameter("rec_id", Me.block_id)

            dt_text_block = qsc_text_block.execute_query()

            ll_resume = dt_text_block.Rows.Count > 0
        End If

        If ll_resume Then
            ' Zet de standaard tekst
            Dim lc_content As String = dt_text_block.Rows(0).Item("block_content_" + Me.global_cms.Language)

            If Not dt_text_block.Rows(0).Item("skin_id") = "" Then
                Me.skin = dt_text_block.Rows(0).Item("skin_id")
            End If

            Dim lt_content As New Utilize.Web.UI.WebControls.literal
            lt_content.Text = lc_content
            Me.pan_textbox.Controls.Add(lt_content)
        End If

        If ll_resume Then
            ' Hier gaan we het formulier aanmaken en toevoegen
            Me.set_style_sheet("uc_text_block.css", Me)
        End If

        Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

        'If advanced styling has been set, add the css classes according to the CMS settings for the current text block
        If ll_resume And dr_data_row.Item("text_block_advanced") Then
            Dim uc_text_block As Utilize.Web.UI.WebControls.panel = Me.pan_textbox
            uc_text_block.CssClass += " advanced bg-color " + Me.block_table + Me.block_rec_id

            Dim advancedStylingLocation As String = "/Styles/AdvancedText/" + Me.block_table + Me.block_rec_id + ".css"
            If Not System.IO.File.Exists(Server.MapPath(advancedStylingLocation)) Then
                Dim styleBuilder = StyleBuilderFactory.GetStyleBuilder("text", Me.block_rec_id, Me.block_table)
                styleBuilder.GenerateCssFile(advancedStylingLocation)
            End If

            Me.set_style_sheet(advancedStylingLocation, Me)
        End If

        If Not Me.global_ws.user_information.user_logged_on Then
            If dr_data_row.Item("show_when") = "2" Then
                ll_resume = False
            End If
        End If

        Me.Visible = ll_resume
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.float_right = True Then
            pan_textbox.CssClass += " pull-right"
        End If
    End Sub
End Class
