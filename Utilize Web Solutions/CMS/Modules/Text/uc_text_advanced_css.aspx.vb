﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim rec_id = Request.Params("rec_id")
        Dim block_table = Request.Params("block_table")
        Dim clientid = Request.Params("clientid")
        Dim lc_css As String

        If ll_resume Then
            ll_resume = Not rec_id Is Nothing And Not block_table Is Nothing
        End If

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(block_table, rec_id,)

            lc_css = "#" + clientid + ".uc_text_block.advanced { background-color: " + dr_data_row.Item("text_block_back_col").ToString + ";}"

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_css)
            Response.End()
        End If
    End Sub
End Class
