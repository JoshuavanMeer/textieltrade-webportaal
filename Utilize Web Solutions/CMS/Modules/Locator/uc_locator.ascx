﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_locator.ascx.vb" Inherits="uc_locator" %>

<div class="uc_locator margin-bottom-20">
    <utilize:literal ID="lt_google_maps_script" runat="server"></utilize:literal>
    <utilize:panel ID="pnl_uc_locator_input" CssClass="select_dealer" runat="server" DefaultButton="button_search_dealer">
        <div class="input-border">
            <div class="row">
                <div class="col-md-12">
                    <utilize:translatetext ID="text_dealer_locator_instruction2" runat="server" Text="Vul hier uw plaats (eventueel aangevuld met uw straat) en land in om de dichtstbijzijnde dealer(s) te zoeken."></utilize:translatetext>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label class="input no-border-top">
                        <utilize:textbox ID="txt_city_postal_location" runat="server" CssClass="form-control input value"></utilize:textbox>
                    </label>
                </div>

                <div class="col-md-3">
                    <label class="input no-border-top">
                            <utilize:dropdownlist ID="cbo_distance" runat="server" CssClass="select form-control value"></utilize:dropdownlist>
                    </label>
                </div>

                <div class="col-md-3">
                    <label class="input no-border-top">
                            <utilize:dropdownlist ID="cbo_countries" runat="server" CssClass="form-control input value"></utilize:dropdownlist>
                    </label>
                </div>
                

                <div class="col-md-3">
                    <utilize:translatebutton ID="button_search_dealer" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Dealer zoeken" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <utilize:placeholder ID="ph_dealer_types" runat="server"></utilize:placeholder>
                </div>
            </div>
        </div>
    </utilize:panel>

    <utilize:panel ID="pnl_no_results" runat="server" Visible="false">
        <h2>
            <utilize:translatetitle ID="title_dealer_locator_no_results" Text="Geen resultaten" runat="server"></utilize:translatetitle></h2>
        <span>
            <utilize:translatetext ID="text_dealer_locator_no_results" runat="server" Text="Er zijn geen dealers gevonden in uw zoek criteria."></utilize:translatetext></span><br />
        <br />
    </utilize:panel>


    <utilize:panel ID="pnl_uc_locator_output" CssClass="output" runat="server" Visible="false">
        <div class="row col-md-12">

            <div id="map_canvas" class="col-md-9"></div>
            <div class="col-md-3">
                <utilize:panel ID="pnl_results" runat="server" Visible="false">
                    <h2>
                        <utilize:translatetitle ID="title_dealer_locator_results" Text="Resultaten" runat="server"></utilize:translatetitle></h2>
                    <utilize:translatetext ID="text_dealer_locator_results_instruction" runat="server" Text="Klik op een dealer in de lijst om de routebeschrijving te bekijken."></utilize:translatetext>

                    <utilize:repeater runat="server" ID="rpt_results">
                        <HeaderTemplate>
                            <table class="overview table table-striped">
                                <thead>
                                </thead>
                                <tbody class="result-panel-tbody">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="<%# DataBinder.Eval(Container.DataItem, "geo_lat").ToString().Replace(",", ".") + "#" + DataBinder.Eval(Container.DataItem, "geo_long").ToString().Replace(",", ".") %>" class="line" onclick="calcRoute(<%# DataBinder.Eval(Container.DataItem, "source_lat").ToString().Replace(",", ".")%>,<%# DataBinder.Eval(Container.DataItem, "source_long").ToString().Replace(",", ".")%>,<%# DataBinder.Eval(Container.DataItem, "geo_lat").ToString().Replace(",", ".")%>,<%# DataBinder.Eval(Container.DataItem, "geo_long").ToString().Replace(",", ".")%>)">
                                <td><strong><em><%# DataBinder.Eval(Container.DataItem, "dealer_name")%></em></strong><br />
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_street")%> <%# DataBinder.Eval(Container.DataItem, "dealer_number")%><br />
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_postal")%>, <%# DataBinder.Eval(Container.DataItem, "dealer_city")%><br />
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_phone")%><br />
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_email")%><br />
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_desc_"+Me.global_cms.Language)%>
                                    <%# DataBinder.Eval(Container.DataItem, "dealer_website")%>
                                </td>

                                <td class="distance"><%# DataBinder.Eval(Container.DataItem, "distance")%></td>
                                <td class="latitude hidden"><%# DataBinder.Eval(Container.DataItem, "geo_lat")%></td>
                                <td class="longitude hidden"><%# DataBinder.Eval(Container.DataItem, "geo_long")%></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
            </table>
                        </FooterTemplate>
                    </utilize:repeater>
                </utilize:panel>
            </div>
        </div>

        <div id="directions" class="directions">
            <div class="row header">
                <div id="mode_of_travel" class="mode-of-travel btn-group pull-left">
                    <button type="button" class="btn btn-default active" aria-label="Driving"><span class="icon-travel-driving location-icon-travel" aria-hidden="true"></span></button>
                    <button type="button" class="btn btn-default" aria-label="Walking"><span class="icon-travel-walking location-icon-travel" aria-hidden="true"></span></button>
                    <button type="button" class="btn btn-default" aria-label="Bicycling"><span class="icon-travel-bicycling location-icon-travel" aria-hidden="true"></span></button>
                    <button type="button" class="btn btn-default" aria-label="Transit"><span class="icon-travel-transit location-icon-travel" aria-hidden="true"></span></button>
                </div>
            </div>
        </div>
    </utilize:panel>
</div>
