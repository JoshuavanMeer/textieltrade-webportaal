﻿Imports System.Net
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Linq
Imports System.Globalization

Partial Class uc_locator
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    'locatie van de webservice met de benodigde locatie gegevens
    'de set aan coordinaten in de bounds zijn de coordinaten waar nederland zich in bevind
    Private _url As String = ""
    Private _max_distance As Integer = 100
    Private _style As NumberStyles = NumberStyles.AllowDecimalPoint OR NumberStyles.AllowLeadingSign
    Private _culture As CultureInfo = CultureInfo.CreateSpecificCulture("en-US")

    ' een container voor de geo data
    Class geolocation
        Public status As String
        Public latitude As Double
        Public longitude As Double
    End Class

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me._url = "https://maps.googleapis.com/maps/api/geocode/xml?address=||addressdata||" '&bounds=50.673835, 2.944336|53.559889, 7.349854&sensor=false"
        Me.Page.MaintainScrollPositionOnPostBack = True

        Dim ll_testing As Boolean = True
        Me.set_style_sheet("uc_locator.css", Me)

        If ll_testing Then
            Me.txt_city_postal_location.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_dealer_locator_city_postal", "Uw plaats of postcode", Me.global_cms.Language))
        End If

        If ll_testing Then
            ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_CM_DEALER_LOC")
        End If

        If ll_testing Then
            Me.set_javascript("uc_locator.js", Me)

            ' Fetch Countries
            Dim qsc_countries As New Utilize.Data.QuerySelectClass
            qsc_countries.select_fields = "*"
            qsc_countries.select_from = "uws_countries"
            qsc_countries.select_where = "cnt_code In (SELECT dealer_country FROM ucm_dealer_locations group by dealer_country)"
            qsc_countries.select_order = "cnt_desc_" & Me.global_cms.Language
            Dim dt_countries As System.Data.DataTable = qsc_countries.execute_query()
            Me.cbo_countries.DataSource = dt_countries
            Me.cbo_countries.DataTextField = "cnt_desc_" & Me.global_cms.Language
            Me.cbo_countries.DataValueField = "cnt_code"
            Me.cbo_countries.DataBind()
            Me.cbo_countries.SelectedValue = "NL"

            If Not Me.Page.IsPostBack And Not Me.get_page_parameter("Location") = "" Then
                'Me.cbo_brand.SelectedValue = Me.get_page_parameter("Brand")
                Me.txt_city_postal_location.Text = Me.get_page_parameter("Location")

                If Not Me.get_page_parameter("Country") = "" Then
                    Me.cbo_countries.SelectedValue = Me.get_page_parameter("Country")
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "find_dealers", "$(document).ready(Function() { document.getElementById('" + Me.button_search_dealer.ClientID + "').click();});", True)
            End If

            ' Fetch first google key if any, otherwise use no script
            Dim qsc_google_key As New Utilize.Data.QuerySelectClass
            qsc_google_key.select_fields = "*"
            qsc_google_key.select_from = "UCM_API_KEYS"
            qsc_google_key.select_where = "key_code = 'GOOGLE'"
            Dim dt_google_data As System.Data.DataTable = qsc_google_key.execute_query()
            Dim google_key As String = ""

            If dt_google_data.Rows.Count > 0 Then
                google_key = dt_google_data.Rows.Item(0).Item("API_KEY")
            End If

            If String.IsNullOrEmpty(google_key.Trim()) Then
                Me.lt_google_maps_script.Text = ""
            Else
                If FrameWork.FrameworkProcedures.GetRequestUrl().ToLower().StartsWith("https://") Then
                    Me.lt_google_maps_script.Text = "<script type=""text/javascript"" src=""https://maps.googleapis.com/maps/api/js?key=" & google_key.Trim() & "&language=" & Me.global_cms.Language.ToLower & """></script>"
                Else
                    Me.lt_google_maps_script.Text = "<script type=""text/javascript"" src=""http://maps.googleapis.com/maps/api/js?key=" & google_key.Trim() & "&language=" & Me.global_cms.Language.ToLower & """></script>"
                End If

                Me._url = Me._url + "&key=" + google_key
            End If

            ' Add dealer types to placeholder, if any
            Dim qsc_dealer_types As New Utilize.Data.QuerySelectClass
            qsc_dealer_types.select_fields = "*"
            qsc_dealer_types.select_from = "UCM_DEALER_TYPES"
            qsc_dealer_types.select_order = "REC_ID DESC"
            Dim dt_dealer_types As System.Data.DataTable = qsc_dealer_types.execute_query()
            If Not IsNothing(dt_dealer_types) Then
                ' Add all types as checkbox
                For Each row As System.Data.DataRow In dt_dealer_types.Rows
                    Dim color_image As New System.Web.UI.WebControls.Image
                    Dim checkbox_type As New System.Web.UI.WebControls.CheckBox
                    checkbox_type.Text = row.Item("DEALER_TYPE_" + Me.global_cms.Language)
                    checkbox_type.AutoPostBack = True
                    checkbox_type.CssClass = "checkbox"
                    checkbox_type.Style.Add("margin", "5px")
                    checkbox_type.BackColor = System.Drawing.ColorTranslator.FromHtml(row.Item("DEALER_COLOR"))

                    AddHandler checkbox_type.CheckedChanged, AddressOf button_search_dealer_Click

                    'color_image.BackColor = System.Drawing.ColorTranslator.FromHtml(row.Item("DEALER_COLOR"))
                    'Dim size As New UI.WebControls.Unit(11, UI.WebControls.UnitType.Pixel)
                    'color_image.Width = size
                    'color_image.Height = size
                    'color_image.Style.Add("margin", "5px")

                    'Me.ph_dealer_types.Controls.AddAt(0, color_image)
                    Me.ph_dealer_types.Controls.AddAt(0, checkbox_type)
                Next
            End If

            ' Focus to search city, possibly with street or alternative with postalcode
            Me.txt_city_postal_location.Focus()
        End If

        If ll_testing And Not Me.Page.IsPostBack Then
            Me.SetDistances()
        End If

        If Not ll_testing Then
            Me.Visible = False
        End If
    End Sub

    Protected Sub button_search_dealer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search_dealer.Click
        Dim selected_distance As Integer

        If Not Integer.TryParse(Me.cbo_distance.SelectedValue,_style, _culture, selected_distance) Or Not String.IsNullOrEmpty(txt_city_postal_location.Text.Trim) Then
            Me.SetResults()
        End If
    End Sub

    Protected Sub SetDistances()
        ' Maak een nieuwe datatable aan voor de waarden
        Dim dt As New System.Data.DataTable
        dt.Columns.Add("desc")
        dt.Columns.Add("value")

        ' Haal de vertaling voor de omschrijving op
        Dim lc_distance_description As String = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_dealer_locator_distance", "tot =km= kilometer", Me.global_cms.Language)

        ' Voeg 10 opties toe
        For i As Integer = 10 To _max_distance Step 10
            dt.Rows.Add(lc_distance_description.Replace("=km=", i.ToString()), i)
        Next

        dt.Rows.Add(global_trans.translate_label("label_show_all_dealers", "Alle dealers tonen", Me.global_cms.Language), "aap")

        ' Koppel de datatable aan de selectie box
        cbo_distance.DataSource = dt
        cbo_distance.DataTextField = dt.Columns("desc").ColumnName
        cbo_distance.DataValueField = dt.Columns("value").ColumnName
        cbo_distance.DataBind()

        ' Selecteer de hoogste waarde
        cbo_distance.SelectedValue = _max_distance
    End Sub

    Protected Sub SetBrands()
        'If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brands") Then
        '    Dim qc_data As New Utilize.Data.QueryCustomSelectClass
        '    qc_data.QueryString = "SELECT brand_code, brand_desc_" + Me.global_cms.Language + " FROM uws_brands ORDER BY brand_desc_" + Me.global_cms.Language
        '    Dim dt_data As System.Data.DataTable = qc_data.execute_query()

        '    dt_data.Rows.Add("", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_dealer_locator_select_a_brand", "Selecteer een merk", Me.global_cms.Language))
        '    Me.cbo_brand.DataSource = dt_data
        '    Me.cbo_brand.DataTextField = "brand_desc_" + Me.global_cms.Language
        '    Me.cbo_brand.DataValueField = "brand_code"
        '    Me.cbo_brand.DataBind()
        '    Me.cbo_brand.SelectedValue = ""
        'Else
        'Me.brand_block.Visible = False
        'End If
    End Sub

    Private Structure location_and_distance
        Dim original_location As Integer
        Dim distance As Integer
    End Structure

    ''' <summary>
    ''' Checks the checkboxes of the available dealer types and returns pre-formatted string
    ''' </summary>
    ''' <returns>Pre-formatted string conform the ucm_dealer_locations.DEALER_TYPE field format or empty string</returns>
    Private Function GetSelectedDealerTypes() As String
        Dim string_selected_type As String = ""

        For Each control In Me.ph_dealer_types.Controls
            If control.ToString = "System.Web.UI.WebControls.CheckBox" Then
                If control.checked Then
                    ' If checkbox checke, and available as type in database, then add to return string
                    Dim qsc_dealer_types As New Utilize.Data.QuerySelectClass
                    qsc_dealer_types.select_fields = "*"
                    qsc_dealer_types.select_from = "UCM_DEALER_TYPES"
                    qsc_dealer_types.select_where = "DEALER_TYPE_" & Me.global_cms.Language & " = '" & control.text & "'"
                    Dim dt_dealer_types As System.Data.DataTable = qsc_dealer_types.execute_query()
                    If Not IsNothing(dt_dealer_types) Then
                        If dt_dealer_types.Rows.Count > 0 Then
                            For Each row As System.Data.DataRow In dt_dealer_types.Rows
                                string_selected_type = string_selected_type & "'-' + dealer_type like '%-" & row.Item("rec_id") & "-%' or "
                            Next
                        End If
                    End If
                End If
            End If
        Next

        If Not String.IsNullOrEmpty(string_selected_type) Then
            string_selected_type = "(" & string_selected_type.Substring(0, string_selected_type.Length - 3) & ")"
        End If

        Return string_selected_type
    End Function

    Protected Sub SetResults()
        'data table en query class aanmaken
        Dim dt_search As System.Data.DataTable = Nothing
        Dim qsc_search As New Utilize.Data.QuerySelectClass

        'geoloc1 is de ingevoerde postcode
        Dim geoloc1 As geolocation = Me.GeoLocate(Me.txt_city_postal_location.Text & "+ 'country " & Me.cbo_countries.SelectedItem.Text & "'")

        'als we een status terug krijgen die Ok is betekent het dat google maps een route kent
        If geoloc1.status = "OK" Then
            Dim selected_dealer_types As String = GetSelectedDealerTypes()
            If String.IsNullOrEmpty(selected_dealer_types) Then
                ' Haal alle dealers op
                With qsc_search
                    .select_fields = String.Format("' ' as distance, '{0}' as source_lat, '{1}' as source_long ,(select dealer_color as 'data()' from ucm_dealer_types where '-' + dealer_type like concat('%-',rec_id,'-%') for xml path('')) as color, *", geoloc1.latitude, geoloc1.longitude)
                    .select_from = "ucm_dealer_locations"
                    .select_order = "geo_lat, geo_long "
                End With
            Else
                ' Haal geselecteerde dealer type(s) op
                With qsc_search
                    .select_fields = String.Format("' ' as distance, '{0}' as source_lat , '{1}' as source_long , (select dealer_color as 'data()' from ucm_dealer_types where '-' + dealer_type like concat('%-',rec_id,'-%') for xml path('')) as color, *", geoloc1.latitude, geoloc1.longitude)
                    .select_from = "ucm_dealer_locations"
                    .select_where = selected_dealer_types
                    .select_order = "geo_lat, geo_long "
                End With
            End If
            dt_search = qsc_search.execute_query()

            'filteren op afstand
            Dim list_dist_locations As New List(Of location_and_distance)
            Dim lc_geolocations As String = ""

            ' Itereer door de dealers
            For Each row As System.Data.DataRow In dt_search.Rows
                Dim geoloc2 As New geolocation
                geoloc2.latitude = row.Item("geo_lat")
                geoloc2.longitude = row.Item("geo_long")
                geoloc2.status = "OK"

                Dim distance As Integer = Math.Floor(Me.GeoDistance(geoloc1, geoloc2))

                Dim selected_distance As Integer
                If Not Integer.TryParse(Me.cbo_distance.SelectedValue,_style, _culture, selected_distance) Then
                    selected_distance = -1
                End If

                If (selected_distance > 0 And distance > selected_distance) Or (selected_distance < 0 And Not Me.cbo_countries.SelectedValue = row.Item("dealer_country")) Then
                    row.Delete()
                Else
                    row.Item("distance") = distance & " km"

                    Dim location_details As String = ""
                    location_details = location_details + "<strong>" + row.Item("dealer_name") + "</strong><br />"
                    location_details = location_details + row.Item("dealer_street") + " " + row.Item("dealer_number") + "<br />"
                    location_details = location_details + row.Item("dealer_postal") + ", " + row.Item("dealer_city") + "<br />"
                    location_details = location_details + row.Item("dealer_phone") + "<br />"
                    location_details = location_details + row.Item("dealer_email") + "<br />"
                    location_details = location_details + row.Item("dealer_desc_" + Me.global_cms.Language)

                    Dim resulting_colors As String() = row.Item("color").ToString().Replace("#", "").Split(" ")
                    Dim result_color As String = resulting_colors(0)
                    Dim star_color As String = ""
                    Dim google_pin_url As String = ""

                    If resulting_colors.Length > 1 Then
                        star_color = resulting_colors(1)
                        If FrameWork.FrameworkProcedures.GetRequestUrl().ToLower().StartsWith("https://") Then
                            google_pin_url = "https://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_star|%E2%80%A2|" + result_color + "|000000|" + star_color
                        Else
                            google_pin_url = "http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_star|%E2%80%A2|" + result_color + "|000000|" + star_color
                        End If
                    Else
                        If FrameWork.FrameworkProcedures.GetRequestUrl().StartsWith("https://") Then
                            google_pin_url = "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + result_color
                        Else
                            google_pin_url = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + result_color
                        End If
                    End If

                    lc_geolocations += "{name: """ + location_details.Replace("'", "\'").Replace("""", "") + """, url: """ + google_pin_url + """, lat:" + row.Item("geo_lat").ToString().Replace(",", ".") + ", lng:" + row.Item("geo_long").ToString().Replace(",", ".") + "},"

                    ' Add website link to dealer when available
                    If Not String.IsNullOrEmpty(row.Item("dealer_website").trim()) Then
                        If row.Item("dealer_website").trim().startsWith("http") Then
                            row.Item("dealer_website") = "<a href ='" + row.Item("dealer_website").trim() + "' target=""_blank"" class=""dealer_link"" >Website</a> "
                        Else
                            row.Item("dealer_website") = "<a href = 'http://" + row.Item("dealer_website").trim() + "' target=""_blank"" class=""dealer_link"" >Website</a> "
                        End If
                    End If

                    If Not String.IsNullOrEmpty(row.Item("dealer_email").trim()) Then
                        row.Item("dealer_email") = "<a href = 'mailto:'" + row.Item("dealer_email") + "'>" + row.Item("dealer_email") + "</a> "
                    End If
                End If
            Next

            dt_search.AcceptChanges()

            ' Fetch the distances
            Dim distance_dict_location As Integer = 0
            For Each row As System.Data.DataRow In dt_search.Rows
                Dim current_position As location_and_distance
                current_position.original_location = distance_dict_location
                current_position.distance = row.Item("distance").replace(" km", "")
                list_dist_locations.Add(current_position)
                distance_dict_location = distance_dict_location + 1
            Next

            ' Sort the distances from small to large
            Dim bool_not_finished As Boolean = True
            While bool_not_finished

                bool_not_finished = False
                Dim sort_itterator As Integer = 1

                While sort_itterator < list_dist_locations.Count
                    If list_dist_locations(sort_itterator - 1).distance > list_dist_locations(sort_itterator).distance Then
                        bool_not_finished = True

                        Dim temp_dist_location = list_dist_locations(sort_itterator - 1)
                        list_dist_locations(sort_itterator - 1) = list_dist_locations(sort_itterator)
                        list_dist_locations(sort_itterator) = temp_dist_location
                    End If

                    sort_itterator = sort_itterator + 1
                End While

            End While

            ' Sort data from small to large
            Dim list_itterator As Integer = 0
            For Each list_item As location_and_distance In list_dist_locations
                If list_item.original_location > list_itterator Then
                    Dim temp_move_row = dt_search.Rows.Item(list_item.original_location).ItemArray
                    dt_search.Rows.Item(list_item.original_location).ItemArray = dt_search.Rows.Item(list_itterator).ItemArray
                    dt_search.Rows.Item(list_itterator).ItemArray = temp_move_row
                End If
                list_itterator = list_itterator + 1
            Next

            dt_search.AcceptChanges()

            ' Fix the geolocation string en zet de data in een array
            If lc_geolocations.EndsWith(",") Then
                lc_geolocations = lc_geolocations.Substring(0, lc_geolocations.Length - 1)
                lc_geolocations = "[" + lc_geolocations + "]"
            End If

            'binden
            If dt_search.Rows.Count > 0 Then
                Me.pnl_uc_locator_output.Visible = True
                Me.pnl_results.Visible = True
                Me.pnl_no_results.Visible = False
                Me.rpt_results.DataSource = dt_search
                Me.rpt_results.DataBind()
                ' Roep de initialize functie aan in de javascript code
                If FrameWork.FrameworkProcedures.GetRequestUrl().ToLower().StartsWith("https://") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "script_locator", "initialize('DRIVING','" + lc_geolocations + "'," + geoloc1.latitude.ToString().Replace(",", ".") + "," + geoloc1.longitude.ToString().Replace(",", ".") + ",'https://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=flag|00FF00');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "script_locator", "initialize('DRIVING','" + lc_geolocations + "'," + geoloc1.latitude.ToString().Replace(",", ".") + "," + geoloc1.longitude.ToString().Replace(",", ".") + ",'http://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=flag|00FF00');", True)
                End If
            Else
                Me.pnl_uc_locator_output.Visible = False
                Me.pnl_results.Visible = False
                Me.pnl_no_results.Visible = True
            End If
        End If
    End Sub

    Protected Function GeoLocate(ByVal address_data As String) As geolocation
        'container
        Dim return_data = New geolocation
        Dim selected_distance As Integer

        If Not Integer.TryParse(Me.cbo_distance.SelectedValue,_style, _culture, selected_distance) Then
            address_data = address_data.Substring(address_data.LastIndexOf("+"))
        End If

        Try
            'webservice benaderen
            Dim file_reader As New WebClient()
            Dim url As String = _url.Replace("||addressdata||", address_data.Replace(" ", "%20"))

            Dim Data As String
            Dim xmlDoc As New XmlDocument
            Data = file_reader.DownloadString(url)
            xmlDoc.LoadXml(Data)

            return_data.status = xmlDoc.GetElementsByTagName("status")(0).InnerText

            If return_data.status = "OK" Then
                Double.TryParse(xmlDoc.GetElementsByTagName("location")(0).ChildNodes(0).InnerText,_style, _culture, return_data.latitude)
                Double.TryParse(xmlDoc.GetElementsByTagName("location")(0).ChildNodes(1).InnerText,_style, _culture, return_data.longitude)
            Else
                return_data.latitude = 0
                return_data.longitude = 0
            End If
        Catch ex As Exception
            return_data.status = "Failed"
            return_data.latitude = 0
            return_data.longitude = 0
        End Try
        Return return_data
    End Function

    Function GeoDistance(ByVal geo1 As geolocation, ByVal geo2 As geolocation) As Double
        'Haversinus berekening
        Dim earth_radius As Double = 6371

        Dim d_lat As Double = (geo2.latitude - geo1.latitude) * Math.PI / 180
        Dim d_lon As Double = (geo2.longitude - geo1.longitude) * Math.PI / 180

        Dim d_lat_1 As Double = geo1.latitude * Math.PI / 180
        Dim d_lat_2 As Double = geo2.latitude * Math.PI / 180

        Dim a As Double = Math.Sin(d_lat / 2) * Math.Sin(d_lat / 2) + Math.Sin(d_lon / 2) * Math.Sin(d_lon / 2) * Math.Cos(d_lat_1) * Math.Cos(d_lat_2)
        Dim c As Double = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a))

        Return Math.Floor(earth_radius * c * 100) / 100
    End Function
End Class
