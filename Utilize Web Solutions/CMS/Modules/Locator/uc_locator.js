﻿//script

var geocoder;
var map;
var infoWindows = [];
var markers = [];
var positions = [];
var directionDisplay;
var directionsService;
var travelMode = "DRIVING";

function initialize(travel_mode, locations, centerlat, centerlong, marker_url) {
    //console.log('initialize');
    //console.log('locations:');
    //console.log(locations);
    travelMode = travel_mode;
    search_location = new google.maps.LatLng(centerlat, centerlong);

    $('div.mode-of-travel button').click(function () {
        travelMode = this.getAttribute("aria-label").toUpperCase();
        $('div.mode-of-travel button').removeClass("active");
        $(this).addClass("active");
        calcRoute();
    });

    //$("div.uc_locator .table .line .latitude")
    // Get the elements to display the map and the directions
    if (document.getElementById("map_canvas") && document.getElementById("directions")) {
        document.getElementById("map_canvas").style.visibility = "visible";
        // Create a new geocoder
        geocoder = new google.maps.Geocoder();
        directionsService = new google.maps.DirectionsService();
        directionsDisplay = new google.maps.DirectionsRenderer();

        // Set the location to the utilize address
        map = new google.maps.Map(document.getElementById("map_canvas"), {
            zoom: 7,
            center: search_location,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true,
            rotateControl: true
        });

        directionsDisplay.setPanel(document.getElementById('directions'));
        directionsDisplay.setMap(map);
        $('#directions').css("display", "none");
        // Add a property to the InfoWindow object
        google.maps.InfoWindow.prototype.opened = false;

        // Set searchlocation marker
        var pinImage = new google.maps.MarkerImage(marker_url,
           new google.maps.Size(21, 34),
           new google.maps.Point(0, 0),
           new google.maps.Point(10, 34));

        var marker = new google.maps.Marker({
            position: search_location,
            map: map,
            icon: pinImage
        })

        // Drop the markers
        dropMarkers(eval(locations));
    }
}

function dropMarkers(locations) {
    //console.log('dropMarkers');
    //console.log('locations: ');
    //console.log(locations);

    for (var i = 0; i < locations.length; i++) {
        addMarkerWithTimeout({ lat: locations[i].lat, lng: locations[i].lng }, 1, locations[i].name, locations[i].url); // timeout was i * 200, but to slow for large number of dealers
    }
}

function addMarkerWithTimeout(position, timeout, content, pinUrl) {
    //console.log('addMarkerWithTimeout');
    //console.log('position: ');
    //console.log(position);
    //console.log('timeout: ' + timeout);
    //console.log('content: ' + content);

    window.setTimeout(function () {

        var pinImage = new google.maps.MarkerImage(pinUrl,
            new google.maps.Size(21, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34));
        
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: pinImage
            //,animation: google.maps.Animation.DROP // No drop timeout was i * 200, but to slow for large number of dealers
        })
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        marker.addListener('click', function () {
            for (var i = 0; i < infoWindows.length; i++) {
                infoWindows[i].close();
            }
            infoWindow.open(map, marker);
            
            var target = document.getElementById(position.lat + '#' + position.lng);
            target.parentNode.scrollTop = target.offsetTop;
        });
        markers.push(marker);
        infoWindows.push(infoWindow);
    }, timeout);
}

// Wordt niet gebruikt
function codeAddress(address) {
    //console.log('codeAddress');
    //console.log('address: ' + address);

    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function calcRoute(startlat, startlng, endlat, endlng) {
    //console.log('calcRoute');
    //console.log('start: ' + start);
    //console.log('end: ' + end);
    start = new google.maps.LatLng(startlat, startlng);
    end = new google.maps.LatLng(endlat, endlng);
    if (start !== undefined) {
        _start = start;
        _end = end;
    } else {
        start = _start;
        end = _end;
    }

    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode[travelMode]
    };

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            document.getElementById("map_canvas").style.visibility = "visible";
            document.getElementById("directions").style.visibility = "visible";
            $('#directions').css("display", "block");
            document.getElementById("mode_of_travel").style.visibility = "visible";
        } else {
            console.log('Route request failed due to ' + status);
            alert("Er is een probleem opgetreden bij het berekenen van de route.");
        }
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            document.getElementById("map_canvas").style.visibility = "visible";
            document.getElementById("directions").style.visibility = "visible";
            $('#directions').css("display", "block");
            document.getElementById("mode_of_travel").style.visibility = "visible";
        } else {
            console.log('Directions request failed due to ' + status);
            alert("Er is een probleem opgetreden bij het berekenen van de route.");
        }
    });
}