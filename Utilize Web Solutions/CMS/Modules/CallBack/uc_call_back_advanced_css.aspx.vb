﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim rec_id = Request.Params("rec_id")
        Dim block_table = Request.Params("block_table")
        Dim clientid = Request.Params("clientid")
        Dim lc_css As String

        If ll_resume Then
            ll_resume = Not rec_id Is Nothing And Not block_table Is Nothing
        End If

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(block_table, rec_id,)

            lc_css = "#" + clientid + ".uc_call_back.advanced .shop-subscribe { background-color: " + dr_data_row.Item("call_back_background").ToString + ";}"
            lc_css += vbCrLf + "#" + clientid + ".uc_call_back.advanced .adv-form input[type='text'] {border: 1px solid " + dr_data_row.Item("call_back_input_line").ToString + ";}"
            lc_css += vbCrLf + "#" + clientid + ".uc_call_back.advanced  .adv-form input[type='text'] { background-color: " + dr_data_row.Item("call_back_input_col").ToString + ";}"
            lc_css += vbCrLf + "#" + clientid + ".uc_call_back.advanced  .adv-text p, .uc_call_back.advanced  .adv-text h2 { color: " + dr_data_row.Item("call_back_text_color").ToString + "!important;}"
            lc_css += vbCrLf + "#" + clientid + ".uc_call_back.advanced input[type='submit'] { color: " + dr_data_row.Item("call_back_btn_tcol").ToString + "!important; font-size: " + dr_data_row.Item("call_back_btn_size").ToString + "px; background-color: " + dr_data_row.Item("call_back_btn_bcolor").ToString + ";"

            If dr_data_row.Item("call_back_btn_trans") Then
                lc_css += "text-transform: uppercase;}"
            Else
                lc_css += "}"
            End If

            lc_css += vbCrLf + "#" + clientid + ".uc_call_back.advanced h2 { font-size: " + dr_data_row.Item("call_back_title_size").ToString + "px;}"

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_css)
            Response.End()
        End If
    End Sub
End Class
