﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_call_back.ascx.vb" Inherits="uc_call_back" %>

<utilize:panel runat="server" ID="pnl_callback" CssClass="uc_call_back margin-bottom-20" DefaultButton="button_call_me">
    <div class="shop-subscribe">
            <div class="col-md-3 md-margin-bottom-20 adv-text">
                    <h2><utilize:translatetitle runat='server' ID="title_call_me" Text="Bel mij"></utilize:translatetitle></h2>
                    <utilize:translatetext runat='server' ID="text_call_me_subtitle" Text="Heeft u een vraag of wil u een afspraak maken? Laat uw gegevens achter en wij bellen u terug."></utilize:translatetext>
            </div> 
            <div class="col-md-9 adv-form">
                <div class="row input-group">
                    <div class="col-md-5 col-xs-12 adv-field-1">
                        <utilize:textbox runat='server' ID="txt_name" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-5 col-xs-12 adv-field-2">
                        <utilize:textbox runat='server' ID="txt_phone_number" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-2 col-xs-12 adv-btn" >
                        <span class="input-group-btn">
                            <utilize:translatebutton runat="server" ID="button_call_me" CssClass="btn" Text="Verzenden" />
                        </span>
                    </div>
                </div>
            </div>
    </div>
</utilize:panel>