﻿
Imports Utilize.Data.API.CMS

Partial Class uc_call_back
    Inherits CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.txt_name.Attributes.Add("placeholder", global_trans.translate_label("label_your_name", "Uw naam", Me.global_cms.Language))
        Me.txt_phone_number.Attributes.Add("placeholder", global_trans.translate_label("label_your_phone_number", "Uw telefoonnummer", Me.global_cms.Language))

        Me.set_style_sheet("uc_call_back.css", Me)

        Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

        'If advanced styling has been set, add the css classes according to the CMS settings for the current call back block
        If dr_data_row.Item("call_back_advanced") Then
            Dim uc_call_back As Utilize.Web.UI.WebControls.panel = Me.pnl_callback
            uc_call_back.CssClass += " advanced"

            Select Case dr_data_row.Item("call_back_orient")
                Case "1"
                    uc_call_back.CssClass += " landscape"
                Case "2"
                    uc_call_back.CssClass += " portrait"
            End Select

            uc_call_back.CssClass += " " + Me.block_table + Me.block_rec_id
            Dim advancedStylingLocation As String = "/Styles/AdvancedCallBack/" + Me.block_table + Me.block_rec_id + ".css"
            If Not System.IO.File.Exists(Server.MapPath(advancedStylingLocation)) Then
                Dim styleBuilder = StyleBuilderFactory.GetStyleBuilder("callback", Me.block_rec_id, Me.block_table)
                styleBuilder.GenerateCssFile(advancedStylingLocation)
            End If

            Me.set_style_sheet(advancedStylingLocation, Me)
        End If
    End Sub

    Protected Sub button_call_me_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_call_me.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.txt_name.Text.Trim = ""

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "fields_not_filled", "alert('" + global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language) + "');", True)
            End If
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_phone_number.Text.Trim = ""

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "fields_not_filled", "alert('" + global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_cms.Language) + "');", True)
            End If
        End If

        If ll_resume Then
            ll_resume = Regex.IsMatch(Me.txt_phone_number.Text.Replace(" ", ""), "\b\d{2,5}[-.]?\d{5,8}\b")

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "fields_not_filled", "alert('" + global_trans.translate_message("message_phonenumber_incorrect", "Het telefoonnummer bevat niet geaccepteerde tekens.", Me.global_cms.Language) + "');", True)
            End If
        End If

        If ll_resume Then
            Dim lo_send_mail As New SendMail
            lo_send_mail.mail_subject = "Terugbelverzoek: " + Me.txt_name.Text

            lo_send_mail.mail_body = "Er is een terugbelverzoek geplaatst op: " + Request.Url.Host.ToLower() + "<br>"
            lo_send_mail.mail_body += "Naam: " + Me.txt_name.Text.Trim() + "<br>"
            lo_send_mail.mail_body += "Telefoonnummer: " + Me.txt_phone_number.Text.Trim()

            ll_resume = lo_send_mail.send_mail()

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "fields_not_filled", "alert('" + global_trans.translate_message("message_mail_send_error", "Er is een fout opgetreden bij het versturen van de email.", Me.global_cms.Language) + "');", True)
            Else
                Me.txt_name.Text = ""
                Me.txt_phone_number.Text = ""

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "email_sent", "alert('" + global_trans.translate_message("message_call_back_sent", "Uw contactverzoek is verstuurd. Wij nemen zo spoedig mogelijk contact met u op.", Me.global_cms.Language) + "');", True)
            End If
        End If
    End Sub
End Class
