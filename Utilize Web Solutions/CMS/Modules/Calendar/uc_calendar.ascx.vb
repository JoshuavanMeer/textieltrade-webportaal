﻿Imports Utilize.Web.Solutions.CMS
Imports System.Data
Imports System.Web.UI.WebControls
Imports System.Globalization

Partial Class uc_calendar
    Inherits cms_user_control

    Private _block_id As String = ""
    Public Shadows Property block_id As String
        Get
            Return _block_id
        End Get
        Set(value As String)
            _block_id = value
        End Set
    End Property

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_calendar") = False Then
            Return
        End If

        ' Set the full calendar stylesheets
        Me.set_style_sheet("~/_THMAssets/plugins/fullcalendar/fullcalendar.css", Me)

        ' Set the full calendar javascripts
        Me.set_javascript("~/_THMAssets/plugins/fullcalendar/lib/moment.min.js", Me)
        Me.set_javascript("~/_THMAssets/plugins/fullcalendar/lib/jquery.min.js", Me)
        Me.set_javascript("~/_THMAssets/plugins/fullcalendar/lib/jquery-ui.min.js", Me)
        Me.set_javascript("~/_THMAssets/plugins/fullcalendar/fullcalendar.min.js", Me)

        ' Set correct language, otherwise default to Dutch
        If Not String.IsNullOrEmpty(Me.global_ws.Language) Then
            Me.set_javascript("~/_THMAssets/plugins/fullcalendar/locale/" + Me.global_ws.Language + ".js", Me)
        End If

        Me.set_javascript("utilize_calendar.js", Me)

        ' Set value of block id hidden on page, such that calendar can find the correct date
        Me.hiddencalendarblock.Text = block_id
        Me.hiddencalendarid.Text = block_id
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

    End Sub

End Class


