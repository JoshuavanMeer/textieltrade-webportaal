﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_calendar.ascx.vb" Inherits="uc_calendar" %>
<utilize:panel runat="server" ID="pnl_calendar" class="uc_calendar margin-bottom-20">
    <utilize:label ID="label_date_header" runat="server"></utilize:label>
    <div class="uc_calendar">
        <div class="input-border margin-bottom-20">
            <utilize:panel runat="server" ID="pnl_selection" CssClass="selection">
                <utilize:translatetext runat="server" ID="text_eventheader_calender" Text="Onderstaand vindt u een overzicht van evenementen. Gebruik de onderstaande knoppen om naar een andere periode te gaan."></utilize:translatetext>
            </utilize:panel>
        </div>

        <div id='calendar<utilize:literal runat="server" id="hiddencalendarid" visible="true" text="0" />'></div>

        <input id="hiddenblock" hidden="hidden" value='<utilize:literal runat="server" id="hiddencalendarblock" visible="true" text="0" />' />
        
        <div id="utilizeCalendar" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                       <span class='close'>×</span>
                    </div>
                    <div class="modal-body">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</utilize:panel>
