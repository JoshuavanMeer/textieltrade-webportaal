﻿$(document).ready(function () {

    $('div[id=\"calendar' + $("#hiddenblock").val() + "\"]").fullCalendar({
        header: {
            left: 'prev today next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        //defaultDate: '2016-09-12',
        navLinks: true, // can click day/week names to navigate views
        editable: false, // it is only viewing, so it is a non-editable event calendar
        eventLimit: true, // allow "more" link when too many events
        events: '/cms/utilize_calendar.aspx?hdr=' + $("#hiddenblock").val(),
        eventClick: function (event) {
            if (event.urltoopen) {
                window.open(event.urltoopen);
            }
            else {
                var modal = document.getElementById('utilizeCalendar');

                // Show modal
                modal.style.display = "block";

                // Add title to header
                var header = document.getElementsByClassName("modal-header")[0];
                header.innerHTML ="<span class='close'>×</span><h2>" + event.title + "</h2>";

                // Add content to body
                var content = document.getElementsByClassName("modal-body")[0];
                content.innerHTML = event.content;

                var span = document.getElementsByClassName("close")[0];
                // When the user clicks on <span> (x), close the modal
                span.onclick = function () {
                    modal.style.display = "none";
                }

            }
            return false;
        }
    });
});