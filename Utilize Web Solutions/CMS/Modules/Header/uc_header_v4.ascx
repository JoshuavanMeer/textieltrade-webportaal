﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header_v4.ascx.vb" Inherits="uc_header_v4" %>

<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v4.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>
<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v5.ascx" TagPrefix="uc2" TagName="uc_languages_block" %>




<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom visible-sm visible-md visible-lg">
    <utilize:placeholder runat="server" ID="ph_customer_name" Visible="false">
        <strong>
            <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_employee_name" Visible="false">
        <strong>
            <utilize:literal runat="server" ID="lt_employee_name"></utilize:literal></strong>
    </utilize:placeholder>
</utilize:panel>

<utilize:placeholder runat="server" ID="ph_scroller_anchor" Visible="false">
    <div id="scroller-anchor"></div>
</utilize:placeholder>

<div class="uc_header_v4" runat="server" id="uc_header_v4">
    <div class="header-v5 header-static header-height">
        <div class="container mobile-menu hidden-lg hidden-md">
            <div class="col-sm-12">
                <ul class="list-inline">
                    <li runat="server" id="li_shopping_cart">
                        <utilize:translatelink runat="server" ID="link_show_basket" Text="Winkelwagen"></utilize:translatelink>
                    </li>
                    <utilize:placeholder runat="server" ID="ph_not_logged_on_mobile">
                        <li>
                            <utilize:hyperlink runat="server" ID="hl_login" Text="Login"></utilize:hyperlink>
                        </li>
                        <li>
                            <utilize:hyperlink runat="server" ID="hl_register" Text="Registreren"></utilize:hyperlink>
                        </li>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_logged_on_mobile">
                        <li>
                            <utilize:hyperlink runat="server" ID="hl_my_account" Text="Mijn account"></utilize:hyperlink>
                            <utilize:hyperlink runat="server" ID="link_my_favorites_mobile" Text="Mijn favorieten"></utilize:hyperlink>
                        </li>
                        <li>
                            <utilize:linkbutton runat="server" ID="lb_logout" Text="Uitloggen"></utilize:linkbutton>
                        </li>
                      
                    </utilize:placeholder>
                      <li>
                            <uc2:uc_languages_block runat="server" ID="uc_languages_block2" />
                        </li>
                </ul>
            </div>
        </div>

        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="hide-scroll">
                <div class="container main">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <div class="logo col-md-7">
                            <a class="navbar-brand" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                                <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />
                                <utilize:placeholder runat="server" ID="ph_company_logo"></utilize:placeholder>
                            </a>
                        </div>
                        <div class="row">
                            <!--[if IE]>
                            <div "class="navbar-header-right-ie col-md-5">
                        <![endif]-->
                            <div class="navbar-header-right col-md-5">
                                <ul class="list-inline">
                                    <li>
                                        <uc1:uc_languages_block runat="server" ID="uc_languages_block1" />
                                    </li>
                                    <utilize:placeholder runat="server" ID="ph_not_logged_on">
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                                        </li>
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                                        </li>
                                    </utilize:placeholder>
                                    <utilize:placeholder runat="server" ID="ph_logged_on">
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                                            <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                                        </li>
                                        <li>
                                            <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                                        </li>
                                    </utilize:placeholder>
                                </ul>
                            </div>
                            <!--[if IE]>
                            </div>
                        <![endif-->
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".uc_menu_flyout">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>

                        <utilize:placeholder runat="server" ID="ph_nav_buttons">
                        </utilize:placeholder>
                    </div>
                </div>
            </div>
            <utilize:literal runat="server" ID="lt_start_div"></utilize:literal>
            <div class="header-menu">
                <div class="menu">
                    <div class="container">
                        <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal_v4"></utilize:placeholder>
                    </div>
                </div>
                <div class="search">
                    <div class="container">
                        <utilize:placeholder runat="server" ID="ph_content_search"></utilize:placeholder>
                    </div>
                </div>
            </div>
            <utilize:literal runat="server" ID="lt_end_div"></utilize:literal>
        </div>
    </div>
</div>
