﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header.ascx.vb" Inherits="uc_header" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search.ascx" TagPrefix="uc1" TagName="uc_content_search" %>
<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>

<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom">
    <strong>
        <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
</utilize:panel>

<div class="header-v5 header-static">
    <div class="topbar-v3">
        <uc1:uc_content_search runat="server" ID="uc_content_search" />

        <div class="container">
            <div class="row">
                <uc1:uc_languages_block runat="server" ID="uc_languages_block" />
                <div class="col-sm-6">
                    <ul class="list-inline right-topbar pull-right">
                        <li runat="server" id="li_shopping_cart" class="hidden-lg hidden-md">
                            <utilize:translatelink runat="server" ID="link_show_basket" Text="Winkelwagen"></utilize:translatelink>
                        </li>
                        <utilize:placeholder runat="server" ID="ph_not_logged_on">
                            <li>
                                <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                            </li>
                            <li>
                                <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                            </li>
                            <li><i class="search fa fa-search search-button"></i></li>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_logged_on">
                            <li>
                                <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                                <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                            </li>
                            <li>
                                <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                            </li>                           
                            <li><i class="search fa fa-search search-button"></i></li>
                        </utilize:placeholder>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".uc_menu_flyout">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <utilize:placeholder runat="server" ID="ph_nav_buttons">
                </utilize:placeholder>

                <a class="navbar-brand" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                    <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />
                </a>
            </div>

            <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal">
            </utilize:placeholder>
        </div>
    </div>
</div>