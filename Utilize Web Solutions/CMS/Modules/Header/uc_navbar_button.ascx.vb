﻿
Partial Class uc_navbar_button
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _datatarget As String = ""
    Public Property datatarget As String
        Set(value As String)
            _datatarget = value
        End Set
        Get
            Return _datatarget
        End Get
    End Property

    Private _icon As String = ""
    Public Property icon As String
        Set(value As String)
            _icon = value
        End Set
        Get
            Return _icon
        End Get
    End Property

    Private _cssclass As String
    Public Property cssclass As String
        Set(value As String)
            _cssclass = value
        End Set
        Get
            Return _cssclass
        End Get
    End Property

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim lc_string As String = ""
        lc_string += " <button type=""button"" class=""navbar-toggle " + Me._cssclass + """ data-toggle=""collapse"" data-target=""" + Me._datatarget + """>"
        lc_string += " <span class=""sr-only"">Toggle navigation</span>"
        lc_string += " <span class=""" + Me._icon + """></span>"
        lc_string += " </button>"

        Dim lt_button As New literal
        lt_button.Text = lc_string

        Me.Controls.Add(lt_button)
    End Sub
End Class
