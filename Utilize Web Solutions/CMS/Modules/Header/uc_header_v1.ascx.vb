﻿Partial Class uc_header
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_header_v1.css", Me)
        Me.img_logo.ImageUrl = Me.ResolveCustomUrl("~/") + Me.global_cms.get_cms_setting("website_logo")

        Me.link_login.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/webshop/login.aspx"
        Me.link_register.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/webshop/paymentprocess/register.aspx"

        If Me.global_ws.user_information.user_logged_on AndAlso Not Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Me.link_my_account.NavigateUrl = Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx"
            Me.link_my_favorites.Visible = False
        End If

        If Me.global_ws.user_information.user_logged_on AndAlso Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_favorites") Then
            Me.link_my_favorites.NavigateUrl = Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/favorites.aspx"
            Me.link_my_account.Visible = False
        ElseIf Me.global_ws.user_information.user_logged_on AndAlso Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() And Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_favorites") Then
            Me.link_my_account.Visible = False
            Me.link_my_favorites.Visible = False
        End If

        Me.link_show_basket.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/webshop/paymentprocess/shopping_cart.aspx"
        Me.link_logout.Attributes.Add("onClick", "location.replace('" + Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/account/logout.aspx" + "');return false;")

        If Me.global_cms.get_cms_setting("sticky_header") = True Then
            Me.ph_scroller_anchor.Visible = True
            Me.lt_start_div.Text = "<div id='scroller'>"
            Me.lt_end_div.Text = "</div>"
        End If

        If Not global_ws.user_information.user_logged_on Then
            Me.link_show_basket.Visible = Me.global_ws.get_webshop_setting("shpcart_unlogged")
        End If

        Dim lo_menu_flyout As Utilize.Web.Solutions.Base.base_usercontrol_base = LoadUserControl("~/CMS/Modules/Menus/uc_menu_flyout_horizontal.ascx")
        Me.ph_menu_flyout_horizontal.Controls.Add(lo_menu_flyout)

        Dim lo_company_logo As Utilize.Web.Solutions.Base.base_usercontrol_base = LoadUserControl("~/Webshop/Modules/CompanyLogo/uc_company_logo.ascx")
        Me.ph_company_logo.Controls.Add(lo_company_logo)

        Me.set_login_information()
    End Sub
    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.link_register.Visible = Me.global_ws.get_webshop_setting("register_enabled") > 0

        Me.ph_logged_on.Visible = Me.global_ws.user_information.user_logged_on
        Me.ph_not_logged_on.Visible = Not Me.global_ws.user_information.user_logged_on

        'When punchout user is logged in, then we hide ph_logged_on
        Select Case True
            Case Utilize.Data.DataProcedures.CheckModule("utlz_ws_ariba")
                ' Functionaliteit voor de module "Vertegenwoordigersmodule"
                If Me.global_ws.user_information.user_logged_on = True And Me.global_ws.user_information.user_fst_name = "Ariba first name" Then
                    ' Hide account and logout options
                    Me.ph_logged_on.Visible = False
                End If
            Case Else
        End Select
    End Sub

    Private Sub set_login_information()
        Select Case True
            Case Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")
                ' Functionaliteit voor de module "Inkoopcombinaties"
                If Me.global_ws.user_information.user_logged_on = True Then
                    If Not Me.global_ws.user_information.user_id_original = "" Then
                        ' Maak het panel zichtbaar
                        Me.pnl_customer_information.Visible = True

                        Me.ph_customer_name.Visible = True
                        Me.lt_customer_name.Text = global_trans.translate_label("label_user_customer_name", "U bent ingelogd als:", Me.global_ws.Language) + " " + Me.global_ws.user_information.ubo_customer.field_get("uws_customers.bus_name")
                    End If

                    If Me.global_ws.user_information.user_id <> Me.global_ws.purchase_combination.user_id Then
                        ' Maak het panel zichtbaar
                        Me.pnl_customer_information.Visible = True

                        Me.ph_employee_name.Visible = True
                        Me.lt_employee_name.Text = global_trans.translate_label("label_user_employee_name", "U bestelt voor:", Me.global_ws.Language) + " " + Me.global_ws.purchase_combination.user_full_name
                    End If

                    If Me.ph_employee_name.Visible And Me.ph_customer_name.Visible Then
                        Me.lt_employee_name.Text = "<br>" + Me.lt_employee_name.Text
                    End If
                End If
            Case Utilize.Data.DataProcedures.CheckModule("utlz_ws_sales_module")
                ' Functionaliteit voor de module "Vertegenwoordigersmodule"
                If Me.global_ws.user_information.user_logged_on = True And Not Me.global_ws.user_information.user_id_original.Trim() = "" Then
                    ' Maak het panel zichtbaar
                    Me.pnl_customer_information.Visible = True
                    Me.ph_customer_name.Visible = True
                    Me.ph_employee_name.Visible = False

                    ' Zet het label
                    Me.lt_customer_name.Text = global_trans.translate_label("label_user_customer_name", "U bent ingelogd als:", Me.global_ws.Language) + " " + Me.global_ws.user_information.ubo_customer.field_get("uws_customers.bus_name")
                End If
            Case Else
                Me.pnl_customer_information.Visible = False
        End Select
    End Sub
End Class
