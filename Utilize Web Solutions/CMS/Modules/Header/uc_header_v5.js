﻿function set_close_button() {
    testing = true;
    if (testing) {
        if ($("#search_button").hasClass("fa-search")) {
            $("#search_button").removeClass("search fa fa-search search-btn search-button");
            $("#search_button").addClass("search fa search-btn fa-times");
            $(".uc_content_search_v3").addClass("show_search_box");
            $(".show_search_box").css("display","block")
            testing = false;
        }
    }
    if (testing) {
        if ($("#search_button").hasClass("fa-times")) {
            $("#search_button").removeClass("search fa search-btn fa-times");
            $("#search_button").addClass("search fa search-btn fa-search");
            $(".uc_content_search_v3").removeClass("show_search_box");
            $(".uc_content_search_v3").css("display", "none")
            testing = false;
        }
    }
};

function set_close_button_mobile() {
    testing = true;
    if (testing) {
        if ($("#search_button_mobile").hasClass("fa-search")) {
            $("#search_button_mobile").removeClass("search fa fa-search search-btn search-button");
            $("#search_button_mobile").addClass("search fa search-btn fa-times");
            $(".uc_content_search_v3").addClass("show_search_box");
            $(".show_search_box").css("display", "block")
            testing = false;
        }
    }
    if (testing) {
        if ($("#search_button_mobile").hasClass("fa-times")) {
            $("#search_button_mobile").removeClass("search fa search-btn fa-times");
            $("#search_button_mobile").addClass("search fa search-btn fa-search");
            $(".uc_content_search_v3").removeClass("show_search_box");
            $(".uc_content_search_v3").css("display", "none")
            testing = false;
        }
    }
};

function show_languages() {
    testing = true;
    if (testing) {
        if ($("#uc_language_block_v3").hasClass("closed")) {
            $("#uc_language_block_v3").removeClass("closed");
            $("#uc_language_block_v3").addClass("open");
            $(".uc_language_block_v3").addClass("show_lang_box");
            testing = false;
        }
    }
    if (testing) {
        if ($("#uc_language_block_v3").hasClass("open")) {
            $("#uc_language_block_v3").removeClass("open");
            $("#uc_language_block_v3").addClass("closed");
            $(".uc_language_block_v3").removeClass("show_lang_box");
            $(".uc_language_block_v3").css("display", "none")
            testing = false;
        }
    }
};