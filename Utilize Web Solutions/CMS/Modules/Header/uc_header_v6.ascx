﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header_v6.ascx.vb" Inherits="uc_header_v6" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v6.ascx" TagPrefix="uc1" TagName="uc_content_search" %>
<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v6.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>
<%@ Register src="~/Webshop/Modules/ShoppingCart/uc_shopping_cart_control.ascx" tagname="uc_shopping_cart_control" tagprefix="uc2" %>

<utilize:placeholder runat="server" ID="ph_scroller_anchor" Visible="false">
    <div id="scroller-anchor"></div>
</utilize:placeholder>

<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom visible-sm visible-md visible-lg">
    <utilize:placeholder runat="server" ID="ph_customer_name" Visible="false">
        <strong>
            <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_employee_name" Visible="false">
        <strong>
            <utilize:literal runat="server" ID="lt_employee_name"></utilize:literal></strong>
    </utilize:placeholder>
</utilize:panel>

<div class="uc_header_v6">

    <div class="topbar fluid-container">
        <div class="container flex">
            <utilize:placeholder runat="server" ID="ph_usps_top_bar"></utilize:placeholder>

            <div class="account-handler hidden-sm">
                <ul class="flex-list">
                    <utilize:placeholder runat="server" ID="ph_not_logged_on">
                        <li>
                            <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                        </li>
                        <li>
                            <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                        </li>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_logged_on">
                        <li>
                            <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                            <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                        </li>
                        <li>
                            <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                        </li>
                    </utilize:placeholder>
                </ul>
            </div>

            <div class="languages-handler">
                <uc1:uc_languages_block runat="server" ID="uc_languages_block1" />
            </div>
        </div>
    </div>

    <header class="header-v5 fluid-container">
        <div class="center-bar-bg">
            <div class="container flex">
                <div class="brand-handler col-sm-2 col-md-2">
                    <a class="logo-container" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                        <div class="image-container">
                            <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />
                            <utilize:placeholder runat="server" ID="ph_company_logo"></utilize:placeholder>
                        </div>
                    </a>
                </div>

                <div class="content-search-handler">
                    <uc1:uc_content_search runat="server" ID="uc_content_search1" />        
                </div>

                <div class="shopping-cart-control-handler">            
                    <uc2:uc_shopping_cart_control ID="uc_shopping_cart_control1" runat="server" />
                </div>            
                               
            </div>
        </div>
        
        <nav class="col-xs-12 col-sm-12 mobile-menu">
            <ul class="mobile-icons list-inline pull-right">
                <li>
                    <uc1:uc_languages_block runat="server" ID="uc_languages_block2" />
                </li>
                <li>
                    <utilize:hyperlink runat="server" ID="link_login_mobile" Text="Inloggen">
                    </utilize:hyperlink>
                    <utilize:linkbutton runat="server" ID="link_logout_mobile" Text="Uitloggen">
                    </utilize:linkbutton>
                </li>
                <li class="hidden">
                    <utilize:placeholder runat="server" ID="ph_nav_buttons"></utilize:placeholder>
                </li>
                <utilize:placeholder runat="server" ID="ph_register2">
                    <li>
                        <utilize:hyperlink runat="server" ID="link_register2" Text="Registreren"></utilize:hyperlink>
                        <utilize:hyperlink runat="server" ID="link_my_account_mobile" Text="Mijn account"></utilize:hyperlink>
                        <utilize:hyperlink runat="server" ID="link_my_favorites_mobile" Text="Mijn favorieten"></utilize:hyperlink>
                    </li>
                </utilize:placeholder>
                <li>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".uc_menu_flyout">
                        <span class="sr-only">Toggle navigation</span>
                        <utilize:translatelabel runat="server" ID="label_mobile_navigation" Text="Menu" CssClass="mob-menu-label"></utilize:translatelabel>                        
                        <div class="icon-bar-group">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </button>
                </li>
            </ul>
        </nav>

        <nav class="navbar navbar-default mega-menu desktop menu-flyout-handler" role="navigation">                        
            <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal_v6"></utilize:placeholder>            
        </nav>  

        <div class="mobile-search">
            <uc1:uc_content_search runat="server" ID="uc_content_search2" /> 
        </div>

        <div class="usps-handler">
            <utilize:placeholder runat="server" ID="ph_usps"></utilize:placeholder>    
        </div>             
        
    </header>

</div>
















