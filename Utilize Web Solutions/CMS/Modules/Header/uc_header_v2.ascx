﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header_v2.ascx.vb" Inherits="uc_header" %>

<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v2.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>


<utilize:placeholder runat="server" ID="ph_scroller_anchor" Visible="false">
    <div id="scroller-anchor"></div>
</utilize:placeholder>

<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom visible-sm visible-md visible-lg">
    <utilize:placeholder runat="server" ID="ph_customer_name" Visible="false">
        <strong>
            <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
    </utilize:placeholder>
    
    <utilize:placeholder runat="server" ID="ph_employee_name" Visible="false">
        <strong>
            <utilize:literal runat="server" ID="lt_employee_name"></utilize:literal></strong>
    </utilize:placeholder>
</utilize:panel>


<div class="uc_header_v2">
    <div class="header">
        <div class="header-height container">
            <a class="logo hide-scroll" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />

                <utilize:placeholder runat="server" ID="ph_company_logo">
                </utilize:placeholder>
            </a>

            <div class="topbar hide-scroll">
                <ul class="loginbar pull-right">    
                    <utilize:placeholder runat="server" ID="ph_usps"></utilize:placeholder>                
                </ul>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".target-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>

                <utilize:placeholder runat="server" ID="ph_nav_buttons">
                </utilize:placeholder>
            </button>
            <!-- End Toggle -->
        </div>
    <utilize:literal runat="server" ID="lt_start_div"></utilize:literal>
        <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse target-menu header-menu">
            <div class="container">
                <ul class="nav navbar-nav pull-left">
                    <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal">
                    </utilize:placeholder>                   
                </ul>
                <ul class="nav navbar-nav pull-right no-margin-left">
                    <li>
                        <a onclick="set_close_button(); return false;" id="search_button" class="search fa search-btn fa-search icon-element"></a>
                        <utilize:placeholder runat="server" ID="ph_search_button">
                        </utilize:placeholder>
                    </li>

                    <utilize:placeholder runat="server" ID="ph_not_logged_on">
                        <li>
                            <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                        </li>
                        <li>
                            <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                        </li>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_logged_on">
                            <li>
                                <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                                <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                            </li>
                            <li>
                                <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                            </li>                            
                    </utilize:placeholder>
                    <li>
                        <!-- BESPOKE: UWS-401 - Removed the image in the language block header -->
                        <a onclick="show_languages(); return false;" id="language_button" class="icon-element"></a>
                        <!-- BESPOKE END -->
                        <uc1:uc_languages_block runat="server" ID="uc_languages_block" />
                    </li>   
                    <li runat="server" id="li_shopping_cart">                        
                        <div class="header-v5">
                            <utilize:placeholder runat="server" ID="ph_shopping_cart">
                            </utilize:placeholder>
                        </div>
                        <utilize:translatelink runat="server" ID="link_show_basket" Text="Winkelwagen" CssClass="hidden-md hidden-lg"></utilize:translatelink>
                    </li>                     
                </ul>
            </div>
        </div>
    </div>
    
<utilize:literal runat="server" ID="lt_end_div"></utilize:literal>
</div>

