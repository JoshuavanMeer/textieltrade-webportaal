﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header_v5.ascx.vb" Inherits="uc_header_v5" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v5.ascx" TagPrefix="uc1" TagName="uc_content_search" %>
<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v5.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>

<utilize:placeholder runat="server" ID="ph_scroller_anchor" Visible="false">
    <div id="scroller-anchor"></div>
</utilize:placeholder>

<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom visible-sm visible-md visible-lg">
    <utilize:placeholder runat="server" ID="ph_customer_name" Visible="false">
        <strong>
            <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_employee_name" Visible="false">
        <strong>
            <utilize:literal runat="server" ID="lt_employee_name"></utilize:literal></strong>
    </utilize:placeholder>
</utilize:panel>

<div class="uc_header_v5">
    <utilize:literal runat="server" ID="lt_start_div"></utilize:literal>

    <div class="header-v5 header-static header-height">
        <div class="hide-scroll">
            <div class="topbar_v3">
                <div class="container top">
                    <div class="row">
                        <div class="tb_left col-md-6">
                            <!-- shop description -->
                            <utilize:translateliteral runat="server" ID="label_header_v3_welcome" Text="Welkom"></utilize:translateliteral>
                        </div>

                        <div class="tb_right col-md-6">
                            <!-- social media buttons-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="hide-scroll">
                <div class="container main">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <div class="row">
                            <div class="logo col-lg-3 col-md-4 col-sm-6">
                                <a class="navbar-brand" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                                    <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />
                                    <utilize:placeholder runat="server" ID="ph_company_logo"></utilize:placeholder>
                                </a>
                            </div>

                            <div class="col-lg-5 col-md-3 desktop-screen">
                                <uc1:uc_content_search runat="server" ID="uc_content_search1" />
                            </div>

                            <div class="col-lg-4 col-md-5 navbar-header-right desktop-screen">
                                <ul class="list-inline pull-right">
                                    <li>
                                        <uc1:uc_languages_block runat="server" ID="uc_languages_block1" />
                                    </li>
                                    <utilize:placeholder runat="server" ID="ph_not_logged_on">
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                                        </li>
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                                        </li>
                                    </utilize:placeholder>
                                    <utilize:placeholder runat="server" ID="ph_logged_on">
                                        <li>
                                            <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                                            <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                                        </li>
                                        <li>
                                            <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                                        </li>
                                    </utilize:placeholder>
                                </ul>
                            </div>

                            <div class="col-sm-6 mobile-screen">
                                <ul class="mobile-icons list-inline pull-right">
                                    <li>
                                        <uc1:uc_languages_block runat="server" ID="uc_languages_block2" />
                                    </li>
                                    <li>
                                        <utilize:hyperlink runat="server" ID="link_login_mobile" CssClass="navbar-toggle fa fa-lock"></utilize:hyperlink>
                                        <utilize:linkbutton runat="server" ID="link_logout_mobile" CssClass="navbar-toggle fa fa-sign-out"></utilize:linkbutton>
                                    </li>
                                    <li>
                                        <utilize:placeholder runat="server" ID="ph_mobile_cart_link">
                                            <a id="link_to_cart_mobile" class="navbar-toggle fa fa-shopping-cart" href="<%=ResolveUrl("~/" + Me.global_cms.Language + "/webshop/paymentprocess/shopping_cart.aspx")%>"></a>
                                        </utilize:placeholder>
                                    </li>
                                    <li>
                                        <utilize:placeholder runat="server" ID="ph_nav_buttons">
                                        </utilize:placeholder>
                                    </li>
                                    <utilize:placeholder runat="server" ID="ph_register2">
                                        <li>
                                            <utilize:hyperlink runat="server" ID="link_register2" CssClass="fa fa-user"></utilize:hyperlink>
                                            <utilize:hyperlink runat="server" ID="link_my_account_2" CssClass="fa fa-user"></utilize:hyperlink>
                                            <utilize:hyperlink runat="server" ID="link_my_favorites_mobile" Text="Mijn favorieten" CssClass="mobile-favorites fa fa-star navbar-toggle"></utilize:hyperlink>
                                        </li>
                                    </utilize:placeholder>
                                    <li>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".uc_menu_flyout">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="fa fa-bars"></span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default mega-menu desktop header-menu" role="navigation">
            <div class="container">
                <div class="row">
                    <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal_v3"></utilize:placeholder>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default mega-menu desktop mobile-screen" role="navigation">
            <div class="hide-scroll">
                <div class="container main">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <uc1:uc_content_search runat="server" ID="uc_content_search_mobile" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <utilize:literal runat="server" ID="lt_end_div"></utilize:literal>
    </div>
</div>
