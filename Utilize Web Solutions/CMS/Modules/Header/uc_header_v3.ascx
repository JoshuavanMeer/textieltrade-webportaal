﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_header" CodeFile="uc_header_v3.ascx.vb" Inherits="uc_header_v3" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v3.ascx" TagPrefix="uc1" TagName="uc_content_search" %>
<%@ Register Src="~/CMS/Modules/Languages/uc_languages_block_v3.ascx" TagPrefix="uc1" TagName="uc_languages_block" %>

<utilize:placeholder runat="server" ID="ph_scroller_anchor" Visible="false">
    <div id="scroller-anchor"></div>
</utilize:placeholder>

<utilize:panel runat='server' ID="pnl_customer_information" Visible="false" CssClass="customer_information alert alert-danger fade in no-margin-bottom visible-sm visible-md visible-lg">
    <utilize:placeholder runat="server" ID="ph_customer_name" Visible="false">
        <strong>
            <utilize:literal runat='server' ID="lt_customer_name"></utilize:literal></strong>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_employee_name" Visible="false">
        <strong>
            <utilize:literal runat="server" ID="lt_employee_name"></utilize:literal></strong>
    </utilize:placeholder>
</utilize:panel>

<div class="uc_header_v3">
    <utilize:literal runat="server" ID="lt_start_div"></utilize:literal>
    <div class="header-v5 header-static header-height">
        <div class="hide-scroll">
            <div class="topbar_v3">
                <div class="container top">
                    <div class="tb_left col-md-6">
                        <!-- shop description -->
                        <utilize:translatelabel runat="server" ID="label_header_description" Text="Wijzig de label_header_description in de veld vertalingen"></utilize:translatelabel>
                    </div>

                    <div class="tb_right col-md-6">
                        <!-- social media buttons-->
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-inline right-topbar pull-right">
                                <li runat="server" id="li_shopping_cart" class="hidden-lg hidden-md">
                                    <utilize:translatelink runat="server" ID="link_show_basket" Text="Winkelwagen"></utilize:translatelink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="hide-scroll">
                <div class="container main">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <div class="navbar-header-left col-md-4">
                            <utilize:translateliteral runat="server" ID="label_header_v3_welcome" Text="Welkom"></utilize:translateliteral>
                        </div>

                        <div class="logo col-md-3">
                            <a class="navbar-brand" href="<%=ResolveUrl("~/?language=" + Me.global_cms.Language)%>">
                                <utilize:image runat="server" ID="img_logo" CssClass="navbar-brand-logo" />
                                <utilize:placeholder runat="server" ID="ph_company_logo"></utilize:placeholder>
                            </a>
                        </div>
                        <!--[if IE]>
                            <div "class="navbar-header-right-ie col-md-5">
                        <![endif]-->
                        <div class="navbar-header-right col-md-5">
                            <ul class="list-inline pull-right">
                                <li>
                                    <uc1:uc_languages_block runat="server" ID="uc_languages_block1" />
                                </li>
                                <utilize:placeholder runat="server" ID="ph_not_logged_on">
                                    <li>
                                        <utilize:translatelink runat="server" ID="link_login" Text="Login"></utilize:translatelink>
                                    </li>
                                    <li>
                                        <utilize:translatelink runat="server" ID="link_register" Text="Registreren"></utilize:translatelink>
                                    </li>
                                </utilize:placeholder>
                                <utilize:placeholder runat="server" ID="ph_logged_on">
                                    <li>
                                        <utilize:translatelink runat="server" ID="link_my_account" Text="Mijn account"></utilize:translatelink>
                                        <utilize:translatelink runat="server" ID="link_my_favorites" Text="Mijn favorieten"></utilize:translatelink>
                                    </li>
                                    <li>
                                        <utilize:translatelinkbutton runat="server" ID="link_logout" Text="Uitloggen"></utilize:translatelinkbutton>
                                    </li>                                       
                                </utilize:placeholder>
                            </ul>
                        </div>
                        <!--[if IE]>
                            </div>
                        <![endif-->

                        <ul class="mobile_icon list-inline pull-right">
                            <li>
                                <uc1:uc_languages_block runat="server" ID="uc_languages_block2" />
                            </li>
                            <li>
                                <utilize:hyperlink runat="server" ID="link_login_mobile" CssClass="fa fa-lock"></utilize:hyperlink>
                                <utilize:linkbutton runat="server" ID="link_logout_mobile" CssClass="fa fa-sign-out"></utilize:linkbutton>
                            </li>
                            <li>
                                <a onclick="set_close_button_mobile(); return false;" id="search_button_mobile" class="search fa fa-search search-btn search-button"></a>
                            </li>
                            <li>
                                <utilize:placeholder runat="server" ID="ph_mobile_cart_link">
                                    <a id="link_to_cart_mobile" class="fa fa-shopping-cart" href="<%=ResolveUrl("~/" + Me.global_cms.Language + "/webshop/paymentprocess/shopping_cart.aspx")%>"></a>
                                </utilize:placeholder>
                            </li>
                              <li>
                              <utilize:hyperlink runat="server" ID="link_register2" CssClass="fa fa-user"></utilize:hyperlink>
                              <utilize:hyperlink runat="server" ID="link_my_account_2" CssClass="fa fa-user"></utilize:hyperlink>
                                  <utilize:hyperlink runat="server" ID="link_my_favorites_mobile" Text="Mijn favorieten" CssClass="mobile-favorites fa fa-star navbar-toggle"></utilize:hyperlink>
                            </li>
                            <li>
                                <utilize:placeholder runat="server" ID="ph_nav_buttons">
                                </utilize:placeholder>

                                <button type="button" class="navbar-toggle menu-toggle" data-toggle="collapse" data-target=".uc_menu_flyout">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="fa fa-bars"></span>
                                </button>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="header-menu">
                <div class="container">
                    <div class="mobile_search">
                        <uc1:uc_content_search runat="server" ID="uc_content_search_mobile" />
                    </div>
                    <div id="">
                        <utilize:placeholder runat="server" ID="ph_menu_flyout_horizontal_v3"></utilize:placeholder>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <utilize:literal runat="server" ID="lt_end_div"></utilize:literal>
</div>
