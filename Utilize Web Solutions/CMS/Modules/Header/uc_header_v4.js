﻿function show_languages() {
    testing = true;
    if (testing) {
        if ($("#uc_language_block_v4").hasClass("closed")) {
            $("#uc_language_block_v4").removeClass("closed");
            $("#uc_language_block_v4").addClass("open");
            $(".uc_language_block_v4").addClass("show_lang_box");
            testing = false;
        }
    }
    if (testing) {
        if ($("#uc_language_block_v4").hasClass("open")) {
            $("#uc_language_block_v4").removeClass("open");
            $("#uc_language_block_v4").addClass("closed");
            $(".uc_language_block_v4").removeClass("show_lang_box");
            $(".uc_language_block_v4").css("display", "none")
            testing = false;
        }
    }
};
