﻿
Partial Class Webshop_Modules_Advertising_uc_advertising_blockl
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.block_id = ""
        End If

        If ll_resume Then
            Me.lt_title.Text = Utilize.Data.DataProcedures.GetValue("ucm_js_blocks", "block_title_" + Me.global_cms.Language, Me.block_id)
        End If

        If ll_resume Then
            Dim qsc_scripts As New Utilize.Data.QuerySelectClass
            qsc_scripts.select_fields = "script_content_" + Me.global_cms.Language + " as script_content"
            qsc_scripts.select_from = "ucm_js_scripts"
            qsc_scripts.select_order = "row_ord"
            qsc_scripts.select_where = "hdr_id = @hdr_id"
            qsc_scripts.add_parameter("hdr_id", Me.block_id)

            Dim dt_scripts As System.Data.DataTable = qsc_scripts.execute_query()

            Me.rpt_advertising.DataSource = dt_scripts
            Me.rpt_advertising.DataBind()
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_javascript.css", Me)
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub
End Class
