﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_javascript.ascx.vb" Inherits="Webshop_Modules_Advertising_uc_advertising_blockl" %>

<utilize:panel ID="pnl_javascript" CssClass="uc_javascript" runat="server">
    <h2><utilize:literal runat="server" ID="lt_title"></utilize:literal></h2>
    <div class="scriptblock uc_javascript">
        <utilize:repeater runat="server" ID="rpt_advertising">
            <ItemTemplate>
                <div class="scriptcontent">
                    <utilize:literal ID="lt_javascript" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "script_content") %>'></utilize:literal>
                </div>
            </ItemTemplate>
        </utilize:repeater>
    </div>
</utilize:panel>