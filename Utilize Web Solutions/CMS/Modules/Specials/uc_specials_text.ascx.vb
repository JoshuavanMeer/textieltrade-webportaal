﻿
Partial Class uc_specials_text
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _block_id As String = ""
    Public Shadows Property block_id() As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_specials_text.css", Me)

        Dim qsc_specials As New Utilize.Data.QuerySelectClass
        With qsc_specials
            .select_fields = "*"
            .select_from = "ucm_specials"
            .select_order = "row_ord"
            .select_where = "rec_id = @rec_id"
        End With
        qsc_specials.add_parameter("rec_id", Me._block_id)

        Dim dt_specials As System.Data.DataTable = qsc_specials.execute_query()

        ' Bepaal of de control zichtbaar is op basis van het aantal regels
        Me.Visible = dt_specials.Rows.Count > 0

        If Me.Visible Then
            Me.lt_title.Text = dt_specials.Rows(0).Item("specials_title_" + Me.global_cms.Language)
            Me.lt_content.Text = dt_specials.Rows(0).Item("specials_content_" + Me.global_cms.Language)
        End If
    End Sub
End Class
