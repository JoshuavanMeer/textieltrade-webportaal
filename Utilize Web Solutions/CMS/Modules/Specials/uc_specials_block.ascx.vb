﻿
Partial Class uc_specials_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _specials_count As Integer = 0
    Public Property specials_count() As Integer
        Get
            Return _specials_count
        End Get
        Set(ByVal value As Integer)
            _specials_count = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Dim ll_testing As Boolean = True
        Me.set_style_sheet("uc_specials_block.css", Me)

        If ll_testing Then
            Me.li_title.Text = Utilize.Data.DataProcedures.GetValue("ucm_specials_blocks", "specials_title_" + Me.global_cms.Language, Me.block_id)
            Me.ph_title.Visible = Utilize.Data.DataProcedures.GetValue("ucm_specials_blocks", "specials_title_avail", Me.block_id)
        End If

        Dim qsc_specials As New Utilize.Data.QuerySelectClass
        qsc_specials.select_fields = "*"
        qsc_specials.select_from = "ucm_specials"
        qsc_specials.select_order = "row_ord"
        qsc_specials.select_where = "hdr_id = @hdr_id"
        qsc_specials.add_parameter("hdr_id", Me.block_id)

        Dim dt_specials As System.Data.DataTable = qsc_specials.execute_query()

        ' Bepaal of de control zichtbaar is op basis van het aantal regels
        Me.ph_specials.Visible = dt_specials.Rows.Count > 0

        ' Zet de datasource van de repeater en bouw hem op
        Me.rpt_specials.DataSource = dt_specials
        Me.rpt_specials.DataBind()
    End Sub
End Class
