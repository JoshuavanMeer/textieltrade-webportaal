﻿
Partial Class uc_specials_random
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet eerst de stylesheet
        Me.set_style_sheet("uc_specials_random.css", Me)

        Dim qsc_specials As New Utilize.Data.QuerySelectClass
        qsc_specials.select_fields = "top 1 " + "*"
        qsc_specials.select_from = "ucm_specials"
        qsc_specials.select_order = "newid()"

        If Not block_id.Trim() = "" Then
            qsc_specials.select_where += "and hdr_id = @hdr_id"
            qsc_specials.add_parameter("hdr_id", Me.block_id)
        End If

        Dim dt_specials As System.Data.DataTable = qsc_specials.execute_query()

        ' Bepaal of de control zichtbaar is op basis van het aantal regels
        Me.Visible = dt_specials.Rows.Count > 0

        If Me.Visible Then
            Me.lt_specials_title.Text = dt_specials.Rows(0).Item("specials_title_" + Me.global_cms.Language)
            Me.lt_specials_desc.Text = dt_specials.Rows(0).Item("specials_desc_" + Me.global_cms.Language)
            Me.img_special_logo.ImageUrl = "~/" + dt_specials.Rows(0).Item("specials_image")
            Me.hl_read_more.NavigateUrl = "~/" + dt_specials.Rows(0).Item("tgt_url_" + Me.global_cms.Language)
        End If
    End Sub
End Class
