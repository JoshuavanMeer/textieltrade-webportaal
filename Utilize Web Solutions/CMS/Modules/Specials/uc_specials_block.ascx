﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_specials_block.ascx.vb" Inherits="uc_specials_block" ClassName="uc_specials_block" %>
<utilize:placeholder runat="server" ID="ph_specials">
    <div class="uc_specials_block">
    <utilize:panel runat="server" ID="pnl_specials" CssClass="uc_specials_block margin-bottom-20">
        <utilize:placeholder runat="server" ID="ph_title">
            <h2><utilize:literal runat="server" ID="li_title"></utilize:literal> </h2>
        </utilize:placeholder>
            <utilize:repeater runat="server" ID="rpt_specials">
                <ItemTemplate>
                        <div class='<%# Me.item_size%> thumbnails thumbnail-style thumbnail-kenburn'>
                            <div class="thumbnail-img">
                                <div class="overflow-hidden">
                                    <a href='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_" + Me.global_cms.Language)) %>'><utilize:image ID="img_image" runat='server' CssClass="img-responsive" ImageUrl='<%#"~/" + DataBinder.Eval(Container.DataItem, "specials_image")%>' AlternateText='<%#  DataBinder.Eval(Container.DataItem, "specials_title_" + Me.global_cms.Language)%>' /></a>
                                </div>
                                <a class="btn-more hover-effect" href="#">read more +</a>
                                <asp:hyperlink ID="hl_read_more" runat="server" CssClass="btn-more hover-effect" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url_" + Me.global_cms.Language)) %>'><utilize:translateliteral id="specials_read_more" runat="server" Text="Lees meer +"></utilize:translateliteral></asp:hyperlink>
                            </div>
                            <div class="caption">
                                <h3><a class="hover-effect" href="#"><utilize:literal ID="lt_title" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "specials_title_" + Me.global_cms.Language)%>'></utilize:literal></a></h3>
                                <utilize:literal ID="lt_desc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "specials_desc_" + Me.global_cms.Language)%>'></utilize:literal>
                            </div>
                        </div>
                        <utilize:placeholder runat="server" ID="ph_clearfix" Visible="<%#Me.clearfix() %>">
                            <div class="customclearfix"></div>
                        </utilize:placeholder>
                </ItemTemplate>
            </utilize:repeater>
    </utilize:panel>
    </div>
</utilize:placeholder>
