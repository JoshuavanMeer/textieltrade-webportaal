﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_specials_text.ascx.vb" Inherits="uc_specials_text" ClassName="uc_specials_text" %>
<utilize:placeholder runat="server" ID="ph_specials_text">
    <div class="uc_specials_text">
        <h2><utilize:literal runat="server" ID="lt_title"></utilize:literal></h2>
        <utilize:literal runat="server" ID="lt_content"></utilize:literal>
    </div>
</utilize:placeholder>