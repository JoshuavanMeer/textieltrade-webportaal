﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_specials_random.ascx.vb" Inherits="uc_specials_random" ClassName="uc_specials_random" %>
<utilize:panel runat="server" ID="pnl_specials_random" CssClass="uc_specials_random margin-bottom-20">
    <div class="uc_specials_random">
    <h2><utilize:literal runat='server' ID="lt_specials_title"></utilize:literal></h2>
    <utilize:image runat="server" ID="img_special_logo" CssClass="img-responsive" />
    <utilize:literal runat="server" ID="lt_specials_desc"></utilize:literal>
    <asp:hyperlink ID="hl_read_more" runat="server">Lees meer...</asp:hyperlink>
    </div>
</utilize:panel>
