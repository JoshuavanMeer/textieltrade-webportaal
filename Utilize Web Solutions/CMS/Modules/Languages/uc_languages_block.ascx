﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_languages_block.ascx.vb" Inherits="uc_languages_block" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

<div class="col-sm-6">
    <utilize:placeholder runat="server" ID="ph_language_block">
        <div class="uc_languages_block">
            <utilize:placeholder runat="server" ID="ph_link_desc">
                <h2><utilize:translatetitle runat="server" ID="title_languages" Text="Talen"></utilize:translatetitle></h2>
            </utilize:placeholder>
            <div class="lang_wrapper">
                <ul class="left-topbar">
                    <li>
                        <a><utilize:translatelabel runat="server" ID="label_language" Text="Kies uw taal"></utilize:translatelabel>: <strong><%= Utilize.Data.DataProcedures.GetValue("ucm_languages", "lng_desc_" + Me.global_cms.Language, Me.global_cms.Language)%></strong></a>
                        <ul class="language">
                            <utilize:repeater runat="server" ID="rpt_links">
                                <ItemTemplate>
                                    <li><utilize:linkbutton runat="server" ID="ib_switch_languages" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'><%# DataBinder.Eval(Container.DataItem, "lng_desc_" + Me.global_cms.Language) %><img class="lang-img" src='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "lng_image"))%>' alt='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'></utilize:linkbutton></li>
                                </ItemTemplate>
                            </utilize:repeater>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </utilize:placeholder>    
</div>