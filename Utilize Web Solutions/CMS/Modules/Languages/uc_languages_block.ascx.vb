﻿
Partial Class uc_languages_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True
        Dim dt_languages As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_languages As New Utilize.Data.QuerySelectClass
            qsc_languages.select_fields = "*"
            qsc_languages.select_from = "ucm_languages"
            qsc_languages.select_order = "row_ord"

            dt_languages = qsc_languages.execute_query()
        End If

        If ll_resume Then
            Me.rpt_links.DataSource = dt_languages
            Me.rpt_links.DataBind()
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_languages_block.css", Me)
        End If

        If ll_resume And Not Me.Page.IsPostBack() Then
            Me.lt_raw_url.Text = Request.RawUrl.ToString()
        End If

        If Me.rpt_links.Items.Count > 1 Then
            Me.ph_language_block.Visible = True
        Else
            Me.ph_language_block.Visible = False
        End If
    End Sub

    Protected Sub rpt_links_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_links.ItemCommand
        Dim lcUrl As String = Me.lt_raw_url.Text
        Dim loUrlClass As New base_url_base(Me.lt_raw_url.Text)
        loUrlClass.set_parameter("language", e.CommandArgument)
        'Destroy the Categories helper Session, so a new will be created there
        If Not Session("UserCategoriesModel") Is Nothing Then
            Session.Remove("UserCategoriesModel")
        End If
        If Not Cache.Get(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel") Is Nothing Then
            Cache.Remove(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel")
        End If
        Response.Redirect(loUrlClass.get_url(), True)
    End Sub
End Class
