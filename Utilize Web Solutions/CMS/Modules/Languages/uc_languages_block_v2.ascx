﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_languages_block_v2.ascx.vb" Inherits="uc_languages_block" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

    <div id="uc_language_block_v2" class="closed">
        <utilize:placeholder runat="server" ID="ph_link_desc">
            <div class="uc_languages_block">
                <h2><utilize:translatetitle runat="server" ID="title_languages" Text="Talen"></utilize:translatetitle></h2>
            </div>
        </utilize:placeholder>
    
        <div class="search-open uc_language_block_v2" style="display: none;">
            <div class="col-md-12 no-margin-right">
                <div class="animated fadeInDown">                
                    <ul class="languages hoverSelectorBlock">
                        <utilize:repeater runat="server" ID="rpt_links">
                            <ItemTemplate>
                                <li><utilize:linkbutton runat="server" ID="ib_switch_languages" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'><img class="lang-img" src='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "lng_image"))%>' alt='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'><%# DataBinder.Eval(Container.DataItem, "lng_desc_" + Me.global_cms.Language) %></utilize:linkbutton></li>
                            </ItemTemplate>
                        </utilize:repeater>
                    </ul>
                </div>           
            </div> 
        </div>
    </div>
            