﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_languages_block_v5.ascx.vb" Inherits="uc_languages_block_v3" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

<div class="uc_languages_block">
    <div class="lang_wrapper">
        <ul class="right-header">
            <li>
                <a><span><%= Utilize.Data.DataProcedures.GetValue("ucm_languages", "lng_desc_" + Me.global_cms.Language, Me.global_cms.Language)%></span></a>
                <div class="language-list">
                    <ul class="language dropdown-menu">
                        <utilize:repeater runat="server" ID="rpt_links">
                            <ItemTemplate>
                                <li><utilize:linkbutton runat="server" ID="ib_switch_languages" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'><%# DataBinder.Eval(Container.DataItem, "lng_desc_" + Me.global_cms.Language) %><img class="lang-img" src='<%# Me.Page.ResolveUrl("~/" + DataBinder.Eval(Container.DataItem, "lng_image"))%>' alt='<%# DataBinder.Eval(Container.DataItem, "lng_code") %>'></utilize:linkbutton></li>
                            </ItemTemplate>
                        </utilize:repeater>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
