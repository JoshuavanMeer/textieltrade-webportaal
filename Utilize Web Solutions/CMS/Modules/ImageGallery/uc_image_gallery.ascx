﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_image_gallery.ascx.vb" ClassName="uc_image_gallery" Inherits="uc_image_gallery" %>

<utilize:panel runat="server" ID="pnl_image_gallery" CssClass="uc_image_gallery">
    <utilize:placeholder runat='server' ID="ph_image_gallery" Visible='true'>
        <utilize:repeater runat='server' ID="rpt_image_gallery">
            <ItemTemplate>
                <div class="row">
                    <div class="col-md-8">
                        <a href="<%# Me.ResolveCustomUrl("~/") + Me.global_cms.Language + "/cms/photogallery_page.aspx?RecId=" + DataBinder.Eval(Container.DataItem, "rec_id") + "&ColumnsPerItem=" + Me.item_size_int.ToString %>">
                            <%# DataBinder.Eval(Container.DataItem, "title_" + Me.global_cms.Language) %>
                        </a>
                    </div>
                    <div class="col-md-4 right">
                        <%# Format(DataBinder.Eval(Container.DataItem, "date"), "dd-MM-yyyy") %>
                    </div>
                </div>
            </ItemTemplate>
        </utilize:repeater>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_image_gallery" Visible='false'>
        <utilize:translatetext runat='server' ID="text_no_image_gallery" Text="Er zijn op dit moment geen fotogalerijen beschikbaar"></utilize:translatetext>
    </utilize:placeholder>
</utilize:panel>

