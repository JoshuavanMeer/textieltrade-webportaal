﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_image_gallery_details.ascx.vb" ClassName="uc_image_gallery_details" Inherits="uc_image_gallery_details" %>

<utilize:panel runat="server" ID="pnl_image_gallery_details" CssClass="uc_image_gallery_details">
    <utilize:placeholder runat="server" ID="pt_title" Visible='true'><h1><utilize:literal runat="server" ID="lt_title"></utilize:literal></h1></utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_image_gallery_details" Visible='true'>
        <div class="content">
            <div class="row">
                <utilize:repeater runat='server' ID="rpt_image_gallery_details">
                    <ItemTemplate>
                        <div class="<%# Me.item_size%> padding-bottom-20">
                            <a title="<%# DataBinder.Eval(Container.DataItem, "img_desc_" + Me.global_cms.Language) %>" class='<%# "fancybox img-hover-v2"  %>' href="<%# Me.ResolveCustomUrl("~/") + DataBinder.Eval(Container.DataItem, "img_thumb") %>" rel="<%# "gallery" + DataBinder.Eval(Container.DataItem, "photogall_dir")%>">
                                <span>
                                    <img class="img-responsive" alt="<%# DataBinder.Eval(Container.DataItem, "img_desc_" + Me.global_cms.Language) %>" src="<%# Me.ResolveCustomUrl("~/") + DataBinder.Eval(Container.DataItem, "img_thumb") %>" />
                                </span>
                            </a>

                            <utilize:literal runat="server" id="lt_image_title"></utilize:literal>
                            <utilize:literal runat="server" id="lt_image_text"></utilize:literal>
                        </div>

                        <utilize:placeholder runat="server" ID="ph_clearfix" Visible="<%#Me.clearfix() %>">
                            <div class="customclearfix margin-bottom-20"></div>
                        </utilize:placeholder>
                    </ItemTemplate>
                </utilize:repeater>
            </div>
        </div>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_image_gallery_details" Visible='false'>
        <utilize:translatetext runat='server' ID="text_no_image_gallery_details" Text="Er zijn op dit moment geen foto's in deze fotogalerij beschikbaar"></utilize:translatetext>
    </utilize:placeholder>
</utilize:panel>


