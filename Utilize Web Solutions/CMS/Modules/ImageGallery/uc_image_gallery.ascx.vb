﻿
Partial Class uc_image_gallery
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Create the query to collect the list of photogalleries from the database
            Dim qscPhotoGallery As New Utilize.Data.QuerySelectClass
            qscPhotoGallery.select_fields = "*"
            qscPhotoGallery.select_from = "ucm_photogallery"
            qscPhotoGallery.select_order = "row_ord"

            ' Execute the query and place result in datatable
            Dim dtPhotoGallery As System.Data.DataTable = qscPhotoGallery.execute_query()

            ' Bind datatable to repeater
            Me.rpt_image_gallery.DataSource = dtPhotoGallery
            Me.rpt_image_gallery.DataBind()

            ' Check if there are any galleries
            ll_testing = Me.rpt_image_gallery.Items.Count > 0
        End If

        ' If there are no galleries, then make the gallery invisible and make the error message visible
        If Not ll_testing Then
            Me.ph_image_gallery.Visible = False
            Me.ph_no_image_gallery.Visible = True
        End If

        ' Set style sheet when there are galleries
        If ll_testing Then
            Me.set_style_sheet("uc_image_gallery.css", Me)
        End If
    End Sub
End Class
