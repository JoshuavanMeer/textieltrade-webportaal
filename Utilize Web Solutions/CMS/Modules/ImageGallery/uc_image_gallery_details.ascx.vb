﻿
Imports System.Web.UI.WebControls

Partial Class uc_image_gallery_details
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    ' Id of photo gallery, which is used to collect the photos belonging to it
    Private _photogallery_id As String = ""
    Public Property photogallery_id As String
        Get
            Return _photogallery_id
        End Get
        Set(value As String)
            _photogallery_id = value
        End Set
    End Property

    ' The column setting determines the column width of each photo
    Private _column_setting As String = ""

    Public Property column_setting As String
        Get
            Return _column_setting
        End Get
        Set(value As String)
            _column_setting = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing And Not Me.block_id = "" Then
            _photogallery_id = Me.block_id
        End If

        If ll_testing Then
            ' Create the query to collect the detail list of photos from the database belonging to the given photogallery id
            Dim qscPhotoGallery As New Utilize.Data.QuerySelectClass
            qscPhotoGallery.select_fields = "*"
            qscPhotoGallery.select_from = "ucm_photogall_photos"
            qscPhotoGallery.select_where = "photogall_dir = @rec_id and img_thumb != '' "
            qscPhotoGallery.add_parameter("rec_id", _photogallery_id)
            qscPhotoGallery.select_order = "row_ord"

            ' Execute the query and place result in datatable
            Dim dtPhotoGalleryDetails As System.Data.DataTable = qscPhotoGallery.execute_query()

            ' Bind datatable to repeater
            Me.rpt_image_gallery_details.DataSource = dtPhotoGalleryDetails
            Me.rpt_image_gallery_details.DataBind()

            ' Show title of photogallery
            Me.lt_title.Text = Utilize.Data.DataProcedures.GetValue("ucm_photogallery", "title_" + Me.global_cms.Language, _photogallery_id)

            ' Check if there are any photos for the given id
            ll_testing = Me.rpt_image_gallery_details.Items.Count > 0

            Dim lb_hide As Boolean = Utilize.Data.DataProcedures.GetValue("ucm_photogallery", "hide_title", _photogallery_id)

            ' If this option is checked then remove the title
            pt_title.Visible = Not lb_hide
        End If

        ' If there are no photos, then make the gallery invisible and make the error message visible
        If Not ll_testing Then
            Me.ph_image_gallery_details.Visible = False
            Me.ph_no_image_gallery_details.Visible = True
        End If

        ' Set style sheet when there are photos and also add the fancybox style sheet & javascript
        If ll_testing Then
            Me.set_style_sheet("uc_image_gallery_details.css", Me)

            ' Set the fancybox javascript files
            Me.Page.Master.FindControl("ph_fancybox").Visible = True

            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/jquery.fancybox.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5", Me)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox", "jQuery(document).ready(function() {$('.fancybox').fancybox({openEffect  : 'fade', closeEffect : 'fade', prevEffect:  'fade', nextEffect:  'fade'});});", True)
        End If
    End Sub

    Private Sub rpt_image_gallery_details_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_image_gallery_details.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lt_image_title As Literal = e.Item.FindControl("lt_image_title")
            lt_image_title.Text = "<h2>" + e.Item.DataItem("image_title_" + Me.global_ws.Language) + "</h2>"
            lt_image_title.Visible = Not String.IsNullOrEmpty(e.Item.DataItem("image_title_" + Me.global_ws.Language))

            Dim lt_image_text As Literal = e.Item.FindControl("lt_image_text")
            lt_image_text.Text = e.Item.DataItem("image_text_" + Me.global_ws.Language).ToString().Replace(Chr(10), "<br>")
            lt_image_title.Visible = Not String.IsNullOrEmpty(e.Item.DataItem("image_text_" + Me.global_ws.Language))
        End If
    End Sub
End Class

