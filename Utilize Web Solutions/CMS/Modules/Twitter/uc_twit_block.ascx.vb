﻿
Partial Class uc_twit_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim lc_twitter_text As String = ""
        Me.set_javascript("http://widgets.twimg.com/j/2/widget.js", Me)

        'lc_twitter_text += "<script language=""javascript"">"
        'lc_twitter_text += "var widget = new TWTR.Widget({"
        'lc_twitter_text += "    version: 2,"
        'lc_twitter_text += "    type:   'profile',"
        'lc_twitter_text += "    rpp: 3,"
        'lc_twitter_text += "    interval: 6000,"
        'lc_twitter_text += "    width: '100%',"
        'lc_twitter_text += "    theme: {"
        'lc_twitter_text += "        shell: {"
        'lc_twitter_text += "        background:  '#FFF',"
        'lc_twitter_text += "        color:  '#000'"
        'lc_twitter_text += "},"
        'lc_twitter_text += "    tweets: {"
        'lc_twitter_text += "    background:  '#FFF',"
        'lc_twitter_text += "    color:  '#000',"
        'lc_twitter_text += "    links:  '#000'"
        'lc_twitter_text += "}"
        'lc_twitter_text += "},"
        'lc_twitter_text += "features: {"
        'lc_twitter_text += "    scrollbar: false,"
        'lc_twitter_text += "    loop: false,"
        'lc_twitter_text += "    live: false,"
        'lc_twitter_text += "    hashtags: true,"
        'lc_twitter_text += "    timestamp: true,"
        'lc_twitter_text += "    avatars: false,"
        'lc_twitter_text += "    behavior:  'all'"
        'lc_twitter_text += "}"
        'lc_twitter_text += "});"
        'lc_twitter_text += "widget.render().setUser('" + Me.global_cms.get_cms_setting("twitter_account") + "').start();"
        'lc_twitter_text += "</script>"

        lc_twitter_text = ""
        lc_twitter_text += "<a class=""twitter-timeline""  href=""https://twitter.com/" + Me.global_cms.get_cms_setting("twitter_account") + """  data-widget-id=""373082809310527488"">Tweets van @" + Me.global_cms.get_cms_setting("twitter_account") + "</a>"
        lc_twitter_text += "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+""://platform.twitter.com/widgets.js"";fjs.parentNode.insertBefore(js,fjs);}}(document,""script"",""twitter-wjs"");</script>"

        Me.lt_twitter_script.Text = lc_twitter_text

        ' ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Render", "widget.render().setUser('demaatschappij').start();", True)
    End Sub
End Class
