﻿
Partial Class uc_document_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _page_nr As Integer = 0 ' Huidig paginanummer
    Private _page_item_count As Integer = 1 ' Aantal items per pagina

    Private _page_count As Integer = 0 ' Aantal pagina's
    Private _page_nr_count As Integer = 10 ' Aantal pagina's om te tonen
    Private _page_nr_endcount As Integer = 2 ' Aantal pagina's op het eind

    Protected Friend _page_name As String = ""

    Private _block_id As String = ""

    Public Shadows Property block_id As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Haal de juiste producten op
        Dim qsc_source As New Utilize.Data.QuerySelectClass
        With qsc_source
            .select_fields = "*"
            .select_from = "ucm_documents"
            .select_order = "row_ord"
            .select_where = "hdr_id = @hdr_id"
        End With
        qsc_source.add_parameter("hdr_id", Me.block_id)

        Dim dt_source As System.Data.DataTable = qsc_source.execute_query()

        _page_name = Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_cms.Language, Me.get_page_parameter("UsCode"))

        'Add correct logo to file extension on the fly, so that when changes in logo happens it is valid for all files in database
        dt_source.Columns.Add("doc_type_url_image")
        For Each row As System.Data.DataRow In dt_source.Rows
            If row.Item("doc_type") = 1 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/PDFImage.png"
            ElseIf row.Item("doc_type") = 2 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/WinWordLogo.png"
            ElseIf row.Item("doc_type") = 3 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/ExcelLogo.png"
            ElseIf row.Item("doc_type") = 4 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/Ziplogo.png"
            End If
        Next
        Me.rpt_documents.DataSource = dt_source
        Me.rpt_documents.DataBind()

        Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_document_blocks", "skin_id", _block_id)

        If Not lc_skin_id = "" Then
            Me.skin = lc_skin_id
        End If
        Me.set_style_sheet("uc_document_archive.css", Me)
    End Sub
End Class
