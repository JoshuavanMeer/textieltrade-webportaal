﻿
Partial Class uc_document_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private _item_count As Integer = 0

    Public Property item_count As Integer
        Get
            Return _item_count
        End Get
        Set(ByVal value As Integer)
            _item_count = value
        End Set
    End Property

    Private _block_id As String = ""

    Public Shadows Property block_id As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim qsc_data_table = New Utilize.Data.QuerySelectClass
        With qsc_data_table
            .select_fields = "TOP " + _item_count.ToString() + " " + "*"
            .select_from = "ucm_documents"
            .select_order = "row_ord"
            .select_where = "hdr_id = @hdr_id"
        End With
        qsc_data_table.add_parameter("hdr_id", Me.block_id)

        Dim dt_data_table As System.Data.DataTable = qsc_data_table.execute_query()

        'Add correct logo to file extension on the fly, so that when changes in logo happens it is valid for all files in database
        dt_data_table.Columns.Add("doc_type_url_image")
        For Each row As System.Data.DataRow In dt_data_table.Rows
            If row.Item("doc_type") = 1 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/PDFImage.png"
            ElseIf row.Item("doc_type") = 2 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/WinWordLogo.png"
            ElseIf row.Item("doc_type") = 3 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/ExcelLogo.png"
            ElseIf row.Item("doc_type") = 4 Then
                row.Item("doc_type_url_image") = "~/DefaultImages/Ziplogo.png"
            End If
        Next

        Me.rpt_documents.DataSource = dt_data_table
        Me.rpt_documents.DataBind()

        Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_document_blocks", "skin_id", _block_id)

        If Not lc_skin_id = "" Then
            Me.skin = lc_skin_id
        End If

        Me.set_style_sheet("uc_document_block.css", Me)
    End Sub
End Class
