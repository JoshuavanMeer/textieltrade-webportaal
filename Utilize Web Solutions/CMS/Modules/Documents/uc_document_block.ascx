﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_document_block.ascx.vb" Inherits="uc_document_block" %>

<utilize:panel runat='server' ID="pnl_document_block" CssClass="uc_document_block">
    <utilize:repeater runat='server' ID="rpt_documents">
        <ItemTemplate>
            <div class="col-sm-<%# Me.item_size_int %> padding-bottom-20">
                <span>
                    <img class="img-responsive" src='<%# Me.ResolveCustomUrl(DataBinder.Eval(Container.DataItem, "doc_type_url_image")) %>' />
                </span>
                <h3><%# DataBinder.Eval(Container.DataItem, "doc_desc_" + Me.global_cms.Language) %></h3>
                <utilize:hyperlink runat='server' ID="hl_doc_file" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "doc_file_" + Me.global_cms.Language)) %>' Target="_blank">    
                            <utilize:translatelabel runat="server" ID="label_download_document" Text="Download hier uw document"></utilize:translatelabel></utilize:hyperlink>
            </div>

            <utilize:placeholder runat="server" ID="ph_clearfix" Visible="<%#Me.clearfix() %>">
                <div class="customclearfix margin-bottom-20"></div>
            </utilize:placeholder>
        </ItemTemplate>
    </utilize:repeater>
</utilize:panel>
