﻿<%@ Control Language="VB" AutoEventWireup="False" ClassName="uc_footer" CodeFile="uc_footer.ascx.vb" Inherits="uc_footer" %>

<div class="uc_footer">


    <div class="footer-v4">
        <div class="footer">
            <div class="container">
                <utilize:repeater runat="server" ID="rpt_rows">
                    <ItemTemplate>
                        <utilize:placeholder runat="server" ID="ph_row">
                        </utilize:placeholder>
                    </ItemTemplate>
                </utilize:repeater>
                <utilize:placeholder runat="server" ID="ph_footer_blocks"></utilize:placeholder>
            </div><!--/end continer-->
        </div><!--/footer-->

        <div class="copyright">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-6">                     

                    </div>
                    <div class="col-md-6">  
                        <p class="pull-right">Powered by <a href="http://www.utilize.nl/" target="_blank">Utilize Business Solutions BV</a></p>
                    </div>
                </div>                   
            </div> 
        </div><!--/copyright--> 
    </div>

</div>
