﻿Imports System.Linq
Imports System.Web.UI.WebControls

Partial Class uc_footer
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private footer_blocks As New System.Data.DataTable

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing = True Then
            Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
            Dim ll_footer_blocks_available As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "footer_block_avail", lc_shop_code)

            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "*"
            qsc_select.select_from = "ucm_footer_blocks"

            If Not ll_footer_blocks_available Then
                qsc_select.select_where = "shop_code = @shop_code"
                qsc_select.add_parameter("shop_code", lc_shop_code)
            End If

            qsc_select.select_order = "row_ord asc"
            footer_blocks = qsc_select.execute_query()

            Dim lo_rows = From row In footer_blocks.Rows Where row.item("block_type") = "ROW" Order By row.item("block_desc").ToString().Substring(row.item("block_desc").ToString().Length - 1) Select row

            Dim dt_footer_rows As System.Data.DataTable = footer_blocks.Clone()

            For Each row In lo_rows
                dt_footer_rows.ImportRow(row)
            Next

            Me.rpt_rows.DataSource = dt_footer_rows
            Me.rpt_rows.DataBind()

            Dim lo_rowless = From row In footer_blocks.Rows Where String.IsNullOrEmpty(row.item("parent_row")) Select row

            Dim dt_rowless As System.Data.DataTable = footer_blocks.Clone()

            For Each row In lo_rowless
                dt_rowless.ImportRow(row)
            Next

            add_block_to_placeholder(dt_rowless, Me.ph_footer_blocks)
        End If
    End Sub

    Private Sub rpt_rows_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_rows.ItemCreated
        Dim ph_row As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_row")
        Dim lc_rec_id As String = e.Item.DataItem("rec_id")

        Dim div_row As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
        If e.Item.DataItem("row_vertical_align") Then
            div_row.Attributes.Add("class", "row stretch-children")
        Else
            div_row.Attributes.Add("class", "row")
        End If

        Dim lo_row_blocks = From row In footer_blocks.Rows Where row.item("parent_row") = lc_rec_id Select row

        Dim dt_source As System.Data.DataTable = footer_blocks.Clone()

        For Each row In lo_row_blocks
            dt_source.ImportRow(row)
        Next

        add_block_to_placeholder(dt_source, div_row)

        ph_row.Controls.Add(div_row)
    End Sub

    Private Sub add_block_to_placeholder(dt_data As System.Data.DataTable, ph_placeholder As System.Web.UI.Control)
        For Each dr_footer_block As System.Data.DataRow In dt_data.Rows
            Dim lc_block_type As String = dr_footer_block.Item("block_type")
            Select Case lc_block_type
                Case "TEXT"
                    ' Sla de ID van het block op in een variabele.
                    Dim lc_block_id As String = dr_footer_block("tekst_block_id")
                    Dim lc_block_rec_id As String = dr_footer_block.Item("rec_id")

                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Text/uc_text_block.ascx")

                    uc_block.set_property("block_id", lc_block_id)
                    uc_block.set_property("block_rec_id", lc_block_rec_id)
                    uc_block.set_property("block_table", "ucm_footer_blocks")
                    uc_block.set_property("object_code", "ucm_text")
                    uc_block.set_property("skin", dr_footer_block("skin_id"))
                    uc_block.set_property("column_size", CInt(dr_footer_block("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    uc_block.set_property("float_right", dr_footer_block.Item("float_right"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "BANNER"
                    ' Sla de ID van het block op in een variabele.
                    Dim lc_block_id As String = dr_footer_block("banner_block_id")
                    Dim lc_block_rec_id As String = dr_footer_block.Item("rec_id")

                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Banners/uc_banner_block.ascx")

                    uc_block.set_property("block_id", lc_block_id)
                    uc_block.set_property("block_rec_id", lc_block_rec_id)
                    uc_block.set_property("block_table", "ucm_footer_blocks")
                    uc_block.set_property("skin", dr_footer_block("skin_id"))
                    uc_block.set_property("object_code", "ucm_banners")
                    uc_block.set_property("column_size", CInt(dr_footer_block("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    uc_block.set_property("float_right", dr_footer_block.Item("float_right"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "LINKS"
                    ' Sla de ID van het block op in een variabele.
                    Dim lc_block_id As String = dr_footer_block("link_block_id")
                    Dim lc_block_rec_id As String = dr_footer_block.Item("rec_id")

                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Links/uc_link_block.ascx")

                    uc_block.set_property("block_id", lc_block_id)
                    uc_block.set_property("block_rec_id", lc_block_rec_id)
                    uc_block.set_property("block_table", "ucm_footer_blocks")
                    uc_block.set_property("skin", dr_footer_block("skin_id"))
                    uc_block.set_property("object_code", "ucm_links")
                    uc_block.set_property("column_size", CInt(dr_footer_block("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    uc_block.set_property("float_right", dr_footer_block.Item("float_right"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "MAILING"
                    Dim lc_block_rec_id As String = dr_footer_block.Item("rec_id")

                    ' Maak een nieuwe blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Mailing/uc_mailing_footer.ascx")

                    uc_block.set_property("block_rec_id", lc_block_rec_id)
                    uc_block.set_property("skin", dr_footer_block("skin_id"))
                    uc_block.set_property("object_code", "ucm_mailing")
                    uc_block.set_property("block_table", "ucm_footer_blocks")
                    uc_block.set_property("column_size", CInt(dr_footer_block("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    uc_block.set_property("float_right", dr_footer_block.Item("float_right"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "YOUTUBE"
                    ' Sla de ID van het block op in een variabele.
                    Dim lc_block_id As String = dr_footer_block("youtube_block_id")

                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/YoutubeVideo/uc_youtube_video_block.ascx")

                    uc_block.set_property("block_id", lc_block_id)
                    uc_block.set_property("skin", dr_footer_block("skin_id"))
                    uc_block.set_property("object_code", "ucm_youtube_video")
                    uc_block.set_property("column_size", CInt(dr_footer_block("block_layout")))


                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    If dr_footer_block.Item("item_layout") > 0 Then
                        uc_block.set_property("item_size_int", CInt(dr_footer_block.Item("item_layout")))
                    End If

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "PAY_LOGOS"
                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/Webshop/Modules/Paymentlogos/uc_payment_logos.ascx")

                    uc_block.set_property("skin", dr_footer_block.Item("skin_id"))
                    uc_block.set_property("column_size", CInt(dr_footer_block.Item("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case "CALL_BACK"
                    ' Maak een nieuwe tekst blok aan.
                    Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/CallBack/uc_call_back.ascx")
                    Dim lc_block_rec_id As String = dr_footer_block.Item("rec_id")

                    uc_block.set_property("block_rec_id", lc_block_rec_id)
                    uc_block.set_property("block_table", "ucm_footer_blocks")
                    uc_block.set_property("skin", dr_footer_block.Item("skin_id"))
                    uc_block.set_property("column_size", CInt(dr_footer_block.Item("block_layout")))

                    uc_block.set_property("block_hide_xs", dr_footer_block.Item("block_hide_xs"))
                    uc_block.set_property("block_hide_sm", dr_footer_block.Item("block_hide_sm"))
                    uc_block.set_property("block_hide_md", dr_footer_block.Item("block_hide_md"))
                    uc_block.set_property("block_hide_lg", dr_footer_block.Item("block_hide_lg"))

                    ' Voeg het blok toe aan de pagina.
                    ph_placeholder.Controls.Add(uc_block)
                Case Else
                    Exit Select
            End Select
        Next
    End Sub
End Class
