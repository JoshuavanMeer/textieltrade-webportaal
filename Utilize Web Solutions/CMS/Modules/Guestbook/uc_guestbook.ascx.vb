﻿Imports System.Web.UI.WebControls
Imports Utilize.Web.Solutions.CMS

Partial Class guestbook_uc_guestbook
    Inherits cms_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_guestbook.css", Me)

        ' Me.message_error.Visible = False
        Dim qsc_source As New Utilize.Data.QuerySelectClass
        qsc_source.select_fields = "*"
        qsc_source.select_from = "ucm_guestbook"
        qsc_source.select_order = "gb_date DESC"
        qsc_source.select_where = "gb_approved = 1"

        Dim dt_source As System.Data.DataTable = qsc_source.execute_query()

        Me.txt_name.Attributes.Add("placeholder", Me.label_guestbook_name.Text)
        Me.txt_email.Attributes.Add("placeholder", Me.label_guestbook_email.Text)
        Me.txt_reaction.Attributes.Add("placeholder", Me.label_guestbook_description.Text)

        Me.rpt_guestbook.DataSource = dt_source
        Me.rpt_guestbook.DataBind()
    End Sub

    Protected Function create_time_string(ByVal lo_date As Date) As String

        Dim lc_time_string As String = Format(lo_date, "h:mm:sstt")
        Dim lc_am_pm As String = ""
        Dim li_hours As Integer = Date.Now.Hour

        If li_hours >= 0 And li_hours < 12 Then
            lc_am_pm = " AM"
        Else
            lc_am_pm = " PM"
        End If
        lc_time_string &= lc_am_pm

        Return lc_time_string
    End Function

    Protected Sub btn_back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Me.ph_guestbook_entry_completed.Visible = False
        Me.ph_guestbook_reactions.Visible = True
    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        Me.ph_guestbook_reactions.Visible = True
        Me.ph_guestbook_entry.Visible = False
    End Sub

    Protected Sub button_add_reaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_reaction.Click
        'Laten we het formulier schoonmaken voordat we het tonen.
        Me.txt_email.Text = ""
        Me.txt_name.Text = ""
        Me.txt_reaction.Text = ""
        Me.lbl_message_error.Visible = False
        Me.ph_guestbook_reactions.Visible = False
        Me.ph_guestbook_entry.Visible = True
    End Sub

    Protected Sub button_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("ucm_guestbook")
        ubo_bus_obj.table_insert_row("ucm_guestbook", Me.global_ws.user_information.user_id)

        Dim ll_resume As Boolean = True

        ' Test eerst of de naam ingevuld is en laat een error zien indien dit niet zo is.
        If ll_resume Then
            If Me.txt_name.Text.Length < 1 Then
                Me.lbl_message_error.Visible = True
                ll_resume = False
            End If
        End If

        ' Test daarna of het e-mail adres ingevuld is en laat een error zien indien dit niet zo is.
        If ll_resume Then
            If Me.txt_email.Text.Length < 1 Then
                Me.lbl_message_error.Visible = True
                ll_resume = False
            End If
        End If

        ' Test daarna of de reactie ingevuld is en laat een error zien indien dit niet zo is.
        If ll_resume Then
            If Me.txt_reaction.Text.Length < 1 Then
                Me.lbl_message_error.Visible = True
                ll_resume = False
            End If
        End If

        'Als alles goed gaat kunnen we de data opslaan
        If ll_resume Then
            ubo_bus_obj.field_set("gb_date", Date.Now)
            ubo_bus_obj.field_set("gb_name", Me.txt_name.Text.Trim())
            ubo_bus_obj.field_set("gb_reaction", Me.txt_reaction.Text.Trim())
            ubo_bus_obj.field_set("gb_email", Me.txt_email.Text.Trim())
            ubo_bus_obj.field_set("gb_approved", 0)
            ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            Me.rpt_guestbook.DataBind()

            'Nu moeten we de velden van het formulier weer leegmaken en eventuele foutmeldingen verbergen

            Me.txt_email.Text = ""
            Me.txt_name.Text = ""
            Me.txt_reaction.Text = ""
            Me.lbl_message_error.Visible = False
            Me.ph_guestbook_entry_completed.Visible = True
            Me.ph_guestbook_entry.Visible = False

        End If
    End Sub
End Class
