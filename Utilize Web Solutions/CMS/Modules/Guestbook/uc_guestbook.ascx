﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_guestbook.ascx.vb" Inherits="guestbook_uc_guestbook" %>
<div class="uc_guestbook margin-bottom-20">

    <utilize:placeholder runat="server" ID="ph_guestbook_entry_completed" Visible ="false">
        <h2><utilize:translatetitle runat="server" ID="title_guestbook_entry_completed" Text="Uw reactie is geplaatst"></utilize:translatetitle></h2>
        <utilize:translatetext runat="server" ID="text_guestbook_entry_completed" Text="Uw reactie is geplaatst, kies voor de knop 'Terug' om terug te gaan naar het overzicht van reacties."></utilize:translatetext>
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="btn_back" Text="Terug" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
        </div>

    </utilize:placeholder>
    
    <utilize:placeholder runat="server" ID="ph_guestbook_reactions">
        <h2><utilize:translatetitle runat="server" ID="title_guestbook" Text="Gastenboek"></utilize:translatetitle></h2>
        <utilize:translatetext runat="server" ID="text_guestbook" Text="Welkom bij ons gastenboek<br />Kies voor 'Voeg reactie toe' om een reactie op ons gastenboek achter te laten."></utilize:translatetext>
        <div class="buttons">
            <utilize:translatebutton ID="button_add_reaction" Text="Voeg reactie toe" runat="server" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
        </div>
   
        <h2><utilize:translatetitle runat="server" ID="title_guestbook_reactions" Text="Overzicht reacties"></utilize:translatetitle></h2>
        <utilize:translatetext runat="server" ID="text_guestbook_reactions" Text="Onderstaand vindt u een overzicht van reacties van onze bezoekers."></utilize:translatetext>
        
        <utilize:repeater runat='server' ID="rpt_guestbook" pager_enabled="true" pagertype="bottom" page_size="10">
            <HeaderTemplate>
                <table>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <b>
                            <%# Format(DataBinder.Eval(Container.DataItem, "gb_date"), "dd-MM-yyyy")%>
                            -
                            <%# create_time_string(DataBinder.Eval(Container.DataItem, "gb_date")) %>
                            -
                            <%# DataBinder.Eval(Container.DataItem, "gb_name")%>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <%# DataBinder.Eval(Container.DataItem, "gb_reaction")%></p>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </utilize:repeater>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_guestbook_entry" Visible="false">
        <div class="row margin-bottom-10">
            
            <h1><utilize:translatetitle runat="server" Text="Reactie toevoegen" ID="title_guestbook_add_reaction"></utilize:translatetitle></h1>
            <utilize:translatetext runat="server" ID="text_guestbook_add_reaction" Text="Vul de onderstaande velden in en kies voor 'Verzenden' om uw reactie in te dienen."></utilize:translatetext>
            <div class="row">
                <div class="col-md-12">
                    <div class="input-border padding-right">
                        <utilize:translatelabel runat="server" ID="label_guestbook_name" Text="Naam" Visible="false"></utilize:translatelabel>
                        <utilize:textbox MaxLength="200" CssClass="form-control" class="text_box" runat="server" ID="txt_name"></utilize:textbox>
                        <br />
        
                        <utilize:translatelabel runat="server" id="label_guestbook_email" Text="E-mailadres" Visible="false"></utilize:translatelabel>
                        <utilize:textbox MaxLength="200" CssClass="form-control" runat="server" class="text_box" ID="txt_email"></utilize:textbox>
                        <br />
            
                        <utilize:translatelabel runat="server" ID="label_guestbook_description" Text="Omschrijving" Visible="false"></utilize:translatelabel>
                        <utilize:textarea runat="server" CssClass="form-control" class="reaction_box" ID="txt_reaction"></utilize:textarea>
                        
                        <div class="buttons">
                            <utilize:translatebutton runat="server" ID="btn_cancel" Text="Annuleren" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
                            <utilize:translatebutton runat="server" ID="btn_submit" Text="Verzenden" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <utilize:label ID="lbl_message_error" runat="server" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message" Visible="false" />       
    </utilize:placeholder>
</div>
