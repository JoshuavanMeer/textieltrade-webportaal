﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu.ascx.vb" Inherits="uc_menu" %>

<div class="mobile-category uc_menu">
    <utilize:panel runat="server" cssclass="uc_menu collapse navbar-collapse mega-menu navbar-responsive-collapse panel-width"> 
        <div class="panel-group" id="accordion">
            <div class="panel panel-borderset">
                <div class="mobile-heading panel-heading">
                    <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"><utilize:translatetitle runat="server" ID="title_menu" Text="Menu"></utilize:translatetitle></a>
                    </h2>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body mobile-body">
                        <ul class="nav navbar-nav">
                            <utilize:placeholder runat='server' ID="ph_menu"></utilize:placeholder>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <utilize:hyperlink runat="server" ID="link_level_up">
            <span class="glyphicon glyphicon-arrow-left"></span>
            <utilize:literal runat="server" ID="lt_level_up"></utilize:literal>
        </utilize:hyperlink>
    </utilize:panel>
</div>
