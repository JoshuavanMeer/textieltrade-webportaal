﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal_v4.ascx.vb" Inherits="uc_menu_flyout_horizontal_v4" %>

<utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="header-menu-left list-inline col-md-9 uc_menu_flyout_horizontal_v4">
        <div class="row">
            <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
        </div>
    </div>
        <div class="header-menu-right col-md-3">
            <utilize:placeholder runat="server" ID="ph_shopping_cart"></utilize:placeholder>
        </div>
</utilize:panel>