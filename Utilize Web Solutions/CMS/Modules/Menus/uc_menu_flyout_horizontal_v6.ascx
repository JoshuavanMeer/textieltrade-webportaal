﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal_v6.ascx.vb" Inherits="uc_menu_flyout_horizontal_v3" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v5.ascx" TagPrefix="uc1" TagName="uc_content_search" %>


<utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="header-menu-left uc_menu_flyout_horizontal_v6 container">
        <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
    </div>
</utilize:panel>
