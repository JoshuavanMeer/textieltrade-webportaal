﻿
Partial Class uc_menu
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private us_code As String = ""

    Private Function get_menu_description() As String

        Dim lc_return_value As String = ""
        Dim qsc_structure As New Utilize.Data.QuerySelectClass
        qsc_structure.select_fields = "us_desc_" + Me.global_cms.Language
        qsc_structure.select_from = "ucm_structure"
        qsc_structure.select_order = "row_ord"
        qsc_structure.select_where = "us_code = @us_code and shop_code = @shop_code "

        If Not Me.global_ws.user_information.user_logged_on Then
            qsc_structure.select_where += " and (show_type = 0 or show_type = 2)"
        Else
            qsc_structure.select_where += " and (show_type = 0 or show_type = 1)"
        End If

        qsc_structure.add_parameter("us_code", us_code)
        qsc_structure.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

        Dim dt_data_Table As System.Data.DataTable = qsc_structure.execute_query()

        If dt_data_Table.Rows.Count > 0 Then
            lc_return_value = dt_data_Table.Rows(0).Item("us_desc_" + Me.global_cms.Language)
        Else
            lc_return_value = global_trans.translate_title("title_menu", "Menu", Me.global_ws.Language)
        End If

        Return lc_return_value
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet hier het niveaunummer goed zodat de css class goed doorgaat.
        Dim ll_Resume As Boolean = Me.Visible

        Me.set_style_sheet("uc_menu.css", Me)
        Me.us_code = Me.get_page_parameter("uscode")
        Me.create_menu()

        If ll_Resume Then
            Me.title_menu.Text = Me.get_menu_description()
        End If

        If ll_Resume Then
            ' Hier maken we een button voor de navbar aan
            Dim uc_navbar_button As Base.base_usercontrol_base = LoadUserControl("~/CMS/Modules/Header/uc_navbar_button.ascx")
            uc_navbar_button.set_property("datatarget", ".uc_category_menu")
            uc_navbar_button.set_property("cssclass", "category_menu_icon")
            uc_navbar_button.set_property("icon", "glyphicon glyphicon-asterisk")

            Me.Page.Master.FindControl("uc_header").FindControl("ph_nav_buttons").Controls.Add(uc_navbar_button)
        End If

        If ll_Resume Then
            '  Als de bovenliggende categorie niet leeg is:
            Me.link_level_up.Visible = Not us_code.Trim() = ""

            If Not us_code = "" Then
                ' Haal de bovenliggende categorie op
                Dim lc_parent_cat As String = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_parent", us_code)
                Dim lc_category_url As String = Utilize.Data.DataProcedures.GetValue("ucm_structure", "tgt_url_" + Me.global_ws.Language, lc_parent_cat)

                Me.lt_level_up.Text = global_trans.translate_label("link_level_up", "Een niveau omhoog", Me.global_ws.Language)
                Me.link_level_up.NavigateUrl = Me.ResolveCustomUrl("~/" + lc_category_url)
            End If
        End If

        Me.Visible = ll_Resume
    End Sub

    Private Function create_menu() As Boolean
        Dim ll_Resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_Resume Then
            ' Maak een nieuwe datatabla aan
            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "ucm_structure"
            qsc_data_table.select_order = "row_ord"

            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            qsc_data_table.select_where = "us_parent = @us_parent and shop_code = @shop_code "

            ' Als de gebruiker niet is ingelogd, dan willen we niet de items tonen die beschikbaar mogen zijn wanneer de gebruiker is ingelogd
            If Not Me.global_ws.user_information.user_logged_on Then
                qsc_data_table.select_where += "and (show_type = 0 or show_type = 2)"
            Else
                qsc_data_table.select_where += "and (show_type = 0 or show_type = 1)"
            End If

            qsc_data_table.add_parameter("us_parent", Me.us_code)
            qsc_data_table.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            ' Als de module voor meerdere menu's aan staat, dan moet er een filter gezet worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_menu") = True Then
                qsc_data_table.select_where += "and menu_group_id = @menu_group_id"
                qsc_data_table.add_parameter("menu_group_id", Me.block_id)
            End If

            ' En laad de tabel
            dt_data_table = qsc_data_table.execute_query()

            ll_Resume = dt_data_table.Rows.Count > 0

            If Not ll_Resume Then
                Dim lc_us_code As String = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_parent", us_code)

                If Not lc_us_code = "" Then
                    us_code = Utilize.Data.DataProcedures.GetValue("ucm_structure", "us_parent", us_code)
                    Me.create_menu()
                End If
            End If
        End If

        'start loop
        If ll_Resume Then
            Dim firstchild = True
            Dim lastchild = False

            For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                ''   Do While ll_move_next
                Dim lc_us_page_type As String = dr_data_row.Item("us_page_type")

                ' Maak een nieuwe li aan en voeg deze toe aan de ul
                Dim hgc_subli As New HtmlGenericControl("li")
                hgc_subli.Attributes.Add("class", "li-width")

                Me.ph_menu.Controls.Add(hgc_subli)

                ' Zoek of de li bestaat in de db als een parent
                Dim qsc_select As New Utilize.Data.QuerySelectClass

                If lc_us_page_type = "UWS_CATEGORIES" Then
                    qsc_select.select_fields = "*"
                    qsc_select.select_from = "uws_categories"
                    qsc_select.select_order = "row_ord"
                    qsc_select.select_where = "cat_parent = @cat_parent"
                    qsc_select.add_parameter("cat_parent", dr_data_row.Item("us_prod_cat"))

                    ' shop_code is empty for the main shop
                    ' shop_code is present for a focus shop
                    ' category_available true means all categories must be show (both main and focus shop)
                    ' category_available false means only categories of the focus shop is shown
                    Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
                    Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
                    If ll_category_avail Then
                        qsc_select.select_where += " and (shop_code = @shop_code or shop_code = '')"
                    Else
                        qsc_select.select_where += " and shop_code = @shop_code"
                    End If
                    qsc_select.add_parameter("shop_code", lc_shop_code)
                Else
                    qsc_select.select_fields = "*"
                    qsc_select.select_from = "ucm_structure"
                    qsc_select.select_order = "row_ord"
                    qsc_select.select_where = "us_parent = @us_code and shop_code = @shop_code"
                    qsc_select.add_parameter("us_code", dr_data_row.Item("us_code"))
                    qsc_select.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())
                End If

                Dim dt_children As System.Data.DataTable = qsc_select.execute_query()

                ' Maak een nieuwe hyperlink aan
                Dim hl_hyperlink As New System.Web.UI.WebControls.HyperLink
                Select Case lc_us_page_type
                    Case "TEXT"
                        hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + Me.ResolveCustomUrl(dr_data_row.Item("tgt_url_" + Me.global_cms.Language)))
                        hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)
                    Case "DEFAULT"
                        hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + Me.ResolveCustomUrl(dr_data_row.Item("tgt_url_" + Me.global_cms.Language)))
                        hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)
                    Case "LINK_EXTERNAL"
                        hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)

                        If dr_data_row.Item("us_page_url_" + Me.global_cms.Language) = "" Then
                            hl_hyperlink.NavigateUrl = dr_data_row.Item("us_page_url")
                        Else
                            hl_hyperlink.NavigateUrl = dr_data_row.Item("us_page_url_" + Me.global_cms.Language)
                        End If

                        hl_hyperlink.Target = "_blank"
                    Case "UWS_CATEGORIES"
                        If dr_data_row.Item("us_prod_cat") = "" Then
                            Dim lc_url As String = Me.global_cms.Language + "/webshop/product_list.aspx"

                            hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)
                            hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + lc_url)
                        Else
                            Dim lc_url As String = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", Me.global_ws.Language + dr_data_row.Item("us_prod_cat"), "lng_code + cat_code")

                            hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)
                            hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + lc_url)
                        End If
                    Case "LINK_INTERNAL"
                        Dim lc_page_url As String = dr_data_row.Item("us_page_url_" + Me.global_cms.Language)

                        If lc_page_url = "" Then
                            lc_page_url = dr_data_row.Item("us_page_url")
                        End If

                        ' Controleer hoe de pagina gestart wordt
                        If lc_page_url.ToLower().StartsWith("http") Then
                            ' Het is een volledige URL
                            hl_hyperlink.NavigateUrl = lc_page_url
                        Else
                            ' Het is een URL zonder domeinnaam
                            hl_hyperlink.NavigateUrl = "~/" + lc_page_url
                        End If
                        hl_hyperlink.Text = dr_data_row.Item("us_desc_" + Me.global_cms.Language)
                    Case Else
                        Exit Select
                End Select

                ' Voeg de hyperlink to aan de li
                hgc_subli.Controls.AddAt(0, hl_hyperlink)

                ' Ga naar de volgende regel in de tabel
            Next
        End If
        Return True
    End Function
End Class
