﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal_v2.ascx.vb" Inherits="uc_menu_flyout_horizontal_v2" %>

<div class="uc_menu_flyout_horizontal_v2">
    <utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse target-menu">
        <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
    </utilize:panel>
</div>