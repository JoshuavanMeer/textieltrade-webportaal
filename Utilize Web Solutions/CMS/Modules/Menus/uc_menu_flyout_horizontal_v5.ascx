﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal_v5.ascx.vb" Inherits="uc_menu_flyout_horizontal_v5" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v5.ascx" TagPrefix="uc1" TagName="uc_content_search" %>
<%@ Register src="~/Webshop/Modules/ShoppingCart/uc_shopping_cart_control.ascx" tagname="uc_shopping_cart_control5" tagprefix="uc5" %>

<utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="header-menu-left list-inline col-md-9 uc_menu_flyout_horizontal_v5">
        <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
    </div>
    <div class="header-menu-right col-md-3">
        <uc5:uc_shopping_cart_control5 ID="uc_shopping_cart_control1" runat="server" />
    </div>
</utilize:panel>