﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal.ascx.vb" Inherits="uc_menu_flyout_horizontal" %>

<div class="uc_menu_flyout_horizontal">
    <utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <utilize:placeholder runat="server" ID="ph_shopping_cart"></utilize:placeholder>
        <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
    </utilize:panel>
</div>