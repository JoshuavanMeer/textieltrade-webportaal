﻿Imports Utilize.Web.Solutions.CMS

Partial Class uc_menu_flyout_horizontal_v5
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

#Region " Level count"
    Private _level_count As Integer = 3
    Private _us_start As String = ""
    Private _dt_data_table As System.Data.DataTable

    ''' <summary>
    ''' This property defines the number of levels to be shown
    ''' </summary>
    ''' <value>An integer which indicates the number of levels</value>
    ''' <returns>An integer which indicates the number of levels</returns>
    ''' <remarks></remarks>
    Public Property level_count() As Integer
        Get
            Return _level_count
        End Get
        Set(ByVal value As Integer)
            _level_count = value
        End Set
    End Property

    Private _level_start As Integer = 1
    Public Property level_start As Integer
        Get
            Return _level_start
        End Get
        Set(ByVal value As Integer)
            _level_start = value
        End Set
    End Property
#End Region

    Private _css_class As String = ""
    Public Property css_class As String
        Get
            Return _css_class
        End Get
        Set(value As String)
            _css_class = value
        End Set
    End Property

    Private _block_id As String = ""
    Public Overloads Property block_id As String
        Get
            Return _block_id
        End Get
        Set(ByVal value As String)
            _block_id = value
        End Set
    End Property

    Private level_number As Integer = 0

    Private Property dt_data_table As System.Data.DataTable
        Get
            Return _dt_data_table
        End Get
        Set(ByVal value As System.Data.DataTable)
            _dt_data_table = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Zet hier het niveaunummer goed zodat de css class goed doorgaat.
        level_number = _level_start - 1

        Dim ll_Resume As Boolean = Me.Visible
        Me.set_style_sheet("uc_menu_flyout_horizontal_v5.css", Me)

        If ll_Resume And _level_start > 1 Then
            Dim lc_us_code As String = Me.get_page_parameter("UsCode")
            Dim lc_us_list As String = Me.get_page_parameter("UsParent")

            If lc_us_list = "" And level_start = 2 Then
                lc_us_list = lc_us_code

                ' Als het het tweede niveau is en er is geen us_code meegegeven, dan niet tonen.
                If lc_us_code = "" Then
                    ll_Resume = False
                End If
            Else
                ll_Resume = Not lc_us_list.Trim() = ""
            End If

            If ll_Resume Then
                If lc_us_list.EndsWith("|") Then
                    lc_us_list = lc_us_list.Substring(0, lc_us_list.LastIndexOf("|"))
                End If

                Dim la_us_list() As String = lc_us_list.Split("|")

                Try
                    Me._us_start = la_us_list(la_us_list.Length - (_level_start - 1))
                Catch ex As Exception
                    Me._us_start = "UTILIZE"
                End Try
            End If
        End If

        If ll_Resume Then
            ' deze functie itereert van het hoofmenu helemaal naar alle onderliggende menu's
            add_li_to_ul(Me.ph_menu, _us_start)
        End If

        Me.Visible = ll_Resume
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.Visible Then
            Me.Visible = Me.ph_menu.Controls(0).HasControls
        End If
    End Sub

    Private Sub add_li_to_ul(ByVal hgc_li As Object, ByVal us_parent As String, Optional ByVal current_level As Integer = 1)
        Dim ll_Resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_data_table As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_Resume Then
            ' Maak een nieuwe datatabla aan
            qsc_data_table = New Utilize.Data.QuerySelectClass
            dt_data_table = New System.Data.DataTable

            ' Initialiseren
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "ucm_structure"
            qsc_data_table.select_order = "row_ord"
            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            qsc_data_table.select_where = "us_parent = @us_parent and shop_code = @shop_code and hide_menu_item = 0"

            ' Als de gebruiker niet is ingelogd, dan willen we niet de items tonen die beschikbaar mogen zijn wanneer de gebruiker is ingelogd
            If Not Me.global_ws.user_information.user_logged_on Then
                qsc_data_table.select_where += " and (show_type = 0 or show_type = 2)"
            Else
                qsc_data_table.select_where += " and (show_type = 0 or show_type = 1)"
            End If

            qsc_data_table.add_parameter("us_parent", us_parent)
            qsc_data_table.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            ' Als de module voor meerdere menu's aan staat, dan moet er een filter gezet worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_menu") = True Then
                qsc_data_table.select_where += " and menu_group_id = @menu_group_id"
                qsc_data_table.add_parameter("menu_group_id", _block_id)
            End If

            ' En laad de tabel
            dt_data_table = qsc_data_table.execute_query()
            ll_Resume = dt_data_table.Rows.Count > 0
        End If

        'start loop
        If ll_Resume Then
            level_number += 1

            ' Er zijn regels gevonden dus ga door met een li en ul enz...
            ' Maak de hoofd control "UL" aan
            Dim hgc_ul As New HtmlGenericControl("ul")
            Select Case level_number
                Case 1
                    hgc_ul.Attributes.Add("class", "nav navbar-nav")
                Case Else
                    hgc_ul.Attributes.Add("class", "dropdown-menu")
            End Select

            ' Voeg de control toe aan de parent list item
            hgc_li.Controls.Add(hgc_ul)

            ' Ga naar de eerste regel in de datatable
            For Each dr_data_table As System.Data.DataRow In dt_data_table.Rows
                Dim lc_us_page_type As String = dr_data_table.Item("us_page_type")

                ' Maak een nieuwe li aan en voeg deze toe aan de ul
                Dim hgc_subli As New HtmlGenericControl("li")
                hgc_ul.Controls.Add(hgc_subli)

                ' Zoek of de li bestaat in de db als een parent
                Dim qsc_select As New Utilize.Data.QuerySelectClass

                If lc_us_page_type = "UWS_CATEGORIES" Then
                    qsc_select.select_fields = "*"
                    qsc_select.select_from = "uws_categories"
                    qsc_select.select_order = "row_ord"
                    qsc_select.select_where = "cat_parent = @cat_parent"
                    qsc_select.add_parameter("cat_parent", dr_data_table.Item("us_prod_cat"))

                    ' shop_code is empty for the main shop
                    ' shop_code is present for a focus shop
                    ' category_available true means all categories must be show (both main and focus shop)
                    ' category_available false means only categories of the focus shop is shown
                    Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
                    Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
                    If ll_category_avail Then
                        qsc_select.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
                    Else
                        qsc_select.select_where += " and uws_categories.shop_code = @shop_code"
                    End If
                    qsc_select.add_parameter("shop_code", lc_shop_code)
                Else
                    qsc_select.select_fields = "*"
                    qsc_select.select_from = "ucm_structure"
                    qsc_select.select_order = "row_ord"
                    qsc_select.select_where = "us_parent = @us_code and shop_code = @shop_code and hide_menu_item = 0"
                    qsc_select.add_parameter("us_code", dr_data_table.Item("us_code"))
                    qsc_select.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

                    ' Als de gebruiker niet is ingelogd, dan willen we niet de items tonen die beschikbaar mogen zijn wanneer de gebruiker is ingelogd
                    ' 0: always 1: only logged on 2: not logged on
                    If Not Me.global_ws.user_information.user_logged_on Then
                        qsc_select.select_where += " and (show_type = 0 or show_type = 2)"
                    Else
                        qsc_select.select_where += " and (show_type = 0 or show_type = 1)"
                    End If

                End If

                Dim dt_children As System.Data.DataTable = qsc_select.execute_query()

                ' Get the menu type
                Dim lc_category_menu_type As Integer = 0

                If dt_children.Rows.Count > 0 Then
                    If level_number = 1 Then
                        lc_category_menu_type = dr_data_table.Item("category_menu_type")
                        If lc_category_menu_type = 0 Then
                            hgc_subli.Attributes.Add("class", "dropdown")
                        Else
                            ' Don't show the standard menu on medium/large screens
                            hgc_subli.Attributes.Add("class", "dropdown hidden-md hidden-lg")
                        End If
                    Else
                        hgc_subli.Attributes.Add("class", "dropdown-submenu")
                    End If
                End If

                ' Maak een nieuwe hyperlink aan
                Dim hl_hyperlink As New System.Web.UI.WebControls.HyperLink
                ' Maak een 2e hyperlink voor het mega menu aan
                Dim hl_hyperlink_mega_menu As New System.Web.UI.WebControls.HyperLink

                Select Case lc_us_page_type
                    Case "TEXT"
                        hl_hyperlink.NavigateUrl = Me.Page.ResolveUrl("~/" + Me.ResolveUrl(dr_data_table.Item("tgt_url_" + Me.global_cms.Language)))
                        hl_hyperlink.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)

                        ' Controleer of de huidige us_code overeenkomt met de parameter
                        If dr_data_table.Item("us_code") = Me.get_page_parameter("UsCode") Then
                            ' Voeg de class "active" toe.
                            Dim lc_class As String = hgc_subli.Attributes.Item("class")
                            lc_class += " active"

                            hgc_subli.Attributes.Add("class", lc_class)
                        End If
                    Case "DEFAULT"
                        hl_hyperlink.NavigateUrl = Me.Page.ResolveUrl("~/" + Me.ResolveUrl(dr_data_table.Item("tgt_url_" + Me.global_cms.Language)))
                        hl_hyperlink.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)

                        ' Controleer of de huidige us_code overeenkomt met de parameter
                        If dr_data_table.Item("us_code") = Me.get_page_parameter("UsCode") Then
                            ' Voeg de class "active" toe.
                            Dim lc_class As String = hgc_subli.Attributes.Item("class")
                            lc_class += " active"
                            hgc_subli.Attributes.Add("class", lc_class)
                        End If
                    Case "LINK_EXTERNAL"
                        hl_hyperlink.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)

                        If dr_data_table.Item("us_page_url_" + Me.global_cms.Language) = "" Then
                            hl_hyperlink.NavigateUrl = dr_data_table.Item("us_page_url")
                        Else
                            hl_hyperlink.NavigateUrl = dr_data_table.Item("us_page_url_" + Me.global_cms.Language)
                        End If

                        hl_hyperlink.Target = "_blank"

                        If Request.Url.ToString.ToLower().EndsWith(hl_hyperlink.Text.ToLower()) Then
                            ' Controleer of de huidige us_code overeenkomt met de parameter
                            ' Voeg de class "active" toe.
                            Dim lc_class As String = hgc_subli.Attributes.Item("class")
                            lc_class += " active"
                            hgc_subli.Attributes.Add("class", lc_class)
                        End If
                    Case "UWS_CATEGORIES"
                        '0 is standaard
                        '1 is mega menu
                        Dim lc_url As String = ""

                        If dr_data_table.Item("us_prod_cat") = "" Then
                            lc_url = Me.Page.ResolveUrl("~/") + Me.global_cms.Language + "/webshop/product_list.aspx"

                            If Request.Url.ToString.ToLower().Contains("/webshop/product_list.aspx") Then
                                ' Controleer of de huidige us_code overeenkomt met de parameter
                                ' Voeg de class "active" toe.
                                Dim lc_class As String = hgc_subli.Attributes.Item("class")
                                lc_class += " active"
                                hgc_subli.Attributes.Add("class", lc_class)
                            End If
                        Else
                            lc_url = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", Me.global_ws.Language + dr_data_table.Item("us_prod_cat"), "lng_code + cat_code")

                            If level_number = 1 Then
                                lc_category_menu_type = dr_data_table.Item("category_menu_type")

                                ' Standard menu
                                If lc_category_menu_type = 0 Then
                                    hgc_subli.Attributes.Add("class", "dropdown")
                                Else
                                    ' Don't show the standard menu on medium/large screens
                                    hgc_subli.Attributes.Add("class", "dropdown hidden-md hidden-lg")
                                End If
                            Else
                                hgc_subli.Attributes.Add("class", "dropdown-submenu")
                            End If

                            If Not String.IsNullOrEmpty(Me.get_page_parameter("cat_code")) Or Request.Url.ToString.ToLower().Contains("/webshop/product_list.aspx") Then
                                If Me.get_page_parameter("cat_code").ToLower() = dr_data_table.Item("us_prod_cat").ToLower() Or Me.get_page_parameter("cat_parent").Contains(dr_data_table.Item("us_prod_cat")) Then
                                    ' Controleer of de huidige us_code overeenkomt met de parameter
                                    ' Voeg de class "active" toe.
                                    Dim lc_class As String = hgc_subli.Attributes.Item("class")
                                    lc_class += " active"
                                    hgc_subli.Attributes.Add("class", lc_class)
                                End If
                            End If
                        End If

                        hl_hyperlink.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)
                        If dr_data_table.Item("us_prod_cat") = "" Then
                            hl_hyperlink.NavigateUrl = Me.Page.ResolveUrl(lc_url)
                        Else
                            hl_hyperlink.NavigateUrl = Me.Page.ResolveUrl("~/" + lc_url)
                        End If
                        ' Creer hier het standaard menu
                        ' Ongeacht welke menu type is gekozen moet het standaard menu voor kleine screens toegevoegd worden
                        add_li_to_ul_webshop_categories(hgc_subli, dr_data_table.Item("us_prod_cat"), current_level + 1)

                        ' Creer hier het mega menu
                        If lc_category_menu_type = 1 Then
                            hl_hyperlink_mega_menu.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)
                            hl_hyperlink_mega_menu.NavigateUrl = Me.Page.ResolveUrl("~/" + lc_url)

                            ' Maak een nieuwe list item aan voor de mege menu en voeg deze toe aan de ul
                            Dim hgc_subli_mega As New HtmlGenericControl("li")
                            hgc_subli_mega.Attributes.Add("class", "dropdown mega-menu-fullwidth visible-md visible-lg")
                            hgc_ul.Controls.Add(hgc_subli_mega)

                            ' Voeg de hyperlink toe aan de list item (dit is de titel van het menu item)
                            hgc_subli_mega.Controls.Add(hl_hyperlink_mega_menu)

                            If Not String.IsNullOrEmpty(Me.get_page_parameter("cat_code")) Or Request.Url.ToString.ToLower().Contains("/webshop/product_list.aspx") Then
                                If Me.get_page_parameter("cat_code").ToLower() = dr_data_table.Item("us_prod_cat").ToLower() Or Me.get_page_parameter("cat_parent").Contains(dr_data_table.Item("us_prod_cat")) Then
                                    ' Controleer of de huidige us_code overeenkomt met de parameter
                                    ' Voeg de class "active" toe.
                                    Dim lc_class As String = hgc_subli_mega.Attributes.Item("class")
                                    lc_class += " active"
                                    hgc_subli_mega.Attributes.Add("class", lc_class)
                                End If
                            End If

                            ' Creeer hier het mega menu met categorieen en 1 niveau van subcategorie
                            Me.create_categories_mega_menu_structure(hgc_subli_mega, dr_data_table.Item("us_prod_cat"))
                        End If
                    Case "LINK_INTERNAL"
                        Dim lc_page_url As String = dr_data_table.Item("us_page_url_" + Me.global_cms.Language)

                        If lc_page_url = "" Then
                            lc_page_url = dr_data_table.Item("us_page_url")
                        End If

                        ' Controleer hoe de pagina gestart wordt
                        If lc_page_url.ToLower().StartsWith("http") Then
                            ' Het is een volledige URL
                            hl_hyperlink.NavigateUrl = lc_page_url
                        Else
                            ' Het is een URL zonder domeinnaam
                            hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + lc_page_url)
                        End If
                        hl_hyperlink.Text = dr_data_table.Item("us_desc_" + Me.global_cms.Language)
                    Case Else
                        Exit Select
                End Select

                ' de functie word in zichzelf aangeroepen om de kinderen van de huidige menu lijst toe te voegen
                add_li_to_ul(hgc_subli, dr_data_table.Item("us_code"), current_level + 1)

                ' Voeg de hyperlink to aan de li
                hgc_subli.Controls.AddAt(0, hl_hyperlink)

                ' Ga naar de volgende regel in de tabel
            Next
        End If

        ' nieuw
        If Not ll_Resume Then
            level_number += 1
            qsc_data_table = New Utilize.Data.QuerySelectClass
            dt_data_table = New System.Data.DataTable

            ' Initialiseren
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "ucm_structure"
            qsc_data_table.select_order = "row_ord"

            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            qsc_data_table.select_where = "us_code = @us_parent and shop_code = @shop_code"
            qsc_data_table.add_parameter("us_parent", us_parent)
            qsc_data_table.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            ' Als de gebruiker niet is ingelogd, dan willen we niet de items tonen die beschikbaar mogen zijn wanneer de gebruiker is ingelogd
            If Not Me.global_ws.user_information.user_logged_on Then
                qsc_data_table.select_where += " and (show_type = 0 or show_type = 2)"
            Else
                qsc_data_table.select_where += " and (show_type = 0 or show_type = 1)"
            End If

            dt_data_table = qsc_data_table.execute_query()
            ll_Resume = dt_data_table.Rows.Count > 0

            If ll_Resume Then
                Dim parent As String = dt_data_table.Rows(0).Item("us_parent")

                If current_level = 1 And Not parent = "" Then
                    Dim hgc_ul As New HtmlGenericControl("ul")
                    hgc_ul.Attributes.Add("class", "level" + level_number.ToString())

                    ' toplaag wel of niet bij wel moet er een ID attribuut toegevoegd worden aan de element met de naam nav
                    If us_parent = "" Then
                        hgc_ul.Attributes.Add("id", "nav")
                    End If

                    ' Voeg de control toe aan de parent list item
                    If Me.get_page_parameter("usparent").Contains(dt_data_table.Rows(0).Item("us_code")) Or dt_data_table.Rows(0).Item("us_code") = Me.get_page_parameter("uscode") Then
                        hgc_li.Attributes.Add("class", "active")
                    End If

                    hgc_li.Controls.Add(hgc_ul)
                    add_li_to_ul(hgc_li, parent, current_level + 1)
                End If
            End If
        End If
        level_number -= 1
    End Sub

    Private Sub create_categories_mega_menu_structure(ByVal hgc_li As HtmlGenericControl, ByVal cat_code As String)
        ' AANMAKEN VAN DE MEGA MENU STRUCTUUR
        '<ul class="dropdown-menu">
        Dim hgc_ul_mega As New HtmlGenericControl("ul")
        hgc_ul_mega.Attributes.Add("class", "dropdown-menu")
        hgc_li.Controls.Add(hgc_ul_mega)

        '        <li>
        Dim hgc_li_mega As New HtmlGenericControl("li")
        hgc_ul_mega.Controls.Add(hgc_li_mega)

        '            <div class="mega-menu-content disable-icons">
        Dim hgc_div_content As New HtmlGenericControl("div")
        hgc_div_content.Attributes.Add("class", "mega-menu-content disable-icons")
        hgc_li_mega.Controls.Add(hgc_div_content)

        '                <div class="container">
        Dim hgc_div_container As New HtmlGenericControl("div")
        hgc_div_container.Attributes.Add("class", "container")
        hgc_div_content.Controls.Add(hgc_div_container)

        '                    <div class="row">
        Dim hgc_div_row As New HtmlGenericControl("div")
        hgc_div_row.Attributes.Add("class", "row")
        hgc_div_container.Controls.Add(hgc_div_row)

        ' TOEVOEGEN VAN DE CATEGORIEEN
        Me.add_categories_to_mega_menu(hgc_div_row, cat_code, 0)
    End Sub
    Private Sub add_categories_to_mega_menu(ByVal hgc As HtmlGenericControl, ByVal cat_code As String, ByVal level As Integer)
        Dim ll_Resume As Boolean = True

        ' Ophogen van het niveau
        level += 1

        'BEGIN BESPOKE: DEV-31 
        ll_Resume = IIf(Me.global_cms.get_cms_setting("level_mega_menu") = 0, level <= 2, level <= Me.global_cms.get_cms_setting("level_mega_menu"))
        'END BESPOKE: DEV-31 

        If Not ll_Resume Then
            Exit Sub
        End If

        ' Ophalen van de categorieen met als parent de meegeven categorie

        ' Declareer een nieuwe datatable
        Dim qsc_data_table As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_Resume Then
            Dim userCategoriesModel As New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = cat_code,
                    .GetAllCategories = False
            }
            dt_data_table = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)

            ' Controleer of er categorieen zijn gevonden
            ll_Resume = dt_data_table.Rows.Count > 0
        End If

        Dim ln_bootstrap_columns_left As Integer = 12
        Dim ln_count As Integer = 0

        ' Loop door de gevonden categorieen
        For Each dr_data_table As System.Data.DataRow In dt_data_table.Rows
            ln_count += 1
            ' Maak een nieuwe hyperlink aan
            Dim lc_navigate_url As String = Me.Page.ResolveUrl("~/" + dr_data_table.Item("tgt_url"))
            Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink
            With hl_hyperlink
                .NavigateUrl = lc_navigate_url
                .Text = dr_data_table.Item("cat_desc_tran")
            End With

            ' Maak de column aan bij het hoofdniveau
            If level = 1 Then
                ' Maak een nieuwe row elke 7 categorieen
                If (ln_count Mod 7) = 0 Then
                    Dim hgc_div_row As New HtmlGenericControl("div")
                    hgc_div_row.Attributes.Add("class", "row")
                    hgc.Controls.Add(hgc_div_row)
                    ln_bootstrap_columns_left = 12
                End If
                '<div class="col-md-x">
                Dim hgc_div_col As New HtmlGenericControl("div")
                Dim newWidth As Integer = Me.calculate_column_width(ln_count, dt_data_table.Rows.Count, ln_bootstrap_columns_left)
                ln_bootstrap_columns_left -= newWidth
                Dim col_md As String = "col-md-" + newWidth.ToString()
                hgc_div_col.Attributes.Add("class", col_md)
                hgc.Controls.Add(hgc_div_col)

                ' Voeg een h3 heading toe voor het hoofdniveau
                '<h3 class="mega-menu-heading">
                '    <a href="/Lookx%20-%20Webshop%20B2C%20-%203.0/Utilize%20Web%20Solutions/nl/3/shop-by-collection/make-up.aspx">
                '       MAKE -UP
                '    </a>
                '</h3>
                Dim hgc_h3 As New HtmlGenericControl("h3")
                hgc_h3.Attributes.Add("class", "mega-menu-heading")
                hgc_div_col.Controls.Add(hgc_h3)

                ' Voeg de hyperlink toe aan de h3 heading
                hgc_h3.Controls.Add(hl_hyperlink)

                ' Voeg een list toe voor de subcategorieen
                '<ul class="list-unstyled style-list">
                '</ul>
                Dim hgc_ul As New HtmlGenericControl("ul")
                hgc_ul.Attributes.Add("class", "list-unstyled style-list pull-left")
                hgc_div_col.Controls.Add(hgc_ul)

                ' Voeg de subcategorieen toe
                Me.add_categories_to_mega_menu(hgc_ul, dr_data_table.Item("cat_code"), level)

                Me.add_category_banner_to_mega_menu(hgc_ul.Parent(), dr_data_table)
            Else
                If dt_data_table.Rows.Count > 6 And ln_count = 7 Then
                    Dim hgc_ul As New HtmlGenericControl("ul")
                    hgc_ul.Attributes.Add("class", "list-unstyled style-list pull-right")
                    hgc.Parent().Controls.Add(hgc_ul)
                    hgc = hgc_ul
                End If
                ' Voeg een list item toe aan de list, dit zijn de subcategorieen
                '   <li><a href="/Lookx%20-%20Webshop%20B2C%20-%203.0/Utilize%20Web%20Solutions/nl/3/make-up/shop-by-collection/camouflage.aspx">Camouflage</a></li>
                '   <li><a href="/Lookx%20-%20Webshop%20B2C%20-%203.0/Utilize%20Web%20Solutions/nl/3/make-up/shop-by-collection/kwasten.aspx">Kwasten</a></li>
                Dim hgc_li As New HtmlGenericControl("li")
                hgc_li.Controls.Add(hl_hyperlink)
                hgc.Controls.Add(hgc_li)

                'BEGIN BESPOKE: DEV-31 
                ''Query of cat een parent is 
                Dim qsc_cat_parent As New Utilize.Data.QuerySelectClass
                Dim dt_cat_parent As System.Data.DataTable = Nothing
                ''als die dat heeft dan moet er h3 worden gemaakt en om de link worden gezet.
                ''daarna een nieuwe ul aanmaken en die geeft mee met de functie hgc 
                qsc_cat_parent.select_fields = "*"
                qsc_cat_parent.select_from = "UWS_CATEGORIES"
                qsc_cat_parent.select_where = "cat_parent = @cat_code and shop_code = @shop_code and cat_show = 1"
                qsc_cat_parent.add_parameter("cat_code", dr_data_table.Item("cat_code"))
                qsc_cat_parent.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())
                dt_cat_parent = qsc_cat_parent.execute_query()

                If dt_cat_parent.Rows.Count > 0 And level <= Me.global_cms.get_cms_setting("level_mega_menu") Then
                    Dim hgc_h3 As New HtmlGenericControl("h3")
                    hgc_h3.Attributes.Add("class", "mega-menu-heading")
                    hgc_li.Controls.Add(hgc_h3)

                    ' Voeg de hyperlink toe aan de h3 heading
                    hgc_h3.Controls.Add(hl_hyperlink)

                    Dim hgc_ul As New HtmlGenericControl("ul")
                    hgc_ul.Attributes.Add("class", "List-unstyled style-list pull-left")
                    hgc_li.Controls.Add(hgc_ul)
                    Me.add_categories_to_mega_menu(hgc_ul, dr_data_table.Item("cat_code"), level)
                End If
            End If
            'END BESPOKE: DEV-31
            ' Ga naar de volgende regel in de tabel, de volgende categorie
        Next
    End Sub
    Private Sub add_category_banner_to_mega_menu(ByVal hgc As HtmlGenericControl, ByVal dr_data_table_row As System.Data.DataRow)
        ' Voeg de banner afbeelding toe
        If Not String.IsNullOrEmpty(dr_data_table_row.Item("cat_image")) Then
            Dim lc_image_url As String = Page.ResolveUrl("~/" + dr_data_table_row.Item("cat_image"))
            Dim lc_alternate_text As String = dr_data_table_row.Item("cat_desc_tran").replace("'", "").replace("""", "")
            Dim img_banner_image As New Utilize.Web.UI.WebControls.image
            With img_banner_image
                .ImageUrl = lc_image_url
                .AlternateText = lc_alternate_text
                .CssClass = "img-responsive visible-md visible-lg"
            End With
            hgc.Controls.Add(img_banner_image)
        End If
    End Sub

    ''' <summary>
    ''' Calculates the bootstrap grid width for the main categories in the mega menu
    ''' </summary>
    ''' <param name="index"><code>Integer</code>Index of the current category</param>
    ''' <param name="categories_count "><code>Integer</code>Number of main categories in the mega menu</param>
    ''' <param name="columns_left"><code>Integer</code>Number of columns left in the boostrap 12 columns grid</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculate_column_width(ByVal index As Integer, ByVal categories_count As Integer, ByVal columns_left As Integer) As Integer
        If categories_count > 6 Then
            categories_count = 6
        End If
        ' Calculate the width depended on the number of categories and the bootstrap grid
        Dim width As Integer = 12 / categories_count
        width = Decimal.Floor(width)

        ' Stretch the last column to a total of 12
        If index = categories_count Then
            width = columns_left
        End If

        Return width
    End Function

    Private Sub add_li_to_ul_webshop_categories(ByVal hgc_li As HtmlGenericControl, Optional ByVal cat_code As String = "", Optional ByVal current_level As Integer = 1)
        level_number += 1

        Dim ll_Resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim dt_categories As System.Data.DataTable = Nothing

        If ll_Resume Then
            ' Haal de categorieën op.
            Dim userCategoriesModel As New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = cat_code,
                    .GetAllCategories = False
            }
            dt_categories = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)

            ll_Resume = dt_categories.Rows.Count > 0
        End If

        If ll_Resume And level_number < 3 Then
            ' Er zijn regels gevonden dus ga door met een li en ul enz...
            ' Maak de hoofd control "UL" aan
            Dim hgc_ul As New HtmlGenericControl("ul")

            If level_number = 1 Then
                hgc_ul.Attributes.Add("class", "nav navbar-nav")
            Else
                hgc_ul.Attributes.Add("class", "dropdown-menu")
            End If

            ' Voeg de control toe aan de parent list item
            hgc_li.Controls.Add(hgc_ul)

            ' Ga naar de eerste regel in de datatable
            Dim firstchild As Boolean = True
            Dim lastchild As Boolean = False

            For Each dr_data_row As System.Data.DataRow In dt_categories.Rows

                ' Maak een nieuwe li aan en voeg deze toe aan de ul
                Dim hgc_subli As New HtmlGenericControl("li")
                hgc_ul.Controls.Add(hgc_subli)

                ' Maak een nieuwe hyperlink aan
                Dim lc_navigate_url As String = Me.Page.ResolveUrl("~/" + dr_data_row.Item("tgt_url"))
                Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink
                With hl_hyperlink
                    .NavigateUrl = lc_navigate_url
                    .Text = dr_data_row.Item("cat_desc_tran")
                End With

                ' Voeg de hyperlink to aan de li
                hgc_subli.Controls.Add(hl_hyperlink)

                If Me.get_page_parameter("cat_code") = dr_data_row.Item("cat_code") Then
                    ' Controleer of de huidige categorie code overeenkomt met de parameter
                    ' Voeg de class "active" toe.
                    Dim lc_class As String = hgc_subli.Attributes.Item("class")
                    lc_class += " active"
                    hgc_subli.Attributes.Add("class", lc_class)
                End If

                ' de functie word in zichzelf aangeroepen om de kinderen van de huidige menu lijst toe te voegen
                add_li_to_ul_webshop_categories(hgc_subli, dr_data_row.Item("cat_code"))
            Next
        End If
        level_number -= 1
    End Sub
End Class
