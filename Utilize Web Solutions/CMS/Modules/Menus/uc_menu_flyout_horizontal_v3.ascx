﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_menu_flyout_horizontal_v3.ascx.vb" Inherits="uc_menu_flyout_horizontal_v3" %>

<%@ Register Src="~/CMS/Modules/ContentSearch/uc_content_search_v3.ascx" TagPrefix="uc1" TagName="uc_content_search" %>

<utilize:panel runat="server" id="pnl_menu_flyout" cssclass="uc_menu_flyout collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="header-menu-left list-inline col-md-9 uc_menu_flyout_horizontal_v3">
        <utilize:placeholder ID="ph_menu" runat="server"></utilize:placeholder>
    </div>
    <div class="header-menu-right col-md-3">
        <ul class="search_icon list-inline pull-right">
            <li>
                <a onclick="set_close_button(); return false;" id="search_button" class="search fa fa-search search-btn search-button"></a>
            </li>
        </ul>
        <utilize:placeholder runat="server" ID="ph_shopping_cart"></utilize:placeholder>
        <uc1:uc_content_search runat="server" ID="uc_content_search" />
         <ul class="list-inline right-topbar pull-right">
            <li runat="server" id="li_shopping_cart" class="hidden-lg hidden-md">
                <utilize:translatelink runat="server" ID="link_show_basket" Text="Winkelwagen"></utilize:translatelink>
            </li>
        </ul>
    </div>
</utilize:panel>