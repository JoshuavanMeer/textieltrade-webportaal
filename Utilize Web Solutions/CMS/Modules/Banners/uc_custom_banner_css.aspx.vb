﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim lc_id = Request.Params("CssCode")

        If ll_resume Then
            ll_resume = Not lc_id Is Nothing
        End If

        If ll_resume Then
            'haal de css op
            Dim lc_css As String = Utilize.Data.DataProcedures.GetValue("ucm_cus_banner_block", "css_content", lc_id)
            Dim background_image_css = ""
            Dim dt_return_banners_detail As New System.Data.DataTable
            Dim qsc_return_banners_detail As New Utilize.Data.QuerySelectClass

            'haal de gegevens van alle banners van het banner blok op
            qsc_return_banners_detail.select_fields = "*"
            qsc_return_banners_detail.select_from = "UCM_CUS_BANNERS"
            qsc_return_banners_detail.select_order = "rec_id"
            qsc_return_banners_detail.select_where = "HDR_ID = @hdr_id"
            qsc_return_banners_detail.add_parameter("@hdr_id", lc_id)

            dt_return_banners_detail = qsc_return_banners_detail.execute_query()

            'haal voor elke banner de achtergrond afbeelding op wanneer deze aanwezig is
            For Each row As System.Data.DataRow In dt_return_banners_detail.Rows
                Try
                    Dim id As String = lc_id + row.Item("rec_id").ToString()
                    Dim image As String = row.Item("background_img")
                    Dim imageURL As String = ""

                    If Not image = String.Empty Then
                        imageURL = ResolveUrl("~/" + image)
                    End If
                    background_image_css += "." + imageURL.Substring(imageURL.LastIndexOf("/") + 1, imageURL.LastIndexOf(".") - imageURL.LastIndexOf("/") - 1) + "{background-image: url('" + imageURL + "'); background-size: contain;}"
                Catch ex As Exception
                End Try
            Next

            'bouw de css om tot een style sheet
            lc_css += background_image_css
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_css)
            Response.End()
        End If
    End Sub
End Class
