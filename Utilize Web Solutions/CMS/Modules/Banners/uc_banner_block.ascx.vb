﻿Imports Utilize.Data.API.CMS

Partial Class uc_banner_block
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private Property tm_banner_interval As Int32 = 3

#Region "Functions"
    Public Function color_opacity(ByVal Hex As String, amount As Decimal, Optional background As Boolean = False) As String
        Dim result = ""

        Dim red_org As String = "&H" & Hex.Substring(1, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(3, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(5, 2)
        Dim blue As Integer = CInt(blue_org)

        If background Then
            If green > 250 And red > 250 And blue > 250 Then
                green = 240
                red = 240
                blue = 240
            End If
        End If

        Dim amount_string = amount.ToString.Replace(",", ".")

        result = "rgba(" + red.ToString() + ", " + green.ToString() + ", " + blue.ToString() + ", " + amount_string + ")"

        Return result
    End Function

    Public Function darken(ByVal hex As String) As String
        hex = Replace(hex, "#", "")
        Dim red_org As String = "&H" & hex.Substring(0, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & hex.Substring(2, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & hex.Substring(4, 2)
        Dim blue As Integer = CInt(blue_org)

        If blue < 60 And red < 60 And green < 60 Then
            red = 255
            blue = 255
            green = 255
        Else
            blue = 0
            green = 0
            red = 0
        End If

        Dim result = "rgb(" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + ")"
        Return result
    End Function

    Private Sub create_static_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object)
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As System.Data.DataTable = Nothing

        If ll_resume Then
            ' Maak een nieuwe datatable aan
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "*"
                .select_from = "UCM_BANNERS"
                .select_where = "hdr_id = @hdr_id"
                .select_order = "row_ord"
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If
        Dim dt_banner_block As New System.Data.DataTable

        If ll_resume Then
            Dim qsc_banner_block As New Utilize.Data.QuerySelectClass
            With qsc_banner_block
                .select_fields = "banner_scaling, item_layout"
                .select_from = "ucm_banner_blocks"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_banner_block.add_parameter("rec_id", dt_banner.Rows(0).Item("hdr_id"))

            dt_banner_block = qsc_banner_block.execute_query()
        End If

        Dim hgc_row As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
        hgc_row.Attributes.Add("class", "row")

        hgc_banner_block.controls.add(hgc_row)

        ' Ga naar de eerste regel van de banner tabel
        '  Dim ll_move_next As Boolean = udt_banner.table_move_first()
        Dim ln_layout_counter As Integer = 0

        Dim lc_counter As Integer = 0
        ' Loop door de regels heen
        For Each dr_banner As System.Data.DataRow In dt_banner.Rows
            Dim lc_banner_desc As String = dr_banner.Item("banner_desc_" + Me.global_cms.Language)

            Dim hgc_wrapper_div As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            hgc_wrapper_div.Attributes.Add("class", "static-banner col-md-" + dt_banner_block.Rows(0).Item("item_layout"))
            hgc_wrapper_div.Attributes.Add("id", "static_banner_image" + banner_block_id + lc_counter.ToString())
            hgc_row.Controls.Add(hgc_wrapper_div)

            ln_layout_counter += dt_banner_block.Rows(0).Item("item_layout")

            If ln_layout_counter >= 12 Then
                Dim hgc_clearfix As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                hgc_clearfix.Attributes.Add("class", "clearfix margin-bottom-10")

                hgc_row.Controls.Add(hgc_clearfix)

                ln_layout_counter = 0
            End If

            ' Maak een nieuwe afbeelding aan

            Dim img_banner_image As New Utilize.Web.UI.WebControls.image
            If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                img_banner_image.ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                img_banner_image.AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                img_banner_image.Attributes.Add("class", "img-responsive")
            ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                hgc_wrapper_div.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center; background-repeat: no-repeat;")
                hgc_wrapper_div.Attributes.Add("class", hgc_wrapper_div.Attributes("class") + " banner_background banner_static")
                Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                If System.IO.File.Exists(Server.MapPath(path)) Then
                    Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "banner_block_resize" + lc_counter.ToString() + banner_block_id, "$(document).ready( function () {banner_resizing('" + image1.Height.ToString() + "', '" + hgc_wrapper_div.Attributes("id") + "');});", True)
                End If
            End If

            Dim lt_banner_desc As New literal
            With lt_banner_desc
                .Text = "<h2>" + lc_banner_desc + "</h2>"
            End With

            If dr_banner.Item("add_hover") Or dr_banner.Item("show_text") Or dr_banner.Item("add_hover_image") Then

                hgc_wrapper_div.Attributes.Add("class", hgc_wrapper_div.Attributes("class") + " hover_effect")

                Dim lc_banner_desc_color As String = dr_banner.Item("title_color")

                Dim lc_subbanner_desc As String = dr_banner.Item("banner_subdesc_" + Me.global_cms.Language)
                Dim lc_subbanner_desc_color As String = dr_banner.Item("subtitle_color")

                Dim lc_button_color As String = dr_banner.Item("button_color")
                Dim lc_banner_button As String = dr_banner.Item("banner_button_" + Me.global_cms.Language)

                Dim lc_opacity As String = dr_banner.Item("opacity")
                If lc_opacity = "" Then
                    lc_opacity = "0"
                End If


                Dim hgc_hover_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                If dr_banner.Item("show_text") Then
                    hgc_hover_block.Attributes.Add("class", "banner_block_hover show_always banner_static")
                Else
                    hgc_hover_block.Attributes.Add("class", "banner_block_hover banner_static")
                End If

                Dim hgc_hover_block_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                hgc_hover_block_inner.Attributes.Add("class", "banner_block_hover_inner")
                hgc_hover_block_inner.Attributes.Add("style", "border-color: " + lc_button_color)

                Dim bool_has_content As Boolean = False

                If Not lc_banner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                    Dim hgc_banner_title As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_title.Attributes.Add("class", "banner_title")
                    hgc_banner_title.InnerHtml = "<h2 style='color: " + lc_banner_desc_color + "!important;border-bottom-color: " + lc_banner_desc_color + "'>" + lc_banner_desc + "</h2>"

                    hgc_hover_block_inner.Controls.Add(hgc_banner_title)
                    bool_has_content = True
                End If

                If Not lc_subbanner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                    Dim hgc_banner_subtitle As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_subtitle.Attributes.Add("class", "banner_subtitle")
                    hgc_banner_subtitle.InnerHtml = "<p style='color: " + lc_subbanner_desc_color + "'>" + lc_subbanner_desc + "</p>"

                    hgc_hover_block_inner.Controls.Add(hgc_banner_subtitle)
                    bool_has_content = True
                End If

                If dr_banner.Item("use_hover_color") And dr_banner.Item("show_text") Then
                    hgc_hover_block.Attributes.Add("style", "transition: background 0.5s;")
                End If

                If dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And Not dr_banner.Item("add_hover_image") Then
                    Dim hover_block_id = "block_hover" + banner_block_id + lc_counter.ToString()
                    hgc_hover_block.Attributes.Add("id", hover_block_id)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + hover_block_id + lc_counter.ToString(), "$(document).ready( function () {set_hover_background('" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                ElseIf dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And dr_banner.Item("add_hover_image") Then
                    Dim hover_block_id = "block_hover" + banner_block_id + lc_counter.ToString()

                    Dim hgc_color_opacity_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_color_opacity_block.Attributes.Add("id", hover_block_id)

                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                        hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div_static")
                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div")
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + hover_block_id + lc_counter.ToString(), "$(document).ready( function () {set_hover_background_with_image('" + hgc_wrapper_div.Attributes("id") + "','" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                    hgc_wrapper_div.Controls.Add(hgc_color_opacity_block)
                End If

                If Not lc_banner_button = "" And dr_banner.Item("add_hover") Or dr_banner.Item("show_text") Then
                    Dim lc_button_color_darkend As String = darken(lc_button_color)
                    Dim hgc_banner_button As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_button.Attributes.Add("class", "banner_button")
                    Dim hgc_banner_button_link As New System.Web.UI.HtmlControls.HtmlGenericControl("a")
                    hgc_banner_button_link.Attributes.Add("class", "banner_button_link btn-u btn-brd btn-brd-hover btn-u-light")
                    hgc_banner_button_link.Attributes.Add("style", "color: " + lc_button_color + "!important; border-color: " + lc_button_color + ";")
                    hgc_banner_button_link.Attributes.Add("id", "banner" + banner_block_id + lc_counter.ToString())

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "button_hover_banner" + banner_block_id + lc_counter.ToString(), "$(document).ready( function () {banner_block_button('" + banner_block_id + lc_counter.ToString() + "', '" + lc_button_color + "', '" + lc_button_color_darkend + "');});", True)

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hgc_banner_button_link.Attributes.Add("href", Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                    Else
                        hgc_banner_button_link.Attributes.Add("href", dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                    End If

                    If dr_banner.Item("external_link") Then
                        hgc_banner_button_link.Attributes.Add("target", "_blank")
                    End If
                    hgc_banner_button_link.InnerText = lc_banner_button

                    hgc_banner_button.Controls.Add(hgc_banner_button_link)
                    hgc_hover_block_inner.Controls.Add(hgc_banner_button)
                    bool_has_content = True
                End If

                If Not bool_has_content Then
                    hgc_hover_block_inner.Visible = False
                End If

                hgc_hover_block.Controls.Add(hgc_hover_block_inner)

                If dr_banner.Item("add_hover_image") Then
                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                        Dim img_hover_image As New Utilize.Web.UI.WebControls.image
                        With img_hover_image
                            .ImageUrl = "~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)
                        End With
                        hgc_hover_block.Controls.Add(img_hover_image)
                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        Dim hgc_background_hover_image As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_background_hover_image.Attributes.Add("class", "background_hover_image_div")
                        hgc_background_hover_image.Attributes.Add("style", "background: url('" + Me.ResolveCustomUrl("~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)) + "'); background-position: center; background-repeat: none;")
                        hgc_hover_block.Controls.Add(hgc_background_hover_image)
                    End If
                End If

                'set de link op het hover block omdat de link zelf achter het hover blok terecht komt, als er een button aanwezig is komt de link hier al op te staan.
                If Not dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" And lc_banner_button = "" Then
                    Dim hl_link As String

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hl_link = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                    Else
                        hl_link = dr_banner.Item("location_url_" + Me.global_cms.Language)
                    End If

                    If Not dr_banner.Item("external_link") Then
                        hgc_hover_block.Attributes.Add("onClick", "window.location.href = '" + hl_link + "'")
                    Else
                        hgc_hover_block.Attributes.Add("onClick", "window.open('" + hl_link + "', '_blank');")
                    End If
                    hgc_hover_block.Attributes.Add("style", "cursor: pointer;")
                End If


                hgc_wrapper_div.Controls.Add(hgc_hover_block)
            End If

            If dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" Then
                ' De banner is geen hyperlink
                ' Maak een nieuwe placeholoder aan
                Dim ph_placeholder As New Utilize.Web.UI.WebControls.placeholder

                ' voeg de placeholder aan de div
                hgc_wrapper_div.Controls.Add(ph_placeholder)

                ' Voeg de banner toe aan de placeholder
                If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                    ph_placeholder.Controls.Add(img_banner_image)
                End If

                ph_placeholder.Controls.Add(lt_banner_desc)
            Else
                ' Maak een nieuwe hyperlink aan
                Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink

                If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                    hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                Else
                    hl_hyperlink.NavigateUrl = dr_banner.Item("location_url_" + Me.global_cms.Language)
                End If

                If dr_banner.Item("external_link") Then
                    hl_hyperlink.Attributes.Add("target", "_blank")
                End If

                ' voeg de hyperlink aan de div

                ' Voeg de banner toe aan de hyperlink
                If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                    hl_hyperlink.Controls.Add(img_banner_image)
                End If
                hgc_wrapper_div.Controls.Add(hl_hyperlink)
                hl_hyperlink.Controls.Add(lt_banner_desc)
            End If

            ' Ga naar de volgende regel

            lc_counter += 1

        Next
    End Sub

    Private Sub create_random_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object)
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As System.Data.DataTable = Nothing

        If ll_resume Then
            ' Maak een nieuwe datatable aan
            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "TOP 1 *"
                .select_from = "ucm_banners"
                .select_where = "hdr_id = @hdr_id"
                .select_order = " NEWID() "
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            ' En laad de tabel
            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If

        ' Ga naar de eerste regel van de banner tabel


        Dim dt_banner_block As New System.Data.DataTable

        If ll_resume Then
            Dim qsc_banner_block As New Utilize.Data.QuerySelectClass
            With qsc_banner_block
                .select_fields = "banner_scaling"
                .select_from = "ucm_banner_blocks"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_banner_block.add_parameter("rec_id", dt_banner.Rows(0).Item("hdr_id"))

            dt_banner_block = qsc_banner_block.execute_query()
        End If

        ' Loop door de regels heen
        For Each dr_banner As System.Data.DataRow In dt_banner.Rows
            ' Maak een nieuwe afbeelding aan
            Dim img_banner_image As New Utilize.Web.UI.WebControls.image
            If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                img_banner_image.ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                img_banner_image.AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                img_banner_image.Attributes.Add("class", "img-responsive")
            ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                Me.pan_banner.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center")
                Me.pan_banner.CssClass += " banner_background"

                Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                If System.IO.File.Exists(Server.MapPath(path)) Then
                    Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "banner_block_resize" + banner_block_id, "$(document).ready( function () {banner_resizing('" + image1.Height.ToString() + "', '" + Me.pan_banner.ClientID + "');});", True)
                End If
            End If

            If dr_banner.Item("add_hover") Or dr_banner.Item("show_text") Or dr_banner.Item("add_hover_image") Then
                Me.pan_banner.CssClass += " hover_effect"
                Dim lc_banner_desc As String = dr_banner.Item("banner_desc_" + Me.global_cms.Language)
                Dim lc_banner_desc_color As String = dr_banner.Item("title_color")

                Dim lc_subbanner_desc As String = dr_banner.Item("banner_subdesc_" + Me.global_cms.Language)
                Dim lc_subbanner_desc_color As String = dr_banner.Item("subtitle_color")

                Dim lc_button_color As String = dr_banner.Item("button_color")
                Dim lc_banner_button As String = dr_banner.Item("banner_button_" + Me.global_cms.Language)

                Dim lc_opacity As String = dr_banner.Item("opacity")

                If lc_opacity = "" Then
                    lc_opacity = "0"
                End If

                Dim hgc_hover_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")

                If dr_banner.Item("show_text") Then
                    hgc_hover_block.Attributes.Add("class", "banner_block_hover show_always")
                Else
                    hgc_hover_block.Attributes.Add("class", "banner_block_hover")
                End If

                If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                    hgc_hover_block.Attributes.Add("class", hgc_hover_block.Attributes("class") + " random_banner")
                End If

                Dim hgc_hover_block_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                hgc_hover_block_inner.Attributes.Add("class", "banner_block_hover_inner")
                hgc_hover_block_inner.Attributes.Add("style", "border-color: " + lc_button_color)

                Dim bool_has_content As Boolean = False

                If Not lc_banner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                    Dim hgc_banner_title As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_title.Attributes.Add("class", "banner_title")
                    hgc_banner_title.InnerHtml = "<h2 style='color: " + lc_banner_desc_color + "!important;border-bottom-color: " + lc_banner_desc_color + "'>" + lc_banner_desc + "</h2>"

                    hgc_hover_block_inner.Controls.Add(hgc_banner_title)
                    bool_has_content = True
                End If

                If Not lc_subbanner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                    Dim hgc_banner_subtitle As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_subtitle.Attributes.Add("class", "banner_subtitle")
                    hgc_banner_subtitle.InnerHtml = "<p style='color: " + lc_subbanner_desc_color + "'>" + lc_subbanner_desc + "</p>"

                    hgc_hover_block_inner.Controls.Add(hgc_banner_subtitle)
                    bool_has_content = True
                End If

                If dr_banner.Item("use_hover_color") And dr_banner.Item("show_text") Then
                    hgc_hover_block.Attributes.Add("style", "transition: background 0.5s;")
                End If

                If dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And Not dr_banner.Item("add_hover_image") Then
                    Dim hover_block_id = "block_hover" + banner_block_id
                    hgc_hover_block.Attributes.Add("id", hover_block_id)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + banner_block_id, "$(document).ready( function () {set_hover_background('" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                ElseIf dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And dr_banner.Item("add_hover_image") Then
                    Dim hover_block_id = "block_hover" + banner_block_id

                    Dim hgc_color_opacity_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_color_opacity_block.Attributes.Add("id", hover_block_id)

                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                        hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div random_banner")
                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div no_scaling")
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + hover_block_id, "$(document).ready( function () {set_hover_background_with_image('" + Me.pan_banner.ClientID + "','" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                    hgc_banner_block.Controls.Add(hgc_color_opacity_block)
                End If

                If Not lc_banner_button = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                    Dim lc_button_color_darkend As String = darken(lc_button_color)
                    Dim hgc_banner_button As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_banner_button.Attributes.Add("class", "banner_button")
                    Dim hgc_banner_button_link As New System.Web.UI.HtmlControls.HtmlGenericControl("a")
                    hgc_banner_button_link.Attributes.Add("class", "banner_button_link btn-u btn-brd btn-brd-hover btn-u-light")
                    hgc_banner_button_link.Attributes.Add("style", "color: " + lc_button_color + "!important; border-color: " + lc_button_color + ";")
                    hgc_banner_button_link.Attributes.Add("id", "banner" + banner_block_id)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "button_hover_banner" + banner_block_id, "$(document).ready( function () {banner_block_button('" + banner_block_id + "', '" + lc_button_color + "', '" + lc_button_color_darkend + "');});", True)

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hgc_banner_button_link.Attributes.Add("href", Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                    Else
                        hgc_banner_button_link.Attributes.Add("href", dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                    End If

                    If dr_banner.Item("external_link") Then
                        hgc_banner_button_link.Attributes.Add("target", "_blank")
                    End If

                    hgc_banner_button_link.InnerText = lc_banner_button

                    hgc_banner_button.Controls.Add(hgc_banner_button_link)
                    hgc_hover_block_inner.Controls.Add(hgc_banner_button)
                    bool_has_content = True
                End If

                If Not bool_has_content Then
                    hgc_hover_block_inner.Visible = False
                End If

                hgc_hover_block.Controls.Add(hgc_hover_block_inner)

                If dr_banner.Item("add_hover_image") Then
                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                        Dim img_hover_image As New Utilize.Web.UI.WebControls.image
                        With img_hover_image
                            .ImageUrl = "~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)
                        End With
                        hgc_hover_block.Controls.Add(img_hover_image)
                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        Dim hgc_background_hover_image As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_background_hover_image.Attributes.Add("class", "background_hover_image_div")
                        hgc_background_hover_image.Attributes.Add("style", "background: url('" + Me.ResolveCustomUrl("~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)) + "'); background-position: center")
                        hgc_hover_block.Controls.Add(hgc_background_hover_image)
                    End If

                End If

                'set de link op het hover block omdat de link zelf achter het hover blok terecht komt, als er een button aanwezig is komt de link hier al op te staan.
                If Not dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" And lc_banner_button = "" Then
                    Dim hl_link As String

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hl_link = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                    Else
                        hl_link = dr_banner.Item("location_url_" + Me.global_cms.Language)
                    End If

                    If Not dr_banner.Item("external_link") Then
                        hgc_hover_block.Attributes.Add("onClick", "window.location.href = '" + hl_link + "'")
                    Else
                        hgc_hover_block.Attributes.Add("onClick", "window.open('" + hl_link + "', '_blank');")
                    End If
                    hgc_hover_block.Attributes.Add("style", "cursor: pointer;")
                End If

                Me.pnl_banners.Controls.Add(hgc_hover_block)
            End If

            If dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" Then
                ' De banner is geen hyperlink
                hgc_banner_block.Controls.Add(img_banner_image)
            Else
                ' Maak een nieuwe hyperlink aan
                Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink
                If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                    hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                Else
                    hl_hyperlink.NavigateUrl = dr_banner.Item("location_url_" + Me.global_cms.Language)
                End If

                If dr_banner.Item("external_link") Then
                    hl_hyperlink.Attributes.Add("target", "_blank")
                End If

                ' voeg de hyperlink aan de div
                hgc_banner_block.Controls.Add(hl_hyperlink)

                ' Voeg de banner toe aan de hyperlink
                hl_hyperlink.Controls.Add(img_banner_image)
            End If

        Next
    End Sub

    Private Sub create_slide_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object)
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As System.Data.DataTable = Nothing

        If ll_resume Then

            ' Maak een nieuwe datatable aan
            ' Initialiseren
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "*"
                .select_from = "UCM_BANNERS"
                .select_where = "hdr_id = @hdr_id"
                .select_order = "row_ord"
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            ' En laad de tabel
            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If

        ' Ga naar de eerste regel van de banner tabel

        ' Loop door de regels heen
        For Each dr_banner As System.Data.DataRow In dt_banner.Rows
            ' Maak een nieuwe afbeelding aan
            Dim img_banner_image As New Utilize.Web.UI.WebControls.image
            With img_banner_image
                .ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                .AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
            End With
            img_banner_image.Attributes.Add("title", dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", ""))

            If dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" Then
                ' De banner is geen hyperlink
                ' Maak een nieuwe placeholoder aan
                Dim ph_placeholder As New Utilize.Web.UI.WebControls.placeholder

                ' voeg de placeholder aan de div
                hgc_banner_block.Controls.Add(ph_placeholder)

                ' Voeg de banner toe aan de placeholder
                ph_placeholder.Controls.Add(img_banner_image)
            Else
                ' Maak een nieuwe hyperlink aan
                Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink
                If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                    hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                Else
                    hl_hyperlink.NavigateUrl = dr_banner.Item("location_url_" + Me.global_cms.Language)
                End If

                If dr_banner.Item("external_link") Then
                    hl_hyperlink.Attributes.Add("target", "_blank")
                End If

                ' voeg de hyperlink aan de div
                hgc_banner_block.Controls.Add(hl_hyperlink)

                ' Voeg de banner toe aan de hyperlink
                hl_hyperlink.Controls.Add(img_banner_image)
            End If
        Next
    End Sub

    Private Sub create_youtube_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object)
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As System.Data.DataTable = Nothing

        If ll_resume Then
            ' Maak een nieuwe datatable aan
            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "*"
                .select_from = "UCM_BANNERS"
                .select_where = "hdr_id = @hdr_id"
                .select_order = "row_ord"
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            ' En laad de tabel
            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If

        ' Loop door de regels heen
        For Each dr_banner As System.Data.DataRow In dt_banner.Rows

            Dim iframe As New HtmlGenericControl("iframe")

            iframe.Attributes.Add("class", "youtube-player " & banner_block_id)
            iframe.Attributes.Add("type", "text/html")
            iframe.Attributes.Add("float", "left")
            iframe.Attributes.Add("frameborder", "0")

            Dim lc_youtube_code As String = dr_banner.Item("location_url_" + Me.global_cms.Language)

            If lc_youtube_code.Contains("/") Then
                lc_youtube_code = lc_youtube_code.Substring(lc_youtube_code.LastIndexOf("/") + 1)
            End If

            iframe.Attributes.Add("src", "http://www.youtube.com/embed/" & lc_youtube_code)
            hgc_banner_block.Controls.Add(iframe)
            ' Ga naar de volgende regel
        Next


    End Sub

    Private Sub create_fading_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object)
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As System.Data.DataTable = Nothing


        If ll_resume Then
            ' Maak een nieuwe datatable aan
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "*"
                .select_from = "UCM_BANNERS"
                .select_where = "hdr_id = @hdr_id"
                .select_order = "row_ord"
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            ' Zorg dat alleen onderdelen van het hoogste niveau geladen worden.
            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If

        Dim dt_banner_block As New System.Data.DataTable

        If ll_resume Then
            Dim qsc_banner_block As New Utilize.Data.QuerySelectClass
            With qsc_banner_block
                .select_fields = "banner_scaling, banner_time_interval"
                .select_from = "ucm_banner_blocks"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_banner_block.add_parameter("rec_id", dt_banner.Rows(0).Item("hdr_id"))

            dt_banner_block = qsc_banner_block.execute_query()
        End If

        Dim li_count As Integer = 0

        If ll_resume Then
            li_count = li_count + 1
            ' Ga naar de eerste regel van de banner tabel


            'maak de carousel div aan
            Dim carousel As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            carousel.Attributes.Add("class", "carousel slide carousel-fade")
            carousel.Attributes.Add("data-ride", "carousel")
            carousel.Attributes.Add("id", "carousel" + banner_block_id)
            hgc_banner_block.controls.add(carousel)

            'maak de list aan voor de indicators
            Dim carousel_indicators As New System.Web.UI.HtmlControls.HtmlGenericControl("ol")
            carousel_indicators.Attributes.Add("class", "carousel-indicators")
            carousel.Controls.Add(carousel_indicators)

            'maakt de inner-div aan voor de images
            Dim carousel_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            carousel_inner.Attributes.Add("class", "carousel-inner")
            carousel.Controls.Add(carousel_inner)

            Dim ln_height As Decimal = 0

            Dim first As Boolean = True
            Dim data_slide_number As Integer = 0

            Dim inner_div_counter = 1
            ' Loop door de regels heen
            For Each dr_banner As System.Data.DataRow In dt_banner.Rows
                ' Maak een nieuwe afbeelding aan
                Dim indicator_li As New System.Web.UI.HtmlControls.HtmlGenericControl("li")
                indicator_li.Attributes.Add("data-target", "#carousel" + banner_block_id)
                indicator_li.Attributes.Add("data-slide-to", data_slide_number.ToString)
                data_slide_number += 1

                If first Then
                    indicator_li.Attributes.Add("class", "active")
                End If

                Dim inner_div As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                If first Then
                    inner_div.Attributes.Add("class", "active item")
                    first = False
                Else
                    inner_div.Attributes.Add("class", "item")
                End If

                Dim inner_div_id = "inner_div" + inner_div_counter.ToString() + banner_block_id
                inner_div.Attributes.Add("id", inner_div_id)

                If dr_banner.Item("add_hover") Or dr_banner.Item("show_text") Then
                    Me.pan_banner.CssClass += " hover_effect"

                    Dim lc_banner_desc As String = dr_banner.Item("banner_desc_" + Me.global_cms.Language)
                    Dim lc_banner_desc_color As String = dr_banner.Item("title_color")

                    Dim lc_subbanner_desc As String = dr_banner.Item("banner_subdesc_" + Me.global_cms.Language)
                    Dim lc_subbanner_desc_color As String = dr_banner.Item("subtitle_color")

                    Dim lc_button_color As String = dr_banner.Item("button_color")
                    Dim lc_banner_button As String = dr_banner.Item("banner_button_" + Me.global_cms.Language)

                    Dim lc_opacity As String = dr_banner.Item("opacity")

                    If lc_opacity = "" Then
                        lc_opacity = "0"
                    End If

                    Dim hgc_hover_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")

                    If dr_banner.Item("show_text") Then
                        hgc_hover_block.Attributes.Add("class", "banner_block_hover show_always")
                    Else
                        hgc_hover_block.Attributes.Add("class", "banner_block_hover")
                    End If

                    Dim hgc_hover_block_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_hover_block_inner.Attributes.Add("class", "banner_block_hover_inner")
                    hgc_hover_block_inner.Attributes.Add("style", "border-color: " + lc_button_color)

                    Dim bool_has_content As Boolean = False

                    If Not lc_banner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim hgc_banner_title As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_title.Attributes.Add("class", "banner_title")
                        hgc_banner_title.InnerHtml = "<h2 style='color: " + lc_banner_desc_color + "!important;border-bottom-color: " + lc_banner_desc_color + "'>" + lc_banner_desc + "</h2>"

                        hgc_hover_block_inner.Controls.Add(hgc_banner_title)
                        bool_has_content = True
                    End If

                    If Not lc_subbanner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim hgc_banner_subtitle As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_subtitle.Attributes.Add("class", "banner_subtitle")
                        hgc_banner_subtitle.InnerHtml = "<p style='color: " + lc_subbanner_desc_color + "'>" + lc_subbanner_desc + "</p>"

                        hgc_hover_block_inner.Controls.Add(hgc_banner_subtitle)
                        bool_has_content = True
                    End If

                    If dr_banner.Item("use_hover_color") And dr_banner.Item("show_text") Then
                        hgc_hover_block.Attributes.Add("style", "transition: background 0.5s;")
                    End If

                    If dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And Not dr_banner.Item("add_hover_image") Or dr_banner.Item("add_hover_image") Then
                        Dim hover_block_id = "block_hover" + banner_block_id + inner_div_counter.ToString()
                        hgc_hover_block.Attributes.Add("id", hover_block_id)
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + hover_block_id + inner_div_counter.ToString(), "$(document).ready( function () {set_hover_background('" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                    ElseIf dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") And dr_banner.Item("add_hover_image") Then
                        Dim hover_block_id = "block_hover" + banner_block_id + inner_div_counter.ToString()

                        Dim hgc_color_opacity_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_color_opacity_block.Attributes.Add("id", hover_block_id)

                        If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                            hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div")
                        ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                            hgc_color_opacity_block.Attributes.Add("class", "background_opacity_div no_scaling")
                        End If

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + hover_block_id + inner_div_counter.ToString(), "$(document).ready( function () {set_hover_background_with_image('" + inner_div_id + "','" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10) + "');});", True)
                        inner_div.Controls.Add(hgc_color_opacity_block)
                    End If

                    If Not lc_banner_button = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim lc_button_color_darkend As String = darken(lc_button_color)
                        Dim hgc_banner_button As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_button.Attributes.Add("class", "banner_button")
                        Dim hgc_banner_button_link As New System.Web.UI.HtmlControls.HtmlGenericControl("a")
                        hgc_banner_button_link.Attributes.Add("class", "banner_button_link btn-u btn-brd btn-brd-hover btn-u-light")
                        hgc_banner_button_link.Attributes.Add("style", "color: " + lc_button_color + "!important; border-color: " + lc_button_color + ";")
                        hgc_banner_button_link.Attributes.Add("id", "banner" + banner_block_id + inner_div_counter.ToString())

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "button_hover_banner" + banner_block_id + inner_div_counter.ToString(), "$(document).ready( function () {banner_block_button('" + banner_block_id + inner_div_counter.ToString() + "', '" + lc_button_color + "', '" + lc_button_color_darkend + "');});", True)

                        If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                            hgc_banner_button_link.Attributes.Add("href", Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                        Else
                            hgc_banner_button_link.Attributes.Add("href", dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                        End If

                        If dr_banner.Item("external_link") Then
                            hgc_banner_button_link.Attributes.Add("target", "_blank")
                        End If
                        hgc_banner_button_link.InnerText = lc_banner_button

                        hgc_banner_button.Controls.Add(hgc_banner_button_link)
                        hgc_hover_block_inner.Controls.Add(hgc_banner_button)
                        bool_has_content = True
                    End If

                    If Not bool_has_content Then
                        hgc_hover_block_inner.Visible = False
                    End If

                    hgc_hover_block.Controls.Add(hgc_hover_block_inner)

                    If dr_banner.Item("add_hover_image") Then
                        If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then
                            Dim img_hover_image As New Utilize.Web.UI.WebControls.image
                            With img_hover_image
                                .ImageUrl = "~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)
                            End With
                            hgc_hover_block.Controls.Add(img_hover_image)
                        ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                            Dim hgc_background_hover_image As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                            hgc_background_hover_image.Attributes.Add("class", "background_hover_image_div")
                            hgc_background_hover_image.Attributes.Add("style", "background: url('" + Me.ResolveCustomUrl("~/" + dr_banner.Item("hover_image_url_" + Me.global_cms.Language)) + "'); background-position: center")
                            hgc_hover_block.Controls.Add(hgc_background_hover_image)
                        End If

                    End If

                    'set de link op het hover block omdat de link zelf achter het hover blok terecht komt, als er een button aanwezig is komt de link hier al op te staan.
                    If Not dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" And lc_banner_button = "" Then
                        Dim hl_link As String

                        If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                            hl_link = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                        Else
                            hl_link = dr_banner.Item("location_url_" + Me.global_cms.Language)
                        End If

                        If Not dr_banner.Item("external_link") Then
                            hgc_hover_block.Attributes.Add("onClick", "window.location.href = '" + hl_link + "'")
                        Else
                            hgc_hover_block.Attributes.Add("onClick", "window.open('" + hl_link + "', '_blank');")
                        End If
                        hgc_hover_block.Attributes.Add("style", "cursor: pointer;")
                    End If

                    inner_div.Controls.Add(hgc_hover_block)
                End If

                If dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" Then
                    Dim img_banner_image As New Utilize.Web.UI.WebControls.image
                    With img_banner_image
                        .ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                        .AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                    End With
                    img_banner_image.Attributes.Add("class", "img-responsive")
                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then

                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        img_banner_image.Attributes.Add("class", "hidden-all")
                        inner_div.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center")
                        inner_div.Attributes.Add("class", inner_div.Attributes.Item("class") + " banner_background")

                        Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                        If System.IO.File.Exists(Server.MapPath(path)) Then
                            Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "banner_block_resize" + inner_div_id, "$(document).ready( function () {banner_resizing('" + image1.Height.ToString() + "', '" + inner_div_id + "');});", True)
                        End If
                    End If

                    inner_div_counter += 1
                    inner_div.Controls.Add(img_banner_image)
                Else
                    Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                    Else
                        hl_hyperlink.NavigateUrl = dr_banner.Item("location_url_" + Me.global_cms.Language)
                    End If

                    If dr_banner.Item("external_link") Then
                        hl_hyperlink.Attributes.Add("target", "_blank")
                    End If

                    Dim img_banner_image As New Utilize.Web.UI.WebControls.image
                    With img_banner_image
                        .ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                        .AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                    End With
                    img_banner_image.Attributes.Add("class", "img-responsive")

                    If dt_banner_block.Rows(0).Item("banner_scaling") = 0 Then

                    ElseIf dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        img_banner_image.Attributes.Add("class", "hidden-all")
                        inner_div.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center")

                        Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                        If System.IO.File.Exists(Server.MapPath(path)) Then
                            Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)

                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "banner_block_resize" + inner_div_id, "$(document).ready( function () {banner_resizing('" + image1.Height.ToString() + "', '" + inner_div_id + "');});", True)
                        End If
                    End If

                    inner_div_counter += 1

                    hl_hyperlink.Controls.Add(img_banner_image)
                    inner_div.Controls.Add(hl_hyperlink)
                End If

                carousel_indicators.Controls.Add(indicator_li)
                carousel_inner.Controls.Add(inner_div)

                ' Ga naar de volgende regel
            Next
            'Setting Banners time interval from db settings
            tm_banner_interval = dt_banner_block.Rows(0).Item("banner_time_interval") * 1000

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "carousel_setup", "$(document).ready(function() { $('.carousel').carousel({ interval:" & tm_banner_interval & " }); });", True)
        End If
    End Sub

    Private Sub create_carousel_banner(ByVal banner_block_id As String, ByVal hgc_banner_block As Object, cssclass As String)
        Dim ll_resume As Boolean = True


        'Declareer een nieuwe datatable
        Dim qsc_banner As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner As New System.Data.DataTable
        'nieuwe code
        If ll_resume Then
            'Maak een nieuwe datatable aan
            qsc_banner = New Utilize.Data.QuerySelectClass
            With qsc_banner
                .select_fields = "*"
                .select_from = "UCM_BANNERS"
                .select_where = "hdr_id = @hdr_id"
                .select_order = "row_ord"
            End With
            qsc_banner.add_parameter("hdr_id", banner_block_id)

            dt_banner = qsc_banner.execute_query()

            ll_resume = dt_banner.Rows.Count > 0
        End If

        ' Declareer een nieuwe datatable
        Dim dt_banner_block As New System.Data.DataTable

        If ll_resume Then
            Dim qsc_banner_block As New Utilize.Data.QuerySelectClass
            With qsc_banner_block
                .select_fields = "banner_scaling, item_layout, banner_time_interval"
                .select_from = "ucm_banner_blocks"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_banner_block.add_parameter("rec_id", dt_banner.Rows(0).Item("hdr_id"))

            dt_banner_block = qsc_banner_block.execute_query()
        End If

        Dim li_count As Integer = 0

        If ll_resume Then
            li_count = li_count + 1

            'maak de carousel div aan
            Dim carousel As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            carousel.Attributes.Add("class", "carousel slide")
            carousel.Attributes.Add("data-ride", "carousel")
            carousel.Attributes.Add("id", "carousel" + banner_block_id)
            hgc_banner_block.controls.add(carousel)

            'maak de list aan voor de indicators
            Dim carousel_indicators As New System.Web.UI.HtmlControls.HtmlGenericControl("ol")
            carousel_indicators.Attributes.Add("class", "carousel-indicators")
            carousel.Controls.Add(carousel_indicators)

            'maakt de inner-div aan voor de images
            Dim carousel_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            carousel_inner.Attributes.Add("class", "carousel-inner")
            carousel.Controls.Add(carousel_inner)

            Dim ln_height As Decimal = 0

            Dim first As Boolean = True
            Dim data_slide_number As Integer = 0

            Dim inner_div_counter = 1

            ' Ophalen hoeveel banners er in een blok moeten komen
            Dim ln_item_layout As Integer = dt_banner_block.Rows(0).Item("item_layout")
            Dim ln_item_counter As Integer = 0

            ' Aanmaken van een blok div
            Dim col_div As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            col_div.Attributes.Add("class", "active item")

            ' Maak de indicatoren
            Dim indicator_li As New HtmlGenericControl("li")
            indicator_li.Attributes.Add("data-target", "#carousel" + banner_block_id)
            indicator_li.Attributes.Add("data-slide-to", data_slide_number.ToString)

            ' Loop door de banners heen
            For Each dr_banner As System.Data.DataRow In dt_banner.Rows

                ln_item_counter += ln_item_layout
                ' Voeg class active toe aan de eerste indicator
                If first Then
                    first = False
                    indicator_li.Attributes.Add("class", "active")
                End If

                ' Maak een nieuwe inner div waar de banners in komen
                Dim inner_div As New System.Web.UI.HtmlControls.HtmlGenericControl("div")

                If dr_banner.Item("add_hover") Or dr_banner.Item("show_text") Or dr_banner.Item("add_hover_image") Then
                    Dim lc_banner_desc As String = dr_banner.Item("banner_desc_" + Me.global_cms.Language)
                    Dim lc_banner_desc_color As String = dr_banner.Item("title_color")

                    Dim lc_subbanner_desc As String = dr_banner.Item("banner_subdesc_" + Me.global_cms.Language)
                    Dim lc_subbanner_desc_color As String = dr_banner.Item("subtitle_color")

                    Dim lc_button_color As String = dr_banner.Item("button_color")
                    Dim lc_banner_button As String = dr_banner.Item("banner_button_" + Me.global_cms.Language)

                    Dim hgc_hover_block As New System.Web.UI.HtmlControls.HtmlGenericControl("div")

                    Dim lc_opacity As String = dr_banner.Item("opacity")
                    'Dim lc_opacity As String = dr_banner.Item("opacity")

                    If lc_opacity = "" Then
                        lc_opacity = 0
                    End If

                    If dr_banner.Item("show_text") Then
                        'If dr_banner.Item("show_text") Then
                        If Not cssclass = "" Then
                            hgc_hover_block.Attributes.Add("class", "banner_block_hover show_always " + cssclass)
                        Else
                            hgc_hover_block.Attributes.Add("class", "banner_block_hover show_always")
                        End If
                    Else
                        If Not cssclass = "" Then
                            hgc_hover_block.Attributes.Add("class", "banner_block_hover " + cssclass)
                        Else
                            hgc_hover_block.Attributes.Add("class", "banner_block_hover")
                        End If
                    End If

                    Dim hgc_hover_block_inner As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    hgc_hover_block_inner.Attributes.Add("class", "banner_block_hover_inner")
                    hgc_hover_block_inner.Attributes.Add("style", "border-color:  " + lc_button_color)

                    Dim bool_has_content As Boolean = False

                    If Not lc_banner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim hgc_banner_title As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_title.Attributes.Add("class", "banner_title")
                        hgc_banner_title.InnerHtml = "<h2 style='color: " + lc_banner_desc_color + "!important;border-bottom-color: " + lc_banner_desc_color + "'>" + lc_banner_desc + "</h2>"
                        hgc_hover_block_inner.Controls.Add(hgc_banner_title)
                        bool_has_content = True
                    End If

                    If Not lc_subbanner_desc = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim hgc_banner_subtitle As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_subtitle.Attributes.Add("class", "banner_subtitle")
                        hgc_banner_subtitle.InnerHtml = "<p style='color: " + lc_subbanner_desc_color + "'>" + lc_subbanner_desc + "</p>"

                        hgc_hover_block_inner.Controls.Add(hgc_banner_subtitle)
                        bool_has_content = True
                    End If

                    If dr_banner.Item("use_hover_color") And dr_banner.Item("show_text") Then
                        hgc_hover_block.Attributes.Add("style", "transition: background 0.5s;")
                    End If

                    If dr_banner.Item("use_hover_color") And dr_banner.Item("add_hover") Then
                        Dim hover_block_id = "block_hover" + inner_div_counter.ToString() + banner_block_id
                        hgc_hover_block.Attributes.Add("id", hover_block_id)

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_background" + inner_div_counter.ToString() + banner_block_id, "$(document).ready( function () {set_hover_background('" + hover_block_id + "','" + color_opacity(dr_banner.Item("hover_color"), CType(lc_opacity, Decimal) / 10).ToString() + "');});", True)
                    End If

                    If Not lc_banner_button = "" And (dr_banner.Item("add_hover") Or dr_banner.Item("show_text")) Then
                        Dim lc_button_color_darkend As String = darken(lc_button_color)

                        Dim hgc_banner_button As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        hgc_banner_button.Attributes.Add("class", "banner_button")

                        Dim hgc_banner_button_link As New System.Web.UI.HtmlControls.HtmlGenericControl("a")
                        hgc_banner_button_link.Attributes.Add("class", "banner_button_link btn-u btn-brd btn-brd-hover btn-u-light")
                        hgc_banner_button_link.Attributes.Add("style", "color: " + lc_button_color + "!important; border-color: " + lc_button_color + ";")
                        hgc_banner_button_link.Attributes.Add("id", "banner" + inner_div_counter.ToString() + banner_block_id)

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "button_hover_banner" + inner_div_counter.ToString() + banner_block_id, "$(document).ready( function () {banner_block_button('" + inner_div_counter.ToString() + banner_block_id + "', '" + lc_button_color + "', '" + lc_button_color_darkend + "');});", True)

                        If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                            hgc_banner_button_link.Attributes.Add("href", Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                        Else
                            hgc_banner_button_link.Attributes.Add("href", dr_banner.Item("location_url_" + Me.global_cms.Language).ToString())
                        End If

                        If dr_banner.Item("external_link") Then
                            hgc_banner_button_link.Attributes.Add("target", "_blank")
                        End If

                        hgc_banner_button_link.InnerText = lc_banner_button

                        hgc_banner_button.Controls.Add(hgc_banner_button_link)
                        hgc_hover_block_inner.Controls.Add(hgc_banner_button)
                        bool_has_content = True
                    End If

                    If Not bool_has_content Then
                        hgc_hover_block_inner.Visible = False
                    End If

                    'set de link op het hover block omdat de link zelf achter het hover blok terecht komt, als er een button aanwezig is komt de link hier al op te staan.
                    If Not dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" And lc_banner_button = "" Then
                        Dim hl_link As String

                        If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                            'If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                            hl_link = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                            'hl_link = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)
                        Else
                            hl_link = dr_banner.Item("location_url_" + Me.global_cms.Language)
                            'hl_link = dr_banner.Item("location_url_" + Me.global_cms.Language)
                        End If

                        If Not dr_banner.Item("external_link") Then
                            'If Not dr_banner.Item("external_link") Then
                            hgc_hover_block.Attributes.Add("onClick", "window.location.href = '" + hl_link + "'")
                        Else
                            hgc_hover_block.Attributes.Add("onClick", "window.open('" + hl_link + "', '_blank');")
                        End If

                        hgc_hover_block.Attributes.Add("style", "cursor: pointer;")
                    End If

                    hgc_hover_block.Controls.Add(hgc_hover_block_inner)

                    inner_div.Controls.Add(hgc_hover_block)
                End If

                If dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Trim() = "" Then
                    Dim img_banner_image As New Utilize.Web.UI.WebControls.image
                    With img_banner_image
                        .ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                        .AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                    End With
                    img_banner_image.Attributes.Add("class", "img-responsive")

                    If dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        img_banner_image.Attributes.Add("class", "hidden-all")

                        Dim inner_div_id = "inner_div" + inner_div_counter.ToString() + banner_block_id
                        inner_div.Attributes.Add("id", inner_div_id)
                        inner_div.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center")

                        inner_div.Attributes.Add("class", inner_div.Attributes.Item("class") + " banner_background")

                        Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                        If System.IO.File.Exists(Server.MapPath(path)) Then
                            Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)

                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "banner_block_resize" + inner_div_id, "$(document).ready( function () {banner_resizing('" + image1.Height.ToString() + "', '" + inner_div_id + "');});", True)
                        End If
                    End If

                    inner_div_counter += 1
                    inner_div.Controls.Add(img_banner_image)
                Else
                    Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink

                    If dr_banner.Item("location_url_" + Me.global_cms.Language).startswith("/") Then
                        hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/") + dr_banner.Item("location_url_" + Me.global_cms.Language).ToString().Substring(1)

                    Else
                        hl_hyperlink.NavigateUrl = dr_banner.Item("location_url_" + Me.global_cms.Language)
                        '
                    End If

                    If dr_banner.Item("external_link") Then
                        hl_hyperlink.Attributes.Add("target", "_blank")
                    End If

                    Dim img_banner_image As New Utilize.Web.UI.WebControls.image
                    With img_banner_image
                        .ImageUrl = "~/" + dr_banner.Item("image_url_" + Me.global_cms.Language)
                        .AlternateText = dr_banner.Item("banner_desc_" + Me.global_cms.Language).replace("'", "").replace("""", "")
                    End With
                    img_banner_image.Attributes.Add("class", "img-responsive")

                    If dt_banner_block.Rows(0).Item("banner_scaling") = 1 Then
                        img_banner_image.Attributes.Add("class", "hidden-all")

                        Dim inner_div_id = "inner_div" + inner_div_counter.ToString() + banner_block_id
                        inner_div.Attributes.Add("id", inner_div_id)
                        inner_div.Attributes.Add("style", "background-image: url('" + "/" + dr_banner.Item("image_url_" + Me.global_cms.Language) + "'); background-position: center")

                        Dim path As String = Me.ResolveCustomUrl("~/" + dr_banner.Item("image_url_" + Me.global_cms.Language))

                        If System.IO.File.Exists(Server.MapPath(path)) Then
                            Dim image1 As System.Drawing.Bitmap = CType(System.Drawing.Image.FromFile(Server.MapPath(path)), System.Drawing.Bitmap)

                        End If
                    End If

                    inner_div_counter += 1

                    hl_hyperlink.Controls.Add(img_banner_image)
                    inner_div.Controls.Add(hl_hyperlink)
                End If

                Dim outer_div As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                outer_div.Attributes.Add("class", "carousel-item col-md-" + ln_item_layout.ToString() + " no-padding")

                If dr_banner.Item("add_hover") Then
                    outer_div.Attributes.Add("class", outer_div.Attributes("class") + " hover_effect")
                End If

                outer_div.Controls.Add(inner_div)
                col_div.Controls.Add(outer_div)

                carousel_indicators.Controls.Add(indicator_li)

                ' Bij de laatste banner in dit blok of bij de laatste banner
                If ln_item_counter = 12 Or dt_banner_block.Rows(dt_banner_block.Rows.Count - 1).Equals(dr_banner) Then
                    ' Hoog slide teller op
                    data_slide_number += 1
                    ' Voeg een nieuwe indicator toe
                    indicator_li = New System.Web.UI.HtmlControls.HtmlGenericControl("li")
                    indicator_li.Attributes.Add("data-target", "#carousel" + banner_block_id)
                    indicator_li.Attributes.Add("data-slide-to", data_slide_number.ToString)

                    ' Voeg het blok toe aan de carousel
                    carousel_inner.Controls.Add(col_div)
                    ' Aanmaken van een nieuw block
                    col_div = New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    col_div.Attributes.Add("class", "item")
                    ' Reset de item teller
                    ln_item_counter = 0
                End If
            Next

            ' Toevoegen van de linker control
            Dim hl_left As New Utilize.Web.UI.WebControls.hyperlink
            hl_left.Attributes.Add("class", "left carousel-control")
            hl_left.Attributes.Add("href", "#carousel" + banner_block_id)
            hl_left.Attributes.Add("role", "button")
            hl_left.Attributes.Add("data-slide", "prev")

            Dim span_left As New HtmlGenericControl("span")
            span_left.Attributes.Add("class", "glyphicon glyphicon-chevron-left")
            span_left.Attributes.Add("aria-hidden", "true")
            hl_left.Controls.Add(span_left)
            carousel.Controls.Add(hl_left)

            ' Toevoegen van de rechter control
            Dim hl_right As New Utilize.Web.UI.WebControls.hyperlink
            hl_right.Attributes.Add("class", "right carousel-control")
            hl_right.Attributes.Add("href", "#carousel" + banner_block_id)
            hl_right.Attributes.Add("role", "button")
            hl_right.Attributes.Add("data-slide", "next")

            Dim span_right As New HtmlGenericControl("span")
            span_right.Attributes.Add("class", "glyphicon glyphicon-chevron-right")
            span_right.Attributes.Add("aria-hidden", "true")
            hl_right.Controls.Add(span_right)
            carousel.Controls.Add(hl_right)
            'Setting Banners time interval from db settings
            tm_banner_interval = dt_banner_block.Rows(0).Item("banner_time_interval") * 1000

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "carousel_setup", "$(document).ready(function() { $('.carousel').carousel({ interval:" & tm_banner_interval & " }); });", True)
        End If
    End Sub
#End Region

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True
        Me.set_javascript("uc_banner_block.js", Me)
        ' Declareer een nieuwe datatable
        Dim qsc_banner_block As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_banner_block As New System.Data.DataTable
        If ll_resume Then
            'Controleer of de module banners beschikbaar is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_bnr")
        End If

        If ll_resume Then
            ' Maak een nieuwe datatable aan
            qsc_banner_block = New Utilize.Data.QuerySelectClass
            With qsc_banner_block
                .select_fields = "*"
                .select_from = "UCM_BANNER_BLOCKS"
                .select_order = "row_ord"
                .select_where = "rec_id = @rec_id"
            End With
            qsc_banner_block.add_parameter("rec_id", Me.block_id)

            dt_banner_block = qsc_banner_block.execute_query()
            ll_resume = dt_banner_block.Rows.Count > 0
        End If

        If ll_resume Then
            ' If the item layout is 0 then set a default of 12
            If dt_banner_block.Rows(0).Item("item_layout") = 0 Then
                ' Voeg item_layout toe aan de banner block.
                Dim qsc_add_item_layout As New Utilize.Data.QueryCustomSelectClass
                With qsc_add_item_layout
                    .QueryString = "UPDATE UCM_BANNER_BLOCKS SET item_layout = 12 WHERE rec_id = @rec_id"
                End With
                qsc_add_item_layout.add_parameter("rec_id", Me.block_id)

                qsc_add_item_layout.execute_query(Me.global_ws.user_information.user_id)
            End If
        End If

        If ll_resume Then
            ' Voor de verschillende type banners wordt een aparte methode uitgevoerd.;
            Dim lc_banner_type As String = dt_banner_block.Rows(0).Item("banner_type")
            Select Case lc_banner_type
                Case "STATIC"
                    ' Maak een statische banner aan
                    Me.create_static_banner(Me.block_id, Me.pnl_banners)
                Case "SLIDING"
                    ' Maak een shuivende banner aan
                    Me.create_slide_banner(Me.block_id, Me.pnl_banners)
                Case "RANDOM"
                    ' Maak een willekeurige banner aan.
                    Me.create_random_banner(Me.block_id, Me.pnl_banners)
                Case "YOUTUBE"
                    ' Maak een willekeurige banner aan.
                    Me.create_youtube_banner(Me.block_id, Me.pnl_banners)
                Case "FADING"
                    Me.create_fading_banner(Me.block_id, Me.pnl_banners)
                Case "CAROUSEL"
                    Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

                    'If advanced styling has been set, add the css classes according to the CMS settings for the current call back block
                    If dr_data_row.Item("car_ban_advanced") Then
                        Dim uc_banner_block As Utilize.Web.UI.WebControls.placeholder = Me.pnl_banners
                        Me.pan_banner.CssClass += " advanced " + Me.block_table + Me.block_rec_id

                        If dr_data_row.Item("car_ban_hide_arrows") Then
                            Me.pan_banner.CssClass += " hide-arrow-controls"
                        End If

                        Select Case dr_data_row.Item("car_ban_text_align")
                            Case "1"
                                Me.create_carousel_banner(Me.block_id, Me.pnl_banners, "align-left-center")
                            Case "2"
                                Me.create_carousel_banner(Me.block_id, Me.pnl_banners, "align-center-center")
                            Case "3"
                                Me.create_carousel_banner(Me.block_id, Me.pnl_banners, "align-right-center")
                            Case Else
                                Me.create_carousel_banner(Me.block_id, Me.pnl_banners, "")
                        End Select

                        Dim advancedStylingLocation As String = "/Styles/AdvancedBanners/" + Me.block_table + Me.block_rec_id + ".css"
                        If Not System.IO.File.Exists(Server.MapPath(advancedStylingLocation)) Then
                            Dim styleBuilder = StyleBuilderFactory.GetStyleBuilder("carousel", Me.block_rec_id, Me.block_table)
                            styleBuilder.GenerateCssFile(advancedStylingLocation)
                        End If

                        Me.set_style_sheet(advancedStylingLocation, Me)
                    Else
                        Me.create_carousel_banner(Me.block_id, Me.pnl_banners, "")
                    End If
                Case Else
                    Exit Select
            End Select

            Dim lc_skin_id As String = dt_banner_block.Rows(0).Item("skin_id")
            If Not lc_skin_id = "" Then
                Me.skin = dt_banner_block.Rows(0).Item("skin_id")
            End If

            Me.set_style_sheet("uc_banner_block.css", Me)

            If Me.move_row Then
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "MoveRow" + Me.ClientID, "$(document).ready(function() {$('#" + Me.pan_banner.ClientID + "').parent().insertBefore('#pnl_wrapper');});", True)
            End If
        End If

        If Not Me.global_ws.user_information.user_logged_on Then
            If Not String.IsNullOrEmpty(Me.block_table) And Not String.IsNullOrEmpty(Me.block_rec_id) Then
                Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)
                If dr_data_row.Item("show_when") = "2" Then
                    Me.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.float_right = True Then
            pan_banner.CssClass += " pull-right"
        End If

        If Me.IsPostBack() Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "startRevolutionSlider", "jQuery(document).ready(function () {RevolutionSlider.initRSfullWidth();});", True)
        End If

    End Sub
End Class
