﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_panorama_banner.ascx.vb" Inherits="uc_panorama_banner" %>
<utilize:panel ID="pan_banner" CssClass="uc_panorama_banner" runat="server">
    <div class="slider-inner uc_panorama_banner">
        <div id="da-slider" class="da-slider" style="background: transparent url('<%= ResolveUrl("~/" + Utilize.Data.DataProcedures.GetValue("UCM_PAN_BANNER_BLOCK", "back_image", Me.block_id))%>') repeat 0% 0%;">
            <utilize:repeater runat="server" ID="rpt_slides">
                <ItemTemplate>
                    <utilize:panel runat="server" ID="pnl_slide" CssClass="da-slide">
                        <h2>
                            <utilize:placeholder runat="server" ID="ph_header_1">
                                <i><utilize:literal runat="server" ID="lt_header_1" Text='<%# DataBinder.Eval(Container.DataItem, "header_1") %>'></utilize:literal></i>
                            </utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_header_2">
                                <br>
                                <i><utilize:literal runat="server" ID="lt_header_2" Text='<%# DataBinder.Eval(Container.DataItem, "header_2")%>'></utilize:literal></i>
                            </utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_header_3">
                                <br>
                                <i><utilize:literal runat="server" ID="lt_header_3" Text='<%# DataBinder.Eval(Container.DataItem, "header_3")%>'></utilize:literal></i>
                            </utilize:placeholder>
                        </h2>
                        <p>
                            <utilize:placeholder runat="server" ID="ph_text_1">
                                <i><utilize:literal runat="server" ID="lt_text_1" Text='<%# DataBinder.Eval(Container.DataItem, "text_1")%>'></utilize:literal></i>
                            </utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_text_2">
                                <br>
                                <i><utilize:literal runat="server" ID="lt_text_2" Text='<%# DataBinder.Eval(Container.DataItem, "text_2")%>'></utilize:literal></i>
                            </utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_text_3">
                                <br>
                                <i><utilize:literal runat="server" ID="lt_text_3" Text='<%# DataBinder.Eval(Container.DataItem, "text_3")%>'></utilize:literal></i>
                            </utilize:placeholder>
                        </p>
                        <div class="da-img">
                            <utilize:image runat="server" ImageUrl='<%# ResolveUrl("~/" + DataBinder.Eval(Container.DataItem, "img_url"))%>' AlternateText="" CssClass="img-responsive" />
                        </div>
                    </utilize:panel>
                </ItemTemplate>
            </utilize:repeater>

            <div class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>        
            </div>

        </div>
    </div>
</utilize:panel>