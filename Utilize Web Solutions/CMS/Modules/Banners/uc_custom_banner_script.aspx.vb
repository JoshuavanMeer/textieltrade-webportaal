﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Request.Params("ScriptCode") Is Nothing
        End If

        If ll_resume Then
            Dim lc_script As String = Utilize.Data.DataProcedures.GetValue("ucm_cus_banner_block", "javascript_content", Request.Params("ScriptCode"))

            Response.Clear()
            Response.ContentType = "text/javascript"
            Response.Write(lc_script)
            Response.End()
        End If
    End Sub
End Class
