﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True
        Dim rec_id = Request.Params("rec_id")
        Dim block_table = Request.Params("block_table")
        Dim clientid = Request.Params("clientid")

        If ll_resume Then
            ll_resume = Not rec_id Is Nothing And Not block_table Is Nothing
        End If

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim dr_data_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(block_table, rec_id)
            Dim lc_style_string As New StringBuilder

            lc_style_string.Append("#" + clientid + ".uc_banner_block.advanced .banner_block_hover_inner").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-color: " + get_rgba(dr_data_row.Item("car_ban_backgr_col").ToString, Convert.ToDouble(dr_data_row.Item("car_ban_col_opacity"))) + ";").AppendLine()
            lc_style_string.Append("}").AppendLine()

            If Not dr_data_row.Item("car_ban_indic_shape") = 1 Then
                lc_style_string.Append("#" + clientid + ".uc_banner_block.advanced .carousel-indicators li").AppendLine()
                lc_style_string.Append("{").AppendLine()
                Select Case dr_data_row.Item("car_ban_indic_shape")
                    Case 2
                        lc_style_string.Append("border-radius: 0 !important;").AppendLine()
                    Case 3
                        lc_style_string.Append("border-radius: 0 !important;").AppendLine()
                        lc_style_string.Append("width: 35px;").AppendLine()
                        lc_style_string.Append("height: 10px;").AppendLine()
                End Select
                lc_style_string.Append("}").AppendLine()
            End If

            lc_style_string.Append("#" + clientid + ".uc_banner_block.advanced h2").AppendLine()
            lc_style_string.Append("{").AppendLine()

            If dr_data_row.Item("car_ban_title_transf") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If

            lc_style_string.Append("font-size: " + dr_data_row.Item("car_ban_title_font").ToString + "px;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.Append("#" + clientid + ".uc_banner_block.advanced p").AppendLine()
            lc_style_string.Append("{").AppendLine()

            If dr_data_row.Item("car_ban_sub_transf") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If

            lc_style_string.Append("font-size: " + dr_data_row.Item("car_ban_sub_font").ToString + "px;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.Append("#" + clientid + ".uc_banner_block.advanced .banner_button_link").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-color: " + dr_data_row.Item("car_ban_btn_backgr").ToString + ";").AppendLine()
            lc_style_string.Append("border-width: " + dr_data_row.Item("car_ban_btn_border").ToString + "px;").AppendLine()
            lc_style_string.Append("color: " + dr_data_row.Item("car_ban_btn_color").ToString + "!important;").AppendLine()
            lc_style_string.Append("font-size: " + dr_data_row.Item("car_ban_btn_font").ToString + "px;").AppendLine()
            lc_style_string.Append("height: auto;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.ToString()

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_style_string)
            Response.End()
        End If
    End Sub

    Private Function get_rgba(Hex As String, opacity As Double) As String
        Hex = Replace(Hex, "#", "")
        Dim red_org As String = "&H" & Hex.Substring(0, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(2, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(4, 2)
        Dim blue As Integer = CInt(blue_org)

        opacity = opacity / 100

        Dim result = "rgba(" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + "," + opacity.ToString("F2", System.Globalization.CultureInfo.InvariantCulture) + ")"
        Return result
    End Function
End Class
