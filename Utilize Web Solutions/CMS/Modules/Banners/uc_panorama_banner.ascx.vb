﻿
Partial Class uc_panorama_banner
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_visible As Boolean = True
        Dim dt_banners As System.Data.DataTable = Nothing

        If ll_visible Then
            Me.set_style_sheet("uc_panorama_banner.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/parallax-slider/css/parallax-slider.css", Me)
            Me.set_javascript("~/_THMAssets/plugins/parallax-slider/js/modernizr.js", Me)
            Me.set_javascript("~/_THMAssets/plugins/parallax-slider/js/jquery.cslider.js", Me)
            Me.set_javascript("~/_THMAssets/js/plugins/parallax-slider.js", Me)

            Me.set_javascript("uc_panorama_banner.js", Me)
        End If

        If ll_visible Then
            ' aanpassen
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "rec_id, header_1_" + Me.global_ws.Language + " as header_1, header_2_" + Me.global_ws.Language + " as header_2, header_3_" + Me.global_ws.Language + " as header_3, text_1_" + Me.global_ws.Language + " as text_1, text_2_" + Me.global_ws.Language + " as text_2, text_3_" + Me.global_ws.Language + " as text_3, img_url"
            qsc_select.select_from = "ucm_pan_banners"
            qsc_select.select_where = "hdr_id = @hdr_id"
            qsc_select.select_order = "row_ord asc"
            qsc_select.add_parameter("hdr_id", Me.block_id)

            dt_banners = qsc_select.execute_query()

            ll_visible = dt_banners.Rows.Count > 0
        End If

        If ll_visible Then
            Me.rpt_slides.DataSource = dt_banners
            Me.rpt_slides.DataBind()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "jQuery(document).ready(function() {ParallaxSlider.initParallaxSlider();});", True)
        End If

        If Me.IsPostBack() Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "move_banner", "move_banner();", True)
        End If

        If Me.move_row Then
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "MoveRow + Me.ClientID", "$(document).ready(function() {$('#" + Me.pan_banner.ClientID + "').parent().insertBefore('#pnl_wrapper');});", True)
        End If
    End Sub

    Protected Sub rpt_slides_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_slides.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim header_1 As String = e.Item.DataItem("header_1")
            Dim header_2 As String = e.Item.DataItem("header_2")
            Dim header_3 As String = e.Item.DataItem("header_3")

            Dim ph_header_1 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_header_1")
            Dim ph_header_2 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_header_2")
            Dim ph_header_3 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_header_3")

            Dim text_1 As String = e.Item.DataItem("text_1")
            Dim text_2 As String = e.Item.DataItem("text_2")
            Dim text_3 As String = e.Item.DataItem("text_3")

            Dim ph_text_1 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_text_1")
            Dim ph_text_2 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_text_2")
            Dim ph_text_3 As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_text_3")

            ph_header_1.Visible = (header_1.ToString <> "")
            ph_header_2.Visible = (header_2.ToString <> "")
            ph_header_3.Visible = (header_3.ToString <> "")
            ph_text_1.Visible = (text_1.ToString <> "")
            ph_text_2.Visible = (text_2.ToString <> "")
            ph_text_3.Visible = (text_3.ToString <> "")
        End If
    End Sub
End Class
