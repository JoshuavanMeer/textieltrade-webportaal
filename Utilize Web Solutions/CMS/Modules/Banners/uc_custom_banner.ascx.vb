﻿
Partial Class CMS_Modules_Banners_uc_custom_banner
    Inherits Utilize.Web.Solutions.CMS.cms_user_control

    Private html_content As String = ""
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        'Variabelen die in beide if statements gebruikt worden
        Dim dt_return_banners As New System.Data.DataTable
        Dim dt_return_banners_detail As New System.Data.DataTable

        If ll_resume Then
            'Haal gegevens op van het banner blok
            Dim qsc_return_banners As New Utilize.Data.QuerySelectClass
            qsc_return_banners.select_fields = "*"
            qsc_return_banners.select_from = "ucm_cus_banner_block"
            qsc_return_banners.select_order = "rec_id"
            qsc_return_banners.select_where = "rec_id = @rec_id"
            qsc_return_banners.add_parameter("rec_id", Me.block_id)
            dt_return_banners = qsc_return_banners.execute_query()

            'Haal gegevens op van de banners die bij het blok horen
            Dim qsc_return_banners_detail As New Utilize.Data.QuerySelectClass
            qsc_return_banners_detail.select_fields = "*"
            qsc_return_banners_detail.select_from = "ucm_cus_banners"
            qsc_return_banners_detail.select_where = "hdr_id = @hdr_id"
            qsc_return_banners_detail.add_parameter("hdr_id", Me.block_id)

            qsc_return_banners_detail.select_order = "row_ord"

            dt_return_banners_detail = qsc_return_banners_detail.execute_query()

            'Controleer dat er een blok is en dat dat blok banners heeft
            If dt_return_banners.Rows.Count > 0 And dt_return_banners_detail.Rows.Count > 0 Then
                ll_resume = True
            End If
        End If

        If ll_resume Then
            html_content = dt_return_banners.Rows(0).Item("html_content")
        End If

        If ll_resume Then
            'Creer een div voor de html
            'Laad voor elke banner de html in
            For Each row As System.Data.DataRow In dt_return_banners_detail.Rows
                Dim id As String = Me.block_id + row.Item("rec_id").ToString()
                Dim image As String = row.Item("background_img")
                Dim imageURL As String = ""
                Dim substring As String = ""

                'maak een substring aan voor class name
                If Not image = String.Empty Then
                    'imageURL = ResolveUrl("~/" + image)
                    'substring = imageURL.Substring(imageURL.LastIndexOf("/") + 1, imageURL.LastIndexOf(".") - imageURL.LastIndexOf("/") - 1)
                End If

                Dim lt_html As New literal
                lt_html.Text = row.Item("HTML_CONTENT_" + Me.global_ws.Language)
                Me.ph_custom_banner.Controls.Add(lt_html)
            Next

            'Laad de css en javascript code in
            Me.set_style_sheet("uc_custom_banner_css.aspx?CssCode=" + dt_return_banners.Rows(0).Item("rec_id"), Me)
            Me.set_javascript("uc_custom_banner_script.aspx?ScriptCode=" + dt_return_banners.Rows(0).Item("rec_id"), Me)

            If Me.move_row Then
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "MoveRow" + Me.ClientID, "$(document).ready(function() {$('#" + Me.ph_custom_banner.ClientID + "').parent().insertBefore('#pnl_wrapper');});", True)
            End If
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(stringWriter)

        MyBase.Render(htmlWriter)

        ' Hier voegen we de skin toe
        Dim html As String = stringWriter.ToString().Trim()

        If Not html_content = "" Then
            html = html_content.Replace("|||", html)
        End If

        writer.Write(html)
    End Sub
End Class
