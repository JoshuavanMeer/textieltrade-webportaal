﻿
Partial Class uc_youtube_video_block

    Inherits Utilize.Web.Solutions.CMS.cms_user_control

#Region "Functions"
    Private Function create_youtube_video(ByVal banner_block_id As String, ByVal hgc_banner_block As Object) As Boolean
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim dt_videos As System.Data.DataTable = Nothing

        If ll_resume Then
            ' Maak een nieuwe datatable aan
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            With qsc_select
                .select_from = "UCM_YOUTUBE_VIDEO"
                .select_fields = "*"
                .select_where = " hdr_id = @hdr_id and shop_code = @shop_code"
                .select_order = "ROW_ORD"
            End With
            qsc_select.add_parameter("hdr_id", banner_block_id)
            qsc_select.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            dt_videos = qsc_select.execute_query()

            ll_resume = dt_videos.Rows.Count > 0
        End If

        If ll_resume Then
            Dim lo_row As New HtmlGenericControl("div")

            ' Loop door de regels heen
            For Each row As System.Data.DataRow In dt_videos.Rows
                Dim lo_iframe = New HtmlGenericControl("iframe")
                Dim lo_div = New HtmlGenericControl("div")

                Dim lc_youtube_code As String = row.Item("VIDEO_LINK")

                If lc_youtube_code.Contains("/") Then
                    lc_youtube_code = lc_youtube_code.Substring(lc_youtube_code.LastIndexOf("/") + 1)
                End If

                ' Maak hier de titel en omschrijving aan
                Dim lo_div_title = New HtmlGenericControl("div")
                lo_div_title.Attributes.Add("class", "you-tube-title-and-description")

                Dim lo_title = New HtmlGenericControl("h4")
                lo_title.Attributes.Add("class", "title")
                lo_title.InnerText = row.Item("VIDEO_DESC_" + Me.global_ws.Language)
                lo_div_title.Controls.Add(lo_title)

                Dim lc_short_desc As String = row.Item("VIDEO_SHORT_DESC_" + Me.global_ws.Language)

                If Not String.IsNullOrEmpty(lc_short_desc) Then
                    Dim lo_short_desc = New HtmlGenericControl("span")
                    lo_short_desc.Attributes.Add("class", "description")
                    lo_short_desc.InnerHtml = lc_short_desc
                    lo_div_title.Controls.Add(lo_short_desc)
                End If

                Dim lo_responsive As New HtmlGenericControl("div")
                lo_row.Attributes.Add("class", "row")

                lo_iframe.Attributes.Add("type", "text/html")
                lo_iframe.Attributes.Add("allowfullscreen", "allowfullscreen")
                lo_iframe.Attributes.Add("width", "100%")
                lo_iframe.Attributes.Add("src", "https://www.youtube.com/embed/" + lc_youtube_code)
                lo_iframe.Attributes.Add("frameborder", "0")

                lo_div.Attributes.Add("class", "col-md-" + Me.item_size_int.ToString())
                lo_responsive.Attributes.Add("class", "responsive-video")

                lo_responsive.Controls.Add(lo_iframe)
                lo_div.Controls.Add(lo_responsive)

                ' Voeg de titel en omschrijving toe
                lo_div.Controls.Add(lo_div_title)
                lo_row.Controls.Add(lo_div)
                ph_videos.Controls.Add(lo_row)
            Next
        End If

        Return ll_resume
    End Function
#End Region

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        ' Declareer een nieuwe datatable
        Dim dt_youtube_block As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_from = "UCM_YOUTUBE_BLOCKS"
            qsc_select.select_order = "row_ord"
            qsc_select.select_fields = "*"
            qsc_select.select_where = "rec_id = @rec_id"
            qsc_select.add_parameter("rec_id", Me.block_id)

            dt_youtube_block = qsc_select.execute_query()

            ll_resume = dt_youtube_block.Rows.Count > 0
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_youtube_video_block.css", Me)

            If Not dt_youtube_block.Rows(0).Item("skin_id") = "" Then
                Me.skin = dt_youtube_block.Rows(0).Item("skin_id")
            End If
        End If

        If ll_resume Then
            ll_resume = create_youtube_video(Me.block_id, Me.ph_videos)
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub
End Class
