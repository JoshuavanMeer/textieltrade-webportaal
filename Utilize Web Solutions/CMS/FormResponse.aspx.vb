﻿
Partial Class CMS_FormResponse
    Inherits CMSPageTemplate

    Private Function get_template_code() As String
        Dim lc_form_id As String = Me.get_page_parameter("id")

        Dim lc_template_code As String = Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "form_response_templ", lc_form_id)

        If String.IsNullOrEmpty(lc_template_code) Then
            lc_template_code = Me.global_cms.get_cms_setting("form_response_templ")
        End If

        Return lc_template_code
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Vul de pagina template uit de categorieën
        Dim lc_template_code As String = Me.get_template_code()

        ' Vul de pagina template
        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "form_title_" + Me.global_cms.Language, Me.get_page_parameter("id").Trim())
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("product_details.css")

        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_block = LoadUserControl("~/CMS/Modules/Forms/uc_form_response.ascx")

        uc_block.ID = "uc_form_response"

        ph_target_block.Controls.AddAt(ph_target_block.Controls.Count, uc_block)
    End Sub

End Class
