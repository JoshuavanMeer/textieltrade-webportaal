﻿
Partial Class news_page
    Inherits CMSPageTemplate

    Private news_id As String = ""

    Protected Sub Page_PreIni1t(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not Request.Params("RecId") Is Nothing Then
            news_id = Request.Params("RecId")
        End If

        ' Sla de code van de template op
        Me.cms_template_code = Me.global_cms.get_cms_setting("news_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("ucm_news", "title_" + Me.global_cms.Language, news_id)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not news_id = "" Then
            Dim lc_meta_desc As String = Utilize.Data.DataProcedures.GetValue("ucm_news", "meta_desc_" + Me.global_cms.Language, news_id)

            Me.meta_description = IIf(String.IsNullOrEmpty(lc_meta_desc), Utilize.Data.DataProcedures.GetValue("ucm_news", "desc_" + Me.global_cms.Language, news_id), lc_meta_desc)
            Me.meta_keywords = Utilize.Data.DataProcedures.GetValue("ucm_news", "meta_keyw_" + Me.global_cms.Language, news_id)

            Dim ph_news_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
            Dim uc_news_text As CMS.cms_user_control = LoadUserControl("Modules/News/uc_news_text.ascx")
            uc_news_text.set_property("block_id", Utilize.Data.DataProcedures.GetValue("ucm_news", "hdr_id", news_id))
            uc_news_text.set_property("news_id", news_id)
            uc_news_text.set_property("object_code", "ucm_news_blocks")
            ph_news_zone.Controls.Add(uc_news_text)
        End If
    End Sub
End Class
