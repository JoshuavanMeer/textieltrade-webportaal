﻿<%@ Page Language="VB" MasterPageFile="FWMasterPage.master" AutoEventWireup="false" CodeFile="setup_page.aspx.vb" Inherits="setup_page" title="Details" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../_FWAssets/Styles/Pages/SetupPage.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_content"></utilize:placeholder>
</asp:Content>

