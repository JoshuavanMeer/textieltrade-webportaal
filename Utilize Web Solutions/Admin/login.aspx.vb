
Partial Class login_page
    Inherits Utilize.Web.UI.GenericControls.login_page

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Verwijder de FCK Editor map
        Try
            If System.IO.Directory.Exists(Server.MapPath("~/") + "FCKEditor") Then
                System.IO.Directory.Delete(Server.MapPath("~/") + "FCKEditor", True)
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try

        ' Verwijder de FCK Editor dll
        Try
            If System.IO.File.Exists(Server.MapPath("~/") + "bin/FredCK.FCKeditorV2.dll") Then
                System.IO.File.Delete(Server.MapPath("~/") + "bin/FredCK.FCKeditorV2.dll")
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try

        ' Verwijder oude mappen
        Try
            If System.IO.Directory.Exists(Server.MapPath("~/") + "_FWScripts") Then
                System.IO.Directory.Delete(Server.MapPath("~/") + "_FWScripts", True)
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try

        Try
            If System.IO.Directory.Exists(Server.MapPath("~/") + "_FWImages") Then
                System.IO.Directory.Delete(Server.MapPath("~/") + "_FWImages", True)
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try

        Try
            If System.IO.Directory.Exists(Server.MapPath("~/") + "_FWStyles") Then
                System.IO.Directory.Delete(Server.MapPath("~/") + "_FWStyles", True)
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try

        Try
            If System.IO.Directory.Exists(Server.MapPath("~/") + "_FWResources") Then
                System.IO.Directory.Delete(Server.MapPath("~/") + "_FWResources", True)
            End If
        Catch ex As Exception
            Dim lc_error_message As String = ""
        End Try
    End Sub
End Class