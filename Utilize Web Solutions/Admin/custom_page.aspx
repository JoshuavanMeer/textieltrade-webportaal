﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/FWMasterPage.master" AutoEventWireup="false" CodeFile="custom_page.aspx.vb" Inherits="Admin_custom_page" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../_FWAssets/Styles/Pages/CustomPage.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_content"></utilize:placeholder>
</asp:Content>

