﻿Imports System.Drawing

Partial Class cropper_page
    Inherits Utilize.Web.UI.WebControls.Page

#Region "properties"
    Private image_url_original As String = "../_FWImages/image_not_available_original.jpg"
    Private image_url_cropped As String = "../_FWImages/image_not_available.jpg"

    Private image_dir As String = "..\_FWImages\"

    Private file_name_original As String = "image_not_available_original.jpg"
    Private file_name_cropped As String = "image_not_available.jpg"

    Private crop_width As Integer = 100
    Private crop_height As Integer = 100

    Private source_width As Integer = 0
    Private source_height As Integer = 0

    Private file_width As Integer = 0
    Private file_height As Integer = 0

    Private scale_factor_width As Decimal = 0
    Private scale_factor_height As Decimal = 0
#End Region

#Region " Helper subs"
    Private Sub set_file_properties()
        Dim pp_page_parameters As PageParameters = CType(Me.Page, Utilize.Web.UI.WebControls.Page).utlz_parameters

        Dim lc_image_location As String = pp_page_parameters.CustomParameterCollection.get_parameter("ImageLocation").Replace("_sl_", "/")

        ' Zet de url van de afbeelding
        Me.image_url_original = IIf(lc_image_location.StartsWith("/"), "", "../") + lc_image_location.Replace(".", "_original.")
        Me.image_url_cropped = IIf(lc_image_location.StartsWith("/"), "", "../") + lc_image_location

        ' Zet de directory van de afbeelding
        Me.image_dir = IIf(lc_image_location.StartsWith("/"), "", "..\") + lc_image_location.Replace("/", "\")
        Me.image_dir = Me.image_dir.Substring(0, Me.image_dir.LastIndexOf("\") + 1)

        Dim lo_file_stream As System.IO.FileStream = System.IO.File.OpenRead(Me.Server.MapPath(image_url_cropped))

        ' Haal de bestandsnaam en de extentie op
        Dim file_name As String = Regex.Replace(lo_file_stream.Name, ".*\\", "").Split(".")(0)
        Dim file_ext As String = "." + Regex.Replace(lo_file_stream.Name, ".*\\", "").Split(".")(1)

        ' Zet de bestandsnaam van de oorspronkelijke en doel afbeelding
        file_name_original = Server.MapPath(image_dir & file_name & "_original" & file_ext)
        file_name_cropped = Server.MapPath(image_dir & file_name & file_ext)

        lo_file_stream.Flush()
        lo_file_stream.Dispose()

        ' Hier kopieren we vast het originele bestand
        If Not System.IO.File.Exists(file_name_original) Then
            System.IO.File.Copy(file_name_cropped, file_name_original)
        End If
    End Sub
    Private Sub set_image_properties()
        ' Haal hier de bitmap op
        Dim loBitmap As Bitmap = Me.get_file_bitmap()

        ' Zet de bestandsgrootten
        file_width = loBitmap.Width
        file_height = loBitmap.Height

        ' Ga hier de grootte van de afbeelding bepalen
        If file_width >= file_height Then
            source_height = System.Math.Round(350 * (file_height / file_width), 0)
            source_width = 350
        Else
            source_height = 350
            source_width = System.Math.Round(350 * (file_width / file_height), 0)
        End If

        ' Zet hier de afmetingen van de bron en doel afbeelding
        Me.img_main.Width = source_width
        Me.img_main.Height = source_height
    End Sub

    Private Function get_file_bitmap() As Bitmap
        Dim oFileStream As System.IO.FileStream = System.IO.File.OpenRead(Me.Server.MapPath(image_url_original))
        Dim fileData(oFileStream.Length - 1) As Byte

        ' Read the file into a byte array
        oFileStream.Read(fileData, 0, oFileStream.Length - 1)
        oFileStream.Close()

        Dim image As Byte() = fileData

        Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream(image)

        Dim bitImage As Bitmap = New Bitmap(System.Drawing.Image.FromStream(memStream))

        ' Zet de breedte en hoogte
        file_width = bitImage.Width
        file_height = bitImage.Height

        Return bitImage
    End Function

    Private Function CropBitmap(ByRef bmp As Bitmap, ByVal cropX As Integer, ByVal cropY As Integer, ByVal cropWidth As Integer, ByVal cropHeight As Integer) As Bitmap
        cropWidth = cropWidth * (file_width / source_width)
        cropHeight = cropHeight * (file_height / source_height)

        cropX = cropX * (file_width / source_width)
        cropY = cropY * (file_height / source_height)

        Dim rect As New Rectangle(cropX, cropY, cropWidth, cropHeight)
        Dim cropped As Bitmap = bmp.Clone(rect, bmp.PixelFormat)

        If Not cropWidth.Equals(crop_width) Then
            Dim image_scaled As Bitmap = New Bitmap(crop_width, crop_height)
            Dim grphs As System.Drawing.Graphics = Graphics.FromImage(image_scaled)

            ' Zorg voor maximale beeldkwaliteit
            grphs.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
            grphs.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
            grphs.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
            grphs.DrawImage(cropped, 0, 0, crop_width, crop_height)

            ' schaal de afbeelding naar de gewenste grootte
            cropped = image_scaled.Clone()
            image_scaled.Dispose()
        End If

        Return cropped
    End Function
#End Region
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_file_properties()
        Me.set_image_properties()

        If Not Me.IsPostBack() Then
            Me.img_height.Attributes.Add("value", crop_height)
            Me.img_width.Attributes.Add("value", crop_width)

            Me.img_main.ImageUrl = Me.Page.ResolveUrl(image_url_original)
            Me.img_preview.ImageUrl = Me.Page.ResolveUrl(image_url_original)
        End If

        ' Altijd de cropper opnieuw laden
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "image_crop", "ImageCrop()", True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack() Then
            Me.crop_width = CInt(Me.img_width.Text)
            Me.crop_height = CInt(Me.img_height.Text)
        End If
    End Sub

    Protected Sub btn_crop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_crop.Click
        Dim img_original As Bitmap = get_file_bitmap()
        Dim img_cropped As Bitmap = CropBitmap(img_original, Request.Form.Item("x"), Request.Form.Item("y"), Request.Form.Item("w"), Request.Form.Item("h"))

        ' Verwijder eerst het oorspronkelijke bestand
        If System.IO.File.Exists(file_name_cropped) Then
            System.IO.File.Delete(file_name_cropped)
        End If

        If System.IO.File.Exists(file_name_cropped.Replace("Documents\", "Documents\_thumbs\")) Then
            System.IO.File.Delete(file_name_cropped.Replace("Documents\", "Documents\_thumbs\"))
        End If

        ' Sla het bestand op onder de oorspronkelijke bestandsnaam
        img_cropped.Save(file_name_cropped)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "close_window", "alert('De oorspronkelijke en geschaalde afbeelding is bewaard.'); window.parent.modalWindow.close();", True)
    End Sub
End Class
