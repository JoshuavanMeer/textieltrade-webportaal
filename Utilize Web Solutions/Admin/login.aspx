<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="login_page" %>

<!DOCTYPE html>
<html lang="nl">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Bootstrap CSS -->
    <link href="../_FWBootstrap/css/Bootstrap.css" rel="stylesheet">
    <link href="../_FWAssets/Styles/framework-custom.css" rel="stylesheet" />
    <link href="../_FWBootstrap/css/jasny-bootstrap.css" rel="stylesheet" />
    <link href="../_FWAssets/Styles/Pages/Login.css" rel="stylesheet" />

    <!-- jQuery -->
    <script src="../_FWAssets/Scripts/JQueryLibrary.js"></script>

    <!-- Bootstrap js -->
    <script src="../_FWBootstrap/js/Bootstrap.js"></script>

    <!-- Custom css -->
    <link href="../_FWAssets/Styles/framework-custom.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,600,700,800|Roboto:100,200,300,400,600,700,800|Oswald:100,200,300,400,600,700,800|Open+Sans+Condensed:100,200,300,400,600,700,800|Open+Sans:100,200,300,400,600,700,800|Raleway:100,200,300,400,600,700,800&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
    
     <script type="text/javascript">
        if (top === self) {
        }
        else {
            window.parent.location = self.location;
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <nav class="navbar navbar-fixed-top">
                <div class="navbar-header">
                    <img src="../_FWAssets/Images/Masterpage/util123_logo.jpg" class="logo" />
                </div>
            </nav>

            <div class="container">
    	        <div class="row">
    	            <div class="col-xs-12">
                            <utilize:panel ID="pnl_login" runat="server" DefaultButton="button_login" CssClass="form-wrap">
                                <h1><utilize:literal runat="server" ID="ltSignIn" Text="Aanmelden"></utilize:literal></h1>

                                <div class="form-group">
                                    <label class="sr-only" for="txt_username"><utilize:literal runat="server" ID="ltUserName" Text="Gebruikersnaam"></utilize:literal></label>
                                    <utilize:textbox placeholder="Gebruikersnaam" runat="server" ID="txtLoginCode" cssclass="form-control" ></utilize:textbox>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="txt_password"><utilize:literal runat="server" ID="ltPassWord" Text="Wachtwoord"></utilize:literal></label>
                                    <utilize:textbox placeholder="Wachtwoord" TextMode="Password" runat="server" ID="txtPassword" cssclass="form-control"></utilize:textbox>
                                </div>
                                <utilize:button runat="server" UseSubmitBehavior="true" CssClass="btn btn-primary" ID="button_login" Text="Aanmelden" />
                            <hr>
        	            </utilize:panel>
    		        </div>
                </div> 
            </div> 
        </div>
    </form>
</body>
</html>
