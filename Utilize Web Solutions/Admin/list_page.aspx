﻿<%@ Page Language="VB" MasterPageFile="FWMasterPage.master" AutoEventWireup="false" CodeFile="list_page.aspx.vb" Inherits="list_page" title="Overzicht" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../_FWAssets/Styles/Pages/ListPage.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_content"></utilize:placeholder>
</asp:Content>

