﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/FWMasterPage.master" AutoEventWireup="false" CodeFile="cropper_page.aspx.vb" Inherits="cropper_page" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../_FWAssets/Scripts/jQueryLibrary.js" type="text/javascript"></script>
    <script src="../_FWAssets/Scripts/jQueryImageCropper.js" type="text/javascript"></script>
    <script src="../_FWAssets/Scripts/jQueryAlerts.js" type="text/javascript"></script>

    <link href="../_FWAssets/Styles/Custom/ImageCropper.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_content"></utilize:placeholder>
    <asp:panel runat="server" ID="pnl_image_resize" class="uc_image_resize" Visible="true">
		<table class="imagecropping">
            <tr>
                <th style="text-align: left"><h1>Afbeelding bijsnijden</h1></th>
                <th style="text-align: left"><h1>Formaat afbeelding</h1></th>
            </tr>
			<tr>
				<td style="width: 450px;" rowspan="2">
                    <asp:image ID="img_main" runat="server" CssClass="mainimg" />
				</td>
                <td>
                    <table>
                        <tr>
                            <td>Hoogte:</td>
                            <td colspan="2"><utilize:textbox runat="server" class="img_height" id="img_height" name="img_height" maxlength=4 /></td>
                        </tr>
                        <tr>
                            <td>Breedte:</td>
                            <td><utilize:textbox runat="server" class="img_width" id="img_width" name="img_width" maxlength=4 /></td>
                            <td><utilize:button ID="button1" runat="server" Text="Bijwerken" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom; padding-right: 10px; text-align: right;">
                    <utilize:button ID="button_cancel" runat="server" Text="Annuleren" OnClientClick="window.parent.modalWindow.close(); return false;" />
                    <utilize:button ID="btn_crop" runat="server" text="Uitsnijden" />
                </td>
            </tr>
        </table>
		<div class="div_preview" style="border:1px solid #A3037B; overflow:hidden; display: none;">
            <asp:image ID="img_preview" runat="server" CssClass="preview" />
		</div>

        <input type="hidden" id="x" name="x" />
	    <input type="hidden" id="y" name="y" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />
    </asp:panel>
</asp:Content>

