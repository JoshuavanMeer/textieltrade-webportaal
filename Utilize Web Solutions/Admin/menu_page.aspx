﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="menu_page.aspx.vb" Inherits="menu_page" Title="Hoofdmenu" EnableEventValidation="false" %>

<!DOCTYPE html>
<html lang="nl">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>

    <link rel="shortcut icon" href="favicon.ico">

    <!-- Bootstrap CSS -->
    <link href="../_FWBootstrap/css/Bootstrap.css" rel="stylesheet">
    <link href="../_FWBootstrap/css/simple-sidebar.css" rel="stylesheet" />
    <link href="../_FWBootstrap/css/fileinput.css" rel="stylesheet" />
    <link href="../_FWAssets/Styles/ModalWindow.css" rel="stylesheet" />

    <!-- jQuery -->
    <script src="../_FWAssets/Scripts/JQueryLibrary.js"></script>
    <script src="../_FWAssets/Scripts/jQueryModalWindow.js"></script>

    <!-- Bootstrap js -->
    <script src="../_FWBootstrap/js/Bootstrap.js"></script>
    <script src="../_FWBootstrap/js/fileinput.js"></script>
    <script src="../_FWBootstrap/js/simple-sidebar.js"></script>

    <!-- Custom js -->
    <script src="../_FWAssets/Scripts/UtilizeMenuTabs.js"></script>

    <!-- Custom css -->
    <link href="../_FWAssets/Styles/Pages/menupage.css" rel="stylesheet" />
    <link href="../_FWAssets/Styles/framework-custom.css" rel="stylesheet" />
    <link href="../_FWAssets/Styles/framework-custom-menutabs.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,600,700,800|Roboto:100,200,300,400,600,700,800|Oswald:100,200,300,400,600,700,800|Open+Sans+Condensed:100,200,300,400,600,700,800|Open+Sans:100,200,300,400,600,700,800|Raleway:100,200,300,400,600,700,800&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <utilize:placeholder runat="server" ID="ph_content">
            
            </utilize:placeholder>
        </div>
    </form>
</body>
</html>
