﻿
Imports System.Linq

Partial Class Admin_FilterPage_Filterpage
    Inherits System.Web.UI.Page

    Private Sub Admin_FilterPage_Filterpage_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Get business object name
        Dim querystring = Request.RawUrl.Split("?")(1)
        Dim pageParameters = querystring.Split("&").ToList
        Dim pageObject = pageParameters.Where(Function(x) x.StartsWith("BusObj")).First.Split("=")(1)

        ScriptManager1.RegisterClientScriptBlock(Me, Me.GetType, "setupApp", "$(document).ready(function () {SetupApp('" + pageObject + "');});", True)
    End Sub
End Class
