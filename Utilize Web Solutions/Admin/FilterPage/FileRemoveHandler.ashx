﻿<%@ WebHandler Language="VB" Class="FileRemoveHandler" %>

Imports System
Imports System.Web

Public Class FileRemoveHandler : Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim fileLocation As String = context.Request.Form("filelocation")

        If Not String.IsNullOrEmpty(fileLocation) Then
            If Not fileLocation.StartsWith("/") Then
                fileLocation = "/" + fileLocation
            End If

            Dim serverLocation As String = context.Server.MapPath("~" + fileLocation)

            If System.IO.File.Exists(serverLocation) Then
                System.IO.File.Delete(serverLocation)
            End If
        End If

        context.Response.ContentType = "text/plain"
        context.Response.Write("Hello World")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class