﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Filterpage.aspx.vb" Inherits="Admin_FilterPage_Filterpage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/plugins/bootstrap/css/bootstrap.min.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/plugins/font-awesome/css/font-awesome.min.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Admin/FilterPage/Filterpage.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Styles/GlobalStyle.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/_FWAssets/Styles/framework-custom.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/_FWAssets/Styles/framework-custom-menutabs.css") %>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/_FWAssets/Styles/Pages/menupage.css") %>">

    <!-- JS Global Compulsory -->
    <script src="<%=ResolveUrl("~/_THMAssets/plugins/jquery/jquery.min.js") %>"></script>
    <script src="<%=ResolveUrl("~/_THMAssets/plugins/jquery/jquery-migrate.min.js") %>"></script>
    <script src="<%=ResolveUrl("~/_THMAssets/plugins/bootstrap/js/bootstrap.min.js") %>"></script>

    <script src="<%=ResolveUrl("~/ckeditor/ckeditor.js") %>"></script>

    <!-- VueJs -->    
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/filterpageservice.js") %>"></script>

    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Details/AutocompleteField.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Details/UploadControl.js") %>"></script>

    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Gridview/GridviewPaging.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Gridview/GridviewRow.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Details/DetailField.js") %>"></script>

    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Details/DetailPage.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Gridview/Gridview.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/Filters/Filters.js") %>"></script>
    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/ChildTables/ChildTables.js") %>"></script>

    <script src="<%=ResolveUrl("~/admin/filterpage/scripts/filterpage.js") %>"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div id="app" class="vue-framework-overview">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{pageTitle}}</h1>
                    <Filters
                        :filters="Filters"></Filters>
                    <gridview 
                        :columns="GridviewColumns"
                        :rows="GridViewRows"
                        :pages="GridViewPages"
                        :current-page="CurrentPage"
                        :selected-row="SelectedHeaderRow"
                        :selectable="true"></gridview>
                    <detail-page
                        v-if="Object.keys(DetailInformation).length !== 0"
                        :id="DetailInformation.id"
                        :table="DetailInformation.TableName"
                        :tabs="DetailInformation.Tabs"
                        :title="DetailInformation.DetailTitle"
                        :empty-required-fields="EmptyRequiredFields"></detail-page>
                    <child-tables
                        :tables="ChildTables"></child-tables>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
