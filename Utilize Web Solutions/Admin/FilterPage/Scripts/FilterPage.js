﻿function SetupApp(frameworkObject) {
    var app = new Vue({
        el: '#app',
        components: {
            'gridview': GridView,
            'filters': Filters,
            'child-tables': ChildTables,
            'detail-page': DetailPage
        },
        data() {
            return {
                GridviewColumns: [],
                GridViewRows: [],
                GridViewPages: 1,
                Filters: [],
                ChildTables: [],
                DetailInformation: {},
                CurrentPage: 1,
                SelectedHeaderRow: "",
                EmptyRequiredFields: [],
                pageTitle: ""
            };
        },
        watch: {
            SelectedHeaderRow: function () {
                this.debouncedGetChildTableRows();
            }
        },
        created() {
            this.getGridViewData();
            this.getRows();
            this.getFilters();
            this.debouncedGetChildTableRows = _.debounce(this.getChildTables, 500);
            FilterPageService.$on('changePage', (value) => {                
                this.CurrentPage = parseInt(value);
                this.getRows();
            });
            FilterPageService.$on('updateFilters', (value) => {
                this.CurrentPage = 1;
                this.getRows();
            });
            FilterPageService.$on('deleteRow', (value) => {
                this.deleteRow(value.rowId, value.Table);
            });
            FilterPageService.$on('goToDetails', (value) => {                
                this.getDetailInformation(value.rowId, value.Table);
            });
            FilterPageService.$on('saveDetails', (value) => {                
                this.saveDetails();
            });
            FilterPageService.$on('cancelDetails', (value) => {
                this.DetailInformation = value;
            });
            FilterPageService.$on('updateSelectedRow', (value) => {
                this.SelectedHeaderRow = value;
            });
        },
        methods: {
            getGridViewData() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetGridViewData";                
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.tableName = "";

                axios.post(endpoint, obj)
                    .then(res => {
                        this.GridviewColumns = res.data.d.columns;
                        this.pageTitle = res.data.d.tableDescription;
                    });
            },
            getRows() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetGridviewPage";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.pageNumber = this.CurrentPage;
                obj.filters = this.Filters;

                axios.post(endpoint, obj)
                    .then(res => {
                        this.GridViewRows = res.data.d.GridviewRows;
                        this.SelectedHeaderRow = res.data.d.FirstId;
                    });
                this.getPages();
            },
            getPages() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetGridviewPages";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.filters = this.Filters;

                axios.post(endpoint, obj)
                    .then(res => {
                        this.GridViewPages = res.data.d;
                    });
            },
            getFilters() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetPageFilters";
                var obj = {};
                obj.frameworkObject = frameworkObject;

                axios.post(endpoint, obj)
                    .then(res => {
                        this.Filters = res.data.d;
                    });
            },
            deleteRow(rowId, table) {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/DeleteRow";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.rowId = rowId;

                if (table === undefined) {
                    table = ""
                }

                obj.TableName = table;
                obj.HeaderId = this.SelectedHeaderRow;

                axios.post(endpoint, obj)
                    .then(res => {
                        if (!res.data.d.succes) {
                            alert("Verwijderen mislukt met reden: " + res.data.d.errorMessage);
                        } else {
                            if (res.data.d.isMainTable) {
                                this.getRows();
                            } else {
                                this.getChildTables();
                            }
                        }
                    });
            },
            getDetailInformation(id, table) {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetDetailInformation";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.id = id;
                obj.TableName = table;

                axios.post(endpoint, obj)
                    .then(res => {
                        this.DetailInformation = res.data.d;
                        this.DetailInformation.HeaderId = this.SelectedHeaderRow;
                    });
            },
            saveDetails() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/SaveDetails";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.detailInformation = this.DetailInformation;

                axios.post(endpoint, obj)
                    .then(res => {
                        if (res.data.d.succes) {
                            this.DetailInformation = {};
                            if (res.data.d.isMainTable) {
                                this.getRows();
                            } else {
                                this.getChildTables();
                            }
                        } else {
                            this.EmptyRequiredFields = res.data.d.emptyRequiredFields;
                        }
                    });
            },
            getChildTables() {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/GetChildTables";
                var obj = {};
                obj.frameworkObject = frameworkObject;
                obj.headerRow = this.SelectedHeaderRow;

                axios.post(endpoint, obj)
                    .then(res => {
                        this.ChildTables = res.data.d.tables;
                    });
            }
        }
    });
}