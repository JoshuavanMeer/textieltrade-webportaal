﻿var UploadControl = {
    props: ["field"],
    data() {
        return {
            file: '',
            uploaded: false
        };
    },
    methods: {
        submitFile() {
            var formData = new FormData();
            formData.append(this.file.name, this.file);
            formData.append('FileLocation', this.field.FieldListValues[0].Value);
            
            axios.post('/Admin/FilterPage/FileUploadHandler.ashx',
                formData).then(res => {
                    this.field.FieldValue = res.data;
                    this.uploaded = true;
                });
        },
        removeFile() {
            var formData = new FormData();
            formData.append('FileLocation', this.field.FieldValue);

            axios.post('/Admin/FilterPage/FileRemoveHandler.ashx',
                formData).then(res => {
                    this.field.FieldValue = "";
                    this.uploaded = false;
                    this.$refs.file.value = '';
                });
        },
        handleFileUpload() {
            this.file = this.$refs.file.files[0];
        }
    },
    template: `
<div class="container">
    <div class="large-12 medium-12 small-12 cell">
        <label>
            <input type="file" id="file" ref="file" v-on:change="handleFileUpload()" :disabled="uploaded"/>
        </label>
        <button class="imagebutton btn btn-primary btn-text" v-if="!uploaded" v-on:click="submitFile()" onclick="return false;">Uploaden</button>
        <button class="imagebutton btn btn-primary btn-text" v-if="uploaded" v-on:click="removeFile()" onclick="return false;">Remove</button>
    </div>
</div>
`
}