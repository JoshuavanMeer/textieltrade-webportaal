﻿var DetailField = {
    props: ["field"],
    components: {
        'autoCompleteField': AutoCompleteField,
        'uploadControl': UploadControl
    },
    mounted: function () {
        this.$nextTick(function () {
            if (this.field.FieldType === "htmlarea") {
                CKEDITOR.replace(this.field.Key);
            }
        })
    },
    template: `
    <div class="row field_object">
        <label class="control-label col-sm-12 col-md-4 col-lg-3">{{field.Description}}</label>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <input  class="form-control" 
                    v-if="field.FieldType === 'text' || field.FieldType === 'color' || field.FieldType === 'number'"
                    v-bind:id="field.Key" 
                    v-bind:type="field.FieldType" 
                    v-model="field.FieldValue" 
                    v-bind:required="field.Required" />
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-6">
            <input  class="form-checkbox checkbox"
                    v-if="field.FieldType === 'checkbox'"
                    v-bind:id="field.Key" 
                    v-bind:type="field.FieldType" 
                    v-model="field.FieldValue"/>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <input  class="form-date form-control"
                    v-if="field.FieldType === 'date'"
                    v-bind:id="field.Key" 
                    type="text" 
                    v-model="field.FieldValue"
                    placeholder="dd-mm-yyyy"/>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-6">
            <uploadControl v-if="field.FieldType === 'upload'" :field="field"></uploadControl>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-6">
            <textarea class="form-control"
                    v-if="field.FieldType === 'textarea' || field.FieldType === 'htmlarea'"
                    v-bind:id="field.Key" 
                    v-model="field.FieldValue"></textarea>
        </div>
            
        <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="radio-inline" v-if="field.FieldType === 'radio-buttons'">
                <div class="radio" v-for="button in field.FieldListValues">
                    <input type="radio"                         
                        v-model="field.FieldValue" 
                        v-bind:value="button.Key" 
                        v-bind:id="button.Key"/>
                    <label v-bind:for="button.Key">{{button.Value}}</label>
                </div>                
            </div>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-6">
            <select class="form-control" v-if="field.FieldType === 'dropdown'" v-model="field.FieldValue">
                <option v-for="option in field.FieldListValues" v-bind:value="option.Key">{{option.Value}}</option>
            </select>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-6">
            <autoCompleteField :field="field" v-if="field.FieldType === 'custom-picklist'"></autoCompleteField>        
        </div>
    </div>
`
}