﻿var AutoCompleteField = {
    props: ["field"],
    data() {
        return {
            description: "",
            AutoCompleteData: []
        };
    },
    created() {
        this.debouncedGetAutoCompleteData = _.debounce(this.getAutoCompleteData, 500);
    },
    computed: {
        fieldValue() {
            return this.field.FieldValue;
        }
    },
    watch: {
        fieldValue() {
            this.debouncedGetAutoCompleteData(false);
        }
    },
    methods: {
        getAutoCompleteData(exactOnly) {
            if (this.field.FieldValue !== "") {
                var endpoint = "/Admin/FilterPage/FilterPageMethods.aspx/Autocomplete";
                var obj = {};
                obj.inputValue = this.field.FieldValue;
                obj.table = this.field.ForeignTable;
                obj.exactOnly = exactOnly;

                axios.post(endpoint, obj)
                    .then(res => {
                        var isExactMatch = res.data.d.exactMatch;
                        this.AutoCompleteData = res.data.d.autoCompleteOptions;
                        if (isExactMatch) {
                            this.selectElement(this.AutoCompleteData[0]);
                        } else if (!isExactMatch && exactOnly) {
                            var empty = {};
                            empty.Key = "";
                            empty.Value = "";

                            this.selectElement(empty);
                        }
                    });
            }            
        },
        selectElement(keyValue) {
            this.field.FieldValue = keyValue.Key;                        
            this.description = keyValue.Value;
            this.AutoCompleteData = [];
        },
        focusOut() {
            this.debouncedGetAutoCompleteData(true);
        }
    },
    template: `
<div class="auto-complete">
    <input class="form-control" type="text" v-model.trim="field.FieldValue" v-on:blur="focusOut()" />
    <div class="auto-complete-description input-group-addon" v-bind:class="{ active: description.length }">
        {{description}}
    </div>
    <div class="auto-complete-data" v-if="AutoCompleteData.length > 0">
        <div class="auto-complete-option" v-for="option in AutoCompleteData" v-on:click="selectElement(option)">
            <div class="code">{{option.Key}}</div>
            <div class="description">{{option.Value}}</div>
        </div>
    </div>
</div>
`
}