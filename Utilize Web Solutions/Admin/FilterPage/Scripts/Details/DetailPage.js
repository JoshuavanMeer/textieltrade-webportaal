﻿var DetailPage = {
    props: ["id", "table", "tabs", "EmptyRequiredFields", "title"],
    components: {
        'detailField': DetailField
    },
    methods: {
        cancel() {
            FilterPageService.$emit('cancelDetails', {});
        },
        save() {
            FilterPageService.$emit('saveDetails', this.id);
        }
    },
    template: `
<div class="detail-page">
<div class="page-content">
    <h1>{{title}}</h1>
    <div class="detail-buttons btn-group options">
        <a class="imagebutton btn btn-default btn-text" v-on:click="cancel()" onclick="return false;">
            <span class="imagebutton glyphicon glyphicon-thumbs-down"></span>
            Annuleren
        </a>
        <a class="imagebutton btn btn-primary btn-text" v-on:click="save()" onclick="return false;">
            <span class="imagebutton glyphicon glyphicon-ok"></span>
            Opslaan
        </a>
    </div>
    <div class="tabs">	    
        <ul class="nav nav-tabs">
            <li v-for="tab in tabs" v-bind:class="{active: tab.Key == 'Algemeen'}">
                <a v-bind:href="'#' + tab.Key.replace(' ', '')" data-toggle="tab">{{tab.Key}}</a>
            </li>        
        </ul>
        <div class="alert alert-danger" v-if="EmptyRequiredFields.length > 0">
            <span>De volgende velden zijn verplicht en niet ingevuld</span>
            <ul>
                <li v-for="field in EmptyRequiredFields">
                    {{field.Description}}
                </li>
            </ul>
        </div>
    
        <div class="tab-content clearfix form-horizontal">
            <div v-for="tab in tabs" class="tab-pane" v-bind:class="{active: tab.Key == 'Algemeen'}" v-bind:id="tab.Key.replace(' ', '')">                
                <detailField v-for="field in tab.Value" :field="field" v-bind:key="field.Key"></detailField>
            </div>				
        </div>
    </div>
</div>
</div>
`
};