﻿var GridViewPaging = {
    props: ["pages", "CurrentPage"],
    data() {
        return {
            selectablePages: []
        };
    },
    created() {
        this.setSelectablePages();
    },
    watch: {
        'CurrentPage': function (value) {
            this.setSelectablePages();
        },
        'pages': function (value) {
            this.setSelectablePages();
        }
    },
    methods: {
        setSelectablePages() {
            this.selectablePages = [];

            var start = ((this.CurrentPage - 2 < 1) ? 1 : this.CurrentPage - 2);

            if (start > 1) {
                this.selectablePages.push("1");
            }

            if (start > 2) {
                this.selectablePages.push("...");
            }

            for (i = start; i <= this.CurrentPage; i++) {
                this.selectablePages.push(i.toString());
            }

            if (this.CurrentPage + 1 < this.pages) {
                var newPageNr = this.CurrentPage + 1;
                this.selectablePages.push(newPageNr.toString());

                newPageNr++;

                this.selectablePages.push(newPageNr.toString());

                if (newPageNr < this.pages - 1) {
                    this.selectablePages.push("...");
                }
                if (newPageNr < this.pages) {
                    this.selectablePages.push(this.pages.toString());
                }

            } else if (this.CurrentPage !== this.pages) {
                this.selectablePages.push(this.pages.toString());
            }

        },
        ChangePage(page) {
            var val = parseInt(page);
            if (!isNaN(val)) {
                FilterPageService.$emit('changePage', page);
            }
        }
    },
    template:
        `
<td colspan="8" v-if="pages > 1">
    <table>                 
        <tbody id="lower-paging">
            <tr class="pagination">
                <td class="li-previous" v-if="CurrentPage > 1">
                    <a v-on:click="ChangePage(CurrentPage - 1)">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                    </a>
                </td>
                <td v-for="page in selectablePages" v-bind:class="{active: CurrentPage == page}">
                    <a v-on:click="ChangePage(page)">
                        {{page}}
                    </a>
                </td>                
                <td class="li-next" v-if="CurrentPage < pages">
                    <a v-on:click="ChangePage(CurrentPage + 1)">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        </tbody>     
    </table>
</td>
        `
}