﻿var GridViewRow = {
    props: ["columns", "rowId", "SelectedRow", "selectable", "Table"],
    methods: {
        removeRow() {
            var r = confirm("Weet u zeker dat u deze regel wilt verwijderen?");
            if (r) {
                var obj = {};
                obj.rowId = this.rowId;
                obj.Table = this.Table;

                FilterPageService.$emit('deleteRow', obj);
            }
        },
        editRow() {
            var obj = {};
            obj.rowId = this.rowId;
            obj.Table = this.Table;

            if (this.Table === undefined) {
                obj.Table = "";
            }

            FilterPageService.$emit('goToDetails', obj);
        },
        UpdateSelectedRow() {
            if (this.selectable === true) {
               FilterPageService.$emit('updateSelectedRow', this.rowId);
            }
        }
    },
    template: `
<tr v-bind:class="{active: rowId === SelectedRow}" v-on:click="UpdateSelectedRow()">
    <template v-for="column in columns">
        <td class="deletecolumn" v-if="column.Key == 'remove'">
            <a class="btn btn-xs btn-default" v-on:click="removeRow()" onclick="return false;">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
        </td>

        <td class="editcolumn" v-else-if="column.Key == 'edit'">
            <a class="btn btn-xs btn-default" v-on:click="editRow()" onclick="return false;">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
        </td>
        
        <td v-else>
            {{column.Value}}
        </td>
    </template>
       
</tr>
`
}