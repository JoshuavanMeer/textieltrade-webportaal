﻿var GridView = {
    props: ["columns", "rows", "pages", "CurrentPage", "SelectedRow", "selectable", "Table"],
    components: {
        'gridviewRow': GridViewRow,
        'gridviewPaging': GridViewPaging
    },
    methods: {
        createNew() {
            var obj = {};
            obj.rowId = "";
            obj.Table = this.Table;

            if (this.Table === undefined) {
                obj.Table = "";
            }

            FilterPageService.$emit('goToDetails', obj);
        }
    },
    template: `
<div class="gridview">
    <div class="toolbar">
        <a class="imagebutton btn btn-primary create-new" v-on:click="createNew()" onclick="return false;">
            <span class="imagebutton glyphicon glyphicon-plus"></span>        
        </a>                          
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-condensed f11" cellspacing="0" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <th scope="col" v-for="column in columns">
                        <template v-if="column.Value !=='Edit' && column.Value !== 'Remove'">
                            {{column.Value}}
                        </template>
                    </th>
                </tr>
            </thead>
            <tbody>
                    <gridviewRow v-for="row in rows" 
                        :row-id="row.Key"
                        :columns="row.Value"
                        :key="row.Key"
                        :SelectedRow="SelectedRow"
                        :selectable="selectable"
                        :Table="Table"></gridviewRow>
            </tbody>
            <tfoot>
                <tr>
                    <gridviewPaging :pages="pages" :current-page="CurrentPage"></gridviewPaging>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
`
}