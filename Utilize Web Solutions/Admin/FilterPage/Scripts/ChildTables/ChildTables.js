﻿var ChildTables = {
    props: ["tables"],
    components: {
        'gridview': GridView
    },
    template: `
<div class="child-tables">
    <div class="child-table-buttons">

    </div>
    <div class="tabs">	    
        <ul class="nav nav-tabs" role="tablist">
            <li v-for="table in tables" v-bind:class="{active: tables[0].Key == table.Key}">
                <a v-bind:href="'#' + table.Key.replace(' ', '')" data-toggle="tab">{{table.Value.TableDescription}}</a>
            </li>        
        </ul>
    
        <div class="tab-content clearfix">
            <div v-for="table in tables" 
                class="tab-pane" v-bind:class="{active: tables[0].Key == table.Key}" 
                v-bind:id="table.Key.replace(' ', '')">   
                
                <gridview 
                    :columns="table.Value.Columns"
                    :rows="table.Value.Rows"
                    :selectable="false"
                    :Table="table.Key"></gridview>
            </div>				
        </div>
    </div>
</div>

`
}