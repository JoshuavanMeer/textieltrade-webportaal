﻿var Filters = {
    props: ["filters"],
    methods: {
        ActivateFilters() {
            FilterPageService.$emit('updateFilters', this.filters);
        }
    },
    template: `
<div class="filters form-horizontal">
    <div class="filter-element row field_object" v-for="filter in filters">
        <label class="control-label col-sm-12 col-md-4 col-lg-3">{{filter.Description}}</label>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <input class="input form-control" v-bind:type="filter.fieldType" v-model="filter.FieldValue" v-on:keyup.enter="ActivateFilters()">
        </div>
    </div>
     <div class="row filter-btns" v-if="filters.length > 0">
        <div class="col-sm-12 col-md-4 col-lg-3"></div>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <button class="inputbutton btn btn-default" v-on:click="ActivateFilters()" onclick="return false;">Filter</button>
        </div>
    </div>
</div>
`
}