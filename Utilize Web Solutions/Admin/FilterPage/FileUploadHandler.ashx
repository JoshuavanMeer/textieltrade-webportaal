﻿<%@ WebHandler Language="VB" Class="FileUploadHandler" %>

Imports System
Imports System.Web

Public Class FileUploadHandler : Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If context.Request.Files.Count > 0 Then
            Dim files As HttpFileCollection = context.Request.Files
            Dim fileLocation As String = context.Request.Form("filelocation")

            If Not fileLocation.StartsWith("/") Then
                fileLocation = "/" + fileLocation
            End If

            Dim serverLocation As String = context.Server.MapPath("~" + fileLocation)

            If Not System.IO.Directory.Exists(serverLocation) Then
                System.IO.Directory.CreateDirectory(serverLocation)
            End If

            Dim file As HttpPostedFile = files(0)

            Dim filename As String = serverLocation + file.FileName

            file.SaveAs(filename)
            If file.FileName.Contains(".jpeg") Or file.FileName.Contains(".png") Or file.FileName.Contains(".jpg") Then
                Try
                    Dim extension = System.IO.Path.ChangeExtension(filename, "webp")
                    Dim imageProcessor As New ImageProcessor
                    imageProcessor.SaveImageAsWebp(filename)
                Catch ex As Exception

                End Try
            End If

            context.Response.ContentType = "text/plain"
            context.Response.Write(fileLocation + file.FileName)
        End If
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class