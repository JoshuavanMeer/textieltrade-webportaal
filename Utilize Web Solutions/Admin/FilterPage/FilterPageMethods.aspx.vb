﻿
Imports System.Data
Imports System.IO
Imports System.Linq
Imports System.Web.Services
Imports System.Xml.Serialization

Partial Class Admin_FilterPage_FilterPageMethods
    Inherits System.Web.UI.Page

    <WebMethod>
    Public Shared Function GetGridViewData(frameworkObject As String, tableName As String) As Object
        Dim useTable As String = tableName
        If String.IsNullOrEmpty(useTable) Then
            useTable = GetTableName(frameworkObject)
        End If

        Dim qsc_columns As New Utilize.Data.QuerySelectClass
        With qsc_columns
            .select_fields = "fld_name, fld_description"
            .select_from = "utlz_dd_fields"
            .select_where = "dbf_name = @dbf_name and fld_is_column = 1"
            .select_order = "row_ord"
        End With
        qsc_columns.select_where += " and (mod_code = '' or mod_code in (select mod_code from utlz_fw_modules where mod_active = 1))"
        qsc_columns.add_parameter("dbf_name", useTable)

        Dim dt_columns As System.Data.DataTable = qsc_columns.execute_query

        Dim columns As New List(Of KeyValue)
        dt_columns.AsEnumerable.ToList.ForEach(Sub(x)
                                                   columns.Add(New KeyValue With {.Key = x.Item("fld_name").ToString, .Value = x.Item("fld_description").ToString})
                                               End Sub)

        columns.Add(New KeyValue With {.Key = "edit", .Value = "Edit"})
        columns.Add(New KeyValue With {.Key = "remove", .Value = "Remove"})

        Dim tableDescription As String = Utilize.Data.DataProcedures.GetValue("utlz_dd_tables", "dbf_description", useTable)

        Return New With {
            columns,
            tableDescription
        }
    End Function

    <WebMethod>
    Public Shared Function GetGridviewPages(frameworkObject As String, filters As List(Of FrameworkField)) As Integer
        Dim TableName As String = GetTableName(frameworkObject)
        Dim qsc_pages As New Utilize.Data.QuerySelectClass
        With qsc_pages
            .select_fields = "count(*) as total"
            .select_from = TableName
        End With

        If filters.Count > 0 Then
            For Each keyval In filters
                Dim filter As FrameworkField = keyval
                Dim fieldName As String = filter.Key
                Dim fieldValue As String = filter.FieldValue

                If Not String.IsNullOrEmpty(fieldValue) Then
                    If String.IsNullOrEmpty(qsc_pages.select_where) Then
                        qsc_pages.select_where += fieldName + " like @" + fieldName
                    Else
                        qsc_pages.select_where += " and " + fieldName + " like @" + fieldName
                    End If
                    qsc_pages.add_parameter(fieldName, fieldValue + "%")
                End If
            Next
        End If

        Dim total As Integer = qsc_pages.execute_query.Rows(0).Item("total")

        Return Math.Ceiling(total / 10)
    End Function

    <WebMethod>
    Public Shared Function DeleteRow(frameworkObject As String, rowId As String, TableName As String, HeaderId As String) As Object
        Dim isMainTable As Boolean = False
        If String.IsNullOrEmpty(TableName) Then
            TableName = GetTableName(frameworkObject)
            isMainTable = True
        End If

        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Nothing
        Dim succes As Boolean = True

        If isMainTable Then
            ubo_bus_obj = Utilize.Data.DataProcedures.CreateRecordObject(frameworkObject, rowId)
        Else
            ubo_bus_obj = Utilize.Data.DataProcedures.CreateRecordObject(frameworkObject, HeaderId)
            succes = ubo_bus_obj.table_seek(TableName, rowId)
        End If


        If succes Then
            succes = ubo_bus_obj.table_delete_row(TableName, "framework_delete")
        End If

        Dim errorMessage As String = ubo_bus_obj.error_message

        Return New With {
            succes,
            errorMessage,
            isMainTable
        }
    End Function

    <WebMethod>
    Public Shared Function GetDetailInformation(frameworkObject As String, id As String, TableName As String) As Object
        If String.IsNullOrEmpty(TableName) Then
            TableName = GetTableName(frameworkObject)
        End If

        Dim qsc_fields As New Utilize.Data.QuerySelectClass
        With qsc_fields
            .select_fields = "fld_name, fld_description, fld_tab, fld_mandatory, fld_type, fld_control, fld_xml, fld_dbf_foreign"
            .select_from = "utlz_dd_fields"
            .select_where = "dbf_name = @dbf_name and fld_is_field = 1"
            .select_order = "row_ord"
        End With
        qsc_fields.select_where += " and (mod_code = '' or mod_code in (select mod_code from utlz_fw_modules where mod_active = 1))"
        qsc_fields.add_parameter("dbf_name", TableName)

        Dim dt_fields As System.Data.DataTable = qsc_fields.execute_query

        Dim dr_line_information As System.Data.DataRow = Nothing
        If Not String.IsNullOrEmpty(id) Then
            dr_line_information = Utilize.Data.DataProcedures.GetDataRow(TableName, id)
        End If

        Dim Tabs As New List(Of KeyValue)
        For Each row In dt_fields.Rows
            Dim Fields As New List(Of FrameworkField)

            If String.IsNullOrEmpty(row.item("fld_tab")) Then row.item("fld_tab") = "Algemeen"
            Dim fieldType = GetFieldType(row.item("fld_type"), row.item("fld_control"))

            Dim field As New FrameworkField With {
                .Key = row.item("fld_name"),
                .Required = row.item("fld_mandatory"),
                .Description = row.item("fld_description"),
                .FieldType = fieldType,
                .FieldListValues = GetFieldDefaultList(fieldType, row.item("fld_control"), row.item("fld_xml"), row.item("fld_dbf_foreign"), TableName, row.item("fld_name")),
                .ForeignTable = row.item("fld_dbf_foreign")
            }

            Dim currentTab As KeyValue = Tabs.Find(Function(x) x.Key = row.item("fld_tab"))
            If currentTab Is Nothing Then
                currentTab = New KeyValue With {.Key = row.item("fld_tab"), .Value = Fields}
                Tabs.Add(currentTab)
            Else
                Fields = currentTab.Value
            End If

            If Not dr_line_information Is Nothing Then
                If fieldType.ToLower = "number" Then
                    dr_line_information.Item(row.item("fld_name")) = dr_line_information.Item(row.item("fld_name"))
                    field.FieldValue = dr_line_information.Item(row.item("fld_name")).ToString.Replace(",", ".")
                Else
                    field.FieldValue = dr_line_information.Item(row.item("fld_name"))
                End If
            End If

            Fields.Add(field)
        Next

        Tabs = Tabs.OrderBy(Function(x)
                                If x.Key = "Algemeen" Then
                                    Return 0
                                End If
                                Return 1
                            End Function).ThenBy(Function(x) x.Key).ToList

        Dim tableDescription As String = "Details - " + Utilize.Data.DataProcedures.GetValue("utlz_dd_tables", "dbf_description", TableName)

        Return New DetailInformation With {
            .TableName = TableName,
            .Tabs = Tabs,
            .Id = id,
            .DetailTitle = tableDescription
        }
    End Function

    <WebMethod>
    Public Shared Function SaveDetails(frameworkObject As String, detailInformation As DetailInformation) As Object
        Dim tableName As String = detailInformation.TableName

        Dim isMainTable As Boolean = tableName = GetTableName(frameworkObject)
        Dim tabs As List(Of KeyValue) = detailInformation.Tabs
        Dim id As String = detailInformation.Id

        Dim ubo_object As Utilize.Data.BusinessObject = Nothing

        If isMainTable And String.IsNullOrEmpty(id) Then
            ' New main table row
            ubo_object = Utilize.Data.DataProcedures.CreateBusinessObject(frameworkObject)
        ElseIf isMainTable Then
            ' Edit existing main table row
            ubo_object = Utilize.Data.DataProcedures.CreateRecordObject(frameworkObject, id)
        Else
            ' Either new or editing a sub table row
            ubo_object = Utilize.Data.DataProcedures.CreateRecordObject(frameworkObject, detailInformation.HeaderId)
        End If

        If String.IsNullOrEmpty(id) Then
            ubo_object.table_insert_row(tableName, "framework-update-details")
        Else
            ubo_object.table_seek(tableName, id)
        End If

        Dim emptyRequiredFields As New List(Of FrameworkField)

        For Each tabElement In tabs
            For Each field In tabElement.Value
                If field.Required And String.IsNullOrEmpty(field.FieldValue) Then
                    emptyRequiredFields.Add(field)
                End If

                If Not field.FieldValue Is Nothing Then
                    If field.FieldType.tolower = "number" Then
                        field.FieldValue = Convert.ToDecimal(field.FieldValue.ToString.Replace(".", ","))
                    End If

                    ubo_object.field_set(tableName + "." + field.Key, field.FieldValue)
                End If
            Next
        Next

        Dim succes As Boolean = ubo_object.table_update("framework-update-details")

        Return New With {
            succes,
            emptyRequiredFields,
            isMainTable
        }
    End Function

    <WebMethod>
    Public Shared Function GetGridviewPage(frameworkObject As String, pageNumber As Integer, filters As List(Of FrameworkField)) As Object
        Dim TableName As String = GetTableName(frameworkObject)
        Dim PrimaryKey As String = GetPrimaryField(TableName)
        Dim PrimaryKeyInView As Boolean = True

        pageNumber -= 1

        Dim columnData As List(Of KeyValue) = GetGridViewData(frameworkObject, TableName).columns
        Dim columns As List(Of String) = columnData.Where(Function(x) Not x.Key = "edit" And Not x.Key = "remove").Select(Function(x) x.Key).ToList

        If Not columns.Contains(PrimaryKey) Then
            PrimaryKeyInView = False
            columns.Add(PrimaryKey)
        End If

        Dim qc_page_content As New Utilize.Data.QueryCustomSelectClass

        Dim QueryString As String = "select "
        columns.ForEach(Sub(x) QueryString += x + ", ")
        QueryString = QueryString.Substring(0, QueryString.LastIndexOf(","))

        QueryString += " from " + TableName

        If filters.Count > 0 Then
            For Each keyval In filters
                Dim filter As FrameworkField = keyval
                Dim fieldName As String = filter.Key
                Dim fieldValue As String = filter.FieldValue

                If Not String.IsNullOrEmpty(fieldValue) Then
                    If Not QueryString.Contains(" where ") Then
                        QueryString += " where " + fieldName + " like @" + fieldName
                    Else
                        QueryString += " and " + fieldName + " like @" + fieldName
                    End If
                    qc_page_content.add_parameter(fieldName, fieldValue + "%")
                End If
            Next
        End If

        QueryString += " order by " + GetOrderField(TableName)

        Dim skipsize As Integer = 10 * pageNumber

        QueryString += " OFFSET " + skipsize.ToString + " ROWS"
        QueryString += " FETCH NEXT 10 ROWS ONLY;"

        With qc_page_content
            .QueryString = QueryString
        End With

        Dim dt_page_content As System.Data.DataTable = qc_page_content.execute_query("Framework")

        Dim rows As New List(Of KeyValue)

        For Each row In dt_page_content.Rows
            Dim rowElement = New KeyValue With {
                .Key = row.item(PrimaryKey)
            }

            Dim rowData As New List(Of KeyValue)

            For Each column In columns
                If Not column = PrimaryKey Then
                    rowData.Add(New KeyValue With {.Key = column, .Value = row.item(column).ToString})
                Else
                    If PrimaryKeyInView Then
                        rowData.Add(New KeyValue With {.Key = column, .Value = row.item(column)})
                    End If
                End If
            Next

            rowData.Add(New KeyValue With {.Key = "edit", .Value = ""})
            rowData.Add(New KeyValue With {.Key = "remove", .Value = ""})

            rowElement.Value = rowData

            rows.Add(rowElement)
        Next

        Dim FirstId As String = ""

        If rows.Count > 0 Then
            FirstId = rows.First.Key
        End If

        Return New With {
            .GridviewRows = rows,
            FirstId
        }
    End Function

    <WebMethod>
    Public Shared Function SingleFileUpload(context As HttpContext) As Object
        Return New With {
            .message = "Ghallo"
        }
    End Function

    <WebMethod>
    Public Shared Function GetPageFilters(frameworkObject As String) As Object
        Dim TableName As String = GetTableName(frameworkObject)

        Dim qsc_columns As New Utilize.Data.QuerySelectClass
        With qsc_columns
            .select_fields = "fld_name, fld_description, fld_type, fld_control, fld_xml, fld_dbf_foreign"
            .select_from = "utlz_dd_fields"
            .select_where = "dbf_name = @dbf_name and fld_search = 1"
            .select_order = "row_ord"
        End With
        qsc_columns.select_where += " and (mod_code = '' or mod_code in (select mod_code from utlz_fw_modules where mod_active = 1))"
        qsc_columns.add_parameter("dbf_name", TableName)

        Dim dt_columns As System.Data.DataTable = qsc_columns.execute_query

        Dim pageFilters As New List(Of FrameworkField)

        For Each row In dt_columns.Rows
            Dim filter = New FrameworkField
            filter.Key = row.item("fld_name")

            filter.Description = row.item("fld_description")
            filter.FieldValue = ""

            filter.FieldType = GetFieldType(row.item("fld_type"), row.item("fld_control"))
            filter.FieldListValues = GetFieldDefaultList(filter.FieldType, row.item("fld_control"), row.item("fld_xml"), row.item("fld_dbf_foreign"), TableName, filter.Key)

            pageFilters.Add(filter)
        Next

        Return pageFilters
    End Function

    <WebMethod>
    Public Shared Function Autocomplete(inputValue As String, table As String, exactOnly As Boolean) As Object
        Dim keyField As String = ""
        Dim descriptionField As String = ""

        GetTableMainFields(table, keyField, descriptionField)

        Dim dr_information As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(table, inputValue)
        Dim exactMatch As Boolean = True

        Dim autoCompleteOptions As New List(Of KeyValue)

        If dr_information Is Nothing Then
            exactMatch = False

            If exactOnly Then
                Return New With {
                    autoCompleteOptions,
                    exactMatch
                }
            End If

            Dim qsc_search As New Utilize.Data.QuerySelectClass
            With qsc_search
                .select_fields = "top 10 " + keyField + ", " + descriptionField
                .select_from = table
                .select_where = keyField + " like @keyfield or " + descriptionField + " like @descriptionfield"
                .select_order = keyField
            End With
            qsc_search.add_parameter("keyField", inputValue + "%")
            qsc_search.add_parameter("descriptionField", "%" + inputValue + "%")

            Dim dt_search As System.Data.DataTable = qsc_search.execute_query

            For Each row In dt_search.Rows
                autoCompleteOptions.Add(New KeyValue With {.Key = row.item(keyField), .Value = row.item(descriptionField)})
            Next
        Else
            autoCompleteOptions.Add(New KeyValue With {.Key = dr_information.Item(keyField), .Value = dr_information.Item(descriptionField)})
        End If

        Return New With {
            autoCompleteOptions,
            exactMatch
        }
    End Function

    <WebMethod>
    Public Shared Function GetChildTables(frameworkObject As String, headerRow As String) As Object
        Dim MainTable As String = GetTableName(frameworkObject)

        Dim qsc_child_tables As New Utilize.Data.QuerySelectClass
        With qsc_child_tables
            .select_fields = "dbf_name, obj_desc"
            .select_from = "utlz_fw_obj_lin"
            .select_where = "obj_code = @obj_code"
            .select_order = "row_ord"
        End With
        qsc_child_tables.select_where += " and (mod_code = '' or mod_code in (select mod_code from utlz_fw_modules where mod_active = 1))"
        qsc_child_tables.add_parameter("obj_code", frameworkObject)

        Dim dt_child_tables As System.Data.DataTable = qsc_child_tables.execute_query

        Dim tables As List(Of KeyValue) = dt_child_tables.AsEnumerable.Select(Function(x)
                                                                                  Dim Columns = GetGridViewData(frameworkObject, x.Item("dbf_name")).columns
                                                                                  Dim Rows = GetChildRows(x.Item("dbf_name"), headerRow, 1, Columns, MainTable)
                                                                                  Return New KeyValue With
                                                                                  {
                                                                                    .Key = x.Item("dbf_name"),
                                                                                    .Value = New With {
                                                                                            .TableDescription = x.Item("obj_desc"),
                                                                                            Columns,
                                                                                            Rows
                                                                                        }
                                                                                  }
                                                                              End Function).ToList

        Return New With {
            tables
        }
    End Function

    Private Shared Function GetChildRows(TableName As String, headerRow As String, pageNumber As Integer, columns As List(Of KeyValue), HeaderTable As String) As Object
        Dim PrimaryKey As String = GetPrimaryField(TableName)
        Dim PrimaryKeyInView As Boolean = True

        Dim foreignField As String = GetForeignKeyField(TableName, HeaderTable)

        pageNumber -= 1

        Dim columnNames As List(Of String) = columns.Where(Function(x) Not x.Key = "edit" And Not x.Key = "remove").Select(Function(x) x.Key).ToList

        If Not columnNames.Contains(PrimaryKey) Then
            PrimaryKeyInView = False
            columnNames.Add(PrimaryKey)
        End If

        Dim qc_page_content As New Utilize.Data.QueryCustomSelectClass

        Dim QueryString As String = "select "
        columnNames.ForEach(Sub(x) QueryString += x + ", ")
        QueryString = QueryString.Substring(0, QueryString.LastIndexOf(","))

        QueryString += " from " + TableName

        QueryString += " Where " + foreignField + " = @foreignValue"
        qc_page_content.add_parameter("foreignValue", headerRow)

        QueryString += " order by " + GetOrderField(TableName)

        Dim skipsize As Integer = 10 * pageNumber

        QueryString += " OFFSET " + skipsize.ToString + " ROWS"
        QueryString += " FETCH NEXT 10 ROWS ONLY;"

        With qc_page_content
            .QueryString = QueryString
        End With

        Dim dt_page_content As System.Data.DataTable = qc_page_content.execute_query("Framework")

        Dim rows As New List(Of KeyValue)

        For Each row In dt_page_content.Rows
            Dim rowElement = New KeyValue With {
                .Key = row.item(PrimaryKey)
            }

            Dim rowData As New List(Of KeyValue)

            For Each column In columnNames
                If Not column = PrimaryKey Then
                    rowData.Add(New KeyValue With {.Key = column, .Value = row.item(column)})
                Else
                    If PrimaryKeyInView Then
                        rowData.Add(New KeyValue With {.Key = column, .Value = row.item(column)})
                    End If
                End If
            Next

            rowData.Add(New KeyValue With {.Key = "edit", .Value = ""})
            rowData.Add(New KeyValue With {.Key = "remove", .Value = ""})

            rowElement.Value = rowData

            rows.Add(rowElement)
        Next

        Dim FirstId As String = ""

        If rows.Count > 0 Then
            FirstId = rows.First.Key
        End If

        Return rows
    End Function

    Private Shared Function GetForeignKeyField(tableName As String, headerTable As String) As String
        Dim qsc_foreign_key As New Utilize.Data.QuerySelectClass
        With qsc_foreign_key
            .select_fields = "fld_name"
            .select_from = "utlz_dd_fields"
            .select_where = "dbf_name = @dbf_name and fld_dbf_foreign = @foreign"
        End With
        qsc_foreign_key.add_parameter("dbf_name", tableName)
        qsc_foreign_key.add_parameter("foreign", headerTable)

        Return qsc_foreign_key.execute_query.Rows(0).Item("fld_name")
    End Function

    Private Shared Function GetFieldType(fieldType As Integer, fieldControl As String) As String
        Dim fieldTypeName As String = ""
        Select Case fieldType
            Case 1 ' Text - input field
                ' Posibilities
                ' * Normal textbox
                ' * upload control
                ' * dropdown - foreign key
                ' * dropdown - custom
                ' * autofill - foreign key
                ' * color picker

                Select Case fieldControl.ToUpper
                    Case "TXT_CHARACTER_UPLOAD"
                        fieldTypeName = "upload"
                    Case "CBO_FKEY"
                        fieldTypeName = "dropdown"
                    Case "CBO_CUSTOM"
                        fieldTypeName = "dropdown"
                    Case "TXT_PICKLIST"
                        fieldTypeName = "custom-picklist"
                    Case "TXT_COLOR_PICKER"
                        fieldTypeName = "color"
                    Case Else
                        fieldTypeName = "text"
                End Select

            Case 2 ' Numeric
                ' posibilities
                ' * Normal number field
                ' * radio buttons - values from field data?
                Select Case fieldControl.ToUpper
                    Case "OPG_OPTIONGROUP"
                        fieldTypeName = "radio-buttons"
                    Case Else
                        fieldTypeName = "number"
                End Select
            Case 3 ' date
                fieldTypeName = "date"
            Case 4 ' Text - text area
                ' Posibilities
                ' * normal text area
                ' * html ckeditor field
                Select Case fieldControl.ToUpper
                    Case "TXT_HTML"
                        fieldTypeName = "htmlarea"
                    Case Else
                        fieldTypeName = "textarea"
                End Select
            Case 5 ' Boolean
                fieldTypeName = "checkbox"
            Case Else
                fieldTypeName = "text"
        End Select

        Return fieldTypeName

    End Function

    Private Shared Function GetFieldDefaultList(fieldType As String, fieldControl As String,
                                                Optional valueXml As String = "",
                                                Optional foreignTable As String = "",
                                                Optional tableName As String = "",
                                                Optional fieldName As String = "") As List(Of KeyValue)

        Dim returnList As New List(Of KeyValue)
        Select Case fieldType
            Case "radio-buttons"
                Dim value As DocumentElement = DeserializeXML(Of DocumentElement)(valueXml)

                For Each element In value.NumericValues.OrderBy(Function(x) x.RowOrder)
                    returnList.Add(New KeyValue With {.Key = element.Code, .Value = element.Description})
                Next
            Case "dropdown"
                If fieldControl = "CBO_FKEY" Then
                    Dim options As System.Data.DataTable = GetForeignKeyOptions(foreignTable)

                    For Each row In options.Rows
                        returnList.Add(New KeyValue With {.Key = row.item("primaryKey"), .Value = row.item("descriptionField")})
                    Next
                ElseIf fieldControl = "CBO_CUSTOM" Then
                    Dim options As System.Data.DataTable = GetCustomOptions(tableName, fieldName)

                    For Each row In options.Rows
                        returnList.Add(New KeyValue With {.Key = row.item("fv_code"), .Value = row.item("fv_desc")})
                    Next
                End If
            Case "upload"
                Dim fileLocation As String = getFileLocation(tableName, fieldName)

                returnList.Add(New KeyValue With {.Key = "FileLocation", .Value = fileLocation})
        End Select

        Return returnList
    End Function

    Private Shared Function getFileLocation(tableName As String, fieldName As String) As String
        Dim qsc_file_upload_location As New Utilize.Data.QuerySelectClass
        With qsc_file_upload_location
            .select_fields = "fld_upload_target"
            .select_from = "utlz_dd_fields"
            .select_where = "dbf_name = @dbf_name and fld_name = @fld_name"
        End With
        qsc_file_upload_location.add_parameter("dbf_name", tableName)
        qsc_file_upload_location.add_parameter("fld_name", fieldName)

        Return qsc_file_upload_location.execute_query.Rows(0).Item("fld_upload_target")
    End Function

    Private Shared Function GetCustomOptions(tableName As String, fieldName As String) As DataTable
        Dim qsc_field_values As New Utilize.Data.QuerySelectClass
        With qsc_field_values
            .select_fields = "fv_code, fv_desc"
            .select_from = "utlz_dd_field_values"
            .select_where = "dbf_name = @dbf_name and fld_name = @fld_name"
            .select_order = "row_ord"
        End With
        qsc_field_values.add_parameter("dbf_name", tableName)
        qsc_field_values.add_parameter("fld_name", fieldName)

        Return qsc_field_values.execute_query
    End Function

    Private Shared Function GetForeignKeyOptions(foreignTable As String) As DataTable
        Dim primaryField As String = Nothing
        Dim descriptionField As String = Nothing
        GetTableMainFields(foreignTable, primaryField, descriptionField)

        Dim qsc_data As New Utilize.Data.QuerySelectClass
        With qsc_data
            .select_fields = primaryField + " as primaryKey, " + descriptionField + " as descriptionField"
            .select_from = foreignTable
        End With

        Return qsc_data.execute_query
    End Function

    Private Shared Sub GetTableMainFields(foreignTable As String, ByRef primaryField As String, ByRef descriptionField As String)
        Dim qsc_table_info As New Utilize.Data.QuerySelectClass
        With qsc_table_info
            .select_fields = "dbf_key_field, dbf_desc_field"
            .select_from = "utlz_dd_tables"
            .select_where = "dbf_name = @dbf_name"
        End With
        qsc_table_info.add_parameter("dbf_name", foreignTable)

        Dim dt_table_info As System.Data.DataTable = qsc_table_info.execute_query

        primaryField = dt_table_info.Rows(0).Item("dbf_key_field")
        descriptionField = dt_table_info.Rows(0).Item("dbf_desc_field")
    End Sub

    Private Shared Function DeserializeXML(Of t)(xml As String) As t
        Dim string_reader As StringReader
        Dim xml_serializer As New XmlSerializer(GetType(t))

        string_reader = New StringReader(xml)

        ' Create the new Person object from the serialization.
        Return xml_serializer.Deserialize(string_reader)
    End Function

    Private Shared Function GetPrimaryField(tableName As String) As String
        Dim qsc_order_fields As New Utilize.Data.QuerySelectClass
        With qsc_order_fields
            .select_fields = "dbf_key_field"
            .select_from = "utlz_dd_tables"
            .select_where = "dbf_name = @dbf_name"
        End With
        qsc_order_fields.add_parameter("dbf_name", tableName)
        Return qsc_order_fields.execute_query.Rows(0).Item("dbf_key_field").ToString
    End Function

    Private Shared Function GetOrderField(tableName As String) As String
        Dim qsc_order_fields As New Utilize.Data.QuerySelectClass
        With qsc_order_fields
            .select_fields = "dbf_sort_field"
            .select_from = "utlz_dd_tables"
            .select_where = "dbf_name = @dbf_name"
        End With
        qsc_order_fields.add_parameter("dbf_name", tableName)
        Return qsc_order_fields.execute_query.Rows(0).Item("dbf_sort_field").ToString
    End Function

    Private Shared Function GetTableName(frameworkObject As String) As String
        Dim qsc_table_name As New Utilize.Data.QuerySelectClass
        With qsc_table_name
            .select_fields = "dbf_name"
            .select_from = "utlz_fw_obj"
            .select_where = "obj_code = @obj_code"
        End With
        qsc_table_name.add_parameter("obj_code", frameworkObject)

        Return qsc_table_name.execute_query.Rows(0).Item("dbf_name").ToString
    End Function


    Public Class FrameworkField
        Public Property Key As String
        Public Property Description As String
        Public Property FieldType As String
        Public Property FieldListValues As List(Of KeyValue)
        Public Property FieldValue As String
        Public Property Required As Boolean = False
        Public Property ForeignTable As String
    End Class

    Public Class DetailInformation
        Public Property Id As String
        Public Property Tabs As List(Of KeyValue)
        Public Property TableName As String
        Public Property HeaderId As String
        Public Property DetailTitle As String
    End Class

    Public Class KeyValue
        Public Property Key As String
        Public Property Value As Object
    End Class

    Public Class DocumentElement
        <XmlElementAttribute(ElementName:="ValueList")>
        Public Property NumericValues As List(Of NumericValue)
    End Class

    Public Class NumericValue
        <XmlElementAttribute(ElementName:="code")>
        Public Property Code As String

        <XmlElementAttribute(ElementName:="description")>
        Public Property Description As String

        <XmlElementAttribute(ElementName:="row_ord")>
        Public Property RowOrder As Integer
    End Class
End Class


