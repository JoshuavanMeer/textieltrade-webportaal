﻿<%@ Page Language="VB" MasterPageFile="FWMasterPage.master" AutoEventWireup="false" CodeFile="sort_page.aspx.vb" Inherits="sort_page" title="Details" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../_FWAssets/Scripts/jQueryTableDND.js"></script>
    <link href="../_FWAssets/Styles/Pages/SortPage.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_content"></utilize:placeholder>
</asp:Content>

