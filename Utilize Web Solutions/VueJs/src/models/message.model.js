export class Message {
  constructor (id, type, context, behaviour, duration, messages) {
    this.id = id;
    this.type = type; // string to act as a identifier to replace currently displayed messages of the same type
    this.context = context; // string: | success | warning | error |
    this.behaviour = behaviour; // string: | flash | sticky
    this.duration = duration; // number
    this.messages = messages; // string[]
  }
}
