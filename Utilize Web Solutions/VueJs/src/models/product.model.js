import { formatPrice } from 'src/utilities/helpers';

export class Product {
  constructor (product) {
    this.id = product.id;
    this.shortDescription = product.shortDescription;
    this.subTitle = product.subTitle;
    this.longDescription = product.longDescription;
    this.unit = product.unit;
    this.properties = product.properties;
    this.targetUrl = product.targetUrl;
    this.imageUrl = product.imageUrl;
    this.images = product.images;
    this.brand = product.brand;
    this.saleUnit = product.saleUnit;
    this.prices = this.setPrices(product.prices);
    this.stock = null;
    this.customDecimals = product.customDecimals;
    this.customBooleans = product.customBooleans;
    this.customDateTimes = product.customDateTimes;
  }

  setStock (stock) {
    this.stock = stock;
  }

  setPrices (prices) {
    prices.forEach(priceObj => {
      priceObj.price = formatPrice(priceObj.price);
      priceObj.basePrice = formatPrice(priceObj.basePrice);
      priceObj.quantity = priceObj.quantity === 0 ? 1 : priceObj.quantity;
    });
    return prices;
  }
}
