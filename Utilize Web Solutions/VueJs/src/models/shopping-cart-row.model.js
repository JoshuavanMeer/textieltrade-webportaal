export class ShoppingCartRow {
  constructor (row) {
    this.product = row.product;
    this.quantity = row.quantity;
    this.rowComment = row.rowComment;
  }
}
