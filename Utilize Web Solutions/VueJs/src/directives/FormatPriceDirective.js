const FormatPriceDirecive = {
  inserted (el, { value }) {
    el.innerHTML = '€ ' + value.toFixed(2).replace('.', ',');
  }
}

export default FormatPriceDirecive;
