const state = {
  general: {
    groupProductsInProductList: 0
  },
  globalWs: {

  }
};

const getters = {
  globalWs (state) {
    return state.globalWs;
  },
  language (state) {
    return state.globalWs.language;
  },
  clientCode (state) {
    return state.globalWs.userInformation.clientCode;
  },
  customerId (state) {
    return state.globalWs.userInformation.customerId;
  },
  customerPriceList (state) {
    return state.globalWs.userInformation.customerPriceList;
  },
  userLoggedOn (state) {
    const loggedOn = state.globalWs.userInformation.userLoggedOn !== 'False';
    return loggedOn;
  },
  showPrices (state) {
    return state.globalWs.webshopSettings.showPrices;
  },
  showStock (state) {
    return state.globalWs.webshopSettings.showStock;
  },
  showOrderProduct (state) {
    return state.globalWs.webshopSettings.showOrderProduct;
  }
};

const mutations = {
  setProperty (state, obj) {
    const key = Object.keys(obj)[0];
    state[key] = obj[key];
  }
};

const actions = {
  setGlobalWs ({ commit }, globalWsSettings) {
    commit('setProperty', { globalWs: globalWsSettings });
  }
};

export default {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
};
