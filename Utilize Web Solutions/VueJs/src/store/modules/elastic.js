import axios from 'axios';
import { getUrlParams, createUrlParams } from './../../utilities/url';
import { Product } from './../../models/product.model';

const state = {
  loadingProducts: true,
  noProductsFound: false,
  products: [],
  showMobileFilters: false,
  totalProducts: 0,
  currentPage: 1,
  totalPages: 1,
  layoutType: 'blocks',
  blockSize: 4,
  rangeFrom: 0,
  pageSize: 16,
  queryString: '',
  aggregations: false,
  activeFilters: {},
  filters: [],
  pageSizes: [32, 64, 128],
  searchConfig: {
    UserId: null,
    Category: null,
    StringProperties: null,
    NumericProperties: null,
    StringAggregations: null,
    NumericAggregations: null,
    ExtraQueries: []
  }
};

const getters = {
  pageTitle (state) {
    return state.pageTitle ? state.pageTitle : state.queryString;
  },
  layoutType (state) {
    return state.layoutType;
  },
  language (state, getters, rootState, rootGetters) {
    return rootGetters.language;
  },
  blockSize (state) {
    return state.blockSize;
  },
  totalProducts (state) {
    return state.totalProducts;
  },
  totalPages () {
    return Math.ceil(state.totalProducts / state.pageSize);
  },
  currentPage (state) {
    return state.currentPage;
  },
  loadingProducts (state) {
    return state.loadingProducts;
  },
  pageRange (state, getters) {
    let to;
    let pageFillCount = state.totalProducts % state.pageSize;
    if (pageFillCount && getters.totalPages === state.currentPage) {
      to = state.rangeFrom + pageFillCount;
    } else {
      to = state.pageSize + state.rangeFrom;
    }
    return {
      from: state.rangeFrom + 1,
      to: to
    };
  },
  showMobileFilters (state, getters, rootState, rootGetters) {
    if (rootGetters.screenWidth < 991) {
      return state.showMobileFilters;
    } else {
      return true;
    }
  },
  noProductsFound (state) {
    return state.noProductsFound;
  },
  searchConfig (state) {
    return {
      UserId: null,
      Category: state.searchConfig.Category,
      StringProperties: state.activeFilters,
      NumericProperties: null,
      StringAggregations: state.searchConfig.StringAggregations,
      NumericAggregations: null,
      ExtraQueries: state.searchConfig.ExtraQueries
    }
  },
  filters (state) {
    return state.filters;
  },
  elasticProductsEndpoint (state, getters, rootState, rootGetters) {
    const endpoint = rootGetters['api/elasticProductsEndpoint'];
    const from = state.rangeFrom;
    const language = rootGetters.globalWs.language;
    const size = state.pageSize;
    const aggregations = state.aggregations;
    const query = !state.queryString ? '' : state.queryString;
    const customerId = rootGetters.userLoggedOn ? rootGetters.customerId : '';
    const client = rootGetters.clientCode;
    const priceListCode = rootGetters.customerPriceList;
    return `${endpoint}?lang=${language}&from=${from}&size=${size}&aggr=${aggregations}&query=${query}&customerId=${customerId}&customerPricelist=${priceListCode}&client=${client}`;
  }
};

const mutations = {
  setLoadingStatus (state, payload) {
    state.loadingProducts = payload;
  },
  setProducts (state, products) {
    state.products = products;
  },
  noProductsFound (state, productsFound) {
    state.noProductsFound = productsFound;
  },
  setFilters (state, aggregations) {
    const filters = [...aggregations];
    // Update new filters array with active status based on cached active filters array
    filters.forEach(filter => {
      // check for undefined as aggregations may give back new keys
      if (state.activeFilters[filter.id] !== undefined) {
        filter.value.forEach(filterValue => {
          if (state.activeFilters[filter.id].indexOf(filterValue.key) > -1) {
            filterValue.active = true;
          } else {
            filterValue.active = false;
          }
        });
      }
    });
    state.filters = [...filters];
  },
  setProductStock (state, productWithStock) {
    let index = state.products.findIndex(product => product.id === productWithStock.id);
    state.products[index] = productWithStock;
  },
  setTotalProducts (state, totalProducts) {
    state.totalProducts = totalProducts;
  },
  setTotalPages (state) {
    state.totalPages = Math.ceil(state.totalProducts / state.pageSize);
  },
  initConfig (state, config) {
    state.rangeFrom = config.From;
    state.blockSize = config.BlockSize;
    state.aggregations = config.Aggregations;
    state.queryString = !config.QueryString ? '' : config.QueryString;
    state.pageSize = config.Size;
    state.pageTitle = config.CategoryName;
    state.pageSizes = config.PageSizes;
  },
  initSearchConfig (state, searchConfig) {
    state.searchConfig = { ...searchConfig };
  },
  updateConfigWithParameters (state, config) {
    state.rangeFrom = config.from;
    state.queryString = config.queryString;
    state.pageSize = config.pageSize;
    state.currentPage = config.currentPage;
  },
  changePage (state, pageNumber) {
    state.rangeFrom = (pageNumber - 1) * state.pageSize;
    state.currentPage = pageNumber;
  },
  changeLayout (state, layoutType) {
    state.layoutType = layoutType;
  },
  changePageSize (state, pageSize) {
    state.pageSize = pageSize;
  },
  updateActiveFilters (state, { filterActive, filterId, filterValue }) {
    let activeFilters = { ...state.activeFilters };
    if (typeof activeFilters[filterId] === 'undefined') {
      activeFilters[filterId] = [];
    }
    if (filterActive) {
      activeFilters[filterId].push(filterValue);
    } else {
      const index = activeFilters[filterId].indexOf(filterValue);
      activeFilters[filterId].splice(index, 1);
    }
    state.activeFilters = activeFilters;
  },
  toggleMobileFilters (state) {
    state.showMobileFilters = !state.showMobileFilters;
  },
  updateUrl (state) {
    let filters = {};
    if (Object.entries(state.activeFilters).length !== 0) {
      Object.keys(state.activeFilters).forEach(filter => {
        filters[filter] = state.activeFilters[filter];
      });
    }
    const urlParams = createUrlParams({
      searchtext: state.queryString,
      from: state.rangeFrom,
      pageSize: state.pageSize,
      filters: filters
    });
    history.pushState({}, '', urlParams);
  }
};

const actions = {
  initElastic ({ commit, dispatch }) {
    dispatch('readoutUrl');
    dispatch('getProducts');
  },
  readoutUrl ({ commit }) {
    if (window.location.search.length) {
      const params = getUrlParams(location.href);
      const from = parseInt(params.from) ? parseInt(params.from) : 0;
      const pageSize = parseInt(params.pageSize) ? parseInt(params.pageSize) : state.pageSize;
      const queryString = params.searchtext ? params.searchtext : state.queryString;
      const currentPage = from === 0 ? 1 : from / pageSize + 1;
      const filters = params.filters;

      commit('updateConfigWithParameters', { from: from, pageSize: pageSize, queryString: queryString, currentPage: currentPage });

      if (typeof filters !== 'undefined') {
        Object.keys(filters).forEach(filter => {
          commit('updateActiveFilters', {
            filterActive: true,
            filterId: filter,
            filterValue: filters[filter][0]
          });
        });
      }
    }
  },
  getProducts ({ commit, getters, dispatch, rootGetters }) {
    commit('setLoadingStatus', true);
    axios.post(getters.elasticProductsEndpoint, getters.searchConfig)
      .then(res => {
        if (res.data.products) {
          const products = res.data.products.map(product => new Product(product));
          commit('setProducts', products);
          commit('noProductsFound', false);
          commit('setFilters', res.data.stringAggregations);
          commit('setTotalProducts', res.data.totalItems);
          commit('setLoadingStatus', false);
          commit('setTotalPages');
          commit('updateUrl');
          if (rootGetters.showStock) {
            dispatch('getProductStock', products);
          }
        } else {
          commit('noProductsFound', true);
        }
      });
  },
  updateFilters ({ commit, dispatch }, changedFilter) {
    commit('updateActiveFilters', { ...changedFilter });
    commit('changePage', 1);
    dispatch('getProducts');
  },
  getProductStock ({ commit, rootGetters }, products) {
    products.forEach(product => {
      axios.post(rootGetters['api/productStockEndpoint'], { 'productCode': product.id })
        .then(res => {
          product.setStock(res.data.d);
          commit('setProductStock', product);
        });
    });
  },
  changePage ({ commit, dispatch }, pageNumber) {
    commit('changePage', pageNumber);
    dispatch('getProducts');
  },
  changePageSize ({ commit, dispatch }, pageSize) {
    commit('changePageSize', pageSize);
    commit('changePage', 1);
    dispatch('getProducts');
  },
  changeLayout ({ commit }, layoutType) {
    commit('changeLayout', layoutType);
  },
  toggleMobileFilters ({ commit }) {
    commit('toggleMobileFilters');
  }
};

export default {
  namespaced: true,
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
};
