const state = {
  elasticProductsUrl: '/products',
  productPriceUrl: '/webshop/webshopapi/ProductListWebmethods.aspx/GetProductPrice',
  productStockUrl: '/Webshop/WebshopAPI/ProductStockWebmethods.aspx/GetProductStock',
  orderProductUrl: '/Webshop/WebshopAPI/ProductListWebmethods.aspx/OrderProduct'
};

const getters = {
  elasticProductsEndpoint (state, getters, rootState, rootGetters) {
    return rootState.settings.globalWs.endpoints.elasticEndpoint;
  },
  productStockEndpoint (state) {
    return state.productStockUrl;
  },
  orderProductEndpoint (state) {
    return state.orderProductUrl;
  },
  productPriceEndpoint (state) {
    return state.productPriceUrl;
  }
};

export default {
  namespaced: true,
  state: state,
  getters: getters
};
