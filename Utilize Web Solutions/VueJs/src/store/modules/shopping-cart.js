import axios from 'axios';

const state = {
};

const getters = {
  shoppingCart (state) {
    return state.shoppingCartRows;
  },
  totalPrice (state) {
    return state.totalPrice;
  },
  totalQuantity (state) {
    return state.totalQuantity;
  }
};

const actions = {
  addToCart ({ rootGetters }, payload) {
    let endpoint = rootGetters['api/orderProductEndpoint'];
    return new Promise((resolve, reject) => {
      axios.post(endpoint, JSON.stringify({ products: [payload] })).then(
        res => {
          const response = res.data.d;
          if (response.errorMessages) {
            alert(response.errorMessages);
          } else {
            resolve(true);

            // REFERS TO FUNCTION CALL IN CUSTOM.JS AS LONG AS SHOPPINGCART CONTROL IS NOT VUEJS
            window.updateShoppingCart(response.product_count, response.orderTotal, response.shoppingcartHtml);

            if (response.errorMessages) {
              window.updateErrorMessage(response.errorMessages);
            }

            var message = response.order_message;
            if (response.totalProductsAdded > 0) {
              window.updateOrderMessage(message);
            }
            window.set_shopping_cart_delete_buttons();
            // END CUSTOM.JS FUNCTION CALLS
          }
        })
        .catch(err => {
          // @TODO Error handling
          reject(err);
        });
    });
  }
};

const mutations = {
  addToCart (state, productRow) {
    state.shoppingCartRows.push(productRow);
  }
};

export default {
  namespaced: true,
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
};
