import { Message } from './../../models/message.model';

const state = {
  msgId: 0,
  messages: [],
  duration: [],
  timeOuts: {}
};

const getters = {
  messages: state => state.messages
};

const mutations = {
  // If a msg has the same type as a currently displayed message it will be replaced by a new message
  addMessage (state, newMessage) {
    // const index = state.messages.findIndex(message => message.type === messageConfig.type);
    // if (index) {
    //   state.messages.splice(index, 1);
    // }
    // state.messages.push(newMessage);
  },
  addStickyMessage (state, newMessage) {

  },
  deleteMessage (state, messageId) {
    const index = state.messages.findIndex(message => message.id === messageId);
    state.messages.splice(index, 1);
  }
};

const actions = {
  newMessage ({ commit }, payload) {
    console.log(payload);
    const newMessage = new Message(
      ++state.msgId,
      payload.type,
      payload.context,
      payload.behaviour,
      payload.duration,
      payload.messages
    );

    if (newMessage.behaviour === 'sticky') {

    } else {
      commit('addMessage', newMessage);
    }
  }
};

export default {
  namespaced: true,
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
