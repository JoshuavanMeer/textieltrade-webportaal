import axios from 'axios';
import { Product } from './../../models/product.model';

const state = {
  loadingProducts: false,
  products: [],
  totalProducts: 1234,
  currentPage: 1,
  totalPages: 1,
  layoutType: 'blocks',
  config: {
    From: 0,
    BlockSize: 4,
    Aggregations: false,
    QueryString: '',
    ShowPrices: false,
    Size: 16
  },
  searchConfig: {
    UserId: null,
    Category: null,
    StringProperties: null,
    NumericProperties: null,
    StringAggregations: null,
    NumericAggregations: null,
    ExtraQueries: []
  },
  filters: [],
  activeFilters: {}
};

const getters = {
  pageTitle (state) {
    return state.searchConfig.Category ? state.searchConfig.Category : state.config.QueryString;
  },
  language (state, getters, rootState, rootGetters) {
    return rootGetters.language;
  },
  filters (state) {
    return state.filters;
  },
  totalProducts (state) {
    return state.totalProducts;
  },
  totalPages () {
    return Math.ceil(state.totalProducts / state.config.Size);
  },
  currentPage (state) {
    return state.currentPage;
  },
  pageRange (state) {
    return {
      from: state.config.From,
      to: state.config.Size + state.config.From
    };
  },
  searchConfig (state) {
    return {
      UserId: null,
      Category: state.searchConfig.Category,
      StringProperties: state.activeFilters,
      NumericProperties: null,
      StringAggregations: null,
      NumericAggregations: null,
      ExtraQueries: []
    }
  },
  elasticProductsEndpoint (state, getters, rootstate, rootGetters) {
    const endpoint = rootGetters['api/elasticProductsEndpoint'];
    const from = state.config.From;
    const language = rootGetters.globalWs.language;
    const size = state.config.Size;
    const aggregations = state.config.Aggregations;
    const query = !state.config.QueryString ? '' : state.config.QueryString;
    return `${endpoint}?lang=${language}&from=${from}&size=${size}&aggr=${aggregations}&query=${query}`;
  }
};

const mutations = {
  setLoadingStatus (state, payload) {
    state.loadingProducts = payload;
  },
  setProducts (state, payload) {
    state.products = payload;
  },
  setFilters (state, aggregations) {
    const filters = aggregations;
    // Update filters with active status
    filters.forEach(filter => {
      // check for undefined as aggregations may give back new keys
      if (state.activeFilters[filter.id] !== undefined) {
        filter.value.forEach(filterValue => {
          if (state.activeFilters[filter.id].Values.indexOf(filterValue.key) > -1) {
            filterValue.active = true;
          } else {
            filterValue.active = false;
          }
        });
      }
    });
    filters.sort(function (a, b) {
      if (a.id < b.id) { return -1; }
      if (a.id > b.id) { return 1; }
      return 0;
    });
    state.filters = filters;
  },
  setProductPrice (state, productWithPrice) {
    // let index = state.products.findIndex(product => product.id === productWithPrice.id);
    // state.products[index] = productWithPrice;
  },
  setProductStock (state, productWithStock) {
    let index = state.products.findIndex(product => product.id === productWithStock.id);
    state.products[index] = productWithStock;
  },
  setTotalProducts (state, totalProducts) {
    state.totalProducts = totalProducts;
  },
  setConfig (state, config) {
    state.config = { ...config };
  },
  setSearchConfig (state, searchConfig) {
    state.searchConfig = { ...searchConfig };
  },
  changedPage (state, pageNumber) {
    state.config.From = (pageNumber - 1) * state.config.Size;
    state.currentPage = pageNumber;
  },
  changeLayout (state, layoutType) {
    state.layoutType = layoutType;
  },
  updateActiveFilters (state, { filterActive, filterId, filterValue, language }) {
    let updatedFilters = { ...state.activeFilters };

    if (typeof updatedFilters[filterId] === 'undefined') {
      updatedFilters[filterId] = {
        Language: language,
        Values: []
      };
    }
    if (filterActive) {
      updatedFilters[filterId].Values.push(filterValue);
    } else {
      const index = updatedFilters[filterId].Values.indexOf(filterValue);
      updatedFilters[filterId].Values.splice(index, 1);
    }
    state.activeFilters = updatedFilters;
  }
};

// @TODO Refactor get and update products since searchConfig is now stateless
const actions = {
  getProducts ({ commit, getters, dispatch, state, rootGetters }) {
    commit('setLoadingStatus', true);
    axios.post(getters.elasticProductsEndpoint, state.searchConfig)
      .then(res => {
        const products = res.data.products.map(product => new Product(product, getters.language));
        commit('setProducts', products);
        commit('setFilters', res.data.stringAggregations);
        commit('setTotalProducts', res.data.totalItems);
        commit('setLoadingStatus', false);
        if (rootGetters.showPrices) {
          dispatch('getProductPrices', products);
        }
        if (rootGetters.showStock) {
          dispatch('getProductStock', products);
        }
      });
  },
  updateProducts ({ commit, dispatch, getters, rootGetters }) {
    axios.post(getters.elasticProductsEndpoint, getters.searchConfig).then(res => {
      const products = res.data.products.map(product => new Product(product, getters.language));
      commit('setFilters', res.data.stringAggregations);
      commit('setTotalProducts', res.data.totalItems);
      commit('setProducts', products);
      if (rootGetters.showPrices) {
        dispatch('getProductPrices', products);
      }
      if (rootGetters.showStock) {
        dispatch('getProductStock', products);
      }
    });
  },
  updateFilters ({ commit, dispatch, getters }, changedFilter) {
    commit('updateActiveFilters', { ...changedFilter, language: getters.language });
    commit('changedPage', 1);
    dispatch('updateProducts', changedFilter);
  },
  getProductPrices ({ commit, rootGetters }, products) {
    products.forEach(product => {
      axios.post(rootGetters['api/productPriceEndpoint'], { 'productCode': product.id })
        .then(res => {
          product.setPrice(res.data.d);
        });
    });
  },
  getProductStock ({ commit, rootGetters }, products) {
    products.forEach(product => {
      axios.post(rootGetters['api/productStockEndpoint'], { 'productCode': product.id })
        .then(res => {
          product.setStock(res.data.d);
          commit('setProductStock', product);
        });
    });
  },
  changedPage ({ commit, dispatch }, pageNumber) {
    commit('changedPage', pageNumber);
    dispatch('updateProducts');
  },
  changeLayout ({ commit }, layoutType) {
    commit('changeLayout', layoutType);
  }
};

export default {
  namespaced: true,
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
};
