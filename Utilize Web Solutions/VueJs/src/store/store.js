import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

// Import modules
import messages from './modules/messages';
import api from './modules/api';
import browser from './modules/browser';
import settings from './modules/settings';
import elastic from './modules/elastic';
import shoppingCart from './modules/shopping-cart';

// Configure Axios defaults
axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {

  },
  actions: {

  },
  modules: {
    api: api,
    browser: browser,
    settings: settings,
    messages: messages,
    elastic: elastic,
    shoppingCart: shoppingCart
  }
})
