import Vue from 'vue';
import ElasticProductList from './components/webshop/elastic/elastic-product-list/ElasticProductList.vue';
import ElasticProductFilters from './components/webshop/elastic/elastic-product-filters/ElasticProductFilters.vue';
import ShoppingCartControl from './components/webshop/shopping-cart/ShoppingCartControl.vue';
import store from './store/store';

// Directives
import TranslationDirective from './directives/TranslationDirective';
import FormatPriceDirective from './directives/FormatPriceDirective';
import OutsideClickDirective from './directives/OutsideClickDirective';

Vue.config.productionTip = false

// Setup global directives
Vue.directive('translation', TranslationDirective);
Vue.directive('format-price', FormatPriceDirective);
Vue.directive('outside-click', OutsideClickDirective);

// Initializes the store with global settings from the
// masterpages that are set in the global namespace
window.initVue = new Vue({
  store: store,
  created () {
    // TODO Intialize browser sniffing store module
    this.$store.dispatch('initBrowserDetection');
    this.$store.dispatch('setGlobalWs', window.vue.globalWs);
  }
});

// Initializes elastic search module
const elasticSearch = window.vue.modules.elasticSearch;
if (elasticSearch) {
  new Vue({
    store: store,
    created () {
      this.$store.commit('elastic/initConfig', elasticSearch.config.config);
      this.$store.commit('elastic/initSearchConfig', elasticSearch.config.searchConfig);
    },
    render: h => h(ElasticProductList)
  }).$mount('#elastic-search');
}

const elasticAggrFilters = window.vue.modules.elasticAggrFilters;
if (elasticAggrFilters) {
  new Vue({
    store: store,
    render: l => l(ElasticProductFilters)
  }).$mount('#elastic-aggr-filters');
}

if (window.vue.modules.ShoppingCartControl) {
  new Vue({
    store: store,
    render: h => h(ShoppingCartControl)
  }).$mount('#shopping-cart-control');
}
