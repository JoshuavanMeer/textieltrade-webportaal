const componentList = {};

function bootStrapComponent(componentName) {
  componentList[componentName] = true;
}

export default componentList;
