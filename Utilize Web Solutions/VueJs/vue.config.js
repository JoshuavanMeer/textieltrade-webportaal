var path = require('path')
module.exports = {
  outputDir: path.resolve(__dirname, './../VueJsDist'),
  configureWebpack: {
    resolve: {
      alias: {
        src: path.resolve(__dirname, 'src')
      }
    },
    output: {
      filename: 'js' + '/[name].js',
      chunkFilename: 'js' + '/[name].js'
    }
  }
}
