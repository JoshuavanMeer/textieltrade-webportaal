﻿
Partial Class ErrorPage
    Inherits System.Web.UI.Page

    Private Sub ErrorPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim urlAddress As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_base_url() + "ErrorPageContent.aspx"

        Dim configuration As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/")

        ' Get the section.
        Dim customErrorsSection As System.Web.Configuration.CustomErrorsSection = CType(configuration.GetSection("system.web/customErrors"), System.Web.Configuration.CustomErrorsSection)

        ' Read the <customErrorssection mode.
        Dim currentMode As System.Web.Configuration.CustomErrorsRedirectMode = customErrorsSection.RedirectMode

        If currentMode = System.Web.Configuration.CustomErrorsRedirectMode.ResponseRedirect Then
            ' Als de currentMode op responseredirect staat, voer dan de responseredirect uit
            Response.Redirect(urlAddress, True)
        Else
            ' Anders gaan we op basis van de statuscode de content herschrijven
            Dim request As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(urlAddress), System.Net.HttpWebRequest)
            Dim httpResponse As System.Net.HttpWebResponse = DirectCast(request.GetResponse(), System.Net.HttpWebResponse)

            If httpResponse.StatusCode = System.Net.HttpStatusCode.OK Then
                Dim receiveStream As System.IO.Stream = httpResponse.GetResponseStream()
                Dim readStream As System.IO.StreamReader = Nothing

                If httpResponse.CharacterSet Is Nothing Then
                    readStream = New System.IO.StreamReader(receiveStream)
                Else
                    readStream = New System.IO.StreamReader(receiveStream, Encoding.GetEncoding(httpResponse.CharacterSet))
                End If

                Dim data As String = readStream.ReadToEnd()

                httpResponse.Close()
                readStream.Close()

                Dim CurrentException As HttpException = Server.GetLastError()

                Try
                    Select Case CurrentException.GetHttpCode()
                        Case 404
                            Response.StatusCode = 404
                        Case Else
                            Response.StatusCode = 500
                    End Select
                Catch ex As Exception

                End Try

                Response.Write(data)
            End If
        End If
    End Sub
End Class
