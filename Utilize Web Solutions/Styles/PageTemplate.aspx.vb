﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Request.Params("TemplCode") Is Nothing
        End If

        If ll_resume Then
            Dim lc_style As String = Utilize.Data.DataProcedures.GetValue("ucm_page_templates", "template_style", Request.Params("TemplCode"))

            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_style)
            Response.End()
        End If
    End Sub
End Class
