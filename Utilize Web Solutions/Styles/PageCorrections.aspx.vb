﻿Partial Class Styles_PageCorrections
    Inherits System.Web.UI.Page

    Public Function hexToRbgNew(ByVal Hex As String) As String
        Hex = Replace(Hex, "#", "")
        Dim red_org As String = "&H" & Hex.Substring(0, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(2, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(4, 2)
        Dim blue As Integer = CInt(blue_org)

        If blue < 125 Or red < 125 Or green < 125 Then
            If blue > 245 Then
                blue = 255
            Else
                blue += 40
            End If

            If red > 245 Then
                red = 255
            Else
                red += 40
            End If

            If green > 245 Then
                green = 255
            Else
                green += 40
            End If
        Else
            blue -= 40
            green -= 40
            red -= 40
        End If

        Dim result = "rgb(" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + ")"
        Return result
    End Function

    Public Function hexToRbgNew2(ByVal Hex As String) As String
        Dim result As String = "rgb(0,0,0)"
        If Not String.IsNullOrEmpty(Hex) Then
            Hex = Replace(Hex, "#", "")
            Dim red_org As String = "&H" & Hex.Substring(0, 2)
            Dim red As Integer = CInt(red_org)

            Dim green_org As String = "&H" & Hex.Substring(2, 2)
            Dim green As Integer = CInt(green_org)

            Dim blue_org As String = "&H" & Hex.Substring(4, 2)
            Dim blue As Integer = CInt(blue_org)

            If blue < 50 And red < 50 And green < 50 Then
                blue += 40
                red += 40
                green += 40
            Else
                If blue > 40 Then
                    blue -= 40
                Else
                    blue = 0
                End If

                If green > 40 Then
                    green -= 40
                Else
                    green = 0
                End If

                If red > 40 Then
                    red -= 40
                Else
                    red = 0
                End If
            End If
            result = "rgb(" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + ")"
        End If

        Return result
    End Function

    Public Function darken(ByVal Hex As String) As String
        Hex = Replace(Hex, "#", "")
        Dim red_org As String = "&H" & Hex.Substring(0, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(2, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(4, 2)
        Dim blue As Integer = CInt(blue_org)

        blue -= 10
        green -= 10
        red -= 10

        Dim result = "rgb(" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + ")"
        Return result
    End Function

    Public Function color_opacity(ByVal Hex As String, amount As Decimal, Optional background As Boolean = False) As String
        Dim result = ""

        Dim red_org As String = "&H" & Hex.Substring(1, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(3, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(5, 2)
        Dim blue As Integer = CInt(blue_org)

        If background Then
            If green > 250 And red > 250 And blue > 250 Then
                green = 240
                red = 240
                blue = 240
            End If
        End If

        Dim amount_string = amount.ToString.Replace(",", ".")

        result = "rgba(" + red.ToString() + ", " + green.ToString() + ", " + blue.ToString() + ", " + amount_string + ")"

        Return result
    End Function

    Private Function get_global_settings() As System.Data.DataTable
        Dim global_settings As System.Data.DataTable

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_shops") = True And Not get_shop_code().Equals("") Then
            Dim qsc_select_shop As New Utilize.Data.QuerySelectClass
            qsc_select_shop.select_fields = "*"
            qsc_select_shop.select_from = "uws_shops"
            qsc_select_shop.select_order = "shop_code"
            qsc_select_shop.select_where = "shop_code = @shop_code"
            qsc_select_shop.add_parameter("shop_code", Me.get_shop_code())

            global_settings = qsc_select_shop.execute_query()
        Else
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "*"
            qsc_select.select_from = "ucm_settings"
            qsc_select.select_order = "us_desc"

            global_settings = qsc_select.execute_query()

        End If

        Return global_settings
    End Function

    Private Function create_style_string() As String
        Dim lc_color_1 As String = String.Empty
        Dim lc_color_2 As String = String.Empty
        Dim lc_color_3 As String = String.Empty

        ' Check if inkoopcombinatie is needed
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True Then
            Dim retrieved_record_value = Me.Page.Request.Params("rec")
            If Not String.IsNullOrEmpty(retrieved_record_value) Then
                Dim retrieved_record_value_split = retrieved_record_value.Split("|")

                If retrieved_record_value_split.Length > 1 Then
                    'Check if user is logged in
                    If retrieved_record_value_split(0) = "True" Then
                        ' Check if we need to use user specific colors
                        If Utilize.Data.DataProcedures.GetValue("uws_customers", "use_colors", retrieved_record_value_split(1)) = "True" Then
                            lc_color_1 = Utilize.Data.DataProcedures.GetValue("uws_customers", "color_1", retrieved_record_value_split(1))
                            lc_color_2 = Utilize.Data.DataProcedures.GetValue("uws_customers", "color_2", retrieved_record_value_split(1))
                            lc_color_3 = Utilize.Data.DataProcedures.GetValue("uws_customers", "color_3", retrieved_record_value_split(1))
                        End If
                    End If
                End If
            End If
        End If

        Dim global_settings As System.Data.DataRow = Me.get_global_settings().Rows(0)

        Dim lc_fore_color As String = IIf(String.IsNullOrEmpty(lc_color_1), global_settings.Item("color_foreground"), lc_color_1)
        Dim lc_fore_color_changed As String = hexToRbgNew(lc_fore_color)

        Dim lc_back_color As String = global_settings.Item("color_background")
        Dim lc_background_image As String = global_settings.Item("background_image")
        Dim lc_font_color As String = global_settings.Item("color_font")

        Dim lc_button_color As String = IIf(String.IsNullOrEmpty(lc_color_1), global_settings.Item("button_color"), lc_color_1)
        Dim lc_button_color_changed As String = hexToRbgNew(lc_button_color)
        Dim lc_button_text_color As String = IIf(String.IsNullOrEmpty(lc_color_2), global_settings.Item("button_text_color"), lc_color_2)

        Dim lc_header_font_color As String = IIf(String.IsNullOrEmpty(lc_color_2), global_settings.Item("header_font_color"), lc_color_2)
        Dim lc_header_color As String = IIf(String.IsNullOrEmpty(lc_color_1), global_settings.Item("header_color"), lc_color_1)
        Dim lc_header_link_color As String = IIf(String.IsNullOrEmpty(lc_color_2), global_settings.Item("header_link_color"), lc_color_2)

        Dim lc_body_link_color As String = IIf(String.IsNullOrEmpty(lc_color_1), global_settings.Item("body_link_color"), lc_color_1)
        Dim lc_body_link_color_changed = hexToRbgNew(lc_body_link_color)

        Dim lc_footer_color As String = IIf(String.IsNullOrEmpty(lc_color_1), global_settings.Item("footer_color"), lc_color_1)
        Dim lc_footer_color_darken As String = darken(lc_footer_color)

        Dim lc_footer_font_color As String = global_settings.Item("footer_font_color")
        Dim lc_footer_link_color As String = IIf(String.IsNullOrEmpty(lc_color_2), global_settings.Item("footer_link_color"), lc_color_2)
        Dim lc_footer_link_color_changed As String = hexToRbgNew(lc_footer_link_color)

        Dim lc_style_string As New StringBuilder

        lc_style_string.Append("div.uc_training .testimonials-v6 .testimonials-info:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("border-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".btn-u.btn-u-sea-shop.btn-u-lg.btn-u-house").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("color: " + lc_button_text_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".btn-u.btn-u-sea-shop.btn-u-lg.btn-u-house:hover, .repeater-pager .btn-u:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background: " + lc_fore_color_changed + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("#floating-header").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_header_v3 .header-v5 .dropdown-menu > li > a:hover, div.uc_header_v4 .navbar-header .navbar-header-right ul.list-inline>li .language a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_header_v2 .collapse.navbar-collapse.mega-menu.navbar-responsive-collapse.target-menu").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-menu").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .badge-open span, .header-v5 .badge-open small, .header-v5 .badge-open input[type=submit].close").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append(".uc_product_list .grid-list-icons i.active,").AppendLine()
        lc_style_string.Append(".uc_product_list_v2 .grid-list-icons i.active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_text_color + "!important;").AppendLine()
        lc_style_string.Append("    background: " + lc_button_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_product_details_v2 .uc_product_order_list::before,").AppendLine()
        lc_style_string.Append(".uc_product_details_v2 .uc_product_favorites::before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_product_details_v2 .fav-orderlist-container .btn-u-sea-shop,").AppendLine()
        lc_style_string.Append(".uc_product_details_v2 .fav-orderlist-container .btn-u-sea-shop:focus").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_product_details_v2 .fav-orderlist-container .btn-u-sea-shop:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_product_details_v2 .shop-product .uc_order_product .units-container::before,").AppendLine()
        lc_style_string.Append(".uc_product_details_v2 .shop-product .uc_order_product .normal::before,").AppendLine()
        lc_style_string.Append(".uc_product_details_v2 .shop-product .uc_order_product .order > .sale_unit::before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_text_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".grid-list-icons i").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_button_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_button_text_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".grid-list-icons a:hover i").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_button_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        'Header v3
        lc_style_string.Append("div.uc_header_v3 .navbar-header .navbar-header-left,").AppendLine()
        lc_style_string.Append("div.uc_header_v3 .navbar-header .navbar-header-right ul li a,").AppendLine()
        lc_style_string.Append("div.uc_header_v3 .header-menu-right ul.uc_shopping_cart_control li span,").AppendLine()
        lc_style_string.Append("div.uc_header_v4 .navbar-header .navbar-header-left,").AppendLine()
        lc_style_string.Append("div.uc_header_v4 .navbar-header .navbar-header-right ul li a, div.uc_header_v4 div.mobile-menu .uc_languages_block .language a, div.uc_header_v4 div.mobile-menu .uc_languages_block .language a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_header_v3 .header-menu-right ul.uc_shopping_cart_control li .shopping_cart_badge").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append("div.uc_header_v3 .navbar-header .navbar-header-right .uc_languages_block ul.right-header>li>a:hover,").AppendLine()
        lc_style_string.Append("div.uc_header_v4 .navbar-header .navbar-header-right .uc_languages_block ul.right-header>li>a:hover,").AppendLine()
        lc_style_string.Append("div.uc_header_v3 .header-menu .search:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_header_v3 .navbar-header ul.mobile_icon li a:before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        'If a font or transform has been selected for the headers, add this to the css
        If (Not String.IsNullOrEmpty(global_settings.Item("header_font")) And Not global_settings.Item("header_font") = "OPEN SANS") Or global_settings.Item("header_txt_transform") Then
            lc_style_string.Append("h1, h2, h3, h4, h5,").AppendLine()
            lc_style_string.Append("h1 a, h2 a, h3 a, h4 a, h5 a, h6 a,").AppendLine()
            lc_style_string.Append("h1 span, h2 span, h3 span, h4 span, h5 span,").AppendLine()
            lc_style_string.Append(".uc_single_checkout_v2 .step-no,").AppendLine()
            lc_style_string.Append(".shop-product-prices span").AppendLine()
            lc_style_string.Append("{").AppendLine()
            If Not String.IsNullOrEmpty(global_settings.Item("header_font")) And Not global_settings.Item("header_font") = "OPEN SANS" Then
                lc_style_string.Append("font-family: " + Utilize.Data.DataProcedures.GetValue("utlz_dd_field_values", "fv_desc", global_settings.Item("header_font"), "fv_code") + ", Arial, Helvetica, sans-serif;").AppendLine()
            End If
            If global_settings.Item("header_txt_transform") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If
            lc_style_string.Append("}").AppendLine()
        End If

        'If a font or transform has been selected for the header menu buttons, add this to the css
        If (Not String.IsNullOrEmpty(global_settings.Item("header_btns_font")) And Not global_settings.Item("header_btns_font") = "OPEN SANS") Or global_settings.Item("header_btns_trans") Then
            lc_style_string.Append(".uc_menu_flyout_horizontal ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v2 ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v3 ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v4 ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v5 ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v6 ul.nav.navbar-nav > li > a,").AppendLine()
            lc_style_string.Append(".uc_menu_flyout_horizontal_v6 ul.nav.navbar-nav .mega-menu-heading a").AppendLine()
            lc_style_string.Append("{").AppendLine()
            If Not String.IsNullOrEmpty(global_settings.Item("header_btns_font")) And Not global_settings.Item("header_btns_font") = "OPEN SANS" Then
                lc_style_string.Append("font-family: " + Utilize.Data.DataProcedures.GetValue("utlz_dd_field_values", "fv_desc", global_settings.Item("header_btns_font"), "fv_code") + ", Arial, Helvetica, sans-serif;").AppendLine()
            End If
            If global_settings.Item("header_btns_trans") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If
            lc_style_string.Append("}").AppendLine()
        End If

        'If a font or transform has been selected for the buttons, add this to the css
        If (Not String.IsNullOrEmpty(global_settings.Item("buttons_font")) And Not global_settings.Item("buttons_font") = "OPEN SANS") Or global_settings.Item("buttons_transform") Then
            lc_style_string.Append("input[type='button'],").AppendLine()
            lc_style_string.Append("input[type='submit'],").AppendLine()
            lc_style_string.Append("a.btn-u-sea-shop,").AppendLine()
            lc_style_string.Append("a.banner_button_link").AppendLine()
            lc_style_string.Append("{").AppendLine()
            If Not String.IsNullOrEmpty(global_settings.Item("buttons_font")) And Not global_settings.Item("buttons_font") = "OPEN SANS" Then
                lc_style_string.Append("font-family: " + Utilize.Data.DataProcedures.GetValue("utlz_dd_field_values", "fv_desc", global_settings.Item("buttons_font"), "fv_code") + ", Arial, Helvetica, sans-serif;").AppendLine()
            End If
            If global_settings.Item("buttons_transform") Then
                lc_style_string.Append("text-transform: uppercase;").AppendLine()
            End If
            lc_style_string.Append("}").AppendLine()
        End If

        'If a background image has been selected, add this as background to the body
        If Not String.IsNullOrEmpty(lc_background_image) Then
            lc_style_string.Append("body").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-image: url('/" + lc_background_image + "')!important;").AppendLine()
            lc_style_string.Append("background-repeat: no-repeat!important;").AppendLine()
            lc_style_string.Append("background-position: top center!important;").AppendLine()
            lc_style_string.Append("}").AppendLine()
        End If

        'Header v2
        lc_style_string.Append("@media (max-width: 991px){").AppendLine()
        lc_style_string.Append(".header .navbar-toggle").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("    border-color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .navbar-toggle .glyphicon, .header .navbar-toggle .fa").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .navbar-nav > .active > a, .header .navbar-nav > .active > a:hover, .header .navbar-nav > .active > a:focus, .header .nav > li > .search:hover, .header .nav > li > .search").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("    color: white !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .navbar-nav > li > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append(".header .navbar-toggle:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + darken(lc_header_link_color) + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .nav .search-open, .nav>li>a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("@media (min-width: 992px){").AppendLine()
        lc_style_string.Append(".header .navbar-nav > .active > a, .header .navbar-nav > .active > a:hover, .header .navbar-nav > li > a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("    border-bottom-color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .dropdown-menu .active > a, .header .dropdown-menu .active > a:hover, .header .dropdown-menu > a, .header .dropdown-menu li > a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(" #search_button").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .navbar-nav > li > a, .uc_header_v2 .fa, .uc_header_v2 .usp_text").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .dropdown-menu li a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v2 a:hover .fa").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header .dropdown-menu").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-top-color: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("    border-bottom-color: " + lc_header_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    Background:" + lc_header_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_sizeview_group label").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".panel-collapse .panel-body ul.nav li ul li a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_back_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".panel-body .nav > li > a:hover, .panel-body .nav > li > a:focus, .panel-body .nav > li > a:active, .panel-collapse .panel-body ul.nav li.active > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_back_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".panel-body ul.dropdown-menu.active li.li-width a:hover, ul.dropdown-menu.active li.li-width a:focus, ul.dropdown-menu.active li.li-width a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color + "!important;").AppendLine()
        lc_style_string.Append("    color: " + lc_back_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_call_back .input-group input::-webkit-input-placeholder {color: " + lc_font_color + ";}").AppendLine()

        lc_style_string.Append("div.uc_categories_overview .product-category").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("ul.list-inline li").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("p").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 li.subtotal span, .header-v5 li.subtotal .subtotal-cost").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        If Not global_settings.Item("header_id") = 5 Then
            lc_style_string.Append("@media (max-width: 991px){").AppendLine()
            lc_style_string.Append("div.uc_header_v1.header-v5 .navbar-default .navbar-nav > .active > a, ").AppendLine()
            lc_style_string.Append("div.uc_header_v1.header-v5 .navbar-default .navbar-nav > .active > a:hover, ").AppendLine()
            lc_style_string.Append("div.uc_header_v1.header-v5 .navbar-default .navbar-nav > .active > a:focus, ").AppendLine()
            lc_style_string.Append("div.uc_header_v3 .header-v5 .navbar-default .navbar-nav > .active > a, ").AppendLine()
            lc_style_string.Append("div.uc_header_v3 .header-v5 .navbar-default .navbar-nav > .active > a:hover, ").AppendLine()
            lc_style_string.Append("div.uc_header_v3 .header-v5 .navbar-default .navbar-nav > .active > a:focus,").AppendLine()
            lc_style_string.Append("div.uc_header_v4 .header-v5 .navbar-default .navbar-nav > .active > a, ").AppendLine()
            lc_style_string.Append("div.uc_header_v4 .header-v5 .navbar-default .navbar-nav > .active > a:hover, ").AppendLine()
            lc_style_string.Append("div.uc_header_v4 .header-v5 .navbar-default .navbar-nav > .active > a:focus").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
            lc_style_string.Append("    background: " + lc_header_link_color + "!important;").AppendLine()
            lc_style_string.Append("}").AppendLine()
            lc_style_string.Append("}").AppendLine()
        End If

        lc_style_string.Append(".wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append(".footer-v4 a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_footer_link_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".product-description h4.title-price a:hover, .product-description h4.title-price a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".wizard > .steps .current a:hover, .btn-default:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color_changed + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".breadcrumb-v5 li a:hover, .breadcrumb-v5 li a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color_changed + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("a:hover, a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_body_link_color_changed + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".btn-u:hover, .btn-u:focus, .btn-u.btn-u-sea-shop:hover, .btn-u.btn-u-sea-shop:focus").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_button_color_changed + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".footer-v4 a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_footer_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .navbar-default, .header-v5 .navbar-default ul li ul").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".nav .header, .header-v5 .navbar-default .navbar-nav > li > a, .header-v5 .dropdown-menu > li > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("@media (max-width: 991px){ .header-v5 .navbar-default .navbar-nav .open .dropdown-menu > li > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".footer").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_footer_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()


        lc_style_string.Append(".copyright").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_footer_color_darken + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".footer-v4 .footer p, .footer-v4 .copyright p, .footer-v4 h2").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_footer_font_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("body, .service-box-v1 .service-block, .panel, .news-v3, .sky-form, .sky-form fieldset, .shop-product, input[type=text], .sky-form .input input:not([type='radio']), .sky-form .textarea, .sky-form .select, textarea, .shopping-cart .billing-info-inputs").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_back_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".quantity-multiplier, .select, .pagination>li>a, div.uc_category_menu_collapse div.mobile-heading, .form-control, .input-group-addon, .form-control[disabled]").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_back_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".navbar-default .navbar-nav>.open>a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_back_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".shop-product .quantity-button, .pagination>li>a, .service-block-default p, .wizard > .steps .done a, .wizard > .steps .current p, .wizard > .steps .done p").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".shopping-cart .total-result-in span, .header-v5 .navbar-default .navbar-nav > li > a:focus, .header-v5 .navbar-default .navbar-toggle").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".thumbnail-style:hover h3 a, .thumbnail-style h3 a, .service-block-default h2, div.uc_categories_overview .category-name").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .navbar-default .navbar-toggle .icon-bar").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("a, .list-unstyled a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_body_link_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".wizard > .steps .disabled .number").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".shop-subscribe, .wizard > .steps .current a, .wizard > .steps .done a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".pagination li a:hover, .pagination li a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".pagination > .active > a, .pagination > .active > a:hover, .pagination > .active > a:focus, .pagination > .active > a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("    background-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".breadcrumb-v5 li.active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .navbar-default .navbar-nav > li:hover > a, .header-v5 .navbar-default .navbar-nav > li:active > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("@media (min-width: 992px) {").AppendLine()
        lc_style_string.Append("    .header-v5 .navbar-default .navbar-nav > li:hover > a, .header-v5 .navbar-default .navbar-nav > li:active > a, .header-v5 .navbar-default .navbar-nav > li.active > a").AppendLine()
        lc_style_string.Append("    {").AppendLine()
        lc_style_string.Append("        border-top: solid 2px " + lc_header_link_color + ";").AppendLine()
        lc_style_string.Append("        color: " + lc_header_link_color + "!important;").AppendLine()
        lc_style_string.Append("    }").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .dropdown-menu").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-top: solid 2px " + lc_header_link_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .dropdown-menu li a:hover,  .header-v5 .dropdown-menu li.dropdown-submenu:hover > a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_link_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .dropdown-menu li.active a, ").AppendLine()
        lc_style_string.Append(".header-v5 .navbar-default .navbar-nav > .active > a, .header-v5 .navbar-default .navbar-nav > .active > a:hover, .header-v5 .navbar-default .navbar-nav > .active > a:Focus").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_color + " !important;").AppendLine()
        lc_style_string.Append("    color: " + lc_header_link_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("#topcontrol:hover, #topcontrol:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .shop-badge.badge-icons i, .header-v5  h2 a, .header-v5 p, .header-v5 li, .header-v5 label").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_header_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("h1,h2,h3,h4,h5,h6, body, fieldset, .select, .panel-title, .li-width>a, .sky-form .input input, .sky-form .textarea, input[type=submit], .quantity-multiplier, h4 a, .product-description .gender").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".usp_text, .thumbnail-style h3 a, .form-control, .fa").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("div.uc_usps ul").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_font_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".service-box-v1 li").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-top: dotted 1px " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".service-box-v1 p:after").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".thumbnail-style a.btn-more").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".thumbnail-style a.btn-more:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    box-shadow: 0 0 0 2px " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".thumbnail h3 a:hover, .thumbnail-style h3 a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".news-v3 h2 a:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".banner_w_t i, .banner_w_t b").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".filter-by-block h1").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".btn-u.btn-u-sea-shop, .btn-u, .repeater-pager .btn-u.active , .rgba-banner").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_button_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".btn-u.btn-u-sea-shop, .repeater-pager .btn-u.active, .repeater-pager .btn-u:hover, .btn-u.btn-u-sea-shop:hover, .repeater-pager a:hover::before, .btn-u.btn-u-sea-shop:focus, .btn-u, .btn-u:hover, .btn-u:focus, .rgba-banner").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_button_text_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".content-boxes-in-v4 h2 a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".service-block-v7 i").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("h1").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-bottom: 2px solid " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".footer-v4 h1, .footer-v4 h2").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    border-bottom: 2px solid " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("    color: " + lc_footer_font_color + " !important;").AppendLine()
        lc_style_string.Append("    display: inline;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".footer-v4 h1::after, .footer-v4 h2::after").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    display: block;").AppendLine()
        lc_style_string.Append("    padding-bottom: 1px;").AppendLine()
        lc_style_string.Append("    content: ' ';").AppendLine()
        lc_style_string.Append("    overflow: visible;").AppendLine()
        lc_style_string.Append("    border-bottom: 1px dotted #e4e9f0;").AppendLine()
        lc_style_string.Append("    margin-bottom: 10px;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .shop-badge span.badge-sea").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("    background: " + lc_header_link_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".header-v5 .badge-open").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("      border-top: 2px solid " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("      background:" + lc_header_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("      background-color: " + lc_fore_color + " !important;").AppendLine()
        lc_style_string.Append("      text-decoration: none;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".da-slide h2 i {").AppendLine()
        lc_style_string.Append("background-color: " + lc_fore_color + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        'Header-v6 styles
        lc_style_string.Append(".uc_header_v6 .topbar").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("topbar_backgr_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .account-handler li:hover a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .languages-handler li:hover a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + hexToRbgNew2(global_settings.Item("topbar_backgr_col")) + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .topbar a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .topbar ::before,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .topbar a:hover,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .topbar ::before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("topbar_font_col") + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .account-handler").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("border-left: 1px solid " + global_settings.Item("topbar_separator_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .account-handler a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .languages-handler a").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("border-right: 1px solid " + global_settings.Item("topbar_separator_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .center-bar-bg").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("centerbar_backgr_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .fi-search-icon").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("searchbar_icon_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .searchtype label").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("cart_icon_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("@media (min-width: 992px)").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append(".uc_header_v6 .uc_content_search_v6 .form-control").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("searchbar_backgr_col") + ";").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("searchbar_font_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .fa.fa-shopping-cart::before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("cart_icon_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .cart_total_quantity.shopping_cart_badge").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("cartbadge_backgr_col") + ";").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("cartbadge_font_col") + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("@media (min-width: 992px)").AppendLine()
        lc_style_string.Append("{").AppendLine()

        lc_style_string.Append(".uc_header_v6 .menu-flyout-handler").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("bottombar_backgr_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .header-v5 .dropdown-menu li a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .header-v5 .dropdown > ::after,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .header-v5 .navbar-default .navbar-nav > li:hover > a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .header-v5 .navbar-default .navbar-nav > li > a,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .header-v5 .navbar-default .navbar-nav > li > a:hover,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .header-v5 .navbar-default .navbar-nav > li > a:focus").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("bottombar_font_col") + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .uc_usps i::before,").AppendLine()
        lc_style_string.Append(".uc_header_v6 .usps-top-bar > .uc_usps i::before").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("usp_col") + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 .usp_text").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("usp_font_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 button.navbar-toggle").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("mob_menu_backgr_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 button.navbar-toggle span").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + global_settings.Item("mob_menu_font_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_header_v6 button.navbar-toggle .icon-bar-group span").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background-color: " + global_settings.Item("mob_menu_icon_col") + ";").AppendLine()
        lc_style_string.Append("}").AppendLine()

        If global_settings.Item("header_id") = 5 Then
            lc_style_string.Append(".uc_single_checkout_v2 thead").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background: " + global_settings.Item("bottombar_backgr_col") + " !important;").AppendLine()
            lc_style_string.Append("color: " + global_settings.Item("bottombar_font_col") + " !important;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            Dim qsc_query As New Utilize.Data.QuerySelectClass
            qsc_query.select_fields = "*"
            qsc_query.select_from = "ucm_structure"
            qsc_query.select_where = "us_parent = ''"
            qsc_query.select_order = "row_ord"

            Dim dt_structure As System.Data.DataTable = qsc_query.execute_query()
            Dim ln_main_menu_items As Int32 = dt_structure.Rows.Count + dt_structure.Select("category_menu_type = '1'").Length

            'We need a number that makes sure that the ith menu item is mapped to the correct datarow
            Dim ln_row_to_nthchild_mapping As Int32 = 1
            Dim ll_bool As Boolean = True

            'Apply styling settings for the specific menu buttons
            For index = 1 To ln_main_menu_items
                If dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("specify_color") Then
                    lc_style_string.Append("@media (min-width: 992px) {").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ")").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("background-color: " + dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("background_color") + "!important;").AppendLine()
                    lc_style_string.Append("}").AppendLine()

                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + "):hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").active:not(.hidden-lg),").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + "):focus,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li:hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li.dropdown-submenu:hover > a,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li a:hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li.active > a").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("background-color: " + hexToRbgNew2(dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("background_color")) + "!important;").AppendLine()
                    lc_style_string.Append("}").AppendLine()

                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ") .dropdown-menu").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("background-color: " + dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("background_color") + ";").AppendLine()
                    lc_style_string.Append("}").AppendLine()

                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").mega-menu-fullwidth .dropdown-menu li a:hover").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("color: " + dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("background_color") + "!important;").AppendLine()
                    lc_style_string.Append("}").AppendLine()

                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ") a,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + "):hover a,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li.active:nth-child(" + index.ToString + ") a,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ") a::after").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("color: " + dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("font_color") + "!important;").AppendLine()
                    lc_style_string.Append("}").AppendLine()
                    lc_style_string.Append("}").AppendLine()
                Else
                    lc_style_string.Append("@media (min-width: 992px) {").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + "):hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").active:not(.hidden-lg),").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + "):focus,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li:hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li.dropdown-submenu:hover > a,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li a:hover,").AppendLine()
                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ").dropdown:not(.mega-menu-fullwidth) .dropdown-menu li.active > a").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("background-color: " + hexToRbgNew2(global_settings.Item("bottombar_backgr_col")) + "!important;").AppendLine()
                    lc_style_string.Append("}").AppendLine()

                    lc_style_string.Append(".uc_header_v6 .nav.navbar-nav > li:nth-child(" + index.ToString + ") .dropdown-menu").AppendLine()
                    lc_style_string.Append("{").AppendLine()
                    lc_style_string.Append("background-color: " + global_settings.Item("bottombar_backgr_col") + ";").AppendLine()
                    lc_style_string.Append("}").AppendLine()
                    lc_style_string.Append("}").AppendLine()
                End If

                If ll_bool Then
                    If dt_structure.Rows(index - ln_row_to_nthchild_mapping).Item("category_menu_type") = 1 Then
                        'If there is a mega menu, two li elements are created in the menu, so the number that represents the mapping needs to be adjusted
                        ln_row_to_nthchild_mapping += 1

                        'This boolean makes sure that the ln_row_to_nthchild_mapping number is incremented only once per mega menu element
                        ll_bool = False
                    End If
                Else
                    ll_bool = True
                End If
            Next
        End If
        'End header-v6 styles

        lc_style_string.Append(".uc_single_checkout_v2 .step-no").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background: " + lc_button_color + "!important;").AppendLine()
        lc_style_string.Append("color: " + lc_button_text_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_single_checkout_v2 .uc_coupon_code .btn-apply").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("background: " + lc_button_color + "!important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_single_checkout_v2 .uc_coupon_code .btn-apply span").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + lc_button_text_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_placeholder.uc_product_block .uc_order_product .btn-u-sea-shop:hover").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("border-color: " + lc_button_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_category_menu_collapse.advanced .panel-body ul.dropdown-menu.active li.li-width a:hover,").AppendLine()
        lc_style_string.Append(".uc_category_menu_collapse.advanced ul.dropdown-menu.active li.li-width a:focus,").AppendLine()
        lc_style_string.Append(".uc_category_menu_collapse.advanced ul.dropdown-menu.active li.li-width a:active").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + lc_fore_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        lc_style_string.Append(".uc_placeholder.uc_product_block .uc_order_product div.normal::before,").AppendLine()
        lc_style_string.Append(".uc_placeholder.uc_product_block .uc_order_product div.sale_unit::before,").AppendLine()
        lc_style_string.Append(".uc_product_list_v2 .clickthrough-btns .uc_product_block .uc_order_product .btn-u-sea-shop").AppendLine()
        lc_style_string.Append("{").AppendLine()
        lc_style_string.Append("color: " + lc_button_color + " !important;").AppendLine()
        lc_style_string.Append("}").AppendLine()

        Return lc_style_string.ToString()
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim lc_style_string As String = Me.create_style_string()

        Response.Clear()
        Response.ContentType = "text/css"
        Response.AddHeader("Content-Disposition", "inline")
        Response.Write(lc_style_string)
        Response.End()
    End Sub

    Public Function get_shop_code() As String
        ''de shop_code retourneert op basis van de url welke je op kunt halen via de cms_procedures.get_domain oid
        Dim qsc_field_shop As New Utilize.Data.QuerySelectClass
        qsc_field_shop.select_fields = "shop_code"
        qsc_field_shop.select_from = "uws_shops"
        qsc_field_shop.select_order = "shop_code"
        qsc_field_shop.select_where = "shop_url = @shop_url"
        qsc_field_shop.add_parameter("shop_url", CMS.cms_procedures.get_domain())

        Dim dt_shop As System.Data.DataTable = qsc_field_shop.execute_query()
        Dim shop_code As String = ""
        If dt_shop.Rows.Count > 0 Then
            shop_code = dt_shop.Rows(0).Item("shop_code")
        End If
        Return shop_code
    End Function
End Class
