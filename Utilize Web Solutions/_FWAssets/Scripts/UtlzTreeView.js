﻿function CheckboxClicked(chkevent) {
    // Tested on IE7 and Firefox 3
    var obj;
    try {
        obj = window.event.srcElement;
    }
    catch (Error) {
        //Using a browser that cannot retrieve srcElement i.e. Firefox 3
        //obj will not have been set, obj = null
    }

    if (obj != null) {
        //obj is not null, probably using IE
        if (obj.tagName == "INPUT" && obj.type == "checkbox") {
            __doPostBack(chkevent.srcElement.id, "");
        }
    }
    else {
        //obj is null, probably using Firefox
        if (chkevent != null) {
            if (chkevent.target.toString() == "[object HTMLInputElement]") {
                __doPostBack(chkevent.srcElement.id, "");
            }
        }
    }
}

function scrollToSelected(id) {
    var links = document.getElementById(id).getElementsByTagName('A');

    for (var a = 0; a < links.length; a++) {
        var style = (links[a].currentStyle) ? links[a].currentStyle['fontWeight'] : document.defaultView.getComputedStyle(links[a], null).getPropertyValue('font-weight');
        if (((style + "").toLowerCase() == 'bold' || (style + "").toLowerCase() == '700') && links[a].innerHTML.substring(0, 4).toLowerCase() != '<img') {
            links[a].scrollIntoView(true)
        }

    }
}

function ScrollTo(NodeId, DivId) {
    //the client-side ID of the SelectedNode of TreeView1 is 
    //stored in an hidden input named TreeView1_SelectedNode
    var inpSelectedNode = theForm.elements['ctl00_ContentPlaceHolder1_ctl20_SelectedNode'];

    if (inpSelectedNode.value != "") {
        var objScroll = document.getElementById(inpSelectedNode.value);
        //my treeview is contained in a scrollable div element

        var objDiv = document.getElementById(DivId);
        objDiv.scrollTop = findPosY(objScroll);
    }
}

function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;

}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;

    return curleft;
}