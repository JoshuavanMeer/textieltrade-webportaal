			// jQuery Alert Dialogs Plugin
			//
			// Version 1.1
			//
			// Cory S.N. LaViska
			// A Beautiful Site (http://abeautifulsite.net/)
			// 14 May 2009
			//
			// Visit http://abeautifulsite.net/notebook/87 for more information
			//
			// Usage:
			//		jAlert( message, [title, callback] )
			//		jConfirm( message, [title, callback] )
			//		jPrompt( message, [value, title, callback] )
			// 
			// History:
			//
			//		1.00 - Released (29 December 2008)
			//
			//		1.01 - Fixed bug where unbinding would destroy all resize events
			//
			// License:
			// 
			// This plugin is dual-licensed under the GNU General Public License and the MIT License and
			// is copyright 2008 A Beautiful Site, LLC. 
			//
			(function ($) {

			    $.alerts = {
			        // These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time

			        // Public methods
			        alert: function (message, title, callback) {
			            message = message.replace(/\n/g, '<br />');

			            var modal;
                        
			            modal = '<div id="myAlertModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">' +
                                    '<div class="modal-dialog modal-md">' +
                                        '<div class="modal-content">' +
                                            '<div class="modal-header">' +
                                                '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>' +
                                            '</div>' +
                                            '<div class="modal-body alert alert-info" id="content"></div>' +
                                            '<div class="modal-footer">' +
                                                '<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>'
			            
			            if ($("#myAlertModal").length) {

			            }
			            else {
			                $("BODY").append(modal);
			            }
                        
			            $('#myAlertModal #content').text(message);
                        $('#myAlertModal').modal('show');
			        },


			        confirm: function (message, title, callback) {
			            message = message.replace(/\n/g, '<br />');

			            var modal;

			            modal = '<div id="myConfirmModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">' +
                                    '<div class="modal-dialog modal-md">' +
                                        '<div class="modal-content">' +
                                            '<div class="modal-header">' +
                                                '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>' +
                                            '</div>' +
                                            '<div class="modal-body alert alert-danger" id="content"></div>' +
                                            '<div class="modal-footer">' +
                                                '<button type="button" class="btn btn-default" data-dismiss="modal" id="button_ok">Ok</button>' +
                                                '<button type="button" class="btn btn-primary" data-dismiss="modal" id="button_cancel">Annuleren</button>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>'

			            if ($("#myConfirmModal").length) {
			                
			            }
                        else
			            {
			                $("BODY").append(modal);
                        }


			            $("#button_ok").off("click");
			            $("#button_ok").click(function (result) {
			                if (callback) callback(result);
			            });

			            $('#myConfirmModal #content').text(message);
			            $('#myConfirmModal').modal('show');
			        },
			    }
			
			    // Shortuct functions
			    jAlert = function (message, title, callback) {
			        $.alerts.alert(message, title, callback);
			    }

			    jConfirm = function (message, title, callback) {
			        $.alerts.confirm(message, title, callback);
			    };
			})(jQuery);