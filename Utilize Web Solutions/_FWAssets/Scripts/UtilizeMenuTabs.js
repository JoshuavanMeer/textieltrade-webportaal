var JSTabs = {
    //html locatie waar de tabs aangemaakt worden
    tabMenu: null,
    //html locatie waar de tab bladen zelf aangemaakt worden
    tabContent: null,
    //pointer naar de actieve tab blad
    current: null,
    //Welke teken gebruikt word in de sluit knop.
    closeButtonString: 'X',
    //koppel functies
    switchTab: function (tab) {
        if (this.current) {
            this.current.style.display = 'none';
            this.current.main.className = this.current.main.className.replace(/[\s]*open_tab/g, '');
            this.current.tabButton.className = this.current.tabButton.className.replace(/[\s]*btn-primary/g, ' btn-default');
        };

        tab.style.display = 'block';
        this.current = tab;
        // this.current.main.className += ' open_tab';
        this.current.tabButton.className = 'btn btn-primary';
    },
    newTab: function () {	//Aanmaken van de benodigde html elementen
        var tabButton = document.createElement('a');
        var tabWrapper = document.createElement('div');
        var tabFrame = document.createElement('iframe');
        tabFrame.frameBorder = 0

        var tabClose = document.createElement('span');
        tabClose.className = 'close_button';

        //koppelingen voor onderlinge communicatie
        this.tabWrapper = tabButton.tabWrapper = tabWrapper;
        tabWrapper.tabButton = this.tabButton = tabClose.tabButton = tabButton;
        tabButton.system = tabClose.system = this.system;
        tabClose.tabWrapper = tabWrapper;
        tabWrapper.main = tabClose.main = tabButton.main = this;

        //Events koppelen
        this.onclick = tabButton.onclick = this.system.tabFocus;
        tabClose.onclick = this.system.tabClose;
        //text selectie op de knoppen uitschakelen , werkt alleen niet goed in IE 9 dit is niet een bug maar een vaag gekozen aanpassing.
        this.onselectstart = tabClose.onselectstart = this.onmousedown = tabClose.onmousedown = function () { return false; };

        //code
        tabButton.className = 'btn';

        this.active = tabButton.active = true;
        tabFrame.src = this.href;
        tabFrame.border = '0';
        tabButton.innerHTML = '<span class="text">' + this.innerHTML + '</span>';
        tabClose.innerHTML = this.system.closeButtonString;

        // this.className += ' active_tab'
        this.system.switchTab(tabWrapper);

        //koppelen
        tabWrapper.appendChild(tabFrame)
        tabButton.appendChild(tabClose);

        this.system.tabMenu.appendChild(tabButton)
        this.system.tabContent.appendChild(tabWrapper)
        //Dit is nodig om de standaart functionaliteit van de link uit te schakelen.
        return false;
    },
    tabFocus: function () {
        if (this.active) {
            this.system.switchTab(this.tabWrapper);
        } else {
            this.active = true;
        };
        return false;

    },
    tabClose: function () {
        this.main.onclick = this.main.system.newTab;

        try {
            if (this.parentNode.nextSibling && this.parentNode.className.match(/btn/)) {
                this.parentNode.nextSibling.onclick();
            } else if (this.parentNode.previousSibling && this.parentNode.className.match(/btn/)) {
                this.parentNode.previousSibling.onclick();
            }
        }
        catch (err)
        {
        }

        this.system.tabMenu.removeChild(this.tabButton);
        this.system.tabContent.removeChild(this.tabWrapper);
        // this.main.className = this.main.className.replace(/[\s]*active_tab/g, '').replace(/[\s]*open_tab/g, '');
        this.tabButton.active = false;
        return false;
    },
    //opbouwen van de functionaliteit
    start: function () {
        var links_tmp = document.body.getElementsByTagName('a');
        this.tabContent = document.getElementById('framecontent');
        this.tabMenu = document.getElementById('frametabs');
        for (var a = 0; a < links_tmp.length; a++) {
            if (links_tmp[a].className.match(/tab_link/g)) {
                links_tmp[a].system = this;
                links_tmp[a].onclick = this.newTab;
            }
        }
    },
    init: function () {
        var This = this;
        if (typeof (window.addEventListener) != 'undefined') {
            window.addEventListener('load', function () { This.start(); }, false);
        } else if (typeof (window.attachEvent) != 'undefined') {
            window.attachEvent('onload', function () { This.start() });
        }
    }
};  JSTabs.init();