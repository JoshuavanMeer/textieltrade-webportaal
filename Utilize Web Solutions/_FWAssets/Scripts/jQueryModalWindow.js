/// <reference path="jQueryModalWindow.js" />
var modalWindow = {
    parent: "body",
    windowId: null,
    spacingT: null,
    spacingL: null,
    marginT: null,
    marginL: null,
    border: null,
    content: null,
    height: null,
    width: null,
    close: function () {
        $(".modal-window").remove();
        $(".modal-overlay").remove();
    },
    open: function () {
        $("div.flashobject").css("visibility", "hidden");

        var modal = "";
        modal += "<div class=\"modal-overlay\"></div>";
        modal += "<div id=\"" + this.windowId + "\" class=\"modal-window\" style=\"position:fixed; width:" + this.width + "; height: " + this.height + "; left: " + this.spacingL + "; top: " + this.spacingT + "; margin-left: " + this.marginL + "; margin-top: " + this.marginT + "; \">";
        modal += this.content;
        modal += "</div>";

        $(this.parent).append(modal);
    }
};

var openWindow = function (source, width, height, closebutton, reload) {
    var lnPercentage = 90;
    var border = 1;

    if (width != null && height != null) {
        if (width != 0 && height != 0) {
            modalWindow.height = height + "px";
            modalWindow.width = width + "px";

            modalWindow.spacingT = "50%";
            modalWindow.spacingL = "50%";

            modalWindow.marginT = "-" + Math.round(height / 2) + "px";
            modalWindow.marginL = "-" + Math.round(width / 2) + "px";
            modalWindow.border = 1;
        }
        else {
            modalWindow.height = 100 + "%";
            modalWindow.width = 100 + "%";

            modalWindow.spacingT = Math.round((100 - 100) / 2) + "%";
            modalWindow.spacingL = Math.round((100 - 100) / 2) + "%";

            modalWindow.marginT = "0px";
            modalWindow.marginL = "0px";
            modalWindow.border = 0;
        }
    }
    else {
        modalWindow.height = lnPercentage + "%";
        modalWindow.width = lnPercentage + "%";

        modalWindow.spacingT = Math.round((100 - lnPercentage) / 2) + "%";
        modalWindow.spacingL = Math.round((100 - lnPercentage) / 2) + "%";

        modalWindow.marginT = "0px";
        modalWindow.marginL = "0px";
        modalWindow.border = 1;
    }
    
    modalWindow.windowId = "myModal";

    modalWindow.spacing = (100 - lnPercentage) / 2;
    modalWindow.content = "";

    if (closebutton != null) {
        if (reload != null) {
            modalWindow.content = "<div class=\"modal-close\"><a class='btn btn-default' href='#' onclick='modalWindow.close();location.reload()'>Venster sluiten</a></div>";
        }
        else {
            modalWindow.content = "<div class=\"modal-close\"><a class='btn btn-default' href='#' onclick='modalWindow.close();'>Venster sluiten</a></div>";
        }
    }

    var borderLine = "border: " + modalWindow.border + "px solid rgba(0, 0, 0, .2);"

    modalWindow.content = modalWindow.content + "<iframe width='100%' height='100%' scrolling='auto' allowtransparency='true' src='" + source + "' frameborder=0 style='" + borderLine + "'>&lt/iframe>";

    modalWindow.open();
};
