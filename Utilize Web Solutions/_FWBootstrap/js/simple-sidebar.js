﻿// Add handler to document ready
$(document).ready(function () {
        // Add handler to the click function of the toggle buttn
    $('.sidebar-toggler').click(function () {
        $('.sidebar-toggler').toggleClass('glyphicon-menu-left');
        $('.sidebar-toggler').toggleClass('glyphicon-menu-right');

        // Save the id of the sidebar
        var menuId = $(this).data('toggle');

        // Locate the sidebar
        var frameContent = $('#framecontent');
        frameContent.toggleClass('content-toggled');

        $('.sidebar-wrapper').each(function () {
            $(this).toggleClass('sidebar-toggle');
            $(this).toggleClass('sidebar-toggled');
        })
    });
});