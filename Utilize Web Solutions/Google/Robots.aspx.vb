﻿
Partial Class Robots
    Inherits CMSPageTemplate

    Private Function create_robots() As String
        Dim robots As New StringBuilder
        robots.Append(Utilize.Data.DataProcedures.GetValue("ucm_settings", "robots_txt", "CMS_SETT", "us_code")).AppendLine()

        Return robots.ToString()
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim lc_robots As String = Me.create_robots()

        Response.Clear()
        Response.ContentType = "text/plain"
        Response.AddHeader("Content-Disposition", "inline")
        Response.Write(lc_robots)
        Response.End()
    End Sub

End Class
