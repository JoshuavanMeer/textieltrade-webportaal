﻿
Partial Class Google_GoogleSitemap
    Inherits CMSPageTemplate

    Private Function get_products(ByVal catCode As String) As System.Data.DataTable
        Dim _pf_product_filter As New Utilize.Web.Solutions.Webshop.ws_productfilter

        ' Bouw de product filter class op
        _pf_product_filter.include_sub_categories = True
        _pf_product_filter.page_size = 999999
        _pf_product_filter.category_code = catCode

        Return _pf_product_filter.get_products(Me.global_cms.Language)
    End Function

    Private Function create_SitemapXml_PartsOfTableInUse(ByVal domainUrl As String, ByVal currentDateString As String, ByVal selectUrlField As String, ByVal tableName As String, ByVal whereClause As String) As String

        ' Create the query to collect the list of tables from the database
        Dim qscTable_in_use As New Utilize.Data.QuerySelectClass
        qscTable_in_use.select_fields = selectUrlField
        qscTable_in_use.select_from = tableName
        qscTable_in_use.select_where = whereClause

        ' Execute the query and place result in datatable
        Dim dtTable_url_in_use As System.Data.DataTable = qscTable_in_use.execute_query()
        Dim sitemapStrings As StringBuilder = New StringBuilder

        ' Add the parts in use of the webshop
        For Each drRow As System.Data.DataRow In dtTable_url_in_use.Rows
            sitemapStrings.AppendLine("<url>")
            sitemapStrings.AppendLine("<loc>" + Uri.EscapeUriString(domainUrl + drRow.Item(0).ToString()) + "</loc>")
            sitemapStrings.AppendLine("<lastmod>" + currentDateString + "</lastmod>")
            sitemapStrings.AppendLine("</url>")
        Next

        Return sitemapStrings.ToString()
    End Function

    Private Function create_SitemapXml_PhotogalleriesInUse(ByVal domainUrl As String, ByVal currentDateString As String) As String

        Dim sitemapStrings As StringBuilder = New StringBuilder

        ' Is imagegallery shown on webshop?
        Dim showPhotogalleries As Boolean = False
        showPhotogalleries = Not String.IsNullOrEmpty(create_SitemapXml_PartsOfTableInUse(domainUrl, currentDateString, "tgt_url_" + Me.global_cms.Language, "ucm_structure", "show_type <> 1 and us_code in (select us_code from ucm_structure_blocks where block_type = 'IMAGEGALLERY')"))

        If showPhotogalleries Then

            ' When photogalleries are shown, create the query to collect the list of photogalleries from the database
            Dim qscTable_in_use As New Utilize.Data.QuerySelectClass
            qscTable_in_use.select_fields = "rec_id"
            qscTable_in_use.select_from = "ucm_photogallery"

            ' Execute the query and place result in datatable
            Dim dtTable_url_in_use As System.Data.DataTable = qscTable_in_use.execute_query()

            ' Add the parts in use of the webshop
            For Each drRow As System.Data.DataRow In dtTable_url_in_use.Rows
                sitemapStrings.AppendLine("<url>")
                sitemapStrings.AppendLine("<loc>" + Uri.EscapeUriString(domainUrl + "/" + Me.global_cms.Language + "/cms/photogallery_page.aspx?RecId=" + drRow.Item(0).ToString() + "&amp;ColumnsPerItem=6") + "</loc>")
                sitemapStrings.AppendLine("<lastmod>" + currentDateString + "</lastmod>")
                sitemapStrings.AppendLine("</url>")
            Next

        End If

        Return sitemapStrings.ToString()
    End Function

    Private Function create_SitemapXml_ProductsInUse(ByVal domainUrl As String, ByVal currentDateString As String) As String

        ' Create the query to collect the list of product categories from the database
        Dim qscCategories_tran_in_use As New Utilize.Data.QuerySelectClass
        qscCategories_tran_in_use.select_fields = "tgt_url, cat_code"
        qscCategories_tran_in_use.select_from = "uws_categories_tran"
        qscCategories_tran_in_use.select_where = "cat_code in (select cat_code from uws_categories where cat_show = 1) and lng_code = '" + Me.global_cms.Language + "'"
        qscCategories_tran_in_use.select_order = "tgt_url"

        ' Execute the query and place result in datatable
        Dim dtCategories_url_in_use As System.Data.DataTable = qscCategories_tran_in_use.execute_query()
        Dim sitemapStrings As StringBuilder = New StringBuilder
        Dim listOfProdIds As New List(Of String)

        ' Add the product categories in use of the webshop
        For Each drCategory As System.Data.DataRow In dtCategories_url_in_use.Rows
            sitemapStrings.AppendLine("<url>")
            sitemapStrings.AppendLine("<loc>" + Uri.EscapeUriString(domainUrl + drCategory.Item(0).ToString()) + "</loc>")
            sitemapStrings.AppendLine("<lastmod>" + currentDateString + "</lastmod>")
            sitemapStrings.AppendLine("</url>")

            ' Execute the query and place result in datatable 
            Dim dtProducts_url_in_use As System.Data.DataTable = get_products(drCategory.Item(1).ToString())

            ' Add the products in use of the webshop
            For Each drProduct As System.Data.DataRow In dtProducts_url_in_use.Rows
                If listOfProdIds.Contains(drProduct.Item("prod_code").ToString) Then
                    Continue For
                End If
                listOfProdIds.Add(drProduct.Item("prod_code").ToString)
                sitemapStrings.AppendLine("<url>")
                sitemapStrings.AppendLine("<loc>" + Uri.EscapeUriString(domainUrl + drProduct.Item("tgt_url").ToString) + "</loc>")
                sitemapStrings.AppendLine("<lastmod>" + currentDateString + "</lastmod>")
                sitemapStrings.AppendLine("</url>")
            Next
        Next

        Return sitemapStrings.ToString()
    End Function

    Private Function create_SitemapXml() As String

        ' Execute the query and place result in datatable
        Dim sitemapStrings As StringBuilder = New StringBuilder
        Dim domainUrl As String = relative_url.TrimEnd("/")

        Dim currentDate As Date = DateAndTime.Now
        Dim currentDateString As String = currentDate.ToString("yyyy-MM-dd")

        ' Set header
        sitemapStrings.AppendLine("<?xml version='1.0' encoding='UTF-8'?>")
        sitemapStrings.AppendLine("<urlset xmlns ='http://www.sitemaps.org/schemas/sitemap/0.9' >")

        ' Set general domain url as first
        sitemapStrings.AppendLine("<url>")
        sitemapStrings.AppendLine("<loc>" + Uri.EscapeUriString(domainUrl) + "</loc>")
        sitemapStrings.AppendLine("<lastmod>" + currentDateString + "</lastmod>")
        sitemapStrings.AppendLine("</url>")

        ' Add the several sections in use of the webshop
        ' sitemapStrings.AppendLine(create_SitemapXml_PartsOfTableInUse(domainUrl, currentDateString, "tgt_url_" + Me.global_cms.Language, "ucm_structure", "show_type <> 1"))

        sitemapStrings.AppendLine(create_SitemapXml_PhotogalleriesInUse(domainUrl, currentDateString))

        sitemapStrings.AppendLine(create_SitemapXml_PartsOfTableInUse(domainUrl, currentDateString, "tgt_url_" + Me.global_cms.Language, "ucm_news", "release_date < GETDATE() and hdr_id in (select news_block_id from ucm_structure_blocks where block_type = 'NEWS')"))

        sitemapStrings.AppendLine(create_SitemapXml_ProductsInUse(domainUrl, currentDateString))

        sitemapStrings.AppendLine(create_SitemapXml_PartsOfTableInUse(domainUrl, currentDateString, "tgt_url_" + Me.global_cms.Language, "ucm_specials", "hdr_id in (select specials_block_id from ucm_structure_blocks where block_type = 'SPECIALS')"))

        sitemapStrings.AppendLine(create_SitemapXml_PartsOfTableInUse(domainUrl + "/" + Me.global_cms.Language + "/", currentDateString, "topic_link_url_" + Me.global_cms.Language, "ucm_topics", "hdr_id in (select topics_block_id from ucm_structure_blocks where block_type = 'TOPICS')"))

        ' Finish sitemap.xml with closing tag
        sitemapStrings.AppendLine("</urlset>")

        Return sitemapStrings.ToString()
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim lc_sitemapXml_string As String = Me.create_SitemapXml()

        Response.Clear()
        Response.ContentType = "application/xml"
        Response.AddHeader("Content-Disposition", "inline")
        Response.Write(lc_sitemapXml_string)
        Response.End()
    End Sub

End Class
