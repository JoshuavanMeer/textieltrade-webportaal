﻿
Partial Class order_confirmation
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' In de webshop instellingen kun je de pagina template voor het betalingsproces vastleggen.
        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
        Dim uc_order_confirmation As Webshop.ws_user_control = LoadUserControl("PageBlocks/uc_order_confirmation.ascx")
        ph_zone.Controls.Add(uc_order_confirmation)
    End Sub

End Class