﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_payment.ascx.vb" Inherits="Webshop_PaymentProcess_Services_uc_payment" %>

<div class="uc_payment">
    <div class="row">
        <utilize:placeholder runat="server" ID="ph_sisow_links" Visible="false">
            <link media="all" type="text/css" href="https://bankauswahl.giropay.de/widget/v1/style.css" rel="stylesheet" />
            <link media="all" type="text/css" href="https://bankauswahl.giropay.de/eps/widget/v1/style.css" rel="stylesheet" />
            <script src="https://www.sisow.nl/Sisow/scripts/giro-eps.js" type="text/javascript"></script>    
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_payment_methods">
            <div class="col-md-8">
                <h3>
                    <utilize:translatetitle runat="server" ID="title_payment_services" Text="Betaalmethode"></utilize:translatetitle></h3>
                    <utilize:translatetext runat="server" ID="text_payment_services" Text="Geef rechts aan op welke manier u de bestelling wilt afrekenen."></utilize:translatetext>
                    <utilize:placeholder runat="server" ID="ph_default_payment_option" Visible="False">
                        <h4 class="default_payment_method">
                        <i class="fa fa-info-circle" ></i>
                        <utilize:translatetitle runat="server" ID="title_default_payment_method"></utilize:translatetitle></h4>
                    </utilize:placeholder>
            </div>
            <div class="col-md-4">                
                <utilize:placeholder runat="server" ID="ph_payment_options">
                    <utilize:optiongroup runat="server" ID="opg_payment_types" AutoPostBack="true">
                    </utilize:optiongroup>

                    <utilize:placeholder runat="server" ID="ph_sisow_bank_select" Visible="false">
                        <utilize:dropdownlist runat="server" ID="cbo_banks"></utilize:dropdownlist>
                        <utilize:placeholder runat="server" ID="ph_giro" Visible="false">
                            <utilize:textbox runat="server" ID="text_giro" CssClass="giropayinput form-control" autocomplete="off"></utilize:textbox>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_eps" Visible="false">
                            <utilize:textbox runat="server" ID="text_eps" CssClass="epsinput form-control" autocomplete="off"></utilize:textbox>
                        </utilize:placeholder>
                    </utilize:placeholder>                    
                </utilize:placeholder>
            </div>                
        </utilize:placeholder>
    </div>

    <div class="row">
        <div class="col-md-12">
            <utilize:translatetext runat="server" ID="text_payment_conditions" Text="Wanneer u kiest voor Betalen, dan gaat u akkoord met onze Algemene Verkoopvoorwaarden."></utilize:translatetext>
            <utilize:checkbox runat="server" ID="chk_payment_conditions" CssClass="check" />
        </div>
    </div>
    <div class="buttons pull-right">
        <utilize:translatebutton runat='server' ID="button_finish_order" Text="Afrekenen" CssClass="right btn-u btn-u-sea-shop btn-u-lg"/>
    </div>

    <br style="clear: both" />
    <utilize:alert AlertType="danger" runat="server" ID="pnl_error" Visible="false">
    </utilize:alert>
</div>
