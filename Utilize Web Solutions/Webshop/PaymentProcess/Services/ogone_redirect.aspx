﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ogone_redirect.aspx.vb" Inherits="ogone_redirect" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Ogone Redirect</title>
</head>
<body>
    <form method="post" id="frmPayment" name="frmOgone" action="<%= pay_url %>">
            <!-- Shop gegevens -->
            <input type="hidden" name="PARAMVAR" value="<%= PARAMVAR %>">
            <!-- Order gegevens -->
            <input type="hidden" name="PSPID" value="<%= PSPID %>"/>
            <input type="hidden" name="orderID" value="<%= orderID %>"/>
            <input type="hidden" name="amount" value="<%= amount %>"/>
            <input type="hidden" name="currency" value="<%= currency %>"/>
            <input type="hidden" name="language" value="<%= language %>"/>
     
            <input type="hidden" name="OWNERZIP" value="<%= OWNERZIP%>">
            <input type="hidden" name="OWNERADDRESS" value="<%= OWNERADDRESS%>">
            <input type="hidden" name="OWNERCTY" value="<%= OWNERCTY%>">
            <input type="hidden" name="OWNERTOWN" value="<%= OWNERTOWN%>">
            <input type="hidden" name="OWNERTELNO" value="<%= OWNERTELNO%>">

            <input type="hidden" name="SHASign" value="<%= SHASign %>"/>

            <!-- Redirect url's -->
            <input type="hidden" name="accepturl" value="<%= accepturl %>" />
            <input type="hidden" name="exceptionurl" value="<%= exceptionurl %>" />
            <input type="hidden" name="declineurl" value="<%= declineurl %>" />
            <input type="hidden" name="cancelurl" value="<%= cancelurl %>" />
            <input type="hidden" name="catalogurl" value="<%= catalogurl %>" />
            <input type="hidden" name="homeurl" value="<%= homeurl %>" />
     
            <!-- Betaal methode -->
            <input type="hidden" name="PM" value="<%= PM %>"/>
            <input type="hidden" name="BRAND" value="<%= BRAND %>"/>
     
            <!--<input type="submit" />-->
    </form>
    <script type="text/javascript" language="javascript">
        document.forms['frmPayment'].submit();
    </script>
</body>
</html>


