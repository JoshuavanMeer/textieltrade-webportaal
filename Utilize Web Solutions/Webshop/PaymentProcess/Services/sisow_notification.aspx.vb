﻿
Partial Class Webshop_PaymentProcess_Services_sisow_notification
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private Notify As String = "false"
    Private callback As String = "false"
    Private ORDERID As String = ""
    Private merchant_id As String = ""
    Private merchant_key As String = ""

    Private Function GetPaymentCode() As String
        Dim dt_payment_methods As System.Data.DataTable = Nothing
        ' Ogone gegevens ophalen
        Dim qc_payment_methods As New Utilize.Data.QueryCustomSelectClass With {
            .QueryString = "SELECT * FROM uws_pay WHERE pay_provider = 'SISOW' and not pay_code = '' "
        }
        dt_payment_methods = qc_payment_methods.execute_query("sisow_notification => ORDERID: " + ORDERID)

        ' als de ogone methode ebstaat
        If dt_payment_methods.Rows.Count > 0 Then
            merchant_id = dt_payment_methods.Rows(0).Item("pay_id")
            merchant_key = dt_payment_methods.Rows(0).Item("pay_signature")
            Return dt_payment_methods.Rows(0).Item("pay_code").ToString().Trim()
        End If

        Return "NIET_AANWEZIG"
    End Function

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init

        Try
            ORDERID = Me.get_page_parameter("ec").Replace(Me.GetPaymentCode(), "")
        Catch ex As Exception
            ORDERID = Me.get_page_parameter("ec")
        End Try

        Notify = Me.get_page_parameter("status") = "Success"
        callback = Me.get_page_parameter("callback")

        If Notify = True Then
            Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "rec_id")

            ' Initialiseer en laad het business object
            ubo_orders.bus_init()
            ubo_orders.bus_load()

            Dim lc_payment_id As String = Me.GetPaymentCode() + ubo_orders.field_get("payment_id")

            'de Ogone order id meegeven aan de webwinkel order
            ubo_orders.field_set("uws_orders.payment_id", lc_payment_id)
            ubo_orders.table_update("sisow_notification => ORDERID: " + ORDERID)

            ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")

            'To adjust the po order id in sisow
            Dim po_id = Utilize.Data.DataProcedures.GetValue("uws_so_orders", "ord_nr", ORDERID, "src_id")
            Dim o_id = ORDERID
            Dim trxid = ""
            Try
                trxid = Me.get_page_parameter("trxid")
            Catch ex As Exception
                trxid = ""
            End Try
            'Sending the purchase order id to SISOW
            SisowRest.AdjustPurchaseId(merchant_id, merchant_key, trxid, po_id, o_id)

            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try

            Me.global_ws.shopping_cart.create_shopping_cart()
        End If

        If callback = "true" Then

        End If

    End Sub
End Class
