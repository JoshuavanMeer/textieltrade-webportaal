﻿function giro_pay() {
    (function ($) {
        $(document).ready(function () {
            $('.giropayinput').giropay_widget({ 'return': 'bic', 'kind': 1 });
        });
    })(jQuery);
}

function eps_pay() {
    (function ($) {
        $(document).ready(function () {
            $('.epsinput').eps_widget({ 'return': 'bic' });
        });
    })(jQuery);
}