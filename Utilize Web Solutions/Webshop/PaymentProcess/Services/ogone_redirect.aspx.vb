﻿
Partial Class ogone_redirect
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private lc_querystring As String = ""

    Private dt_payment_methods As System.Data.DataTable = Nothing
    Private qc_payment_methods As New Utilize.Data.QueryCustomSelectClass
    Protected PARAMVAR As String = ""

    'ogone locatie
    Protected pay_url As String = ""
    Protected pay_code As String = ""

    ' bestellinggegevens
    Protected PSPID As String = ""
    Protected orderID As String = ""
    Protected amount As Decimal = 0
    Protected currency As String = ""
    Protected SHASign As String = ""
    Protected language As String = "nl_NL"

    ' locaties voor terug sturen
    Protected accepturl As String = ""
    Protected exceptionurl As String = ""
    Protected declineurl As String = ""
    Protected cancelurl As String = ""
    Protected catalogurl As String = ""
    Protected homeurl As String = ""

    Protected CN As String = ""
    Protected OWNERADDRESS As String = ""
    Protected OWNERZIP As String = ""
    Protected OWNERTOWN As String = ""
    Protected OWNERCTY As String = ""
    Protected OWNERTELNO As String = ""

    ' betaal methode en betaal brand
    Protected PM As String = ""
    Protected BRAND As String = ""

    Private Sub set_order_form()
        orderID = "PR" + Me.get_page_parameter("order_id")

        Dim ubo_order As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_so_orders", Me.get_page_parameter("order_id"), "av_ord_nr + payment_id")

        If ubo_order.field_get("del_address_custom") Then
            OWNERADDRESS = ubo_order.field_get("del_address")
            OWNERCTY = Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_cms.Language, ubo_order.field_get("del_cnt_code"))
            OWNERTELNO = ""
            OWNERTOWN = ubo_order.field_get("del_city")
            OWNERZIP = ubo_order.field_get("del_zip_code")
        Else
            CN = ubo_order.field_get("inv_fst_name") + " " + ubo_order.field_get("inv_infix") + " " + ubo_order.field_get("inv_lst_name")
            OWNERADDRESS = ubo_order.field_get("inv_address")
            OWNERCTY = Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_cms.Language, ubo_order.field_get("inv_cnt_code"))
            OWNERTELNO = ""
            OWNERTOWN = ubo_order.field_get("inv_city")
            OWNERZIP = ubo_order.field_get("inv_zip_code")
        End If


        Dim ln_amount As Decimal = ubo_order.field_get("order_total")
        ln_amount += ubo_order.field_get("vat_total")
        ln_amount += ubo_order.field_get("order_costs")
        ln_amount += ubo_order.field_get("shipping_costs")
        ln_amount += ubo_order.field_get("vat_ord")
        ln_amount += ubo_order.field_get("vat_ship")
        ln_amount -= ubo_order.field_get("uws_orders.cpn_amt")
        ln_amount -= ubo_order.field_get("uws_orders.cpn_vat_amt")

        amount = Decimal.Round(ln_amount * 100)

        currency = dt_payment_methods.Rows(0).Item("pay_currency").ToString().Trim()
        PM = Me.get_page_parameter("payment_type")
        BRAND = Me.get_page_parameter("payment_brand")

        If PM.ToUpper() = "ONLINE" Then
            PM = ""
        End If

        If BRAND.ToUpper() = "ONLINE" Then
            BRAND = ""
        End If

        Dim pay_signature = dt_payment_methods.Rows(0).Item("pay_signature").ToString().Trim()
        Dim hash_object As System.Security.Cryptography.HashAlgorithm = New System.Security.Cryptography.SHA1Managed()
        Dim hash_signature As String = ""

        accepturl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=SUCCESS"
        exceptionurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        declineurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        cancelurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        catalogurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/product_list.aspx"
        homeurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/home.aspx"

        ' hier word de hash samengesteld
        If Not accepturl = "" Then
            hash_signature &= "ACCEPTURL=" & accepturl & pay_signature
        End If

        hash_signature &= "AMOUNT=" & amount & pay_signature

        If Not BRAND = "" Then
            hash_signature &= "BRAND=" & BRAND & pay_signature
        End If

        If Not cancelurl = "" Then
            hash_signature &= "CANCELURL=" & cancelurl & pay_signature
        End If

        If Not catalogurl = "" Then
            hash_signature &= "CATALOGURL=" & catalogurl & pay_signature
        End If

        hash_signature &= "CURRENCY=" & currency & pay_signature

        If Not declineurl = "" Then
            hash_signature &= "DECLINEURL=" & declineurl & pay_signature
        End If

        If Not exceptionurl = "" Then
            hash_signature &= "EXCEPTIONURL=" & exceptionurl & pay_signature
        End If

        If Not homeurl = "" Then
            hash_signature &= "HOMEURL=" & homeurl & pay_signature
        End If

        hash_signature &= "LANGUAGE=" & language & pay_signature
        hash_signature &= "ORDERID=" & orderID & pay_signature

        If Not OWNERADDRESS = "" Then
            hash_signature &= "OWNERADDRESS=" & OWNERADDRESS & pay_signature
        End If

        If Not OWNERCTY = "" Then
            hash_signature &= "OWNERCTY=" & OWNERCTY & pay_signature
        End If

        If Not OWNERTELNO = "" Then
            hash_signature &= "OWNERTELNO=" & OWNERTELNO & pay_signature
        End If

        If Not OWNERTOWN = "" Then
            hash_signature &= "OWNERTOWN=" & OWNERTOWN & pay_signature
        End If

        If Not OWNERZIP = "" Then
            hash_signature &= "OWNERZIP=" & OWNERZIP & pay_signature
        End If

        hash_signature &= "PARAMVAR=" & PARAMVAR & pay_signature

        If Not PM = "" Then
            hash_signature &= "PM=" & PM & pay_signature
        End If

        If Not PSPID = "" Then
            hash_signature &= "PSPID=" & PSPID & pay_signature
        End If

        ' de hash string omzetten in een 40 tekens lange code
        Dim Password() As Byte = Encoding.UTF8.GetBytes(hash_signature)

        Dim HashPassword() As Byte = hash_object.ComputeHash(Password)
        SHASign = BitConverter.ToString(HashPassword).Replace("-", "")
    End Sub
    Private Sub set_shopping_cart_form()
        orderID = pay_code.Trim() + Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("uws_orders.ogone_id")

        Dim ubo_order As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", Me.get_page_parameter("order_id"), "ogone_id")

        If ubo_order.field_get("del_address_custom") Then
            OWNERADDRESS = ubo_order.field_get("del_address")
            OWNERCTY = Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_cms.Language, ubo_order.field_get("del_cnt_code"))
            OWNERTELNO = ""
            OWNERTOWN = ubo_order.field_get("del_city")
            OWNERZIP = ubo_order.field_get("del_zip_code")
        Else
            OWNERADDRESS = ubo_order.field_get("inv_address")
            OWNERCTY = Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_cms.Language, ubo_order.field_get("inv_cnt_code"))
            OWNERTELNO = ""
            OWNERTOWN = ubo_order.field_get("inv_city")
            OWNERZIP = ubo_order.field_get("inv_zip_code")
        End If

        Dim ln_amount As Decimal = ubo_order.field_get("order_total")
        ln_amount += ubo_order.field_get("vat_total")
        ln_amount += ubo_order.field_get("order_costs")
        ln_amount += ubo_order.field_get("shipping_costs")
        ln_amount += ubo_order.field_get("vat_ord")
        ln_amount += ubo_order.field_get("vat_ship")
        ln_amount -= ubo_order.field_get("uws_orders.cpn_amt")
        ln_amount -= ubo_order.field_get("uws_orders.cpn_vat_amt")

        amount = Decimal.Round(ln_amount * 100)

        currency = dt_payment_methods.Rows(0).Item("pay_currency").ToString().Trim()
        PM = Me.get_page_parameter("payment_type")
        BRAND = Me.get_page_parameter("payment_brand")

        If PM.ToUpper() = "ONLINE" Then
            PM = ""
        End If

        If BRAND.ToUpper() = "ONLINE" Then
            BRAND = ""
        End If

        Dim pay_signature = dt_payment_methods.Rows(0).Item("pay_signature").ToString().Trim()
        Dim hash_object As System.Security.Cryptography.HashAlgorithm = New System.Security.Cryptography.SHA1Managed()
        Dim hash_signature As String = ""

        accepturl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=SUCCESS"
        exceptionurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        declineurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        cancelurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/PaymentProcess/order_confirmation.aspx?status=FAILURE"
        catalogurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/Webshop/product_list.aspx"
        homeurl = Me.relative_url & "/" & Me.global_ws.Language.ToLower() & "/home.aspx"

        ' hier word de hash samengesteld
        If Not accepturl = "" Then
            hash_signature &= "ACCEPTURL=" & accepturl & pay_signature
        End If

        hash_signature &= "AMOUNT=" & amount & pay_signature

        If Not BRAND = "" Then
            hash_signature &= "BRAND=" & BRAND & pay_signature
        End If

        If Not cancelurl = "" Then
            hash_signature &= "CANCELURL=" & cancelurl & pay_signature
        End If

        If Not catalogurl = "" Then
            hash_signature &= "CATALOGURL=" & catalogurl & pay_signature
        End If

        hash_signature &= "CURRENCY=" & currency & pay_signature

        If Not declineurl = "" Then
            hash_signature &= "DECLINEURL=" & declineurl & pay_signature
        End If

        If Not exceptionurl = "" Then
            hash_signature &= "EXCEPTIONURL=" & exceptionurl & pay_signature
        End If

        If Not homeurl = "" Then
            hash_signature &= "HOMEURL=" & homeurl & pay_signature
        End If

        hash_signature &= "LANGUAGE=" & language & pay_signature
        hash_signature &= "ORDERID=" & orderID & pay_signature

        If Not OWNERADDRESS = "" Then
            hash_signature &= "OWNERADDRESS=" & OWNERADDRESS & pay_signature
        End If

        If Not OWNERCTY = "" Then
            hash_signature &= "OWNERCTY=" & OWNERCTY & pay_signature
        End If

        If Not OWNERTELNO = "" Then
            hash_signature &= "OWNERTELNO=" & OWNERTELNO & pay_signature
        End If

        If Not OWNERTOWN = "" Then
            hash_signature &= "OWNERTOWN=" & OWNERTOWN & pay_signature
        End If

        If Not OWNERZIP = "" Then
            hash_signature &= "OWNERZIP=" & OWNERZIP & pay_signature
        End If

        hash_signature &= "PARAMVAR=" & PARAMVAR & pay_signature

        If Not PM = "" Then
            hash_signature &= "PM=" & PM & pay_signature
        End If

        If Not PSPID = "" Then
            hash_signature &= "PSPID=" & PSPID & pay_signature
        End If

        ' de hash string omzetten in een 40 tekens lange code
        Dim Password() As Byte = Encoding.UTF8.GetBytes(hash_signature)

        Dim HashPassword() As Byte = hash_object.ComputeHash(Password)
        SHASign = BitConverter.ToString(HashPassword).Replace("-", "")
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.PARAMVAR = Request.Url.GetLeftPart(UriPartial.Authority)

        Select Case global_ws.Language.ToLower()
            Case "nl"
                language = "nl_NL"
            Case "de"
                language = "de_DE"
            Case "en"
                language = "en_US"
            Case Else
                Exit Select
        End Select

        ' Ogone gegevens ophalen
        qc_payment_methods.QueryString = "SELECT * FROM uws_pay WHERE pay_provider = 'OGONE' "
        dt_payment_methods = qc_payment_methods.execute_query("ogone_redirect => ORDERID: " + orderID)

        ' als de ogone methode ebstaat
        If dt_payment_methods.Rows.Count > 0 Then
            ' bestellingsgegevens ophalen
            pay_url = dt_payment_methods.Rows(0).Item("pay_url").ToString().Trim()
            PSPID = dt_payment_methods.Rows(0).Item("pay_id").ToString().Trim()
            pay_code = dt_payment_methods.Rows(0).Item("pay_code").ToString().Trim()
            Select Case True
                Case Me.get_page_parameter("order_id").StartsWith("20")
                    Me.set_order_form()
                Case Else
                    Me.set_shopping_cart_form()
            End Select
        End If
    End Sub
End Class
