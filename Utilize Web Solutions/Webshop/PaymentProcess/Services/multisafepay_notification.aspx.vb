﻿Imports MultiSafepay

Partial Class Webshop_PaymentProcess_Services_multisafepay_notification
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private ORDERID As String = ""

    Private merchantId As String = ""
    Private merchantKey As String = ""
    Private shopId As String = ""
    Private currency As String = ""
    Private payUrl As String = ""

    Private Sub set_merchant_details()
        Dim qsc_merchant As New Utilize.Data.QuerySelectClass
        With qsc_merchant
            .select_fields = "*"
            .select_from = "uws_pay"
        End With
        Dim dt_results As System.Data.DataTable = qsc_merchant.execute_query()

        merchantId = dt_results.Rows(0).Item("pay_id")
        merchantKey = dt_results.Rows(0).Item("pay_signature")
        shopId = dt_results.Rows(0).Item("pay_password")
        Currency = dt_results.Rows(0).Item("pay_currency")
        payUrl = dt_results.Rows(0).Item("pay_url")
    End Sub


    Private Function GetPaymentCode() As String
        Dim dt_payment_methods As System.Data.DataTable = Nothing
        ' Ogone gegevens ophalen
        Dim qc_payment_methods As New Utilize.Data.QueryCustomSelectClass With {
            .QueryString = "SELECT * FROM uws_pay WHERE pay_provider = 'MULTISAFEPAY' and not pay_code = '' "
        }
        dt_payment_methods = qc_payment_methods.execute_query("MULTISAFEPAY_notification => ORDERID: " + ORDERID)

        ' als de ogone methode ebstaat
        If dt_payment_methods.Rows.Count > 0 Then
            Return dt_payment_methods.Rows(0).Item("pay_code").ToString().Trim()
        End If

        Return "NIET_AANWEZIG"
    End Function

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init

        Try
            ORDERID = Me.get_page_parameter("transactionid").Replace(Me.GetPaymentCode(), "")
        Catch ex As Exception
            ORDERID = Me.get_page_parameter("transactionid")
        End Try

        If Not String.IsNullOrEmpty(ORDERID) Then
            set_merchant_details()

            Dim client = New MultiSafepayClient(merchantId, payUrl)

            Dim order = client.GetOrder(ORDERID)

            If order.Status = "completed" Then
                Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "rec_id")

                ' Initialiseer en laad het business object
                ubo_orders.bus_init()
                ubo_orders.bus_load()

                'de Ogone order id meegeven aan de webwinkel order
                ubo_orders.table_update("MULTISAFEPAY_notification => ORDERID: " + ORDERID)

                ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")

                Try
                    Dim WsAvConn As New WSAvConn.WSAvConn With {
                            .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                        }
                    WsAvConn.GetWebshopInformation()
                Catch ex As Exception

                End Try

                Me.global_ws.shopping_cart.create_shopping_cart()
            End If
        End If
    End Sub
End Class
