﻿Imports MultiSafepay

Partial Class Webshop_PaymentProcess_Services_uc_payment
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private dt_payment_methods As System.Data.DataTable = Nothing
    Private ProviderId As PaymentProvider

    Private merchantId As String = ""
    Private merchantKey As String = ""
    Private shopId As String = ""
    Private currency As String = ""
    Private payUrl As String = ""
    Private testMode As Boolean = False

    Private iss() As String

    Private Enum PaymentProvider
        Sisow
        Ogone
        MultiSafepay
    End Enum

    ''' <summary>
    ''' Haal de betaalmethoden op
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub get_payment_types()
        ' Haal het land van de klant op
        Dim lcCntCode As String = Me.global_ws.user_information.ubo_customer.field_get("cnt_code")

        ' Als dit land niet is ingevuld, haal dan het standaard land op
        If lcCntCode = "" Then
            lcCntCode = Me.global_ws.get_webshop_setting("default_country")
        End If

        ' Haal de betalingsmethoden voor het geselecteerde land op
        Dim qscCountryPm As New Utilize.Data.QuerySelectClass
        With qscCountryPm
            .select_fields = "*"
            .select_from = "uws_country_pm"
            .select_where = "cnt_code = @cnt_code"
        End With
        qscCountryPm.add_parameter("cnt_code", lcCntCode)

        Dim dtCountryPm As System.Data.DataTable = qscCountryPm.execute_query()

        Dim qsc_payment_methods As New Utilize.Data.QuerySelectClass
        With qsc_payment_methods
            .select_fields = "pt_code , pt_desc_" & Me.global_cms.Language & " AS pt_desc, logo"
            .select_from = "uws_pay_types"
        End With

        If Not Me.global_ws.user_information.ubo_customer.field_get("account_available") = True Then
            qsc_payment_methods.select_where += "on_account_enab = 0"
        End If

        qsc_payment_methods.select_order = "row_ord"

        ' Als er meerdere regels gevonden zijn, dan moet de query voor de betalingsmethoden aangevuld worden
        If dtCountryPm.Rows.Count > 0 Then
            If String.IsNullOrEmpty(qsc_payment_methods.select_where) Then
                qsc_payment_methods.select_where += "pt_code in (select pm_code from uws_country_pm where cnt_code = @cnt_code) "
            Else
                qsc_payment_methods.select_where += "and pt_code in (select pm_code from uws_country_pm where cnt_code = @cnt_code) "
            End If
            qsc_payment_methods.add_parameter("cnt_code", lcCntCode)
        End If

        dt_payment_methods = qsc_payment_methods.execute_query()
    End Sub

    Private Sub set_merchant_details()
        Dim qsc_merchant As New Utilize.Data.QuerySelectClass
        With qsc_merchant
            .select_fields = "*"
            .select_from = "uws_pay"
        End With
        Dim dt_results As System.Data.DataTable = qsc_merchant.execute_query()

        merchantId = dt_results.Rows(0).Item("pay_id")
        merchantKey = dt_results.Rows(0).Item("pay_signature")
        shopId = dt_results.Rows(0).Item("pay_password")
        currency = dt_results.Rows(0).Item("pay_currency")
        payUrl = dt_results.Rows(0).Item("pay_url")
        testMode = dt_results.Rows(0).Item("sisow_test_mode")

        Select Case dt_results.Rows(0).Item("pay_provider").ToString.ToUpper
            Case "SISOW"
                ProviderId = PaymentProvider.Sisow
            Case "MULTISAFEPAY"
                ProviderId = PaymentProvider.MultiSafepay
            Case "OGONE"
                ProviderId = PaymentProvider.Ogone
        End Select

    End Sub

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_payment.css", Me)
        Me.set_javascript("uc_payment.js", Me)

        Me.chk_payment_conditions.Text = global_trans.translate_message("label_accept_general_terms", "Geef aan dat u akkoord gaat met onze Algemene Verkoopvoorwaarden.", Me.global_ws.Language)
        Me.chk_payment_conditions.Visible = Me.global_ws.get_webshop_setting("accept_terms")

        ' Haal hier de betaalmethoden op en zet die in de dt_payment_methods
        Me.set_merchant_details()
        Me.get_payment_types()

        ' Toon betaalmethode label ipv radio buttons als er maar 0 of 1 betaalmethode is
        Dim nr_payment_method_count = dt_payment_methods.Rows.Count
        If nr_payment_method_count <= 1 Then
            Me.ph_payment_options.Visible = False
            Me.text_payment_services.Visible = False
            Me.ph_default_payment_option.Visible = True
            If nr_payment_method_count = 0 Then
                Me.title_default_payment_method.Text = global_trans.translate_message("title_default_payment_method_account", "Deze order wordt op rekening besteld", Me.global_ws.Language)
            ElseIf nr_payment_method_count = 1 Then
                Me.title_default_payment_method.Text = dt_payment_methods.Rows(0).Item("pt_desc")
            End If
        End If

        If ProviderId = PaymentProvider.Sisow Then
            Me.create_sisowrest()
            ph_sisow_links.Visible = True
            ph_sisow_bank_select.Visible = True
        End If

        If Not Me.IsPostBack() Then
            Me.opg_payment_types.DataSource = dt_payment_methods
            Me.opg_payment_types.DataTextField = "pt_desc"
            Me.opg_payment_types.DataValueField = "pt_code"
            Me.opg_payment_types.DataBind()

            If Me.dt_payment_methods.Rows.Count > 0 Then
                Me.opg_payment_types.SelectedIndex = 0
                Me.opg_payment_types_SelectedIndexChanged(Nothing, Nothing)
            End If

            If ProviderId = PaymentProvider.Sisow Then
                Me.fill_sisow_banks()
            End If
        End If

        If ProviderId = PaymentProvider.Sisow Then
            If Me.opg_payment_types.SelectedValue.ToLower() = "eps" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "epssetup", "eps_pay();", True)
                Me.ph_eps.Visible = True
            End If
            If Me.opg_payment_types.SelectedValue.ToLower() = "giropay" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "giropaysetup", "giro_pay();", True)
                Me.ph_giro.Visible = True
            End If
        End If

        Me.button_finish_order.OnClientClick = "$('.order_progress').css('display', 'block');$('.uc_payment .inputbutton').addClass('active');"
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.ph_payment_options.Visible = Me.dt_payment_methods.Rows.Count >= 1 And Me.ph_payment_methods.Visible
        Me.opg_payment_types.Visible = Me.ph_payment_options.Visible And Me.dt_payment_methods.Rows.Count > 1

        If ProviderId = PaymentProvider.Sisow Then
            Me.cbo_banks.Visible = Me.opg_payment_types.SelectedValue.ToLower() = "ideal"
            Me.ph_giro.Visible = Me.opg_payment_types.SelectedValue.ToLower() = "giropay"
            Me.ph_eps.Visible = Me.opg_payment_types.SelectedValue.ToLower() = "eps"

            If Me.opg_payment_types.SelectedValue.ToLower() = "giropay" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "giropaysetup", "giro_pay();", True)
            End If
            If Me.opg_payment_types.SelectedValue.ToLower() = "eps" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "epssetup", "eps_pay();", True)
            End If
        End If
    End Sub

    Private Sub update_max_credits(ByVal order_id As String)
        Dim ln_credit_value As Decimal = Me.global_ws.user_information.ubo_customer.field_get("credits_value")

        If ln_credit_value = 0 Then
            ln_credit_value = global_ws.get_webshop_setting("credits_value")
        End If

        Dim qsc_select As New Utilize.Data.QuerySelectClass
        With qsc_select
            .select_fields = "user_id, SUM(row_amt) as row_amt"
            .select_from = "uws_order_lines"
            .select_where = "hdr_id = @hdr_id and not user_id = '' group by user_id "
        End With
        qsc_select.add_parameter("hdr_id", order_id)
        Dim dt_order_lines As System.Data.DataTable = qsc_select.execute_query()

        For Each dr_order_line As System.Data.DataRow In dt_order_lines.Rows
            Dim qsc_update As New Utilize.Data.QueryUpdateClass
            With qsc_update
                .update_table = "uws_customers_users"
                .update_string = "credits_available = credits_available - @credits_used, av_sync = 0"
                .update_where = "rec_id = @user_id "
            End With
            qsc_update.add_parameter("credits_used", (System.Math.Ceiling(dr_order_line.Item("row_amt")) / ln_credit_value))
            qsc_update.add_parameter("user_id", dr_order_line.Item("user_id"))

            qsc_update.execute_update(Me.global_ws.user_information.user_id)
        Next
    End Sub

    Private Sub update_max_amount(ByVal order_id As String)
        Dim ln_credit_value As Decimal = 1

        Dim qsc_select As New Utilize.Data.QuerySelectClass
        With qsc_select
            .select_fields = "user_id, SUM(row_amt) as row_amt"
            .select_from = "uws_order_lines"
            .select_where = "hdr_id = @hdr_id and not user_id = '' group by user_id "
        End With
        qsc_select.add_parameter("hdr_id", order_id)

        Dim dt_order_lines As System.Data.DataTable = qsc_select.execute_query()

        For Each dr_order_line As System.Data.DataRow In dt_order_lines.Rows
            Dim qsc_update As New Utilize.Data.QueryUpdateClass
            With qsc_update
                .update_table = "uws_customers_users"
                .update_string = "credits_available = credits_available - @credits_used, av_sync = 0"
                .update_where = "rec_id = @user_id "
            End With
            qsc_update.add_parameter("credits_used", System.Math.Round(dr_order_line.Item("row_amt"), 2))
            qsc_update.add_parameter("user_id", dr_order_line.Item("user_id"))

            qsc_update.execute_update(Me.global_ws.user_information.user_id)
        Next
    End Sub

    Private Sub opg_payment_types_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opg_payment_types.SelectedIndexChanged
        Dim lc_payment_type As String = Me.opg_payment_types.SelectedValue
        Me.global_ws.shopping_cart.field_set("payment_type", lc_payment_type)
    End Sub

    Protected Sub button_finish_order_Click(sender As Object, e As EventArgs) Handles button_finish_order.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.button_finish_order.Visible = False
            Me.pnl_error.Visible = False
        End If

        If ll_resume And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_price_multip") Then
            ll_resume = Not Session("use_price_multiplier")

            If Not ll_resume Then
                Me.button_finish_order.Visible = True
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "price_multiplier_on", "alert('" + global_trans.translate_message("message_price_multiplier_on", "U kunt niet bestellen zolang de prijsvermenigvuldiging aan staat. Zet dit uit op de mijn account pagina", Me.global_ws.Language) + "');", True)
            End If
        End If

        If ll_resume And Me.global_ws.get_webshop_setting("accept_terms") = True Then
            ll_resume = Me.chk_payment_conditions.Checked

            If Not ll_resume Then
                Me.button_finish_order.Visible = True
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "accept_terms", "alert('" + global_trans.translate_message("message_accept_general_terms", "U kunt alleen bij ons bestellen wanneer u akkoord gaat met onze algemene voorwaarden.", Me.global_ws.Language) + "');", True)
            End If
        End If

        If ll_resume And Utilize.Data.API.Webshop.IntermediateServiceProcedures.get_setting("sync_cust_on_login") Then
            Dim dr_customer As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow("uws_customers", Me.global_ws.user_information.ubo_customer.field_get("uws_customers.rec_id"))
            ll_resume = Not dr_customer.Item("blocked")

            If Not ll_resume Then
                Me.button_finish_order.Visible = True
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "account_blocked", "alert('" + global_trans.translate_message("message_account_blocked", "Uw account is geblokkeerd, neem contact met ons op.", Me.global_ws.Language) + "');", True)
            End If
        End If

        If ll_resume And IsPostBack Then
            Dim lc_payment_type As String = Me.opg_payment_types.SelectedValue
            Dim lc_order_id As String = Me.global_ws.shopping_cart.field_get("rec_id")

            If Utilize.Data.DataProcedures.GetValue("uws_pay_types", "pt_online", lc_payment_type) = True Then
                Me.global_ws.shopping_cart.field_set("payment_id", Me.global_ws.shopping_cart.field_get("rec_id"))
                Me.global_ws.shopping_cart.field_set("payment_type", lc_payment_type)
                Me.global_ws.shopping_cart.field_set("payment_online", True)
            Else
                Me.global_ws.shopping_cart.field_set("payment_id", "")
                Me.global_ws.shopping_cart.field_set("payment_type", lc_payment_type)
                Me.global_ws.shopping_cart.field_set("payment_online", False)
            End If
        End If

        ' Toon sisow als de payment online true is
        If ll_resume And IsPostBack And Me.global_ws.shopping_cart.field_get("payment_online") = True Then
            Dim lc_payment_type As String = Me.opg_payment_types.SelectedValue
            Me.global_ws.shopping_cart.field_set("payment_type", lc_payment_type)
            Me.global_ws.shopping_cart.save_shopping_cart()

            Select Case ProviderId
                Case PaymentProvider.Sisow
                    ll_resume = OnlineSisowPayment(lc_payment_type)
                Case PaymentProvider.Ogone
                    ll_resume = OnlineOgonePayment(lc_payment_type)
                Case PaymentProvider.MultiSafepay
                    ll_resume = OnlineMultiSafepayPayment(lc_payment_type)
            End Select
        End If

        If ll_resume And IsPostBack And Me.global_ws.shopping_cart.field_get("payment_online") = False Then
            Dim lc_order_id As String = Me.global_ws.shopping_cart.field_get("rec_id")
            Dim lc_full_name As String = Me.global_ws.user_information.user_full_name

            If String.IsNullOrEmpty(Me.global_ws.shopping_cart.field_get("email_address")) Then
                Me.global_ws.shopping_cart.field_set("email_address", Me.global_ws.user_information.ubo_customer.field_get("email_address"))
            End If

            Dim lc_email_address As String = Me.global_ws.shopping_cart.field_get("email_address")

            If String.IsNullOrEmpty(Me.global_ws.shopping_cart.field_get("uws_orders.email_name")) Then
                Me.global_ws.shopping_cart.field_set("uws_orders.email_name", lc_full_name)
            End If

            ' Sla de bestelling op en maak een verkooporder aan
            Me.global_ws.shopping_cart.save_shopping_cart()

            Select Case global_ws.price_mode
                Case Webshop.price_mode.Credits
                    ' Werk de webshop gebruikers bij met de maximum aantallen besteld
                    Me.update_max_credits(Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("rec_id"))
                Case Webshop.price_mode.Money
                    ' Werk de webshop gebruikers bij met de maximum aantallen besteld
                    Me.update_max_amount(Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("rec_id"))
                Case Webshop.price_mode.Unlimited
                    Exit Select
                Case Else
                    Exit Select
            End Select

            Me.global_ws.shopping_cart.create_purchase_order()

            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try

            ' Toon de juiste status
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/paymentprocess/order_confirmation.aspx?Status=SUCCESS&orderid=" + lc_order_id)
        End If
    End Sub

#Region "MultiSafepayFunctions"
    Private Function OnlineMultiSafepayPayment(payment_type As String) As Boolean
        Dim lc_order_id As String = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("uws_orders.rec_id")

        Dim lc_notification As String = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/Services/multisafepay_notification.aspx"
        Dim lc_success As String = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/order_confirmation.aspx?status=SUCCESS"
        Dim lc_failed As String = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/order_confirmation.aspx?status=CANCELLED"

        Dim orderDescription As String = global_trans.translate_label("lt_debtor", "Debiteur", Me.global_ws.Language)
        orderDescription += " #" + Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")
        orderDescription += " " + Me.global_ws.user_information.ubo_customer.field_get("uws_customers.bus_name")
        orderDescription += " " + global_trans.translate_label("lt_order", "Bestelling", Me.global_ws.Language)
        orderDescription += " #" + lc_order_id

        Dim multiSafepayObj As New MultiSafepay.Model.Order With
        {
            .Type = MultiSafepay.Model.OrderType.Redirect,
            .OrderId = lc_order_id,
            .GatewayId = payment_type,
            .AmountInCents = Me.global_ws.shopping_cart.order_total_incl_vat * 100,
            .CurrencyCode = Currency,
            .Description = orderDescription,
            .PaymentOptions = New MultiSafepay.Model.PaymentOptions(lc_notification,
                                                                    lc_success,
                                                                    lc_failed),
            .Customer = New MultiSafepay.Model.Customer() With
            {
                .FirstName = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.fst_name"),
                .LastName = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.infix") + " " + Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.lst_name"),
                .Country = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.cnt_code"),
                .Locale = Me.global_ws.Language,
                .Email = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.email_address"),
                .CompanyName = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.bus_name")
            }
        }

        Dim client = New MultiSafepayClient(merchantId, payUrl)

        Try
            Dim result = client.CustomOrder(multiSafepayObj)

            Me.Response.Redirect(result.PaymentUrl)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region "OgoneFunctions"
    Private Function OnlineOgonePayment(brand As String) As Boolean
        Dim lc_payment_type As String = Utilize.Data.DataProcedures.GetValue("uws_pay_types", "pt_type", brand)
        Dim lo_guid As Guid = Guid.NewGuid
        Dim lc_ogone_id As String = lo_guid.ToString("n").Substring(0, 20)

        Me.global_ws.shopping_cart.field_set("ogone_id", lc_ogone_id)

        Me.global_ws.shopping_cart.save_shopping_cart()
        Try
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/paymentprocess/services/ogone_redirect.aspx?payment_type=" + lc_payment_type + "&payment_brand=" + brand + "&order_id=" + lc_ogone_id)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region "SisowFunctions"
    Private obj_sisowrest As SisowRest = Nothing
    Private Sub create_sisowrest()
        obj_sisowrest = New SisowRest(merchantId, merchantKey)
        obj_sisowrest.testMode = testMode
    End Sub

    Private Sub fill_sisow_banks()
        Dim lo_select_bank As New System.Web.UI.WebControls.ListItem
        With lo_select_bank
            .Value = ""
            .Text = global_trans.translate_label("label_sisow_select_bank", "Selecteer een bank....", Me.global_ws.Language)
        End With
        Me.cbo_banks.Items.Add(lo_select_bank)

        If obj_sisowrest.DirectoryRequest(False, iss) = 0 Then
            For I As Integer = 1 To iss.Length Step 2
                Dim lo_listitem As New System.Web.UI.WebControls.ListItem
                With lo_listitem
                    .Value = iss(I - 1)
                    .Text = iss(I)
                End With
                Me.cbo_banks.Items.Add(lo_listitem)
            Next
        End If
    End Sub

    Private Function OnlineSisowPayment(payment_type As String) As Boolean
        obj_sisowrest.issuerId = cbo_banks.SelectedValue

        obj_sisowrest.merchantId = Me.merchantId
        obj_sisowrest.merchantKey = Me.merchantKey

        obj_sisowrest.entranceCode = Me.global_ws.shopping_cart.field_get("rec_id")
        obj_sisowrest.purchaseId = Me.global_ws.shopping_cart.field_get("rec_id")
        obj_sisowrest.description = Me.global_ws.shopping_cart.field_get("rec_id")

        obj_sisowrest.locale = Me.global_ws.Language

        obj_sisowrest.amount = System.Math.Round(Me.global_ws.shopping_cart.order_total_incl_vat, 2)

        obj_sisowrest.notifyUrl = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/Services/sisow_notification.aspx"
        obj_sisowrest.returnUrl = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/order_confirmation.aspx"
        obj_sisowrest.cancelUrl = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/order_confirmation.aspx"
        obj_sisowrest.callbackUrl = IIf(Me.relative_url.EndsWith("/"), Me.relative_url, Me.relative_url + "/") + Me.global_ws.Language + "/Webshop/PaymentProcess/Services/sisow_notification.aspx"
        'obj_sisowrest.testMode = True


        If payment_type = "IDEAL" Then
            obj_sisowrest.payment = ""
        ElseIf payment_type = "GIROPAY" Then
            obj_sisowrest.payment = payment_type.ToLower()
            obj_sisowrest.bic = Me.text_giro.Text
        ElseIf payment_type = "EPS" Then
            obj_sisowrest.payment = payment_type.ToLower()
            obj_sisowrest.bic = Me.text_eps.Text
        Else
            obj_sisowrest.payment = payment_type.ToLower()
        End If

        Dim ln_error_code As Integer = obj_sisowrest.TransactionRequest()

        If ln_error_code = 0 Then
            Me.Response.Redirect(obj_sisowrest.issuerUrl)
            Return True
        ElseIf obj_sisowrest.testMode Then
            Me.pnl_error.Visible = True

            Me.pnl_error.Text = ln_error_code.ToString() + "<br>"
            Me.pnl_error.Text += obj_sisowrest.errorMessage.ToString() + "<br>"
            Me.pnl_error.Text += "ShopId=" + Me.shopId + "<br>"
            Me.pnl_error.Text += "MerchantId=" + Me.merchantId + "<br>"
            Me.pnl_error.Text += "MerchantKey=" + Me.merchantKey + "<br>"

            Return True
        ElseIf obj_sisowrest.testMode = False Then
            Me.pnl_error.Visible = True
            Me.pnl_error.Text += ln_error_code.ToString() + "<br>"
        End If
        Return False
    End Function
#End Region
End Class
