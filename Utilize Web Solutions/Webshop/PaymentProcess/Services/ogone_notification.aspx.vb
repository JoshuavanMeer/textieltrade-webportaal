﻿
Partial Class Webshop_PaymentProcess_PaymentProviders_worldpay_notification
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private NCERROR As String = ""
    Private ORDERID As String = ""
    Private PAYID As String = ""
    Private STATUS As String = ""
    Private PM As String = ""

    Private OrderCode As String = ""
    Private PaymentId As String = ""
    Private PaymentStatus As String = ""
    Private PaymentAmount As String = ""
    Private PaymentCurrency As String = ""
    Private PaymentMethod As String = ""

    Private Function GetPaymentCode() As String
        Dim qc_payment_methods As New Utilize.Data.QueryCustomSelectClass
        With qc_payment_methods
            .QueryString = "SELECT * FROM uws_pay WHERE pay_provider = 'OGONE' and not pay_code = '' "
        End With

        ' Ogone gegevens ophalen
        Dim dt_payment_methods As System.Data.DataTable = qc_payment_methods.execute_query("ogone_notification => ORDERID: " + ORDERID)

        ' als de ogone methode ebstaat
        If dt_payment_methods.Rows.Count > 0 Then
            Return dt_payment_methods.Rows(0).Item("pay_code").ToString().Trim()
        End If

        Return "NIET_AANWEZIG"
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' De onderstaande parameters komen binnen
        NCERROR = Me.get_page_parameter("NCERROR")

        Try
            ORDERID = Me.get_page_parameter("ORDERID").Replace(Me.GetPaymentCode(), "")
        Catch ex As Exception
            ORDERID = Me.get_page_parameter("ORDERID")
        End Try

        PAYID = Me.get_page_parameter("PAYID")
        STATUS = Me.get_page_parameter("STATUS")
        PM = Me.get_page_parameter("PM")

        'nu kijken we naar de ID van de order in de webshop. als deze order met 20 begint is deze opgebouwd vanuit Account view
        'als dit het geval is moet het veldje AV_SYNC op false staan.

        ' Dus alleen strings die beginnen met 20 geven een true terug bij controle.
        If ORDERID.StartsWith("PR") Then
            If (STATUS = "9" Or STATUS = "5") Then
                'De order code begint met een 20 de av_sync moet op false en we halen het uit
                Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_so_orders", ORDERID.Replace("PR", ""), "av_ord_nr")
                ' Initialiseer en laad het business object
                ubo_orders.bus_init()
                ubo_orders.bus_load()
                ubo_orders.field_set("uws_so_orders.av_sync", False)
                ubo_orders.field_set("uws_so_orders.payment_id", PAYID)
                ubo_orders.field_set("uws_so_orders.payment_type", PM)
                ubo_orders.field_set("uws_so_orders.payment_online", True)
                ubo_orders.table_update("ogone_notification => ORDERID: " + ORDERID)

                Try
                    Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                    WsAvConn.GetWebshopInformation()
                Catch ex As Exception

                End Try
            End If
        End If

        If Not ORDERID.StartsWith("PR") And (STATUS = "9" Or STATUS = "5") Then
            'is geen externe av bestelling.
            ' order object aanmaken
            Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "ogone_id")

            ' Initialiseer en laad het business object
            ubo_orders.bus_init()
            ubo_orders.bus_load()

            Dim lc_payment_id As String = Me.GetPaymentCode() + ubo_orders.field_get("uws_orders.payment_id")

            'de Ogone order id meegeven aan de webwinkel order
            ubo_orders.field_set("uws_orders.payment_id", lc_payment_id)
            ubo_orders.table_update("ogone_notification => ORDERID: " + ORDERID)

            ubo_orders.api_execute_method("create_purchase_order", ubo_orders, "uws_orders")

            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If

        'Select Case STATUS
        '    Case "1"
        '        Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "rec_id")
        '        ubo_orders.table_delete_row("uws_orders", "ogone_notification => ORDERID: " + ORDERID)
        '    Case "2"
        '        Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "rec_id")
        '        ubo_orders.table_delete_row("uws_orders", "ogone_notification => ORDERID: " + ORDERID)
        '    Case "93"
        '        Dim ubo_orders As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", ORDERID, "rec_id")
        '        ubo_orders.table_delete_row("uws_orders", "ogone_notification => ORDERID: " + ORDERID)
        '    Case Else
        '        Exit Select
        'End Select
    End Sub
End Class
