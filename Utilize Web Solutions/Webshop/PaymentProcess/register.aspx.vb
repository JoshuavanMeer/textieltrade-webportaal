﻿
Partial Class register
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_register As Webshop.ws_user_control = LoadUserControl("~/Webshop/PaymentProcess/PageBlocks/uc_register.ascx")
        ph_zone.Controls.Add(uc_register)
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class