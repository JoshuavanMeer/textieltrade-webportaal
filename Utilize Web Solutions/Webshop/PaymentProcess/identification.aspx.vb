﻿
Partial Class identification
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")

    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_identification As Webshop.ws_user_control = LoadUserControl("PageBlocks/uc_identification.ascx")
        ph_zone.Controls.Add(uc_identification)
    End Sub

End Class