﻿
Partial Class check_order
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        If Not Me.IsPostBack() Then
            Dim ll_move_next As Boolean = Me.global_ws.shopping_cart.ubo_shopping_cart.move_first("uws_order_lines")

            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Me.global_ws.shopping_cart.ubo_shopping_cart

            Do While ll_move_next
                Dim ln_ord_qty As Decimal = ubo_bus_obj.field_get("uws_order_lines.unit_qty")
                ubo_bus_obj.field_set("uws_order_lines.unit_qty", ln_ord_qty, "/prog")
                ubo_bus_obj.api_field_updated("uws_order_lines.prod_price", ubo_bus_obj, "/prog")
                ubo_bus_obj.api_field_updated("uws_order_lines.row_amt", ubo_bus_obj, "/prog")
                ubo_bus_obj.api_field_updated("uws_order_lines.vat_amt", ubo_bus_obj, "/prog")

                ll_move_next = Me.global_ws.shopping_cart.ubo_shopping_cart.move_next("uws_order_lines")
            Loop

            ubo_bus_obj.api_field_updated("uws_orders.order_total_tax", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.order_total", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.vat_total", ubo_bus_obj, "/prog")

            ubo_bus_obj.api_field_updated("uws_orders.order_costs", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.vat_ord", ubo_bus_obj, "/prog")

            ubo_bus_obj.api_field_updated("uws_orders.shipping_costs", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.vat_ship", ubo_bus_obj, "/prog")
        End If

        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_single_checkout As Webshop.ws_user_control

        If global_ws.get_webshop_setting("use_single_chkout_v2") Then
            uc_single_checkout = LoadUserControl("PageBlocks/uc_single_checkout_v2.ascx")
        Else
            uc_single_checkout = LoadUserControl("PageBlocks/uc_single_checkout.ascx")
        End If

        ph_zone.Controls.Add(uc_single_checkout)
    End Sub
End Class