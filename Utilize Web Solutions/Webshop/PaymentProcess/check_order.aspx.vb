﻿
Partial Class check_order
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")

        If Me.global_ws.user_information.user_logged_on And Me.global_ws.get_webshop_setting("checkout_type") = 1 Then
            Response.Redirect(Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/single_checkout.aspx"), True)
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        If Not Me.IsPostBack() Then
            Dim ll_move_next As Boolean = Me.global_ws.shopping_cart.ubo_shopping_cart.move_first("uws_order_lines")

            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Me.global_ws.shopping_cart.ubo_shopping_cart

            Do While ll_move_next
                Dim ln_ord_qty As Decimal = ubo_bus_obj.field_get("uws_order_lines.ord_qty")
                ubo_bus_obj.field_set("uws_order_lines.ord_qty", ln_ord_qty)

                ll_move_next = Me.global_ws.shopping_cart.ubo_shopping_cart.move_next("uws_order_lines")
            Loop

            ubo_bus_obj.api_field_updated("uws_orders.order_total_tax", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.order_total", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.vat_total", ubo_bus_obj, "/prog")

            ubo_bus_obj.api_field_updated("uws_orders.order_costs", ubo_bus_obj, "/prog")
            ubo_bus_obj.api_field_updated("uws_orders.shipping_costs", ubo_bus_obj, "/prog")
        End If

        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_check_order As Webshop.ws_user_control = LoadUserControl("PageBlocks/uc_check_order.ascx")
        ph_zone.Controls.Add(uc_check_order)
    End Sub
End Class