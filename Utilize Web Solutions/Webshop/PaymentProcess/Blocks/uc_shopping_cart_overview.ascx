﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_shopping_cart_overview.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_shopping_cart_overview" %>

<%@ Register Src="~/webshop/Modules/ProductInformation/uc_order_product.ascx" tagname="uc_order_product" tagprefix="uc1" %>
<%@ Register Src="~/webshop/Modules/ProductInformation/uc_customer_product_codes.ascx" tagname="uc_customer_product_codes" tagprefix="uc2" %>

<%--WINKELWAGEN--%>
<utilize:placeholder runat="server" ID="ph_shopping_cart">
    <utilize:panel runat="server" ID="pnl_shopping_cart" CssClass="uc_shopping_cart margin-bottom-10" DefaultButton="button_recalculate_prices">
        <utilize:placeholder runat="server" ID="ph_items">
            <table class="lines table">
                <utilize:repeater runat="server" ID="rpt_shopping_cart" pager_enabled="true" page_size="10" pagertype="top">
                    <%--RIJ MET KOLOMTITELS--%>
                    <HeaderTemplate>
                        <thead>
                            <tr>
                                <utilize:placeholder runat="server" ID="ph_image">
                                    <th class="image"></th>
                                </utilizE:placeholder>
                                <th class="code"><span class="hidden-xs"><utilize:translateliteral runat="server" ID="col_product_code" Text="Productcode"></utilize:translateliteral> & </span><utilize:translateliteral runat="server" ID="col_product_description" Text="Omschrijving"></utilize:translateliteral></th>
                                <utilize:placeholder runat='server' ID="ph_size">
                                    <th class="size"><utilize:translateliteral runat="server" ID="col_product_size" Text="Maat"></utilize:translateliteral></th>
                                </utilize:placeholder>
                                  <utilize:placeholder runat="server" ID="ph_selected_employee" Visible='<%#current_user_is_manager_or_superuser() %>'>
                                    <th class="employee"><utilize:translateliteral runat="server" ID="col_product_employee" Text="Medewerker"></utilize:translateliteral></th>
                                </utilize:placeholder>
                                <utilize:placeholder runat='server' ID="ph_product_price">
                                    <th class="price">
                                        <utilize:translateliteral runat="server" ID="col_product_price" Text="Prijs" Visible="false"></utilize:translateliteral>
                                        <utilize:translateliteral runat="server" ID="col_product_credits" Text="Credits" Visible="false"></utilize:translateliteral>
                                    </th>
                                </utilize:placeholder>
                          
                                <th class="quantity"><utilize:translateliteral runat="server" ID="col_product_quantity" Text="Aantal"></utilize:translateliteral></th>
                                                       
                                <utilize:placeholder runat='server' ID="ph_row_total">
                                    <th class="amount"><utilize:translateliteral runat="server" ID="col_product_row_total" Text="Totaal"></utilize:translateliteral></th>
                                </utilize:placeholder>
                            </tr>
                        </thead>
                        <tbody>
                    </HeaderTemplate>
                    
                    <%--WINKELWAGEN ARTIKELEN--%>
                    <ItemTemplate>
                        <utilize:literal runat="server" ID="lt_rec_id" Text='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' Visible="false"></utilize:literal>
                        <tr class="normal">
                            <utilize:placeholder runat="server" ID="ph_image">
                                <td class="image" runat="server" id="td_image">
                                    <utilize:image CssClass="img-responsive" runat="server" ID="img_product" ImageUrl='<%# Me.get_image(DataBinder.Eval(Container.DataItem, "prod_code")) %>' />
                                </td>
                            </utilize:placeholder>
                            <td class="code product-in-table" runat='server' id="td_product" data-content-before='<%# global_trans.translate_label("col_product", "Product", Me.global_ws.Language) %>'>
                                <div class="product-it-in">
                                    <h3>
                                        <a href="<%# Me.ResolveCustomUrl("~" + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")) %>"><utilize:literal runat="server" ID="lt_prod_desc" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:literal></a>
                                    </h3>
                                    <h5>
                                        <utilize:literal runat="server" ID="lt_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' Visible='<%# Me.global_ws.get_webshop_setting("prod_code_hide") = False %>'></utilize:literal>
                                    </h5>
                                    <utilize:placeholder Visible='<%# Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_units") And Not DataBinder.Eval(Container.DataItem, "unit_code") = "" %>' runat="server" ID="ph_unit">
                                        <div class="row_comments">
                                            <h5><utilize:literal runat="server" ID="lt_unit" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_units", "unit_desc_" + Me.global_ws.Language, DataBinder.Eval(Container.DataItem, "unit_code")) + " (" + global_trans.translate_label("label_unit_content", "Inhoud", Me.global_ws.Language) + ": " + String.Format("{0:G29}", DataBinder.Eval(Container.DataItem, "ord_qty") / DataBinder.Eval(Container.DataItem, "unit_qty")).ToString() + " "%>'></utilize:literal><utilize:translateliteral runat="server" Text="Stuks" ID="label_unit_pieces"></utilize:translateliteral>) </div></h2>
                                    </utilize:placeholder>


                                    <utilize:placeholder  Visible="false" runat="server" ID="ph_comments">
                                        <div class="row_comments hidden-xs">
                                            <utilize:literal runat='server' ID="lt_product_comment" Text='<%# DataBinder.Eval(Container.DataItem, "comment")%>'></utilize:literal>
                                            <utilize:textbox runat="server" ID="txt_product_comment" MaxLength="200" Text='<%# DataBinder.Eval(Container.DataItem, "comment")%>' CssClass="input form-control margin-bottom-5"></utilize:textbox>
                                        </div>
                                    </utilize:placeholder> 


                                    <utilize:placeholder  Visible='<%# DataBinder.Eval(Container.DataItem, "project_location") <> ""%>' runat="server" ID="ph_project_location">
                                            <br /><utilize:translatelabel runat="server" ID="label_project_location" Text="Project locatie"></utilize:translatelabel><utilize:literal runat='server' ID="lt_project_location" Text='<%# ": " + Utilize.Data.DataProcedures.GetValue("uws_project_location", "location_name", DataBinder.Eval(Container.DataItem, "project_location")) %>'></utilize:literal>
                                    </utilize:placeholder>
                                    
                                    <utilize:placeholder runat="server" ID="ph_customer_product_codes" Visible="false">
                                        <div class="hidden-xs customer-product-codes">
                                            <uc2:uc_customer_product_codes ID="uc_customer_product_codes1" runat="server" product_code='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' />
                                        </div>
                                    </utilize:placeholder>
                                </div>
                            </td>

                            <utilize:placeholder runat='server' ID="ph_size">
                                <td class="size" data-content-before='<%# global_trans.translate_label("col_product_size", "Maat", Me.global_ws.Language) %>'><utilize:literal runat="server" ID="lt_size" Text='<%# DataBinder.Eval(Container.DataItem, "size")%>'></utilize:literal></td>
                            </utilize:placeholder>

                             <utilize:placeholder runat='server' ID="ph_row_employee" Visible='<%#current_user_is_manager_or_superuser() %>'>
                                <td class="employee" runat="server" ID="employee_cell" data-content-before='<%# global_trans.translate_label("col_product_employee", "Medewerker", Me.global_ws.Language) %>'><utilize:literal runat="server" ID="lt_employee"></utilize:literal></td>
                            </utilize:placeholder>

                            <utilize:placeholder runat='server' ID="ph_product_price">
                                <td class="price" data-content-before='<%# global_trans.translate_label("col_product_price", "Prijs", Me.global_ws.Language) %>'>
                                    <utilize:literal runat="server" ID="lt_prod_price_original" Visible="false"></utilize:literal>
                                    <utilize:literal runat="server" ID="lt_prod_price"></utilize:literal>
                                </td>
                            </utilize:placeholder>
                            
                            <td class="quantity" data-content-before='<%# global_trans.translate_label("col_product_quantity", "Aantal", Me.global_ws.Language) %>'>
                                <uc1:uc_order_product ID="uc_order_product1" visible ="false" runat="server" unit_code='<%# DataBinder.Eval(Container.DataItem, "unit_code")%>' product_code='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' Enabled="false" Value='<%# DataBinder.Eval(Container.DataItem, "ord_qty")%>' />                                
                            </td>

                            <utilize:placeholder runat='server' ID="ph_row_total">
                                <td class="amount shop-red" data-content-before='<%# global_trans.translate_label("col_product_row_total", "Totaal", Me.global_ws.Language) %>'><utilize:literal runat="server" ID="lt_row_amount" ></utilize:literal></td>
                            </utilize:placeholder>

                            <utilize:placeholder runat='server' ID="ph_delete_row">
                                <td class="remove_row" data-content-before='<%# global_trans.translate_label("col_delete_row", "Verwijder", Me.global_ws.Language) %>'>
                                            <utilize:linkbutton runat="server" CssClass="close" ID="link_delete_row" text="x" CommandName="delete_row" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' />
                                            <utilize:checkbox runat="server" ID="chk_cpn_product" Visible="false" Checked='<%# DataBinder.Eval(Container.DataItem, "cpn_product") %>' />
                                </td>
                            </utilize:placeholder>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </tbody>
                    </FooterTemplate>
                </utilize:repeater>
            </table>
        </utilize:placeholder>
    
        <%--KNOPPEN--%>
    <div class="wrapper-re">
        <div class="buttons pull-right">
            <utilize:translatebutton runat='server' ID="button_recalculate_prices" Text="Herberekenen" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </div>
    </utilize:panel>
</utilize:placeholder>

<%--WINKELWAGEN LEEG--%>
<utilize:placeholder runat="server" ID="ph_shopping_cart_empty">
    <utilize:translatetext runat="server" ID="text_shopping_cart_no_items" Text="Er zijn geen producten in uw winkelwagen geplaatst."></utilize:translatetext>    
</utilize:placeholder>
