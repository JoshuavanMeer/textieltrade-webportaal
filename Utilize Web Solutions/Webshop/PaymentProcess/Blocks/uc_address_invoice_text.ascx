﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_address_invoice_text.ascx.vb" Inherits="uc_address_invoice" %>
<div class="uc_address_invoice">
    <h2 class="title-type"><utilize:translatetitle runat="server" ID="title_invoice_address" Text="Factuuradres"/></h2>
    <div class="billing-info-inputs checkbox-list">
        <utilize:literal runat='server' ID="lt_invoice_address"></utilize:literal>
        <br style="clear:both" />
        <utilize:hyperlink runat='server' ID="link_change_invoice_address" tekst="Factuuradres wijzigen" CssClass="hyperlink" />
    </div>
</div>

