﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_process_indicator.ascx.vb" Inherits="uc_process_indicator" %>
<div class="uc_process_indicator">
    <utilize:translatelabel runat="server" ID="label_process_order_content" Text="Inhoud van uw bestelling"></utilize:translatelabel>
    <utilize:translatelabel runat="server" ID="label_process_registration" Text="Registratie / identificatie"></utilize:translatelabel>
    <utilize:translatelabel runat="server" ID="label_process_check_order" Text="Controleer uw bestelling"></utilize:translatelabel>
    <utilize:translatelabel runat="server" ID="label_process_pay_order" Text="Bestelling afronden"></utilize:translatelabel>
</div>