﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_delivery_methods.ascx.vb" Inherits="uc_delivery_methods" %>
<div class="uc_delivery_methods">
    <div class="row">
        <div class="col-md-8">
            <h3><utilize:translatetitle runat="server" ID="title_delivery_method" Text="Verzendmethode"></utilize:translatetitle></h3>
            <utilize:translatetext runat="server" ID="text_delivery_method" Text="Geef rechts aan op welke manier u de bestelling wilt laten versturen."></utilize:translatetext>
        </div>
        <div class="col-md-4">
            <utilize:panel runat="server" ID="pnl_delivery_methods" CssClass="deliverymethods">
                <utilize:optiongroup runat="server" ID="opg_delivery_methods" AutoPostBack="true">
        
                </utilize:optiongroup>
            </utilize:panel>
        </div>
    </div>
</div>