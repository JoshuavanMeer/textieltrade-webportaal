﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_address_block.ascx.vb" Inherits="uc_address_block" %>

<div class="col-md-12 margin-bottom-10 uc_address_block">
    <div class="input-border">
        <utilize:placeholder runat="server" ID="ph_address_book_select" Visible='false'>
            <div class="row sky-space-20">
                <div class="col-md-8 col-md-offset-0 margin-bottom-15">
                    <label><utilize:translateliteral ID="label_select_delivery_address" runat="server" Text="Afleveradres kiezen"></utilize:translateliteral></label>                              
                    <utilize:dropdownlist ID="cbo_select_address" runat="server" Cssclass="value select_address form-control" AutoPostBack='true'></utilize:dropdownlist>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:checkbox runat="server" ID="chk_change_delivery_address" AutoPostBack="true" CssClass="inline" />

        <utilize:placeholder runat="server" ID="ph_address_information">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0 margin-bottom-15">
                        <label><utilize:translateliteral ID="label_address_country" runat="server" Text="Land"></utilize:translateliteral></label>                              
                        <utilize:dropdownlist ID="cbo_address_country" runat="server" data-google-api="country" Cssclass="value country form-control" AutoPostBack="true"></utilize:dropdownlist>                    
                    </div>
                </div>

            <utilize:placeholder runat='server' ID="ph_bus_name">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_bus_name" runat="server" Text="Bedrijfsnaam"></utilize:translateliteral></label>  
                        <label class="input login-input no-border-top">
                            <div class="input-group"> 
                                <utilize:textbox ID="txt_address_bus_name" data-google-api="search" runat="server" Cssclass="value bus_name form-control"></utilize:textbox>
                                <utilize:placeholder runat='server' ID="ph_bus_name_mandatory">
                                        <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </utilize:placeholder>
                            </div>
                        </label>
                    </div>
                </div>

            </utilize:placeholder>
                <div class="row sky-space-20">
                    <div class="col-md-3 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_fst_name" runat="server" Text="Voornaam"></utilize:translateliteral></label>  
                    
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                            
                                <utilize:textbox ID="txt_address_fst_name" runat="server" Cssclass="value fst_name form-control"></utilize:textbox>
                                <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                            </div>
                        </label>
                    </div>


                    <div class="col-md-2 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_infix" runat="server" Text="Tussenvoegsel"></utilize:translateliteral></label>  
                        <utilize:textbox ID="txt_address_infix" runat="server" CssClass="value infix form-control"></utilize:textbox>
                    </div>

                    <div class="col-md-3 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_lst_name" runat="server" Text="Achternaam"></utilize:translateliteral></label>  
                    
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                            
                                <utilize:textbox ID="txt_address_lst_name" runat="server" Cssclass="value lst_name form-control"></utilize:textbox>
                                <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                            </div>
                        </label>
                    </div>
                </div>

            <utilize:placeholder runat="server" ID="ph_address_0">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_address" runat="server" Text="Adres"></utilize:translateliteral></label>  
                    
                            <label class="input login-input no-border-top">
                                <div class="input-group">
                                    
                                    <utilize:textbox ID="txt_address_address" runat="server" data-google-api="address" Cssclass="value address form-control" MaxLength="80"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </div>
                            </label>

                    </div>
                </div>
            </utilize:placeholder>


            <utilize:placeholder runat="server" ID="ph_address_1">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_street" runat="server" Text="Straat"></utilize:translateliteral></label>  
                        <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <utilize:textbox ID="txt_address_street1" runat="server" data-google-api="street" Cssclass="value street form-control" MaxLength="80"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </div>
                        </label>
                    </div>
                </div>

                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_housenr" runat="server" Text="Huisnr."></utilize:translateliteral></label>  

                        <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <utilize:textbox ID="txt_address_housenr1" runat="server" data-google-api="street_number" Cssclass="value housenr form-control" MaxLength="10"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </div>
                        </label>
                    </div>
                </div>

                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_housenr_add" runat="server" Text="Toev."></utilize:translateliteral></label>  
                        <utilize:textbox ID="txt_address_houseadd1" runat="server" Cssclass="value housenradd form-control" MaxLength="10"></utilize:textbox>
                        <br style="clear:both;" />
                    </div>
                </div>

            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_address_2">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:literal ID="label_address_housenr1" runat="server" Text="Huisnr."></utilize:literal></label>  

                        <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <utilize:textbox ID="txt_address_housenr2" runat="server" data-google-api="street_number" Cssclass="value housenr form-control" MaxLength="10"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </div>
                        </label>

                    </div>
                </div>

                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:literal ID="label_address_street1" runat="server" Text="Straat"></utilize:literal></label> 
                        
                        <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <utilize:textbox ID="txt_address_street2" runat="server" data-google-api="street" Cssclass="value street form-control" MaxLength="80"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>
                                </div>
                        </label>
                         
                    </div>
                </div>

                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:literal ID="label_address_housenr_add1" runat="server" Text="Toev."></utilize:literal></label>  
                        <utilize:textbox ID="txt_address_houseadd2" runat="server" Cssclass="value housenradd form-control" MaxLength="10"></utilize:textbox>
                    </div>
                </div>
            </utilize:placeholder>

                <div class="row sky-space-20">
                    <div class="col-md-3 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_zip_code" runat="server" Text="Postcode"></utilize:translateliteral></label>                  
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                            
                                <utilize:textbox ID="txt_address_zip_code" runat="server" data-google-api="postalcode" Cssclass="value zip_code form-control" style="text-transform: uppercase"></utilize:textbox>
                                <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req" ></i></span>  
                            </div>
                        </label>
                    </div>

                    <div class="col-md-5 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_city" runat="server" Text="Plaats"></utilize:translateliteral></label>     
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                                <utilize:textbox ID="txt_address_city" runat="server" data-google-api="city" Cssclass="value city form-control"></utilize:textbox>  
                                <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>  
                            </div>
                        </label>
                    </div>           
                </div>

            <utilize:placeholder runat='server' ID="ph_phone_number">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_phonenumber" runat="server" Text="Telefoonnummer"></utilize:translateliteral></label>  
                    
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                                <utilize:textbox ID="txt_address_phonenumber" runat="server" data-google-api="phonenumber" Cssclass="value phonenumber form-control"></utilize:textbox>
                                
                                <utilize:placeholder runat='server' ID="ph_phonenumber_mandatory">
                                    <span class="input-group-addon">
                                                    <i class="icon-prepend fa fa-asterisk req"></i>
                                    </span>
                                </utilize:placeholder>
                            </div>
                        </label>
                    </div>
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat='server' ID="ph_cell_phone_number" Visible="false">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_address_cell_phonenumber" runat="server" Text="Mobiel nummer"></utilize:translateliteral></label>  
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                                <utilize:textbox ID="txt_address_cell_phonenumber" runat="server" Cssclass="value phonenumber form-control"></utilize:textbox>
                            </div>
                        </label>
                    </div>
                </div>
            </utilize:placeholder>

             <utilize:placeholder runat='server' ID="ph_vat_number" Visible="false">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_vat_number" runat="server" Text="BTW Nummer"></utilize:translateliteral></label>  
                        <div class="input-group">
                        <utilize:textbox ID="txt_vat_number" runat="server" Cssclass="value vat_number form-control" MaxLength="20"></utilize:textbox>
                        <utilize:placeholder runat='server' ID="ph_vat_number_mandatory" Visible="false">
                            <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                            </utilize:placeholder>
                    </div>
                    </div>
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_coc_number" Visible="false">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_coc_number" runat="server" Text="KvK Nummer"></utilize:translateliteral></label>  
                        <utilize:textbox ID="txt_coc_number" runat="server" Cssclass="value coc_number form-control" MaxLength="80"></utilize:textbox>
                    </div>
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_website" Visible="false">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_website" runat="server" Text="Uw website"></utilize:translateliteral></label>  
                        <utilize:textbox ID="txt_website" runat="server" Cssclass="value website form-control" MaxLength="80"></utilize:textbox>
                    </div>
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_email_address" Visible="false">
                <div class="row sky-space-20">
                    <div class="col-md-8 col-md-offset-0">
                        <label><utilize:translateliteral ID="label_email_address" runat="server" Text="Email adres"></utilize:translateliteral></label>  

                        <label class="input login-input no-border-top">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-prepend fa fa-envelope"></i></span>
                                <utilize:textbox ID="txt_email_address" runat="server" Cssclass="value email_address form-control" MaxLength="80"></utilize:textbox>
                                
                                <utilize:placeholder runat="server" ID="ph_email_address_mandatory" Visible="false">
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                                </utilize:placeholder>
                            </div>
                        </label>
                    </div>
                </div>
            </utilize:placeholder>


            <utilize:placeholder runat="server" ID="ph_user_information">
                <utilize:placeholder runat='server' ID="ph_login_code">
                    <div class="row sky-space-20">
                        <div class="col-md-8 col-md-offset-0">
                            <label><utilize:translateliteral ID="label_login_code" runat="server" Text="Login code"></utilize:translateliteral><span class="mandatory_sign">&nbsp;*</span><br style="clear:both;" /></label>  
                            <utilize:textbox ID="txt_login_code" runat="server" Cssclass="value login_code form-control" MaxLength="80"></utilize:textbox>
                        </div>
                    </div>
                </utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_create_account" Visible="false">
                    <utilize:checkbox ID="chk_create_account" runat="server" Cssclass="value create_account" AutoPostBack="true"></utilize:checkbox>
                    <br style="clear: both;" />
                </utilize:placeholder>


                <utilize:placeholder runat='server' ID="ph_create_account_details">                    
                    <div class="row sky-space-20">
                        <div class="col-md-4 col-md-offset-0">
                            <label><utilize:translateliteral ID="label_login_password" runat="server" Text="Wachtwoord"></utilize:translateliteral></label>  

                            <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-lock"></i></span>
                                    <utilize:textbox ID="txt_login_password" runat="server" Cssclass="value login_password form-control" MaxLength="20" TextMode="Password"></utilize:textbox>
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                                </div>
                            </label>
                        
                        </div>        

                        <div class="col-md-4 col-md-offset-0">                
                            <label><utilize:translateliteral ID="label_login_password_confirm" runat="server" Text="Bevestig het wachtwoord"/></label>  

                            <label class="input login-input no-border-top">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-lock"></i></span>
                                    <utilize:textbox ID="txt_login_password_confirm" runat="server" CssClass="value login_password_confirm form-control" MaxLength="20" TextMode="Password" />
                                    <span class="input-group-addon"><i class="icon-prepend fa fa-asterisk req"></i></span>
                                </div>
                            </label>

                        
                        </div>                            
                    </div>
                    <utilize:placeholder runat="server" ID="ph_format_error" Visible="false">
                        <div class="alert alert-danger">
                            <utilize:translatelabel runat="server" ID="lbl_missing_format" text="Uw wachtwoord moet voldoen aan de volgende eisen:"></utilize:translatelabel>
                            <ul>
                                <li><utilize:translateliteral runat="server" ID="lt_min_char" Text="Minimaal 6 tekens lang"></utilize:translateliteral></li>
                                <li><utilize:translateliteral runat="server" ID="lt_max_char" Text="Maximaal 128 tekens lang"></utilize:translateliteral></li>
                                <utilize:placeholder runat="server" ID="ph_uppercase"><li><utilize:translateliteral runat="server" ID="lt_uppercase" Text="Minimaal 1 hoofdletter"></utilize:translateliteral></li></utilize:placeholder>  
                                <utilize:placeholder runat="server" ID="ph_lowercase"><li><utilize:translateliteral runat="server" ID="lt_lowercase" Text="Minimaal 1 lowercase letter"></utilize:translateliteral></li></utilize:placeholder>  
                                <utilize:placeholder runat="server" ID="ph_digit"><li><utilize:translateliteral runat="server" ID="lt_digit" Text="Minimaal 1 getal"></utilize:translateliteral></li></utilize:placeholder>  
                                <utilize:placeholder runat="server" ID="ph_special_chars"><li><utilize:translateliteral runat="server" ID="lt_special_chars" Text="Minimaal 1 speciaal karakter"></utilize:translateliteral></li></utilize:placeholder>                             
                            </ul>
                        </div>
                    </utilize:placeholder>
                </utilize:placeholder>
            </utilize:placeholder>
       
            <utilize:placeholder runat="server" ID="ph_address_book_save" Visible='false'>
                <utilize:checkbox runat="server" ID="chk_save_delivery_address" />
            </utilize:placeholder>

            <div class="row sky-space-20">
                <div class="col-md-8 col-md-offset-0">
                    <utilize:checkbox Visible="true" runat="server" ID="chk_register_newsletter" CssClass="inline"/>
                </div>
            </div>
        </utilize:placeholder>
    </div>
    
    <utilize:alert ID="message_error" AlertType="danger" runat="server" Text="Een of meer verplichte velden zijn niet gevuld." Visible="false" ></utilize:alert>
</div>

