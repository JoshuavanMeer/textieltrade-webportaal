﻿
Partial Class uc_process_indicator
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_process_indicator.css", Me)

        Select Case True
            Case Request.Url.ToString.ToLower().Contains("shopping_cart.aspx")
                Me.label_process_order_content.CssClass = "active"
            Case Request.Url.ToString.ToLower().Contains("identification.aspx")
                Me.label_process_check_order.CssClass = "active"
            Case Request.Url.ToString.ToLower().Contains("check_order.aspx")
                Me.label_process_check_order.CssClass = "active"
            Case Request.Url.ToString.ToLower().Contains("finish_order.aspx")
                Me.label_process_pay_order.CssClass = "active"
            Case Else
                Me.Visible = False
        End Select

        If Me.Visible Then
            Me.Visible = Me.global_ws.get_webshop_setting("checkout_type") = 0
        End If
    End Sub
End Class
