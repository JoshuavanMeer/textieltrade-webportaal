﻿Imports System.Linq
Imports System.Data

Partial Class Webshop_CheckOut_Blocks_uc_shopping_cart_totals
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim pg_page As System.Web.UI.Page = Me.Page

        AddHandler pg_page.PreRenderComplete, AddressOf Me.RecalculateQuantities
    End Sub

    Protected Sub RecalculateQuantities()
        Dim ll_testing As Boolean = True

        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        ' Haal hier uit de webshop instellingen op of de prijzen inclusief of exclusief BTW zijn.
        Me.ph_prices_vat_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1 And Not prices_not_visible
        Me.ph_prices_vat_incl.Visible = (Me.global_ws.get_webshop_setting("price_type") = 2 Or Me.global_ws.get_webshop_setting("price_type") = 3) And Not prices_not_visible

        ' Toon de betalingskorting alleen als deze instelling aan staan in de webshop instellingen
        Me.ph_pay_disc_amt_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1 And Me.global_ws.get_webshop_setting("use_payment_discount")
        Me.ph_pay_disc_amt_incl.Visible = Me.global_ws.get_webshop_setting("price_type") = 2 And Me.global_ws.get_webshop_setting("use_payment_discount")

        Me.ph_pay_disc_amt_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 3 And Me.global_ws.get_webshop_setting("use_payment_discount")

        If ll_testing Then
            ll_testing = Me.global_ws.shopping_cart.product_count > 0
            Me.Visible = ll_testing
        End If

        If ll_testing Then
            If Not global_ws.price_mode = Webshop.price_mode.Credits Then

                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ship_del_con") Then
                    Dim delivery_condition_costs = CalculateOrderCosts()
                    Dim vat_pct = GetOrderVatPct() / 100
                    If Me.global_ws.get_webshop_setting("del_con_type") = 1 Then
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("shipping_costs", delivery_condition_costs)
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("vat_ship", delivery_condition_costs * vat_pct)
                    ElseIf Me.global_ws.get_webshop_setting("del_con_type") = 2 Then
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("order_costs", delivery_condition_costs)
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("vat_ord", delivery_condition_costs * vat_pct)
                    End If
                End If


                ' Er moeten bedragen getoond worden.
                Me.ph_amounts.Visible = True

                ' Bedrag betalingskorting
                Me.lt_pay_disc_amt_excl_vat.Text = Format(Me.global_ws.shopping_cart.field_get("pay_disc_amt"), "c")
                Me.lt_pay_disc_amt_incl_vat.Text = Format(Me.global_ws.shopping_cart.field_get("pay_disc_amt") + Me.global_ws.shopping_cart.field_get("pay_disc_vat_amt"), "c")

                Me.lt_coupon_amt_incl_vat.Text = Format(Me.global_ws.shopping_cart.field_get("uws_orders.cpn_amt") + Me.global_ws.shopping_cart.field_get("uws_orders.cpn_vat_amt"), "c")
                Me.lt_coupon_amt_excl_vat.Text = Format(Me.global_ws.shopping_cart.field_get("uws_orders.cpn_amt"), "c")

                Me.lt_order_subtotal_excl_vat.Text = Format(Me.global_ws.shopping_cart.order_subtotal_excl_vat, "c")
                Me.lt_order_subtotal_incl_vat.Text = Format(Me.global_ws.shopping_cart.order_subtotal_incl_vat, "c")

                Me.lt_order_shipping_costs_excl_vat.Text = Format(Me.global_ws.shopping_cart.order_shipping_costs_excl_vat, "c")
                Me.lt_order_shipping_costs_incl_vat.Text = Format(Me.global_ws.shopping_cart.order_shipping_costs_incl_vat, "c")

                Me.lt_order_costs_excl_vat.Text = Format(Me.global_ws.shopping_cart.order_costs_excl_vat, "c")
                Me.lt_order_costs_incl_vat.Text = Format(Me.global_ws.shopping_cart.order_costs_incl_vat, "c")

                Me.lt_order_vat_total.Text = Format(Me.global_ws.shopping_cart.order_vat_total, "c")
                Me.lt_order_total_incl_vat.Text = Format(Me.global_ws.shopping_cart.order_total_incl_vat, "c")
                Me.lt_order_total_incl_vat1.Text = Format(Me.global_ws.shopping_cart.order_total_incl_vat, "c")

                Me.ph_order_cost_incl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")
                Me.ph_order_cost_excl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")
            Else
                ' Er moeten credits getoond worden
                Me.ph_credits.Visible = True

                ' Betalingskorting
                Me.lt_pay_disc_amt_excl_vat.Text = Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.field_get("pay_disc_amt"))
                Me.lt_pay_disc_amt_incl_vat.Text = Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.field_get("pay_disc_amt") + Me.global_ws.shopping_cart.field_get("pay_disc_vat_amt"))

                Me.lt_credits_order.Text = Format(Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.order_subtotal_excl_vat), "N0")
                Me.lt_credits_available.Text = Format(global_ws.user_information.ubo_customer.field_get("uws_customers_users.credits_available"), "N0")
                Dim ln_credits_left As Decimal = global_ws.user_information.ubo_customer.field_get("uws_customers_users.credits_available") - Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.order_subtotal_excl_vat)

                Me.lt_credits_left.Text = Format(ln_credits_left, "N0")

                ' Als de gebruiker een manager is, dan kan er in het totaal niet aangegeven worden hoeveel credits er nog over zijn
                Me.lt_credits_available.Visible = Me.global_ws.purchase_combination.user_is_manager = False
                Me.total_credits_available.Visible = Me.global_ws.purchase_combination.user_is_manager = False

                Me.lt_credits_left.Visible = Me.global_ws.purchase_combination.user_is_manager = False
                Me.total_credits_left.Visible = Me.global_ws.purchase_combination.user_is_manager = False
            End If
        End If

        Me.set_style_sheet("uc_shopping_cart_totals.css", Me)

        Me.ph_coupon_excl.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_coupons") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ext_coupons")
        Me.ph_coupon_incl.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_coupons") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ext_coupons")
    End Sub


    Private Function CalculateOrderCosts() As Decimal
        Dim qsc_ As New Utilize.Data.QuerySelectClass
        qsc_.select_fields = "*"
        qsc_.select_from = "uws_del_con_costs"
        qsc_.select_where = " customer_id = '" + Me.global_ws.user_information.user_customer_id + "'"
        Dim del_condition = Me.global_ws.user_information.ubo_customer.field_get("del_condition")
        If Not String.IsNullOrWhiteSpace(del_condition) Then
            qsc_.select_where += " or condition_code = '" + del_condition + "'"
        End If
        Dim conditions = qsc_.execute_query().AsEnumerable _
            .Select(Function(x) New DeliveryCondition With {.DebtorId = x.Item("customer_id").ToString(), .DeliveryCondition = x.Item("condition_code").ToString(),
            .OrderAmout = x.Item("order_amt"), .OrderCosts = x.Item("ship_cost")}) _
            .ToList() _
            .Where(Function(x) x.OrderAmout <= Me.global_ws.shopping_cart.order_subtotal_excl_vat) _
            .ToList()

        If conditions.Count = 0 Then
            Return 0
        End If

        Dim selectedConditions = conditions.Where(Function(x) Not String.IsNullOrEmpty(x.DebtorId)).ToList().OrderBy(Function(x) x.OrderAmout)
        If selectedConditions.Count = 0 Then
            selectedConditions = conditions.OrderBy(Function(x) x.OrderAmout)
        End If
        Return selectedConditions.Last().OrderCosts
    End Function

    Public Function GetOrderVatPct() As Decimal
        Dim vat_code = Me.global_ws.shopping_cart.field_get("vat_code_ord")
        Dim vat_pct = 21
        If Not String.IsNullOrWhiteSpace(vat_code) Then
            Dim qsc As New Utilize.Data.QuerySelectClass
            qsc.select_fields = "vat_pct"
            qsc.select_from = "uws_vat"
            qsc.select_where = "vat_code = '" + vat_code + "'"
            vat_pct = qsc.execute_query().AsEnumerable().Select(Function(x) x.Item("vat_pct").ToString()).First()
        End If
        Return vat_pct
    End Function

End Class

Public Class DeliveryCondition
    Public Property DeliveryCondition As String
    Public Property OrderAmout As Decimal
    Public Property OrderCosts As Decimal
    Public Property DebtorId As String
End Class
