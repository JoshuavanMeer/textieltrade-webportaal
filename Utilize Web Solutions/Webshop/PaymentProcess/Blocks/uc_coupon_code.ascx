﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_coupon_code.ascx.vb" Inherits="uc_coupon_code" %>
<div class="uc_coupon_code col-md-6 sm-margin-bottom-30">
    <div class="col-md-12">
        <div class="row">
            <utilize:translatetext runat="server" ID="text_coupon_code" Text="Heeft u een couponcode, vul deze dan hieronder in:"></utilize:translatetext>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <utilize:translateliteral runat="server" ID="label_coupon_code" Text="couponcode" Visible="false"></utilize:translateliteral>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <section>
                <label class="input login-input">
                    <utilize:panel runat="server" ID="pnl_input" DefaultButton="button_apply_coupon_code" CssClass="input-group">
                        <utilize:textbox runat="server" ID="txt_coupon_code" CssClass="form-control"></utilize:textbox>
                        <utilize:linkbutton Glyphicon="glyphicon glyphicon-ok" runat="server" ID="button_apply_coupon_code" CssClass="input-group-addon btn-default btn-apply" />
                        <utilize:linkbutton Glyphicon="glyphicon glyphicon-remove" runat="server" ID="button_remove_coupon_code" CssClass="input-group-addon btn-default btn-remove" visible="false" />
                    </utilize:panel>
                </label>
            </section>
        </div>
    </div>
    <div class="col-md-12 margin-top-20">
        <div class="row">
            <utilize:alert runat="server" runat="server" ID="message_coupon_status"></utilize:alert>
        </div>
    </div>
</div>