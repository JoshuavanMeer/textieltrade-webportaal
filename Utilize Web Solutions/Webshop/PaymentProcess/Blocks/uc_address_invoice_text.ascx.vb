﻿
Partial Class uc_address_invoice
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_address_invoice_text.css", Me)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.lt_invoice_address.Text = Me.global_ws.shopping_cart.get_invoice_adress()
        Me.link_change_invoice_address.ToolTip = global_trans.translate_text("tooltip_change_invoice_address", "Wijzig hier het factuuradres van de order", Me.global_cms.Language)
    End Sub
End Class
