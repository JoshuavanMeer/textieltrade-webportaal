﻿
Imports System.Data

Partial Class uc_coupon_code
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Sub uc_coupon_code_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Me.button_apply_coupon_code.Text = global_trans.translate_button("button_apply_coupon_code", "Toepassen", Me.global_ws.Language)
        ' Me.button_remove_coupon_code.Text = global_trans.translate_button("button_remove_coupon_code", "Verwijderen", Me.global_ws.Language)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.txt_coupon_code.Attributes.Add("placeholder", Me.label_coupon_code.Text)
        End If

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_coupons") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ext_coupons")
        End If

        If ll_resume Then
            ll_resume = Me.global_ws.shopping_cart.product_count > 0
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_coupon_code.css", Me)
        End If

        If ll_resume Then
            Me.txt_coupon_code.Text = Me.global_ws.shopping_cart.field_get("cpn_code")

            Me.txt_coupon_code.Enabled = Me.global_ws.shopping_cart.field_get("cpn_code") = ""
            Me.button_apply_coupon_code.Visible = Me.global_ws.shopping_cart.field_get("cpn_code") = ""
            Me.button_remove_coupon_code.Visible = Not Me.button_apply_coupon_code.Visible

            If Me.button_remove_coupon_code.Visible Then
                Me.pnl_input.DefaultButton = "button_remove_coupon_code"
            End If

            If Not Me.global_ws.shopping_cart.field_get("cpn_code") = "" Then
                Me.message_coupon_status.AlertType = alerttype.succes
                Me.message_coupon_status.Text = global_trans.translate_message("message_coupon_applied", "De bovenstaande couponcode is toegepast op deze bestelling.", Me.global_ws.Language)
                Me.message_coupon_status.CssClass = ""
                Me.message_coupon_status.Font.Bold = True
            End If
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If

    End Sub

    Protected Sub button_apply_coupon_code_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_apply_coupon_code.Click
        Me.message_coupon_status.Visible = False
        Dim ll_resume As Boolean = True

        ' Er zit een bug in het kiezen voor de terug knop en het toepassen
        If ll_resume And Not Me.global_ws.shopping_cart.field_get("cpn_code") = "" Then
            Me.txt_coupon_code.Text = Me.global_ws.shopping_cart.field_get("cpn_code")

            ll_resume = False

            If Not ll_resume Then
                Me.message_coupon_status.AlertType = alerttype.danger
                Me.message_coupon_status.Visible = True
                Me.message_coupon_status.Text = global_trans.translate_message("message_coupon_code_applied", "Er is reeds een couponcode op de bestelling toegepast.", Me.global_ws.Language)
            End If
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_coupon_code.Text.Trim() = ""

            If Not ll_resume Then
                ' Me.message_coupon_status.AlertType = alerttype.danger
                ' Me.message_coupon_status.Visible = True
                ' Me.message_coupon_status.Text = global_trans.translate_message("message_coupon_status", "Een of meer verplichte velden zijn niet gevuld.", Me.global_ws.Language)
            End If
        End If

        If ll_resume Then
            Me.global_ws.shopping_cart.field_set("cpn_code", Me.txt_coupon_code.Text.ToUpper())

            ll_resume = Not Me.global_ws.shopping_cart.field_get("cpn_code").ToString() = ""

            If Not ll_resume Then
                Me.txt_coupon_code.Text = ""
                Me.message_coupon_status.AlertType = alerttype.danger
                Me.message_coupon_status.Visible = True
                If Me.global_ws.shopping_cart.ubo_shopping_cart.error_message = "De ingevulde couponcode is niet bruikbaar op deze order." Then
                    Me.message_coupon_status.Text = global_trans.translate_message("message_coupon_code_not_usable", "De ingevulde couponcode is niet bruikbaar op deze order.", Me.global_ws.Language)
                Else
                    Me.message_coupon_status.Text = global_trans.translate_message("message_coupon_code_invalid", "De ingevulde couponcode is niet (meer) geldig.", Me.global_ws.Language)
                End If

            End If
        End If

        If ll_resume Then
            Response.Redirect(FrameWork.FrameworkProcedures.GetRequestUrl())
        End If
    End Sub

    Protected Sub button_remove_coupon_code_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_remove_coupon_code.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim couponcode As String = Me.global_ws.shopping_cart.field_get("cpn_code").ToString()
            Dim product_cpn_recid As String = ""

            For Each row As DataRow In Me.global_ws.shopping_cart.product_table.Table.Rows
                If row.Item("cpn_product") = True Then
                    product_cpn_recid = row.Item("rec_id")
                End If
            Next

            If Not String.IsNullOrEmpty(product_cpn_recid) Then
                Me.global_ws.shopping_cart.product_delete_by_id(product_cpn_recid)
            End If
        End If

        If ll_resume Then
            Me.global_ws.shopping_cart.field_set("cpn_code", "")
            Me.global_ws.shopping_cart.field_set("cpn_product_added", False)
        End If

        If ll_resume Then
            Me.message_coupon_status.Visible = False
            Me.txt_coupon_code.Text = ""
        End If

        If ll_resume Then
            Response.Redirect(FrameWork.FrameworkProcedures.GetRequestUrl())
        End If
    End Sub
End Class
