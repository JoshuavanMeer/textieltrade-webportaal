﻿Partial Class uc_delivery_methods
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Controleer of de module actief is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ship_meth")
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_delivery_methods.css", Me)
        End If

        If ll_resume And Not Me.IsPostBack() Then
            Dim lc_sg_code As String = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sg_code")

            If Not lc_sg_code = "" Then
                Dim qsc_ship_meth As New Utilize.Data.QuerySelectClass
                qsc_ship_meth.select_fields = "uws_ship_meth_lines.sm_code, uws_ship_methods.sm_desc"
                qsc_ship_meth.select_from = "uws_ship_meth_lines"
                qsc_ship_meth.select_where = "uws_ship_meth_lines.sg_code = @sg_code"
                qsc_ship_meth.add_join("left outer join", "uws_ship_methods", "uws_ship_meth_lines.sm_code = uws_ship_methods.sm_code")

                qsc_ship_meth.add_parameter("sg_code", lc_sg_code)
                qsc_ship_meth.select_order = "row_ord asc"

                ' Bouw de datatable op
                Dim dt_data_table As System.Data.DataTable = qsc_ship_meth.execute_query()

                Me.opg_delivery_methods.DataSource = dt_data_table
                Me.opg_delivery_methods.DataTextField = "sm_desc"
                Me.opg_delivery_methods.DataValueField = "sm_code"
                Me.opg_delivery_methods.DataBind()

                Dim dr_data_row As System.Data.DataRow() = dt_data_table.Select("sm_code = '" + Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sm_code") + "'")

                If dr_data_row.Length = 0 Then
                    If dt_data_table.Rows.Count > 0 Then
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("sm_code", dt_data_table.Rows(0).Item("sm_code"))
                    Else
                        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("sm_code", "")
                    End If
                Else
                    Me.opg_delivery_methods.SelectedValue = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sm_code")
                End If
            End If
        End If
    End Sub

    Protected Sub opg_delivery_methods_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles opg_delivery_methods.SelectedIndexChanged
        Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("sm_code", Me.opg_delivery_methods.SelectedValue)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim lc_sg_code As String = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sg_code")

        If Not lc_sg_code = "" Then
            Dim qsc_ship_meth As New Utilize.Data.QuerySelectClass
            qsc_ship_meth.select_fields = "uws_ship_meth_lines.sm_code, uws_ship_methods.sm_desc"
            qsc_ship_meth.select_from = "uws_ship_meth_lines"
            qsc_ship_meth.select_where = "uws_ship_meth_lines.sg_code = @sg_code"
            qsc_ship_meth.add_join("left outer join", "uws_ship_methods", "uws_ship_meth_lines.sm_code = uws_ship_methods.sm_code")

            qsc_ship_meth.add_parameter("sg_code", lc_sg_code)
            qsc_ship_meth.select_order = "row_ord asc"

            ' Bouw de datatable op
            Dim dt_data_table As System.Data.DataTable = qsc_ship_meth.execute_query()

            Me.opg_delivery_methods.DataSource = dt_data_table
            Me.opg_delivery_methods.DataTextField = "sm_desc"
            Me.opg_delivery_methods.DataValueField = "sm_code"
            Me.opg_delivery_methods.DataBind()

            Dim dr_data_row As System.Data.DataRow() = dt_data_table.Select("sm_code = '" + Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sm_code") + "'")

            If dr_data_row.Length = 0 Then
                If dt_data_table.Rows.Count > 0 Then
                    Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("sm_code", dt_data_table.Rows(0).Item("sm_code"))
                Else
                    Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("sm_code", "")
                End If
            Else
                Me.opg_delivery_methods.SelectedValue = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("sm_code")
            End If
        End If

        Me.Visible = Me.opg_delivery_methods.Items.Count > 1
    End Sub
End Class
