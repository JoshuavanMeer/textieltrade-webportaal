﻿
Partial Class uc_order_description
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_order_description.css", Me)

        Me.ph_customer_reference.Visible = Me.global_ws.get_webshop_setting("show_order_desc")
        Me.ph_customer_comments.Visible = Me.global_ws.get_webshop_setting("show_order_comm")
        Me.txt_customer_comments.Attributes.Add("style", "width:100%;")

        Me.txt_customer_comments.Attributes.Add("placeholder", Me.label_customer_comments.Text)
        Me.txt_customer_reference.Attributes.Add("placeholder", Me.label_customer_reference.Text)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.Visible = Me.ph_customer_comments.Visible Or Me.ph_customer_reference.Visible
    End Sub

    Public Property customer_reference As String
        Get
            Return txt_customer_reference.Text
        End Get
        Set(ByVal value As String)
            txt_customer_reference.Text = value
        End Set
    End Property
    Public Property customer_comments As String
        Get
            Return txt_customer_comments.Text
        End Get
        Set(ByVal value As String)
            txt_customer_comments.Text = value
        End Set
    End Property

End Class


