﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_description.ascx.vb" Inherits="uc_order_description" %>
<div class="uc_order_description">
    <div class="col-md-12">
        <div class="row">
            <h3><utilize:translatetitle runat="server" ID="title_customer_references" Text="Leg hieronder eventuele extra gegevens vast"></utilize:translatetitle></h3>
    
            <utilize:placeholder runat="server" ID="ph_customer_reference" Visible="false">
            
                    <utilize:translateliteral runat='server' ID="label_customer_reference" Text="Uw referentie" Visible="false"></utilize:translateliteral>
                    <utilize:textbox runat="server" ID="txt_customer_reference" MaxLength="40" CssClass="form-control input"></utilize:textbox>
                    <br />
            </utilize:placeholder>
            <utilize:placeholder runat='server' ID="ph_customer_comments" Visible="false">

                    <utilize:translateliteral runat='server' ID="label_customer_comments" Text="Uw opmerkingen" Visible="false"></utilize:translateliteral>
                    <utilize:textarea runat="server" ID="txt_customer_comments" CssClass="form-control textarea"></utilize:textarea>
            
            </utilize:placeholder>
    </div>
    </div>
</div>