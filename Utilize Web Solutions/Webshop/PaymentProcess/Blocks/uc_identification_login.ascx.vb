﻿Imports System.Linq

Partial Class uc_identification_login
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub button_login_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_login.Click
        Dim ll_resume As Boolean = True

        Try
            If ll_resume Then
                ' Probeer in te loggen
                ll_resume = Me.global_ws.user_information.login(0, Me.txt_logon_name.Text, Me.txt_logon_password.Text)

                If Not ll_resume Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('" + Me.global_ws.user_information.error_message + "');", True)
                End If
            End If

            If ll_resume Then
                ' Hier controleren we of een gebruiker geblokkeerd is of niet.
                ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers", "blocked", global_ws.user_information.user_customer_id))

                If Not ll_resume Then
                    Me.global_ws.user_information.logout()

                    Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
                End If
            End If

            If ll_resume Then
                ' Hier controleren we of een gebruiker geblokkeerd is of niet.
                ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "blocked", global_ws.user_information.user_id))

                If Not ll_resume Then
                    Me.global_ws.user_information.logout()

                    Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
                End If
            End If

            If ll_resume Then
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/check_order.aspx", True)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_identification_login.css", Me)

        txt_logon_name.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_name", "Gebruikersnaam", Me.global_cms.Language))
        txt_logon_password.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_password", "Wachtwoord", Me.global_cms.Language))

        Me.link_forgot_password.Text = global_trans.translate_button("link_forgot_password", "Wachtwoord vergeten?", Me.global_cms.Language)
        Me.link_forgot_password.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/paymentprocess/request_password.aspx"

        Me.txt_logon_name.Focus()
    End Sub
End Class
