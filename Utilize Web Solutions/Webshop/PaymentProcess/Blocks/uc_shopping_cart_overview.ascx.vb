﻿Imports Utilize.Data

Partial Class Webshop_CheckOut_Blocks_uc_shopping_cart_overview
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _show_delete_item As Boolean = True
    Private _show_quantity_box As Boolean = True

    Private _order_entry As Boolean = false
    Public property order_entry As Boolean
        Get
            Return _order_entry
        End Get
        Set(value As Boolean)
            _order_entry = value
        End Set
    End Property

#Region "Properties"
    Public Property show_delete_item() As Boolean
        Get
            Return _show_delete_item
        End Get
        Set(ByVal value As Boolean)
            _show_delete_item = value
        End Set
    End Property
    Public Property show_quantity_box() As Boolean
        Get
            Return _show_quantity_box
        End Get
        Set(ByVal value As Boolean)
            _show_quantity_box = value
        End Set
    End Property
#End Region

#Region "Page subroutines"
    Private price_type As Integer = 0
    ' 0 = Excl BTW
    ' 1 = Incl BTW

    Private volume_discount_type As Integer = 1
    ' 0 = Prijs van/voor
    ' 1 = Alleen staffelprijs
    ' 2 = Staffelweergave
    ' 3 = Staffelweergave - Alleen prijzen

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.price_type = Me.global_ws.get_webshop_setting("price_type")

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_px_list") Then
            Me.volume_discount_type = Me.global_ws.get_webshop_setting("volume_discount_type")
        End If

        Me.block_css = False
        Me.set_style_sheet("uc_shopping_cart_overview.css", Me)

        If global_ws.get_webshop_setting("use_single_chkout_v2") Then
            Me.set_style_sheet("~/Styles/AdvancedStyles/uc_shopping_cart_overview_v2.css", Me)
        End If

        Me.button_recalculate_prices.Visible = Me.show_quantity_box And Not Me.global_ws.get_webshop_setting("hide_recalculate")

        Me.rpt_shopping_cart.DataSource = Me.global_ws.shopping_cart.product_table
        Me.rpt_shopping_cart.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Als het aantal besteld is gewijzigd, dan moeten we gaan herberekenen
        If Me.postback_control.Contains("txt_order_qty") Or Me.postback_control.Contains("txt_quantity") Then
            For Each rpt_item As System.Web.UI.WebControls.RepeaterItem In Me.rpt_shopping_cart.Items
                Dim uc_order_product As uc_order_product = rpt_item.FindControl("uc_order_product1")

                Dim chk_cpn_product As Object = rpt_item.FindControl("chk_cpn_product")
                If chk_cpn_product IsNot Nothing Then
                    If Not CType(chk_cpn_product, checkbox).Checked Then
                        Dim lc_rec_id As String = CType(rpt_item.FindControl("lt_rec_id"), literal).Text
                        Dim lc_comment As String = CType(rpt_item.FindControl("txt_product_comment"), textbox).Text
                        Dim ln_quantity As Integer = 0

                        Select Case True
                            Case Me.postback_control.EndsWith("txt_order_qty")
                                ln_quantity = CInt(Request.Form(uc_order_product.FindControl("txt_order_qty").UniqueID))
                            Case Me.postback_control.EndsWith("txt_quantity")
                                ln_quantity = CInt(Request.Form(uc_order_product.FindControl("txt_quantity").UniqueID))
                            Case Else
                                Exit Select
                        End Select

                        ' Update het product in de winkelwagen
                        If Not Me.global_ws.shopping_cart.product_update(lc_rec_id, uc_order_product.Value, lc_comment) Then
                            Exit For
                        End If
                    End If
                End If
            Next

            Me.rpt_shopping_cart.DataSource = Me.global_ws.shopping_cart.product_table
            Me.rpt_shopping_cart.DataBind()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.rpt_shopping_cart.DataSource = Me.global_ws.shopping_cart.product_table
        Me.rpt_shopping_cart.DataBind()

        ' Afhankelijk van artikelen in shopping cart
        If Not _order_entry Then
            Me.ph_shopping_cart.Visible = Me.global_ws.shopping_cart.product_count > 0
            Me.ph_shopping_cart_empty.Visible = Me.global_ws.shopping_cart.product_count = 0
        Else
            Me.ph_shopping_cart.Visible = true
            Me.ph_shopping_cart_empty.Visible = false
        End If        

        If Me.ph_shopping_cart_empty.Visible Then
            ' Hier gaan we zorgen dat de standaard tekst op de pagina niet getoond wordt als deze beschikbaar is.
            Dim lo_text As Utilize.Web.Solutions.Translations.translatetext = Me.Parent.FindControl("text_shopping_cart")

            If Not lo_text Is Nothing Then
                lo_text.Visible = False
            End If
        End If
    End Sub
#End Region

#Region "Functions"
    Friend Function get_image(ByVal product_code As String) As String
        Dim lc_return_image As String = Utilize.Data.DataProcedures.GetValue("uws_products", "thumbnail_normal", product_code)

        Return Webshop.ws_images.get_image_path(lc_return_image, "thumbnail_normal", Me)
    End Function
    Private Function get_quantity() As ArrayList
        Dim al_quantity As New ArrayList

        For i As Integer = 1 To 19
            al_quantity.Add(i.ToString())
        Next
        al_quantity.Add("anders")
        Return al_quantity
    End Function

    Public Function recalculate_order_quantity(ByVal sender As Object, ByVal e As System.EventArgs) As Boolean
        For Each rpt_item As System.Web.UI.WebControls.RepeaterItem In Me.rpt_shopping_cart.Items
            Dim uc_order_product As uc_order_product = rpt_item.FindControl("uc_order_product1")

            Dim chk_cpn_product As Object = rpt_item.FindControl("chk_cpn_product")
            If chk_cpn_product IsNot Nothing Then
                If Not CType(chk_cpn_product, checkbox).Checked Then
                    ' Haal de maat op
                    Dim lc_rec_id As String = CType(rpt_item.FindControl("lt_rec_id"), literal).Text
                    Dim lc_comment As String = CType(rpt_item.FindControl("lt_product_comment"), literal).Text
                    Dim ln_ord_qty As Decimal = uc_order_product.Value

                    If CType(rpt_item.FindControl("txt_product_comment"), textbox).Visible Then
                        lc_comment = CType(rpt_item.FindControl("txt_product_comment"), textbox).Text
                    End If

                    ' Voeg de boel toe aan de winkelwagen
                    If Not Me.global_ws.shopping_cart.product_update(lc_rec_id, uc_order_product.Value, lc_comment) Then
                        Exit For
                    End If
                End If
            End If
        Next

        ' En bouw de repeater opnieuw op
        Me.rpt_shopping_cart.DataBind()

        Return True
    End Function

    Protected Function current_user_is_manager_or_superuser() As Boolean
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And global_ws.user_information.user_logged_on Then

            If global_ws.purchase_combination.user_is_manager Then
                Return True
            End If
            Return False
        End If
        Return False
    End Function
#End Region

#Region "Buttons"
    Protected Sub button_recalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_recalculate_prices.Click
        For Each rpt_item As System.Web.UI.WebControls.RepeaterItem In Me.rpt_shopping_cart.Items
            Dim uc_order_product As uc_order_product = rpt_item.FindControl("uc_order_product1")

            Dim chk_cpn_product As Object = rpt_item.FindControl("chk_cpn_product")
            If chk_cpn_product IsNot Nothing Then
                If Not CType(chk_cpn_product, checkbox).Checked Then
                    Dim lc_rec_id As String = CType(rpt_item.FindControl("lt_rec_id"), literal).Text
                    Dim lc_comment As String = CType(rpt_item.FindControl("lt_product_comment"), literal).Text

                    If CType(rpt_item.FindControl("txt_product_comment"), textbox).Visible Then
                        lc_comment = CType(rpt_item.FindControl("txt_product_comment"), textbox).Text
                    End If

                    Dim ll_resume As Boolean = True

                    If ll_resume Then
                        ' Controleer hier of de credits overschreden worden, of niet.
                        ll_resume = Me.global_ws.purchase_combination.check_limit(Me.global_ws.purchase_combination.user_id, uc_order_product.product_code, uc_order_product.size, uc_order_product.Value)

                        If Not ll_resume Then
                            Select Case global_ws.price_mode
                                Case Webshop.price_mode.Credits
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_credit_reached", "Het maximum aantal toegekende credits wordt overschreden.", Me.global_ws.Language) + "');", True)
                                Case Webshop.price_mode.Money
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_amount_reached", "Het maximum toegekend bedrag wordt overschreden.", Me.global_ws.Language) + "');", True)
                                Case Webshop.price_mode.Unlimited
                                    Exit Select
                                Case Else
                                    Exit Select
                            End Select
                        End If
                    End If

                    If ll_resume Then
                        ll_resume = Me.global_ws.shopping_cart.product_update(lc_rec_id, uc_order_product.Value, lc_comment)
                    End If

                    If Not ll_resume Then
                        Exit For
                    End If
                End If
            End If
        Next

        ' En bouw de repeater opnieuw op
        Me.rpt_shopping_cart.DataSource = Me.global_ws.shopping_cart.product_table 'Me.global_ws.shopping_cart.ubo_shopping_cart.data_tables("uws_order_lines").data_table
        Me.rpt_shopping_cart.DataBind()
    End Sub
#End Region

#Region "Shopping cart items"
    Protected Sub rpt_shopping_cart_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_shopping_cart.ItemCommand
        Select Case e.CommandName.ToLower()
            Case "delete_row"
                Dim lc_rec_id As String = e.CommandArgument

                ' Verwijder de geselecteerde regel
                Me.global_ws.shopping_cart.product_delete_by_id(lc_rec_id)

                ' En bouw de repeater opnieuw op
                Me.rpt_shopping_cart.DataBind()
            Case Else
                Exit Select
        End Select
    End Sub
    Protected Sub rpt_shopping_cart_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_shopping_cart.ItemCreated
        Dim ll_show_prices As Boolean = True

        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
        If prices_not_visible Then
            ll_show_prices = False
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            ' Als er geen afbeeldingen getoond moeten worden in de winkelwagen, maak de kolom dan onzichtbaar
            If Me.global_ws.get_webshop_setting("hide_order_images") Then
                Dim ph_image As placeholder = e.Item.FindControl("ph_image")
                ph_image.Visible = False
            End If


            ' Kolom maat alleen zichtbaar wanneer sizeview aan staat
            e.Item.FindControl("ph_size").Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")

            ' Verberg hier de kolom met prijzen, of maak deze zichtbaar
            e.Item.FindControl("ph_product_price").Visible = ll_show_prices
            e.Item.FindControl("ph_row_total").Visible = ll_show_prices

            If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                e.Item.FindControl("col_product_price").Visible = True
                e.Item.FindControl("col_product_credits").Visible = False
            Else
                e.Item.FindControl("col_product_price").Visible = False
                e.Item.FindControl("col_product_credits").Visible = True
            End If
        End If

        If e.Item.ItemType = System.Web.UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = System.Web.UI.WebControls.ListItemType.Item Then
            Dim uc_order_product As uc_order_product = e.Item.FindControl("uc_order_product1")
            uc_order_product.Enabled = Me._show_quantity_box
            uc_order_product.FindControl("button_order").Visible = False
            uc_order_product.FindControl("button_order1").Visible = False

            ' Als in de webshop instellingen is aangegeven dat de herberekenen knop verborgen moet worden, dan moet de autopostback aangezet worden
            If Me.global_ws.get_webshop_setting("hide_recalculate") Then
                uc_order_product.AutoPostBack = True
            End If

            Dim ph_delete_row As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_delete_row")
            ph_delete_row.Visible = Me._show_delete_item

            Dim link_delete_row As linkbutton = e.Item.FindControl("link_delete_row")
            link_delete_row.OnClientClick = "return confirm('" + global_trans.translate_message("message_delete_shopping_cart_row", "Weet u zeker dat u het geselecteerde product uit uw winkelwagen wilt verwijderen?", Me.global_ws.Language) + "');"

            Dim lt_row_amount As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            Dim lt_prod_price As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_prod_price")
            Dim lt_prod_price_original As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_prod_price_original")

            If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                If Me.price_type = 1 Then
                    'Excl
                    lt_row_amount.Text = Format(e.Item.DataItem("row_amt"), "c")
                    lt_prod_price.Text = Format(e.Item.DataItem("prod_price"), "c")

                    ' Zet hier de oorspronkelijke productprijs
                    lt_prod_price_original.Text = Format(e.Item.DataItem("prod_price_original"), "c")
                    lt_prod_price_original.Visible = e.Item.DataItem("prod_price") < e.Item.DataItem("prod_price_original") And Me.volume_discount_type = 1

                    If lt_prod_price_original.Visible Then
                        lt_prod_price.Text = "<span class=""actual"">" + lt_prod_price.Text + "</span>"
                        lt_prod_price_original.Text = "<span class=""original"">" + lt_prod_price_original.Text + "</span>"
                    End If
                Else
                    ' Bereken hier de te tonen prijzen
                    Dim ln_row_amt_incl_vat As Decimal = e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt")
                    Dim ln_prod_price_incl_vat As Decimal = (e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt")) / e.Item.DataItem("ord_qty")
                    Dim ln_vat_pct As Decimal = 0

                    If e.Item.DataItem("row_amt") > 0 Then
                        ln_vat_pct = (100 / e.Item.DataItem("row_amt")) * e.Item.DataItem("vat_amt")
                    End If

                    'Incl
                    lt_row_amount.Text = Format(ln_row_amt_incl_vat, "c")
                    lt_prod_price.Text = Format(ln_prod_price_incl_vat, "c")

                    ' Bereken hier de oorspronkelijke productprijs incl. BTW
                    Dim ln_prod_price_original As Decimal = e.Item.DataItem("prod_price_original")
                    Dim ln_prod_price_vat_original As Decimal = e.Item.DataItem("vat_amt_original")
                    Dim ln_prod_price_original_incl_vat As Decimal = ln_prod_price_original + ln_prod_price_vat_original

                    lt_prod_price_original.Text = Format(ln_prod_price_original_incl_vat, "c")
                    lt_prod_price_original.Visible = ln_prod_price_incl_vat < ln_prod_price_original_incl_vat And Me.volume_discount_type = 1

                    If lt_prod_price_original.Visible Then
                        lt_prod_price.Text = "<span class=""actual"">" + lt_prod_price.Text + "</span>"
                        lt_prod_price_original.Text = "<span class=""original"">" + lt_prod_price_original.Text + "</span>"
                    End If
                End If
            Else
                If Me.price_type = 1 Then
                    'Excl
                    lt_row_amount.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt"))
                    lt_prod_price.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("prod_price"))
                Else
                    'Incl
                    lt_row_amount.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt"))
                    lt_prod_price.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("prod_price")) + global_ws.convert_amount_to_credits(e.Item.DataItem("vat_amt")) / e.Item.DataItem("ord_qty")
                End If
            End If

            'Als de module prijscombinaties aanstaat moet de geselecteerde medewerker worden weergegeven wanneer de ingelogde gebruiker een manager of een superuser is.

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And global_ws.user_information.user_logged_on Then

                If global_ws.purchase_combination.user_is_manager Then
                    Dim ph_selected_employee As placeholder = e.Item.FindControl("ph_selected_employee")
                    Dim ph_row_employee As placeholder = e.Item.FindControl("ph_row_employee")
                    Dim lt_employee As literal = ph_row_employee.FindControl("lt_employee")

                    'Check of er een user_id in de regel staat.
                    If Not e.Item.DataItem("user_id") = "" Then
                        'lt_employee.Text = get_full_name_for_user_id(e.Item.DataItem("user_id"))
                        lt_employee.Text = Me.global_ws.purchase_combination.user_full_name
                        ph_row_employee.Visible = True
                        'ph_selected_employee.Visible = True
                    Else
                        Dim employee_table_cell As HtmlTableCell = ph_row_employee.FindControl("employee_cell")
                        employee_table_cell.Attributes("class") += " empty-cell"
                    End If

                End If
            End If

            ' Als er met coupons gewerkt wordt en er zijn gratis producten, dan moet voor deze producten de aantallen knop gedisabled worden.
            ' Verder mag deze regel ook niet verwijderd worden.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_coupons") And e.Item.DataItem("cpn_product") Then
                uc_order_product.Enabled = False
                link_delete_row.Visible = False
            End If

            ' Als er geen afbeeldingen getoond moeten worden in de winkelwagen, maak de kolom dan onzichtbaar
            If Me.global_ws.get_webshop_setting("hide_order_images") Then
                Dim ph_image As placeholder = e.Item.FindControl("ph_image")
                ph_image.Visible = False

                Dim td_product As HtmlTableCell = e.Item.FindControl("td_product")
                td_product.Style.Add("text-align", "left")
            End If

            ' Kolom maat alleen zichtbaar wanneer sizeview aan staat
            e.Item.FindControl("ph_size").Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")

            ' Verberg hier de kolom met prijzen, of maak deze zichtbaar
            e.Item.FindControl("ph_product_price").Visible = ll_show_prices
            e.Item.FindControl("ph_row_total").Visible = ll_show_prices

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_row_ref") = True Then
                ' Standaard het panel zichtbaar maken
                e.Item.FindControl("ph_comments").Visible = True

                ' Onderstaand worden de opmerkingen zichtbaar/onzichtbaar gemaakt
                Dim lt_product_comment As literal = e.Item.FindControl("lt_product_comment")
                lt_product_comment.Visible = Not Me._show_delete_item And Not Me._show_quantity_box

                Dim txt_product_comment As textbox = e.Item.FindControl("txt_product_comment")
                txt_product_comment.Visible = Me._show_delete_item And Me._show_quantity_box
                txt_product_comment.Attributes.Add("placeholder", global_trans.translate_label("label_product_comment", "Opmerkingen", Me.global_ws.Language))

                ' Maak hier het comments panel zichtbaar / onzichtbaar
                If txt_product_comment.Visible = False And e.Item.DataItem("comment") = "" Then
                    e.Item.FindControl("ph_comments").Visible = False
                Else
                    e.Item.FindControl("ph_comments").Visible = True
                End If
            End If

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_prod") = True Then
                ' Standaard het panel zichtbaar maken
                e.Item.FindControl("ph_customer_product_codes").Visible = True

                Dim uc_customer_product_codes As uc_customer_product_codes = e.Item.FindControl("uc_customer_product_codes1")
                uc_customer_product_codes.read_only = Not (Me._show_delete_item And Me._show_quantity_box)
            End If

            ' Advanded product filters aka product configurator
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") = True And Not String.IsNullOrEmpty(e.Item.DataItem("set_code")) Then
                Dim ll_product_is_main_product_in_set As Boolean = String.IsNullOrEmpty(e.Item.DataItem("hdr_id_set"))
                Dim ll_product_is_optional_in_set As Boolean = Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_optional_in_set(e.Item.DataItem("prod_code"))

                ' Don't show this control if the product is part of a set.
                e.Item.FindControl("ph_comments").Visible = False

                ' If the product is not a main product in the set.
                If Not ll_product_is_main_product_in_set Then
                    ' Don't show these controls if the product is part of a set, but not the main product.
                    Me.remove_controls_in_placeholder(e.Item.FindControl("ph_product_price"), "price")
                    Me.remove_controls_in_placeholder(e.Item.FindControl("ph_row_total"), "price")

                    If Not ll_product_is_optional_in_set Then
                        Me.remove_controls_in_placeholder(e.Item.FindControl("ph_delete_row"), "remove_row")
                    End If

                    ' Add a class to first table cell if the product is part of a set
                    Dim ph_image As placeholder = e.Item.FindControl("ph_image")
                    If ph_image.Visible = True Then
                        Dim td_image As HtmlTableCell = e.Item.FindControl("td_image")
                        td_image.Attributes.Add("class", "image part-of-set")
                    Else
                        Dim td_product As HtmlTableCell = e.Item.FindControl("td_product")
                        td_product.Attributes.Add("class", "code product-in-table part-of-set")
                    End If
                End If
            End If
        End If
    End Sub
#End Region

    Private sub remove_controls_in_placeholder(byval ph As Utilize.Web.UI.WebControls.placeholder, byval css_class As string)
        ph.Controls.Clear()

        Dim empty_td As New HtmlTableCell
        If Not String.IsNullOrEmpty(css_class) Then
            empty_td.Attributes.Add("class", css_class)
        End If

        ph.Controls.Add(empty_td)
    End sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lc_user_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_full_name_for_user_id(ByVal lc_user_id As String) As String
        If Not lc_user_id = "" Then
            Dim lo_qsc As New QuerySelectClass
            With lo_qsc
                .select_fields = "fst_name, lst_name, infix, title, rec_id"
                .select_from = "uws_customers_users"
                .select_where = "rec_id = @rec_id"
            End With
            lo_qsc.add_parameter("@rec_id", lc_user_id)

            Dim dt_current_user As System.Data.DataTable = lo_qsc.execute_query()

            If dt_current_user.Rows.Count > 0 Then
                Return (dt_current_user.Rows(0).Item("fst_name") + " " + dt_current_user.Rows(0).Item("infix")).trim() + " " + dt_current_user.Rows(0).Item("lst_name")
            Else
                Return "Niet gevonden"
            End If
        End If

        Return ""
    End Function

End Class
