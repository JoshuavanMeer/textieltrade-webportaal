﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_identification_login.ascx.vb" Inherits="uc_identification_login" %>

<utilize:placeholder runat='server' ID="ph_login" Visible="true">
<div class="uc_identification_login margin-bottom-60">
    <h3><utilize:translatetitle runat="server" ID="title_login" Text="Inloggen"></utilize:translatetitle></h3>
    <utilize:translatetext runat="server" ID="text_login" Text="Bent u een bestaande klant, vul dan hieronder uw gegevens in om in te loggen."></utilize:translatetext>

    <utilize:panel runat="server" ID="pnl_login" DefaultButton="button_login" cssclass="border">
        <section>
            <label class="input login-input">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <utilize:textbox ID="txt_logon_name" runat="server" CssClass="txt_name form-control"></utilize:textbox>
                </div>
            </label>
        </section>

        <section>
            <label class="input login-input no-border-top">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <utilize:textbox ID="txt_logon_password" TextMode="Password" runat="server" CssClass="txt_password form-control"></utilize:textbox>
                </div>
            </label>
        </section>
        <section>
            <utilize:translatebutton ID="button_login" UseSubmitBehavior="true" CssClass="btn-u btn-u-sea-shop btn-block margin-bottom-20" Text="Inloggen" runat="server" />
        </section>
        <section class="height-fix">
            <div class="row col-sm-12 margin-bottom-5 text-right ">
                <span class="checkbox sb_remember_me" style="margin-top: 10px!important">
                    <utilize:translatelink runat="server" ID="link_forgot_password" Text="Wachtwoord vergeten?"></utilize:translatelink>
                </span>
            </div>
        </section>
    </utilize:panel>
</div>

</utilize:placeholder>
