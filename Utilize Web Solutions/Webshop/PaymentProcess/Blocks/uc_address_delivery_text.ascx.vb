﻿
Partial Class uc_address_delivery
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_address_delivery_text.css", Me)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.lt_delivery_address.Text = Me.global_ws.shopping_cart.get_delivery_adress()
    End Sub
End Class
