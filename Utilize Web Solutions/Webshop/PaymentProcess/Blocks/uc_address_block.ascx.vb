﻿Imports System.Net.Mail
Imports Utilize.Data
Imports Utilize.Data.API.Webshop

Partial Class uc_address_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _uboCustomer As Utilize.Data.BusinessObject = Nothing
    Public Property uboCustomer As Utilize.Data.BusinessObject
        Get
            If _uboCustomer Is Nothing Then
                Return Me.global_ws.user_information.ubo_customer
            Else
                Return _uboCustomer
            End If
        End Get
        Set(value As Utilize.Data.BusinessObject)
            _uboCustomer = value
        End Set
    End Property

    Private _create_account As Boolean = False
    Public ReadOnly Property create_account As Boolean
        Get
            Return _create_account
        End Get
    End Property

    Private _customer_user_password As String = ""
    ''' <summary>
    ''' Property die password van user geeft als sales module aan staat
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property customer_user_password As String
        Get
            If _sales_module Or Me.global_ws.get_webshop_setting("create_acct_optional") = 2 Then
                Return _customer_user_password
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Private _sales_module As Boolean = False
    ''' <summary>
    ''' Property die aangeeft of de gebruiker via de sales module wordt toegevoegd.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property sales_module As Boolean
        Get
            Return _sales_module
        End Get
        Set(value As Boolean)
            _sales_module = value
        End Set
    End Property

    Public Enum addresstype
        customer_invoice_address = 1
        customer_delivery_address = 2
        shoppingcart_invoice_address = 3
        shoppingcart_delivery_address = 4
    End Enum

    Private _address_type As addresstype = 1
    ''' <summary>
    ''' This property indicates the type of address we are filling
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property address_type As addresstype
        Get
            Return _address_type
        End Get

        Set(ByVal value As addresstype)
            If Not value = 1 Then
                Me._use_login = False
            End If

            _address_type = value
        End Set
    End Property

    Private _use_login As Boolean = False
    ''' <summary>
    ''' This property indicates wether the login information should be shown.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property use_login As Boolean
        Get
            Return _use_login
        End Get
        Set(ByVal value As Boolean)
            If Not _address_type = 1 Then
                _use_login = False
            Else
                _use_login = value
            End If
        End Set
    End Property

    Private _enabled As Boolean = True
    Public Property enabled As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal value As Boolean)
            _enabled = value

            Me.chk_change_delivery_address.Enabled = _enabled
            Me.txt_address_bus_name.Enabled = _enabled
            Me.txt_address_fst_name.Enabled = _enabled
            Me.txt_address_infix.Enabled = _enabled
            Me.txt_address_lst_name.Enabled = _enabled
            Me.txt_address_address.Enabled = _enabled

            Me.txt_address_zip_code.Enabled = _enabled
            Me.txt_address_city.Enabled = _enabled
            Me.txt_email_address.Enabled = _enabled
            Me.txt_address_phonenumber.Enabled = _enabled
            Me.txt_address_cell_phonenumber.Enabled = _enabled
            Me.txt_vat_number.Enabled = _enabled
            Me.txt_coc_number.Enabled = _enabled

            Me.txt_login_password.Enabled = _enabled
            Me.txt_login_password_confirm.Enabled = _enabled

            Me.chk_register_newsletter.Enabled = _enabled

            ' Zet het juiste land
            Me.cbo_address_country.Enabled = _enabled
        End Set
    End Property

    Private _register_type As Integer = 0
    Private _register_phone As Integer = 0

    Private _change_company_info As Integer = 0
    Private _show_vat_number As Boolean = False
    Private _show_coc_number As Boolean = False
    Private _show_website As Boolean = False

#Region " Public address functions"
    ''' <summary>
    ''' This function checks wether all mandatory information is filled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function check_address() As Boolean
        Dim ll_testing As Boolean = True

        Me.ph_format_error.Visible = False
        Me.message_error.Visible = False
        Me.message_error.Text = ""

        ' De adressen moeten alleen gechecked worden als het om een factuuradres gaat of als het om een afleveradres gaat met het vinkje alternatief afleveradres aan.
        If ll_testing And IIf(Me.address_type = addresstype.customer_delivery_address Or Me.address_type = addresstype.shoppingcart_delivery_address, Me.chk_change_delivery_address.Checked = True, True) Then
            ' Controleer eerst alle verplichte velden

            ' De bedrijfsnaam is afhankelijk van de webshop instellingen
            Select Case _register_type
                Case 2
                    ' Niet verplicht
                Case 3
                    ' Niet weergeven
                Case Else
                    ' Verplicht dus controleren of deze is gevuld
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_bus_name.Text.Trim)
                        If Not ll_testing And Me._register_type < 2 Then
                            Me.txt_address_bus_name.CssClass += " error"
                        Else
                            Me.txt_address_bus_name.CssClass = Me.txt_address_bus_name.CssClass.Replace("error", "")
                        End If
                    End If
            End Select

            ' Als het om het factuuradres gaat, dan de onderstaande controle uitvoeren
            If Not Me.address_type = addresstype.customer_delivery_address And Not Me.address_type = addresstype.shoppingcart_delivery_address Then
                Select Case _register_phone
                    Case 2
                        ' Verplicht dus controleren of deze is ingevuld
                        If ll_testing Then
                            ll_testing = Not String.IsNullOrEmpty(Me.txt_address_phonenumber.Text.Trim)
                        End If

                        If Not ll_testing Then

                            If String.IsNullOrEmpty(Me.txt_address_phonenumber.Text.Trim) And Me._register_phone = 2 Then
                                Me.txt_address_phonenumber.CssClass += " error"
                            Else
                                Me.txt_address_phonenumber.CssClass = Me.txt_address_phonenumber.CssClass.Replace("error", "")
                            End If

                            If String.IsNullOrEmpty(Me.txt_address_bus_name.Text.Trim) And Me._register_type < 2 Then
                                Me.txt_address_bus_name.CssClass += " error"
                            Else
                                Me.txt_address_bus_name.CssClass = Me.txt_address_bus_name.CssClass.Replace("error", "")
                            End If

                        ElseIf Not Regex.IsMatch(Me.txt_address_phonenumber.Text, "^[\+]?[0-9]*[\s+]?[(\s*[0-9]*\)*]?[(-|\s)[0-9]*]*$") And Me._register_phone = 2 Then
                            Me.txt_address_phonenumber.CssClass += " error"

                            If String.IsNullOrEmpty(Me.txt_address_bus_name.Text.Trim) And Me._register_type < 2 Then
                                Me.txt_address_bus_name.CssClass += " error"
                            Else
                                Me.txt_address_bus_name.CssClass = Me.txt_address_bus_name.CssClass.Replace("error", "")
                            End If

                            ll_testing = False
                        Else
                            Me.txt_address_phonenumber.CssClass = Me.txt_address_phonenumber.CssClass.Replace("error", "")
                        End If
                    Case 3
                        ' Niet weergeven
                    Case Else

                End Select
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_fst_name.Text.Trim)
                If Not ll_testing Then

                    If String.IsNullOrEmpty(Me.txt_address_fst_name.Text.Trim) Then
                        Me.txt_address_fst_name.CssClass += " error"
                    Else
                        Me.txt_address_fst_name.CssClass = Me.txt_address_fst_name.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_address_fst_name.CssClass = Me.txt_address_fst_name.CssClass.Replace("error", "")
                End If
            End If

            If Not ll_testing Then

                If String.IsNullOrEmpty(Me.txt_address_fst_name.Text.Trim) Then
                    Me.txt_address_fst_name.CssClass += " error"
                Else
                    Me.txt_address_fst_name.CssClass = Me.txt_address_fst_name.CssClass.Replace("error", "")
                End If

                If String.IsNullOrEmpty(Me.txt_address_bus_name.Text.Trim) And Me._register_type < 2 Then
                    Me.txt_address_bus_name.CssClass += " error"
                Else
                    Me.txt_address_bus_name.CssClass = Me.txt_address_bus_name.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_address_fst_name.CssClass = Me.txt_address_fst_name.CssClass.Replace("error", "")
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_lst_name.Text.Trim)

                If Not ll_testing Then
                    If String.IsNullOrEmpty(Me.txt_address_lst_name.Text.Trim) Then
                        Me.txt_address_lst_name.CssClass += " error"
                    Else
                        Me.txt_address_lst_name.CssClass = Me.txt_address_lst_name.CssClass.Replace("error", "")
                    End If
                Else
                    Me.txt_address_lst_name.CssClass = Me.txt_address_lst_name.CssClass.Replace("error", "")
                End If

            End If

            If Not ll_testing Then

                If String.IsNullOrEmpty(Me.txt_address_lst_name.Text.Trim) Then
                    Me.txt_address_lst_name.CssClass += " error"
                Else
                    Me.txt_address_lst_name.CssClass = Me.txt_address_lst_name.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_address_lst_name.CssClass = Me.txt_address_lst_name.CssClass.Replace("error", "")
            End If

            ' Haal de gewenste adres invoer op
            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

            ' Controleer de juiste gegevens
            Select Case ln_address_layout
                Case 0
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_address.Text.Trim)
                        If Not ll_testing Then

                            If String.IsNullOrEmpty(Me.txt_address_address.Text.Trim) Then
                                Me.txt_address_address.CssClass += " error"
                            Else
                                Me.txt_address_address.CssClass = Me.txt_address_address.CssClass.Replace("error", "")
                            End If

                        Else
                            Me.txt_address_address.CssClass = Me.txt_address_address.CssClass.Replace("error", "")
                        End If
                    End If

                    If Not ll_testing Then

                        If String.IsNullOrEmpty(Me.txt_address_address.Text.Trim) Then
                            Me.txt_address_address.CssClass += " error"
                        Else
                            Me.txt_address_address.CssClass = Me.txt_address_address.CssClass.Replace("error", "")
                        End If

                    Else
                        Me.txt_address_address.CssClass = Me.txt_address_address.CssClass.Replace("error", "")
                    End If
                Case 1
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_street1.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_address_street1.CssClass += " error"
                        Else
                            Me.txt_address_street1.CssClass = Me.txt_address_street1.CssClass.Replace("error", "")
                        End If
                    End If

                    If Not ll_testing Then
                        Me.txt_address_street1.CssClass += " error"
                    Else
                        Me.txt_address_street1.CssClass = Me.txt_address_street1.CssClass.Replace("error", "")
                    End If

                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_housenr1.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_address_housenr1.CssClass += " error"
                        Else
                            Me.txt_address_housenr1.CssClass = Me.txt_address_housenr1.CssClass.Replace("error", "")
                        End If
                    End If

                    If Not ll_testing Then
                        Me.txt_address_housenr1.CssClass += " error"
                    Else
                        Me.txt_address_housenr1.CssClass = Me.txt_address_housenr1.CssClass.Replace("error", "")
                    End If
                Case 2
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_street2.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_address_street2.CssClass += " error"
                        Else
                            Me.txt_address_street2.CssClass = Me.txt_address_street2.CssClass.Replace("error", "")
                        End If
                    End If

                    If Not ll_testing Then
                        Me.txt_address_street2.CssClass += " error"
                    Else
                        Me.txt_address_street2.CssClass = Me.txt_address_street2.CssClass.Replace("error", "")
                    End If

                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_housenr2.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_address_housenr2.CssClass += " error"
                        Else
                            Me.txt_address_housenr2.CssClass = Me.txt_address_housenr2.CssClass.Replace("error", "")
                        End If
                    End If

                    If Not ll_testing Then
                        Me.txt_address_housenr2.CssClass += " error"
                    Else
                        Me.txt_address_housenr2.CssClass = Me.txt_address_housenr2.CssClass.Replace("error", "")
                    End If
                Case Else
                    Exit Select
            End Select

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_zip_code.Text.Trim)
                If Not ll_testing Then

                    If String.IsNullOrEmpty(Me.txt_address_zip_code.Text.Trim) Then
                        Me.txt_address_zip_code.CssClass += " error"
                    Else
                        Me.txt_address_zip_code.CssClass = Me.txt_address_zip_code.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_address_zip_code.CssClass = Me.txt_address_zip_code.CssClass.Replace("error", "")
                End If
            End If

            If Not ll_testing Then

                If String.IsNullOrEmpty(Me.txt_address_zip_code.Text.Trim) Then
                    Me.txt_address_zip_code.CssClass += " error"
                Else
                    Me.txt_address_zip_code.CssClass = Me.txt_address_zip_code.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_address_zip_code.CssClass = Me.txt_address_zip_code.CssClass.Replace("error", "")
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_city.Text.Trim)
                If Not ll_testing Then

                    If String.IsNullOrEmpty(Me.txt_address_city.Text.Trim) Then
                        Me.txt_address_city.CssClass += " error"
                    Else
                        Me.txt_address_city.CssClass = Me.txt_address_city.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_address_city.CssClass = Me.txt_address_city.CssClass.Replace("error", "")
                End If
            End If

            If Not ll_testing Then

                If String.IsNullOrEmpty(Me.txt_address_city.Text.Trim) Then
                    Me.txt_address_city.CssClass += " error"
                Else
                    Me.txt_address_city.CssClass = Me.txt_address_city.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_address_city.CssClass = Me.txt_address_city.CssClass.Replace("error", "")
            End If

            'When we have consumer user (customer_user under default debtor) we do not need the vat mandatory when on in the settings
            Dim ll_resume = True
            If DefaultDebtorHelper.check_module() AndAlso Not Me.chk_create_account.Checked Then
                ll_resume = False
            End If

            If ll_resume And ll_testing And Me.global_ws.get_webshop_setting("vat_number_mandatory") And Me.global_ws.get_webshop_setting("show_vat_nr") Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_vat_number.Text.Trim)
                If Not ll_testing Then
                    If String.IsNullOrEmpty(Me.txt_vat_number.Text.Trim) Then
                        Me.txt_vat_number.CssClass += " error"
                    Else
                        Me.txt_vat_number.CssClass = Me.txt_vat_number.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_vat_number.CssClass = Me.txt_vat_number.CssClass.Replace("error", "")
                End If
            End If
            '    ll_testing = True
            'End If

            If ll_resume And Not ll_testing And Me.global_ws.get_webshop_setting("vat_number_mandatory") And Me.global_ws.get_webshop_setting("show_vat_nr") Then

                If String.IsNullOrEmpty(Me.txt_vat_number.Text.Trim) And Me.global_ws.get_webshop_setting("vat_number_mandatory") Then
                    Me.txt_vat_number.CssClass += " error"
                Else
                    Me.txt_vat_number.CssClass = Me.txt_vat_number.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_vat_number.CssClass = Me.txt_vat_number.CssClass.Replace("error", "")
            End If




            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.cbo_address_country.SelectedValue.Trim)
                If Not ll_testing Then
                    Me.cbo_address_country.CssClass += " error"
                Else
                    Me.cbo_address_country.CssClass = Me.cbo_address_country.CssClass.Replace("error", "")
                End If
            End If

            If Not ll_testing Then
                If String.IsNullOrEmpty(Me.cbo_address_country.SelectedValue.Trim) Then
                    Me.cbo_address_country.CssClass += " error"
                Else
                    Me.cbo_address_country.CssClass = Me.cbo_address_country.CssClass.Replace("error", "")
                End If
            Else
                Me.cbo_address_country.CssClass = Me.cbo_address_country.CssClass.Replace("error", "")
            End If

            ' Emailadres alleen controleren als het om een factuuradres gaat.
            If ll_testing And Not Me.address_type = addresstype.customer_delivery_address And Not Me.address_type = addresstype.shoppingcart_delivery_address Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_email_address.Text.Trim)
                If Not ll_testing Then

                    If String.IsNullOrEmpty(Me.txt_email_address.Text.Trim) Then
                        Me.txt_email_address.CssClass += " error"
                    Else
                        Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                End If
            End If

            If Not ll_testing Then

                If String.IsNullOrEmpty(Me.txt_email_address.Text.Trim) Then
                    Me.txt_email_address.CssClass += " error"
                Else
                    Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                End If

            Else
                Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
            End If

            If ll_testing And _use_login Then
                ' Als het registreren niet optioneel is of het vinkje 'Account aanmaken' is aangevinkt, standaard email adres controle
                If Me.global_ws.get_webshop_setting("create_acct_optional") = 0 Or Me.chk_create_account.Checked Then
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_login_password.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_login_password.CssClass += " error"
                        Else
                            Me.txt_login_password.CssClass = Me.txt_login_password.CssClass.Replace("error", "")
                        End If
                    End If

                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_login_password_confirm.Text.Trim)
                        If Not ll_testing Then
                            Me.txt_login_password_confirm.CssClass += " error"
                        Else
                            Me.txt_login_password_confirm.CssClass = Me.txt_login_password_confirm.CssClass.Replace("error", "")
                        End If
                    End If
                End If
            End If

            If Not ll_testing Then
                Me.txt_login_password.CssClass += " error"

            Else
                Me.txt_login_password.CssClass = Me.txt_login_password.CssClass.Replace("error", "")
            End If

            If Not ll_testing Then
                Me.txt_login_password_confirm.CssClass += " error"
            Else
                Me.txt_login_password_confirm.CssClass = Me.txt_login_password_confirm.CssClass.Replace("error", "")
            End If

            If Not ll_testing Then
                Me.message_error.Visible = True
                Me.message_error.Text = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_ws.Language)
            End If
        End If

        If ll_testing And Me.ph_vat_number.Visible And Me.txt_vat_number.Enabled Then
            Dim check_vat_return As Integer = 0
            Dim country_code As String = Me.cbo_address_country.SelectedValue
            check_vat_return = Webshop.ws_procedures.check_vat_number(country_code, Me.txt_vat_number.Text)

            If Not (check_vat_return = 1 Or check_vat_return = -1) Then
                '-1 = no vat check, 0 = vat incorrect, 1 = vat correct Or empty, 2 = service Not available
                ll_testing = False
                Me.message_error.Visible = True
                Me.cbo_address_country.CssClass += " error"

                If check_vat_return = 0 Or check_vat_return = 2 Then
                    Me.message_error.Text = global_trans.translate_message("message_VAT_field_incorrect", "Uw btw nummer kan niet worden gevalideerd.", Me.global_ws.Language)
                End If
            Else
                Me.cbo_address_country.CssClass = Me.cbo_address_country.CssClass.Replace("error", "")
            End If
        End If

        ' UWS-619: Password AVG
        ' Check format first password
        If ll_testing And _use_login And Not Me.global_ws.get_webshop_setting("create_acct_optional") = 2 Then
            If Me.global_ws.get_webshop_setting("create_acct_optional") = 0 Or Me.chk_create_account.Checked Then
                Dim upper As Boolean = global_ws.get_webshop_setting("passw_upper_case")
                Dim lower As Boolean = global_ws.get_webshop_setting("passw_lower_case")
                Dim numeric As Boolean = global_ws.get_webshop_setting("passw_numeric")
                Dim special As Boolean = global_ws.get_webshop_setting("passw_special_char")
                ll_testing = Utilize.Web.Solutions.Webshop.password_check.check_password(Me.txt_login_password.Text, upper, lower, numeric, special)
            End If

            If Not ll_testing Then
                Me.txt_login_password.CssClass += " error"
                Me.ph_format_error.Visible = True
            Else
                Me.txt_login_password.CssClass = Me.txt_login_password.CssClass.Replace("error", "")
            End If
        End If
        ' END UWS-619

        ' Als de logingegevens getoond worden moet daarop gecontroleerd worden
        If ll_testing And Me._use_login Then
            If ll_testing Then
                ' Controleer hier of de wachtwoorden aan elkaar gelijk zijn.
                ll_testing = Me.txt_login_password.Text.Trim() = Me.txt_login_password_confirm.Text.Trim()

                If Not ll_testing Then
                    Me.message_error.Visible = True
                    Me.txt_login_password.CssClass += " error"
                    Me.message_error.Text = global_trans.translate_message("message_passwords_not_equal", "De wachtwoorden zijn niet aan elkaar gelijk.", Me.global_ws.Language)
                Else
                    Me.txt_login_password.CssClass = Me.txt_login_password.CssClass.Replace("error", "")
                End If
            End If
        End If

        ' Als de logingegevens getoond worden moet daarop gecontroleerd worden
        If ll_testing And Me._use_login Then
            If ll_testing Then
                'Doe een basic email adress check
                Try
                    Dim email_address_check As MailAddress = New MailAddress(Me.txt_email_address.Text)
                    ll_testing = True
                Catch ex As Exception
                    ll_testing = False
                End Try

                If Not ll_testing Then
                    Me.message_error.Text = global_trans.translate_message("message_email_address_incorrect", "Uw email adres is niet correct.", Me.global_cms.Language)
                    Me.message_error.Visible = True

                    If String.IsNullOrEmpty(Me.txt_email_address.Text.Trim) Then
                        Me.txt_email_address.CssClass += " error"
                    Else
                        Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                    End If

                Else
                    Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                End If
            End If
        End If

        ' Als de logingegevens getoond worden moet daarop gecontroleerd worden
        If ll_testing And Me._use_login Then
            ' Als de gebruiker niet ingelogd is, dan gaan we controleren of het emailadres al bestaat
            If ll_testing And Not Me.global_ws.user_information.user_logged_on Then
                ' Als het registreren niet optioneel is of het vinkje 'Account aanmaken' is aangevinkt, standaard email adres controle
                If Me.global_ws.get_webshop_setting("create_acct_optional") = 0 Or Me.chk_create_account.Checked Or DefaultDebtorHelper.check_module() Then
                    Dim qsc_customers_users As New Utilize.Data.QuerySelectClass
                    With qsc_customers_users
                        .select_fields = "rec_id"
                        .select_from = "uws_customers_users"
                        .select_where = "(email_address = @email_address or login_code = @login_code) and temp_account = 0"
                    End With
                    qsc_customers_users.add_parameter("email_address", Me.txt_email_address.Text)
                    qsc_customers_users.add_parameter("login_code", Me.txt_email_address.Text)

                    Dim dt_data_table As System.Data.DataTable = qsc_customers_users.execute_query()

                    ll_testing = dt_data_table.Rows.Count = 0

                    If Not ll_testing Then
                        Me.message_error.Visible = True
                        Me.message_error.Text = global_trans.translate_message("message_email_address_exists", "Het emailadres is reeds bekend bij ons.", Me.global_ws.Language)
                        Me.txt_email_address.CssClass += " error"
                    Else
                        Me.txt_email_address.CssClass = Me.txt_email_address.CssClass.Replace("error", "")
                    End If
                End If
            End If
        End If

        Return ll_testing
    End Function
    ''' <summary>
    ''' This function sets the address fields with the neccesary information
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function set_address() As Boolean
        Dim ll_testing As Boolean = True

        Select Case _address_type
            Case addresstype.customer_invoice_address
                ll_testing = Me.set_customer_invoice_address()
            Case addresstype.customer_delivery_address
                ll_testing = Me.set_customer_delivery_address()
            Case addresstype.shoppingcart_invoice_address
                ll_testing = Me.set_shoppingcart_invoice_address()
            Case addresstype.shoppingcart_delivery_address
                ll_testing = Me.set_shoppingcart_delivery_address()
            Case Else
                Exit Select
        End Select

        Return ll_testing
    End Function
    ''' <summary>
    ''' This function saves the address information in the business object but does not update the table.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function save_address() As Boolean
        Dim ll_testing As Boolean = True
        Dim lc_default_debtor As String = Me.global_ws.get_webshop_setting("default_debtor")

        Select Case _address_type
            Case addresstype.customer_invoice_address
                If Not DefaultDebtorHelper.check_module() And Not create_account Then
                    ll_testing = Me.save_customer_invoice_address()

                    If Not Me._sales_module Then
                        ll_testing = Me.save_shoppingcart_invoice_address()
                    End If
                Else
                    Dim ubo_customer_user As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_customers_users")
                    ubo_customer_user.table_insert_row("uws_customers_users", Me.global_ws.user_information.user_id)

                    ubo_customer_user.field_set("uws_customers_users.hdr_id", lc_default_debtor)
                    ubo_customer_user.field_set("uws_customers_users.fst_name", Me.txt_address_fst_name.Text)
                    ubo_customer_user.field_set("uws_customers_users.infix", Me.txt_address_infix.Text)
                    ubo_customer_user.field_set("uws_customers_users.lst_name", Me.txt_address_lst_name.Text)
                    ubo_customer_user.field_set("uws_customers_users.email_address", Me.txt_email_address.Text)
                    ubo_customer_user.field_set("uws_customers_users.login_code", Me.txt_email_address.Text)

                    _customer_user_password = Me.global_ws.generate_password()

                    Dim guid_password As String = Guid.NewGuid().ToString()

                    If Me.uboCustomer.field_get("row_new") OrElse DefaultDebtorHelper.check_module() Then
                        Dim ln_create_acct_optional As Integer = Me.global_ws.get_webshop_setting("create_acct_optional")
                        Select Case ln_create_acct_optional
                            Case 0
                                ' Zelf een account aanmaken
                                ubo_customer_user.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                            Case 1
                                ' Optioneel account aanmaken
                                If Not Me.chk_create_account.Checked Then
                                    ' Zet hier het vinkje welke aangeeft dat het een tijdelijk account is
                                    If DefaultDebtorHelper.check_module() Then
                                        ubo_customer_user.field_set("uws_customers_users.temp_account", False)
                                    Else
                                        ubo_customer_user.field_set("uws_customers_users.temp_account", True)
                                    End If
                                    ubo_customer_user.field_set("uws_customers_users.login_password", guid_password)
                                Else
                                    ' Hier heeft de klant aangegeven dat hij een account aan wil maken
                                    ubo_customer_user.field_set("uws_customers_users.temp_account", False)
                                    ubo_customer_user.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                                End If
                            Case 2
                                ' Automatisch account aanmaken
                                ubo_customer_user.field_set("uws_customers_users.login_password", _customer_user_password)
                                ubo_customer_user.field_set("uws_customers_users.last_password_update", New DateTime(1900, 1, 1))
                            Case Else
                                Exit Select
                        End Select
                    Else
                        If Me.ph_user_information.Visible Then
                            ubo_customer_user.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                            _customer_user_password = Me.txt_login_password.Text
                        End If
                    End If

                    ll_testing = ubo_customer_user.table_update(Me.global_ws.user_information.user_id)

                    If ll_testing Then
                        ll_testing = Me.global_ws.user_information.login(1, Me.txt_email_address.Text, guid_password)
                        If ll_testing Then
                            'Save address details to the shoppingCart
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_bus_name", Me.txt_address_bus_name.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_fst_name", Me.txt_address_fst_name.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_infix", Me.txt_address_infix.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_lst_name", Me.txt_address_lst_name.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_address", Me.txt_address_address.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_house_nr", Me.txt_address_housenr1.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_city", Me.txt_address_city.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_cnt_code", Me.cbo_address_country.SelectedValue)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_phone_nr", Me.txt_address_phonenumber.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_cell_phone_nr", Me.txt_address_cell_phonenumber.Text)
                            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("del_address_custom", True)
                            'Save addres details to customers_addr
                            uboCustomer.table_insert_row("uws_customers_addr", Me.global_ws.user_information.user_id)
                            uboCustomer.field_set("uws_customers_addr.cust_user_rec_id", Me.global_ws.user_information.user_id)
                            uboCustomer.field_set("uws_customers_addr.del_bus_name", Me.txt_address_bus_name.Text)
                            uboCustomer.field_set("uws_customers_addr.del_fst_name", Me.txt_address_fst_name.Text)
                            uboCustomer.field_set("uws_customers_addr.del_infix", Me.txt_address_infix.Text)
                            uboCustomer.field_set("uws_customers_addr.del_lst_name", Me.txt_address_lst_name.Text)
                            uboCustomer.field_set("uws_customers_addr.del_address", Me.txt_address_address.Text)
                            uboCustomer.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr1.Text)
                            uboCustomer.field_set("uws_customers_addr.del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
                            uboCustomer.field_set("uws_customers_addr.del_city", Me.txt_address_city.Text)
                            uboCustomer.field_set("uws_customers_addr.del_cnt_code", Me.cbo_address_country.SelectedValue)
                            'add phone number and cell number to customer_addr
                            uboCustomer.field_set("uws_customers_addr.del_phone_nr", Me.txt_address_phonenumber.Text)
                            uboCustomer.field_set("uws_customers_addr.del_cell_phone_nr", Me.txt_address_cell_phonenumber.Text)
                            ll_testing = uboCustomer.table_update(Me.global_ws.user_information.user_id)
                        End If
                    End If
                End If

            Case addresstype.customer_delivery_address
                ll_testing = Me.save_customer_delivery_address()

                If Not Me._sales_module Then
                    ll_testing = Me.save_shoppingcart_delivery_address()
                End If
            Case addresstype.shoppingcart_invoice_address
                ll_testing = Me.save_shoppingcart_invoice_address()
            Case addresstype.shoppingcart_delivery_address
                If Not DefaultDebtorHelper.user_has_default_debtor() Then
                    ll_testing = Me.save_shoppingcart_delivery_address()
                Else
                    ll_testing = Me.save_shoppingcart_delivery_address_as_default_debtor()
                End If
            Case Else
                Exit Select
        End Select

        Return ll_testing
    End Function

    Private Sub set_mandatory_signs()
        Select Case Me.address_type
            Case addresstype.customer_invoice_address
                Me.ph_email_address_mandatory.Visible = True
                Me.ph_bus_name_mandatory.Visible = Me._register_type < 2
                Me.ph_phonenumber_mandatory.Visible = Me._register_phone = 2
            Case addresstype.customer_delivery_address
                Me.ph_bus_name_mandatory.Visible = Me._register_type < 2
            Case addresstype.shoppingcart_invoice_address
                Me.ph_bus_name_mandatory.Visible = Me._register_type < 2
            Case addresstype.shoppingcart_delivery_address
                Me.ph_bus_name_mandatory.Visible = Me._register_type < 2
            Case Else
                Exit Select
        End Select

        If Me.global_ws.get_webshop_setting("vat_number_mandatory") And Me.global_ws.get_webshop_setting("show_vat_nr") Then
            Me.ph_vat_number_mandatory.Visible = True
        End If

    End Sub
#End Region

#Region " Private address functions"
    Private Function get_countries() As Utilize.Data.DataTable
        ' Haal de landen op
        Dim udt_data_table As New Utilize.Data.DataTable
        With udt_data_table
            .table_name = "uws_countries"
        End With
        udt_data_table.table_init()

        udt_data_table.select_fields = "cnt_code, cnt_desc_" + Me.global_ws.Language + " as cnt_desc"
        udt_data_table.add_where("and cnt_show = @cnt_show")
        udt_data_table.add_where_parameter("cnt_show", True)
        udt_data_table.table_load()

        ' Maak een lege regel aan
        Dim dr_data_row As System.Data.DataRow = udt_data_table.data_table.NewRow()
        dr_data_row.Item("cnt_code") = ""
        dr_data_row.Item("cnt_desc") = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_country_not_selected", "Niet geselecteerd", Me.global_ws.Language)
        udt_data_table.data_table.Rows.InsertAt(dr_data_row, 0)

        Return udt_data_table
    End Function

    Private Function save_customer_invoice_address() As Boolean
        ' Als registreren toegestaan is en het is een nieuwe regel en er mag niet besteld worden wanneer de klant is ingelogd, dan de klant blokkeren
        If Not Me.global_ws.get_webshop_setting("register_enabled") = 2 And uboCustomer.field_get("row_new") Then
            Me.uboCustomer.field_set("blocked", True)
        End If

        If uboCustomer.field_get("row_new") Then
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And Utilize.Data.API.Webshop.IntermediateServiceProcedures.get_setting("generate_external_id") Then
                Me.uboCustomer.field_set("av_nr", Utilize.Data.API.Webshop.IntermediateServiceProcedures.GenerateUserId)
            End If
        End If

        Me.uboCustomer.field_set("bus_name", Me.txt_address_bus_name.Text)
        Me.uboCustomer.field_set("fst_name", Me.txt_address_fst_name.Text)
        Me.uboCustomer.field_set("infix", Me.txt_address_infix.Text)
        Me.uboCustomer.field_set("lst_name", Me.txt_address_lst_name.Text)

        Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

        Select Case ln_address_layout
            Case 0
                Me.uboCustomer.field_set("address", Me.txt_address_address.Text)
                Me.uboCustomer.field_set("street", "")
                Me.uboCustomer.field_set("house_nr", "")
                Me.uboCustomer.field_set("house_add", "")
            Case 1
                Me.uboCustomer.field_set("address", "")
                Me.uboCustomer.field_set("street", Me.txt_address_street1.Text)
                Me.uboCustomer.field_set("house_nr", Me.txt_address_housenr1.Text)
                Me.uboCustomer.field_set("house_add", Me.txt_address_houseadd1.Text)
            Case 2
                Me.uboCustomer.field_set("address", "")
                Me.uboCustomer.field_set("street", Me.txt_address_street2.Text)
                Me.uboCustomer.field_set("house_nr", Me.txt_address_housenr2.Text)
                Me.uboCustomer.field_set("house_add", Me.txt_address_houseadd2.Text)
            Case Else
                Exit Select
        End Select

        Me.uboCustomer.field_set("zip_code", Me.txt_address_zip_code.Text.ToUpper())
        Me.uboCustomer.field_set("city", Me.txt_address_city.Text)
        Me.uboCustomer.field_set("cnt_code", Me.cbo_address_country.SelectedValue)
        Me.uboCustomer.field_set("phone_nr", Me.txt_address_phonenumber.Text)
        Me.uboCustomer.field_set("cell_phone_nr", Me.txt_address_cell_phonenumber.Text)

        If Me.ph_vat_number.Visible Then
            If String.IsNullOrEmpty(Me.txt_vat_number.Text.Trim()) Then
                Me.uboCustomer.field_set("vat_nr", "")
            Else
                Me.uboCustomer.field_set("vat_nr", Me.txt_vat_number.Text.ToUpper())
            End If
        End If

        If Me.ph_coc_number.Visible And Not String.IsNullOrEmpty(Me.txt_coc_number.Text.Trim) Then
            Me.uboCustomer.field_set("coc_nr", Me.txt_coc_number.Text.ToUpper())
        End If

        If Me.ph_website.Visible And Not String.IsNullOrEmpty(Me.txt_website.Text.Trim) Then
            Me.uboCustomer.field_set("website", Me.txt_website.Text.ToUpper())
        End If

        '  Als het een nieuwe gebruiker betreft, dan gaan we het emailadres zetten
        Me.uboCustomer.field_set("email_address", Me.txt_email_address.Text)

        Dim ll_testing As Boolean = True

        ' Maak altijd een nieuwe gebruiker aan
        If Not Me.global_ws.user_information.user_logged_on Then
            Me.uboCustomer.field_set("uws_customers_users.fst_name", Me.txt_address_fst_name.Text)
            Me.uboCustomer.field_set("uws_customers_users.infix", Me.txt_address_infix.Text)
            Me.uboCustomer.field_set("uws_customers_users.lst_name", Me.txt_address_lst_name.Text)

            Me.uboCustomer.field_set("uws_customers_users.email_address", Me.txt_email_address.Text)
            Me.uboCustomer.field_set("uws_customers_users.login_code", Me.txt_email_address.Text)

            _customer_user_password = Me.global_ws.generate_password()

            If Me.uboCustomer.field_get("row_new") OrElse DefaultDebtorHelper.check_module() Then
                Dim ln_create_acct_optional As Integer = Me.global_ws.get_webshop_setting("create_acct_optional")
                Select Case ln_create_acct_optional
                    Case 0
                        ' Zelf een account aanmaken
                        Me.uboCustomer.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                    Case 1
                        ' Optioneel account aanmaken
                        If Not Me.chk_create_account.Checked Then
                            ' Zet hier het vinkje welke aangeeft dat het een tijdelijk account is
                            Me.uboCustomer.field_set("uws_customers_users.temp_account", True)
                            Me.uboCustomer.field_set("uws_customers_users.login_password", Guid.NewGuid().ToString())
                        Else
                            ' Hier heeft de klant aangegeven dat hij een account aan wil maken
                            Me.uboCustomer.field_set("uws_customers_users.temp_account", False)
                            Me.uboCustomer.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                        End If
                    Case 2
                        ' Automatisch account aanmaken
                        Me.uboCustomer.field_set("uws_customers_users.login_password", _customer_user_password)
                        Me.uboCustomer.field_set("uws_customers_users.last_password_update", New DateTime(1900, 1, 1))
                    Case Else
                        Exit Select
                End Select
            Else
                If Me.ph_user_information.Visible Then
                    Me.uboCustomer.field_set("uws_customers_users.login_password", Me.txt_login_password.Text)
                    _customer_user_password = Me.txt_login_password.Text
                End If
            End If
        End If

        ' Als de module aan staat en het vinkje is zichtbaar, dan moeten de gegevens opgeslagen worden in de mailingen 
        If Me.chk_register_newsletter.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_mailings") And ll_testing Then
            Dim lo_mailing_object As BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("ucm_mailings", Me.txt_email_address.Text, "ml_email")

            ' Hier controleren of het emailadres bestaat, zo nee een regel toevoegen met het vinkje "Ja/Nee"
            If lo_mailing_object.data_tables("ucm_mailings").data_row_count = 0 Then 'Maak een categorie Then
                ' De gegevens die in de tabel moeten worden opgeslagen zijn:
                lo_mailing_object.table_insert_row("ucm_mailings", Me.global_ws.user_information.user_id)
                lo_mailing_object.field_set("ml_fst_name", Me.txt_address_fst_name.Text)
                lo_mailing_object.field_set("ml_lst_name", Me.txt_address_lst_name.Text)
                lo_mailing_object.field_set("ml_infix", Me.txt_address_infix.Text)
                lo_mailing_object.field_set("ml_email", Me.txt_email_address.Text)
                lo_mailing_object.field_set("ml_wants_mailing", Me.chk_register_newsletter.Checked)

            Else ' Zo ja, de regel bijwerken met de keuze in de checkbox
                lo_mailing_object.field_set("ml_fst_name", Me.txt_address_fst_name.Text)
                lo_mailing_object.field_set("ml_lst_name", Me.txt_address_lst_name.Text)
                lo_mailing_object.field_set("ml_infix", Me.txt_address_infix.Text)
                lo_mailing_object.field_set("ml_wants_mailing", Me.chk_register_newsletter.Checked)
            End If

            ll_testing = lo_mailing_object.table_update(Me.global_ws.user_information.user_id)
        End If

        Return ll_testing
    End Function
    Private Function save_customer_delivery_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me.uboCustomer.field_set("del_address_custom", Me.chk_change_delivery_address.Checked)
            Me.uboCustomer.field_set("del_bus_name", Me.txt_address_bus_name.Text)
            Me.uboCustomer.field_set("del_fst_name", Me.txt_address_fst_name.Text)
            Me.uboCustomer.field_set("del_infix", Me.txt_address_infix.Text)
            Me.uboCustomer.field_set("del_lst_name", Me.txt_address_lst_name.Text)

            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", cbo_address_country.SelectedValue)
            Select Case ln_address_layout
                Case 0
                    Me.uboCustomer.field_set("del_address", Me.txt_address_address.Text)
                    Me.uboCustomer.field_set("del_street", "")
                    Me.uboCustomer.field_set("del_house_nr", "")
                    Me.uboCustomer.field_set("del_house_add", "")
                Case 1
                    Me.uboCustomer.field_set("del_address", "")
                    Me.uboCustomer.field_set("del_street", Me.txt_address_street1.Text)
                    Me.uboCustomer.field_set("del_house_nr", Me.txt_address_housenr1.Text)
                    Me.uboCustomer.field_set("del_house_add", Me.txt_address_houseadd1.Text)
                Case 2
                    Me.uboCustomer.field_set("del_address", "")
                    Me.uboCustomer.field_set("del_street", Me.txt_address_street2.Text)
                    Me.uboCustomer.field_set("del_house_nr", Me.txt_address_housenr2.Text)
                    Me.uboCustomer.field_set("del_house_add", Me.txt_address_houseadd2.Text)
                Case Else
                    Exit Select
            End Select

            Me.uboCustomer.field_set("del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
            Me.uboCustomer.field_set("del_city", Me.txt_address_city.Text)
            Me.uboCustomer.field_set("del_cnt_code", Me.cbo_address_country.SelectedValue)
        End If

        Return ll_testing
    End Function
    Private Function save_shoppingcart_invoice_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me.global_ws.shopping_cart.field_set("inv_bus_name", Me.txt_address_bus_name.Text)
            Me.global_ws.shopping_cart.field_set("inv_fst_name", Me.txt_address_fst_name.Text)
            Me.global_ws.shopping_cart.field_set("inv_infix", Me.txt_address_infix.Text)
            Me.global_ws.shopping_cart.field_set("inv_lst_name", Me.txt_address_lst_name.Text)

            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", cbo_address_country.SelectedValue)
            Select Case ln_address_layout
                Case 0
                    Me.global_ws.shopping_cart.field_set("inv_address", Me.txt_address_address.Text)
                    Me.global_ws.shopping_cart.field_set("inv_street", "")
                    Me.global_ws.shopping_cart.field_set("inv_house_nr", "")
                    Me.global_ws.shopping_cart.field_set("inv_house_add", "")
                Case 1
                    Me.global_ws.shopping_cart.field_set("inv_address", "")
                    Me.global_ws.shopping_cart.field_set("inv_street", Me.txt_address_street1.Text)
                    Me.global_ws.shopping_cart.field_set("inv_house_nr", Me.txt_address_housenr1.Text)
                    Me.global_ws.shopping_cart.field_set("inv_house_add", Me.txt_address_houseadd1.Text)
                Case 2
                    Me.global_ws.shopping_cart.field_set("inv_address", "")
                    Me.global_ws.shopping_cart.field_set("inv_street", Me.txt_address_street2.Text)
                    Me.global_ws.shopping_cart.field_set("inv_house_nr", Me.txt_address_housenr2.Text)
                    Me.global_ws.shopping_cart.field_set("inv_house_add", Me.txt_address_houseadd2.Text)
                Case Else
                    Exit Select
            End Select

            Me.global_ws.shopping_cart.field_set("inv_zip_code", Me.txt_address_zip_code.Text.ToUpper())
            Me.global_ws.shopping_cart.field_set("inv_city", Me.txt_address_city.Text)
            Me.global_ws.shopping_cart.field_set("inv_cnt_code", Me.cbo_address_country.SelectedValue)
        End If

        Return ll_testing
    End Function
    Private Function save_shoppingcart_delivery_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' In het business object maakt dit vinkje de velden al leeg.
            Me.global_ws.shopping_cart.field_set("del_address_custom", Me.chk_change_delivery_address.Checked)

            Me.global_ws.shopping_cart.field_set("del_bus_name", Me.txt_address_bus_name.Text)
            Me.global_ws.shopping_cart.field_set("del_fst_name", Me.txt_address_fst_name.Text)
            Me.global_ws.shopping_cart.field_set("del_infix", Me.txt_address_infix.Text)
            Me.global_ws.shopping_cart.field_set("del_lst_name", Me.txt_address_lst_name.Text)

            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", cbo_address_country.SelectedValue)
            Select Case ln_address_layout
                Case 0
                    Me.global_ws.shopping_cart.field_set("del_address", Me.txt_address_address.Text)
                    Me.global_ws.shopping_cart.field_set("del_street", "")
                    Me.global_ws.shopping_cart.field_set("del_house_nr", "")
                    Me.global_ws.shopping_cart.field_set("del_house_add", "")
                Case 1
                    Me.global_ws.shopping_cart.field_set("del_address", "")
                    Me.global_ws.shopping_cart.field_set("del_street", Me.txt_address_street1.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_nr", Me.txt_address_housenr1.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_add", Me.txt_address_houseadd1.Text)
                Case 2
                    Me.global_ws.shopping_cart.field_set("del_address", "")
                    Me.global_ws.shopping_cart.field_set("del_street", Me.txt_address_street2.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_nr", Me.txt_address_housenr2.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_add", Me.txt_address_houseadd2.Text)
                Case Else
                    Exit Select
            End Select

            Me.global_ws.shopping_cart.field_set("del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
            Me.global_ws.shopping_cart.field_set("del_city", Me.txt_address_city.Text)
            Me.global_ws.shopping_cart.field_set("del_cnt_code", Me.cbo_address_country.SelectedValue)

            ' Zet de details van de adres velden
            Me.global_ws.shopping_cart.field_set("del_phone_nr", Me.txt_address_phonenumber.Text)
            Me.global_ws.shopping_cart.field_set("del_cell_phone_nr", Me.txt_address_cell_phonenumber.Text)
            Me.global_ws.shopping_cart.field_set("del_email_address", Me.txt_email_address.Text)
        End If

        Return ll_testing
    End Function

    Private Function save_shoppingcart_delivery_address_as_default_debtor() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", cbo_address_country.SelectedValue)

            'Updating the uws_customers_addr for the user under default debtor
            If uboCustomer.table_locate("uws_customers_addr", "cust_user_rec_id =" & Me.global_ws.user_information.user_id) Then

                uboCustomer.field_set("uws_customers_addr.del_bus_name", Me.txt_address_bus_name.Text)
                uboCustomer.field_set("uws_customers_addr.del_fst_name", Me.txt_address_fst_name.Text)
                uboCustomer.field_set("uws_customers_addr.del_infix", Me.txt_address_infix.Text)
                uboCustomer.field_set("uws_customers_addr.del_lst_name", Me.txt_address_lst_name.Text)
                uboCustomer.field_set("uws_customers_addr.del_phone_nr", Me.txt_address_phonenumber.Text)
                uboCustomer.field_set("uws_customers_addr.del_cell_phone_nr", Me.txt_address_cell_phonenumber.Text)

                Select Case ln_address_layout
                    Case 0
                        uboCustomer.field_set("uws_customers_addr.del_address", Me.txt_address_address.Text)
                        uboCustomer.field_set("uws_customers_addr.del_street", "")
                        uboCustomer.field_set("uws_customers_addr.del_house_nr", "")
                        uboCustomer.field_set("uws_customers_addr.del_house_add", "")
                    Case 1
                        uboCustomer.field_set("uws_customers_addr.del_address", "")
                        uboCustomer.field_set("uws_customers_addr.del_street", Me.txt_address_street1.Text)
                        uboCustomer.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr1.Text)
                        uboCustomer.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd1.Text)
                    Case 2
                        uboCustomer.field_set("uws_customers_addr.del_address", "")
                        uboCustomer.field_set("uws_customers_addr.del_street", Me.txt_address_street2.Text)
                        uboCustomer.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr2.Text)
                        uboCustomer.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd2.Text)
                    Case Else
                        Exit Select
                End Select

                uboCustomer.field_set("uws_customers_addr.del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
                uboCustomer.field_set("uws_customers_addr.del_city", Me.txt_address_city.Text)
                uboCustomer.field_set("uws_customers_addr.del_cnt_code", Me.cbo_address_country.SelectedValue)
            End If
            If uboCustomer.table_locate("uws_customers_users", "rec_id =" & Me.global_ws.user_information.user_id) Then
                uboCustomer.field_set("uws_customers_users.fst_name", Me.txt_address_fst_name.Text)
                uboCustomer.field_set("uws_customers_users.infix", Me.txt_address_infix.Text)
                uboCustomer.field_set("uws_customers_users.lst_name", Me.txt_address_lst_name.Text)
            End If
            ll_testing = uboCustomer.table_update(Me.global_ws.user_information.user_id)

            ' In het business object maakt dit vinkje de velden al leeg.
            Me.global_ws.shopping_cart.field_set("del_address_custom", Me.chk_change_delivery_address.Checked)

            Me.global_ws.shopping_cart.field_set("del_bus_name", Me.txt_address_bus_name.Text)
            Me.global_ws.shopping_cart.field_set("del_fst_name", Me.txt_address_fst_name.Text)
            Me.global_ws.shopping_cart.field_set("del_infix", Me.txt_address_infix.Text)
            Me.global_ws.shopping_cart.field_set("del_lst_name", Me.txt_address_lst_name.Text)

            Select Case ln_address_layout
                Case 0
                    Me.global_ws.shopping_cart.field_set("del_address", Me.txt_address_address.Text)
                    Me.global_ws.shopping_cart.field_set("del_street", "")
                    Me.global_ws.shopping_cart.field_set("del_house_nr", "")
                    Me.global_ws.shopping_cart.field_set("del_house_add", "")
                Case 1
                    Me.global_ws.shopping_cart.field_set("del_address", "")
                    Me.global_ws.shopping_cart.field_set("del_street", Me.txt_address_street1.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_nr", Me.txt_address_housenr1.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_add", Me.txt_address_houseadd1.Text)
                Case 2
                    Me.global_ws.shopping_cart.field_set("del_address", "")
                    Me.global_ws.shopping_cart.field_set("del_street", Me.txt_address_street2.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_nr", Me.txt_address_housenr2.Text)
                    Me.global_ws.shopping_cart.field_set("del_house_add", Me.txt_address_houseadd2.Text)
                Case Else
                    Exit Select
            End Select

            Me.global_ws.shopping_cart.field_set("del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
            Me.global_ws.shopping_cart.field_set("del_city", Me.txt_address_city.Text)
            Me.global_ws.shopping_cart.field_set("del_cnt_code", Me.cbo_address_country.SelectedValue)

            ' Zet de details van de adres velden
            Me.global_ws.shopping_cart.field_set("del_phone_nr", Me.txt_address_phonenumber.Text)
            Me.global_ws.shopping_cart.field_set("del_cell_phone_nr", Me.txt_address_cell_phonenumber.Text)
            Me.global_ws.shopping_cart.field_set("del_email_address", Me.txt_email_address.Text)
            Me.global_ws.shopping_cart.field_set("uws_orders.email_name", Me.txt_address_fst_name.Text.Trim() & " " & Me.txt_address_infix.Text.Trim() & " " & Me.txt_address_lst_name.Text)
        End If

        Return ll_testing
    End Function

    Private Function set_customer_invoice_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Toon de standaard gegevens voor de factuur
            Me.txt_address_bus_name.Text = Me.uboCustomer.field_get("bus_name")
            Me.txt_address_fst_name.Text = Me.uboCustomer.field_get("fst_name")
            Me.txt_address_infix.Text = Me.uboCustomer.field_get("infix")
            Me.txt_address_lst_name.Text = Me.uboCustomer.field_get("lst_name")

            ' Adres layout velden
            Me.txt_address_street1.Text = Me.uboCustomer.field_get("street")
            Me.txt_address_housenr1.Text = Me.uboCustomer.field_get("house_nr")
            Me.txt_address_houseadd1.Text = Me.uboCustomer.field_get("house_add")

            Me.txt_address_street2.Text = Me.uboCustomer.field_get("street")
            Me.txt_address_housenr2.Text = Me.uboCustomer.field_get("house_nr")
            Me.txt_address_houseadd2.Text = Me.uboCustomer.field_get("house_add")

            ' Standaard adresveld
            Me.txt_address_address.Text = Me.uboCustomer.field_get("address")
            Me.txt_address_zip_code.Text = Me.uboCustomer.field_get("zip_code").ToUpper()
            Me.txt_address_city.Text = Me.uboCustomer.field_get("city")

            Me.txt_vat_number.Text = Me.uboCustomer.field_get("vat_nr")
            Me.txt_website.Text = Me.uboCustomer.field_get("website")
            Me.txt_coc_number.Text = Me.uboCustomer.field_get("coc_nr")

            Me.txt_address_phonenumber.Text = Me.uboCustomer.field_get("phone_nr")

            ' Als het de sales module is, dan moet er een ander emailadres getoond worden.
            If Me.global_ws.user_information.user_customer_id <> Me.global_ws.user_information.user_customer_id_original Then
                Me.txt_email_address.Text = Me.uboCustomer.field_get("email_address")
            Else
                Me.txt_email_address.Text = Me.uboCustomer.field_get("uws_customers_users.email_address")
                Me.txt_login_password.Text = Me.uboCustomer.field_get("uws_customers_users.login_password")
                Me.txt_login_password_confirm.Text = Me.uboCustomer.field_get("uws_customers_users.login_password")
            End If

            ' Zet het juiste land
            Me.cbo_address_country.SelectedValue = Me.uboCustomer.field_get("cnt_code")
        End If

        Return ll_testing
    End Function
    Private Function set_customer_delivery_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me.chk_change_delivery_address.Checked = Me.uboCustomer.field_get("del_address_custom")

            Me.txt_address_bus_name.Text = Me.uboCustomer.field_get("del_bus_name")
            Me.txt_address_fst_name.Text = Me.uboCustomer.field_get("del_fst_name")
            Me.txt_address_infix.Text = Me.uboCustomer.field_get("del_infix")
            Me.txt_address_lst_name.Text = Me.uboCustomer.field_get("del_lst_name")

            ' Standaard adresveld
            Me.txt_address_address.Text = Me.uboCustomer.field_get("del_address")

            ' Adres layout velden
            Me.txt_address_street1.Text = Me.uboCustomer.field_get("del_street")
            Me.txt_address_street2.Text = Me.uboCustomer.field_get("del_street")
            Me.txt_address_housenr1.Text = Me.uboCustomer.field_get("del_house_nr")
            Me.txt_address_housenr2.Text = Me.uboCustomer.field_get("del_house_nr")
            Me.txt_address_houseadd1.Text = Me.uboCustomer.field_get("del_house_add")
            Me.txt_address_houseadd2.Text = Me.uboCustomer.field_get("del_house_add")

            Me.txt_address_zip_code.Text = Me.uboCustomer.field_get("del_zip_code").ToUpper()
            Me.txt_address_city.Text = Me.uboCustomer.field_get("del_city")
            Me.cbo_address_country.SelectedValue = Me.uboCustomer.field_get("del_cnt_code")
        End If

        Return ll_testing
    End Function
    Private Function set_shoppingcart_invoice_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Toon de standaard gegevens voor de factuur
            Me.txt_address_bus_name.Text = Me.global_ws.shopping_cart.field_get("inv_bus_name")
            Me.txt_address_fst_name.Text = Me.global_ws.shopping_cart.field_get("inv_fst_name")
            Me.txt_address_infix.Text = Me.global_ws.shopping_cart.field_get("inv_infix")
            Me.txt_address_lst_name.Text = Me.global_ws.shopping_cart.field_get("inv_lst_name")
            Me.txt_address_address.Text = Me.global_ws.shopping_cart.field_get("inv_address")

            Me.txt_address_street1.Text = Me.global_ws.shopping_cart.field_get("inv_street")
            Me.txt_address_street2.Text = Me.global_ws.shopping_cart.field_get("inv_street")
            Me.txt_address_housenr1.Text = Me.global_ws.shopping_cart.field_get("inv_house_nr")
            Me.txt_address_housenr2.Text = Me.global_ws.shopping_cart.field_get("inv_house_nr")
            Me.txt_address_houseadd1.Text = Me.global_ws.shopping_cart.field_get("inv_house_add")
            Me.txt_address_houseadd2.Text = Me.global_ws.shopping_cart.field_get("inv_house_add")


            Me.txt_address_zip_code.Text = Me.global_ws.shopping_cart.field_get("inv_zip_code").ToUpper()
            Me.txt_address_city.Text = Me.global_ws.shopping_cart.field_get("inv_city")

            ' Zet het juiste land
            Me.cbo_address_country.SelectedValue = Me.global_ws.shopping_cart.field_get("inv_cnt_code")

            Me.txt_address_cell_phonenumber.Text = Me.global_ws.shopping_cart.field_get("del_cell_phone_nr")
            Me.txt_address_phonenumber.Text = Me.global_ws.shopping_cart.field_get("del_phone_nr")
            Me.txt_email_address.Text = Me.global_ws.shopping_cart.field_get("del_email_address")
        End If

        Return ll_testing
    End Function
    Private Function set_shoppingcart_delivery_address() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me.chk_change_delivery_address.Checked = True

            If Me.global_ws.shopping_cart.field_get("del_address_custom") Then
                Me.txt_address_bus_name.Text = Me.global_ws.shopping_cart.field_get("del_bus_name")
                Me.txt_address_fst_name.Text = Me.global_ws.shopping_cart.field_get("del_fst_name")
                Me.txt_address_infix.Text = Me.global_ws.shopping_cart.field_get("del_infix")
                Me.txt_address_lst_name.Text = Me.global_ws.shopping_cart.field_get("del_lst_name")
                Me.txt_address_address.Text = Me.global_ws.shopping_cart.field_get("del_address")

                Me.txt_address_street1.Text = Me.global_ws.shopping_cart.field_get("del_street")
                Me.txt_address_street2.Text = Me.global_ws.shopping_cart.field_get("del_street")
                Me.txt_address_housenr1.Text = Me.global_ws.shopping_cart.field_get("del_house_nr")
                Me.txt_address_housenr2.Text = Me.global_ws.shopping_cart.field_get("del_house_nr")
                Me.txt_address_houseadd1.Text = Me.global_ws.shopping_cart.field_get("del_house_add")
                Me.txt_address_houseadd2.Text = Me.global_ws.shopping_cart.field_get("del_house_add")

                Me.txt_address_zip_code.Text = Me.global_ws.shopping_cart.field_get("del_zip_code").ToUpper()
                Me.txt_address_city.Text = Me.global_ws.shopping_cart.field_get("del_city")

                Try
                    Me.cbo_address_country.SelectedValue = Me.global_ws.shopping_cart.field_get("del_cnt_code")
                Catch ex As Exception

                End Try
            Else
                Me.txt_address_bus_name.Text = Me.global_ws.shopping_cart.field_get("inv_bus_name")
                Me.txt_address_fst_name.Text = Me.global_ws.shopping_cart.field_get("inv_fst_name")
                Me.txt_address_infix.Text = Me.global_ws.shopping_cart.field_get("inv_infix")
                Me.txt_address_lst_name.Text = Me.global_ws.shopping_cart.field_get("inv_lst_name")
                Me.txt_address_address.Text = Me.global_ws.shopping_cart.field_get("inv_address")

                Me.txt_address_street1.Text = Me.global_ws.shopping_cart.field_get("inv_street")
                Me.txt_address_street2.Text = Me.global_ws.shopping_cart.field_get("inv_street")
                Me.txt_address_housenr1.Text = Me.global_ws.shopping_cart.field_get("inv_house_nr")
                Me.txt_address_housenr2.Text = Me.global_ws.shopping_cart.field_get("inv_house_nr")
                Me.txt_address_houseadd1.Text = Me.global_ws.shopping_cart.field_get("inv_house_add")
                Me.txt_address_houseadd2.Text = Me.global_ws.shopping_cart.field_get("inv_house_add")

                Me.txt_address_zip_code.Text = Me.global_ws.shopping_cart.field_get("inv_zip_code").ToUpper()
                Me.txt_address_city.Text = Me.global_ws.shopping_cart.field_get("inv_city")

                Try
                    Me.cbo_address_country.SelectedValue = Me.global_ws.shopping_cart.field_get("inv_cnt_code")
                Catch ex As Exception

                End Try
            End If

            Me.txt_address_cell_phonenumber.Text = Me.global_ws.shopping_cart.field_get("del_cell_phone_nr")
            Me.txt_address_phonenumber.Text = Me.global_ws.shopping_cart.field_get("del_phone_nr")
            Me.txt_email_address.Text = Me.global_ws.shopping_cart.field_get("del_email_address")
        End If

        Return ll_testing
    End Function
#End Region

#Region " Adresboek functies"
    Private Sub fill_delivery_addresses()
        ' Haal de afleveradressen op
        Dim udt_data_table As New Utilize.Data.DataTable
        With udt_data_table
            .table_name = "uws_customers_addr"
        End With
        udt_data_table.table_init()

        udt_data_table.select_fields = "rec_id, del_bus_name + ', ' + del_address + ', ' + del_zip_code + ', ' + del_city as del_bus_name"
        udt_data_table.add_where("and cust_id = @cust_id")
        udt_data_table.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        udt_data_table.table_load()

        ' Kopieer de datatable
        Dim udt_target As System.Data.DataTable = udt_data_table.data_table

        ' Voeg een nieuwe regel toe met lege waarden aan de datatable
        Dim dr_data_row As System.Data.DataRow = udt_target.NewRow()
        dr_data_row.Item("rec_id") = ""
        dr_data_row.Item("del_bus_name") = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_addressbook_not_selected", "Niet geselecteerd", Me.global_ws.Language)

        udt_target.Rows.InsertAt(dr_data_row, 0)

        Me.cbo_select_address.DataSource = udt_target
        Me.cbo_select_address.DataValueField = "rec_id"
        Me.cbo_select_address.DataTextField = "del_bus_name"
        Me.cbo_select_address.DataBind()
    End Sub
    Public Function save_addressbook() As Boolean
        Dim ll_testing As Boolean = True

        If Me.chk_save_delivery_address.Checked Then
            Dim ubo_customer_address As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

            If ll_testing Then
                ll_testing = ubo_customer_address.data_tables("uws_customers").data_row_count = 1

                If Not ll_testing Then

                End If
            End If

            If ll_testing Then
                If String.IsNullOrEmpty(Me.cbo_select_address.SelectedValue.Trim) Then
                    ll_testing = ubo_customer_address.table_insert_row("uws_customers_addr", Me.global_ws.user_information.user_id)
                Else
                    ll_testing = ubo_customer_address.table_locate("uws_customers_addr", "rec_id = '" + Me.cbo_select_address.SelectedValue + "'")
                End If
            End If

            If ll_testing Then
                ' Sla hier de gegevens van het adresboek op
                ubo_customer_address.field_set("uws_customers_addr.del_bus_name", Me.txt_address_bus_name.Text)
                ubo_customer_address.field_set("uws_customers_addr.del_fst_name", Me.txt_address_fst_name.Text)
                ubo_customer_address.field_set("uws_customers_addr.del_infix", Me.txt_address_infix.Text)
                ubo_customer_address.field_set("uws_customers_addr.del_lst_name", Me.txt_address_lst_name.Text)

                ' Haal de gewenste adres invoer op
                Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

                Select Case ln_address_layout
                    Case 0
                        ubo_customer_address.field_set("uws_customers_addr.del_address", Me.txt_address_address.Text)
                        ubo_customer_address.field_set("uws_customers_addr.del_street", "")
                        ubo_customer_address.field_set("uws_customers_addr.del_house_nr", "")
                        ubo_customer_address.field_set("uws_customers_addr.del_house_add", "")
                    Case 1
                        ubo_customer_address.field_set("uws_customers_addr.del_address", "")
                        ubo_customer_address.field_set("uws_customers_addr.del_street", Me.txt_address_street1.Text)
                        ubo_customer_address.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr1.Text)
                        ubo_customer_address.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd1.Text)
                    Case 2
                        ubo_customer_address.field_set("uws_customers_addr.del_address", "")
                        ubo_customer_address.field_set("uws_customers_addr.del_street", Me.txt_address_street2.Text)
                        ubo_customer_address.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr2.Text)
                        ubo_customer_address.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd2.Text)
                    Case Else
                        Exit Select
                End Select

                ubo_customer_address.field_set("uws_customers_addr.del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
                ubo_customer_address.field_set("uws_customers_addr.del_city", Me.txt_address_city.Text)
                ubo_customer_address.field_set("uws_customers_addr.del_cnt_code", Me.cbo_address_country.SelectedValue)

                ll_testing = ubo_customer_address.table_update(Me.global_ws.user_information.user_id)
            End If

            If ll_testing Then
                ' Sla hier de gegevens in de winkelwagen op
                Me.global_ws.shopping_cart.field_set("del_id", ubo_customer_address.field_get("uws_customers_addr.rec_id"))
            End If

            If ll_testing Then
                Me.fill_delivery_addresses()
            End If
        End If

        Return ll_testing
    End Function

    Protected Sub cbo_select_address_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_select_address.SelectedIndexChanged
        ' Sla de wijzigingen op
        Me.global_ws.shopping_cart.field_set("del_id", Me.cbo_select_address.SelectedValue)

        ' En zorg dat de velden inggevuld worden
        Me.set_shoppingcart_delivery_address()
    End Sub
#End Region

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_address_block.css", Me)
        Me.set_javascript("~/_THMAssets/js/plugins/maskedinput.min.js", Me)

        Me.cbo_address_country.DataTextField = "cnt_desc"
        Me.cbo_address_country.DataValueField = "cnt_code"
        Me.cbo_address_country.DataSource = Me.get_countries().data_table
        Me.cbo_address_country.DataBind()

        If Not Me.IsPostBack() Then
            Me.cbo_address_country.DataTextField = "cnt_desc"
            Me.cbo_address_country.DataValueField = "cnt_code"
            Me.cbo_address_country.DataSource = Me.get_countries().data_table
            Me.cbo_address_country.DataBind()

            Me.chk_change_delivery_address.Text = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_delivery_address_different", "Ik wil mijn bestelling op het onderstaande adres laten bezorgen.", Me.global_ws.Language)
            Me.chk_create_account.Text = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_create_account", "Maak een account aan voor mijn volgende bezoek.", Me.global_ws.Language)
        End If

        If Not _sales_module Then
            Me.set_address()
            Me.fill_delivery_addresses()
        End If

        ' Haal de manier van omgaan met bedrijfsnaam op
        Me._register_type = Me.global_ws.get_webshop_setting("register_type")
        Me._show_vat_number = Me.global_ws.get_webshop_setting("show_vat_nr")
        Me._change_company_info = Me.global_ws.get_webshop_setting("change_company_info")
        Me._show_coc_number = Me.global_ws.get_webshop_setting("show_coc_nr")
        Me._show_website = Me.global_ws.get_webshop_setting("show_website")


        Select Case _register_type
            Case 2
                ' Bedrijfsnaam beschikbaar maar niet verplicht
                Me.ph_bus_name.Visible = True
                Me.ph_bus_name_mandatory.Visible = False
            Case 3
                ' Bedrijfsnaam verbergen
                Me.ph_bus_name.Visible = False
                Me.ph_bus_name_mandatory.Visible = False
            Case Else
                ' Bedrijfsnaam verplicht
                Me.ph_bus_name.Visible = True
                Me.ph_bus_name_mandatory.Visible = True
        End Select

        ' Haal de manier van omgaan met het telefoonnummer op
        Me._register_phone = Me.global_ws.get_webshop_setting("register_phone")

        Me.ph_phone_number.Visible = False
        Me.ph_phonenumber_mandatory.Visible = False

        ' Als het om het factuuradres of winkelwagen adres gaat
        If Me.address_type = addresstype.customer_invoice_address Then
            Select Case _register_phone
                Case 2
                    Me.ph_phone_number.Visible = True
                Case 3
                    Me.ph_phone_number.Visible = False
                Case Else
                    Me.ph_phone_number.Visible = True
            End Select
        End If

        If Me.address_type = addresstype.customer_invoice_address Then
            Me.ph_email_address.Visible = True
        End If

        ' Het wijzigen van het afleveradres mag alleen zichtbaar zijn bij een afleveradres blok
        Me.chk_change_delivery_address.Visible = Me.address_type = addresstype.customer_delivery_address

        Me.ph_user_information.Visible = Me._use_login
        Me.ph_login_code.Visible = False

        Me.autocomplete_address_via_google_map()
    End Sub

    Private Sub autocomplete_address_via_google_map()
        ' Get address autocomplete setting
        Dim ll_autocomplete As Boolean = Me.global_ws.get_webshop_setting("autocomplete")

        If ll_autocomplete Then
            Dim lc_default_country As String = Me.global_ws.get_webshop_setting("default_country")
            If String.IsNullOrEmpty(lc_default_country) Then
                lc_default_country = "nl"
            End If
            Me.set_javascript("~/webshop/paymentprocess/blocks/uc_address_block.js", Me)
            If Me.Visible Then
                ScriptManager.RegisterStartupScript(Page, Me.GetType(), "google_search", "googleSearch('" + lc_default_country + "');", True)
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Wanneer het om een afleveradres gaat, dan moeten de adresvelden alleen beschikbaar zijn wanneer de checkbox is aangevinkt.
        Select Case _address_type
            Case addresstype.customer_delivery_address
                Me.ph_address_information.Visible = Me.chk_change_delivery_address.Checked
                Me.ph_vat_number.Visible = False
                Me.ph_coc_number.Visible = False
                Me.ph_website.Visible = False
            Case addresstype.shoppingcart_delivery_address

                Me.ph_address_information.Visible = Me.chk_change_delivery_address.Checked

                If Not DefaultDebtorHelper.user_has_default_debtor() Then
                    Me.ph_address_book_select.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book")
                    Me.ph_address_book_save.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book")
                Else
                    Me.ph_address_book_select.Visible = False
                    Me.ph_address_book_save.Visible = False
                End If

                Me.cbo_select_address.SelectedValue = Me.global_ws.shopping_cart.field_get("del_id")
                Me.ph_vat_number.Visible = False
                Me.ph_coc_number.Visible = False
                Me.ph_website.Visible = False

                'The code in the if below handles the salesorder delivery address in the case of EXACT online. Do not change, without consulting code of BOC and fellow developers!
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_eol") Then
                    If Me.cbo_select_address.SelectedIndex = 0 Then
                        Me.chk_save_delivery_address.Checked = True
                        Me.enabled = True
                    Else
                        Me.chk_save_delivery_address.Checked = False
                        Me.enabled = False
                    End If
                    Me.chk_save_delivery_address.Visible = False
                End If

            Case addresstype.customer_invoice_address
                Me.ph_vat_number.Visible = Me._show_vat_number
                Me.ph_coc_number.Visible = Me._show_coc_number
                Me.ph_website.Visible = Me._show_website
            Case addresstype.shoppingcart_invoice_address
                Exit Select
            Case Else
                Exit Select
        End Select

        Me.chk_register_newsletter.Text = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_register_newsletter", "Ik wil mij aanmelden voor de nieuwsbrief.", Me.global_ws.Language)

        If String.IsNullOrEmpty(Me.cbo_select_address.SelectedValue.Trim) Then
            ' Als er geen afleveradres is geselecteerd, dan moet de tekst afleveradres opslaan getoond worden.
            Me.chk_save_delivery_address.Text = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_save_new_delivery_addres", "Ik wil het nieuwe afleveradres bewaren.", Me.global_ws.Language)
        Else
            ' Als er wel een afleveradres is geselecteerd, dan moet de tekst afleveradres opslaan getoond worden.
            Me.chk_save_delivery_address.Text = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_save_existing_delivery_addres", "Ik de wijzigingen in het afleveradres bewaren.", Me.global_ws.Language)
        End If

        ' In de webshop instellingen is instelbaar of het optioneel is dat een klant een account aanmaakt
        If Me.global_ws.get_webshop_setting("create_acct_optional") = 1 Then
            Me.ph_create_account.Visible = True
            Me.ph_create_account_details.Visible = Me.chk_create_account.Checked
        End If

        ' Indien de webshop instellingen nog optioneel nog verplicht zijn, wordt er automatisch een wachtwoord gemaakt en gemaild
        If Me.global_ws.get_webshop_setting("create_acct_optional") = 2 Then
            Me.ph_create_account_details.Visible = False
        End If

        ' Hier bepalen we of de checkbox voor het aanmelden van de nieuwsbrief wel of niet getoond moet worden.
        Me.chk_register_newsletter.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_cm_mailings")

        If Me.chk_register_newsletter.Visible Then
            Me.chk_register_newsletter.Visible = Me.address_type = addresstype.customer_invoice_address
        End If

        ' Zet de vertalingen goed
        Me.label_address_street1.Text = Me.label_address_street.Text
        Me.label_address_housenr1.Text = Me.label_address_housenr.Text
        Me.label_address_housenr_add1.Text = Me.label_address_housenr_add.Text

        ' Toon op basis van de iso landcode de gewenste adresinvoer.
        Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

        Me.ph_address_0.Visible = False
        Me.ph_address_1.Visible = False
        Me.ph_address_2.Visible = False

        Select Case ln_address_layout
            Case 0
                Me.ph_address_0.Visible = True
            Case 1
                Me.ph_address_1.Visible = True
            Case 2
                Me.ph_address_2.Visible = True
            Case Else
                Exit Select
        End Select

        Me.set_mandatory_signs()

        ' Afleveradres detail velden
        If Me.address_type = addresstype.shoppingcart_delivery_address Then
            Me.ph_email_address.Visible = False

            If Me.global_ws.get_webshop_setting("del_addr_details") Then
                Me.ph_cell_phone_number.Visible = True
                Me.ph_phone_number.Visible = True
                Me.ph_email_address.Visible = True
            End If
        End If

        Me.ph_uppercase.Visible = global_ws.get_webshop_setting("passw_upper_case")
        Me.ph_lowercase.Visible = global_ws.get_webshop_setting("passw_lower_case")
        Me.ph_digit.Visible = global_ws.get_webshop_setting("passw_numeric")
        Me.ph_special_chars.Visible = global_ws.get_webshop_setting("passw_special_char")

        Dim mask As String = Utilize.Data.DataProcedures.GetValue("UWS_COUNTRIES", "POSTCODE_MASK", Me.cbo_address_country.SelectedValue)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Set_Mask", "$(document).ready(function (){ $('.input.value.zip_code.form-control').mask('" + mask + "');});", True)

        Me.autocomplete_address_via_google_map()

        If DefaultDebtorHelper.user_has_default_debtor() Then
            Me.txt_email_address.Enabled = False
        End If
    End Sub

    Protected Sub chk_change_delivery_address_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_change_delivery_address.CheckedChanged
        Me.txt_address_bus_name.Text = ""
        Me.txt_address_fst_name.Text = ""
        Me.txt_address_infix.Text = ""
        Me.txt_address_lst_name.Text = ""
        Me.txt_address_address.Text = ""
        Me.txt_address_zip_code.Text = ""
        Me.txt_address_city.Text = ""
        Me.cbo_address_country.SelectedValue = ""

        If Not Me.chk_change_delivery_address.Checked Then
            Me.global_ws.shopping_cart.field_set("del_id", "")
            Me.message_error.Visible = Me.chk_change_delivery_address.Checked
        End If
    End Sub
    Protected Sub chk_create_account_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_create_account.CheckedChanged
        Me.message_error.Visible = False
    End Sub

    Private Sub uc_address_block_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me._create_account = chk_create_account.Checked
    End Sub
End Class
