﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_shopping_cart_totals.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_shopping_cart_totals" %>
<div class="uc_shopping_cart_totals col-md-6 pull-right">
    <div class="row">
        <utilize:placeholder runat="server" ID="ph_amounts" Visible="false">
            <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
                <ul class="list-inline total-result">
                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_subtotal_excl_vat"></utilize:literal></span>
                        </div>    
                    </li>

                    <utilize:placeholder runat='server' ID="ph_order_cost_excl_vat">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span><utilize:literal runat="server" ID="lt_order_costs_excl_vat"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>

                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_shipping_costs_excl_vat"></utilize:literal></span>
                        </div>    
                    </li>

                    <utilize:placeholder runat='server' ID="ph_pay_disc_amt_excl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_pay_disc_amt_excl_vat" Text="Betalingskorting"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_pay_disc_amt_excl_vat"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>

                    <utilize:placeholder runat="server" ID="ph_coupon_excl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_coupon_amt_excl_vat" Text="Coupon kortingen"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_coupon_amt_excl_vat"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>

                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_vat_total"></utilize:literal></span>
                        </div>    
                    </li>

                    <li class="divider"></li>

                    <li class="total-price">
                        <h4><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_total_incl_vat"></utilize:literal></span>
                        </div>
                    </li>
                </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_prices_vat_incl">
                <ul class="list-inline total-result">
                    <li><h4><utilize:translateliteral runat="server" ID="totals_subtotal_incl_vat" Text="Subtotaal incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_subtotal_incl_vat"></utilize:literal></span>
                        </div>
                    </li>
                    
                    <utilize:placeholder runat='server' ID="ph_order_cost_incl_vat">
                        <li><h4><utilize:translateliteral runat="server" ID="totals_order_costs_incl_vat" Text="Orderkosten incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_costs_incl_vat"></utilize:literal></span>
                        </div>
                        </li>
                    </utilize:placeholder>

                    <li><h4><utilize:translateliteral runat="server" ID="totals_shipping_costs_incl_vat" Text="Verzendkosten incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_shipping_costs_incl_vat"></utilize:literal></span>
                        </div>
                    </li>

                    <utilize:placeholder runat='server' ID="ph_pay_disc_amt_incl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_pay_disc_amt_incl_vat" Text="Betalingskorting"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_pay_disc_amt_incl_vat"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>

                    <utilize:placeholder runat="server" ID="ph_coupon_incl">
                        <li><h4><utilize:translateliteral runat="server" ID="totals_coupon_amt_incl_vat" Text="Coupon kortingen"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_coupon_amt_incl_vat"></utilize:literal></span>
                            </div>
                        </li>
                    </utilize:placeholder>

                    <li><h4><utilize:translateliteral runat="server" ID="totals_total_incl_vat1" Text="Totaal incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_total_incl_vat1"></utilize:literal></span>
                        </div>
                    </li>
                </ul>
            </utilize:placeholder>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_credits" Visible="false">
            <ul class="list-inline total-result credit-overview">
                <li>
                    <h4><utilize:translateliteral runat="server" ID="total_credits_order" Text="Huidige bestelling"></utilize:translateliteral></h4>
                    <div class="total-result-in">
                        <span><utilize:literal runat="server" ID="lt_credits_order"></utilize:literal></span>
                    </div>
                </li>

                <li>
                    <h4><utilize:translateliteral runat="server" ID="total_credits_available" Text="Vorig aantal credits"></utilize:translateliteral></h4>
                    <div class="total-result-in">
                        <span><utilize:literal runat="server" ID="lt_credits_available"></utilize:literal></span>
                    </div>
                </li>

                <li>
                    <h4><utilize:translateliteral runat="server" ID="total_credits_left" Text="Nieuw aantal credits"></utilize:translateliteral></h4>
                    <div class="total-result-in">
                        <span><utilize:literal runat="server" ID="lt_credits_left"></utilize:literal></span>
                    </div>
                </li>
            </ul>
        </utilize:placeholder>
    </div>
</div>