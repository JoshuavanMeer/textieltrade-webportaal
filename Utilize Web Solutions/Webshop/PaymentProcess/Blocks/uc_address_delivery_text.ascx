﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_address_delivery_text.ascx.vb" Inherits="uc_address_delivery" %>
<div class="uc_address_delivery">
    <h2 class="title-type"><utilize:translatetitle runat="server" ID="title_delivery_address" Text="Afleveradres"/></h2>
    <div class="billing-info-inputs checkbox-list">
        <utilize:literal runat='server' ID="lt_delivery_address"></utilize:literal>
    </div>
</div>