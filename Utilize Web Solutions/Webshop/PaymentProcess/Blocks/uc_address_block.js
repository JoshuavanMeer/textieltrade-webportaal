﻿
function googleSearch(code) {
    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name', /* straat */
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    var lc_country_code = code;
    var cbo_country;
    
    // find selected country code
    if ($('*[data-google-api="country"]') != null) {
        if ($('*[data-google-api="country"]')[0].value != "") {
            lc_country_code = $('*[data-google-api="country"]')[0].value;
        }
        cbo_country = "#" + $('*[data-google-api="country"]')[0].id;
    }
     
    // find controls 
    var search = $('*[data-google-api="search"]')[0];
    var city = $('*[data-google-api="city"]')[0];
    var postalcode = $('*[data-google-api="postalcode"]')[0];
    var address = $('*[data-google-api="address"]')[0];
    var street = $('*[data-google-api="street"]')[0];
    var street_number = $('*[data-google-api="street_number"]')[0];
    var phone_number = $('*[data-google-api="phonenumber"]')[0];    

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    if (search != null) {
        autocomplete = new google.maps.places.Autocomplete(search, { types: ['establishment'] });
        autocomplete.setComponentRestrictions({ 'country': lc_country_code });
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            search.value = search.value.split(',')[0];
            fillInAddress();
        });
    }

    function clearAddress() {   
      
        if (search !=null) {
         search.value = '';
        }
        if (city !=null) {
            city.value = '';
        }
        if (postalcode !=null) {
           postalcode.value = '';
        }
        if (address != null) {
            address.value = '';
        }
        if (street != null) {
            street.value = '';
        }
        if (street_number != null) {
            street_number.value = '';
        }
    }

    // [START region_fillform]
    function fillInAddress() {
        var house_number = "";     

        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
       
        // fill telefoon number
        if (phone_number !=null && place.formatted_phone_number !=null) {
            phone_number.value= place.formatted_phone_number
        }         

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {            
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                switch (addressType) {
                    case 'locality':
                        city.value = val;
                        break;                       
                    case 'route':
                        if (address != null) {
                            address.value = val + " " + house_number;                         
                        }
                        if (street != null) {
                            street.value = val;
                        }
                        break;
                    case 'postal_code':
                        postalcode.value = val;
                        break;
                    case 'street_number':
                        if (street_number != null) {
                            street_number.value = val;
                        }
                        else
                        {
                            house_number = val;
                        }
                        break;
                }
            }
        }
    }

    $(cbo_country).change(function (e) {
        e.preventDefault();
        clearAddress();
    });
}
