﻿
Partial Class Webshop_PaymentProcess_request_password
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' In de webshop instellingen kun je de pagina template voor het betalingsproces vastleggen.
        If Me.global_ws.get_webshop_setting("webshop_login") = True Then
            Me.cms_template_code = Me.global_ws.get_webshop_setting("login_template")
        Else
            Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

            If Session("log_in_app") Then
                Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
                lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
            End If

            If lc_template_code.Trim() = "" Then
                lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
            End If

            Me.cms_template_code = lc_template_code
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
        Dim uc_order_confirmation As Webshop.ws_user_control = LoadUserControl("PageBlocks/uc_request_password.ascx")
        ph_zone.Controls.Add(uc_order_confirmation)
    End Sub
End Class
