﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_identification.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_identification" %>

<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx" tagname="uc_address_block" tagprefix="utilize" %>
<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_identification_login.ascx" tagname="uc_identification_login" tagprefix="utilize" %>

<div class="uc_identification">
    <div class="shopping-cart">
        <div class="wizard margin-bottom-20">
            <div class="steps clearfix">
	    <ul role="tablist">
                <div class="row">
                    <div class="col-md-4">
                        <li role="tab" class="first done" aria-disabled="false" aria-selected="true">
                            <utilize:hyperlink runat="server" id="hl_first_step" aria-controls="steps-uid-0-p-0"><span class="current-info audible">current step: </span><span class="number">1.</span>             
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_shopping_cart_steps" Text="Winkelwagen"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_review_and_edit" Text="Controleer jouw winkelwagen"></utilize:translatetext>
                                    <i class="rounded-x fa fa-check"></i>
                                </div>    
                            </utilize:hyperlink>
                        </li>
                    </div>
                    <div class="col-md-4">
                        <li role="tab" class="current" aria-disabled="true">
                            <a id="steps-uid-0-t-1" href="#" aria-controls="steps-uid-0-p-1"><span class="number">2.</span> 
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_billing_info" Text="Betalingsinformatie"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_billing_info" Text="Adres gegevens"></utilize:translatetext>
                                    <i class="rounded-x fa fa-home"></i>
                                </div>    
                            </a>
                        </li>
                    </div>
                    <div class="col-md-4">
                        <li role="tab" class="disabled last" aria-disabled="true">
                            <a id="steps-uid-0-t-2" href="#" aria-controls="steps-uid-0-p-2"><span class="number">3.</span> 
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_payment" Text="Betalingsmethode"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_payment" Text="Kies een betalingsmethode"></utilize:translatetext>
                                    <i class="rounded-x fa fa-credit-card"></i>
                                </div>    
                            </a>
                        </li>
                    </div>
                </div>
            </ul>
	    </div>
        </div>
    </div>

    <utilize:placeholder runat='server' ID="ph_login" Visible="true">
        <utilize:uc_identification_login ID="uc_identification_login1" runat="server"/>
        <hr>
    </utilize:placeholder>

    <utilize:placeholder runat='server' ID="ph_registration" Visible="true">

        <div class="block">
            <h3><utilize:translatetitle runat="server" ID="title_new_customer" Text="Bent u een nieuwe klant?"></utilize:translatetitle></h3>
            <utilize:translatetext runat="server" ID="text_new_customer" Text="Wij willen u graag van harte welkom heten als nieuwe klant. Vul hieronder uw gegevens in om een account bij ons aan te maken."></utilize:translatetext>
        </div>
       <utilize:uc_address_block ID="uc_address_block1" runat="server" address_type="customer_invoice_address" use_login="true" />
         <utilize:placeholder runat='server' ID="ph_change_del_address" Visible="true">
        <div class="block">
            <h3><utilize:translatetitle runat="server" ID="title_different_delivery_address" Text="Wilt u uw bestelling standaard op een ander adres af laten leveren?"></utilize:translatetitle></h3>
            <utilize:translatetext runat="server" ID="text_different_delivery_address" Text="Wanneer u uw bestelling op een ander adres wilt af laten leveren dan de factuur, dan kunt u dat hieronder aangeven."></utilize:translatetext>
        </div>
            
        <utilize:uc_address_block ID="uc_address_block2" runat="server" address_type="customer_delivery_address" />
              </utilize:placeholder>
    </utilize:placeholder>

    <utilize:placeholder runat='server' ID="ph_buttons" Visible="true">
    <div class="col-md-8 mb-margin-bottom-30">
        <div class="block col-md-12">
            <asp:PlaceHolder runat="server" ID="ph_recaptcha">
                <div id="div_recaptcha" class="g-recaptcha" data-sitekey="<%=reCAPTCHAApiKey%>" data-theme="<%=reCAPTCHATheme%>" data-size="<%=reCAPTCHASize%>"></div>
            </asp:PlaceHolder>
            <div class="alert alert-danger" runat="server" id="div_recaptcha_alert" visible="false">
                <utilize:literal runat="server" ID="lt_recaptcha_error"></utilize:literal>
            </div>
            <utilize:alert runat="server" ID="alert_problem_register_user" AlertType="danger" Visible="false"></utilize:alert>
            <div class="buttons">
                <utilize:translatebutton runat="server" ID="button_previous_step" Text="Vorige stap" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg" />&nbsp;
                <utilize:translatebutton runat="server" ID="button_register" Text="Registreren" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg" />
            </div>
        </div>
    </div>
    </utilize:placeholder>
</div>