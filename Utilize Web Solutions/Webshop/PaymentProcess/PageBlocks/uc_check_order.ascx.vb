﻿
Partial Class Webshop_CheckOut_Blocks_uc_check_order
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

#Region "Page subroutines"
    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_check_order.css", Me)
        Me.set_style_sheet("~/_THMAssets/plugins/jquery-steps/css/custom-jquery.steps.css", Me)

        Utilize.Data.API.Webshop.DefaultDebtorHelper.load_address_to_shoppingcart()

        Me.uc_address_block1.set_address()
        Me.uc_address_block2.set_address()

        Me.uc_order_description1.customer_reference = Me.global_ws.shopping_cart.field_get("order_description")
        Me.uc_order_description1.customer_comments = Me.global_ws.shopping_cart.field_get("order_comments")

        Me.hl_first_step.NavigateUrl = Me.ResolveCustomUrl("~/") + "Webshop/PaymentProcess/shopping_cart.aspx"
        Me.hl_second_step.NavigateUrl = Me.ResolveCustomUrl("~/") + "Webshop/PaymentProcess/identification.aspx"

        ' Maak de order description zichtbaar als een van de opties aan staat
        Me.ph_order_description.Visible = Me.global_ws.get_webshop_setting("show_order_desc") Or Me.global_ws.get_webshop_setting("show_order_comm")

        'hide invoice address for customers_users under default debtor
        If Not Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Me.button_change_invoice_address.Visible = Me.global_ws.get_webshop_setting("change_inv_address")
        Else
            Me.button_change_invoice_address.Visible = False
            Me.ph_invoice_address_text.Visible = False
        End If

        ' Zet de instellingen voor de checkbox voor het versturen van de extra email:
        Me.txt_email_address.Text = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.email_address")

        ' De placeholder is alleen zichtbaar wanneer de gebruiker namens een klant is ingelogd
        Me.ph_send_extra_confirmation.Visible = Me.global_ws.user_information.user_customer_id <> Me.global_ws.user_information.user_customer_id_original

        If Not Me.Page.IsPostBack() Then
            Me.txt_customer_salutation.Text = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("email_name")
        End If

        'haal de setting op van de uws_pay
        Dim qsc_pay_provider As New Utilize.Data.QuerySelectClass
        With qsc_pay_provider
            .select_fields = "pay_provider"
            .select_from = "uws_pay"
        End With
        Dim dt_results = qsc_pay_provider.execute_query()

        Dim uc_payment As Webshop.ws_user_control = LoadUserControl("~/webshop/paymentprocess/services/uc_payment.ascx")
        ph_payment_services.Controls.Add(uc_payment)
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.global_ws.shopping_cart.field_set("order_description", Me.uc_order_description1.customer_reference)
        Me.global_ws.shopping_cart.field_set("order_comments", Me.uc_order_description1.customer_comments)
        Me.global_ws.shopping_cart.field_set("email_name", Me.txt_customer_salutation.Text)

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sales_module") = True Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True Then
            If Me.chk_send_email_to_customer.Checked Then
                Me.global_ws.shopping_cart.field_set("email_address_extra", Me.txt_email_address.Text)
            Else
                Me.global_ws.shopping_cart.field_set("email_address_extra", "")
            End If
        End If
    End Sub

    Protected Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.global_ws.shopping_cart.product_count > 0 Then
            Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
        End If

        If Me.global_ws.get_webshop_setting("stock_limit") = True Then
            ' When the stock limit option in the webshop settings is set, we check if all the orderlines in the shoppingcart can be delivered
            Me.check_stock_availability()
        End If
    End Sub

    ''' <summary>
    ''' This sub checks the stock in the shoppingcart and shows a stock limit message with the option to delete the order lines
    ''' </summary>
    Private Sub check_stock_availability()
        Me.pnl_stock_message.Visible = False

        Me.ph_delivery_methods.Visible = True
        Me.ph_payment_services.Visible = True

        Dim dtProductsOutOfStock As System.Data.DataTable = Me.global_ws.shopping_cart.products_out_of_stock

        If dtProductsOutOfStock.Rows.Count > 0 Then
            Me.ph_delivery_methods.Visible = False
            Me.ph_payment_services.Visible = False

            Me.pnl_stock_message.Visible = True

            Dim lcStockMessage As String = ""

            For Each drProduct As System.Data.DataRow In dtProductsOutOfStock.Rows
                lcStockMessage += drProduct.Item("prod_code") + " - " + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", drProduct.Item("prod_code") + Me.global_ws.Language, "prod_code + lng_code") + "<br>"
            Next

            lcStockMessage = global_trans.translate_text("text_shopping_cart_out_of_stock", "De bestelde aantallen van een of meer regels in uw winkelwagen zijn hoger dan dat bij ons op voorraad is.<br />[1]<br />U kunt zelf de regels aanpassen/verwijderen, kies rechts voor verwijderen om deze producten uit de winkelwagen te verwijderen", Me.global_ws.Language).Replace("[1]", lcStockMessage) + "<br>"

            Me.lt_stock_message.Text = lcStockMessage

            Me.button_delete_stock_products.OnClientClick = "return confirm('" + global_trans.translate_message("message_delete_stock_products", "Weet u zeker dat u de producten uit de winkelwagen wilt verwijderen?", Me.global_ws.Language) + "')"
        End If
    End Sub

    Private Sub button_delete_stock_products_Click(sender As Object, e As EventArgs) Handles button_delete_stock_products.Click
        ' Get de products which are out of stock
        Dim dtProductsOutOfStock As System.Data.DataTable = Me.global_ws.shopping_cart.products_out_of_stock

        ' Walk through the products which are out of stock
        For Each drProduct As System.Data.DataRow In dtProductsOutOfStock.Rows
            ' Select the orderlines which are out of stock
            Dim dtOrderLines As System.Data.DataTable = Me.global_ws.shopping_cart.product_table.ToTable()

            For Each drOrderLine As System.Data.DataRow In dtOrderLines.Select("prod_code = '" + drProduct.Item("prod_code") + "'")
                ' Delete each orderline which is out of stock
                Me.global_ws.shopping_cart.product_delete_by_id(drOrderLine.Item("rec_id"))
            Next
        Next
    End Sub

#End Region

#Region "Buttons"
    Protected Sub button_previous_step_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_previous_step.Click
        Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
    End Sub

    Protected Sub button_change_delivery_address_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_change_delivery_address.Click
        Me.pnl_overlay_delivery.Visible = True
    End Sub
    Protected Sub button_change_invoice_address_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_change_invoice_address.Click
        Me.pnl_overlay_invoice.Visible = True
    End Sub

    Protected Sub button_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_cancel.Click
        Me.pnl_overlay_invoice.Visible = False
    End Sub
    Protected Sub button_cancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_cancel1.Click
        Me.pnl_overlay_delivery.Visible = False
    End Sub

    Protected Sub button_change_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_change.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Me.uc_address_block1.check_address()
        End If

        If ll_resume Then
            ll_resume = Me.uc_address_block1.save_address()
        End If

        If ll_resume Then
            Me.pnl_overlay_invoice.Visible = False
        End If
    End Sub
    Protected Sub button_change2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_change2.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Me.uc_address_block2.check_address()
        End If

        If ll_resume Then
            ll_resume = Me.uc_address_block2.save_address()
        End If

        If ll_resume Then
            ll_resume = Me.uc_address_block2.save_addressbook()
        End If

        If ll_resume Then
            Me.pnl_overlay_delivery.Visible = False
        End If
    End Sub
#End Region

End Class
