﻿Imports Utilize.Data.API.Webshop

Partial Class Webshop_CheckOut_Blocks_uc_order_confirmation
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _order_number As String = ""
    Private Const timeoutSeconds As Integer = 5

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_order_confirmation.css", Me)
        'Getting order id
        If Request.QueryString("orderid") IsNot Nothing Then
            Me._order_number = Me.get_page_parameter("orderid")
        ElseIf Request.QueryString("ec") IsNot Nothing Then
            Me._order_number = Me.get_page_parameter("ec")
        End If

        Dim lc_status_code As String = Me.get_page_parameter("status").ToUpper()
        Dim ll_sync_order As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("sync_orders")

        If ll_sync_order Then
            Dim ll_status_confirmed = False
            For i As Integer = 0 To timeoutSeconds
                Dim ln_status_code As IntermediateEnums.SalesOrderStatus = Utilize.Data.DataProcedures.GetValue("uws_so_orders", "intermediate_status", _order_number, "src_id")

                If ln_status_code = IntermediateEnums.SalesOrderStatus.CriticalFailure Then
                    lc_status_code = "INTERMEDIATE_FAILURE"
                    ll_status_confirmed = True
                    Exit For
                End If

                If ln_status_code = IntermediateEnums.SalesOrderStatus.Succes Then
                    ll_status_confirmed = True
                    Exit For
                End If

                Threading.Thread.Sleep(1000)
            Next

            If Not ll_status_confirmed Then
                lc_status_code = "INTERMEDIATE_FAILURE"
            End If
        End If

        Select Case lc_status_code
            ' Onderstaand de stadia van ogone
            ' Success wordt ook gebruikt voor op rekening kopen
            Case "SUCCESS"
                Me.global_ws.shopping_cart.create_shopping_cart()

                Dim on_demand_sync As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync")

                Me.ph_order_placed.Visible = True
                Me.button_go_to_order.Visible = Not Me._order_number.Trim() = "" And Not on_demand_sync
            Case "PENDING"
                Me.global_ws.shopping_cart.create_shopping_cart()

                Me.ph_order_pending.Visible = True
                Me.button_go_to_order.Visible = False
            Case "FAILURE"
                Me.ph_order_refused.Visible = True
                Me.button_go_to_order.Visible = False
                ' Onderstaand de stadia van Sisow
            Case "EXPIRED"
                Me.ph_order_expired.Visible = True
                Me.button_go_to_order.Visible = False
            Case "CANCELLED"
                Me.ph_order_canceled.Visible = True
                Me.button_go_to_order.Visible = False
                ' Below is the failure status for the intermediate service
            Case "INTERMEDIATE_FAILURE"
                Me.ph_intermediate_failure.Visible = True
                Me.lt_ord_nr.Text = Utilize.Data.DataProcedures.GetValue("uws_so_orders", "ord_nr", _order_number, "src_id")
            Case Else
                Me.global_ws.shopping_cart.create_shopping_cart()

                Me.ph_order_placed.Visible = True
        End Select
    End Sub

    Protected Sub button_continue_shopping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_continue_shopping.Click
        If Not Session("ProductPage") Is Nothing Then
            Response.Redirect(Session("ProductPage"))
        Else
            Response.Redirect("~/home.aspx")
        End If
    End Sub

    Protected Sub button_go_to_order_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_go_to_order.Click
        Dim lc_order_number As String = Utilize.Data.DataProcedures.GetValue("uws_so_orders", "ord_nr", _order_number, "src_id")
        Response.Redirect("~/" + Me.global_ws.Language + "/account/orders.aspx?OrderId=" + lc_order_number, True)
    End Sub
End Class
