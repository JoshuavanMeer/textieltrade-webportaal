﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Json
Imports Utilize.Data.API.Webshop

' Object for mapping Google's json validation response
<DataContract()>
Public Class ReCaptchaResponseRegister
    <DataMember(Name:="success")>
    Public Success As Boolean
    <DataMember(Name:="error_codes")>
    Public ErrorCodes As List(Of String)
End Class

Partial Class uc_register
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    ' UWS-387: Registration Google reCAPTCHA
    Public ReadOnly Property reCAPTCHAApiKey As String
        Get
            Return System.Web.HttpContext.Current.Session("RecaptchaApiKey")
        End Get
    End Property

    Private ReadOnly Property reCAPTCHAApiSecret As String
        Get
            Return System.Web.HttpContext.Current.Session("RecaptchaApiSecret")
        End Get
    End Property

    Public reCAPTCHATheme As String = "light" ' options: light/dark
    Public reCAPTCHASize As String = "normal" ' options: normal/compact
    ' END UWS-387

#Region "Page subroutines"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            Me.set_style_sheet("uc_register.css", Me)

            ll_testing = Not Me.global_ws.user_information.user_logged_on

            If ll_testing Then
                ll_testing = Not (Me.global_ws.get_webshop_setting("register_enabled") = 0)
            End If

            If Not ll_testing Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' UWS-387: Registration Google reCAPTCHA
        If ll_testing Then
            Dim ll_recaptcha_available As Boolean = Not String.IsNullOrEmpty(System.Web.HttpContext.Current.Session("RecaptchaApi"))

            Me.div_recaptcha_alert.Visible = False
            Me.ph_recaptcha.Visible = ll_recaptcha_available

            If ll_recaptcha_available Then
                Me.set_javascript("uc_register.js", Me)
            End If
        End If
        ' END UWS-387
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If DefaultDebtorHelper.check_module() Then
            If Not uc_address_block1.create_account Then
                Me.ph_change_del_address.Visible = False
            Else
                Me.ph_change_del_address.Visible = True
            End If

        End If
    End Sub
#End Region
#Region "Buttons"
    Protected Sub button_register_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_register.Click
        Dim ll_testing As Boolean = True

        Me.alert_problem_register_user.Visible = False

        ' UWS-387: Registration Google reCAPTCHA
        Dim ll_recaptcha_verified As Boolean = False
        Dim ll_recaptcha_available As Boolean = Not String.IsNullOrEmpty(System.Web.HttpContext.Current.Session("RecaptchaApi"))
        Me.ph_recaptcha.Visible = ll_recaptcha_available
        Me.div_recaptcha_alert.Visible = False

        If ll_testing And ll_recaptcha_available Then
            ' (re)Initialize the captcha on target panel
            ll_recaptcha_verified = VerifyReCaptcha()

            If Not ll_recaptcha_verified Then
                reCaptchaScript("div_recaptcha")
            End If
        End If
        'END UWS-387

        If ll_testing Then
            ' Controleer de adressen
            If Not DefaultDebtorHelper.check_module() Then
                ll_testing = Me.uc_address_block1.check_address() And Me.uc_address_block2.check_address()
            Else
                ll_testing = Me.uc_address_block1.check_address()
            End If
        End If

        ' UWS-387: Registration Google reCAPTCHA
        If ll_testing And ll_recaptcha_available Then
            ll_testing = ll_recaptcha_verified

            If Not ll_testing Then
                Me.lt_recaptcha_error.Text = global_trans.translate_label("message_recaptcha_invalid", "Zet een vinkje bij &quot;Ik ben geen robot&quot; in de reCAPTCHA", Me.global_cms.Language)
                Me.div_recaptcha_alert.Visible = True
            End If
        End If
        'END UWS-387

        If ll_testing Then
            ' Sla de adressen op in het business object
            If Not DefaultDebtorHelper.check_module() Then
                ll_testing = Me.uc_address_block1.save_address() And Me.uc_address_block2.save_address()
            Else
                Me.uc_address_block1.save_address()
            End If

            If Not ll_testing Then
                Me.alert_problem_register_user.Text = global_trans.translate_message("message_incomplete_register_address", "Uw adresgegevens zijn incompleet.", Me.global_ws.Language)
                Me.alert_problem_register_user.Visible = True
            End If
        End If


        If ll_testing Then
            If Not DefaultDebtorHelper.check_module() Then
                ll_testing = Me.global_ws.user_information.save_user_information()
            End If

            If Not ll_testing Then
                Me.alert_problem_register_user.Text = global_trans.translate_message("message_cannot_registrate_because_logincode", "Wij kunnen u niet registren, want het emailadres/logincode wordt al gebruikt.", Me.global_ws.Language)
                Me.alert_problem_register_user.Visible = True
            End If
        End If

        If ll_testing Then
            Try
                If Me.global_ws.get_webshop_setting("create_acct_optional") = 0 Or Me.global_ws.get_webshop_setting("create_acct_optional") = 2 Or (Me.global_ws.get_webshop_setting("create_acct_optional") = 1 And uc_address_block1.create_account) Then

                    ' Verstuur hier de registratie email, indien gebruiker account heeft aangemaakt of automatisch is aangemaakt
                    Dim ws_email_registration As New Utilize.Web.Solutions.Webshop.Email.ws_email_registration
                    ws_email_registration.customer_user_id = Me.global_ws.user_information.user_id
                    If Me.uc_address_block1.use_login And Me.global_ws.get_webshop_setting("create_acct_optional") = 2 Then
                        ws_email_registration.customer_password = Me.uc_address_block1.customer_user_password
                    End If

                    ws_email_registration.language = Me.global_ws.Language

                    If Not ws_email_registration.send_email() Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "mailing_email_address_empty", "alert('" + global_trans.translate_message("message_email_not_able_to_send", "Email kon niet worden verstuurd. Neem contact met ons op voor verdere verwerking.", Me.global_ws.Language) + "');", True)
                    End If

                End If
            Catch ex As Exception

            End Try
        End If

        If ll_testing Then
            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If

        If ll_testing Then
            Me.ph_registration.Visible = False
            Me.ph_buttons.Visible = False

            Me.ph_registration_complete.Visible = True
            Me.text_registration_successfull_order.Visible = True
        End If

        ' Als registreren is toegestaan en de gebruiker mag ingelogd blijven, dan hoeft deze niet uitgelogd te worden.
        If ll_testing And Not Me.global_ws.get_webshop_setting("register_enabled") = 2 Then
            Me.global_ws.user_information.logout()
            Me.text_registration_successfull_no_order.Visible = True
            Me.text_registration_successfull_order.Visible = False
        ElseIf ll_testing Then
            'If fine to stay logged in, then redirect to account overview page
            Response.Redirect("~/" + me.global_ws.language + "/Webshop/Account/Account.aspx", True)
        End If
    End Sub

    Private Sub uc_register_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' Initialize the captcha on main panel since it Is visible on page load
        reCaptchaScript("div_recaptcha")
    End Sub

    Private Sub reCaptchaScript(ByVal element As String)
        ' Initialize the captcha field by element ID for panel
        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "reCaptchaSettings", "var reCaptchaSettings = {'element':'" + element + "','settings': {'sitekey':'" + reCAPTCHAApiKey + "','theme':'" + reCAPTCHATheme + "','size':'" + reCAPTCHASize + "'}};", True)
        If Page.IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "reCaptcha", "reCaptchaSetUp.init(1);", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType(), "OnLoad", "jQuery(document).ready(function($){ reCaptchaSetUp.init(); });", True)
        End If
    End Sub

    Private Function VerifyReCaptcha() As Boolean
        Dim reCaptcha_Verified As Boolean = False
        Dim requestString = String.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", reCAPTCHAApiSecret, Request("g-recaptcha-response"))
        Dim req = CType(WebRequest.Create(requestString), HttpWebRequest)

        Using reCaptchaResponse As WebResponse = req.GetResponse()
            Dim serializer As DataContractJsonSerializer = New DataContractJsonSerializer(GetType(ReCaptchaResponseRegister))
            Dim gResponse As ReCaptchaResponseRegister = TryCast(serializer.ReadObject(reCaptchaResponse.GetResponseStream()), ReCaptchaResponseRegister)
            If gResponse.Success Then
                reCaptcha_Verified = True
            End If
        End Using

        Return reCaptcha_Verified
    End Function

#End Region

End Class
