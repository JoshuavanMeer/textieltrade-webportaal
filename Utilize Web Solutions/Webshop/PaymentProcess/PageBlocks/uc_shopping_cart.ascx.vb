﻿
Partial Class Webshop_CheckOut_Blocks_uc_shopping_cart
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

#Region "Page subroutines"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        Me.set_style_sheet("uc_shopping_cart.css", Me)
        Me.set_style_sheet("~/_THMAssets/plugins/jquery-steps/css/custom-jquery.steps.css", Me)

        Me.button_order_step_2.Text = global_trans.translate_button("button_order_step_2", "Bestellen", Me.global_ws.Language)
        Me.button_continue_shopping.Text = global_trans.translate_button("button_continue_shopping", "Verder winkelen", Me.global_ws.Language)

        Me.button_order_step_21.Text = global_trans.translate_button("button_order_step_2", "Bestellen", Me.global_ws.Language)
        Me.button_continue_shopping1.Text = global_trans.translate_button("button_continue_shopping", "Verder winkelen", Me.global_ws.Language)

        ' Wizard wordt alleen getoond als het een stappen proces is, niet wanneer het een single checkout is.
        Me.ph_wizard.Visible = Not Me.global_ws.get_webshop_setting("checkout_type") = 1
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.uc_shopping_cart_overview1.DataBind()

        Me.text_shopping_cart.Visible = Me.global_ws.shopping_cart.product_count > 0
        Me.button_order_step_2.Visible = Me.global_ws.shopping_cart.product_count > 0
        Me.button_order_step_21.Visible = Me.global_ws.shopping_cart.product_count > 0
        Me.ph_navigation_top.Visible = Me.global_ws.shopping_cart.product_count > 0
    End Sub
#End Region

#Region "Buttons"
    Protected Sub button_order_step_2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_order_step_2.Click, button_order_step_21.Click
        CType(Me.uc_shopping_cart_overview1, Webshop_CheckOut_Blocks_uc_shopping_cart_overview).recalculate_order_quantity(Nothing, Nothing)

        Response.Redirect(Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/identification.aspx"), True)
    End Sub

    Protected Sub button_continue_shopping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_continue_shopping.Click, button_continue_shopping1.Click
        If Not Session("ProductPage") Is Nothing Then
            Response.Redirect(Session("ProductPage"))
        Else
            Response.Redirect("~/home.aspx")
        End If
    End Sub
#End Region
End Class
