﻿reCaptchaSetUp = {
    reset: 0,
    attempt: 0,
    init: function (reset) {
        reset && (this.reset = reset);
        /* grecaptcha (google) often not ready on Dom ready */
        if (!window.grecaptcha && this.attempt < 10) {
            this.attempt;
            setTimeout(function () { reCaptchaSetUp.init(); }, 200);
        }
        else {
            if (!window.reCaptchaSettings) {
                return;
            }
            this.attempt = 0;
            if (this.reset) {
                try {
                    grecaptcha.reset();
                } catch (e) { /* do nothing */ }
            }
            try {
                grecaptcha.render(reCaptchaSettings.element, reCaptchaSettings.settings);
            } catch (e) { /* do nothing */ }
        }
    }
};