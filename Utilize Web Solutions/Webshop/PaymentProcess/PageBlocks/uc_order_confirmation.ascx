﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_confirmation.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_order_confirmation" %>

    <div class="order_confirmation">
        <utilize:placeholder runat="server" ID="ph_order_placed" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_order_placed" Text="Uw bestelling is geplaatst!"></utilize:translatetitle></h1>
            <utilize:translatetext runat='server' ID="text_order_placed" Text="Uw bestelling is geplaatst, u ontvangt snel een bevestiging van uw bestelling per email."></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_order_pending" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_order_pending" Text="Nog geen bevestiging van betaling"></utilize:translatetitle></h1>
            <utilize:translatetext runat='server' ID="text_order_pending" Text="Wij wachten op een bevestiging van uw betaling. Zodra deze binnen is zullen wij uw bestelling in behandeling nemen."></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_order_refused" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_order_refused" Text="Fout opgetreden"></utilize:translatetitle></h1>
            <utilize:translatetext runat='server' ID="text_order_refused" Text="Er is een fout opgetreden bij het verwerken van uw betaling."></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_order_expired" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_order_expired" Text="Uw sessie was verlopen"></utilize:translatetitle></h1>
            <utilize:translatetext runat='server' ID="text_order_expired" Text="Het betaal process heeft te lang still gestaan en is verlopen"></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_order_canceled" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_order_canceled" Text="U heeft de betaling afgebroken"></utilize:translatetitle></h1>
            <utilize:translatetext runat="server" id="text_order_canceled" Text="Het betaal proces was afgebroken."></utilize:translatetext>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_intermediate_failure" Visible="false">
            <h1><utilize:translatetitle runat="server" ID="title_crit_failure" Text="Er is iets fout gegaan"></utilize:translatetitle></h1>
            <utilize:translatetext runat="server" id="text_please_contact" Text="Er is iets fout gegaan met het verwerken van uw order. Neem contact met ons op"></utilize:translatetext>
            <div>
                <b><utilize:translateliteral runat="server" ID="lt_your_ordernumber" Text="Uw ordernummer: "></utilize:translateliteral></b>
                <utilize:literal runat="server" ID="lt_ord_nr"></utilize:literal>
            </div>
        </utilize:placeholder>

        <div class="buttons">
            <utilize:translatebutton runat='server' ID="button_go_to_order" CssClass="button_go_to_order btn-u btn-u-sea-shop btn-u-lg" Text="Ga naar bestelling"   />
            <utilize:translatebutton runat='server' ID="button_continue_shopping" CssClass="button_continue_shopping btn-u btn-u-sea-shop btn-u-lg" Text="Verder winkelen"   />
        </div>
    </div>
