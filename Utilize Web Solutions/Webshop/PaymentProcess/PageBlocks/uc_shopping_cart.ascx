﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_shopping_cart.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_shopping_cart" %>

<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_overview.ascx" tagname="uc_shopping_cart_overview" tagprefix="utilize" %>
<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_totals.ascx" tagname="uc_shopping_cart_totals" tagprefix="utilize" %>
<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_coupon_code.ascx" tagname="uc_coupon_code" tagprefix="utilize" %>

<utilize:placeholder runat="server" ID="ph_shopping_cart">
    <div class="shopping-cart">
        <div class="wizard margin-bottom-20">
            <utilize:panel runat="server" ID="ph_wizard" class="steps clearfix">
                <ul role="tablist">
                    <div class="row">
                        <div class="col-md-4">
                            <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
                                <a id="steps-uid-0-t-0" href="#" aria-controls="steps-uid-0-p-0"><span class="current-info audible">current step: </span><span class="number">1.</span>             
                                    <div class="overflow-h">
                                        <h2><utilize:translatetitle runat="server" ID="title_shopping_cart_steps" Text="Winkelwagen"></utilize:translatetitle></h2>
                                        <utilize:translatetext runat="server" ID="text_review_and_edit" Text="Controleer jouw winkelwagen"></utilize:translatetext>
                                        <i class="rounded-x fa fa-check"></i>
                                    </div>    
                                </a>
                            </li>
                        </div>
                        <div class="col-md-4">
                            <li role="tab" class="disabled" aria-disabled="true">
                                <a id="steps-uid-0-t-1" href="#" aria-controls="steps-uid-0-p-1"><span class="number">2.</span> 
                                    <div class="overflow-h">
                                        <h2><utilize:translatetitle runat="server" ID="title_billing_info" Text="Betalingsinformatie"></utilize:translatetitle></h2>
                                        <utilize:translatetext runat="server" ID="text_billing_info" Text="Adres gegevens"></utilize:translatetext>
                                        <i class="rounded-x fa fa-home"></i>
                                    </div>    
                                </a>
                            </li>
                        </div>
                        <div class="col-md-4">
                            <li role="tab" class="disabled last" aria-disabled="true">
                                <a id="steps-uid-0-t-2" href="#" aria-controls="steps-uid-0-p-2"><span class="number">3.</span> 
                                    <div class="overflow-h">
                                        <h2><utilize:translatetitle runat="server" ID="title_payment" Text="Betalingsmethode"></utilize:translatetitle></h2>
                                        <utilize:translatetext runat="server" ID="text_payment" Text="Kies een betalingsmethode"></utilize:translatetext>
                                        <i class="rounded-x fa fa-credit-card"></i>
                                    </div>    
                                </a>
                            </li>
                        </div>
                    </div>
                </ul>
            </utilize:panel>

            <h1><utilize:translatetitle runat="server" ID="title_shopping_cart"></utilize:translatetitle></h1>
            <div class="block">
                <utilize:translatetext runat="server" ID="text_shopping_cart" Text="Hieronder wordt de inhoud van uw winkelwagen weergegeven."></utilize:translatetext>
            </div>
    
            <utilize:placeholder runat="server" ID="ph_navigation_top">
                <div class="block margin-bottom-10">
                    <div class="buttons">
                        <utilize:button runat='server' ID="button_continue_shopping1" Text="Verder winkelen" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
                        <utilize:button runat='server' ID="button_order_step_21" Text="Bestellen" CssClass="btn-u btn-u-sea-shop btn-u-lg pull-right" />
                    </div>
                </div>
            </utilize:placeholder>

            <div class="block margin-bottom-10">
                <utilize:uc_shopping_cart_overview ID="uc_shopping_cart_overview1" runat="server" show_delete_item="true" show_quantity_box="true"/>
            </div>
    
            <div class="coupon-code">
                <div class="row">
                    <utilize:uc_coupon_code ID="uc_coupon_code1" runat="server" />
                        <utilize:uc_shopping_cart_totals ID="uc_shopping_cart_totals1" runat="server" />
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="actions">
                    <div class="pull-left"><utilize:translatebutton runat='server' ID="button_continue_shopping" Text="Verder winkelen" CssClass="btn-u btn-u-sea-shop btn-u-lg" /></div>
                    <div class="pull-right"><utilize:translatebutton runat='server' ID="button_order_step_2" Text="Bestellen" CssClass="btn-u btn-u-sea-shop btn-u-lg" /></div>
            </div>
        </div>
    </div>
</utilize:placeholder>