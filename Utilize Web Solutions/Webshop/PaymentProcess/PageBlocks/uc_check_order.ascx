﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_check_order.ascx.vb" Inherits="Webshop_CheckOut_Blocks_uc_check_order" %>

<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_overview.ascx" tagname="uc_shopping_cart_overview" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_totals.ascx" tagname="uc_shopping_cart_totals" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_address_delivery_text.ascx" tagname="uc_address_delivery_text" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_address_invoice_text.ascx" tagname="uc_address_invoice_text" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx" tagname="uc_address_block" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_order_description.ascx" tagname="uc_order_description" tagprefix="utilize" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_delivery_methods.ascx" tagname="uc_delivery_methods" tagprefix="utilize" %>

<div class="shopping-cart col-md-12">
    <div class="wizard margin-bottom-20">

        <div class="steps clearfix">
            <ul role="tablist">
                <div class="row">
                    <div class="col-md-4">
                        <li role="tab" class="first done" aria-disabled="true">
                            <utilize:hyperlink runat="server" id="hl_first_step" aria-controls="steps-uid-0-p-0"><span class="current-info audible">current step: </span><span class="number">1.</span>             
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_shopping_cart_steps" Text="Winkelwagen"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_review_and_edit" Text="Controleer jouw winkelwagen"></utilize:translatetext>
                                    <i class="rounded-x fa fa-check"></i>
                                </div>    
                            </utilize:hyperlink>
                        </li>
                    </div>
                    <div class="col-md-4">
                        <li role="tab" class="done" aria-disabled="false" aria-selected="true">
                            <utilize:hyperlink runat="server" id="hl_second_step" aria-controls="steps-uid-0-p-1"><span class="number">2.</span> 
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_billing_info" Text="Betalingsinformatie"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_billing_info" Text="Adres gegevens"></utilize:translatetext>
                                    <i class="rounded-x fa fa-home"></i>
                                </div>    
                            </utilize:hyperlink>
                        </li>
                    </div>
                    <div class="col-md-4">
                        <li role="tab" class="current last" aria-disabled="true">
                            <a id="steps-uid-0-t-2" href="#" aria-controls="steps-uid-0-p-2"><span class="number">3.</span> 
                                <div class="overflow-h">
                                    <h2><utilize:translatetitle runat="server" ID="title_payment" Text="Betalingsmethode"></utilize:translatetitle></h2>
                                    <utilize:translatetext runat="server" ID="text_payment" Text="Kies een betalingsmethode"></utilize:translatetext>
                                    <i class="rounded-x fa fa-credit-card"></i>
                                </div>    
                            </a>
                        </li>
                    </div>
                </div>
            </ul>
        </div>

    <div class="block">
        <h1><utilize:translatetitle runat="server" ID="title_check_order" Text="Overzicht bestelling"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_check_order" Text="Controleer hier uw gegevens en kies 'Betalen' om de bestelling af te ronden."></utilize:translatetext>
    </div>

    <utilize:placeholder runat="server" ID="ph_address">
        <div class="row">
            <utilize:placeholder runat="server" ID="ph_invoice_address_text">
                <div class=" col-md-6 md-margin-bottom-40">
                    <div class="margin-bottom-10"><utilize:uc_address_invoice_text ID="uc_address_invoice1" runat="server" block_css="false" /></div>
                    <utilize:translatebutton runat="server" id="button_change_invoice_address" Text="Wijzigen" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
                </div>
                </utilize:placeholder>
                <div class=" col-md-6 md-margin-bottom-40">
                    <div class="margin-bottom-10"><utilize:uc_address_delivery_text ID="uc_address_delivery1" runat="server" block_css="false" /></div>
                    <utilize:translatebutton runat="server" id="button_change_delivery_address" Text="Wijzigen" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
            </div> 
        </div>
        
        <utilize:modalpanel runat="server" ID="pnl_overlay_delivery" Visible="false">
            <div class="modal-content">
                <div class="modal-header">
                    <h2><utilize:translatetitle runat="server" ID="title_change_delivery_address" Text="Afleveradres wijzigen"></utilize:translatetitle></h2>
                </div>
                <div class="modal-body">
                    <utilize:uc_address_block ID="uc_address_block2" runat="server" address_type="shoppingcart_delivery_address" />
                </div>
                <div class="modal-footer">
                    <utilize:translatebutton runat="server" ID="button_cancel1" Text="Annuleren" CssClass="left btn-u btn-u-sea-shop btn-u-lg" /> 
                    <utilize:translatebutton runat="server" ID="button_change2" Text="Wijzigen" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
                </div>
            </div>
        </utilize:modalpanel>

        <utilize:modalpanel runat="server" ID="pnl_overlay_invoice" Visible="false">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2><utilize:translatetitle runat="server" ID="title_change_invoice_address" Text="Factuuradres wijzigen"></utilize:translatetitle></h2>
                    </div>
                    <div class="modal-body">
                        <utilize:uc_address_block ID="uc_address_block1" runat="server" address_type="shoppingcart_invoice_address" />
                    </div>
                    <div class="modal-footer">
                        <utilize:translatebutton runat="server" ID="button_cancel" Text="Annuleren" CssClass="left btn-u btn-u-sea-shop btn-u-lg"/> 
                        <utilize:translatebutton runat="server" ID="button_change" Text="Wijzigen" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
                    </div>
                </div>
        </utilize:modalpanel>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_order_description" Visible="false">
        <utilize:uc_order_description ID="uc_order_description1" runat="server" />
        <div class="row margin-bottom-20"></div>
    </utilize:placeholder>
    
    <utilize:placeholder runat="server" ID="ph_send_extra_confirmation" Visible="false">
        <div class="row">
            <div class="col-md-12">
                <h2><utilize:translatetitle ID="title_sales_information" Text="Informatie orderbevestiging" runat="server"></utilize:translatetitle></h2>
                <utilize:translatetext runat="server" ID="text_sales_information" Text="Pas indien gewenst de informatie hieronder aan om de email naar een ander adres te sturen en/of de aanhef wijzigen."></utilize:translatetext>
            </div>
        </div>
        <div class="row margin-bottom-20">
            <div class="col-md-6">
                <utilize:translateliteral ID="label_customer_salutation" Text="Aanhef orderbevestiging" runat="server"></utilize:translateliteral>
            </div>
            <div class="col-md-6">
                <div>
                    <utilize:textbox runat="server" ID="txt_customer_salutation" CssClass="form-control"></utilize:textbox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <span class="checkbox margin">
                    <utilize:checkbox runat="server" ID="chk_send_email_to_customer" name="chk_send_email_to_customer" />
                    <label for="chk_send_email_to_customer"><utilize:translateliteral ID="label_send_email_to_customer" Text="Extra orderbevestiging sturen naar het volgende emailadres" runat="server"></utilize:translateliteral></label>
                </span>
            </div>
            <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <utilize:textbox runat="server" ID="txt_email_address" CssClass="form-control"></utilize:textbox>
                    </div>
            </div>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_shopping_cart">
        <h2><utilize:translatetitle runat="server" ID="title_shopping_cart"></utilize:translatetitle></h2>
        <utilize:uc_shopping_cart_overview ID="uc_shopping_cart_overview1" runat="server" show_delete_item="false" show_quantity_box="false" block_css="false"/>
            
         <utilize:panel runat="server" ID="pnl_stock_message" Visible="false" cssclass="alert alert-danger">
            <utilize:literal runat="server" ID="lt_stock_message"></utilize:literal>

            <utilize:translatebutton runat="server" ID="button_delete_stock_products" Text="Verwijderen" CssClass="btn-u btn-u-sea-shop btn-u-lg pull-right" />
            <br style="clear: both" />
        </utilize:panel>

        <div class="coupon-code margin-bottom-20">
            <div class="row">
                <div class="col-sm-4"></div>
                <utilize:uc_shopping_cart_totals ID="uc_shopping_cart_totals1" runat="server" block_css="false" />
                <br style="clear: both;" />
            </div>
        </div>
    </utilize:placeholder>

    <div class="margin-bottom-20">
        <utilize:placeholder runat='server' ID="ph_delivery_methods">
                <utilize:uc_delivery_methods ID="uc_delivery_methods1" runat="server" />
        </utilize:placeholder>
    </div>

    <utilize:placeholder runat="server" ID="ph_payment_services">

    </utilize:placeholder>

    <div class="block">
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_previous_step" Text="Vorige stap" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </div>
    <br style="clear: both;" />
    </div>
</div>