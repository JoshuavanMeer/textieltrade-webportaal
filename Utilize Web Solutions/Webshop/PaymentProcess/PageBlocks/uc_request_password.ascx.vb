﻿
Imports System.Net.Mail

Partial Class uc_request_password
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _error_message As String = ""

    Protected Sub button_request_password_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_request_password.Click
        Me.message_mandatory_fields_not_filled.Visible = False
        Dim input_value As String = Me.txt_email_address.Text.Trim()

        Dim logon_for_reset As Boolean = Me.global_ws.get_webshop_setting("use_logon_for_reset")

        ''''''''''''''''''''''''''''''''''''''''
        '' Hieronder voeren we de standaard controles uit
        ''''''''''''''''''''''''''''''''''''''''
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not input_value = ""

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_ws.Language)
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_resume AndAlso Not logon_for_reset Then
            Try
                Dim email_address_check As MailAddress = New MailAddress(Me.txt_email_address.Text)
                ll_resume = True
            Catch ex As Exception
                ll_resume = False
            End Try

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_email_address_incorrect", "Uw email adres is niet correct.", Me.global_cms.Language)
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If


        Dim dt_user_information As System.Data.DataTable = Nothing

        If ll_resume Then
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "*"
            qsc_select.select_from = "uws_customers_users"

            If logon_for_reset Then
                qsc_select.select_where += "uws_customers_users.login_code = @input_value "
            Else
                qsc_select.select_where += "uws_customers_users.email_address = @input_value "
            End If

            ' Voor tijdelijke accounts kan het wachtwoord niet opgevraagd worden.
            qsc_select.select_where += "and uws_customers_users.temp_account = 0 "

            ' Voor blocked accounts kan het wachtwoord niet opgevraagd worden.
            qsc_select.select_where += "and uws_customers_users.blocked = 0 "

            qsc_select.add_parameter("input_value", input_value)

            dt_user_information = qsc_select.execute_query()

            ll_resume = dt_user_information.Rows.Count > 0

            If Not ll_resume Then
                If logon_for_reset Then
                    Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_login_code_invalid", "De ingevulde logincode is niet bekend bij ons.", Me.global_ws.Language)
                Else
                    Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_email_addresses_invalid", "Het ingevulde emailadres is niet bekend bij ons.", Me.global_ws.Language)
                End If
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_resume Then
            ll_resume = dt_user_information.Rows.Count = 1

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_error_occured", "Er is een fout opgetreden bij het ophalen van de gegevens.", Me.global_ws.Language)
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If


        ''''''''''''''''''''''''''''''''''''''''
        '' Hier maken we een nieuw wachtwoord aan en slaan we versleuteld op in de database
        ''''''''''''''''''''''''''''''''''''''''
        Dim lc_new_password As String = ""
        Dim ubo_customer As Utilize.Data.BusinessObject = Nothing

        If ll_resume Then
            ' Maak een nieuw wachtwoord aan en sla deze op bij de gebruiker
            lc_new_password = Me.global_ws.generate_password()

            ' Maak een businessobject van klanten aan
            ubo_customer = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", dt_user_information.Rows(0).Item("hdr_id"), "rec_id")

            ' Controleer of de klant gevonden is
            ll_resume = ubo_customer.data_tables("uws_customers").data_row_count > 0
        End If

        If ll_resume Then
            ' Zoek de webshop gebruiker op
            ll_resume = ubo_customer.table_locate("uws_customers_users", "rec_id = '" + dt_user_information.Rows(0).Item("rec_id") + "'")
        End If

        If ll_resume Then
            ' Wijzig het wachtwoord
            ubo_customer.field_set("uws_customers_users.login_password", lc_new_password)
            ubo_customer.field_set("uws_customers_users.last_password_update", New DateTime(1900, 1, 1))

            ll_resume = ubo_customer.table_update(Me.global_ws.user_information.user_id)
        End If

        ''''''''''''''''''''''''''''''''''''''''
        '' Hier versturen we de email
        ''''''''''''''''''''''''''''''''''''''''
        If ll_resume Then
            ' Verstuur hier de email
            Dim lo_send_password_mail As New ws_email_password

            lo_send_password_mail.UserId = dt_user_information.Rows(0).Item("rec_id")
            lo_send_password_mail.email_address = dt_user_information.Rows(0).Item("email_address")
            lo_send_password_mail.language = Me.global_ws.Language
            lo_send_password_mail.password = lc_new_password

            ll_resume = lo_send_password_mail.send_email()

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = lo_send_password_mail.error_message
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        ''''''''''''''''''''''''''''''''''''''''
        '' Als alles gelukt is verbergen we een en ander
        ''''''''''''''''''''''''''''''''''''''''
        If ll_resume Then
            Me.pnl_request_password.Visible = False
            Me.pnl_request_password_sent.Visible = True
        End If
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_request_password.css", Me)

        Dim logon_for_reset As Boolean = Me.global_ws.get_webshop_setting("use_logon_for_reset")

        Me.text_forgot_password.Visible = Not logon_for_reset
        Me.text_forgot_pass_login.Visible = logon_for_reset

        Dim placeholder_text As String = ""

        If logon_for_reset Then
            placeholder_text = global_trans.translate_label("label_your_logoncode", "Vul uw logincode in", Me.global_ws.Language)
        Else
            placeholder_text = global_trans.translate_label("label_your_email_address", "Email adres", Me.global_ws.Language)
        End If

        Me.txt_email_address.Attributes.Add("placeholder", placeholder_text)
    End Sub
End Class
