﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_request_password.ascx.vb" Inherits="uc_request_password" %>
<div class="uc_request_password">
    <h1><utilize:translatetitle runat="server" ID="title_request_password" Text="Wachtwoord opvragen"></utilize:translatetitle></h1>
    <utilize:panel runat="server" ID="pnl_request_password" DefaultButton="button_request_password">
        <utilize:translatetext runat="server" ID="text_forgot_pass_login" Text="Vul in het onderstaande veld uw logincode in om uw wachtwoord naar het gekoppelde emailadres te laten versturen"></utilize:translatetext>
        <utilize:translatetext runat="server" ID="text_forgot_password" Text="Vul de onderstaande velden in om uw wachtwoord naar het ingevulde emailadres te laten versturen"></utilize:translatetext>
        <div class="form-group">
            <utilize:textbox ID="txt_email_address" runat="server" CssClass="value form-control"></utilize:textbox>
        </div>
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_request_password" CssClass="right btn-u btn-u-sea-shop btn-u-lg" Text="Opvragen" />
        </div>
        <utilize:label runat='server' ID="message_mandatory_fields_not_filled" Visible="false" CssClass="message"></utilize:label>
    </utilize:panel>
    <utilize:panel runat="server" ID="pnl_request_password_sent" Visible="false">
        <utilize:translatetext runat="server" ID="text_password_sent" Text="Uw wachtwoord is succesvol verstuurd naar het ingevulde email adres."></utilize:translatetext>
    </utilize:panel>
</div>
