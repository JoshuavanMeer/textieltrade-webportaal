﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_register.ascx.vb" Inherits="uc_register" %>

<%@ Register src="~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx" tagname="uc_address_block" tagprefix="utilize" %>

<div class="uc_register">
    <h1><utilize:translatetitle runat="server" ID="title_register" Text="Registreren"></utilize:translatetitle></h1>
    <div class="row no-margin-right">
        <utilize:placeholder runat='server' ID="ph_registration" Visible="true">
            <div class="col-md-8 mb-margin-bottom-30">
                <div class="block">
                    <h3>
                        <utilize:translatetitle runat="server" ID="title_new_customer" Text="Bent u een nieuwe klant?"></utilize:translatetitle></h3>
                    <utilize:translatetext runat="server" ID="text_new_customer" Text="Wij willen u graag van harte welkom heten als nieuwe klant. Vul hieronder uw gegevens in om een account bij ons aan te maken."></utilize:translatetext>
                </div>
            </div>
            <utilize:uc_address_block ID="uc_address_block1" runat="server" address_type="customer_invoice_address" use_login="true" />
             
            <utilize:placeholder runat='server' ID="ph_change_del_address" Visible="true">
           <div class="col-md-8 mb-margin-bottom-30">
                <div class="block">
                    <h3>
                        <utilize:translatetitle runat="server" ID="title_different_delivery_address" Text="Wilt u uw bestelling standaard op een ander adres af laten leveren?"></utilize:translatetitle></h3>
                    <utilize:translatetext runat="server" ID="text_different_delivery_address" Text="Wanneer u uw bestelling op een ander adres wilt af laten leveren dan de factuur, dan kunt u dat hieronder aangeven."></utilize:translatetext>

                </div>
            </div>
            <utilize:uc_address_block ID="uc_address_block2" runat="server" address_type="customer_delivery_address" />
        </utilize:placeholder>
        </utilize:placeholder>

        <utilize:placeholder runat='server' ID="ph_buttons" Visible="true">
            <div class="col-md-8 mb-margin-bottom-30">
                <div class="block">
                    <asp:PlaceHolder runat="server" ID="ph_recaptcha">
                        <div id="div_recaptcha" class="g-recaptcha" data-sitekey="<%=reCAPTCHAApiKey%>" data-theme="<%=reCAPTCHATheme%>" data-size="<%=reCAPTCHASize%>"></div>
                    </asp:PlaceHolder>
                    <div class="alert alert-danger" runat="server" id="div_recaptcha_alert" visible="false">
                        <utilize:literal runat="server" ID="lt_recaptcha_error"></utilize:literal>
                    </div>
                    <div class="buttons">
                        <utilize:translatebutton runat="server" ID="button_register" Text="Registreren" CssClass="btn-u btn-u-sea-shop btn-block margin-bottom-20" />
                    </div>
                </div>
                <utilize:alert runat="server" ID="alert_problem_register_user" AlertType="danger" Visible="false"></utilize:alert>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_registration_complete" Visible="false">
            <div class="col-md-8 mb-margin-bottom-30">
                <div class="block">
                    <h3>
                        <utilize:translatetitle runat="server" ID="title_registration_successvol" Text="Registratie voltooid"></utilize:translatetitle></h3>
                    <utilize:translatetext runat="server" ID="text_registration_successfull_no_order" Text="Hartelijk dank voor uw registratie, wij nemen zo snel mogelijk contact met u op om u toegang te geven tot onze webshop." Visible="false"></utilize:translatetext>
                    <utilize:translatetext runat="server" ID="text_registration_successfull_order" Text="Hartelijk dank voor uw registratie, u kunt nu direct bestellen in onze webshop." Visible="false"></utilize:translatetext>
                </div>
            </div>
        </utilize:placeholder>
    </div>
</div>