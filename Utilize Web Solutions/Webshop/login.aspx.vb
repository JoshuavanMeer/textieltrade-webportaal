﻿

<Serializable()>
Public Class login
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("login_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_login", "Inloggen", Me.global_ws.Language)
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Webshop/Modules/Login/uc_logon_page.ascx")
        uc_block.ID = "uc_login"

        ph_target_block.Controls.Add(uc_block)
    End Sub
End Class
