﻿Imports System.Web.Services
Imports System.IO
Imports Utilize.Web.Solutions.Webshop

Partial Class Webshop_WebshopAPI_ProductListWebmethods
    Inherits System.Web.UI.Page

    Private Shared _settings As System.Data.DataRow
    Private Shared ReadOnly Property settings As System.Data.DataRow
        Get
            If _settings Is Nothing Then
                Dim qsc_settings As New Utilize.Data.QuerySelectClass
                With qsc_settings
                    .select_fields = "*"
                    .select_from = "uws_settings"
                End With
                _settings = qsc_settings.execute_query().Rows(0)
            End If

            Return _settings
        End Get
    End Property

    Private Shared _prod_comm_character_count As Integer = settings.Item("short_desc_count")
    Private Shared _prod_comm_character_separator As String = "."

    <WebMethod()>
    Public Shared Function GetProductList(prod_codes As List(Of String), language As String, addedCount As Integer) As Object
        Try
            Dim controlpage As New CMSPageTemplate

            Dim Form As New HtmlForm
            Form.ViewStateMode = ViewStateMode.Disabled

            Dim div_products As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            div_products.Attributes.Add("class", "product-row")

            Form.Controls.Add(div_products)

            Dim dict_decimals As New Dictionary(Of String, Integer)
            Dim counter As Integer = addedCount
            For Each element In prod_codes
                If Not element Is Nothing Then
                    Dim uc_product_row As New Utilize.Web.Solutions.Base.base_usercontrol_base
                    uc_product_row = controlpage.LoadUserControl("~/webshop/modules/overviewcontrols/uc_product_row.ascx")
                    uc_product_row.set_property("product_row", GetMissingProductData(element, language))

                    div_products.Controls.Add(uc_product_row)

                    If counter Mod 2 = 0 Then
                        Dim mobclearfix As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        mobclearfix.Attributes.Add("class", "mobile-clearfix")
                        div_products.Controls.Add(mobclearfix)
                    End If

                    counter += 1

                    Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(element)
                    dict_decimals.Add(element, ln_decimal_numbers)
                End If
            Next

            Dim clearfix As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            clearfix.Attributes.Add("class", "clearfix")
            div_products.Controls.Add(clearfix)

            Dim div_end_anchor As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            div_end_anchor.Attributes.Add("class", "end-anchor")

            Form.Controls.Add(div_end_anchor)

            controlpage.Controls.Add(Form)

            Using sw As StringWriter = New StringWriter
                HttpContext.Current.Server.Execute(controlpage, sw, False)

                Dim htmlString As String = sw.ToString()
                Return New With {.html = htmlString, .decimals = dict_decimals}
            End Using
        Catch ex As Exception
            Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()

            Throw New Exception(global_trans.translate_label("lbl_wrong", "Er is iets fout gegaan bij het ophalen van de producten", loGlobalWs.Language))
        End Try
    End Function

    Private Shared Function GetMissingProductData(ByVal prod_code As String, language As String) As System.Data.DataRow
        ' Because we only have the products codes and not the entire data
        ' Add the product codes to a string 
        Dim qsc_details As New Utilize.Data.QuerySelectClass

        ' Get the product details
        With qsc_details
            .select_fields = "uws_products_tran.*, uws_products.*, '' as prod_comm_short, CASE WHEN uws_products_tran.shp_grp = '' THEN uws_products_tran.prod_code ELSE uws_products_tran.shp_grp END as group_code"
            .select_from = "uws_products"
            .select_where = "uws_products_tran.lng_code = @lng_code"
        End With

        If Not String.IsNullOrEmpty(prod_code) Then
            qsc_details.select_where += " and uws_products_tran.prod_code = @prod_code and uws_products_tran.lng_code = @lng_code"
        End If

        qsc_details.add_join("INNER JOIN", "uws_products_tran", "uws_products.prod_code = uws_products_tran.prod_code")
        qsc_details.add_parameter("lng_code", language)
        qsc_details.add_parameter("prod_code", prod_code)

        Dim dt_details As System.Data.DataTable = qsc_details.execute_query()

        FixCollectedProducts(dt_details, language)

        Return dt_details.Rows(0)
    End Function

    Private Shared Sub FixCollectedProducts(ByRef dt_target As System.Data.DataTable, language As String)
        dt_target.Columns.Item("prod_comm_short").ReadOnly = False
        dt_target.Columns.Item("prod_comm_short").MaxLength = Integer.MaxValue
        dt_target.AcceptChanges()

        ' Advanced product filters aka product configurator
        Dim ll_utlz_ws_prod_filters As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters")

        ' Hier gaan we de HTML Tags verwijderen uit het prod_comm gedeelte
        For Each dr_data_row As System.Data.DataRow In dt_target.Rows
            ' Vul een verkorte product_comm in of als korte vul alles 
            Dim str_prod_comm_short As String = dr_data_row("prod_comm").ToString
            If _prod_comm_character_count < str_prod_comm_short.Length Then
                dr_data_row("prod_comm_short") = str_prod_comm_short.Substring(0, str_prod_comm_short.LastIndexOf(_prod_comm_character_separator, _prod_comm_character_count) + 1)
            Else
                dr_data_row("prod_comm_short") = str_prod_comm_short
            End If

            'Haal hier de HTML tags uit de korte omschrijving
            dr_data_row.Item("prod_comm_short") = System.Text.RegularExpressions.Regex.Replace(dr_data_row.Item("prod_comm_short"), "<(.|\n)*?>", String.Empty)

            ' Haal de images op
            dr_data_row("thumbnail_normal") = get_image(dr_data_row("thumbnail_normal"), "thumbnail_normal")
            dr_data_row("thumbnail_large") = get_image(dr_data_row("thumbnail_normal"), "thumbnail_large")
            dr_data_row("image_small") = get_image(dr_data_row("image_small"), "image_small")
            dr_data_row("image_normal") = get_image(dr_data_row("image_normal"), "image_normal")
            dr_data_row("image_large") = get_image(dr_data_row("image_large"), "image_large")

            ' Advanced product filters aka product configurator
            If ll_utlz_ws_prod_filters Then
                Dim ll_products_is_main_part_of_set As Boolean = Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_part_of_set(dr_data_row("prod_code"))
                ' Het product is een hoofdproduct van een set
                If ll_products_is_main_part_of_set Then
                    ' Zet de omschrijving van het product naar de set omschrijving
                    dr_data_row("prod_desc") = Utilize.Web.Solutions.Webshop.ws_procedures.get_set_description(dr_data_row("prod_code"), dr_data_row("prod_desc"), language)
                End If
            End If
        Next
        dt_target.AcceptChanges()

        dt_target.Columns.Item("prod_comm_short").ReadOnly = True
        dt_target.AcceptChanges()
    End Sub


    Private Shared Function get_image(image_url As String, image_size As String) As String

        Dim lc_image_file_path As String = get_default_image(image_size)

        If Not String.IsNullOrWhiteSpace(image_url) Then
            If Not image_url.StartsWith("http") AndAlso System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/") + image_url) Then
                lc_image_file_path = image_url
            ElseIf image_url.StartsWith("http") Then

                lc_image_file_path = image_url

            End If
        End If
        Return lc_image_file_path

    End Function

    Private Shared Function get_default_image(image_size As String) As String

        Select Case image_size
            Case "image_small"
                Return "Documents/ProductImages/not-available-small.jpg"
            Case "image_normal"
                Return "Documents/ProductImages/not-available-normal.jpg"
            Case "image_large"
                Return "Documents/ProductImages/not-available-large.jpg"
            Case "thumbnail_small"
                Return "Documents/ProductImages/not-available-thumbnail-small.jpg"
            Case "thumbnail_normal"
                Return "Documents/ProductImages/not-available-thumbnail-normal.jpg"
            Case Else
                Return "Documents/ProductImages/not-available-thumbnail-large.jpg"
        End Select

    End Function

    <WebMethod()>
    Public Shared Function OrderProduct(products As Object()) As Object
        ' Get the current global_ws
        Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()
        Dim lcOrderMessage As String = ""
        Dim lcErrorMessages As String = ""
        Dim ln_products_added As Integer = 0
        Dim lc_unit_code As String = ""


        For Each product In products
            'Check if product comment module is on or not
            Dim lc_prod_comment As String = ""
            If Not product.ContainsKey("prod_comment") Then
                lc_prod_comment = ""
            Else
                lc_prod_comment = product.item("prod_comment")
            End If

            'Getting the default product unit if any
            Dim lc_default_product_unit = Utilize.Data.API.Webshop.product_manager.get_product_unit_default(product.item("prod_code")).ToString()

            'Setting the unit code
            'For products with Normal and Sales Units
            If String.IsNullOrEmpty(product.item("unit_code")) And String.IsNullOrEmpty(lc_default_product_unit) Then
                lc_unit_code = ""
            End If
            'When we have product units and call comes from product details we assign the "data-unit-code" tag from the javascript call
            If Not String.IsNullOrEmpty(product.item("unit_code")) Then
                lc_unit_code = product.item("unit_code")
            End If
            'When product has product units and the call comes from the product list we assign the default product unit
            If String.IsNullOrEmpty(product.item("unit_code")) And Not String.IsNullOrEmpty(lc_default_product_unit) Then
                lc_unit_code = lc_default_product_unit
            End If

            If Not loGlobalWs.shopping_cart.product_add(product.item("prod_code"), product.item("ord_qty"), "", lc_unit_code, lc_prod_comment, "") Then
                Dim lcErrorMsg = loGlobalWs.shopping_cart.error_message.Replace("[1]", product.item("prod_code")) + "<br/>"
                lcErrorMessages += lcErrorMsg
            Else
                ln_products_added += 1
            End If

            ' Get the default order message
            lcOrderMessage = global_trans.translate_message("message_products_ordered", "Er zijn [1] producten toegevoegd aan uw winkelwagen", loGlobalWs.Language)
            lcOrderMessage = lcOrderMessage.Replace("[1]", product.item("ord_qty").ToString)
        Next

        ' Generate a page for creating the shopping_cart_control_content
        Dim htmlString As String = GetShoppingCartHtml()

        Dim lc_product_count As String = loGlobalWs.shopping_cart.product_count

        ' If there is no decimal then set the product count to an integer
        Dim isInteger As Boolean = CType(lc_product_count, Decimal) = CType(lc_product_count, Integer)
        If isInteger And lc_product_count.Contains(",") Then
            lc_product_count = lc_product_count.Remove(lc_product_count.IndexOf(","))
        End If

        Return New With {.product_count = lc_product_count,
            .orderTotal = Format(loGlobalWs.shopping_cart.order_subtotal_excl_vat, "c"),
            .order_message = lcOrderMessage,
            .shoppingcartHtml = htmlString,
            .errorMessages = lcErrorMessages,
            .totalProductsAdded = ln_products_added}
    End Function

    Private Shared Function GetShoppingCartHtml() As String
        Dim page As New CMSPageTemplate
        Dim form As New HtmlForm
        form.ViewStateMode = ViewStateMode.Disabled

        Dim div_products As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
        div_products.Attributes.Add("class", "shoppingcart-products")

        form.Controls.Add(div_products)

        Dim uc_shopping_cart_control_content As New Utilize.Web.Solutions.Base.base_usercontrol_base
        uc_shopping_cart_control_content = page.LoadUserControl("~/webshop/modules/shoppingcart/uc_shopping_cart_control_content.ascx")

        div_products.Controls.Add(uc_shopping_cart_control_content)

        Dim div_end_anchor As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
        div_end_anchor.Attributes.Add("class", "end-anchor")

        form.Controls.Add(div_end_anchor)

        page.Controls.Add(form)

        Dim htmlString As String = ""
        Using sw As StringWriter = New StringWriter
            HttpContext.Current.Server.Execute(page, sw, False)
            htmlString = sw.ToString()
        End Using

        ' filter away the asp elements from the shopping_cart_control_content html element
        Dim start_location As Integer = htmlString.IndexOf("<ul class=""mCustomScrollbar""")
        Dim string_length As Integer = htmlString.IndexOf("<div class=""end-anchor"">") - start_location
        htmlString = htmlString.Substring(start_location, string_length)

        Return htmlString
    End Function

    <WebMethod()>
    Public Shared Function RemoveFromShoppingcart(ByVal recId As String) As Object
        Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()

        loGlobalWs.shopping_cart.product_delete_by_id(recId)

        Dim lc_product_count As String = loGlobalWs.shopping_cart.product_count.ToString()

        ' If there is no decimal then set the product count to an integer
        If lc_product_count.EndsWith(",0000") Then
            lc_product_count = CType(lc_product_count, Int32).ToString
        End If

        Return New With {.product_count = lc_product_count,
            .orderTotal = Format(loGlobalWs.shopping_cart.order_subtotal_excl_vat, "c"),
            .shoppingcartHtml = GetShoppingCartHtml()
        }
    End Function

    <WebMethod()>
    Public Shared Function GetProductPrice(productCode As String) As Object
        Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()

        If (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And
        Not loGlobalWs.show_prices And loGlobalWs.user_information.user_logged_on Then
            Return New With {.salesAction = False}
        End If

        Dim prodPrice As String = ""
        Dim prodPriceOriginal As String = ""

        Dim ll_sales_action_1 As Boolean = False
        Dim ln_prod_price1 As Decimal = 0
        Dim ln_prod_original_price1 As Decimal = 0
        If loGlobalWs.get_webshop_setting("price_type") = 1 Then
            ln_prod_price1 = loGlobalWs.get_product_price(productCode, "")
            ln_prod_original_price1 = loGlobalWs.get_product_price_default(productCode)
        Else
            ln_prod_price1 = loGlobalWs.get_product_price_incl_vat(productCode, "")
            ln_prod_original_price1 = loGlobalWs.get_product_price_default_incl_vat(productCode)

        End If

        ll_sales_action_1 = ln_prod_original_price1 > ln_prod_price1

        If Not loGlobalWs.price_mode = Webshop.price_mode.Credits Then
            prodPrice = Format(ln_prod_price1, "c")
            prodPriceOriginal = Format(ln_prod_original_price1, "c")
        Else
            prodPrice = loGlobalWs.convert_amount_to_credits(ln_prod_price1)
            prodPriceOriginal = loGlobalWs.convert_amount_to_credits(ln_prod_original_price1)
        End If

        Return New With {
            .salesAction = ll_sales_action_1 And loGlobalWs.check_sales_action(productCode),
            .productPrice = prodPrice,
            .productPriceOriginal = prodPriceOriginal
        }
    End Function
End Class
