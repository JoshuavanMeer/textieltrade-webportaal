﻿Imports System.Web.Services
Partial Class Webshop_WebshopAPI_ProductStockWebmethods
    Inherits System.Web.UI.Page

    <WebMethod()>
    Public Shared Function GetProductStock(productCode As String) As Object

        Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()
        Dim ln_stock As Decimal = Utilize.Web.Solutions.Webshop.ws_procedures.get_stock(productCode, "")

        Return New With {
            .stockTotal = ln_stock
        }

    End Function
End Class
