﻿
Imports System.Web.Services

Partial Class Webshop_WebshopAPI_GenericWebmethods
    Inherits System.Web.UI.Page

    <WebMethod()>
    Public Shared Function GetTranslation(type As String, id As String, defaultText As String, language As String) As Object
        Dim result = ""
        Select Case type
            Case "button"
                result = global_trans.translate_button(id, defaultText, language)
            Case "message"
                result = global_trans.translate_message(id, defaultText, language)
            Case "text"
                result = global_trans.translate_text(id, defaultText, language)
            Case "title"
                result = global_trans.translate_title(id, defaultText, language)
            Case "label"
                result = global_trans.translate_label(id, defaultText, language)
        End Select

        Return New With {.translationText = result}
    End Function

End Class
