﻿
Partial Class Webshop_Ariba_punchout
    Inherits System.Web.UI.Page

    Private Sub Webshop_Ariba_Punchout_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ll_testing As Boolean = True
        Dim url = Request.Url
        Dim ur_query As String = url.Query.Trim("?")
        Dim ur_parameters() As String = ur_query.Split("&")
        Dim ur_tokens() As String
        ur_tokens = ur_parameters(0).Split("=")
        Dim cr_session_id = ur_tokens(0)

        Dim dt_single_timestamp As Date = DateTime.Now()
        Dim login_code = ""
        Dim login_pass = ""

        Dim qsc_ariba_session As New Utilize.Data.QuerySelectClass
        With qsc_ariba_session
            .select_fields = "login_code, timestamp"
            .select_from = "uws_ariba_session"
            .select_where = "session_string = @session_string "
        End With

        qsc_ariba_session.add_parameter("@session_string", cr_session_id)

        Dim dt_ariba_session As System.Data.DataTable = qsc_ariba_session.execute_query()

        If dt_ariba_session.Rows.Count <> 1 Then
            ll_testing = False
        End If


        If dt_ariba_session.Rows.Count = 1 Then
            dt_single_timestamp = dt_ariba_session.Rows(0)("timestamp")
            login_code = Convert.ToString(dt_ariba_session.Rows(0)("login_code"))
        End If

        If Not dt_single_timestamp.AddHours(24) > DateTime.Now() Then
            ll_testing = False
        End If

        If ll_testing Then
            Dim qsc_customers_users As New Utilize.Data.QuerySelectClass
            With qsc_customers_users
                .select_fields = "login_code, login_password"
                .select_from = "uws_customers_users"
                .select_where = "login_code = @login_code "
            End With
            qsc_customers_users.add_parameter("@login_code", login_code)
            Dim dt_customers_users As System.Data.DataTable = qsc_customers_users.execute_query()
            login_pass = Convert.ToString(dt_customers_users.Rows(0)("login_password"))
        End If


        Dim ubo_customer As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_customers_users")


        If ubo_customer.table_locate("uws_customers_users", "login_code = '" + login_code + "'") Then
            Dim lc_password As String = System.Guid.NewGuid.ToString("n")
            ubo_customer.field_set("uws_customers_users.login_password", lc_password)
            ubo_customer.table_update("punchout")
            Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws().user_information.login(Webshop.user_type.Customer, login_code, lc_password)
        End If


        Response.Redirect(Me.ResolveClientUrl("~/home.aspx"))


    End Sub

End Class