﻿
Imports System.Data
Imports System.Globalization
Imports System.Linq
Imports Newtonsoft.Json
Imports Utilize.Ariba.Helpers
Imports Utilize.Ariba.PunchoutResponse
Imports Utilize.Web.Solutions.Webshop

Partial Class punchout_redirect
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Protected punchout_url As String = ""

    Private Sub set_punchout_form()
        punchout_url = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "arb_punchout_url", Me.global_ws.user_information.user_id).ToString

        Dim from_identity As List(Of Credential) = JsonConvert.DeserializeObject(Of List(Of Credential))(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "arb_credential_to", Me.global_ws.user_information.user_id).ToString)
        Dim to_identity As List(Of Credential) = JsonConvert.DeserializeObject(Of List(Of Credential))(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "arb_credential_from", Me.global_ws.user_information.user_id).ToString)

        Dim url = global_ws.get_webshop_setting("Arb_Url")

        Dim buyercoockie As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_code", Me.global_ws.user_information.user_id)
        Dim shopping_cart_table = Me.global_ws.shopping_cart.product_table
        Dim builder = New PunchoutResponseBuilder() _
            .AddFromCredentials(from_identity) _
            .AddToCredentials(to_identity) _
            .Sender(url)


        Me.global_ws.shopping_cart.product_table.ToTable().AsEnumerable.ToList().ForEach(Function(x) AddOrderLine(builder, x))

        Dim obj = builder.CreatePunchoutReponse(buyercoockie)
        Dim content = Ariba.AribaXmlSerializer.Serialize(obj)

        Dim input As New HtmlInputHidden()
        input.ID = "cxml-urlencoded"
        input.Value = content
        ph_punchout.Controls.Add(input)
    End Sub

    Private Function AddOrderLine(builder As PunchoutResponseBuilder, x As DataRow)
        Dim qsc_arb_specs As New Utilize.Data.QuerySelectClass
        With qsc_arb_specs
            .select_fields = "arb_unspc, arb_uom"
            .select_from = "uws_products"
            .select_where = "prod_code = @prod_code "
        End With
        qsc_arb_specs.add_parameter("@prod_code", x.Item("prod_code"))
        Dim dt_ariba_specs As System.Data.DataTable = qsc_arb_specs.execute_query()
        Dim unspc = dt_ariba_specs.Rows(0)("arb_unspc")
        Dim uom = dt_ariba_specs.Rows(0)("arb_uom")
        builder.AddItem(x.Item("ord_qty"), x.Item("prod_code"), x.Item("prod_price"), x.Item("prod_desc"), unspc, uom)
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not IsPostBack Then
            set_punchout_form()
        Else
            'If sending has been succesfull, then empty shoppingcart and show message
            Me.global_ws.shopping_cart.create_shopping_cart()
            Response.Redirect("~/" + me.global_ws.language + "/webshop/paymentprocess/shopping_cart.aspx?punchout=success")
        End If


    End Sub
End Class
