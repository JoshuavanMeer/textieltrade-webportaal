﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="punchout_redirect.aspx.vb" Inherits="punchout_redirect" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Punchout Redirect</title>
</head>
<body>
    <form method="post" id="frmPunchOut" name="frmOci" action="<%= punchout_url %>">
          <utilize:placeholder ID="ph_punchout" runat="server" ></utilize:placeholder>
    </form>
    <script type="text/javascript" language="javascript">
        document.forms['frmPunchOut'].submit();
    </script>
</body>
</html>


