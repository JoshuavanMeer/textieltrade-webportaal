﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reports_product.ascx.vb" Inherits="uc_reports_product" %>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<div class="uc_reports_product">
    <h1><utilize:translatetitle ID="title_report_product" runat="server"></utilize:translatetitle></h1>
    <utilize:translatetext runat="server" ID="text_report_product" Text="Onderstaand vindt u een overzicht van producten en aantallen die u in het verleden bij ons besteld heeft."></utilize:translatetext>

    <utilize:placeholder ID="ph_report_control_panel" runat="server">
        <div class="report_control_panel">
            <utilize:translatebutton ID="button_export_to_excel" Text="Exporteren" runat="server" CssClass="left btn-u btn-u-sea-shop btn-u-lg"  />
            
            <div class="form-horizontal">
                <div class="form-group">
                    <utilize:translatelabel id="label_report_filter_employees" runat="server" Text="Periode" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                    <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_from" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                    </div>

                    <utilize:translatelabel id="label_report_filter_until" runat="server" Text="tot" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                    <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_until" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                    </div>
                        
                    <div class="col-sm-2">
                        <utilize:translatebutton  ID="button_refresh_selection" Text="Vernieuwen" runat="server" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
                    </div>
                </div>
            </div>             
        </div>

     </utilize:placeholder>
     <utilize:placeholder ID="ph_report_result_table" runat="server">
        
            <div class="table-responsive">
            <utilize:repeater runat="server" ID="rpt_products" pager_enabled="true" page_size="20" pagertype="bottom" EnableViewState="false">
        <HeaderTemplate>
            <table class="overview table table-striped">
                <tr class="header">
                    <th class="code"><utilize:translatelabel id="col_product_code" Text="Productcode" runat="server"></utilize:translatelabel></th>
                    <th><utilize:translatelabel id="col_product_description" Text="Omschrijving" runat="server"></utilize:translatelabel></th>
                    <th class="number"><utilize:translatelabel id="col_product_size" text="Maat" runat="server"></utilize:translatelabel></th>
                    <th class="number"><utilize:translatelabel id="col_order_quantity" Text="Aantal" runat="server"></utilize:translatelabel></th>
                    <th class="amount"><utilize:translatelabel id="col_order_amount" Text="Bedrag" runat="server"></utilize:translatelabel></th>
                    
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
                <tr class="line">         
                    <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                    <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%></td>
                    <td class="size"><%# format_date(DataBinder.Eval(Container.DataItem, "size"))%></td>
                    <td class="number"><%# Format(DataBinder.Eval(Container.DataItem, "ord_qty"), "N0")%></td>
                    <td class="amount"><%# Format(DataBinder.Eval(Container.DataItem, "row_amt"), "c")%></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
                </table>
        </FooterTemplate>                                    
        </utilize:repeater>
       </div>
    </utilize:placeholder>
    </div>
