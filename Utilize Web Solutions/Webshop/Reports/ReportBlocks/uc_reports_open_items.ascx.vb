﻿Imports Utilize.Data
Imports System.Globalization

Partial Class uc_reports_open_items
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Voeg de postbacktrigger toe voor de export knop
        Dim loExportTrigger As New System.Web.UI.PostBackTrigger
        loExportTrigger.ControlID = Me.button_export_to_excel.UniqueID

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loExportTrigger)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim dt_employees As System.Data.DataTable = Me.get_data_source()

        rpt_open_items.DataSource = dt_employees
        rpt_open_items.DataBind()

        Me.set_style_sheet("uc_reports_open_items.css", Me)
    End Sub

    Private Function get_data_source() As System.Data.DataTable
        Dim lo_employees_qsc As New QuerySelectClass()

        lo_employees_qsc.select_fields = " uws_openitems.inv_nr, uws_openitems.inv_desc, " & _
                                        " uws_openitems.inv_date, uws_openitems.inv_amt, " & _
                                        " uws_openitems.exp_date "

        lo_employees_qsc.select_from = " uws_openitems "

        ' We willen alleen de facturen tonen die niet (volledig) betaald zijn en binnen de datum range vallen die opgegeven is.
        lo_employees_qsc.select_where += " uws_openitems.inv_amt > uws_openitems.paid_amt "
        lo_employees_qsc.select_where += " and uws_openitems.cust_id = @cust_id "

        lo_employees_qsc.add_parameter("cust_id", global_ws.user_information.user_customer_id)

        'Bepaal de sortering
        lo_employees_qsc.select_order = "uws_openitems.inv_date desc"

        Return lo_employees_qsc.execute_query()

    End Function

    Protected Sub button_export_to_excel_Click(sender As Object, e As EventArgs) Handles button_export_to_excel.Click
        Dim dt_data_table As System.Data.DataTable = Me.get_data_source()

        Dim ExcelXML As New Utilize.FrameWork.ExcelXML
        Dim sbXml As StringBuilder = ExcelXML.export_excel(dt_data_table, "product_history", "Utilize Business Solutions BV")

        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=product_history.xml")
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Write(sbXml)
        HttpContext.Current.Response.Write(vbCrLf)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
    End Sub

    ' XLC-135 Bugfix don't show pdf if history line of open item is not present
    Public Function CheckDocument(ByVal inv_nr As String) As Boolean
        Dim lc_doc_nr As String = Utilize.Data.DataProcedures.GetValue("uws_history", "doc_nr", inv_nr, "doc_nr")

        If Not String.IsNullOrEmpty(lc_doc_nr) Then
            Dim lo_byte As Byte() = get_invoice(lc_doc_nr)
            If Not lo_byte Is Nothing Then
                Return True
            End If
        End If
        Return False
    End Function
    Private Function get_invoice(doc_nr As String) As Byte()
        Dim lb_return As Byte() = Nothing

        Try
            Dim udt_data_table As New Utilize.Data.DataTable
            udt_data_table.table_name = "uws_history"
            udt_data_table.table_init()

            udt_data_table.add_where("and doc_nr = @doc_nr")
            udt_data_table.add_where("and cust_id = @cust_id")

            udt_data_table.add_where_parameter("doc_nr", doc_nr)
            udt_data_table.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            udt_data_table.table_load()

            If udt_data_table.data_row_count > 0 Then
                lb_return = CType(udt_data_table.field_get("ord_document"), Byte())
            End If
        Catch ex As Exception

        End Try

        Return lb_return
    End Function
    ' END XLC-135
End Class

