﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reports_employee_history.ascx.vb" Inherits="uc_reports_employee_history" %>
<div class="uc_reports_employee_history">
    <utilize:placeholder ID="ph_report_overview" runat="server">
        <utilize:placeholder ID="ph_report_control_panel" runat="server">
            <h1><utilize:translatetitle runat="server" ID="title_report_employee"></utilize:translatetitle></h1>
            <utilize:translatetext runat="server" ID="text_report_employee" Text="Onderstaand vindt u een overzicht van medewerkers voor welke in de geselecteerde periode producten zijn besteld."></utilize:translatetext>

            <utilize:translatebutton ID="button_export_to_excel" Text="Exporteren" runat="server" CssClass="left btn-u btn-u-sea-shop btn-u-lg" />

            <div class="form-horizontal">
                <div class="form-group">
                    <utilize:translatelabel runat='server' ID="label_search_user_name" Text="Naam gebruiker" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                        <utilize:textbox runat='server' ID="txt_search_user_name" cssclass="form-control"></utilize:textbox>
                    </div>

                    <utilize:translatelabel runat='server' ID="label_search_user_address" Text="Adres" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                        <utilize:textbox runat='server' ID="txt_search_user_address" cssclass="form-control"></utilize:textbox>
                    </div>

                    <div class="col-sm-2">

                   </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <utilize:translatelabel id="label_report_filter_employees" runat="server" Text="Periode" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                        <div class="col-sm-3">
                        <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_from" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                        </div>

                        <utilize:translatelabel id="label_report_filter_until" runat="server" Text="tot" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                        <div class="col-sm-3">
                        <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_until" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                        </div>
                        
                        <div class="col-sm-2">
                        <utilize:translatebutton  ID="button_refresh_selection" Text="Vernieuwen" runat="server" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
                        </div>
                    </div>
                </div>
             
            </div>

           
        </utilize:placeholder>

        <div class="table-responsive">
                <utilize:repeater runat="server" ID="rpt_report_employee_history" pager_enabled="true" page_size="20" pagertype="bottom" EnableViewState="false">
                    <HeaderTemplate>
                        <table class="overview table table-striped">
                            <tr class="header">
                                <th>
                                    <utilize:translatelabel ID="col_report_name" Text="Naam" runat="server"></utilize:translatelabel></th>
                                <th>
                                    <utilize:translatelabel ID="col_address" text="Adres" runat="server"></utilize:translatelabel></th>
                                <th>
                                    <utilize:translatelabel ID="col_zip_code" Text="Postcode" runat="server"></utilize:translatelabel></th>
                                <th>
                                    <utilize:translatelabel ID="col_city" Text="Plaats" runat="server"></utilize:translatelabel></th>
                                <th></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="line">
                            <td><%# get_formatted_name(DataBinder.Eval(Container.DataItem, "title"), DataBinder.Eval(Container.DataItem, "fst_name"), DataBinder.Eval(Container.DataItem, "infix"), DataBinder.Eval(Container.DataItem, "lst_name"))%></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "address")%></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "zip_code")%></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "city")%></td>
                            <td class="change"><utilize:translatelinkbutton runat="server" ID="link_report_drill_down" Text="Details" CommandName="change" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'></utilize:translatelinkbutton></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </utilize:repeater>
            </div>
    </utilize:placeholder>
    
    <utilize:placeholder ID="ph_report_details" runat="server" Visible="false">
        <h1><utilize:translatetitle runat='server' ID="title_report_employee_details" Text="Overzicht bestelregels"></utilize:translatetitle></h1>
        <div class="table-responsive">
            <utilize:repeater runat="server" ID="rpt_orders_overview">
                <HeaderTemplate>
                        <table class="overview table table-striped">
                            <tr class="header">
                                <th class="number"><utilize:translatelabel runat='server' ID="col_order_number" Text="Ordernummer"></utilize:translatelabel></th>
                                <th class="date"><utilize:translatelabel runat='server' ID="col_order_date" Text="Datum"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th class="size"><utilize:translatelabel runat='server' ID="col_product_size" Text="Maat"></utilize:translatelabel></th>
                                <th class="quantity"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <th class="amount"><utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_invoice_pdf" Text="Kopiefactuur"></utilize:translatelabel></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="line">
                                <td class="number"><%# DataBinder.Eval(Container.DataItem, "doc_nr")%></td>
                                <td class="date"><%# format_date(DataBinder.Eval(Container.DataItem, "ord_date")) %></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "prod_code") %><br /><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                                <td class="size"><%# DataBinder.Eval(Container.DataItem, "size") %></td>
                                <td class="quantity"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%></td>
                                <td class="amount"><%# Format(DataBinder.Eval(Container.DataItem, "row_amt"), "c")%></td>
                                <td><utilize:imagebutton style="background:none; height:21px; width:62px; margin: 0;" OnClientClick='<%# "window.open(""" + Me.ResolveCustomUrl("~/") + "Webshop/Account/OpenDocument.aspx?DocType=invoice&DocNr=" + DataBinder.Eval(Container.DataItem, "doc_nr") + """);return false;"%>' ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' runat="server" ID="button_copy_invoice" /></td>
                            </tr>
                        </ItemTemplate>
                    <FooterTemplate>
                            </table>
                    </FooterTemplate>   
            </utilize:repeater>
        </div>
        <br style="clear: both;" />
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </utilize:placeholder>
</div>
