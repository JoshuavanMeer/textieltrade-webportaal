﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reports_open_items.ascx.vb" Inherits="uc_reports_open_items" %>
<div class="uc_reports_open_items">
<h1><utilize:translatetitle ID="title_report_open_items" runat="server"></utilize:translatetitle></h1>
<utilize:translatetext runat="server" ID="text_report_open_items" Text="Onderstaand vindt u een overzicht van facturen die op dit moment volgens onze administratie open staan."></utilize:translatetext>

    <utilize:placeholder ID="ph_report_control_panel" runat="server">
        <div class="report_control_panel">
            <utilize:translatebutton ID="button_export_to_excel" Text="Exporteren" runat="server" CssClass="left btn-u btn-u-sea-shop btn-u-lg" />
            <div class="right_buttons">
            </div>
        </div>
    </utilize:placeholder>
     <utilize:placeholder ID="ph_report_result_table" runat="server">
       
          <div class="table-responsive">
            <utilize:repeater runat="server" ID="rpt_open_items" pager_enabled="true" page_size="20" pagertype="bottom" EnableViewState="false">
        <HeaderTemplate>
            <table class="overview table table-striped">
                <tr class="header">
                    <th class="number"><utilize:translatelabel id="col_invoice_number" text="Factuurnummer" runat="server"></utilize:translatelabel></th>
                    <th><utilize:translatelabel id="col_invoice_description" text="Omschrijving" runat="server"></utilize:translatelabel></th>
                    <th><utilize:translatelabel id="col_invoice_date" text="Datum" runat="server"></utilize:translatelabel></th>
                    <th class="date"><utilize:translatelabel id="col_invoice_expiration_date" text="Vervaldatum" runat="server"></utilize:translatelabel></th>
                    <th><utilize:translatelabel runat='server' ID="col_invoice_pdf" Text="Kopiefactuur"></utilize:translatelabel></th>
                    <th class="amount"><utilize:translatelabel runat='server' ID="col_invoice_amount" Text="Factuurbedrag"></utilize:translatelabel></th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
                <tr class="line">         
                    <td  class="number"><%# DataBinder.Eval(Container.DataItem, "inv_nr")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "inv_desc")%></td>
                    <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "inv_date"), "d")%></td>
                    <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "exp_date"), "d")%></td>
                    <td><utilize:imagebutton style="background:none; height:21px; width:62px; margin: 0;" OnClientClick='<%# "window.open(""" + Me.ResolveCustomUrl("~/") + "Webshop/Account/OpenDocument.aspx?DocType=invoice&DocNr=" + DataBinder.Eval(Container.DataItem, "inv_nr") + """);return false;"%>' ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' runat="server" ID="button_copy_invoice" Visible='<%# CheckDocument(DataBinder.Eval(Container.DataItem, "inv_nr")) %>'/></td>
                    <td class="amount"><%# Format(DataBinder.Eval(Container.DataItem, "inv_amt"), "c") %></td>

                </tr>
        </ItemTemplate>
        <FooterTemplate>
                </table>
        </FooterTemplate>                                    
        </utilize:repeater>
       </div>
    </utilize:placeholder>

    </div>
