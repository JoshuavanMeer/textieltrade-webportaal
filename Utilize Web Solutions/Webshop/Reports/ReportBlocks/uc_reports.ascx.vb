﻿
Partial Class uc_reports
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_visible As Boolean = False
        'Toon de control alleen als er is ingelogd en de gebruiker een superuser of manager is
        ll_visible = Me.global_ws.user_information.user_logged_on

        If ll_visible Then
            ll_visible = global_ws.purchase_combination.user_is_manager
        End If

        Me.Visible = ll_visible
        Me.set_style_sheet("uc_reports.css", Me)

        'Zet de links naar de rapporten
        link_report_history_employee.NavigateUrl = ResolveCustomUrl("~/" + Me.global_ws.Language + "/webshop/reports/EmployeeHistory.aspx")
        link_report_history_order.NavigateUrl = ResolveCustomUrl("~/" + Me.global_ws.Language + "/webshop/reports/OrderHistory.aspx")
        link_report_history_product.NavigateUrl = ResolveCustomUrl("~/" + Me.global_ws.Language + "/webshop/reports/ProductHistory.aspx")
        link_report_open_items.NavigateUrl = ResolveCustomUrl("~/" + Me.global_ws.Language + "/webshop/reports/OpenItems.aspx")

        Select Case True
            Case Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_px_globe")
                link_report_history_employee.Visible = False
            Case Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_eol")
                link_report_history_employee.Visible = False
            Case Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_unit4")
                link_report_history_employee.Visible = False
            Case Else
                Exit Select
        End Select
    End Sub
End Class
