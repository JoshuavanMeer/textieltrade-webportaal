﻿Imports System.Globalization
Imports System.Web.UI.WebControls
Imports Utilize.Data

Partial Class uc_reports_order_history
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _date_from As String = ""
    Private _date_till As String = ""

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Voeg de postbacktrigger toe voor de export knop
        Dim loExportTrigger As New System.Web.UI.PostBackTrigger
        loExportTrigger.ControlID = Me.button_export_to_excel.UniqueID

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loExportTrigger)

        If Not Me.IsPostBack() Then
            ph_report_details.Visible = False
            ph_report_overview.Visible = True
        End If

        Me.set_style_sheet("uc_reports_order_history.css", Me)

        If Not Me.IsPostBack() Then
            Me.txt_date_from.Text = "01-01-" + (System.DateTime.Now.Year - 1).ToString()
            Me.txt_date_until.Text = System.DateTime.Parse("01-01-" + (System.DateTime.Now.Year + 1).ToString()).AddDays(-1).ToShortDateString()

            _date_from = Me.txt_date_from.Text
            _date_till = Me.txt_date_until.Text
        Else
            _date_from = Request.Form(Me.txt_date_from.UniqueID)
            _date_till = Request.Form(Me.txt_date_until.UniqueID)
        End If

        Dim dt_orders As System.Data.DataTable = get_data_source()
        rpt_report_orders.EnableViewState = False
        rpt_report_orders.DataSource = dt_orders
        rpt_report_orders.DataBind()
    End Sub

    Protected Function format_date(ByVal lc_date As String) As String
        Return lc_date.Split(" ")(0)
    End Function

    Private Function get_data_source() As System.Data.DataTable
        Dim lo_datasource As New Utilize.Data.QuerySelectClass
        lo_datasource.select_fields = "crec_id, doc_nr, ord_desc, ord_date, ord_tot"
        lo_datasource.select_from = "uws_history"
        lo_datasource.select_where = "cust_id = @cust_id "

        lo_datasource.select_where += "AND uws_history.ord_date BETWEEN @from_date AND @until_date "
        lo_datasource.select_order = "doc_nr desc"

        lo_datasource.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        lo_datasource.add_parameter("from_date", get_from_date())
        lo_datasource.add_parameter("until_date", get_until_date())

        Return lo_datasource.execute_query()
    End Function
    Private Function get_data_details_source(ByVal crec_id As String) As System.Data.DataTable
        Dim qsc_lines As New Utilize.Data.QuerySelectClass
        qsc_lines.select_fields = "*"
        qsc_lines.select_from = "uws_history_lines"
        qsc_lines.select_where = "hdr_id = @hdr_id"
        qsc_lines.add_parameter("hdr_id", crec_id)

        Return qsc_lines.execute_query()
    End Function

    Private Function get_export_source() As System.Data.DataTable
        Dim lo_datasource As New Utilize.Data.QuerySelectClass
        lo_datasource.select_fields = "doc_nr, ord_desc, ord_date, ord_tot, prod_code, prod_desc, size, row_amt"
        lo_datasource.select_from = "uws_history"
        lo_datasource.add_join("left outer join", "uws_history_lines", "uws_history.crec_id = uws_history_lines.hdr_id")

        lo_datasource.select_where = "cust_id = @cust_id "

        lo_datasource.select_where += "AND uws_history.ord_date BETWEEN @date_from AND @date_till "
        lo_datasource.select_order = "doc_nr desc"

        lo_datasource.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        lo_datasource.add_parameter("date_from", get_from_date())
        lo_datasource.add_parameter("date_till", get_until_date())

        Return lo_datasource.execute_query()
    End Function
    Public Function get_formatted_name(ByVal lc_title As String, ByVal lc_first_name As String, ByVal lc_infix As String, ByVal lc_last_name As String) As String
        Dim lc_full_name As String = ""

        If lc_title <> "" Then
            lc_full_name &= lc_title & " "
        End If

        lc_full_name &= lc_first_name

        If lc_infix <> "" Then
            lc_full_name &= " " & lc_infix
        End If

        lc_full_name &= " " & lc_last_name
        Return lc_full_name
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'van' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_from_date() As DateTime
        Dim lo_date_time_from As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_from

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_from
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_from)

        If ll_resume Then
            Return lo_date_time_from
        Else
            'Maak het tekstveld leeg indien de waarde ongeldig is
            txt_date_from.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 1800
        Return New DateTime(1900, 1, 1)
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'tot' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_until_date() As DateTime
        Dim lo_date_time_until As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_till

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_until
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_until)

        If ll_resume Then
            Return lo_date_time_until
        Else
            'Maak het tekstveld leef indien er een ongeldige waarde is ingevoerd
            txt_date_until.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 9000
        Return New DateTime(9000, 1, 1)
    End Function

    Protected Sub rpt_report_orders_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_report_orders.ItemCommand
        If e.CommandName = "change" Then
            Dim lc_rec_id As String = e.CommandArgument

            Me.rpt_order_details.DataSource = Me.get_data_details_source(lc_rec_id)
            Me.rpt_order_details.DataBind()

            Dim udt_data_Table As New Utilize.Data.DataTable
            udt_data_Table.table_name = "uws_history"
            udt_data_Table.table_init()
            udt_data_Table.add_where("and crec_id = @crec_id")
            udt_data_Table.add_where_parameter("crec_id", lc_rec_id)
            udt_data_Table.table_load()

            If udt_data_Table.data_row_count > 0 Then
                Me.lbl_order_date.Text = Format(udt_data_Table.field_get("ord_date"), "d")
                Me.lbl_order_number.Text = udt_data_Table.field_get("doc_nr")
                Me.lbl_order_description.Text = udt_data_Table.field_get("ord_desc")

                Me.lt_order_subtotal_excl_vat.Text = Format(udt_data_Table.field_get("ord_tot") - udt_data_Table.field_get("ship_cost") - udt_data_Table.field_get("ord_cost"), "c")
                Me.lt_order_shipping_costs_excl_vat.Text = Format(udt_data_Table.field_get("ship_cost"), "c")
                Me.lt_order_costs_excl_vat.Text = Format(udt_data_Table.field_get("ord_cost"), "c")

                Me.lt_order_vat_total.Text = Format(udt_data_Table.field_get("vat_tot"), "c")
                Me.lt_order_total_incl_vat.Text = Format(udt_data_Table.field_get("ord_tot") + udt_data_Table.field_get("vat_tot"), "c")

                Me.ph_report_overview.Visible = False
                Me.ph_report_details.Visible = True
            End If
        End If
    End Sub

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.ph_report_details.Visible = False
        Me.ph_report_overview.Visible = True
    End Sub
    Protected Sub button_back_to_overview_Click(sender As Object, e As EventArgs) Handles button_close_details.Click
        Me.ph_report_overview.Visible = True
        Me.ph_report_details.Visible = False
    End Sub

    Protected Sub button_export_to_excel_Click(sender As Object, e As EventArgs) Handles button_export_to_excel.Click
        Dim dt_data_table As System.Data.DataTable = Me.get_export_source()

        Dim ExcelXML As New Utilize.FrameWork.ExcelXML
        Dim sbXml As StringBuilder = ExcelXML.export_excel(dt_data_table, "product_history", "Utilize Business Solutions BV")

        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=product_history.xml")
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Write(sbXml.ToString())
        HttpContext.Current.Response.Write(vbCrLf)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
    End Sub

    Private Sub rpt_order_details_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_order_details.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Try
                ' Decimalen
                Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

                '<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%>
                Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")
                If lt_ord_qty IsNot Nothing Then
                    lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class
