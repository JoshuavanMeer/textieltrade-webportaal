﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reports.ascx.vb" Inherits="uc_reports" %>
<div class="uc_reports">
    <h2><utilize:translatetitle runat="server" ID="title_reports" Text="Rapportages"></utilize:translatetitle></h2>
    <div class="reports_wrapper">
        <utilize:translatelink runat="server" ID="link_report_history_employee" Text="Historie per medewerker" visible="false"></utilize:translatelink>
        <utilize:translatelink runat="server" ID="link_report_history_cost_center" Text="Historie per kostenplaats" visible="false"></utilize:translatelink>
        <utilize:translatelink runat="server" ID="link_report_history_order" Text="Historie per order"></utilize:translatelink>
        <utilize:translatelink runat="server" ID="link_report_history_product" Text="Historie per product"></utilize:translatelink>
        <utilize:translatelink runat="server" ID="link_report_open_items" Text="Openstaande posten"></utilize:translatelink>
    </div>
</div>