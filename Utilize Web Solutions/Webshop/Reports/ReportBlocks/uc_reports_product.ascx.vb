﻿Imports Utilize.Data
Imports System.Globalization

Partial Class uc_reports_product
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _date_from As String = ""
    Private _date_till As String = ""

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Voeg de postbacktrigger toe voor de export knop
        Dim loExportTrigger As New System.Web.UI.PostBackTrigger
        loExportTrigger.ControlID = Me.button_export_to_excel.UniqueID

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loExportTrigger)

        Me.set_style_sheet("uc_reports_product.css", Me)

        If Not Me.IsPostBack() Then
            Me.txt_date_from.Text = "01-01-" + (System.DateTime.Now.Year - 1).ToString()
            Me.txt_date_until.Text = System.DateTime.Parse("01-01-" + (System.DateTime.Now.Year + 1).ToString()).AddDays(-1).ToShortDateString()

            _date_from = Me.txt_date_from.Text
            _date_till = Me.txt_date_until.Text
        Else
            _date_from = Request.Form(Me.txt_date_from.UniqueID)
            _date_till = Request.Form(Me.txt_date_until.UniqueID)
        End If

        rpt_products.DataSource = Me.get_data_source()
        rpt_products.DataBind()
    End Sub

    Private Function get_data_source() As System.Data.DataTable
        Dim lo_datasource As New QuerySelectClass()
        lo_datasource.select_fields = " uws_history_lines.prod_code, uws_history_lines.prod_desc, uws_history_lines.size, SUM(uws_history_lines.ord_qty) AS ord_qty, SUM(uws_history_lines.row_amt) AS row_amt "
        lo_datasource.select_from = " uws_history_lines "
        lo_datasource.add_join(" LEFT OUTER JOIN", "uws_history", "uws_history_lines.hdr_id = uws_history.crec_id ")

        'Hier doen we een subselect om de orders te selecteren
        lo_datasource.select_where += " uws_history.cust_id = @cust_id "
        lo_datasource.select_where += " AND uws_history.ord_date BETWEEN @from_date AND @until_date "
        lo_datasource.select_where += " GROUP BY uws_history_lines.prod_code, uws_history_lines.prod_desc, uws_history_lines.size"
        '
        'Vul de SQL parameters in.
        lo_datasource.add_parameter("from_date", get_from_date())
        lo_datasource.add_parameter("until_date", get_until_date())
        lo_datasource.add_parameter("cust_id", global_ws.user_information.user_customer_id)

        'Bepaal de sortering
        lo_datasource.select_order = "uws_history_lines.prod_code "

        Return lo_datasource.execute_query()
    End Function

    ''' <summary>
    ''' Verwijdert de tijd van de datum
    ''' </summary>
    ''' <param name="lc_date"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function format_date(ByVal lc_date As String) As String
        Return lc_date.Split(" ")(0)
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'van' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_from_date() As DateTime
        Dim lo_date_time_from As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_from

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_from
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_from)

        If ll_resume Then
            Return lo_date_time_from
        Else
            'Maak het tekstveld leeg indien de waarde ongeldig is
            txt_date_from.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 1800
        Return New DateTime(1900, 1, 1)
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'tot' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_until_date() As DateTime
        Dim lo_date_time_until As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_till

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_until
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_until)

        If ll_resume Then
            Return lo_date_time_until
        Else
            'Maak het tekstveld leef indien er een ongeldige waarde is ingevoerd
            txt_date_until.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 9000
        Return New DateTime(9000, 1, 1)
    End Function

    Protected Sub button_export_to_excel_Click(sender As Object, e As EventArgs) Handles button_export_to_excel.Click
        Dim dt_data_table As System.Data.DataTable = Me.get_data_source()

        Dim ExcelXML As New Utilize.FrameWork.ExcelXML
        Dim sbXml As StringBuilder = ExcelXML.export_excel(dt_data_table, "product_history", "Utilize Business Solutions BV")

        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=product_history.xml")
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Write(sbXml)
        HttpContext.Current.Response.Write(vbCrLf)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
    End Sub
End Class
