﻿﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reports_order_history.ascx.vb" Inherits="uc_reports_order_history" %>
<div class="uc_reports_order_history">
    <utilize:placeholder ID="ph_report_overview" runat="server">
        
        
        <h1><utilize:translatetitle ID="title_report_order" runat="server" text="Historie per bestelling"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_report_order" Text="Onderstaand vindt u een overzicht van bestellingen die in het verleden zijn geplaatst. Kies voor details om de inhoud van de bestelling te bekijken"></utilize:translatetext>

        <utilize:placeholder ID="ph_report_control_panel" runat="server">

            <div class="report_control_panel">
                <utilize:panel ID="pnl_control_panel" runat="server" DefaultButton="button_refresh_selection">
                    <utilize:translatebutton ID="button_export_to_excel" Text="Exporteren" runat="server" CssClass="left btn-u btn-u-sea-shop btn-u-lg"/>

                    <div class="form-horizontal">
                    <div class="form-group">
                    <utilize:translatelabel id="label_report_filter_employees" runat="server" Text="Periode" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                    <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_from" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                    </div>

                    <utilize:translatelabel id="label_report_filter_until" runat="server" Text="tot" CssClass="col-sm-2 control-label"></utilize:translatelabel>
                    <div class="col-sm-3">
                    <utilize:textbox cssclass="form-control" runat="server" ID="txt_date_until" watermark="  -  -    " MaxLength="10"></utilize:textbox>
                    </div>
                        
                    <div class="col-sm-2">
                    <utilize:translatebutton  ID="button_refresh_selection" Text="Vernieuwen" runat="server" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
                    </div>
                </div>
            </div>

                
                </utilize:panel>
            </div>
        </utilize:placeholder>
       
        <div class="table-responsive">
       <utilize:repeater runat="server" ID="rpt_report_orders" pager_enabled="true" page_size="20" pagertype="bottom" EnableViewState="false">
            <HeaderTemplate>
                <table class="overview table table-striped">
                    <tr class="header">
                        <th class="number"><utilize:translatelabel ID="col_order_number" Text="Ordernummer" runat="server"></utilize:translatelabel></th>
                        <th><utilize:translatelabel ID="col_order_description" Text="Omschrijving" runat="server"></utilize:translatelabel></th>
                        <th class="date"><utilize:translatelabel ID="col_order_date" Text="Orderdatum" runat="server"></utilize:translatelabel></th>
                        <th class="amount"><utilize:translatelabel ID="col_order_amount" Text="Bedrag" runat="server"></utilize:translatelabel></th>
                        <th class="quantity"><utilize:translatelabel runat='server' ID="col_invoice_pdf" Text="Kopiefactuur"></utilize:translatelabel></th>
                        <th></th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">         
                        <td  class="number"><%# DataBinder.Eval(Container.DataItem, "doc_nr")%></td>
                        <td><%# DataBinder.Eval(Container.DataItem, "ord_desc")%></td>
                        <td class="date"><%# format_date(DataBinder.Eval(Container.DataItem, "ord_date"))%></td>
                        <td class="amount"><%# Format(DataBinder.Eval(Container.DataItem, "ord_tot"), "c")%></td>
                        <td class="quantity"><utilize:imagebutton style="background:none; height:21px; width:62px; margin: 0;" OnClientClick='<%# "window.open(""" + Me.ResolveCustomUrl("~/") + "Webshop/Account/OpenDocument.aspx?DocType=invoice&DocNr=" + DataBinder.Eval(Container.DataItem, "doc_nr") + """);return false;"%>' ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' runat="server" ID="button_copy_invoice" /></td>
                        <td class="change"><utilize:translatelinkbutton runat="server" ID="link_report_drill_down" Text="Details" CommandName="change" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>'></utilize:translatelinkbutton></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
            </FooterTemplate>                                    
        </utilize:repeater>
       </div>
    </utilize:placeholder>
    <utilize:placeholder ID="ph_report_details" runat="server">
        <h1><utilize:translatetitle runat='server' ID="title_report_order_details" Text="Overzicht van historische bestelling"></utilize:translatetitle></h1>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <b><utilize:translatelabel runat='server' ID="label_order_number" Text="Ordernummer"></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat='server' ID="lbl_order_number"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b><utilize:translatelabel runat='server' ID="label_order_date" Text="Datum"></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat='server' ID="lbl_order_date"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <b><utilize:translatelabel runat='server' ID="label_order_description" Text="Omschrijving"></utilize:translatelabel></b>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat='server' ID="lbl_order_description"></utilize:label>
                    </div>
                </div>
            </div>
        </div>           

            <div class="table-responsive">
                <utilize:repeater runat="server" ID="rpt_order_details">
                    <HeaderTemplate>
                    <table class="lines table table-striped">
                            <tr class="header">
                                <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <th class="quantity"><utilize:translatelabel runat='server' ID="col_product_size" Text="Maat"></utilize:translatelabel></th>
                                <th class="quantity"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <th class="amount"><utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="line">
                                <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code") %></td>
                                <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                                <td class="quantity"><%# DataBinder.Eval(Container.DataItem, "size") %></td>
                                <td class="quantity"><utilize:literal runat="server" ID="lt_ord_qty"></utilize:literal></td>
                                <td  class="amount"><%# Format(DataBinder.Eval(Container.DataItem, "row_amt"), "c")%></td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </table>
                    </FooterTemplate>                                    
                </utilize:repeater>
                </div>
        <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
            <div class="row">
                <div class="col-md-6 price-overview">
                    <div class="row">
                        <div class="col-md-6">
                            <b><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></b>
                        </div>
                        <div class="col-md-6">
                            <utilize:literal runat="server" ID="lt_order_subtotal_excl_vat"></utilize:literal>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <b><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></b>
                        </div>
                        <div class="col-md-6">
                            <utilize:literal runat="server" ID="lt_order_costs_excl_vat"></utilize:literal>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <b><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></b>
                        </div>
                        <div class="col-md-6">
                            <utilize:literal runat="server" ID="lt_order_shipping_costs_excl_vat"></utilize:literal>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <b><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></b>
                        </div>
                        <div class="col-md-6">
                            <utilize:literal runat="server" ID="lt_order_vat_total"></utilize:literal>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <b><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></b>
                        </div>
                        <div class="col-md-6">
                            <utilize:literal runat="server" ID="lt_order_total_incl_vat"></utilize:literal>
                        </div>
                    </div>
                </div>
            </div>            
        </utilize:placeholder>
        <br style="clear: both;" />
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </utilize:placeholder>
</div>
