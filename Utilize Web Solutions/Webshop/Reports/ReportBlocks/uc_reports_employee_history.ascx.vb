﻿Imports Utilize.Data
Imports System.Data
Imports System.Globalization

Partial Class uc_reports_employee_history
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _date_from As String = ""
    Private _date_till As String = ""

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Voeg de postbacktrigger toe voor de export knop
        Dim loExportTrigger As New System.Web.UI.PostBackTrigger
        With loExportTrigger
            .ControlID = Me.button_export_to_excel.UniqueID
        End With

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loExportTrigger)

        Me.set_style_sheet("uc_reports_employee_history.css", Me)

        If Not Me.IsPostBack() Then
            Me.txt_date_from.Text = "01-01-" + (System.DateTime.Now.Year - 1).ToString()
            Me.txt_date_until.Text = System.DateTime.Parse("01-01-" + (System.DateTime.Now.Year + 1).ToString()).AddDays(-1).ToShortDateString()

            _date_from = Me.txt_date_from.Text
            _date_till = Me.txt_date_until.Text
        Else
            _date_from = Request.Form(Me.txt_date_from.UniqueID)
            _date_till = Request.Form(Me.txt_date_until.UniqueID)
        End If

        Dim dt_employees As System.Data.DataTable = get_data_source()

        rpt_report_employee_history.EnableViewState = False
        rpt_report_employee_history.DataSource = dt_employees
        rpt_report_employee_history.DataBind()
    End Sub

    Protected Function format_date(ByVal lc_date As String) As String
        Return lc_date.Split(" ")(0)
    End Function

    Private Function get_data_source() As System.Data.DataTable
        ' Hier doen we een subselect om de medewerkers te filteren op basis van door hen geplaatste orders die zijn geplaatst
        ' gedurende de periode die is ingesteld in de user interface
        Dim lo_employees_qsc As New QuerySelectClass
        With lo_employees_qsc
            .select_fields = "uws_customers_users.title, uws_customers_users.fst_name, uws_customers_users.infix, uws_customers_users.lst_name, uws_customers_users.rec_id, uws_customers_users.address, uws_customers_users.zip_code, uws_customers_users.city"
            .select_from = "uws_customers_users"
            .select_where = "uws_customers_users.hdr_id = @cust_id AND uws_customers_users.rec_id IN (SELECT uws_history_lines.user_id FROM uws_history_lines INNER JOIN uws_history ON uws_history.crec_id = uws_history_lines.hdr_id WHERE uws_history.ord_date BETWEEN @from_date AND @until_date)"
            .select_order = "lst_name asc"
        End With
        lo_employees_qsc.add_parameter("from_date", get_from_date())
        lo_employees_qsc.add_parameter("until_date", get_until_date())
        lo_employees_qsc.add_parameter("cust_id", global_ws.user_information.user_customer_id)

        If Not Request.Form(Me.txt_search_user_address.UniqueID) = "" Then
            lo_employees_qsc.select_where += " AND uws_customers_users.address like '%' + @address + '%'"
            lo_employees_qsc.add_parameter("address", Request.Form(Me.txt_search_user_address.UniqueID))
        End If

        If Not Request.Form(Me.txt_search_user_name.UniqueID) = "" Then
            lo_employees_qsc.select_where += " AND (uws_customers_users.fst_name like '%' + @fst_name + '%'"
            lo_employees_qsc.select_where += " OR uws_customers_users.lst_name like '%' + @lst_name + '%')"
            lo_employees_qsc.add_parameter("fst_name", Request.Form(Me.txt_search_user_name.UniqueID))
            lo_employees_qsc.add_parameter("lst_name", Request.Form(Me.txt_search_user_name.UniqueID))
        End If

        'Bepaal de sortering
        lo_employees_qsc.select_order = "lst_name"

        Return lo_employees_qsc.execute_query()
    End Function
    Private Function get_details_source(ByVal user_id As String) As System.Data.DataTable
        ' Hier doen we een subselect om de medewerkers te filteren op basis van door hen geplaatste orders die zijn geplaatst
        ' gedurende de periode die is ingesteld in de user interface
        Dim lo_employees_qsc As New QuerySelectClass
        With lo_employees_qsc
            .select_fields = "uws_history.doc_nr, uws_history.ord_date, uws_history_lines.prod_code, uws_history_lines.prod_desc, uws_history_lines.size, uws_history_lines.ord_qty, uws_history_lines.row_amt"
            .select_from = "uws_history_lines"
            .select_where = "uws_history_lines.user_id = @user_id and uws_history.ord_date BETWEEN @from_date AND @until_date"
        End With
        lo_employees_qsc.add_join("inner join", "uws_history", "uws_history_lines.hdr_id = uws_history.crec_id")

        'Vul de SQL parameters in.
        lo_employees_qsc.add_parameter("from_date", get_from_date())
        lo_employees_qsc.add_parameter("until_date", get_until_date())
        lo_employees_qsc.add_parameter("user_id", user_id)

        'Bepaal de sortering
        lo_employees_qsc.select_order = "doc_nr"

        Return lo_employees_qsc.execute_query()
    End Function
    Private Function get_export_source() As System.Data.DataTable
        ' Hier doen we een subselect om de medewerkers te filteren op basis van door hen geplaatste orders die zijn geplaatst
        ' gedurende de periode die is ingesteld in de user interface
        Dim lo_employees_qsc As New QuerySelectClass
        With lo_employees_qsc
            .select_fields = "uws_history.doc_nr, uws_history.ord_date, uws_history_lines.prod_code, uws_history_lines.prod_desc, uws_history_lines.size, uws_history_lines.ord_qty, uws_history_lines.row_amt, uws_customers_users.fst_name + ' ' + uws_customers_users.infix + ' ' + uws_customers_users.lst_name as cust_name, uws_customers_users.address, uws_customers_users.zip_code, uws_customers_users.city"
            .select_from = "uws_history_lines"
            .select_where = "uws_history.cust_id = @cust_id and uws_history.ord_date BETWEEN @from_date AND @until_date"
            .select_order = "lst_name asc"
        End With
        lo_employees_qsc.add_join("inner join", "uws_history", "uws_history_lines.hdr_id = uws_history.crec_id")
        lo_employees_qsc.add_join("left outer join", "uws_customers_users", "uws_history_lines.user_id = uws_customers_users.rec_id")

        'Vul de SQL parameters in.
        lo_employees_qsc.add_parameter("from_date", get_from_date())
        lo_employees_qsc.add_parameter("until_date", get_until_date())
        lo_employees_qsc.add_parameter("cust_id", global_ws.user_information.user_customer_id)

        'Bepaal de sortering
        lo_employees_qsc.select_order = "doc_nr desc, prod_code"

        Return lo_employees_qsc.execute_query()
    End Function
    Public Function get_formatted_name(ByVal lc_title As String, ByVal lc_first_name As String, ByVal lc_infix As String, ByVal lc_last_name As String) As String
        Dim lc_full_name As String = lc_last_name

        If lc_infix <> "" Then
            lc_full_name &= ", " & lc_infix
        End If

        lc_full_name &= ", " + lc_first_name

        Return lc_full_name
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'van' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_from_date() As DateTime
        Dim lo_date_time_from As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_from

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_from
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_from)

        If ll_resume Then
            Return lo_date_time_from
        Else
            'Maak het tekstveld leeg indien de waarde ongeldig is
            txt_date_from.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 1800
        Return New DateTime(1900, 1, 1)
    End Function

    ''' <summary>
    ''' Deze functie haalt de 'tot' datum op en converteert deze naar een <code>DateTime</code> object
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function get_until_date() As DateTime
        Dim lo_date_time_until As DateTime = DateTime.MinValue

        Dim lc_date As String = _date_till

        'Probeer de datum in te lezen en sla het resultaat op in lo_date_time_until
        Dim ll_resume As Boolean = DateTime.TryParseExact(lc_date, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, lo_date_time_until)

        If ll_resume Then
            Return lo_date_time_until
        Else
            'Maak het tekstveld leef indien er een ongeldige waarde is ingevoerd
            txt_date_until.Text = ""
        End If

        'Er is geen (geldige) datum ingevuld, zet het jaartal op 9000
        Return New DateTime(9000, 1, 1)
    End Function

    Protected Sub button_export_to_excel_Click(sender As Object, e As EventArgs) Handles button_export_to_excel.Click
        Dim dt_data_table As System.Data.DataTable = Me.get_export_source()

        Dim ExcelXML As New Utilize.FrameWork.ExcelXML
        Dim sbXml As StringBuilder = ExcelXML.export_excel(dt_data_table, "employee_history", "Utilize Business Solutions BV")

        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=employee_history.xml")
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Write(sbXml.ToString())
        HttpContext.Current.Response.Write(vbCrLf)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
    End Sub

    Protected Sub rpt_report_employee_history_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_report_employee_history.ItemCommand
        ' Als een gebruiker gewijzigd wordt
        Select Case e.CommandName
            Case "change"
                Dim lc_user_id As String = e.CommandArgument

                Me.rpt_orders_overview.DataSource = Me.get_details_source(lc_user_id)
                Me.rpt_orders_overview.DataBind()

                Me.ph_report_overview.Visible = False
                Me.ph_report_details.Visible = True
            Case Else
                Exit Select
        End Select
    End Sub

    Protected Sub button_close_details_Click(sender As Object, e As EventArgs) Handles button_close_details.Click
        Me.ph_report_details.Visible = False
        Me.ph_report_overview.Visible = True
    End Sub
End Class
