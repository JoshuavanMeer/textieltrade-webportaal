﻿Partial Class OpenItems
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        Dim lc_page_title_code As String = "title_report_open_items"
        Dim lc_page_title As String = "Openstaande posten"

        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title(lc_page_title_code, lc_page_title, Me.global_ws.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim lc_location_of_user_control As String = "ReportBlocks/uc_reports_open_items.ascx"

        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
        Dim uc_change_password As Webshop.ws_user_control = LoadUserControl(lc_location_of_user_control)
        ph_zone.Controls.Add(uc_change_password)
    End Sub

End Class
