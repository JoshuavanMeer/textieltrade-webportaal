﻿Imports System.Data
Imports System.Globalization
Imports System.IO

#Region " Public Classes"
Namespace Customer4CustomerService
    Public Class Keyval
        Dim _key As String
        Public Property key() As String
            Get
                Return _key
            End Get
            Set(ByVal value As String)
                _key = value
            End Set
        End Property
        Dim _value As Object
        Public Property value() As Object
            Get
                Return _value
            End Get
            Set(ByVal value As Object)
                _value = value
            End Set
        End Property
    End Class

    Public Class Webshop
        Private _logincode As String
        Public Property login_code() As String
            Get
                Return _logincode
            End Get
            Set(ByVal value As String)
                _logincode = value
            End Set
        End Property

        Private _password As String
        Public Property login_password() As String
            Get
                Return _password
            End Get
            Set(ByVal value As String)
                _password = value
            End Set
        End Property

        Private _framework_version As Double
        Public Property framework_version() As Double
            Get
                Return _framework_version
            End Get
            Set(ByVal value As Double)
                _framework_version = value
            End Set
        End Property
    End Class

    Public Class Customer
        Private _rec_id As String
        Public Property rec_id() As String
            Get
                Return _rec_id
            End Get
            Set(ByVal value As String)
                _rec_id = value
            End Set
        End Property
        Private _address As String
        Public Property address() As String
            Get
                Return _address
            End Get
            Set(ByVal value As String)
                _address = value
            End Set
        End Property
        Private _blocked As String
        Public Property blocked() As String
            Get
                Return _blocked
            End Get
            Set(ByVal value As String)
                _blocked = value
            End Set
        End Property
        Private _bus_name As String
        Public Property bus_name() As String
            Get
                Return _bus_name
            End Get
            Set(ByVal value As String)
                _bus_name = value
            End Set
        End Property
        Private _city As String
        Public Property city() As String
            Get
                Return _city
            End Get
            Set(ByVal value As String)
                _city = value
            End Set
        End Property
        Private _cnt_code As String
        Public Property cnt_code() As String
            Get
                Return _cnt_code
            End Get
            Set(ByVal value As String)
                _cnt_code = value
            End Set
        End Property
        Private _del_address As String
        Public Property del_address() As String
            Get
                Return _del_address
            End Get
            Set(ByVal value As String)
                _del_address = value
            End Set
        End Property
        Private _del_address_custom As String = "False"
        Public Property del_address_custom() As String
            Get
                Return _del_address_custom
            End Get
            Set(ByVal value As String)
                _del_address_custom = value
            End Set
        End Property
        Private _account_available As String
        Public Property account_available() As String
            Get
                Return _account_available
            End Get
            Set(ByVal value As String)
                _account_available = value
            End Set
        End Property
        Private _del_bus_name As String
        Public Property del_bus_name() As String
            Get
                Return _del_bus_name
            End Get
            Set(ByVal value As String)
                _del_bus_name = value
            End Set
        End Property
        Private _del_city As String
        Public Property del_city() As String
            Get
                Return _del_city
            End Get
            Set(ByVal value As String)
                _del_city = value
            End Set
        End Property
        Private _del_cnt_code As String
        Public Property del_cnt_code() As String
            Get
                Return _del_cnt_code
            End Get
            Set(ByVal value As String)
                _del_cnt_code = value
            End Set
        End Property
        Private _del_fst_name As String
        Public Property del_fst_name() As String
            Get
                Return _del_fst_name
            End Get
            Set(ByVal value As String)
                _del_fst_name = value
            End Set
        End Property
        Private _del_house_nr As String
        Public Property del_house_nr() As String
            Get
                Return _del_house_nr
            End Get
            Set(ByVal value As String)
                _del_house_nr = value
            End Set
        End Property
        Private _del_infix As String
        Public Property del_infix() As String
            Get
                Return _del_infix
            End Get
            Set(ByVal value As String)
                _del_infix = value
            End Set
        End Property
        Private _del_lst_name As String
        Public Property del_lst_name() As String
            Get
                Return _del_lst_name
            End Get
            Set(ByVal value As String)
                _del_lst_name = value
            End Set
        End Property
        Private _del_region As String
        Public Property del_region() As String
            Get
                Return _del_region
            End Get
            Set(ByVal value As String)
                _del_region = value
            End Set
        End Property
        Private _del_title As String
        Public Property del_title() As String
            Get
                Return _del_title
            End Get
            Set(ByVal value As String)
                _del_title = value
            End Set
        End Property
        Private _del_zip_code As String
        Public Property del_zip_code() As String
            Get
                Return _del_zip_code
            End Get
            Set(ByVal value As String)
                _del_zip_code = value
            End Set
        End Property
        Private _email_address As String
        Public Property email_address() As String
            Get
                Return _email_address
            End Get
            Set(ByVal value As String)
                _email_address = value
            End Set
        End Property
        Private _fax_nr As String
        Public Property fax_nr() As String
            Get
                Return _fax_nr
            End Get
            Set(ByVal value As String)
                _fax_nr = value
            End Set
        End Property
        Private _fst_name As String
        Public Property fst_name() As String
            Get
                Return _fst_name
            End Get
            Set(ByVal value As String)
                _fst_name = value
            End Set
        End Property
        Private _house_nr As String
        Public Property house_nr() As String
            Get
                Return _house_nr
            End Get
            Set(ByVal value As String)
                _house_nr = value
            End Set
        End Property
        Private _infix As String
        Public Property infix() As String
            Get
                Return _infix
            End Get
            Set(ByVal value As String)
                _infix = value
            End Set
        End Property
        Private _initials As String
        Public Property initials() As String
            Get
                Return _initials
            End Get
            Set(ByVal value As String)
                _initials = value
            End Set
        End Property
        Private _lst_name As String
        Public Property lst_name() As String
            Get
                Return _lst_name
            End Get
            Set(ByVal value As String)
                _lst_name = value
            End Set
        End Property
        Private _phone_nr As String
        Public Property phone_nr() As String
            Get
                Return _phone_nr
            End Get
            Set(ByVal value As String)
                _phone_nr = value
            End Set
        End Property
        Private _pl_code As String
        Public Property pl_code() As String
            Get
                Return _pl_code
            End Get
            Set(ByVal value As String)
                _pl_code = value
            End Set
        End Property
        Private _region As String
        Public Property region() As String
            Get
                Return _region
            End Get
            Set(ByVal value As String)
                _region = value
            End Set
        End Property
        Private _sc_code As String
        Public Property sc_code() As String
            Get
                Return _sc_code
            End Get
            Set(ByVal value As String)
                _sc_code = value
            End Set
        End Property
        Private _title As String
        Public Property title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property
        Private _vat_code As String
        Public Property vat_code() As String
            Get
                Return _vat_code
            End Get
            Set(ByVal value As String)
                _vat_code = value
            End Set
        End Property
        Private _vat_nr As String
        Public Property vat_nr() As String
            Get
                Return _vat_nr
            End Get
            Set(ByVal value As String)
                _vat_nr = value
            End Set
        End Property
        Private _zip_code As String
        Public Property zip_code() As String
            Get
                Return _zip_code
            End Get
            Set(ByVal value As String)
                _zip_code = value
            End Set
        End Property
    End Class

    Public Class Device
        Private _rec_id As String
        Public Property rec_id() As String
            Get
                Return _rec_id
            End Get
            Set(ByVal value As String)
                _rec_id = value
            End Set
        End Property

        Private _device_token As String
        Public Property device_token() As String
            Get
                Return _device_token
            End Get
            Set(ByVal value As String)
                _device_token = value
            End Set
        End Property

        Private _device_id As String
        Public Property device_id() As String
            Get
                Return _device_id
            End Get
            Set(ByVal value As String)
                _device_id = value
            End Set
        End Property

        Private _user_id As String
        Public Property user_id() As String
            Get
                Return _user_id
            End Get
            Set(ByVal value As String)
                _user_id = value
            End Set
        End Property

        Private _last_login As Date
        Public Property last_login() As Date
            Get
                Return _last_login
            End Get
            Set(ByVal value As Date)
                _last_login = value
            End Set
        End Property
    End Class

    Public Class jsonorder
        Private _cust_id As String
        Public Property cust_id() As String
            Get
                Return _cust_id
            End Get
            Set(ByVal value As String)
                _cust_id = value
            End Set
        End Property
        Private _ord_desc As String
        Public Property ord_desc() As String
            Get
                Return _ord_desc
            End Get
            Set(ByVal value As String)
                _ord_desc = value
            End Set
        End Property
        Private _order_comments As String
        Public Property order_comments() As String
            Get
                Return _order_comments
            End Get
            Set(ByVal value As String)
                _order_comments = value
            End Set
        End Property
        Private _del_date As Date
        Public Property del_date() As Date
            Get
                Return _del_date
            End Get
            Set(ByVal value As Date)
                _del_date = value
            End Set
        End Property

        Private _inv_bus_name As String
        Public Property inv_bus_name() As String
            Get
                Return _inv_bus_name
            End Get
            Set(ByVal value As String)
                _inv_bus_name = value
            End Set
        End Property
        Private _inv_fst_name As String
        Public Property inv_fst_name() As String
            Get
                Return _inv_fst_name
            End Get
            Set(ByVal value As String)
                _inv_fst_name = value
            End Set
        End Property
        Private _inv_infix As String
        Public Property inv_infix() As String
            Get
                Return _inv_infix
            End Get
            Set(ByVal value As String)
                _inv_infix = value
            End Set
        End Property
        Private _inv_lst_name As String
        Public Property inv_lst_name() As String
            Get
                Return _inv_lst_name
            End Get
            Set(ByVal value As String)
                _inv_lst_name = value
            End Set
        End Property
        Private _inv_address As String
        Public Property inv_address() As String
            Get
                Return _inv_address
            End Get
            Set(ByVal value As String)
                _inv_address = value
            End Set
        End Property
        Private _inv_zip_code As String
        Public Property inv_zip_code() As String
            Get
                Return _inv_zip_code
            End Get
            Set(ByVal value As String)
                _inv_zip_code = value
            End Set
        End Property
        Private _inv_city As String
        Public Property inv_city() As String
            Get
                Return _inv_city
            End Get
            Set(ByVal value As String)
                _inv_city = value
            End Set
        End Property
        Private _inv_cnt_code As String
        Public Property inv_cnt_code() As String
            Get
                Return _inv_cnt_code
            End Get
            Set(ByVal value As String)
                _inv_cnt_code = value
            End Set
        End Property
        Private _phone_nr As String
        Public Property phone_nr() As String
            Get
                Return _phone_nr
            End Get
            Set(ByVal value As String)
                _phone_nr = value
            End Set
        End Property
        Private _email_address As String
        Public Property email_address() As String
            Get
                Return _email_address
            End Get
            Set(ByVal value As String)
                _email_address = value
            End Set
        End Property
        Private _del_address_custom As String = "False"
        Public Property del_address_custom() As String
            Get
                Return _del_address_custom
            End Get
            Set(ByVal value As String)
                _del_address_custom = value
            End Set
        End Property
        Private _del_bus_name As String
        Public Property del_bus_name() As String
            Get
                Return _del_bus_name
            End Get
            Set(ByVal value As String)
                _del_bus_name = value
            End Set
        End Property
        Private _del_fst_name As String
        Public Property del_fst_name() As String
            Get
                Return _del_fst_name
            End Get
            Set(ByVal value As String)
                _del_fst_name = value
            End Set
        End Property
        Private _del_infix As String
        Public Property del_infix() As String
            Get
                Return _del_infix
            End Get
            Set(ByVal value As String)
                _del_infix = value
            End Set
        End Property
        Private _del_lst_name As String
        Public Property del_lst_name() As String
            Get
                Return _del_lst_name
            End Get
            Set(ByVal value As String)
                _del_lst_name = value
            End Set
        End Property
        Private _del_address As String
        Public Property del_address() As String
            Get
                Return _del_address
            End Get
            Set(ByVal value As String)
                _del_address = value
            End Set
        End Property
        Private _del_house_nr As String
        Public Property del_house_nr() As String
            Get
                Return _del_house_nr
            End Get
            Set(ByVal value As String)
                _del_house_nr = value
            End Set
        End Property
        Private _del_zip_code As String
        Public Property del_zip_code() As String
            Get
                Return _del_zip_code
            End Get
            Set(ByVal value As String)
                _del_zip_code = value
            End Set
        End Property
        Private _del_city As String
        Public Property del_city() As String
            Get
                Return _del_city
            End Get
            Set(ByVal value As String)
                _del_city = value
            End Set
        End Property
        Private _del_cnt_code As String
        Public Property del_cnt_code() As String
            Get
                Return _del_cnt_code
            End Get
            Set(ByVal value As String)
                _del_cnt_code = value
            End Set
        End Property
        Private _signature As String = ""
        Public Property signature() As String
            Get
                Return _signature
            End Get
            Set(ByVal value As String)
                _signature = value
            End Set
        End Property

        Private _orderlines() As jsonorderline
        Public Property orderlines() As jsonorderline()
            Get
                Return _orderlines
            End Get
            Set(ByVal value As jsonorderline())
                _orderlines = value
            End Set
        End Property

        Private _jsonorderline As jsonorderline
        Public Property jsonorderlines() As jsonorderline
            Get
                Return _jsonorderline
            End Get
            Set(ByVal value As jsonorderline)
                _jsonorderline = value
            End Set
        End Property
        Private _order_subtotal_excl_vat_format As String
        Public Property order_subtotal_excl_vat_format() As String
            Get
                Return _order_subtotal_excl_vat_format
            End Get
            Set(ByVal value As String)
                _order_subtotal_excl_vat_format = value
            End Set
        End Property
        Private _order_costs_excl_vat_format As String
        Public Property order_costs_excl_vat_format() As String
            Get
                Return _order_costs_excl_vat_format
            End Get
            Set(ByVal value As String)
                _order_costs_excl_vat_format = value
            End Set
        End Property
        Private _shipping_costs_excl_vat_format As String
        Public Property shipping_costs_excl_vat_format() As String
            Get
                Return _shipping_costs_excl_vat_format
            End Get
            Set(ByVal value As String)
                _shipping_costs_excl_vat_format = value
            End Set
        End Property
        Private _order_vat_total_format As String
        Public Property order_vat_total_format() As String
            Get
                Return _order_vat_total_format
            End Get
            Set(ByVal value As String)
                _order_vat_total_format = value
            End Set
        End Property
        Private _order_total_incl_vat_format As String
        Public Property order_total_incl_vat_format() As String
            Get
                Return _order_total_incl_vat_format
            End Get
            Set(ByVal value As String)
                _order_total_incl_vat_format = value
            End Set
        End Property
    End Class

    Public Class jsonorderline
        Private _prod_code As String
        Public Property prod_code() As String
            Get
                Return _prod_code
            End Get
            Set(ByVal value As String)
                _prod_code = value
            End Set
        End Property
        Private _prod_desc As String
        Public Property prod_desc() As String
            Get
                Return _prod_desc
            End Get
            Set(ByVal value As String)
                _prod_desc = value
            End Set
        End Property
        Private _ord_qty As Integer
        Public Property ord_qty() As Integer
            Get
                Return _ord_qty
            End Get
            Set(ByVal value As Integer)
                _ord_qty = value
            End Set
        End Property
        Private _unit_qty As Decimal
        Public Property unit_qty() As Decimal
            Get
                Return _unit_qty
            End Get
            Set(ByVal value As Decimal)
                _unit_qty = value
            End Set
        End Property
        Private _prod_price_format As String
        Public Property prod_price_format() As String
            Get
                Return _prod_price_format
            End Get
            Set(ByVal value As String)
                _prod_price_format = value
            End Set
        End Property
        Private _row_disc_format As String
        Public Property row_disc_format() As String
            Get
                Return _row_disc_format
            End Get
            Set(ByVal value As String)
                _row_disc_format = value
            End Set
        End Property
        Private _row_amt_format As String
        Public Property row_amt_format() As String
            Get
                Return _row_amt_format
            End Get
            Set(ByVal value As String)
                _row_amt_format = value
            End Set
        End Property
        Private _is_opt As String
        Public Property is_opt() As String
            Get
                Return _is_opt
            End Get
            Set(ByVal value As String)
                _is_opt = value
            End Set
        End Property
        Private _size As String
        Public Property size() As String
            Get
                Return _size
            End Get
            Set(ByVal value As String)
                _size = value
            End Set
        End Property
    End Class
End Namespace

#End Region

Partial Class c4c_service
    Inherits System.Web.UI.Page

#Region "Private Enums"
    Private Enum group_types
        none = 0
        group_val1 = 1
        group_val2 = 2
        group = 3
    End Enum
#End Region

#Region " Private variabelen"

    Private page_method As String = ""
    Private callback As String = ""
    Private allow_origin As Boolean = False
    Private global_ws As New Utilize.Web.Solutions.Webshop.global_ws

#End Region

#Region " Page event"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Hier gaan we de access allow origin waarde ophalen
        Me.SetAccessAllowOrigin()

        callback = IIf(String.IsNullOrEmpty(Request.Params("callback")), "", Request.Params("callback"))
        page_method = IIf(String.IsNullOrEmpty(Request.Params("method")), "", Request.Params("method"))
        page_method = page_method.ToLower()
        'Me.write_log("C4C: page_methode: " + page_method)
        Select Case page_method
            Case "background"
                Me.WriteJson(Me.GetJsonString(Me.getBackground()))
            Case "banners"
                Me.WriteJson(Me.GetJsonString(Me.GetBanners()))
            Case "calculateshoppingcart"
                Me.WriteJson(Me.GetShoppingCart())
            Case "categories"
                Me.WriteJson(Me.GetJsonString(Me.GetAllCategories()))
            Case "css"
                Me.WriteJson(Me.CustomColors())
            Case "currency"
                Me.WriteJson(Me.GetJsonString(Me.GetCurrency()))
            Case "device"
                Me.WriteJson(Me.DeviceToken())
            Case "devicetoken"
                Me.WriteJson(Me.GetJsonString(Me.getDeviceTokens()))
            Case "languages"
                Me.WriteJson(Me.GetJsonString(Me.GetLanguages()))
            Case "indicator"
                Me.WriteJson(Me.GetJsonString(Me.GetIndicators()))
            Case "login"
                Me.WriteJson(Me.Login())
                'Case "logout"
                'Me.WriteJson(Me.Logout())
            Case "placeorder"
                Me.WriteJson(Me.PlaceOrder())
            Case "prices"
                Me.WriteJson(Me.GetJsonString(Me.getEuroPrices()))
            Case "products"
                Me.WriteJson(Me.GetJsonString(Me.GetAllProducts()))
            Case "productspecifics"
                Me.WriteJson(Me.GetJsonString(Me.GetAllSpecifics()))
            Case "shipmethod"
                Me.WriteJson(Me.GetJsonString(Me.GetShipMethods()))
            Case "translations"
                Me.WriteJson(Me.GetJsonString(Me.getTranslation()))
            Case "vat"
                Me.WriteJson(Me.GetJsonString(Me.GetVat()))
            Case Else
                Exit Select
        End Select
    End Sub

#End Region

#Region " Functies voor de communicatie met de APP"

    Private Sub SetAccessAllowOrigin()
        Dim udt_data_table As New Utilize.Data.DataTable
        With udt_data_table
            .table_name = "uws_c4c"
        End With
        udt_data_table.table_init()
        udt_data_table.table_load()

        Me.allow_origin = udt_data_table.field_get("ac_allow_origin")
    End Sub

    Private Sub WriteJson(ByVal lc_json_string As String, Optional ByVal contentType As String = "")
        ' Toevoegen headers
        If Me.allow_origin Then
            Me.Response.AddHeader("Access-Control-Allow-Origin", "*")
        End If

        HttpContext.Current.Response.AddHeader("Access-Control-Request-Method", "POST,GET")
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "X-Requested-With")

        If callback = "" Then
            HttpContext.Current.Response.ContentType = "text/json"
        Else
            HttpContext.Current.Response.ContentType = "text/javascript"

            ' als er geen callback wordt meegegeven moet er ook geen functie worden aangeroepen
            lc_json_string = callback + "(" + lc_json_string + ");"
        End If

        If Not String.IsNullOrEmpty(contentType) Then
            HttpContext.Current.Response.ContentType = contentType
        End If

        HttpContext.Current.Response.Write(lc_json_string)

        ' Sends the response buffer
        HttpContext.Current.Response.Flush()

        ' Prevents any other content from being sent to the browser
        HttpContext.Current.Response.SuppressContent = True

        ' Directs the thread to finish, bypassing additional processing
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub

    Private Function GetJsonString(ByVal dt_data_table As DataTable) As String
        ' Maak een dictionary aan
        Dim dict As New Dictionary(Of String, Object)

        ' Maak een array aan
        Dim arr(dt_data_table.Rows.Count - 1) As Object

        ' Loop door de array heen
        For i As Integer = 0 To dt_data_table.Rows.Count - 1
            Dim dictionary_row As New Dictionary(Of String, Object)

            For a As Integer = 0 To dt_data_table.Columns.Count - 1
                dictionary_row.Add(dt_data_table.Columns(a).ToString(), dt_data_table.Rows(i).Item(a))
            Next

            arr(i) = dictionary_row
        Next

        ' Voeg de array toe aan de dictionary
        dict.Add(dt_data_table.TableName, arr)

        Dim json As New System.Web.Script.Serialization.JavaScriptSerializer
        With json
            .MaxJsonLength = 999999999
        End With

        ' Serialiseer de json
        Dim lc_json_string As String = json.Serialize(dict)

        Return lc_json_string
    End Function

    Private Function GenerateDataTable(ByVal table_values() As Customer4CustomerService.Keyval) As DataTable
        Dim dt_export As New DataTable
        With dt_export
            .TableName = "data"
        End With

        For Each _obj As Customer4CustomerService.Keyval In table_values ' kolommen toevoegen
            Dim dt_column As New DataColumn
            With dt_column
                .ColumnName = _obj.key
            End With
            dt_export.Columns.Add(dt_column)
        Next

        Dim dt_row = dt_export.NewRow
        For Each _obj As Customer4CustomerService.Keyval In table_values 'waardes toevoegen
            dt_row.Item(_obj.key) = _obj.value
        Next

        dt_export.Rows.Add(dt_row)

        Return dt_export
    End Function

#End Region

    Private Function replaceImagesSrcWithUrl(ByVal lc_html As String) As String
        Dim lc_return_value As String = ""

        ' Hier wordt de url opgehaald van de webshop
        Dim lc_webshop_url As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)

        ' Vervang het relatieve pad van de afbeeldingen
        'lc_return_value = lc_html.Replace("src=""/Documents/", "src=""" + lc_webshop_url + "/Documents/")
        lc_return_value = lc_html.Replace("src=""/", "src=""" + lc_webshop_url + "/")

        Return lc_return_value
    End Function

    ''' <summary>
    ''' Resolve url when url may contain an external url or internal Utilize url
    ''' </summary>
    ''' <param name="relativeUrl">The URL associated with the System.Web.UI.Control.TemplateSourceDirectory property or external url</param>
    ''' <returns>The converted URL.</returns>
    Public Function ResolveCustomUrl(relativeUrl As String) As String
        'Check emptyness
        If String.IsNullOrEmpty(relativeUrl) Then
            Return ""
        End If

        'Check if started with external
        If relativeUrl.StartsWith("~/http") Then
            Return relativeUrl.Replace("~/http", "http")
        End If
        If relativeUrl.StartsWith("/http") Then
            Return relativeUrl.Replace("/http", "http")
        End If
        If relativeUrl.StartsWith("http") Then
            Return relativeUrl
        End If

        'Return original resolve url result of internal url
        Return Me.ResolveCustomUrl(relativeUrl)
    End Function

#Region " Functies voor het ophalen van het assortiment uit de webshop"
    Private Function GetBanners() As DataTable
        Dim dt_data_table_banners As DataTable = Nothing
        Dim dt_data_table_newest_ten As DataTable = Nothing
        Dim qsc_banners As New Utilize.Data.QuerySelectClass
        Dim count As New Int32
        Dim max As New Int32
        Dim selected_row As New Int32

        qsc_banners.select_fields = "uws_c4c_banners.ad_image, uws_c4c_banners.ad_type, uws_c4c_banners.ad_prod_code, uws_c4c_banners.ad_cat_code, uws_c4c_banners.rec_id"
        qsc_banners.select_from = "uws_c4c_banners"

        dt_data_table_banners = qsc_banners.execute_query()
        dt_data_table_banners.TableName = "bannerData"

        dt_data_table_newest_ten = dt_data_table_banners.Clone()
        count = 1
        max = 0
        selected_row = 0

        'gets the 10 newest rows in the banner data table
        For Each row As DataRow In dt_data_table_banners.Rows
            If count <= 10 Then
                max = dt_data_table_banners.Rows.Count
                selected_row = max - count
                dt_data_table_newest_ten.ImportRow(dt_data_table_banners.Rows.Item(selected_row))
            End If
            count = count + 1
        Next

        For Each row As DataRow In dt_data_table_newest_ten.Rows
            Dim lcImageUrl As String = row.Item("ad_image")
            If Not String.IsNullOrEmpty(lcImageUrl) Then
                row.Item("ad_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/" + lcImageUrl
            Else
                row.Item("ad_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/Documents/ProductImages/not-available-large.jpg"
            End If
        Next

        Return dt_data_table_newest_ten
    End Function

    Private Function GetAllProducts() As DataTable
        Dim lc_language As String = ""
        Dim lc_currency As String = ""
        Dim lc_hdr_id As String = ""
        Dim lc_user_id As String = ""
        Dim ll_testing As Boolean = True
        Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim lc_error_message As String = ""

        ' Ophalen en decrypten versleutelde login gegevens
        If ll_testing Then
            Dim lc_credentials_encrypted As String = Request.Params("login_credentials")
            Dim lc_credentials As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

            'Gegevens in een klasse zetten
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            shop = serializer.Deserialize(Of Customer4CustomerService.Webshop)(lc_credentials)
        End If

        ' Probeer in te loggen
        If ll_testing Then
            ll_testing = Me.global_ws.user_information.login(0, shop.login_code, shop.login_password)
            lc_user_id = Utilize.Data.DataProcedures.GetValue("uws_customers", "rec_id", global_ws.user_information.user_customer_id)

            If Not ll_testing Then
                lc_error_message = Me.global_ws.user_information.error_message
            End If
        End If

        If ll_testing Then
            lc_hdr_id = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", lc_user_id)

            lc_currency = Utilize.Data.DataProcedures.GetValue("uws_customers", "cur_code", lc_hdr_id)
            lc_language = Utilize.Data.DataProcedures.GetValue("uws_customers", "cnt_code", lc_hdr_id)
        End If

        If ll_testing Then
            If String.IsNullOrEmpty(lc_language) Then
                lc_language = "nl"
            End If
        End If

        ' Haal eerst de categorieen op
        Dim dt_data_table_cat As DataTable = Nothing
        If ll_testing Then
            Dim qsc_select_cat As New Utilize.Data.QuerySelectClass
            With qsc_select_cat
                .select_fields = "cat_code"
                .select_from = "uws_categories"
                .select_order = "row_ord"
                .select_where = "cat_show = 1"
            End With

            ' shop_code is empty for the main shop
            ' shop_code is present for a focus shop
            ' category_available true means all categories must be show (both main and focus shop)
            ' category_available false means only categories of the focus shop is shown
            Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
            Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
            If ll_category_avail Then
                qsc_select_cat.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
            Else
                qsc_select_cat.select_where += " and uws_categories.shop_code = @shop_code"
            End If
            qsc_select_cat.add_parameter("shop_code", lc_shop_code)

            ' Laad de gegevens in de datatable
            dt_data_table_cat = qsc_select_cat.execute_query()

            ll_testing = dt_data_table_cat IsNot Nothing

            If Not ll_testing Then
                lc_error_message = "Fout bij het ophalen van de categorieen"
            End If
        End If

        Dim dt_products As DataTable = Nothing
        If ll_testing Then
            ' Haal hier de producten op
            Dim qsc_select_products As New Utilize.Data.QuerySelectClass

            ' Als er geen categorieen zijn, geef dan de producten uit uws_products terug
            If dt_data_table_cat.Rows.Count = 0 Then
                qsc_select_products.select_fields = "prod_code as rec_id"
            End If

            ' Als er wel categorieen zijn, geef dan de producten uit uws_categories_prod terug
            If dt_data_table_cat.Rows.Count > 0 Then
                qsc_select_products.select_fields = "uws_categories_prod.rec_id"
                qsc_select_products.add_join("left outer join", "uws_categories_prod", "uws_products.prod_code = uws_categories_prod.prod_code")
            End If

            ' Dit is de gegevens die opgehaald moeten worden
            qsc_select_products.select_fields += ", uws_products.prod_code, uws_products_tran.prod_desc, uws_products_tran.prod_desc2, uws_products.sale_unit, uws_products.prod_price, uws_products.prod_price1, uws_products.prod_price2, uws_products.prod_stock, uws_products.vat_code"
            qsc_select_products.select_fields += ", uws_products.ship_days, uws_products_tran.prod_comm, uws_products.color_code, uws_products.sizebar_code, uws_products.prod_ship"
            qsc_select_products.select_fields += ", uws_products.image_normal as prod_image"
            qsc_select_products.select_fields += ", uws_products.bar_code"
            qsc_select_products.select_fields += ", uws_products.shp_grp, uws_products.grp_desc1, uws_products.grp_val1, uws_products.grp_desc2, uws_products.grp_val2, uws_products.prod_prio, 0 as show_grouped, 1 as main_product"
            qsc_select_products.select_fields += ", uws_products.change_date as prod_change_date"
            qsc_select_products.select_fields += ", uws_categories_prod.change_date as cat_change_date, uws_categories_prod.cat_code as prod_cat"
            qsc_select_products.select_fields += ", uws_categories.cat_parent as prod_cat_parent"

            qsc_select_products.select_from = "uws_products"
            qsc_select_products.select_where = "uws_products.prod_show = 1 and uws_products.prod_code in (select prod_code from uws_categories_prod) and lng_code = @lng_code"

            ' Als de module merk uitsluiting per klant aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brand_excl") = True Then
                qsc_select_products.select_where += " and uws_products.brand_code not in (select brand_code from uws_customers_brands where cust_id = @cust_id)"
            End If

            ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
                Dim lc_ass_code As String = ""

                Dim qsc_ass_code As New Utilize.Data.QuerySelectClass
                With qsc_ass_code
                    .select_fields = "uws_customers.ass_code"
                    .select_from = "uws_customers"
                    .select_where = "uws_customers.rec_id = @user_id"
                End With
                qsc_ass_code.add_parameter("user_id", lc_user_id)

                Dim dt_data_table_ass As System.Data.DataTable = qsc_ass_code.execute_query()
                lc_ass_code = dt_data_table_ass.Rows(0).Item(0)

                Dim qsc_cust_ass_incl As New Utilize.Data.QuerySelectClass
                With qsc_cust_ass_incl
                    .select_fields = "*"
                    .select_from = "uws_cust_ass_cat"
                End With
                Dim dt_data_table_cust_ass_incl As System.Data.DataTable = qsc_cust_ass_incl.execute_query()

                Dim qsc_cust_ass_excl As New Utilize.Data.QuerySelectClass
                With qsc_cust_ass_excl
                    .select_fields = "*"
                    .select_from = "uws_cust_ass_excl"
                End With
                Dim dt_data_table_cust_ass_excl As System.Data.DataTable = qsc_cust_ass_excl.execute_query()

                If Not String.IsNullOrEmpty(lc_ass_code) Then
                    ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True And dt_data_table_cust_ass_incl.Rows.Count > 0 Then
                        qsc_select_products.select_where += " and uws_products.prod_code in (select prod_code from uws_categories_prod where uws_categories_prod.cat_code in (select cat_code from uws_categories where uws_categories.cat_parent in  (select uws_cust_ass_cat.cat_code from uws_cust_ass_cat where uws_cust_ass_cat.ass_code = @ass_code_incl)))"
                        qsc_select_products.add_parameter("ass_code_incl", lc_ass_code)
                    End If

                    ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True And dt_data_table_cust_ass_excl.Rows.Count > 0 Then
                        qsc_select_products.select_where += " and uws_products.prod_code not in (select prod_code from uws_categories_prod where uws_categories_prod.cat_code in (select cat_code from uws_categories where uws_categories.cat_parent in (select uws_cust_ass_excl.cat_code from uws_cust_ass_excl where uws_cust_ass_excl.ass_code = @ass_code_excl)))"
                        qsc_select_products.add_parameter("ass_code_excl", lc_ass_code)
                    End If
                End If
            End If

            ' Als de module landspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cnt_ass") = True Then
                Dim lc_ass_code As String = Utilize.Data.DataProcedures.GetValue("uws_countries", "ass_code", Me.global_ws.user_information.ubo_customer.field_get("cnt_code"))

                If Not Me.global_ws.user_information.ubo_customer.field_get("ass_code") = "" Then
                    qsc_select_products.select_where += " and uws_products.prod_code not in (select prod_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
                    qsc_select_products.select_where += " and uws_products.prod_group not in (select grp_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
                End If
            End If

            qsc_select_products.select_order = "uws_products.prod_prio DESC, uws_products.prod_code"
            qsc_select_products.add_join("inner join", "uws_products_tran", "uws_products_tran.prod_code = uws_products.prod_code")
            qsc_select_products.add_join("inner join", "uws_categories", "uws_categories.cat_code = uws_categories_prod.cat_code")

            qsc_select_products.add_parameter("lng_code", lc_language)
            qsc_select_products.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            ' Laad de gegevens in de datatable
            Try
                dt_products = qsc_select_products.execute_query()
                dt_products.TableName = "data"
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try
        End If

        ' Hier wordt door de producten heen gelopen en bepaalde extra informatie opgehaald
        If ll_testing Then
            ' Loop de producten
            For Each row As DataRow In dt_products.Rows
                ' Wijzig de url voor de afbeelding
                Dim lcImageUrl As String = row.Item("prod_image")
                If Not String.IsNullOrEmpty(lcImageUrl) Then
                    row.Item("prod_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/" + lcImageUrl
                Else
                    row.Item("prod_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/Documents/ProductImages/not-available-large.jpg"
                End If

                ' Voeg hoofdcategorie toe
                Dim lcCatParent As String = row.Item("prod_cat_parent")
                If String.IsNullOrEmpty(lcCatParent) Then
                    row.Item("prod_cat_parent") = row.Item("prod_cat")
                End If
                'Else If lcCatParent has parent Then
                '   row.Item("prod_cat_parent") = row.Item("prod_cat")
                'End If

                ' Hier wordt de klantspecifieke prijs opgehaald
                row.Item("prod_price") = Me.global_ws.get_product_price(row.Item("prod_code"), "")
            Next
        End If

        If Not ll_testing Then
            'Me.write_log("Customer4Customer: Producten - " + dt_products.Rows.Count.ToString())
            'Else
            Me.write_log("Customer4Customer: Fout bij het ophalen van de producten", lc_error_message)
        End If

        If dt_products Is Nothing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}, _
            New Customer4CustomerService.Keyval With {.key = "error_occured", .value = ll_testing}}

            dt_products = Me.GenerateDataTable(table_values)
        End If
        Return dt_products
    End Function

    Private Function GetAllCategories() As DataTable
        Dim lc_hdr_id As String = ""
        Dim lc_user_id As String = ""
        Dim ll_testing As Boolean = True
        Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim lc_error_message As String = ""

        If ll_testing Then
            Dim lc_credentials_encrypted As String = Request.Params("login_credentials")
            Dim lc_credentials As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

            'Gegevens in een klasse zetten
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            shop = serializer.Deserialize(Of Customer4CustomerService.Webshop)(lc_credentials)
        End If

        ' Probeer in te loggen
        If ll_testing Then
            ll_testing = Me.global_ws.user_information.login(0, shop.login_code, shop.login_password)
            lc_user_id = Utilize.Data.DataProcedures.GetValue("uws_customers", "rec_id", global_ws.user_information.user_customer_id)

            If Not ll_testing Then
                lc_error_message = Me.global_ws.user_information.error_message
            End If
        End If

        If ll_testing Then
            lc_hdr_id = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", lc_user_id)
        End If

        'Select alle hoofdcategorien die producten hebben of die subcategorien met producten hebben
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        With qsc_categories
            .select_fields = "uws_categories.cat_parent, uws_categories.cat_code as cat_code, uws_categories.cat_desc"
            .select_from = "uws_categories"
            .select_order = "row_ord"
            .select_where = "(uws_categories.cat_parent = @cat_parent and uws_categories.cat_show = 1 and uws_categories.cat_code in (select cat_code from uws_categories_prod))"
        End With

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        qsc_categories.select_where += " or (uws_categories.cat_code in (select cat_parent from uws_categories where uws_categories.cat_code in (select cat_code from uws_categories_prod)))"

        'qsc_categories.add_parameter("lng_code", "nl")
        qsc_categories.add_parameter("cat_parent", "")

        ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
            Dim lc_ass_code As String = ""

            Dim qsc_ass_code As New Utilize.Data.QuerySelectClass
            With qsc_ass_code
                .select_fields = "uws_customers.ass_code"
                .select_from = "uws_customers"
                .select_where = "uws_customers.rec_id = @user_id"
            End With
            qsc_ass_code.add_parameter("user_id", lc_user_id)

            Dim dt_data_table_ass As System.Data.DataTable = qsc_ass_code.execute_query()
            lc_ass_code = dt_data_table_ass.Rows(0).Item(0)

            Dim qsc_cust_ass_incl As New Utilize.Data.QuerySelectClass
            With qsc_cust_ass_incl
                .select_fields = "*"
                .select_from = "uws_cust_ass_cat"
            End With
            Dim dt_data_table_cust_ass_incl As System.Data.DataTable = qsc_cust_ass_incl.execute_query()

            Dim qsc_cust_ass_excl As New Utilize.Data.QuerySelectClass
            With qsc_cust_ass_excl
                .select_fields = "*"
                .select_from = "uws_cust_ass_excl"
            End With
            Dim dt_data_table_cust_ass_excl As System.Data.DataTable = qsc_cust_ass_excl.execute_query()

            If Not String.IsNullOrEmpty(lc_ass_code) Then
                ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True And dt_data_table_cust_ass_incl.Rows.Count > 0 Then
                    qsc_categories.select_where += " and uws_categories.cat_code in (select cat_code from uws_categories where uws_categories.cat_code in (select uws_cust_ass_cat.cat_code from uws_cust_ass_cat where uws_cust_ass_cat.ass_code = @ass_code_incl))"
                    qsc_categories.add_parameter("ass_code_incl", lc_ass_code)
                End If

                ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True And dt_data_table_cust_ass_excl.Rows.Count > 0 Then
                    qsc_categories.select_where += " and uws_categories.cat_code not in (select cat_code from uws_categories where uws_categories.cat_code in (select uws_cust_ass_excl.cat_code from uws_cust_ass_excl where uws_cust_ass_excl.ass_code = @ass_code_excl))"
                    qsc_categories.add_parameter("ass_code_excl", lc_ass_code)
                End If
            End If
        End If

        Dim dt_data_table_cat = qsc_categories.execute_query()
        dt_data_table_cat.TableName = "catData"
        Return dt_data_table_cat

    End Function

    Private Function GetAllSpecifics() As DataTable
        Dim lc_error_message As String = ""
        Dim lc_hdr_id As String = ""
        Dim lc_language As String = ""
        Dim lc_user_id As String = ""
        Dim ll_testing As Boolean = True
        Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim dt_data_table_specs As DataTable = Nothing
        Dim dt_products As DataTable = Nothing
        Dim qsc_select_specs As New Utilize.Data.QuerySelectClass

        'Ophalen en decrypten versleutelde login gegevens
        If ll_testing Then
            Dim lc_credentials_encrypted As String = Request.Params("login_credentials")
            Dim lc_credentials As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

            'Gegevens in een klasse zetten
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            shop = serializer.Deserialize(Of Customer4CustomerService.Webshop)(lc_credentials)
        End If

        ' Probeer in te loggen
        If ll_testing Then
            ll_testing = Me.global_ws.user_information.login(0, shop.login_code, shop.login_password)
            lc_user_id = Utilize.Data.DataProcedures.GetValue("uws_customers", "rec_id", global_ws.user_information.user_customer_id)

            If Not ll_testing Then
                lc_error_message = Me.global_ws.user_information.error_message
            End If
        End If

        If ll_testing Then
            lc_hdr_id = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", lc_user_id)
            lc_language = Utilize.Data.DataProcedures.GetValue("uws_customers", "cnt_code", lc_hdr_id)
        End If

        If ll_testing Then
            If String.IsNullOrEmpty(lc_language) Then
                lc_language = "nl"
            End If
        End If

        qsc_select_specs.select_fields = "uws_prod_spec.spec_code, uws_prod_spec.prod_code, uws_prod_spec.spec_val, uws_prod_spec.spec_fval"
        qsc_select_specs.select_fields += ", uws_prod_spec_desc.spec_desc_" + lc_language + " as spec_desc"
        qsc_select_specs.select_fields += ", uws_prod_spec_val.val_desc_" + lc_language + " as val_desc, uws_prod_spec_val.val_code"
        qsc_select_specs.add_join("left outer join", "uws_prod_spec_desc", "uws_prod_spec.spec_code = uws_prod_spec_desc.spec_code")
        qsc_select_specs.add_join("left outer join", "uws_prod_spec_val", "uws_prod_spec.spec_val = uws_prod_spec_val.val_code")
        qsc_select_specs.select_from = "uws_prod_spec"
        ' qsc_select_specs.select_where = " uws_prod_spec_desc.spec_code in (select spec_code from uws_prod_spec)"
        'qsc_select_specs.select_where += " and uws_prod_spec.spec_code in (select spec_code from uws_prod_spec_val)"

        Try
            dt_data_table_specs = qsc_select_specs.execute_query()
            dt_data_table_specs.TableName = "specs"
        Catch ex As Exception
            ll_testing = False
            lc_error_message = ex.Message
        End Try

        If ll_testing Then
            ' Loop door de specificaties
            For Each row As DataRow In dt_data_table_specs.Rows
                Dim lc_value As String = row.Item("spec_val")
                If String.IsNullOrEmpty(lc_value) Then
                    row.Item("spec_val") = row.Item("spec_fval")
                Else
                    row.Item("spec_val") = row.Item("val_desc")
                End If
            Next
        End If
        Return dt_data_table_specs
    End Function

    Private Function GetCurrency() As DataTable
        Dim qsc_currency As New Utilize.Data.QuerySelectClass
        Dim dt_data_table_currency As DataTable = Nothing

        qsc_currency.select_fields = "uws_currency.cur_code, uws_currency.exchange_rate, uws_currency.cur_desc"
        qsc_currency.select_from = "uws_currency"

        dt_data_table_currency = qsc_currency.execute_query()

        If dt_data_table_currency.Rows.Count = 0 Then
            dt_data_table_currency.Rows.Add("EUR", 1, "Euro")
        End If

        dt_data_table_currency.TableName = "currencyData"
        Return dt_data_table_currency
    End Function

    Private Function GetLanguages() As DataTable
        Dim qsc_languages As New Utilize.Data.QuerySelectClass
        With qsc_languages
            .select_fields = "ucm_languages.lng_code, ucm_languages.lng_desc_nl as lng_desc, ucm_languages.lng_image"
            .select_from = "ucm_languages"
            .select_where = "shop_code = @shop_code"
            .select_order = "row_ord"
        End With
        qsc_languages.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

        Dim dt_data_table_language As System.Data.DataTable = qsc_languages.execute_query()
        dt_data_table_language.TableName = "languageData"

        Return dt_data_table_language
    End Function

    Private Function GetVat() As DataTable
        Dim qsc_vat As New Utilize.Data.QuerySelectClass
        With qsc_vat
            .select_fields = "uws_vat.vat_code, uws_vat.vat_desc, uws_vat.vat_pct"
            .select_from = "uws_vat"
        End With
        Dim dt_data_table_vat As System.Data.DataTable = qsc_vat.execute_query()
        dt_data_table_vat.TableName = "vatData"

        Return dt_data_table_vat
    End Function

    Private Function GetShipMethods() As DataTable
        Dim qsc_ship_methods As New Utilize.Data.QuerySelectClass
        With qsc_ship_methods
            .select_fields = "uws_ship_methods.sm_code, uws_ship_methods.sm_desc"
            .select_from = "uws_ship_methods"
        End With
        Dim dt_data_table_ship_method As System.Data.DataTable = qsc_ship_methods.execute_query()
        dt_data_table_ship_method.TableName = "shipMethodData"

        Return dt_data_table_ship_method
    End Function

    Private Function GetIndicators() As DataTable
        Dim dt_data_table_stock_indicators As DataTable = Nothing
        Dim ll_testing As Boolean = True
        Dim lc_error_message As String = ""
        Dim qsc_stock_indicators As New Utilize.Data.QuerySelectClass
        Dim lc_language As String = Me.global_ws.Language

        If String.IsNullOrEmpty(lc_language) Then
            lc_language = "nl"
        End If
        If ll_testing Then
            qsc_stock_indicators.select_fields = "uws_stock_indicators.stock_desc_" + lc_language + " as stock_desc, uws_stock_indicators.stock_image, uws_stock_indicators.stock_color, uws_stock_indicators.stock_quantity"
            qsc_stock_indicators.select_from = "uws_stock_indicators"

            dt_data_table_stock_indicators = qsc_stock_indicators.execute_query()
            dt_data_table_stock_indicators.TableName = "indicatorData"

            For Each row As DataRow In dt_data_table_stock_indicators.Rows
                ' Wijzig de url voor de afbeelding
                Dim lcImageUrl As String = row.Item("stock_image")
                If Not String.IsNullOrEmpty(lcImageUrl) Then
                    row.Item("stock_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/" + lcImageUrl
                Else
                    row.Item("stock_image") = FrameWork.FrameworkProcedures.GetRequestUrl().Substring(0, Request.Url.ToString.IndexOf("/webshop/services")) + "/Documents/ProductImages/not-available-large.jpg"
                End If
            Next
        End If

        If dt_data_table_stock_indicators Is Nothing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
                New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}}

            dt_data_table_stock_indicators = Me.GenerateDataTable(table_values)
        End If

        Return dt_data_table_stock_indicators
    End Function

#End Region

#Region "Winkelwagen functies"
    'winkelwagen wordt aangemaakt met klantspecifieke tabellen
    Private Function GetShoppingCart() As String

        Dim ll_testing As Boolean = True
        Dim lc_error_message As String = ""
        Dim lc_invoice_address As String = ""
        Dim lc_delivery_address As String = ""

        Dim lc_order As String = ""
        Dim lc_json_order As String = ""
        Dim lc_json_order_lines As String = ""

        Dim lc_order_base64 As String = ""

        ' Haal de data uit de post request
        For Each key In HttpContext.Current.Request.Form.AllKeys
            If key = "cart" Then
                lc_order_base64 = HttpContext.Current.Request.Form(key)
            End If
        Next

        ll_testing = Not String.IsNullOrEmpty(lc_order_base64)

        If Not ll_testing Then
            lc_error_message = "Gegevens kunnen niet worden uitgelezen"
        End If

        ' Zet de base64string om
        If ll_testing Then
            Try
                lc_order = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_order_base64))
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try
        End If

        If ll_testing Then
            ' Vul het jsonorder object met de data
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            Dim newOrder As Customer4CustomerService.jsonorder = serializer.Deserialize(Of Customer4CustomerService.jsonorder)(lc_order)

            ' Voeg een order toe aan de winkelwagen
            Dim ubo_shopping_cart As Utilize.Data.BusinessObject = global_ws.shopping_cart.ubo_shopping_cart

            ' Zetten van de gegevens van de order
            ubo_shopping_cart.field_set("uws_orders.cust_id", newOrder.cust_id)
            'ubo_shopping_cart.field_set("uws_orders.del_date", newOrder.del_date)

            ' Toevoegen van de regels aan de order
            For Each lo_jsonline As Customer4CustomerService.jsonorderline In newOrder.orderlines
                If ll_testing Then
                    ll_testing = ubo_shopping_cart.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        lc_error_message = ubo_shopping_cart.error_message
                    End If
                End If

                If ll_testing Then
                    ubo_shopping_cart.field_set("uws_order_lines.prod_code", lo_jsonline.prod_code)
                    ubo_shopping_cart.field_set("uws_order_lines.prod_desc", Utilize.Data.DataProcedures.GetValue("uws_products", "prod_desc", lo_jsonline.prod_code))
                    ubo_shopping_cart.field_set("uws_order_lines.unit_qty", lo_jsonline.unit_qty)
                    ubo_shopping_cart.field_set("uws_order_lines.prod_price", Utilize.Data.DataProcedures.GetValue("uws_products", "prod_price", lo_jsonline.prod_code))
                End If
            Next

            ' In een try/catch voor gebruikers die deze functionaliteit nog niet hebben
            Try
                ubo_shopping_cart.api_field_updated("uws_orders.order_total_tax", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_order_lines.order_total", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_orders.vat_total", ubo_shopping_cart, "/prog")

                ubo_shopping_cart.api_field_updated("uws_order_lines.order_costs", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_orders.vat_code_ord", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_orders.vat_ord", ubo_shopping_cart, "/prog")

                ubo_shopping_cart.api_field_updated("uws_order_lines.shipping_costs", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_orders.vat_code_ship", ubo_shopping_cart, "/prog")
                ubo_shopping_cart.api_field_updated("uws_orders.vat_ship", ubo_shopping_cart, "/prog")

            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try

            ' Ophalen van de aangemaakt order en regels en zet deze om naar een json string
            If ll_testing Then
                lc_json_order = Me.GetJsonString(ubo_shopping_cart.data_tables("uws_orders").data_table)
                lc_json_order_lines = Me.GetJsonString(ubo_shopping_cart.data_tables("uws_order_lines").data_table)

                '        lc_invoice_address = global_ws.shopping_cart.get_invoice_adress()
                '        lc_delivery_address = global_ws.shopping_cart.get_delivery_adress()
            End If
        End If

        '' Zet de gegevens die worden teruggegeven
        Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "order", .value = lc_json_order}, _
            New Customer4CustomerService.Keyval With {.key = "lines", .value = lc_json_order_lines}, _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}}
        ''New Customer4CustomerService.Keyval With {.key = "invoice", .value = lc_invoice_address}, _
        ' New Customer4CustomerService.Keyval With {.key = "delivery", .value = lc_delivery_address}, _
        '    New Customer4CustomerService.Keyval With {.key = "order_placed", .value = ll_testing}, _

        Return Me.GetJsonString(Me.GenerateDataTable(table_values))
    End Function

    Private Function PlaceOrder() As String
        Me.write_log("C4C: PlaceOrder")

        Dim ll_testing As Boolean = True
        Dim lc_error_message As String = ""
        Dim lc_invoice_address As String = ""
        Dim lc_delivery_address As String = ""
        Dim lc_key As String = ""
        Dim lc_order As String = ""
        Dim lc_json_order As String = ""
        Dim lc_json_order_lines As String = ""

        Dim lc_order_base64 As String = ""

        ' Haal de data uit de post request

        For Each key In HttpContext.Current.Request.Form.AllKeys
            Me.write_log("C4C: key: " + key)
            lc_key += key + " "
            If key = "order" Then
                lc_order_base64 = HttpContext.Current.Request.Form(key)
                Me.write_log("C4C: lc_order_base64: " + lc_order_base64)
            End If
        Next

        ll_testing = Not String.IsNullOrEmpty(lc_order_base64)

        If Not ll_testing Then
            lc_error_message = "Gegevens kunnen niet worden uitgelezen"
        End If

        ' Zet de base64string om
        If ll_testing Then
            Try
                lc_order = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_order_base64))
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try
        End If

        If ll_testing Then
            ' Vul het jsonorder object met de data
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            Dim newOrder As Customer4CustomerService.jsonorder = serializer.Deserialize(Of Customer4CustomerService.jsonorder)(lc_order)
            Me.write_log("C4C: Add order " + newOrder.ord_desc + " for customer " + newOrder.cust_id)

            ' Voeg een order toe aan de winkelwagen
            Dim ubo_shopping_cart As Utilize.Data.BusinessObject = global_ws.shopping_cart.ubo_shopping_cart
            ll_testing = ubo_shopping_cart.table_insert_row("uws_orders", Me.global_ws.user_information.user_id)

            If Not ll_testing Then
                lc_error_message = ubo_shopping_cart.error_message
            End If

            If ll_testing Then
                ' Zetten van de gegevens van de order
                ubo_shopping_cart.field_set("uws_orders.cust_id", newOrder.cust_id)
                ubo_shopping_cart.field_set("uws_orders.order_description", IIf(newOrder.ord_desc Is Nothing, "", newOrder.ord_desc))
                ubo_shopping_cart.field_set("uws_orders.email_address", IIf(newOrder.email_address Is Nothing, "", newOrder.email_address))
                ubo_shopping_cart.field_set("uws_orders.order_comments", IIf(newOrder.order_comments Is Nothing, "", newOrder.order_comments))
                ubo_shopping_cart.field_set("uws_orders.del_date", newOrder.del_date)

                ' Alternatief afleveradres
                ubo_shopping_cart.field_set("uws_orders.del_address_custom", IIf(newOrder.del_bus_name Is Nothing, False, CBool(newOrder.del_address_custom)))
                ubo_shopping_cart.field_set("uws_orders.del_bus_name", IIf(newOrder.del_bus_name Is Nothing, "", newOrder.del_bus_name))
                ubo_shopping_cart.field_set("uws_orders.del_fst_name", IIf(newOrder.del_fst_name Is Nothing, "", newOrder.del_fst_name))
                ubo_shopping_cart.field_set("uws_orders.del_infix", IIf(newOrder.del_infix Is Nothing, "", newOrder.del_infix))
                ubo_shopping_cart.field_set("uws_orders.del_lst_name", IIf(newOrder.del_lst_name Is Nothing, "", newOrder.del_lst_name))
                ubo_shopping_cart.field_set("uws_orders.del_address", IIf(newOrder.del_address Is Nothing, "", newOrder.del_address))
                ubo_shopping_cart.field_set("uws_orders.del_house_nr", IIf(newOrder.del_house_nr Is Nothing, "", newOrder.del_house_nr))
                ubo_shopping_cart.field_set("uws_orders.del_zip_code", IIf(newOrder.del_zip_code Is Nothing, "", newOrder.del_zip_code))
                ubo_shopping_cart.field_set("uws_orders.del_city", IIf(newOrder.del_city Is Nothing, "", newOrder.del_city))
                ubo_shopping_cart.field_set("uws_orders.del_cnt_code", IIf(newOrder.del_cnt_code Is Nothing, "", newOrder.del_cnt_code))

                ' Handtekening
                ubo_shopping_cart.field_set("uws_orders.signature", newOrder.signature)

                ' Toevoegen van de regels aan de order in de winkelwagen
                For Each lo_jsonline As Customer4CustomerService.jsonorderline In newOrder.orderlines
                    Dim lc_product_code As String = lo_jsonline.prod_code
                    Dim lc_size As String = lo_jsonline.size

                    If ll_testing Then
                        ll_testing = ubo_shopping_cart.table_insert_row("uws_order_lines", Me.global_ws.user_information.user_id)

                        If Not ll_testing Then
                            lc_error_message = ubo_shopping_cart.error_message
                            Me.write_log("C4C: Add order error " + lc_error_message)
                        End If
                    End If

                    If ll_testing Then
                        ubo_shopping_cart.field_set("uws_order_lines.prod_code", lc_product_code)
                        ubo_shopping_cart.field_set("uws_order_lines.prod_desc", Utilize.Data.DataProcedures.GetValue("uws_products", "prod_desc", lo_jsonline.prod_code))
                        ubo_shopping_cart.field_set("uws_order_lines.unit_qty", lo_jsonline.unit_qty)
                        If Not String.IsNullOrEmpty(lo_jsonline.size) Then
                            ubo_shopping_cart.field_set("uws_order_lines.size", lc_size)
                        End If
                    End If

                    If Not ll_testing Then
                        Exit For
                    End If
                Next

                ' In een try/catch voor gebruikers die deze functionaliteit nog niet hebben
                Try
                    ubo_shopping_cart.api_field_updated("uws_orders.order_total_tax", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.order_total", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.vat_total", ubo_shopping_cart, "/prog")

                    ubo_shopping_cart.api_field_updated("uws_orders.order_costs", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.vat_code_ord", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.vat_ord", ubo_shopping_cart, "/prog")

                    ubo_shopping_cart.api_field_updated("uws_orders.shipping_costs", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.vat_code_ship", ubo_shopping_cart, "/prog")
                    ubo_shopping_cart.api_field_updated("uws_orders.vat_ship", ubo_shopping_cart, "/prog")

                Catch ex As Exception
                    ll_testing = False
                    lc_error_message = ex.Message
                End Try
            End If

            ' Zet de aangemaakte order en regels om naar een json string
            If ll_testing Then
                lc_json_order = Me.GetJsonString(ubo_shopping_cart.data_tables("uws_orders").data_table)
                lc_json_order_lines = Me.GetJsonString(ubo_shopping_cart.data_tables("uws_order_lines").data_table)
            End If

            ' Maak de de order in de winkelwagen een verkooporder
            If ll_testing Then
                ll_testing = global_ws.shopping_cart.create_purchase_order()

                If Not ll_testing Then
                    lc_error_message = ubo_shopping_cart.error_message
                    Me.write_log("C4C: Add order error " + ubo_shopping_cart.error_message)
                End If
            End If
        End If

        ' Zet de gegevens die worden teruggegeven
        Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "order", .value = lc_json_order}, _
            New Customer4CustomerService.Keyval With {.key = "lines", .value = lc_json_order_lines}, _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}, _
            New Customer4CustomerService.Keyval With {.key = "keys", .value = lc_key}, _
            New Customer4CustomerService.Keyval With {.key = "order_placed", .value = ll_testing}}

        Return Me.GetJsonString(Me.GenerateDataTable(table_values))
    End Function
#End Region

#Region "Login functies"
    Public Function getBackground() As DataTable
        Dim ll_testing As Boolean = True
        Dim lc_background_string As String = ""
        Dim lc_background_url As String = ""

        Dim qsc_background As New Utilize.Data.QuerySelectClass
        With qsc_background
            .select_fields = "uws_c4c.background_img_url"
            .select_from = "uws_c4c"
        End With
        Dim dt_background As System.Data.DataTable = qsc_background.execute_query
        dt_background.TableName = "backgroundData"

        Return dt_background

        'algemeen:
        'veld naam background_img_url
        'veld omschrijving achtergrond afbeelding
        'veld type character field
        'breedte 200
        'veld weergeven yes
        'tabblad: Vormgeving
        'api controls:
        'aangepaste veld control teken - upload control
        'doeldirectory: Documents/Images/C4CBanners/
        'afbeelding: yes
        'solutions:
        'framework oplossing: webshop
        'oplossing module: klanten voor klanten app

    End Function

    Public Function Login() As String
        Dim ll_testing As Boolean = True

        ' Declaratie variabelen
        Dim table() As Customer4CustomerService.Keyval = Nothing
        Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim lc_country As String = ""
        Dim lc_currency As String = ""
        Dim lc_error_message As String = ""
        Dim lc_exchange_rate As String = ""
        Dim lc_header As String = ""
        Dim lc_json_string As String = ""
        Dim lc_shop_name As String = ""
        Dim lc_user_id As String = ""
        Dim lc_user_address As String = ""
        Dim lc_user_zipcode As String = ""
        Dim lc_user_city As String = ""
        Dim lc_user_email As String = ""
        Dim ll_ship_method As Boolean = False
        Dim ll_show_order_history As Boolean = False
        Dim ll_show_outgoing_invoices As Boolean = False
        Dim ll_show_prices As Boolean = False
        Dim ll_show_specs As Boolean = False
        Dim ll_show_stock As Boolean = False
        Dim ll_show_comments As Boolean = False
        Dim ll_show_references As Boolean = False
        Dim ll_price_type As Boolean = False

        ' Controleer hier of de klanten voor klanten module beschikbaar is
        If ll_testing Then
            ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_C4C_APP")
            If Not ll_testing Then
                lc_error_message = global_trans.translate_message("message_c4c_module_not_available", "De module 'Klanten voor klant app' is niet beschikbaar.", Me.global_ws.Language)
            End If
        End If

        ' Ophalen en decrypten versleutelde login gegevens
        If ll_testing Then
            Dim lc_credentials_encrypted As String = Request.Params("login_credentials")
            Dim lc_credentials As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

            'Gegevens in een klasse zetten
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            shop = serializer.Deserialize(Of Customer4CustomerService.Webshop)(lc_credentials)
        End If

        ' Probeer in te loggen
        If ll_testing Then
            ll_testing = Me.global_ws.user_information.login(0, shop.login_code, shop.login_password)
            lc_user_id = Utilize.Data.DataProcedures.GetValue("uws_customers", "rec_id", global_ws.user_information.user_customer_id)

            If Not ll_testing Then
                lc_error_message = Me.global_ws.user_information.error_message
            End If
        End If

        ' Hier controleren we of de klant geblokkeerd is of niet.
        If ll_testing Then
            ll_testing = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers", "blocked", global_ws.user_information.user_customer_id))

            If Not ll_testing Then
                Me.global_ws.user_information.logout()
                lc_error_message = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_ws.Language)
            End If
        End If

        ' Hier controleren we of de gebruiker geblokkeerd is of niet.
        If ll_testing Then
            ll_testing = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "blocked", global_ws.user_information.user_id))

            If Not ll_testing Then
                Me.global_ws.user_information.logout()
                lc_error_message = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_ws.Language)
            End If
        End If

        'Hier halen we de adres gegevens op.
        If ll_testing Then
            lc_user_email = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "email_address", global_ws.user_information.user_id)
            lc_user_address = Utilize.Data.DataProcedures.GetValue("uws_customers", "address", global_ws.user_information.user_customer_id)
            lc_user_zipcode = Utilize.Data.DataProcedures.GetValue("uws_customers", "zip_code", global_ws.user_information.user_customer_id)
            lc_user_city = Utilize.Data.DataProcedures.GetValue("uws_customers", "city", global_ws.user_information.user_customer_id)
        End If

        'Hier controleren we of de prijzen getoond worden of niet
        If ll_testing Then
            ll_show_prices = CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "decl_px", global_ws.user_information.user_id))

            ' Toekomst!
            If shop.framework_version >= 3.3 Then
                ' Hier worden de nieuwe velden voor het framework in gezet
            End If
        End If

        'Hier controleren we of de bestelgeschiedenis getoond mag worden of niet
        If ll_testing Then
            ll_show_order_history = CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "decl_poh", global_ws.user_information.user_id))

            If shop.framework_version >= 3.3 Then
                ' Hier worden de nieuwe velden voor het framework in gezet
            End If
        End If

        'Hier controleren we of de facturen getoond mogen worden of niet
        If ll_testing Then
            ll_show_outgoing_invoices = CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "decl_oi", global_ws.user_information.user_id))

            If shop.framework_version >= 3.3 Then
                ' Hier worden de nieuwe velden voor het framework in gezet
            End If
        End If

        'Hier controleren we of de voorraad getoond mag worden
        If ll_testing Then
            ll_show_stock = Me.global_ws.get_webshop_setting("show_stock")
        End If

        If ll_testing Then
            lc_header = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", global_ws.user_information.user_id)

            If Not String.IsNullOrEmpty(lc_header) Then
                lc_currency = Utilize.Data.DataProcedures.GetValue("uws_customers", "cur_code", lc_header)
                lc_country = Utilize.Data.DataProcedures.GetValue("uws_customers", "cnt_code", lc_header)
            End If
        End If

        'Hier wordt de bedrijfsnaam opgehaald
        If ll_testing Then
            lc_header = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", global_ws.user_information.user_id)

            If Not String.IsNullOrEmpty(lc_header) Then
                lc_shop_name = Utilize.Data.DataProcedures.GetValue("uws_customers", "bus_name", lc_header)
            End If
        End If

        ' Hier wordt gekeken of de klant een opmerking/beschrijving mag meegeven
        If ll_testing Then
            ll_show_comments = Me.global_ws.get_webshop_setting("show_order_comm")
            ll_show_references = Me.global_ws.get_webshop_setting("show_order_desc")
            If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                ll_price_type = True
            Else
                ll_price_type = False
            End If
        End If

        ' Controleer of de module actief is
        If ll_testing Then
            ll_ship_method = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_ship_meth")
        End If

        Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "logged_in", .value = ll_testing}, _
            New Customer4CustomerService.Keyval With {.key = "login_code", .value = shop.login_code}, _
            New Customer4CustomerService.Keyval With {.key = "login_password", .value = shop.login_password}, _
            New Customer4CustomerService.Keyval With {.key = "user_email", .value = lc_user_email}, _
            New Customer4CustomerService.Keyval With {.key = "user_id", .value = lc_user_id}, _
            New Customer4CustomerService.Keyval With {.key = "cnt_code", .value = lc_country}, _
            New Customer4CustomerService.Keyval With {.key = "cur_code", .value = lc_currency}, _
            New Customer4CustomerService.Keyval With {.key = "header", .value = lc_header}, _ 
            New Customer4CustomerService.Keyval With {.key = "error_message", .value = lc_error_message}, _
            New Customer4CustomerService.Keyval With {.key = "hide_prices", .value = ll_show_prices}, _
            New Customer4CustomerService.Keyval With {.key = "show_order_history", .value = ll_show_order_history}, _
            New Customer4CustomerService.Keyval With {.key = "show_outgoing_invoices", .value = ll_show_outgoing_invoices}, _
            New Customer4CustomerService.Keyval With {.key = "bus_name", .value = lc_shop_name}, _
            New Customer4CustomerService.Keyval With {.key = "bus_address", .value = lc_user_address}, _
            New Customer4CustomerService.Keyval With {.key = "bus_zipcode", .value = lc_user_zipcode}, _
            New Customer4CustomerService.Keyval With {.key = "bus_city", .value = lc_user_city}, _ 
            New Customer4CustomerService.Keyval With {.key = "show_stock", .value = ll_show_stock}, _
            New Customer4CustomerService.Keyval With {.key = "show_comments", .value = ll_show_comments}, _ 
            New Customer4CustomerService.Keyval With {.key = "show_reference", .value = ll_show_references}, _
            New Customer4CustomerService.Keyval With {.key = "ship_methods", .value = ll_ship_method}, _ 
            New Customer4CustomerService.Keyval With {.key = "excl_btw", .value = ll_price_type} _
        }

        table = table_values

        ' Haal hier de json string van een datatable op
        lc_json_string = Me.GetJsonString(Me.GenerateDataTable(table))

        'Teruggeven JsonString van de ingevoerde tabel.
        Return lc_json_string
    End Function

    Public Function CustomColors() As String
        Dim lc_css_string As String = ""
        Dim lc_css_url As String = ""
        Dim lc_file_location As String = "/Documents/CSS/"
        Dim lc_file_name As String = "C4C_custom_CSS.css"
        Dim lc_file_path As String = Server.MapPath("~/Documents/CSS")
        Dim lc_json_String As String = ""
        Dim lc_webshop_url As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
        Dim lc_info As Byte()
        Dim lc_error_message As String = ""
        Dim ll_testing As Boolean = True

        ' Create url css location
        lc_css_url = lc_webshop_url + lc_file_location + lc_file_name

        If String.IsNullOrEmpty(lc_css_url) Then
            ll_testing = False
            Me.write_log("C4C: Css path error", lc_css_url)
        End If

        If ll_testing Then
            ' Verwijder het bestaande bestand
            If (File.Exists(lc_file_path + lc_file_name)) Then
                File.Delete(lc_file_path + lc_file_name)
            End If
        End If

        'Maak het bestand aan
        Dim fs As FileStream = File.Create(lc_file_path + lc_file_name)

        'Schrijf css in het bestand
        If ll_testing Then
            Try
                lc_css_string = Me.createCustomCSS()
                lc_info = New UTF8Encoding(True).GetBytes(lc_css_string)
                fs.Write(lc_info, 0, lc_info.Length)
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
                Me.write_log("C4C: Writing css error", lc_error_message)
            End Try
        End If

        fs.Close()

        If ll_testing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
                New Customer4CustomerService.Keyval With {.key = "css_path", .value = lc_css_url} _
            }

            lc_json_String = Me.GetJsonString(Me.GenerateDataTable(table_values))
        End If

        Return lc_json_String
    End Function

    Public Function color_opacity(ByVal Hex As String, amount As Decimal, Optional background As Boolean = False) As String
        Dim result = ""

        Dim red_org As String = "&H" & Hex.Substring(1, 2)
        Dim red As Integer = CInt(red_org)

        Dim green_org As String = "&H" & Hex.Substring(3, 2)
        Dim green As Integer = CInt(green_org)

        Dim blue_org As String = "&H" & Hex.Substring(5, 2)
        Dim blue As Integer = CInt(blue_org)

        If background Then
            If green > 250 And red > 250 And blue > 250 Then
                green = 240
                red = 240
                blue = 240
            End If
        End If

        Dim amount_string = amount.ToString.Replace(",", ".")

        result = "rgba(" + red.ToString() + ", " + green.ToString() + ", " + blue.ToString() + ", " + amount_string + ")"

        Return result
    End Function

    Private Function createCustomCSS() As String
        Dim lc_css_string As String = ""

        Dim qsc_c4c_settings As New Utilize.Data.QuerySelectClass
        With qsc_c4c_settings
            .select_fields = "*"
            .select_from = "uws_c4c"
        End With
        Dim dt_c4c_settings As System.Data.DataTable = qsc_c4c_settings.execute_query()

        Dim lc_background_color As String = dt_c4c_settings.Rows(0).Item("color_background")
        Dim lc_background_msg_color As String = dt_c4c_settings.Rows(0).Item("color_background_msg")
        Dim lc_background_menu_color As String = dt_c4c_settings.Rows(0).Item("color_background_mn")
        Dim lc_badge_color As String = dt_c4c_settings.Rows(0).Item("color_badge")
        Dim lc_base_color As String = dt_c4c_settings.Rows(0).Item("color_base")
        Dim lc_font_color As String = dt_c4c_settings.Rows(0).Item("color_font")
        Dim lc_font_msg_color As String = dt_c4c_settings.Rows(0).Item("color_font_msg")
        Dim lc_font_pressed_color As String = dt_c4c_settings.Rows(0).Item("color_font_pressed")
        Dim lc_title_color As String = dt_c4c_settings.Rows(0).Item("color_title")

        'Zet de achtergrond kleur
        lc_css_string += "*"
        lc_css_string += "{"
        lc_css_string += "font-family: verdana!important;"
        lc_css_string += "}"

        lc_css_string += ".x - container,"
        lc_css_string += ".x-scroll-container {"
        lc_css_string += "background-color:" + lc_background_color + "!important;"
        lc_css_string += "}"

        lc_css_string += ".x-filter-menu,"
        lc_css_string += ".x-main-menu{"
        lc_css_string += "background-color:" + lc_background_menu_color + "!important;"
        lc_css_string += "}"

        'Zet de achtergrond van de msgbox
        lc_css_string += ".x-msgbox, .x-msgbox .x-container {"
        lc_css_string += "background-color:" + lc_background_msg_color + "!important;"
        lc_css_string += "}"

        'Zet de rand kleur van de msgbox
        lc_css_string += ".x-msgbox {"
        lc_css_string += "border-color:" + lc_background_msg_color + "!important;"
        lc_css_string += "}"

        'Zet de badge kleur
        lc_css_string += ".x-hasbadge .x-badge{"
        lc_css_string += "background-color:" + lc_badge_color + "!important;"
        lc_css_string += "}"

        'Zet de knoppen naar basis kleur
        lc_css_string += ".x-button,"
        lc_css_string += ".x-msgbox .x-toolbar.x-docked-bottom .x-button{"
        lc_css_string += "background-color:" + lc_base_color + "!important;"
        lc_css_string += "border-color:" + lc_base_color + "!important;"
        lc_css_string += "}"

        'Zet de basis kleur
        lc_css_string += ".x-container.x-toolbar,"
        lc_css_string += ".x-container.x-toolbar.x-navigation-bar .x-horizontal,"
        lc_css_string += ".x-container.x-toolbar.x-shop-indicator .x-horizontal,"
        lc_css_string += ".x-container.x-toolbar.x-product-indicator .x-container{"
        lc_css_string += "background-color:" + lc_base_color + "!important;"
        lc_css_string += "}"

        'Zet de basis kleur in msg boxes
        lc_css_string += ".x-msgbox .x-toolbar,"
        lc_css_string += ".x-msgbox .x-toolbar .x-title .x-innerhtml{"
        lc_css_string += "background-color: transparent!important;"
        lc_css_string += "color:" + lc_base_color + "!important;"
        lc_css_string += "}"

        'Zet loader kleuren spinner.
        lc_css_string += ".x-loading-spinner>span.x-loading-top:before {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.15) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-top {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.99) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-top:after {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.9) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-right:before {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.3) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-right {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.25) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-right:after {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.2) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-bottom:before {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.5) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-bottom {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.4) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-bottom:after {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.35) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-left:before {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.7) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-left {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.8) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-loading-spinner>span.x-loading-left:after {"
        lc_css_string += "background-color: " + Me.color_opacity(lc_base_color, 0.6) + ";"
        lc_css_string += "}"

        'Zet de font kleur van tekst en icon
        lc_css_string += ".x-msgbox .x-toolbar.x-docked-bottom span.x-button-label,"
        lc_css_string += ".x-msgbox .x-toolbar.x-docked-bottom span.x-badge,"
        lc_css_string += ".x-button span.x-button-label, .x-button span.x-badge,"
        lc_css_string += ".x-container.x-toolbar .x-horizontal .x-innerhtml,"
        lc_css_string += ".header .x-title .x-innerhtml,"
        lc_css_string += ".x-button:before,"
        lc_css_string += ".x-button span.x-button-icon,"
        lc_css_string += ".x-button{"
        lc_css_string += "color:" + lc_font_color + "!important;"
        lc_css_string += "}"

        'Zet de kleur van de carousel indicators
        lc_css_string += ".x-carousel-indicator-dark span.x-carousel-indicator-active{"
        lc_css_string += "background-color: " + Me.color_opacity(lc_font_color, 0.7) + ";"
        lc_css_string += "}"
        lc_css_string += ".x-carousel-indicator-dark span{"
        lc_css_string += "background-color: " + Me.color_opacity(lc_font_color, 0.3) + ";"
        lc_css_string += "}"

        'Zet de font kleur in de msgbox
        lc_css_string += ".x-msgbox .x-dock-body .x-msgbox-text .x-innerhtml{"
        lc_css_string += "text-shadow:none;"
        lc_css_string += "color:" + lc_font_msg_color + "!important;"
        lc_css_string += "}"

        'Zet de font kleur van de knoppen wanneer erop gedrukt wordt
        lc_css_string += ".x-button:active:before,"
        lc_css_string += ".x-button:active span.x-button-icon,"
        lc_css_string += ".x-button:active span.x-button-label{"
        lc_css_string += "color:" + lc_font_pressed_color + "!important;"
        lc_css_string += "}"

        'Zet de titel kleur
        lc_css_string += ".login .x-title .x-innerhtml{"
        lc_css_string += "color:" + lc_title_color + "!important;"
        lc_css_string += "}"

        'Zet de transparanties die niet mogen veranderen
        lc_css_string += ".login .x-panel,"
        lc_css_string += ".x-container .login-toolbar,"
        lc_css_string += ".x-container.x-form-fieldset,"
        lc_css_string += ".x-container.x-field-text{"
        lc_css_string += "background-color:transparent!important;"
        lc_css_string += "}"

        'Zet de witkleuren die niet mogen veranderen
        lc_css_string += ".product,"
        lc_css_string += ".product .x-container,"
        lc_css_string += ".x-account-panel,"
        lc_css_string += ".x-cart-list .cartListItem,"
        lc_css_string += ".x-cart-list .cartListItem img,"
        lc_css_string += ".x-cart-list .cartListItem .x-container,"
        lc_css_string += ".x-product-page .x-form-fieldset,"
        lc_css_string += ".x-shop-menu .x-field-search{"
        lc_css_string += "background-color: white!important;"
        lc_css_string += "}"

        Return lc_css_string
    End Function

    Private Function getCountry(ByVal login_code As String) As String
        Dim ll_header = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", global_ws.user_information.user_id)
        Dim ll_country As String = Utilize.Data.DataProcedures.GetValue("uws_customers", "cnt_code", ll_header)
        If String.IsNullOrEmpty(ll_country) Then
            ll_country = Me.global_ws.Language
        End If
        If String.IsNullOrEmpty(ll_country) Then
            ll_country = "nl"
        End If

        Return ll_country
    End Function

    Private Function getEuroPrices() As DataTable
        Dim lc_language As String = "nl"
        Dim lc_currency As String = "EUR"
        Dim lc_hdr_id As String = ""
        Dim lc_user_id As String = ""
        Dim ll_testing As Boolean = True
        Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim lc_error_message As String = ""

        ' Haal eerst de categorieen op
        Dim dt_data_table_cat As DataTable = Nothing
        If ll_testing Then
            Dim qsc_select_cat As New Utilize.Data.QuerySelectClass
            With qsc_select_cat
                .select_fields = "cat_code"
                .select_from = "uws_categories"
                .select_where = "cat_show = 1"
            End With

            ' shop_code is empty for the main shop
            ' shop_code is present for a focus shop
            ' category_available true means all categories must be show (both main and focus shop)
            ' category_available false means only categories of the focus shop is shown
            Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
            Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
            If ll_category_avail Then
                qsc_select_cat.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
            Else
                qsc_select_cat.select_where += " and uws_categories.shop_code = @shop_code"
            End If
            qsc_select_cat.add_parameter("shop_code", lc_shop_code)

            ' Laad de gegevens in de datatable
            dt_data_table_cat = qsc_select_cat.execute_query()

            ll_testing = dt_data_table_cat IsNot Nothing

            If Not ll_testing Then
                lc_error_message = "Fout bij het ophalen van de categorieen"
            End If
        End If

        Dim dt_products As DataTable = Nothing
        If ll_testing Then
            ' Haal hier de producten op
            Dim qsc_select_products As New Utilize.Data.QuerySelectClass

            ' Als er geen categorieen zijn, geef dan de producten uit uws_products terug
            If dt_data_table_cat.Rows.Count = 0 Then
                qsc_select_products.select_fields = "prod_code as rec_id"
            End If

            ' Als er wel categorieen zijn, geef dan de producten uit uws_categories_prod terug
            If dt_data_table_cat.Rows.Count > 0 Then
                qsc_select_products.select_fields = "uws_categories_prod.rec_id"
                qsc_select_products.add_join("left outer join", "uws_categories_prod", "uws_products.prod_code = uws_categories_prod.prod_code")
            End If

            ' Dit is de gegevens die opgehaald moeten worden
            qsc_select_products.select_fields += ", uws_products.prod_code, uws_products.prod_price"
            qsc_select_products.select_from = "uws_products"
            qsc_select_products.select_where = "uws_products.prod_show = 1 and uws_products.prod_code in (select prod_code from uws_categories_prod) and lng_code = @lng_code"

            ' Als de module merk uitsluiting per klant aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brand_excl") = True Then
                qsc_select_products.select_where += " and uws_products.brand_code not in (select brand_code from uws_customers_brands where cust_id = @cust_id)"
            End If

            ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
                Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

                If Not String.IsNullOrEmpty(Me.global_ws.user_information.ubo_customer.field_get("ass_code")) Then
                    ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True Then
                        Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                        If Not lc_category_string = "" Then
                            qsc_select_products.select_where += " and uws_products.prod_code in (select prod_code from uws_categories_prod where '" + lc_category_string + "' like '%|' + uws_categories_prod.cat_code + '|%')"
                        End If
                    End If

                    ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                        Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)
                        qsc_select_products.select_where += " and uws_products.prod_code not in (select prod_code from uws_categories_prod where '" + lc_category_exclusion_string + "' like '%|' + uws_categories_prod.cat_code + '|%')"
                    End If
                End If
            End If

            ' Als de module landspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cnt_ass") = True Then
                Dim lc_ass_code As String = Utilize.Data.DataProcedures.GetValue("uws_countries", "ass_code", Me.global_ws.user_information.ubo_customer.field_get("cnt_code"))

                If Not Me.global_ws.user_information.ubo_customer.field_get("ass_code") = "" Then
                    qsc_select_products.select_where += " and uws_products.prod_code not in (select prod_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
                    qsc_select_products.select_where += " and uws_products.prod_group not in (select grp_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
                End If
            End If

            qsc_select_products.select_order = "uws_products.prod_prio DESC, uws_products.prod_code"
            qsc_select_products.add_join("inner join", "uws_products_tran", "uws_products_tran.prod_code = uws_products.prod_code")
            qsc_select_products.add_join("inner join", "uws_categories", "uws_categories.cat_code = uws_categories_prod.cat_code")

            qsc_select_products.add_parameter("lng_code", lc_language)
            qsc_select_products.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            ' Laad de gegevens in de datatable
            Try
                dt_products = qsc_select_products.execute_query()
                dt_products.TableName = "priceData"
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try
        End If

        If Not ll_testing Then
            'Me.write_log("Customer4Customer: Producten - " + dt_products.Rows.Count.ToString())
            'Else
            Me.write_log("Customer4Customer: Fout bij het ophalen van de producten", lc_error_message)
        End If

        If dt_products Is Nothing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}, _
            New Customer4CustomerService.Keyval With {.key = "error_occured", .value = ll_testing}}

            dt_products = Me.GenerateDataTable(table_values)
        End If
        Return dt_products
    End Function
    'Private Function GetCurrencySymbol() As String
    '    Dim ll_header = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", global_ws.user_information.user_id)
    '    Dim lc_currency_symbol As String = Utilize.Data.DataProcedures.GetValue("uws_customers", "cur_code", ll_header)
    '    If String.IsNullOrEmpty(lc_currency_symbol) Then
    '        lc_currency_symbol = "€"
    '    End If

    '    Return lc_currency_symbol
    'End Function

    Private Function getTranslation() As DataTable
        Dim ll_testing As Boolean = True
        Dim lc_language As String = ""
        ' Dim shop As Customer4CustomerService.Webshop = Nothing
        Dim lc_error_message As String = ""

        If ll_testing Then
            Dim lc_language_encrypted As String = Request.Params("language_param")
            lc_language = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_language_encrypted))
        End If

        Dim dt_translation As DataTable = Nothing
        If ll_testing Then
            ' Haal hier de vertalingen op
            Dim qsc_select_translations As New Utilize.Data.QuerySelectClass

            ' Dit zijn de gegevens die opgehaald moeten worden
            qsc_select_translations.select_fields += "uws_c4c_translations.field_code, uws_c4c_translations.field_desc_" + lc_language + " as field_desc"
            qsc_select_translations.select_from = "uws_c4c_translations"

            ' Laad de gegevens in de datatable
            Try
                dt_translation = qsc_select_translations.execute_query()
                dt_translation.TableName = "translations"
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
                Me.write_log("C4C: load translations", ex.Message)
            End Try
        End If

        If Not ll_testing Then
            ' Me.write_log("Customer4Customer: Vertalingen [" + lc_language + "] - " + dt_translation.Rows.Count.ToString())
            'Else
            Me.write_log("Customer4Customer: Fout bij het ophalen van de vertalingen", lc_error_message)
        End If

        If dt_translation Is Nothing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}, _ 
            New Customer4CustomerService.Keyval With {.key = "error_occured", .value = ll_testing}}

            dt_translation = Me.GenerateDataTable(table_values)
        End If
        Return dt_translation
    End Function

    'Public Function Logout() As String
    '    Dim lc_error_message As String = ""
    '    Dim lc_json_string As String = ""
    '    Dim lc_user_id As String = ""
    '    Dim ll_testing As Boolean = True
    '    Dim table() As Customer4CustomerService.Keyval = Nothing
    '    Dim shop As Customer4CustomerService.Webshop = Nothing

    '    ' Ophalen en decrypten versleutelde login gegevens
    '    If ll_testing Then
    '        Dim lc_credentials_encrypted As String = Request.Params("login_credentials")
    '        Dim lc_credentials As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

    '        'Gegevens in een klasse zetten
    '        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
    '        shop = serializer.Deserialize(Of Customer4CustomerService.Webshop)(lc_credentials)
    '    End If

    '    ' Probeer in te loggen
    '    If ll_testing Then
    '        ll_testing = Me.global_ws.user_information.login(0, shop.login_code, shop.login_password)
    '        lc_user_id = Utilize.Data.DataProcedures.GetValue("uws_customers", "rec_id", global_ws.user_information.user_customer_id)

    '        If Not ll_testing Then
    '            lc_error_message = Me.global_ws.user_information.error_message
    '        End If
    '    End If

    '    Dim table_values() As Customer4CustomerService.Keyval = { _
    '        New Customer4CustomerService.Keyval With {.key = "error_message", .value = lc_error_message}, _
    '        New Customer4CustomerService.Keyval With {.key = "logged out", .value = ll_testing} _
    '        }
    '    table = table_values

    '    ' Haal hier de json string van een datatable op
    '    lc_json_string = Me.GetJsonString(Me.GenerateDataTable(table))

    '    'Teruggeven JsonString van de ingevoerde tabel.
    '    Return lc_json_string
    'End Function
#End Region

#Region "Notification functies"
    Private Function getDeviceTokens() As DataTable
        Dim ll_testing As Boolean = True
        Dim lc_error_message As String = ""

        Dim dt_tokens As DataTable = Nothing
        If ll_testing Then
            ' Haal hier de tokens op
            ' Dit zijn de gegevens die opgehaald moeten worden
            Dim qsc_select_tokens As New Utilize.Data.QuerySelectClass
            With qsc_select_tokens
                .select_fields = "*"
                .select_from = "uws_c4c_device_token"
            End With

            ' Laad de gegevens in de datatable
            Try
                dt_tokens = qsc_select_tokens.execute_query()
                dt_tokens.TableName = "tokens"
            Catch ex As Exception
                ll_testing = False
                lc_error_message = ex.Message
            End Try
        End If

        If dt_tokens Is Nothing Then
            Dim table_values() As Customer4CustomerService.Keyval = { _
            New Customer4CustomerService.Keyval With {.key = "error_msg", .value = lc_error_message}, _
            New Customer4CustomerService.Keyval With {.key = "error_occured", .value = ll_testing}}

            dt_tokens = Me.GenerateDataTable(table_values)
        End If

        Return dt_tokens
    End Function

    Private Function DeviceToken() As String
        Dim table() As Customer4CustomerService.Keyval = Nothing
        Dim device As Customer4CustomerService.Device = Nothing
        Dim tokens As DataTable = Nothing
        Dim ubo_device_token As Utilize.Data.BusinessObject = Nothing
        Dim ll_testing As Boolean = True
        Dim lc_credentials As String = ""
        Dim lc_credentials_encrypted As String = ""
        Dim lc_error_message As String = ""

        If ll_testing Then
            lc_credentials_encrypted = Request.Params("device_credentials")
            lc_credentials = System.Text.Encoding.Default.GetString(Convert.FromBase64String(lc_credentials_encrypted))

            'Gegevens in een klasse zetten
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
            device = serializer.Deserialize(Of Customer4CustomerService.Device)(lc_credentials)
        End If

        tokens = Me.getDeviceTokens()

        For Each row As DataRow In tokens.Rows
            If String.Equals(row.Item("user_id"), device.user_id) And String.Equals(row.Item("device_id"), device.device_id) Then
                'ubo_device_token = Utilize.Data.DataProcedures.CreateRecordObject("uws_c4c_device_token", row.Item("rec_id"), "rec_id")
                If Not String.Equals(row.Item("device_token"), device.device_token) Then
                    'ubo_device_token.table_locate("uws_c4c_device_token", "rec_id = '" + row.Item("rec_id") + "'")
                    'ubo_device_token.field_set("uws_c4c_device_token.device_token", device.device_token)
                    'ubo_device_token.table_update(Me.global_ws.user_information.user_id)
                    ' replace device_token in row
                End If
            Else
                'ubo_device_token.table_insert_row("uws_c4c_device_token")
                'ubo_device_token.field_set("user_id", device.user_id)
                'ubo_device_token.field_set("device_token", device.device_token)
                'ubo_device_token.field_set("device_id", device.device_id)
                'ubo_device_token.field_set("last_login", device.last_login)
                ' new row with user_id  & device_id & device_token
            End If
        Next

        'table = { _
        '    New Customer4CustomerService.Keyval With {.key = "token_placed", .value = ll_testing}, _
        '    New Customer4CustomerService.Keyval With {.key = "error_message", .value = lc_error_message} _
        '}

        Return Me.GetJsonString(Me.GenerateDataTable(table))

    End Function
#End Region

    Public Function write_log(ByVal log_message As String, Optional ByVal log_details As String = "") As String
        Try
            ' Create the business object
            Dim ubo_err_log As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("utlz_err_log")

            ' Insert a new row
            ubo_err_log.table_insert_row("utlz_error_log", Me.global_ws.user_information.user_id)

            ' Set the details
            ubo_err_log.field_set("err_date", System.DateTime.Now())
            ubo_err_log.field_set("err_msg", log_message)
            ubo_err_log.field_set("err_details", log_details)

            ' Save the information
            ubo_err_log.table_update(Me.global_ws.user_information.user_id)

            ' Release the error object
            ubo_err_log = Nothing
        Catch ex As Exception

        End Try

        Return ""
    End Function
End Class