﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_tearsheet.ascx.vb" Inherits="Bespoke_Modules_Tearsheet_uc_tearsheet" %>
<utilize:placeholder runat="server" Visible="True" ID="ph_tearsheet_generator">
    <div class="uc_tearsheet">
        <utilize:linkbutton runat="server" ID="lb_generate_tearsheet" CssClass="tearsheet-btn">
            <i class="fa fa-file-pdf-o pdf-download-btn"></i>
            <utilize:translatelabel runat="server" ID="label_tearsheet_download_pdf" Text="PDF"></utilize:translatelabel>
        </utilize:linkbutton>
    </div>
</utilize:placeholder>
