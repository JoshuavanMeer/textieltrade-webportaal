﻿Imports Newtonsoft.Json

Partial Class Bespoke_Modules_Tearsheet_uc_tearsheet
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _sessionId As String = ""
    Private _productCode As String = ""

    Public Property SessionId As String
        Get
            Return _sessionId
        End Get
        Set(value As String)
            _sessionId = value
        End Set
    End Property

    Public Property ProductCode As String
        Get
            Return _productCode
        End Get
        Set(value As String)
            _productCode = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Me.ph_tearsheet_generator.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_tearsheet")

        Me.set_style_sheet("uc_tearsheet.css", Me)
        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Me.lb_generate_tearsheet)
    End Sub

    Private Function GetTearsheetProductData(ByVal Language As String) As String

        Dim tearsheetDto As New TearsheetDto
        Dim tearsheetProduct As New TearsheetProduct
        Dim companyInfo As New CompanyInfo
        Dim tearsheetSettings As New TearsheetSettings

        Dim lc_prod_barcode As String = ""
        Dim list_prod_specs As New List(Of KeyValuePair(Of String, String))
        Dim lc_base_url As String = Request.Url.GetLeftPart(UriPartial.Authority)

        ' Add tearsheet settings
        Dim ln_fontsize_large As Integer = Me.global_ws.get_webshop_setting("fontsize_large")
        Dim ln_fontsize_medium As Integer = Me.global_ws.get_webshop_setting("fontsize_medium")
        Dim ln_fontsize_small As Integer = Me.global_ws.get_webshop_setting("fontsize_small")

        tearsheetSettings.FontSizeLarge = ln_fontsize_large
        tearsheetSettings.FontSizeMedium = ln_fontsize_medium
        tearsheetSettings.FontSizeSmall = ln_fontsize_small

        ' Add main image url 
        Dim qsc_prod_data_table As New Utilize.Data.QuerySelectClass
        qsc_prod_data_table.select_fields = "*"
        qsc_prod_data_table.select_from = "uws_products"
        qsc_prod_data_table.select_where = "prod_code = @prod_code"
        qsc_prod_data_table.add_parameter("prod_code", _productCode)

        Dim dt_data_table As System.Data.DataTable = qsc_prod_data_table.execute_query()

        If dt_data_table.Rows.Count > 0 Then
            Dim dr_data As System.Data.DataRow = dt_data_table.Rows(0)
            tearsheetProduct.Code = _productCode
            Dim lc_main_image As String = lc_base_url + Me.ResolveCustomUrl("~/" + dr_data.Item("image_large"))

            If Not System.IO.File.Exists(Server.MapPath(Me.ResolveCustomUrl("~/" + dr_data.Item("image_large")))) Then
                lc_main_image = lc_base_url + "/documents/productimages/not-available-large.jpg"
            End If
            tearsheetProduct.MainImageUrl = lc_main_image
        End If

        ' Add subimages
        tearsheetProduct.SubImagesUrl = New List(Of String)

        Dim qsc_prod_subimages_table As New Utilize.Data.QuerySelectClass
        qsc_prod_subimages_table.select_fields = "*"
        qsc_prod_subimages_table.select_from = "uws_product_images"
        qsc_prod_subimages_table.select_where = "prod_code = @prod_code"
        qsc_prod_subimages_table.add_parameter("prod_code", _productCode)
        Dim dt_prod_subimages_data_table As System.Data.DataTable = qsc_prod_subimages_table.execute_query()

        If dt_prod_subimages_data_table.Rows.Count > 0 Then
            For Each dr_data_row As System.Data.DataRow In dt_prod_subimages_data_table.Rows
                Dim lc_sub_image As String = lc_base_url + Me.ResolveCustomUrl("~/" + dr_data_row.Item("image_small"))
                If Not System.IO.File.Exists(Server.MapPath(Me.ResolveCustomUrl("~/" + dr_data_row.Item("image_small")))) Then
                    lc_sub_image = lc_base_url + "/documents/productimages/not-available-small.jpg"
                End If
                tearsheetProduct.SubImagesUrl.Add(lc_sub_image)
            Next
        End If

        ' Set product specs as a list 
        Dim qsc_prod_tran_data_table As New Utilize.Data.QuerySelectClass
        qsc_prod_tran_data_table.select_fields = "*"
        qsc_prod_tran_data_table.select_from = "uws_products_tran"
        qsc_prod_tran_data_table.select_where = "prod_code = @prod_code and lng_code = @prod_lng"
        qsc_prod_tran_data_table.add_parameter("prod_code", _productCode)
        qsc_prod_tran_data_table.add_parameter("prod_lng", Me.global_ws.Language)

        Dim dt_data_tran_table As System.Data.DataTable = qsc_prod_tran_data_table.execute_query()

        If dt_data_tran_table.Rows.Count > 0 Then
            Dim dr_data As System.Data.DataRow = dt_data_tran_table.Rows(0)

            tearsheetProduct.Title = dr_data.Item("prod_desc")

            If Not String.IsNullOrEmpty(dr_data.Item("prod_desc")) Then
                tearsheetProduct.Description = StripHtmlTags(dr_data.Item("prod_comm"))
            End If

            ' Add product code and barcode
            list_prod_specs.Add(New KeyValuePair(Of String, String)(global_trans.translate_label("label_product_code", Me.global_ws.Language), _productCode))

            If Not String.IsNullOrEmpty(lc_prod_barcode) Then
                Add_To_List(global_trans.translate_label("label_barcode", Me.global_ws.Language), lc_prod_barcode, list_prod_specs)
            End If

            ' Add product grouping values
            If Me.global_ws.get_webshop_setting("tear_showgrouping") Then
                Add_To_List(dr_data.Item("grp_desc1").ToString(), dr_data.Item("grp_val1").ToString(), list_prod_specs)
                Add_To_List(dr_data.Item("grp_desc2").ToString(), dr_data.Item("grp_val2").ToString(), list_prod_specs)
            End If

            ' Add free text values
            If Me.global_ws.get_webshop_setting("tear_showfreetext") Then
                Add_To_List(global_trans.translate_label("label_free_text1", Me.global_ws.Language), dr_data.Item("ff_desc1").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text2", Me.global_ws.Language), dr_data.Item("ff_desc2").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text3", Me.global_ws.Language), dr_data.Item("ff_desc3").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text4", Me.global_ws.Language), dr_data.Item("ff_desc4").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text5", Me.global_ws.Language), dr_data.Item("ff_desc5").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text6", Me.global_ws.Language), dr_data.Item("ff_desc6").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text7", Me.global_ws.Language), dr_data.Item("ff_desc7").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text8", Me.global_ws.Language), dr_data.Item("ff_desc8").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text9", Me.global_ws.Language), dr_data.Item("ff_desc9").ToString(), list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_text10", Me.global_ws.Language), dr_data.Item("ff_desc10").ToString(), list_prod_specs)
            End If
        End If

        ' Add free list values 
        If Me.global_ws.get_webshop_setting("tear_showfreelist") Then
            Dim pf_product_details As New ws_productdetails
            Dim dt_product_details As System.Data.DataTable = pf_product_details.get_product_details(_productCode, Me.global_ws.Language)
            If dt_product_details.Rows.Count > 0 Then
                Dim lc_free_fl1 As String = Utilize.Data.DataProcedures.GetValue("uws_free_list1", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code1"))
                Dim lc_free_fl2 As String = Utilize.Data.DataProcedures.GetValue("uws_free_list2", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code2"))
                Dim lc_free_fl3 As String = Utilize.Data.DataProcedures.GetValue("uws_free_list3", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code3"))
                Dim lc_free_fl4 As String = Utilize.Data.DataProcedures.GetValue("uws_free_list4", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code4"))

                Add_To_List(global_trans.translate_label("label_free_list1", Me.global_ws.Language), lc_free_fl1, list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_list2", Me.global_ws.Language), lc_free_fl2, list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_list3", Me.global_ws.Language), lc_free_fl3, list_prod_specs)
                Add_To_List(global_trans.translate_label("label_free_list4", Me.global_ws.Language), lc_free_fl4, list_prod_specs)
            End If
        End If

        ' Add product specs
        If Me.global_ws.get_webshop_setting("tear_showspecs") Then
            Dim udt_product_specs As System.Data.DataTable = Nothing

            Dim lc_language = Me.global_cms.Language
            Dim qc_query_class As New Utilize.Data.QuerySelectClass

            qc_query_class.select_from = "UWS_PROD_SPEC"
            qc_query_class.select_where = "UWS_PROD_SPEC.prod_code = @prod_code "
            qc_query_class.add_parameter("prod_code", Me.get_page_parameter("prod_code"))
            qc_query_class.add_join("left outer join", "UWS_PROD_SPEC_DESC", "UWS_PROD_SPEC.spec_code = UWS_PROD_SPEC_DESC.spec_code")
            qc_query_class.add_join("left outer join", "UWS_PROD_SPEC_VAL", "UWS_PROD_SPEC.spec_code = UWS_PROD_SPEC_VAL.spec_code AND UWS_PROD_SPEC.spec_val = UWS_PROD_SPEC_VAL.val_code")
            qc_query_class.select_fields = "UWS_PROD_SPEC_DESC.SPEC_DESC_" & lc_language & " AS SPEC_DESC , "
            qc_query_class.select_fields &= "CASE WHEN uws_prod_spec.spec_free = 1 THEN uws_prod_spec.spec_fval ELSE UWS_PROD_SPEC_VAL.VAL_DESC_" & lc_language & " END AS VAL_DESC "
            qc_query_class.select_order = "uws_prod_spec.row_ord"

            udt_product_specs = qc_query_class.execute_query()

            If udt_product_specs.Rows.Count > 0 Then
                For Each dr_data_row As System.Data.DataRow In udt_product_specs.Rows
                    Dim lc_spec_desc As String = dr_data_row.Item("spec_desc")
                    Dim lc_spec_val As String = dr_data_row.Item("val_desc")
                    Add_To_List(lc_spec_desc, lc_spec_val, list_prod_specs)
                Next
            End If
        End If

        ' Add default product prices
        If Me.global_ws.get_webshop_setting("tear_showprice") Then
            Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
            If Not prices_not_visible Then
                Dim ll_show_prices As Boolean = IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
                If ll_show_prices Then
                    ' 0 = standard price
                    ' 1 = customer price
                    If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                        If Me.global_ws.get_webshop_setting("tear_price_type") = 0 Then
                            If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                                ' Excl. BTW
                                Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price_default(_productCode)
                                tearsheetProduct.DefaultPrice = Format(ln_prod_default_price, "c")
                            Else
                                ' Incl. BTW
                                Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price_default_incl_vat(_productCode)
                                tearsheetProduct.DefaultPrice = Format(ln_prod_default_price, "c")
                            End If
                        ElseIf Me.global_ws.get_webshop_setting("tear_price_type") = 1 Then
                            If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                                ' Excl. BTW
                                Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price(_productCode, "")
                                tearsheetProduct.DefaultPrice = Format(ln_prod_default_price, "c")
                            Else
                                ' Incl. BTW
                                Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price_incl_vat(_productCode, "")
                                tearsheetProduct.DefaultPrice = Format(ln_prod_default_price, "c")
                            End If
                        End If
                    Else
                        ' Use credits
                        If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                            ' Excl. BTW
                            Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price_default(_productCode)
                            tearsheetProduct.DefaultPrice = Me.global_ws.convert_amount_to_credits(ln_prod_default_price)
                        Else
                            ' Incl. BTW
                            Dim ln_prod_default_price As Decimal = Me.global_ws.get_product_price_default_incl_vat(_productCode)
                            tearsheetProduct.DefaultPrice = Me.global_ws.convert_amount_to_credits(ln_prod_default_price)
                        End If
                    End If
                End If
            End If
        End If

        ' Set company information
        companyInfo.LogoUrl = lc_base_url + Me.ResolveCustomUrl("~/" + Me.global_ws.get_webshop_setting("tearsheet_logo"))
        Dim lc_footer_company_info As String = StripHtmlTags(global_trans.translate_text("label_tearsheet_footer_information", "Le Nouveau Chef - De Droogmakerij 29H - 1851 LX Heiloo, Nederland TEL: +31 (0)72 533 5900 - www.lenouveauchef.nl", Me.global_ws.Language))
        companyInfo.FooterInformation = lc_footer_company_info

        ' Combine tearsheet product, company info and settings to one object
        tearsheetProduct.ProductSpecs = list_prod_specs
        tearsheetDto.CompanyInfo = companyInfo
        tearsheetDto.TearsheetProduct = tearsheetProduct
        tearsheetDto.TearsheetSettings = tearsheetSettings

        Dim lcJsonTearsheet = JsonConvert.SerializeObject(tearsheetDto)
        Me.global_rma.logger.log(lcJsonTearsheet)
        Return lcJsonTearsheet
    End Function


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(ViewState("ImportSessionId")) Then
            SessionId = Guid.NewGuid().ToString()
            ViewState("ImportSessionId") = SessionId
        Else
            _sessionId = ViewState("ImportSessionId")
        End If
    End Sub

    Private Sub getPdfBytes()
        Dim loGenerateTearsheetService As New CreateTearsheetService
        loGenerateTearsheetService.CreateTearsheet(_sessionId, Me.GetTearsheetProductData(Me.global_ws.Language), ConfigurationManager.AppSettings("tearsheet_generator_url"))
    End Sub

    Private Sub lb_generate_tearsheet_Click(sender As Object, e As EventArgs) Handles lb_generate_tearsheet.Click
        Try
            getPdfBytes()

            Session("SessionId") = SessionId

            Dim loByte As Byte() = Cache(SessionId)
            Response.Clear()
            Response.BufferOutput = False
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-length", loByte.Length.ToString())
            Response.AddHeader("Content-Disposition", "attachment; filename=" + _productCode + "_tearsheet.pdf")
            Response.BinaryWrite(loByte)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "mailing_email_address_empty", "alert('" + global_trans.translate_message("MESSAGE_TEARSHEET_ERROR", "Er is iets fout gegaan bij het aanmaken van de tearsheet, probeer het later opnieuw", Me.global_ws.Language) + "');", True)

            CreateError(ex)
        End Try
    End Sub

    Private Sub CreateError(ex As Exception)
        ' Create the business object
        Dim ubo_err_log As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("utlz_err_log")

        ' Insert a new row
        ubo_err_log.table_insert_row("utlz_error_log", Me.global_ws.user_information.user_id)

        ' Set the details
        ubo_err_log.field_set("err_date", System.DateTime.Now())
        ubo_err_log.field_set("err_msg", ex.Message)
        ubo_err_log.field_set("err_details", ex.StackTrace)

        ' Save the information
        ubo_err_log.table_update(Me.global_ws.user_information.user_id)

        ' Release the error object
        ubo_err_log = Nothing
    End Sub

    Private Function StripHtmlTags(ByVal html As String) As String
        Dim lc_stripped As String = ""
        lc_stripped = Regex.Replace(html, "<.*?>", "")
        ' Bullet entities do not render correctly in PDF files and are replaced with dashes
        lc_stripped = Regex.Replace(lc_stripped, "&bull;", "-")
        Return lc_stripped
    End Function

    Private Sub Add_To_List(ByVal label As String, ByVal value As String, list As List(Of KeyValuePair(Of String, String)))
        If Not String.IsNullOrEmpty(value) Then
            list.Add(New KeyValuePair(Of String, String)(label, value))
        End If
    End Sub
End Class
