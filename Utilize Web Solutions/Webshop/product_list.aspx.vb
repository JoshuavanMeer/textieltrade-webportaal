﻿

<Serializable()>
Public Class product_list
    Inherits CMSPageTemplate

    Private Function get_page_template(ByVal current_cat As String) As String
        Dim lc_page_template As String = ""

        If Not current_cat = "" Then
            Dim qc_select As New Utilize.Data.QuerySelectClass
            qc_select.select_fields = "cat_code, cat_parent, page_template"
            qc_select.select_from = "uws_categories"
            qc_select.select_where = "cat_code = @cat_code"
            qc_select.add_parameter("cat_code", current_cat)

            Dim dt_data_table As System.Data.DataTable = qc_select.execute_query()

            If dt_data_table.Rows.Count > 0 Then
                Dim lc_cat_code As String = dt_data_table.Rows(0).Item("cat_code")
                Dim lc_parent_cat As String = dt_data_table.Rows(0).Item("cat_parent")

                If dt_data_table.Rows(0).Item("page_template").ToString.Trim() = "" Then
                    lc_page_template = Me.get_page_template(lc_parent_cat)
                Else
                    lc_page_template = dt_data_table.Rows(0).Item("page_template").ToString.Trim()
                End If
            End If
        Else
            lc_page_template = Me.global_ws.get_webshop_setting("product_template")
        End If

        Return lc_page_template
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_cat_code As String = Me.get_page_parameter("cat_code")
        Dim lc_template_code As String = ""

        If Not lc_cat_code.Trim() = "" Then
            ' Vul de pagina template uit de categorieën
            lc_template_code = Me.get_page_template(lc_cat_code)
        End If

        If lc_template_code.Trim() = "" Then
            ' Gebruik de standaard pagina template
            lc_template_code = Me.global_ws.get_webshop_setting("product_template")
        End If

        ' Vul de pagina template
        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")

        If Me.get_page_parameter("cat_code") = "" Then
            Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_label("label_webshop", "Webshop", Me.global_ws.Language)
        Else
            Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", Me.get_page_parameter("cat_code").Trim() + Me.global_ws.Language, "cat_code + lng_code")
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim lc_cat_code As String = Me.get_page_parameter("cat_code")

        If (Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_type", lc_cat_code) = 2 Or Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_code", lc_cat_code, "cat_parent") = "") Or
            Not String.IsNullOrEmpty(Me.get_page_parameter("list_code")) Then
            ' Maak een nieuwe tekst blok aan.
            Dim uc_block As Base.base_usercontrol_base

            If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_productlist") Then
                If global_ws.get_webshop_setting("use_prod_list_v2") Then
                    uc_block = LoadUserControl("~/Webshop/Modules/ProductList/uc_product_list_v2.ascx")
                Else
                    uc_block = LoadUserControl("~/Webshop/Modules/ProductList/uc_product_list.ascx")
                End If
            Else
                uc_block = LoadUserControl("~/Webshop/Modules/ProductListElastic/uc_product_list_elastic.ascx")
            End If

            uc_block.set_property("block_id", lc_cat_code)
            uc_block.set_property("object_code", "uws_categories")

            ph_target_block.Controls.Add(uc_block)

            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "setOverview", "setOverview();", True)
        Else
            ' Maak een nieuwe tekst blok aan.
            Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Webshop/Modules/Categories/uc_categories_overview.ascx")
            uc_block.set_property("block_id", lc_cat_code)
            uc_block.set_property("object_code", "uws_categories")
            uc_block.set_property("item_size_int", 3)

            Try
                uc_block.set_property("item_size_int", CInt(Me.global_ws.get_webshop_setting("cat_item_layout")))
            Catch ex As Exception

            End Try


            ph_target_block.Controls.Add(uc_block)
        End If

        If Not lc_cat_code = "" Then
            Dim lc_meta_desc As String = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "meta_desc", lc_cat_code + Me.global_ws.Language, "cat_code + lng_code")

            Me.meta_description = IIf(String.IsNullOrEmpty(lc_meta_desc), Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", lc_cat_code + Me.global_ws.Language, "cat_code + lng_code"), lc_meta_desc)
            Me.meta_keywords = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "meta_keyw", lc_cat_code + Me.global_ws.Language, "cat_code + lng_code")
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Session("ProductPage") = Request.RawUrl.ToString()
        End If
    End Sub
End Class
