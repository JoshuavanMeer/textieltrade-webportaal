﻿<%@ WebHandler Language="VB" Class="MultiOrderUploadFile" %>

Imports System
Imports System.Web
Imports System.Linq


<Serializable()>
Public Class MultiOrderUploadFile : Implements IHttpHandler

    ''' <summary>
    ''' Process request if there are files to be updated
    ''' </summary>
    ''' <param name="context"></param>
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        ' If files in upload, then process the order details
        If context.Request.Files.Count > 0 Then

            Dim cust_id As String = context.Request.Params.Get("a")
            Dim user_id As String = context.Request.Params.Get("b")
            Dim session_id As String = context.Request.Params.Get("c")
            Dim language As String = context.Request.Params.Get("d")
            Dim csvfiles_orders As New List(Of String)

            For Each s As String In context.Request.Files
                Dim file As HttpPostedFile = context.Request.Files.Get(s)
                Dim filename As String = file.FileName
                Dim ll_resume As Boolean = True
                'Is it a CSV file
                If ll_resume Then
                    ll_resume = filename.ToLower().EndsWith(".csv")
                End If

                If ll_resume Then

                    Dim file_bytes As New List(Of Byte)
                    Dim file_byte As Integer = file.InputStream.ReadByte
                    While file_byte <> -1
                        file_bytes.Add(file_byte)
                        Try
                            file_byte = file.InputStream.ReadByte()
                        Catch ex As Exception
                            file_byte = -1
                        End Try
                    End While

                    Dim file_content As Char() = Text.Encoding.Default.GetChars(file_bytes.ToArray)

                    csvfiles_orders.Add(file_content)

                End If
            Next

            ' Maak het importobject aan en zet de waarden
            Dim loImportObject As New ImportObject(session_id)
            loImportObject.RecId = user_id
            loImportObject.Language = language
            loImportObject.Url = ""
            loImportObject.Finished = False
            loImportObject.RowCount = 0
            loImportObject.CurrentRow = 0
            loImportObject.ErrorCount = 0
            loImportObject.SuccessCount = 0
            loImportObject.ErrorText = ""


            Dim WSMultiOrderImport As New ImportMultiOrderService
            Dim lo_application As New Web.HttpApplication
            Dim lo_datasecurity_encryptor As Utilize.Secure.WebEncryption.Encryptor = Nothing

            Try
                lo_datasecurity_encryptor = lo_application.Application.Get("lo_datasecurity_encryptor")
            Catch ex As Exception
                lo_datasecurity_encryptor = Nothing
            End Try

            Dim lo_csvfiles_orders As String() = csvfiles_orders.ToArray
            Dim lc_csvfiles_orders As String = lo_datasecurity_encryptor.Encrypt(lo_csvfiles_orders, lo_csvfiles_orders.GetType)

            ' Roep hier de webservice aan met de gegevens
            WSMultiOrderImport.ImportOrders(session_id, cust_id, user_id, lc_csvfiles_orders)

        Else
            context.Response.ContentType = "text/html"
            context.Response.Write("")
        End If
    End Sub


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class

