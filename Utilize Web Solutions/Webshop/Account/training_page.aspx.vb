﻿
Partial Class Webshop_Account_training_page
    Inherits CMSPageTemplate
    Private training_id As String = ""
    Private dt_child_blocks As System.Data.DataTable = Nothing

    Private Sub Webshop_Account_training_page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("/home.aspx")
        End If

        Dim prod_code As String = Utilize.Data.DataProcedures.GetValue("uws_training", "prod_code", training_id)

        Dim qsc_so_orders As New Utilize.Data.QuerySelectClass
        qsc_so_orders.select_fields = "prod_code"
        qsc_so_orders.select_from = "uws_so_order_lines"
        qsc_so_orders.select_where = "uws_so_order_lines.prod_code = @prod_code and uws_so_orders.cust_id = @cust_id"
        qsc_so_orders.add_join("left outer join", "uws_so_orders", "uws_so_order_lines.hdr_id = uws_so_orders.ord_nr")
        qsc_so_orders.add_parameter("prod_code", prod_code)
        qsc_so_orders.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        Dim dt_so_orders As System.Data.DataTable = qsc_so_orders.execute_query()

        Dim qsc_history As New Utilize.Data.QuerySelectClass
        qsc_history.select_fields = "prod_code"
        qsc_history.select_from = "uws_history_lines"
        qsc_history.select_where = "uws_history_lines.prod_code = @prod_code and uws_history.cust_id = @cust_id"
        qsc_history.add_join("left outer join", "uws_history", "uws_history_lines.hdr_id = uws_history.crec_id")
        qsc_history.add_parameter("prod_code", prod_code)
        qsc_history.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        Dim dt_history As System.Data.DataTable = qsc_history.execute_query()

        If dt_history.Rows.Count = 0 And dt_so_orders.Rows.Count = 0 And Not prod_code = "" Then
            Response.Redirect("/home.aspx")
        End If

        Dim qsc_blocks As New Utilize.Data.QuerySelectClass
        qsc_blocks.select_fields = "*"
        qsc_blocks.select_from = "uws_training_blocks"
        qsc_blocks.select_where = "training_id = @training_id"
        qsc_blocks.select_where += " And parent_row = ''"
        qsc_blocks.select_order = "row_ord"
        qsc_blocks.add_parameter("training_id", training_id)
        Dim dt_blocks As System.Data.DataTable = qsc_blocks.execute_query()

        Dim qsc_child_blocks As New Utilize.Data.QuerySelectClass
        qsc_child_blocks.select_fields = "*"
        qsc_child_blocks.select_from = "uws_training_blocks"
        qsc_child_blocks.select_where = "training_id = @training_id"
        qsc_child_blocks.select_where += " and parent_row <> ''"
        qsc_child_blocks.select_order = "row_ord"
        qsc_child_blocks.add_parameter("training_id", training_id)
        dt_child_blocks = qsc_child_blocks.execute_query()

        If dt_blocks.Rows.Count > 0 Then
            Dim ph_content_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

            For Each row As System.Data.DataRow In dt_blocks.Rows
                add_structure_block(ph_content_zone, row)
            Next

        End If

    End Sub

    Private Sub add_structure_block(parent_control As Control, current_row As System.Data.DataRow)
        Dim lc_block_type As String = current_row.Item("block_type")
        Select Case lc_block_type
            Case "TEXT"
                Dim lc_block_id As String = current_row.Item("text_block_id")

                Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Text/uc_text_block.ascx")
                uc_block = create_control(lc_block_type, uc_block)


                uc_block.set_property("block_hide_xs", current_row.Item("block_hide_xs"))
                uc_block.set_property("block_hide_sm", current_row.Item("block_hide_sm"))
                uc_block.set_property("block_hide_md", current_row.Item("block_hide_md"))
                uc_block.set_property("block_hide_lg", current_row.Item("block_hide_lg"))

                uc_block.set_property("block_id", lc_block_id)
                uc_block.set_property("object_code", "ucm_text")
                uc_block.set_property("column_size", CInt(current_row.Item("block_layout")))

                parent_control.Controls.Add(uc_block)
            Case "BANNER"
                Dim lc_block_id As String = current_row.Item("banner_block_id")

                Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Banners/uc_banner_block.ascx")
                uc_block = create_control(lc_block_type, uc_block)

                uc_block.set_property("block_hide_xs", current_row.Item("block_hide_xs"))
                uc_block.set_property("block_hide_sm", current_row.Item("block_hide_sm"))
                uc_block.set_property("block_hide_md", current_row.Item("block_hide_md"))
                uc_block.set_property("block_hide_lg", current_row.Item("block_hide_lg"))

                uc_block.set_property("block_id", lc_block_id)
                uc_block.set_property("object_code", "ucm_banners")
                uc_block.set_property("column_size", CInt(current_row.Item("block_layout")))

                ' Haal de item layout op uit het banner block
                Dim ln_item_layout As Integer = Utilize.Data.DataProcedures.GetValue("ucm_banner_blocks", lc_block_id, "item_layout")

                ' Zet de item layout
                If ln_item_layout > 0 Then
                    uc_block.set_property("item_size_int", ln_item_layout)
                End If

                parent_control.Controls.Add(uc_block)
            Case "YOUTUBE"
                Dim lc_block_id As String = current_row.Item("youtube_block_id")

                Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/YoutubeVideo/uc_youtube_video_block.ascx")
                uc_block = create_control(lc_block_type, uc_block)

                uc_block.set_property("block_hide_xs", current_row.Item("block_hide_xs"))
                uc_block.set_property("block_hide_sm", current_row.Item("block_hide_sm"))
                uc_block.set_property("block_hide_md", current_row.Item("block_hide_md"))
                uc_block.set_property("block_hide_lg", current_row.Item("block_hide_lg"))

                uc_block.set_property("block_id", lc_block_id)
                uc_block.set_property("object_code", "ucm_youtube_video")
                uc_block.set_property("column_size", CInt(current_row.Item("block_layout")))

                If current_row.Item("item_layout") > 0 Then
                    uc_block.set_property("item_size_int", CInt(current_row.Item("item_layout")))
                End If

                parent_control.Controls.Add(uc_block)
            Case "DOCUMENT"
                Dim lc_block_id As String = current_row.Item("doc_block_id")

                Dim uc_block As Base.base_usercontrol_base = loadcontrol("/CMS/Modules/Documents/uc_document_block.ascx")
                uc_block = create_control(lc_block_type, uc_block)

                uc_block.set_property("block_hide_xs", current_row.Item("block_hide_xs"))
                uc_block.set_property("block_hide_sm", current_row.Item("block_hide_sm"))
                uc_block.set_property("block_hide_md", current_row.Item("block_hide_md"))
                uc_block.set_property("block_hide_lg", current_row.Item("block_hide_lg"))

                uc_block.set_property("block_id", lc_block_id)

                Dim ln_item_count As Integer = Utilize.Data.DataProcedures.GetValue("ucm_document_blocks", "item_count", lc_block_id)
                uc_block.set_property("item_count", ln_item_count)
                uc_block.set_property("column_size", CInt(current_row.Item("block_layout")))

                parent_control.Controls.Add(uc_block)
            Case "PROD_SELECTION"
                Dim lc_block_id As String = current_row.Item("prod_selection_id")

                Dim uc_block As Base.base_usercontrol_base = loadcontrol("/Webshop/Modules/ProductSelection/uc_product_selection.ascx")
                uc_block = create_control(lc_block_type, uc_block)

                uc_block.set_property("block_hide_xs", current_row.Item("block_hide_xs"))
                uc_block.set_property("block_hide_sm", current_row.Item("block_hide_sm"))
                uc_block.set_property("block_hide_md", current_row.Item("block_hide_md"))
                uc_block.set_property("block_hide_lg", current_row.Item("block_hide_lg"))

                uc_block.set_property("block_id", lc_block_id)
                uc_block.set_property("object_code", "ucm_text")
                uc_block.set_property("column_size", CInt(current_row.Item("block_layout")))

                If current_row.Item("item_layout") > 0 Then
                    uc_block.set_property("item_size_int", CInt(current_row.Item("item_layout")))
                End If

                Dim ln_item_count As Integer = Utilize.Data.DataProcedures.GetValue("uws_prod_selection", "PROD_SHOW_NR", lc_block_id)
                uc_block.set_property("item_count", ln_item_count)

                parent_control.Controls.Add(uc_block)
            Case "ROW"
                Dim hgc_row As New HtmlGenericControl("div")
                Dim lc_class As String = "row"

                If current_row.Item("block_hide_xs") Then
                    lc_class += " hidden-xs"
                End If

                If current_row.Item("block_hide_sm") Then
                    lc_class += " hidden-sm"
                End If

                If current_row.Item("block_hide_md") Then
                    lc_class += " hidden-md"
                End If

                If current_row.Item("block_hide_lg") Then
                    lc_class += " hidden-lg"
                End If

                hgc_row.Attributes.Add("class", lc_class)

                Dim child_blocks() As System.Data.DataRow = dt_child_blocks.Select("parent_row = '" + current_row.Item("rec_id") + "'")

                If child_blocks.Length > 0 Then
                    For Each row As System.Data.DataRow In child_blocks
                        add_structure_block(hgc_row, row)
                    Next
                End If
                parent_control.Controls.Add(hgc_row)
        End Select
    End Sub

    Private Sub Webshop_Account_training_page_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        If Not Request.Params("RecId") Is Nothing Then
            training_id = Request.Params("RecId")
        Else
            Response.Redirect("~/home.aspx")
        End If

        Me.cms_template_code = Utilize.Data.DataProcedures.GetValue("uws_training", "template_code", training_id)

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("uws_training", "title_" + Me.global_ws.Language, training_id)
    End Sub
End Class
