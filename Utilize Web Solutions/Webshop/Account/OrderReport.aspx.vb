﻿
Partial Class OrderReport
    Inherits Base.base_page_base

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim uc_order_report As Webshop.ws_user_control = Me.LoadUserControl("~/Webshop/Account/AccountBlocks/uc_order_report.ascx")
        Me.ph_order_report.Controls.Add(uc_order_report)

        Me.Page.Title = global_trans.translate_title("title_order_overview", "Bestellingen overzicht", Me.global_ws.Language)
    End Sub
End Class
