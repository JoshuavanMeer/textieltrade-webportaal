﻿
Imports System.IO
Imports System.Web.UI.WebControls
Imports Utilize.Data.API.Webshop
Imports System.Linq
Imports System.Threading
Imports System.Globalization

Partial Class uc_order_history
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private txt_year_extender As New AjaxControlToolkit.MaskedEditExtender
    Private txt_month_extender As New AjaxControlToolkit.MaskedEditExtender
    Private txt_day_extender As New AjaxControlToolkit.MaskedEditExtender

    Private price_type As Integer = 0
    Public prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
    Public on_demand_sync As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync")
    Public hide_pdf As Boolean = Me.global_ws.get_webshop_setting("hide_pdf")

    Private current_orders As DataTransferObjects.SalesHistoryDto()

    Public Sub New()
        txt_year_extender.Mask = "9999"
        txt_month_extender.Mask = "99"
        txt_day_extender.Mask = "99"
    End Sub

    Private Function get_data_source_from_boc(Optional remove_filter As Boolean = False) As System.Data.DataTable
        Dim externalNumber As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")
        Dim qsc_history_structure As New Utilize.Data.QuerySelectClass
        With qsc_history_structure
            .select_fields = "top 0 *"
            .select_from = "uws_history"
        End With
        Dim dt_return_value As System.Data.DataTable = qsc_history_structure.execute_query
        dt_return_value.Columns.Add("has_backorder_lines", GetType(Boolean))

        If Not String.IsNullOrEmpty(externalNumber) Then
            If remove_filter Then
                Me.button_remove_filter.Visible = False
            End If

            getHistoryHeaders(externalNumber, remove_filter)

            If Not current_orders Is Nothing Then
                If Not String.IsNullOrEmpty(Request.Form(Me.txt_ord_nr.UniqueID)) And Not remove_filter Then
                    ' Als een ordernummer is ingevuld
                    current_orders = current_orders.Where(Function(x) x.Id.StartsWith(Request.Form(Me.txt_ord_nr.UniqueID))).ToArray

                    Me.button_remove_filter.Visible = True
                End If

                If Not String.IsNullOrEmpty(Request.Form(Me.txt_cust_ref.UniqueID)) And Not remove_filter Then
                    ' Als een klant referentie is ingevuld
                    current_orders = current_orders.Where(Function(x) x.Reference.Contains(Request.Form(Me.txt_cust_ref.UniqueID))).ToArray

                    Me.button_remove_filter.Visible = True
                End If

                For Each element In current_orders
                    Dim newRow As System.Data.DataRow = dt_return_value.NewRow()
                    newRow.Item("crec_id") = element.Id
                    newRow.Item("trn_type") = 1
                    newRow.Item("doc_nr") = element.Id
                    newRow.Item("cust_id") = Me.global_ws.user_information.user_customer_id
                    newRow.Item("ord_desc") = element.Reference
                    newRow.Item("ord_date") = element.OrderDate
                    newRow.Item("vat_ship") = 0
                    newRow.Item("ord_cost") = element.OrderCosts
                    newRow.Item("ord_tot") = element.OrderAmount
                    newRow.Item("vat_tot") = element.OrderVatAmount
                    newRow.Item("has_backorder_lines") = element.HasBackorderLines

                    dt_return_value.Rows.Add(newRow)
                Next
            End If
        End If

        Return dt_return_value
    End Function

    Private Sub getHistoryHeaders(externalNumber As String, Optional remove_filter As Boolean = False)
        Dim ld_from As DateTime = DateTime.Now.AddYears(-1)
        Dim ld_to As DateTime = DateTime.Now

        If Not String.IsNullOrEmpty(Request.Form(Me.txt_from_date.UniqueID)) And Not remove_filter Then
            ld_from = DateTime.ParseExact(Request.Form(Me.txt_from_date.UniqueID), "yyyy-MM-dd", CultureInfo.InvariantCulture)
            Me.button_remove_filter.Visible = True
        End If

        If Not String.IsNullOrEmpty(Request.Form(Me.txt_to_date.UniqueID)) And Not remove_filter Then
            ld_to = DateTime.ParseExact(Request.Form(Me.txt_to_date.UniqueID), "yyyy-MM-dd", CultureInfo.InvariantCulture)
            Me.button_remove_filter.Visible = True
        End If

        Dim lo_intermediate_service = IntermediateServiceFactory.GetIntermediateConnector

        current_orders = lo_intermediate_service.GetHistoryHeaders(externalNumber, ld_from, ld_to)
    End Sub

    Private Function get_data_source_history(Optional ByVal remove_filter As Boolean = False) As System.Data.DataTable
        ' Hier gaan we de historie ophalen
        Dim qsc_order_history As New Utilize.Data.QuerySelectClass
        qsc_order_history.select_fields = "uws_history.*"
        qsc_order_history.select_from = "uws_history"
        qsc_order_history.select_order = "uws_history.doc_nr desc"
        qsc_order_history.select_where = "uws_history.cust_id = @cust_id and trn_type = 1"

        qsc_order_history.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

        If Me.IsPostBack() And Not remove_filter Then
            Me.button_remove_filter.Visible = False

            If txt_year.Text.Contains("_") Then
                txt_year.Text = ""
            End If

            If txt_month.Text.Contains("_") Then
                txt_month.Text = ""
            End If

            If txt_day.Text.Contains("_") Then
                txt_day.Text = ""
            End If

            '''''''''''''''''''''''''''''''''''''''''''
            ' Regel hieronder de zoeken functionaliteit
            '''''''''''''''''''''''''''''''''''''''''''
            If Not String.IsNullOrEmpty(Request.Form(Me.txt_ord_nr.UniqueID)) Then
                ' Als een ordernummer is ingevuld
                qsc_order_history.select_where += " and doc_nr like @doc_nr + '%'"
                qsc_order_history.add_parameter("doc_nr", Request.Form(Me.txt_ord_nr.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_cust_ref.UniqueID)) Then
                ' Als een klant referentie is ingevuld
                qsc_order_history.select_where += " and ord_desc like '%' + @ord_desc + '%'"
                qsc_order_history.add_parameter("ord_desc", Request.Form(Me.txt_cust_ref.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_prod_code.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_order_history.select_where += " and crec_id in (select hdr_id from uws_history_lines where prod_code like @prod_code + '%')"
                qsc_order_history.add_parameter("prod_code", Request.Form(Me.txt_prod_code.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_prod_desc.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_order_history.select_where += " and crec_id in (select hdr_id from uws_history_lines where prod_desc like '%' + @prod_desc + '%')"
                qsc_order_history.add_parameter("prod_desc", Request.Form(Me.txt_prod_desc.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_year.UniqueID)) Then
                qsc_order_history.select_where += " and YEAR(ord_date) = @year"
                qsc_order_history.add_parameter("year", Request.Form(Me.txt_year.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_month.UniqueID)) Then
                qsc_order_history.select_where += " and MONTH(ord_date) = @month"
                qsc_order_history.add_parameter("month", Request.Form(Me.txt_month.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_day.UniqueID)) Then
                qsc_order_history.select_where += " and DAY(ord_date) = @day"
                qsc_order_history.add_parameter("day", Request.Form(Me.txt_day.UniqueID).Trim())

                Me.button_remove_filter.Visible = True
            End If
        End If

        Dim dt_return As System.Data.DataTable = qsc_order_history.execute_query()
        Dim dc_backorder As New System.Data.DataColumn("has_backorder_lines", GetType(Boolean))
        dc_backorder.DefaultValue = False
        dt_return.Columns.Add(dc_backorder)

        Return dt_return
    End Function
    Private Function get_data_source_deliveries(Optional ByVal remove_filter As Boolean = False) As System.Data.DataTable
        ' Hier gaan we de historie ophalen
        Dim qsc_order_history As New Utilize.Data.QuerySelectClass
        qsc_order_history.select_fields = "uws_history.*"
        qsc_order_history.select_from = "uws_history"
        qsc_order_history.select_order = "uws_history.doc_nr desc"
        qsc_order_history.select_where = "uws_history.cust_id = @cust_id and trn_type = 9 "

        qsc_order_history.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

        Return qsc_order_history.execute_query()
    End Function
    Private Function get_order_lines(ByVal hdr_id As String) As System.Data.DataTable
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
            Dim externalNumber As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")
            Dim lo_intermediate_service = IntermediateServiceFactory.GetIntermediateConnector

            Dim details = lo_intermediate_service.GetHistoryDetails(externalNumber, hdr_id)

            Dim qsc_order_history_line As New Utilize.Data.QuerySelectClass
            qsc_order_history_line.select_fields = "top 0 *"
            qsc_order_history_line.select_from = "uws_history_lines"
            Dim dt_return As System.Data.DataTable = qsc_order_history_line.execute_query()
            dt_return.Columns.Add("is_backorder", GetType(Boolean))

            If Not details Is Nothing Then
                For Each element In details
                    Dim newRow As System.Data.DataRow = dt_return.NewRow()
                    newRow.Item("crec_id") = Guid.NewGuid.ToString("n")
                    newRow.Item("hdr_id") = hdr_id
                    newRow.Item("prod_code") = element.ProductCode
                    newRow.Item("ord_qty") = element.Quantity
                    newRow.Item("row_amt") = element.NettAmount
                    newRow.Item("vat_amt") = element.NettVatAmount
                    newRow.Item("is_backorder") = element.IsBackorder

                    dt_return.Rows.Add(newRow)
                Next
            End If

            Return dt_return
        Else
            Dim qsc_order_history_line As New Utilize.Data.QuerySelectClass
            qsc_order_history_line.select_fields = "*"
            qsc_order_history_line.select_from = "uws_history_lines"
            qsc_order_history_line.select_where = "hdr_id = @hdr_id"
            qsc_order_history_line.add_parameter("hdr_id", hdr_id)

            Dim dt_return As System.Data.DataTable = qsc_order_history_line.execute_query
            Dim dc_backorder As New System.Data.DataColumn("is_backorder", GetType(Boolean))
            dc_backorder.DefaultValue = False
            dt_return.Columns.Add(dc_backorder)

            Return dt_return
        End If
    End Function

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.price_type = Me.global_ws.get_webshop_setting("price_type")

        Me.hl_tab_orders.NavigateUrl = "#" + Me.pnl_orders.ClientID
        Me.hl_tab_deliveries.NavigateUrl = "#" + Me.pnl_deliveries.ClientID

        Me.hl_tab_orders.Attributes.Add("onclick", "$('#" + Me.hih_active_tab.ClientID + "').val('pnl_orders');")
        Me.hl_tab_deliveries.Attributes.Add("onclick", "$('#" + Me.hih_active_tab.ClientID + "').val('pnl_deliveries');")

        ' Hier gaan we zorgen dat er alleen maar numerieke waarden ingevuld kunnen worden
        txt_year_extender.ID = "txt_year_extender"
        txt_month_extender.ID = "txt_month_extender"
        txt_day_extender.ID = "txt_day_extender"

        Me.Controls.Add(txt_year_extender)
        Me.Controls.Add(txt_month_extender)
        Me.Controls.Add(txt_day_extender)

        txt_year_extender.TargetControlID = txt_year.ID
        txt_month_extender.TargetControlID = txt_month.ID
        txt_day_extender.TargetControlID = txt_day.ID

        If Not Me.IsPostBack() Then
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
                Me.rpt_orders.DataSource = get_data_source_from_boc()
                Me.rpt_orders.DataBind()

                Me.ph_intermediate.Visible = True

                Dim task As System.Threading.Tasks.Task = Threading.Tasks.Task.Factory.StartNew(Sub() synchronizeHistory(current_orders, Me.global_ws.user_information.user_customer_id, Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")))
            Else
                Me.rpt_orders.DataSource = get_data_source_history()
                Me.rpt_orders.DataBind()

                Me.rpt_deliveries.DataSource = Me.get_data_source_deliveries()
                Me.rpt_deliveries.DataBind()

                Me.ph_normal.Visible = True
            End If
        End If

        Me.set_style_sheet("uc_order_history.css", Me)
        Me.block_css = False

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
    End Sub

    Private Sub synchronizeHistory(historyItems As DataTransferObjects.SalesHistoryDto(), customerId As String, externalId As String)

        If historyItems Is Nothing Then
            Return
        End If

        Dim ubo_history As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_history")

        Dim lo_intermediate_service = IntermediateServiceFactory.GetIntermediateConnector

        For Each item In historyItems
            Dim lc_id As String = Guid.NewGuid.ToString("n").Substring(0, 20)
            If Not ubo_history.table_locate("uws_history", "doc_nr = '" + item.Id + "'") Then
                ubo_history.table_insert_row("uws_history", customerId)
                ubo_history.field_set("uws_history.crec_id", lc_id)
            End If

            If item.OrderAmount <> ubo_history.field_get("ord_tot") Then
                ubo_history.field_set("uws_history.trn_type", 1)
                ubo_history.field_set("uws_history.doc_nr", item.Id)
                ubo_history.field_set("uws_history.cust_id", customerId)
                ubo_history.field_set("uws_history.ord_desc", item.Reference)
                ubo_history.field_set("uws_history.ord_date", item.OrderDate)
                ubo_history.field_set("uws_history.vat_ship", 0)
                ubo_history.field_set("uws_history.vat_ord", 0)
                ubo_history.field_set("uws_history.ship_cost", 0)
                ubo_history.field_set("uws_history.ord_cost", item.OrderCosts)
                ubo_history.field_set("uws_history.ord_tot", item.OrderAmount)
                ubo_history.field_set("uws_history.vat_tot", item.OrderVatAmount)

                ubo_history.table_update(customerId)

                Dim details = lo_intermediate_service.GetHistoryDetails(externalId, item.Id)

                If details Is Nothing Then
                    Continue For
                End If

                For Each line In details
                    Dim lc_line_id = lc_id.Substring(0, 12) + line.RowId
                    If Not ubo_history.table_locate("uws_history_lines", "crec_id = '" + lc_line_id + "'") Then
                        ubo_history.table_insert_row("uws_history_lines", customerId)
                        ubo_history.field_set("uws_history_lines.crec_id", lc_line_id)
                    End If

                    ubo_history.field_set("uws_history_lines.hdr_id", lc_id)
                    ubo_history.field_set("uws_history_lines.prod_code", line.ProductCode)
                    ubo_history.field_set("uws_history_lines.prod_desc", line.ProductDescription)
                    ubo_history.field_set("uws_history_lines.ord_qty", line.Quantity)
                    ubo_history.field_set("uws_history_lines.row_amt", line.GrossAmount)
                    ubo_history.field_set("uws_history_lines.vat_amt", line.GrossVatAmount)

                    ubo_history.table_update(customerId)
                Next
            End If
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Me.ph_orders.Visible = Me.rpt_orders.Items.Count > 0
            Me.ph_no_orders.Visible = Me.rpt_orders.Items.Count = 0

            Me.ph_deliveries.Visible = Me.rpt_deliveries.Items.Count > 0
            Me.ph_no_deliveries.Visible = Me.rpt_deliveries.Items.Count = 0
        End If

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
            Me.rpt_orders.Controls(0).FindControl("ph_pdf").Visible = False
        End If

        ' Als de module "Leveringen" niet aan staat, dan moeten verschillende onderdelen onzichtbaar gemaakt worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_history_del") = False Then
            Me.ph_tabs.Visible = False
            Me.ph_deliveries.Visible = False
            Me.ph_no_deliveries.Visible = False
        End If

        ' Set the active tab and panel on postback
        Select Case hih_active_tab.Value
            Case "pnl_orders"
                Me.li_orders.Attributes.Add("class", "active")
                Me.li_deliveries.Attributes.Remove("class")

                Me.pnl_orders.CssClass = "tab-pane active"
                Me.pnl_deliveries.CssClass = "tab-pane"
            Case "pnl_deliveries"
                Me.li_deliveries.Attributes.Add("class", "active")
                Me.li_orders.Attributes.Remove("class")

                Me.pnl_orders.CssClass = "tab-pane"
                Me.pnl_deliveries.CssClass = "tab-pane active"
            Case Else
                Exit Select
        End Select

        ' Maak de tekst van de button van de leveringen gelijk aan die van de bestellingen
        Me.button_close_delivery_details.Text = Me.button_close_details.Text

        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        Me.ph_order_costs_excl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")
        Me.ph_order_costs_incl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")

        ' Haal hier uit de webshop instellingen op of de prijzen inclusief of exclusief BTW zijn.
        Me.ph_prices_vat_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1 And Not prices_not_visible
        Me.ph_prices_vat_incl.Visible = Me.global_ws.get_webshop_setting("price_type") = 2 And Not prices_not_visible
    End Sub

    Protected Sub rpt_orders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_orders.ItemCommand
        If e.CommandName = "return" Then
            ' Aanpassing voor retouren aanvraag om direct een retouraanvraag te kunnen indienen.
            Dim lc_rec_id As String = e.CommandArgument
            Response.Redirect("~/" + Me.global_ws.Language + "/Account/ReturnRequest.aspx?order=" & lc_rec_id, True)
        ElseIf e.CommandName = "copy" Then
            Dim ll_resume As Boolean = True
            Dim lc_rec_id As String = e.CommandArgument
            Dim dt_data_table As System.Data.DataTable = Me.get_order_lines(lc_rec_id)

            If ll_resume Then
                ll_resume = False

                ' Loop door de winkelwagen regels heen om de producten toe te voegen aan de winkelwagen
                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_show", dr_data_row.Item("prod_code")) = True Then
                        If Me.global_ws.get_webshop_setting("stock_limit") Then
                            Dim ln_stock_quantity As Decimal = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_stock", dr_data_row.Item("prod_code"))
                            ll_resume = ln_stock_quantity >= 1

                            If ll_resume Then
                                Exit For
                            End If
                        Else
                            ll_resume = True
                            Exit For
                        End If
                    End If
                Next

                If Not ll_resume Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_lines_added", "alert('" + global_trans.translate_message("message_history_products_not_available", "Geen van de producten kunnen aan de winkelwagen worden toegevoegd.", Me.global_ws.Language) + "');", True)
                End If
            End If

            If ll_resume Then
                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_show", dr_data_row.Item("prod_code")) = True Then
                        Me.global_ws.shopping_cart.product_add(dr_data_row.Item("prod_code"), 1, "", "", "")
                    End If
                Next

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_lines_added", "alert('" + global_trans.translate_message("message_history_products_added", "De producten zijn toegevoegd aan uw winkelwagen.", Me.global_ws.Language) + "'); window.location='" + Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/paymentprocess/shopping_cart.aspx';", True)
            End If
        End If

        If e.CommandName = "details" Then
            Dim lc_rec_id As String = e.CommandArgument

            Me.rpt_order_details.DataSource = Me.get_order_lines(lc_rec_id)
            Me.rpt_order_details.DataBind()

            Dim orderDetails As OrderHistoryDetails = getOrderDetails(lc_rec_id)

            Me.lbl_order_date.Text = Format(orderDetails.ord_date, "d")
            Me.lbl_order_number.Text = orderDetails.doc_nr
            Me.lbl_order_description.Text = orderDetails.ord_desc

            Me.lt_order_subtotal_excl_vat.Text = Format(orderDetails.ord_tot - orderDetails.ship_cost - orderDetails.ord_cost, "c")
            Me.lt_order_subtotal_incl_vat.Text = Format(orderDetails.ord_tot - orderDetails.ship_cost - orderDetails.vat_ship - orderDetails.ord_cost - orderDetails.vat_ord, "c")

            Me.lt_order_shipping_costs_excl_vat.Text = Format(orderDetails.ship_cost, "c")
            Me.lt_order_shipping_costs_incl_vat.Text = Format(orderDetails.ship_cost + orderDetails.vat_ship, "c")

            Me.lt_order_costs_excl_vat.Text = Format(orderDetails.ord_cost, "c")
            Me.lt_order_costs_incl_vat.Text = Format(orderDetails.ord_cost + orderDetails.vat_ord, "c")

            Me.lt_order_vat_total.Text = Format(orderDetails.vat_tot, "c")
            Me.lt_order_total_incl_vat.Text = Format(orderDetails.ord_tot + orderDetails.vat_tot, "c")
            Me.lt_order_total_incl_vat1.Text = Format(orderDetails.ord_tot + orderDetails.vat_tot, "c")

            Me.ph_orders.Visible = False
            Me.ph_order_details.Visible = True
        End If
    End Sub

    Private Function getOrderDetails(hdrId As String) As OrderHistoryDetails
        Dim returnObj As New OrderHistoryDetails
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
            Dim lo_intermediate_service = IntermediateServiceFactory.GetIntermediateConnector
            Dim externalNumber As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")

            Dim currentOrder = lo_intermediate_service.GetHistoryDetailsById(externalNumber, hdrId)

            returnObj.doc_nr = hdrId
            returnObj.ord_cost = currentOrder.OrderCosts
            returnObj.ord_date = currentOrder.OrderDate
            returnObj.ord_desc = currentOrder.Reference
            returnObj.ord_tot = currentOrder.OrderAmount
            returnObj.ship_cost = 0
            returnObj.vat_tot = currentOrder.OrderVatAmount
            returnObj.vat_ship = 0
        Else
            Dim udt_data_Table As New Utilize.Data.DataTable
            udt_data_Table.table_name = "uws_history"
            udt_data_Table.table_init()
            udt_data_Table.add_where("and crec_id = @crec_id")
            udt_data_Table.add_where_parameter("crec_id", hdrId)
            udt_data_Table.table_load()

            returnObj.doc_nr = udt_data_Table.field_get("doc_nr")
            returnObj.ord_cost = udt_data_Table.field_get("ord_cost")
            returnObj.ord_date = udt_data_Table.field_get("ord_date")
            returnObj.ord_desc = udt_data_Table.field_get("ord_desc")
            returnObj.ord_tot = udt_data_Table.field_get("ord_tot")
            returnObj.ship_cost = udt_data_Table.field_get("ship_cost")
            returnObj.vat_ord = udt_data_Table.field_get("vat_ord")
            returnObj.vat_ship = udt_data_Table.field_get("vat_ship")
            returnObj.vat_tot = udt_data_Table.field_get("vat_tot")
        End If

        Return returnObj
    End Function

    Protected Sub rpt_deliveries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_deliveries.ItemCommand
        If e.CommandName = "copy" Then
            Dim ll_resume As Boolean = True
            Dim lc_rec_id As String = e.CommandArgument
            Dim dt_data_table As System.Data.DataTable = Me.get_order_lines(lc_rec_id)

            If ll_resume Then
                ll_resume = False

                ' Loop door de winkelwagen regels heen om de producten toe te voegen aan de winkelwagen
                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_show", dr_data_row.Item("prod_code")) = True Then
                        If Me.global_ws.get_webshop_setting("stock_limit") Then
                            Dim ln_stock_quantity As Decimal = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_stock", dr_data_row.Item("prod_code"))
                            ll_resume = ln_stock_quantity >= 1

                            If ll_resume Then
                                Exit For
                            End If
                        Else
                            ll_resume = True
                            Exit For
                        End If
                    End If
                Next

                If Not ll_resume Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_lines_added", "alert('" + global_trans.translate_message("message_history_products_not_available", "Geen van de producten kunnen aan de winkelwagen worden toegevoegd.", Me.global_ws.Language) + "');", True)
                End If
            End If

            If ll_resume Then
                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_show", dr_data_row.Item("prod_code")) = True Then
                        Me.global_ws.shopping_cart.product_add(dr_data_row.Item("prod_code"), 1, "", "", "")
                    End If
                Next

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_lines_added", "alert('" + global_trans.translate_message("message_history_products_added", "De producten zijn toegevoegd aan uw winkelwagen.", Me.global_ws.Language) + "'); window.location='" + Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/paymentprocess/shopping_cart.aspx';", True)
            End If
        End If

        If e.CommandName = "details" Then
            Dim lc_rec_id As String = e.CommandArgument

            Me.rpt_delivery_details.DataSource = Me.get_order_lines(lc_rec_id)
            Me.rpt_delivery_details.DataBind()

            Dim udt_data_Table As New Utilize.Data.DataTable
            udt_data_Table.table_name = "uws_history"
            udt_data_Table.table_init()
            udt_data_Table.add_where("and crec_id = @crec_id")
            udt_data_Table.add_where_parameter("crec_id", lc_rec_id)
            udt_data_Table.table_load()

            If udt_data_Table.data_row_count > 0 Then
                Me.lbl_delivery_date.Text = Format(udt_data_Table.field_get("ord_date"), "d")
                Me.lbl_delivery_number.Text = udt_data_Table.field_get("doc_nr")
                Me.lbl_delivery_description.Text = udt_data_Table.field_get("ord_desc")

                Me.ph_deliveries.Visible = False
                Me.ph_delivery_details.Visible = True
            End If
        End If
    End Sub

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.ph_order_details.Visible = False
        Me.ph_orders.Visible = True
    End Sub
    Private Sub button_close_delivery_details_Click(sender As Object, e As EventArgs) Handles button_close_delivery_details.Click
        Me.ph_delivery_details.Visible = False
        Me.ph_deliveries.Visible = True
    End Sub

    Protected Sub button_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search.Click
        ' Zowieso moet het panel zichtbaar zijn als er gezocht gaat worden
        Me.ph_search_history.Visible = True
        Me.ph_search_button.Visible = False
        Me.txt_ord_nr.Focus()
    End Sub
    Protected Sub button_remove_filter_Click(sender As Object, e As EventArgs) Handles button_remove_filter.Click
        Me.txt_cust_ref.Text = ""
        Me.txt_ord_nr.Text = ""
        Me.txt_prod_code.Text = ""
        Me.txt_prod_desc.Text = ""

        Me.txt_day.Text = ""
        Me.txt_month.Text = ""
        Me.txt_year.Text = "'"

        Me.txt_from_date.Text = ""
        Me.txt_to_date.Text = ""

        Me.button_remove_filter.Visible = False

        Me.rpt_orders.page_nr = 0
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
            Me.rpt_orders.DataSource = get_data_source_from_boc(True)
            Me.rpt_orders.DataBind()
        Else
            Me.rpt_orders.DataSource = Me.get_data_source_history(True)
            Me.rpt_orders.DataBind()
        End If

        Me.txt_ord_nr.Focus()
    End Sub

    Protected Sub button_filter_Click(sender As Object, e As EventArgs) Handles button_filter.Click
        ' Als het zoekpanel zichtbaar is, dan moet de zoekopdracht uitgevoerd worden
        If Me.ph_search_history.Visible Then
            Me.rpt_orders.page_nr = 0

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync") Then
                Me.rpt_orders.DataSource = get_data_source_from_boc()
                Me.rpt_orders.DataBind()
            Else
                Me.rpt_orders.DataSource = Me.get_data_source_history()
                Me.rpt_orders.DataBind()
            End If

            Me.txt_ord_nr.Focus()
        End If
    End Sub

    Private Sub rpt_orders_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_orders.ItemCreated
        Dim ph_return_request As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_return_request")

        If Not ph_return_request Is Nothing Then
            ph_return_request.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_rma")
        End If

        Dim ph_backorders As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_backorders")

        If Not ph_backorders Is Nothing Then
            ph_backorders.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync")
        End If
    End Sub

    Private Sub rpt_delivery_details_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_delivery_details.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Try
                ' Decimalen
                Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

                '<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%>
                Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")
                If lt_ord_qty IsNot Nothing Then
                    lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub rpt_order_details_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_order_details.ItemCreated
        Try
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")

            If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                If Me.price_type = 1 Then
                    lt_literal.Text = Format(e.Item.DataItem("row_amt"), "c")
                Else
                    lt_literal.Text = Format((e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt")), "c")
                End If
            Else
                If Me.price_type = 1 Then
                    lt_literal.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt"))
                Else
                    lt_literal.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt"))
                End If
            End If

        Catch ex As Exception

        End Try

        Try
            ' Decimalen
            Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

            '<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%>
            Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")

            If lt_ord_qty IsNot Nothing Then
                lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
            End If
        Catch ex As Exception

        End Try

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        Dim ph_is_backorder As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_is_backorder")

        If Not ph_is_backorder Is Nothing Then
            ph_is_backorder.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync")
        End If
    End Sub

    Public Function CheckDocument(ByVal inv_nr As String) As Boolean
        Dim lc_doc_nr As String = Utilize.Data.DataProcedures.GetValue("uws_history", "doc_nr", inv_nr, "doc_nr")

        If Not String.IsNullOrEmpty(lc_doc_nr) Then
            Dim lo_byte As Byte() = get_invoice(lc_doc_nr)
            If Not lo_byte Is Nothing Then
                If lo_byte.Length > 4 Then
                    If Not Me.global_ws.get_webshop_setting("use_pdf_check") Then
                        Return True
                    End If
                    Return Utilize.Data.API.Webshop.PDFCheck.check_pdf_format(lo_byte)
                End If
            End If
        End If
        Return False
    End Function

    Private Function get_invoice(doc_nr As String) As Byte()
        Dim lb_return As Byte() = Nothing

        Try
            Dim udt_data_table As New Utilize.Data.DataTable
            udt_data_table.table_name = "uws_history"
            udt_data_table.table_init()

            udt_data_table.add_where("and doc_nr = @doc_nr")
            udt_data_table.add_where("and cust_id = @cust_id")

            udt_data_table.add_where_parameter("doc_nr", doc_nr)
            udt_data_table.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            udt_data_table.table_load()

            If udt_data_table.data_row_count > 0 Then
                lb_return = CType(udt_data_table.field_get("ord_document"), Byte())
            End If
        Catch ex As Exception

        End Try

        Return lb_return
    End Function

    Private Class OrderHistoryDetails
        Public ord_date As Date
        Public doc_nr As String
        Public ord_desc As String
        Public ord_tot As Decimal
        Public ship_cost As Decimal
        Public vat_ship As Decimal
        Public ord_cost As Decimal
        Public vat_ord As Decimal
        Public vat_tot As Decimal
    End Class
End Class
