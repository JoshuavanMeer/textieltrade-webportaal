﻿
Imports System.Web.UI.WebControls
Imports Utilize.Data.API.Webshop

Partial Class Webshop_Account_AccountBlocks_uc_openitems
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Function get_data_source() As System.Data.DataTable
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_openitems") Then
            Dim externalNumber As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")
            Dim qsc_structure As New Utilize.Data.QuerySelectClass
            With qsc_structure
                .select_fields = "top 0 *"
                .select_from = "uws_openitems"
            End With
            Dim dt_return_value As System.Data.DataTable = qsc_structure.execute_query

            If Not String.IsNullOrEmpty(externalNumber) Then
                Dim lo_intermediate_service = IntermediateServiceFactory.GetIntermediateConnector

                Dim openItems = lo_intermediate_service.GetOpenItems(externalNumber)

                If Not openItems Is Nothing Then
                    For Each item In openItems
                        Dim dr_line As System.Data.DataRow = dt_return_value.NewRow

                        dr_line.Item("inv_nr") = item.InvoiceId
                        dr_line.Item("inv_date") = item.InvoiceDate
                        dr_line.Item("inv_desc") = item.Reference
                        dr_line.Item("inv_amt") = item.InvoiceAmount
                        dr_line.Item("exp_date") = item.InvoiceDueDate
                        dr_line.Item("paid_amt") = item.InvoicePayedAmount
                        dr_line.Item("inv_key") = ""

                        dt_return_value.Rows.Add(dr_line)
                    Next
                End If
            End If

            Return dt_return_value
        Else
            Dim udt_orders As New Utilize.Data.DataTable
            udt_orders.table_name = "uws_openitems"
            udt_orders.table_init()

            udt_orders.add_where("and cust_id = @cust_id")
            udt_orders.add_where("and inv_amt <> paid_amt")
            udt_orders.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            udt_orders.select_order = "inv_nr desc"

            udt_orders.table_load()

            Return udt_orders.data_table
        End If
    End Function

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.rpt_open_items.DataSource = Me.get_data_source()
        Me.rpt_open_items.DataBind()

        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.set_style_sheet("uc_openitems.css", Me)
        Me.block_css = False

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ph_open_items.Visible = Me.rpt_open_items.Items.Count > 0
        Me.ph_no_open_items.Visible = Me.rpt_open_items.Items.Count = 0
    End Sub

    Public Function get_order_total(ByVal ld_amount As Decimal) As String
        If global_ws.price_mode = Webshop.price_mode.Credits Then
            Return global_ws.convert_amount_to_credits(ld_amount).ToString()
        Else
            Return Format(ld_amount, "c")
        End If
    End Function

    Private Sub rpt_open_items_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_open_items.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            If (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_openitems")) Then
                e.Item.FindControl("col_invoice_pdf").Visible = False
            End If
        End If

        If (e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem) Then
            If (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_openitems")) Then
                e.Item.FindControl("button_copy_invoice").Visible = False
            End If
        End If

    End Sub

    Public Function CheckDocument(inv_nr As String) As Boolean
        Dim lc_inv_data As Byte() = CType(Utilize.Data.DataProcedures.GetValue("uws_history", "ord_document", inv_nr, "doc_nr"), Byte())

        Return lc_inv_data IsNot Nothing AndAlso lc_inv_data.Length > 0
    End Function
End Class
