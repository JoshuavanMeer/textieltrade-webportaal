﻿
Imports System.Net.Mail

Partial Class uc_users
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Function get_data_source(Optional ByVal remove_filter As Boolean = False) As System.Data.DataTable
        Dim qsc_users As New Utilize.Data.QuerySelectClass
        qsc_users.select_fields = "*"
        qsc_users.select_from = "uws_customers_users"

        qsc_users.select_where = "hdr_id = @hdr_id and blocked = 0"
        qsc_users.add_parameter("hdr_id", Me.global_ws.user_information.user_customer_id)

        qsc_users.select_order = "lst_name asc"

        If Me.IsPostBack() And Not remove_filter Then
            Me.button_filter_remove.Visible = False

            '''''''''''''''''''''''''''''''''''''''''''
            ' Regel hieronder de zoeken functionaliteit
            '''''''''''''''''''''''''''''''''''''''''''
            If Not String.IsNullOrEmpty(Request.Form(Me.txt_search_user_name.UniqueID)) Then
                ' Als een ordernummer is ingevuld
                qsc_users.select_where += " and (fst_name like '%' + @user_name + '%' or lst_name like '%' + @user_name + '%') "
                qsc_users.add_parameter("user_name", Request.Form(Me.txt_search_user_name.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_search_user_address.UniqueID)) Then
                ' Als een klant referentie is ingevuld
                qsc_users.select_where += " and address like '%' + @address + '%'"
                qsc_users.add_parameter("address", Request.Form(Me.txt_search_user_address.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_search_user_zip_code.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_users.select_where += " and zip_code like @zip_code + '%'"
                qsc_users.add_parameter("zip_code", Request.Form(Me.txt_search_user_zip_code.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_search_user_city.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_users.select_where += " and city like @city + '%'"
                qsc_users.add_parameter("city", Request.Form(Me.txt_search_user_city.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If
        End If

        Return qsc_users.execute_query()
    End Function

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.rpt_users.EnableViewState = False
        Me.rpt_users.DataSource = Me.get_data_source()
        Me.rpt_users.DataBind()

        Me.block_css = False
        Me.set_style_sheet("uc_users.css", Me)

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
        Me.button_close_details.Visible = False

        Me.chk_rights_order.Text = global_trans.translate_label("label_rights_order", "Bestellen toegestaan", Me.global_ws.Language)
        Me.chk_rights_openitems.Text = global_trans.translate_label("label_rights_openitems", "Openstaande posten bekijken toegestaan", Me.global_ws.Language)
        Me.chk_rights_history.Text = global_trans.translate_label("label_rights_history", "Historie bekijken toegestaan", Me.global_ws.Language)
        Me.chk_rights_customer_data.Text = global_trans.translate_label("label_rights_cust_info", "Bedrijfsgegevens wijzigen toegestaan", Me.global_ws.Language)
        Me.chk_rights_users.Text = global_trans.translate_label("label_rights_users", "Gebruikers toevoegen toegestaan", Me.global_ws.Language)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Me.ph_users.Visible = Me.rpt_users.Items.Count > 0
            Me.ph_no_users.Visible = Me.rpt_users.Items.Count = 0

            ' Zoeken alleen beschikbaar wanneer nodig de module inkoopcombinaties aan staat
            Me.button_search.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb")

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
                Me.ph_user_rights.Visible = False
                Me.ph_address_information.Visible = True
                Me.button_add_user.Visible = Me.global_ws.purchase_combination.user_is_manager And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers.default_employee") = ""
            Else
                Me.ph_address_information.Visible = False
                Me.button_add_user.Visible = True
            End If
        End If
    End Sub

    Protected Sub rpt_users_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_users.ItemCommand
        ' Als een gebruiker gewijzigd wordt
        Select Case e.CommandName
            Case "change"
                Dim lc_rec_id As String = e.CommandArgument

                ' Haal dan de gebruikersgegevens op
                Dim udt_data_Table As New Utilize.Data.DataTable
                udt_data_Table.table_name = "uws_customers_users"
                udt_data_Table.table_init()
                udt_data_Table.add_where("and rec_id = @rec_id")
                udt_data_Table.add_where_parameter("rec_id", lc_rec_id)
                udt_data_Table.table_load()

                ' Als de gebruikersgegevens gevonden zijn
                If udt_data_Table.data_row_count > 0 Then
                    ' Verberg en toon de juiste onderdelen 
                    Me.ph_users.Visible = False
                    Me.ph_user_details.Visible = True

                    Me.button_close_details.Visible = True
                    Me.button_add_user.Visible = False

                    Me.title_change_user.Visible = True
                    Me.text_change_user.Visible = True

                    ' Vul de beschikbare velden 
                    Me.txt_rec_id.Text = lc_rec_id
                    Me.txt_fst_name.Text = udt_data_Table.field_get("fst_name")
                    Me.txt_infix.Text = udt_data_Table.field_get("infix")
                    Me.txt_lst_name.Text = udt_data_Table.field_get("lst_name")
                    Me.txt_login_code.Text = udt_data_Table.field_get("login_code")
                    Me.txt_email_address.Text = udt_data_Table.field_get("email_address")

                    ' Zet hier de waarden
                    Me.chk_rights_order.Checked = Not udt_data_Table.field_get("decl_po")
                    Me.chk_rights_openitems.Checked = Not udt_data_Table.field_get("decl_oi")
                    Me.chk_rights_history.Checked = Not udt_data_Table.field_get("decl_poh")
                    Me.chk_rights_users.Checked = Not udt_data_Table.field_get("decl_usr")
                    Me.chk_rights_customer_data.Checked = Not udt_data_Table.field_get("decl_cm")


                    Me.ph_password.Visible = False
                End If
            Case "select"
                ' Laad de geselecteerde gebruiker in in de inkoopcombinaties en toon een melding.
                Me.global_ws.purchase_combination.user_id = e.CommandArgument

                If Me.global_ws.user_information.user_id_original = "" Then
                    Me.global_ws.user_information.user_id_original = Me.global_ws.user_information.user_id
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "user_selected", "alert('" + global_trans.translate_message("message_user_selected", "De gebruiker is geselecteerd", Me.global_ws.Language) + "'); document.location='" + Me.ResolveCustomUrl("~/") + "home.aspx';", True)
            Case Else
                Exit Select
        End Select
    End Sub
    Protected Sub rpt_users_ItemCreated(ByVal sender As Object, ByVal e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_users.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Header Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim ph_purchase_combinations As placeholder = e.Item.FindControl("ph_purchase_combinations")
            ph_purchase_combinations.Visible = Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")

            Dim ph_credits As placeholder = e.Item.FindControl("ph_credits")
            ph_credits.Visible = Me.global_ws.price_mode = Webshop.price_mode.Credits

            Dim ph_email_address As placeholder = e.Item.FindControl("ph_email_address")
            ph_email_address.Visible = Not Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            ' Gebruikersgegevens mogen niet gewijzigd worden als het om de module inkoopcombinaties gaat
            Dim lb_change_user As linkbutton = e.Item.FindControl("link_change_user")
            lb_change_user.Visible = Not Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")

            ' Een gebruiker mag alleen geselecteerd worden wanneer het om de module inkoopcombinaties gaat
            Dim lb_select_user As linkbutton = e.Item.FindControl("link_select_user")
            lb_select_user.Visible = Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")
        End If
    End Sub

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.ph_users.Visible = Me.rpt_users.Items.Count > 0
        Me.ph_no_users.Visible = Me.rpt_users.Items.Count = 0

        Me.ph_user_details.Visible = False

        Me.button_close_details.Visible = False
        Me.button_add_user.Visible = True

        Me.text_change_user.Visible = False
        Me.text_add_user.Visible = False

        Me.title_add_user.Visible = False
        Me.title_change_user.Visible = False
    End Sub
    Protected Sub button_add_user_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_user.Click
        ' Verberg en toon de juiste onderdelen 
        Me.ph_users.Visible = False
        Me.ph_user_details.Visible = True

        Me.button_close_details.Visible = True
        Me.button_add_user.Visible = False

        Me.title_add_user.Visible = True
        Me.title_change_user.Visible = False

        Me.text_change_user.Visible = True
        Me.text_add_user.Visible = False

        Me.ph_password.Visible = True

        ' Maak hier de velden leeg
        Me.txt_rec_id.Text = ""
        Me.txt_fst_name.Text = ""
        Me.txt_infix.Text = ""
        Me.txt_lst_name.Text = ""
        Me.txt_user_address.Text = ""
        Me.txt_user_city.Text = ""
        Me.txt_user_zip_code.Text = ""

        Me.txt_login_code.Text = ""
        Me.txt_password.Text = ""

        Me.txt_email_address.Text = ""

        ' Zet standaard alle rechten aan
        Me.chk_rights_order.Checked = True
        Me.chk_rights_openitems.Checked = True
        Me.chk_rights_history.Checked = True
        Me.chk_rights_users.Checked = True
        Me.chk_rights_customer_data.Checked = True
    End Sub
    Protected Sub button_save_user_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_save_user.Click
        Me.message_mandatory_fields_not_filled.Visible = False
        Me.message_login_code_not_accepted.Visible = False

        Me.alert_fields_error.Visible = False

        Dim ll_resume As Boolean = True
        Dim ubo_customer As Utilize.Data.BusinessObject = Nothing
        Dim ubo_user As Utilize.Data.BusinessObject = Nothing

        If ll_resume Then
            ll_resume = Me.check_mandatory_fields()

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Visible = True
                Me.alert_fields_error.Visible = True
            End If
        End If

        If ll_resume Then
            Try
                Dim email_address_check As MailAddress = New MailAddress(Me.txt_email_address.Text)
                ll_resume = True
            Catch ex As Exception
                ll_resume = False
            End Try

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_email_address_incorrect", "Uw email adres is niet correct.", Me.global_cms.Language)
                Me.message_mandatory_fields_not_filled.Visible = True
                Me.alert_fields_error.Visible = True
            End If
        End If

        If ll_resume Then
            'Check if new user has entered existing login code
            If Me.txt_rec_id.Text = "" Then
                Dim qsc_check_user_exists As New Utilize.Data.QuerySelectClass
                qsc_check_user_exists.select_fields = "*"
                qsc_check_user_exists.select_from = "uws_customers_users"
                qsc_check_user_exists.select_where = "login_code = @login_code"
                qsc_check_user_exists.add_parameter("login_code", Me.txt_login_code.Text)

                Dim dt_check_user_exists As System.Data.DataTable = qsc_check_user_exists.execute_query()

                ll_resume = (dt_check_user_exists.Rows.Count = 0)
            End If

            If Not ll_resume Then
                Me.message_login_code_not_accepted.Visible = True
            End If
        End If

        If ll_resume Then
            ubo_customer = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

            ll_resume = ubo_customer.data_tables("uws_customers").data_row_count > 0

            If Not ll_resume Then
                ' Fout afhandeling
            End If
        End If

        If ll_resume Then
            If Me.txt_rec_id.Text = "" Then
                ll_resume = ubo_customer.table_insert_row("uws_customers_users", Me.global_ws.user_information.user_id)
            Else
                ll_resume = ubo_customer.table_locate("uws_customers_users", "rec_id = '" + Me.txt_rec_id.Text + "'")
            End If

            If Not ll_resume Then
                ' Fout afhandeling
            End If
        End If

        If ll_resume Then
            ' Zet hier de velden
            ubo_customer.field_set("uws_customers_users.fst_name", Me.txt_fst_name.Text)
            ubo_customer.field_set("uws_customers_users.infix", Me.txt_infix.Text)
            ubo_customer.field_set("uws_customers_users.lst_name", Me.txt_lst_name.Text)
            ubo_customer.field_set("uws_customers_users.address", Me.txt_user_address.Text)
            ubo_customer.field_set("uws_customers_users.zip_code", Me.txt_user_zip_code.Text)
            ubo_customer.field_set("uws_customers_users.city", Me.txt_user_city.Text)
            ubo_customer.field_set("uws_customers_users.email_address", Me.txt_email_address.Text)
            ubo_customer.field_set("uws_customers_users.login_code", Me.txt_login_code.Text)

            ubo_customer.field_set("uws_customers_users.decl_po", Not Me.chk_rights_order.Checked)
            ubo_customer.field_set("uws_customers_users.decl_oi", Not Me.chk_rights_openitems.Checked)
            ubo_customer.field_set("uws_customers_users.decl_poh", Not Me.chk_rights_history.Checked)
            ubo_customer.field_set("uws_customers_users.decl_usr", Not Me.chk_rights_users.Checked)
            ubo_customer.field_set("uws_customers_users.decl_cm", Not Me.chk_rights_customer_data.Checked)

            ' Als het een nieuwe gebruiker is, dan wordt het wachtwoord ook bewaard
            If Me.txt_rec_id.Text.Trim() = "" Then
                ubo_customer.field_set("uws_customers_users.login_password", Me.txt_password.Text)

                ' Als de module inkoopcombinaties aan staat, dan moeten ook het aantal toegekende credits ingevuld worden
                If ll_resume And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
                    Dim ln_credits_assigned As Decimal = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "credits_assigned", Me.global_ws.user_information.ubo_customer.field_get("uws_customers.default_employee"))
                    ubo_customer.field_set("uws_customers_users.credits_assigned", ln_credits_assigned)
                    ubo_customer.field_set("uws_customers_users.credits_available", ln_credits_assigned)
                End If
            End If

            ll_resume = ubo_customer.table_update(Me.global_ws.user_information.user_id)

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Text = global_trans.translate_message("message_login_code_not_accepted", "Uw login code is niet geaccepteerd.", Me.global_cms.Language)
                Me.message_mandatory_fields_not_filled.Visible = True
                Me.alert_fields_error.Visible = True
            End If
        End If

        ' Als de module Gebruikersspecifice producten aan staat, dan moeten ook de producten voor de medewerker toegevoegd worden
        If ll_resume And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_webs_usr_prd") Then
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "*"
            qsc_select.select_from = "uws_user_products"
            qsc_select.select_where = "user_id = @user_id"
            qsc_select.add_parameter("user_id", Me.global_ws.user_information.ubo_customer.field_get("default_employee"))

            Dim dt_products As System.Data.DataTable = qsc_select.execute_query()

            ubo_user = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers_users", ubo_customer.field_get("uws_customers_users.rec_id"), "rec_id")

            For Each dr_data_row As System.Data.DataRow In dt_products.Rows
                ubo_user.table_insert_row("uws_user_products", Me.global_ws.user_information.user_id)
                ubo_user.field_set("uws_user_products.cust_id", Me.global_ws.user_information.user_customer_id)
                ubo_user.field_set("uws_user_products.prod_code", dr_data_row.Item("prod_code"))
            Next

            ll_resume = ubo_user.table_update(Me.global_ws.user_information.user_id)

            If Not ll_resume Then
                ' Fout afhandeling
            End If
        End If

        If ll_resume Then
            Me.ph_user_details.Visible = False
            Me.ph_users.Visible = True

            Me.button_close_details.Visible = False
            Me.button_add_user.Visible = True

            Me.text_change_user.Visible = False
            Me.text_add_user.Visible = False

            Me.title_add_user.Visible = False
            Me.title_change_user.Visible = False

            Me.rpt_users.DataSource = Me.get_data_source()
            Me.rpt_users.DataBind()
        End If

        If ll_resume Then
            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub button_filter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter.Click
        ' Als het zoekpanel zichtbaar is, dan moet de zoekopdracht uitgevoerd worden
        If Me.ph_search_users.Visible Then
            Me.rpt_users.page_nr = 0

            Me.rpt_users.DataSource = Me.get_data_source()
            Me.rpt_users.DataBind()

            Me.txt_search_user_name.Focus()
        End If

        Me.button_filter_remove.Visible = True
    End Sub
    Protected Sub button_filter_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter_remove.Click
        Me.txt_search_user_name.Text = ""
        Me.txt_search_user_address.Text = ""
        Me.txt_search_user_city.Text = ""
        Me.txt_search_user_zip_code.Text = ""

        Me.rpt_users.DataSource = get_data_source(True)
        Me.rpt_users.page_nr = 0
        Me.rpt_users.DataBind()

        Me.button_filter.Visible = True
        Me.button_filter_remove.Visible = False

        Me.txt_search_user_name.Focus()
    End Sub

    Protected Sub button_search_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button_search.Click
        ' Zowieso moet het panel zichtbaar zijn als er gezocht gaat worden
        Me.ph_search_users.Visible = True
        Me.button_search.Visible = False

        Me.txt_search_user_name.Focus()
    End Sub

    Private Function check_mandatory_fields() As Boolean
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ll_testing = Not Me.txt_fst_name.Text.Trim() = ""
        End If

        If ll_testing Then
            ll_testing = Not Me.txt_lst_name.Text.Trim() = ""
        End If

        If ll_testing And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
            ll_testing = Not Me.txt_user_address.Text.Trim() = ""
        End If

        If ll_testing And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
            ll_testing = Not Me.txt_user_zip_code.Text.Trim() = ""
        End If

        If ll_testing And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
            ll_testing = Not Me.txt_user_city.Text.Trim() = ""
        End If

        If ll_testing Then
            ll_testing = Not Me.txt_email_address.Text.Trim() = ""
        End If

        If ll_testing Then
            ll_testing = Not Me.txt_login_code.Text.Trim() = ""
        End If

        If ll_testing And Me.txt_rec_id.Text = "" Then
            ll_testing = Not Me.txt_password.Text.Trim() = ""
        End If

        Return ll_testing
    End Function
End Class
