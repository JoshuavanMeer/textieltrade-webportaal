﻿function reinstate_after_postback(link, rma_bulk, rma_normal, bulk_id, order_id) {
    //var current_rma = $(".current_rma").text();
    var current_rma;

    if ($("#" + bulk_id).hasClass("active")) {
        current_rma = rma_bulk;
    }

    if ($("#" + order_id).hasClass("active")) {
        current_rma = rma_normal;
    }

    sessionStorage.setItem("link", link);
    sessionStorage.setItem("rma_bulk", rma_bulk);
    sessionStorage.setItem("rma_normal", rma_normal);
    sessionStorage.setItem("bulk_id", bulk_id);
    sessionStorage.setItem("order_id", order_id);

    $(".dropzone").each(function () {
        var id = $(this).attr("id");
        var new_link;
        if ($("#" + bulk_id).hasClass("active")) {
            var prod_code_raw = $(this).parents("tr").first().prev().prev().find(".check-box > label").first().text()
            var prod_code = prod_code_raw.substring(0, prod_code_raw.indexOf("&"));
            var ord_nr = $(this).parent().parent().parent().parent().prev().prev().find(".hdr_id > span").text();
            new_link = link + "?rmacode=" + current_rma + "&prodcode=" + prod_code + "&ordnr=" + ord_nr;
        } else {
            var prod_code = $(this).parents("tr").first().prev().find(".check-box > label").first().text();
            new_link = link + "?rmacode=" + current_rma + "&prodcode=" + prod_code;
        }
        dropzone_init(id, new_link);
    });
}

$(document).ready(function () {
    reinstate_after_postback(sessionStorage.getItem("link"), sessionStorage.getItem("rma_bulk"), sessionStorage.getItem("rma_normal"), sessionStorage.getItem("bulk_id"), sessionStorage.getItem("order_id"));
});

function dropzone_init(id, url_loc) {
    Dropzone.autoDiscover = false;

    var mydropzone = new Dropzone("#" + id, {
        url: url_loc,
        maxFilesize: 10,
        acceptedFiles: "image/*",
        addRemoveLinks: true,
        success: function (file, response) {
            var imgName = response;
            file.previewElement.classList.add("dz-success");
            $(".dz-remove").on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();

                var imageIdRaw = $(this).parent().find(".dz-filename > span").text();
                var imageId = imageIdRaw.substring(0, imageIdRaw.lastIndexOf("."));

                if ($("#" + sessionStorage.getItem("bulk_id")).hasClass("active")) {
                    var rma_code = $(".current_rma_bulk").text();
                } else {
                    var rma_code = $(".current_rma").text();
                }

                console.log(rma_code);  

                __doPostBack('remove_image', imageId + "&" + rma_code);
            });
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    });
}

function remove_image() {

}


//$(window).unload(function () {
//    $(".remove_images").click();
//});

function move_product_selector() {
    $(".alt_product_selector").after($(".product-selector"));
}

function move_product_selector_back() {
    $(".bulk-details").after($(".product-selector"));
}