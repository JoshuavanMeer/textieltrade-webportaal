﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_change_password.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_change_password" %>

<utilize:panel runat="server" CssClass="block account" DefaultButton="button_save_changes">
    <h1>
        <utilize:translatetitle ID="title_change_password" runat="server" Text="Wachtwoord wijzigen"></utilize:translatetitle></h1>
    <utilize:translatetext runat="server" ID="text_change_password" Text="Vul hieronder twee keer uw nieuwe wachtwoord in en kies voor wijzen om het wachtwoord te bewaren."></utilize:translatetext>
    <utilize:placeholder runat="server" ID="ph_password_validity" Visible="false">
        <div class="alert alert-warning">
            <utilize:translatetext runat="server" ID="text_password_validity" Text="Uw wachtwoord is verlopen, vul hier onder een nieuw wachtwoord in"></utilize:translatetext>
        </div>
    </utilize:placeholder>
    <div class="input-border">
        <utilize:placeholder runat="server" ID="ph_change_password">

            <section>
                <label>
                    <utilize:translatelabel ID="label_old_password" runat="server" Text="Uw oude wachtwoord"></utilize:translatelabel>
                    *
                </label>

                <label class="input">
                    <utilize:textbox ID="tb_old_password" runat="server" CssClass="text form-control" MaxLength="80" TextMode="Password"></utilize:textbox>
                </label>
            </section>

            <section>
                <label>
                    <utilize:translatelabel ID="label_new_password" runat="server" Text="Uw nieuwe wachtwoord"></utilize:translatelabel>
                    *
                </label>

                <label class="input">
                    <utilize:textbox ID="tb_password_1" runat="server" CssClass="text form-control" MaxLength="80" TextMode="Password"></utilize:textbox>
                </label>
            </section>
            <section>
                <label>
                    <utilize:translatelabel ID="label_new_password_confirm" runat="server" Text="Herhaal wachtwoord"></utilize:translatelabel>
                    *
                </label>

                <label class="input">
                    <utilize:textbox ID="tb_password_2" runat="server" CssClass="text form-control" MaxLength="80" TextMode="Password"></utilize:textbox>
                </label>
            </section>
            <section>
                <utilize:translatemessage ID="message_mandatory_fields_not_filled" runat="server" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message" Visible="false"></utilize:translatemessage>
                <utilize:translatemessage ID="message_passwords_not_equal" runat="server" CssClass="message" Text="De wachtwoorden zijn niet aan elkaar gelijk." Visible="false"></utilize:translatemessage>
            </section>

            <utilize:placeholder runat="server" ID="ph_incorrect_old_password" Visible="false">
                <div class="alert alert-danger">
                    <utilize:translateliteral runat="server" ID="lt_incorrect_password" Text="Het oude wachtwoord wat u heeft opgegeven is niet correct"></utilize:translateliteral>
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_format_error" Visible="false">
                <div class="alert alert-danger">
                    <utilize:translatelabel runat="server" ID="lbl_missing_format" Text="Uw wachtwoord moet voldoen aan de volgende eisen:"></utilize:translatelabel>
                    <ul>
                        <li><utilize:translateliteral runat="server" ID="lt_min_char" Text="Minimaal 6 tekens lang"></utilize:translateliteral></li>
                        <li><utilize:translateliteral runat="server" ID="lt_max_char" Text="Maximaal 128 tekens lang"></utilize:translateliteral></li>
                        
                        <utilize:placeholder runat="server" ID="ph_uppercase">
                            <li><utilize:translateliteral runat="server" ID="lt_uppercase" Text="Minimaal 1 hoofdletter"></utilize:translateliteral></li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_lowercase">
                            <li><utilize:translateliteral runat="server" ID="lt_lowercase" Text="Minimaal 1 lowercase letter"></utilize:translateliteral></li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_digit">
                            <li><utilize:translateliteral runat="server" ID="lt_digit" Text="Minimaal 1 getal"></utilize:translateliteral></li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_special_chars">
                            <li><utilize:translateliteral runat="server" ID="lt_special_chars" Text="Minimaal 1 speciaal karakter"></utilize:translateliteral></li>
                        </utilize:placeholder>
                    </ul>
                </div>
            </utilize:placeholder>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_password_changed">
            <utilize:translatetext ID="text_password_saved" runat="server" Text="Uw wachtwoord is gewijzigd. Bij uw eerst volgende login kunt u uw nieuwe wachtwoord gebruiken."></utilize:translatetext>
        </utilize:placeholder>

        <br style="clear: both;" />
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
            <utilize:translatebutton runat="server" ID="button_save_changes" Text="Bewaren" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </div>
</utilize:panel>
