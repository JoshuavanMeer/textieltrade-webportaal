﻿
Partial Class Webshop_Account_AccountBlocks_uc_change_password
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_change_password.css", Me)
        Me.block_css = False

        ' UWS-619: Password AVG
        If Session.Item("password_validity") IsNot Nothing Then
            Me.ph_password_validity.Visible = Not Session.Item("password_validity")
        End If
        ' END UWS-619

        Me.ph_change_password.Visible = True
        Me.ph_password_changed.Visible = False

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Me.button_my_account.Visible = False
        End If

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        Me.ph_uppercase.Visible = global_ws.get_webshop_setting("passw_upper_case")
        Me.ph_lowercase.Visible = global_ws.get_webshop_setting("passw_lower_case")
        Me.ph_digit.Visible = global_ws.get_webshop_setting("passw_numeric")
        Me.ph_special_chars.Visible = global_ws.get_webshop_setting("passw_special_char")
    End Sub

#Region "Buttons"
    Protected Sub button_save_changes_login_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_save_changes.Click
        Me.ph_format_error.Visible = False
        Me.ph_incorrect_old_password.Visible = False

        ' Declareer een business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Nothing

        Me.message_mandatory_fields_not_filled.Visible = False
        Me.message_passwords_not_equal.Visible = False

        ' Controleer hier op verplichte velden
        Dim ll_resume = Not String.IsNullOrEmpty(Me.tb_password_1.Text) And Not String.IsNullOrEmpty(Me.tb_password_2.Text) And Not String.IsNullOrEmpty(Me.tb_old_password.Text)
        Me.message_mandatory_fields_not_filled.Visible = Not ll_resume

        ' UWS-619: Password AVG
        If ll_resume Then
            Dim lc_hashed_password As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.login_password")
            Dim lc_salt As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.password_salt")

            Dim lo_pkcs_service As New Utilize.Secure.Password.PkcsSha256PasswordService

            Dim lc_old_pass As String = Me.tb_old_password.Text
            ll_resume = lo_pkcs_service.ValidatePassword(lc_old_pass, lc_salt, lc_hashed_password)

            If Not ll_resume Then
                Dim lc_old_hash As String = Utilize.FrameWork.Encryption.CreateMD5Signature(lc_old_pass)
                ll_resume = lc_hashed_password.Equals(lc_old_hash)

                If Not ll_resume Then
                    ll_resume = lc_hashed_password.Equals(lc_old_pass)
                End If

                If Not ll_resume Then
                    Me.ph_incorrect_old_password.Visible = True
                End If
            End If
        End If

        ' Check password formating
        If ll_resume Then
            Dim upper As Boolean = global_ws.get_webshop_setting("passw_upper_case")
            Dim lower As Boolean = global_ws.get_webshop_setting("passw_lower_case")
            Dim numeric As Boolean = global_ws.get_webshop_setting("passw_numeric")
            Dim special As Boolean = global_ws.get_webshop_setting("passw_special_char")
            ll_resume = Utilize.Web.Solutions.Webshop.password_check.check_password(Me.tb_password_1.Text, upper, lower, numeric, special)

            If Not ll_resume Then
                Me.ph_format_error.Visible = True
            End If
        End If
        ' END UWS-619

        If ll_resume Then
            ll_resume = Me.tb_password_1.Text = Me.tb_password_2.Text

            Me.message_passwords_not_equal.Visible = Not ll_resume
        End If

        If ll_resume Then
            ubo_bus_obj = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

            ' Set the test flag
            Dim ll_testing As Boolean = True

            ' Look up a specific value in the table
            If ll_testing Then
                ll_testing = ubo_bus_obj.table_locate("uws_customers_users", "rec_id = '" + Me.global_ws.user_information.user_id + "'")
            End If

            If ll_testing Then
                ' Change the value in the main table of the business object
                ubo_bus_obj.field_set("uws_customers_users.login_password", Me.tb_password_1.Text)
                ubo_bus_obj.field_set("uws_customers_users.av_sync", False)
                ubo_bus_obj.field_set("uws_customers_users.last_password_update", DateTime.Now)
                Me.global_ws.user_information.ubo_customer.field_set("uws_customers_users.last_password_update", DateTime.Now)

                ' Save the changes
                ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
            End If

            If ll_testing Then
                Me.ph_change_password.Visible = False
                Me.ph_password_changed.Visible = True
                Me.button_save_changes.Visible = False
                Me.text_change_password.Visible = False
            End If
        End If

        If ll_resume Then
            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

End Class
