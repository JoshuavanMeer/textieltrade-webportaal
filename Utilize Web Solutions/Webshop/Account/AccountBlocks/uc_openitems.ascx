﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_openitems.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_openitems" %>

<div class="block account uc_open_items">
    <utilize:placeholder runat='server' ID="ph_open_items">
        <h1><utilize:translatetitle runat='server' id="title_open_items" Text="Openstaande posten overzicht"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_openitems" Text="Onderstaand vindt u een overzicht van posten die volgens onze administratie nog moeten worden voldaan."></utilize:translatetext>

        <div class="overview">
        <utilize:repeater runat="server" ID="rpt_open_items" pager_enabled="true" page_size="20" pagertype="bottom">
        <HeaderTemplate>
            <div class="table-responsive">
            <table class="overview table table-striped">
                <tbody>
                    <tr class="">
                        <th class="number"><utilize:translatelabel runat='server' ID="col_invoice_number" Text="Factuurnummer"></utilize:translatelabel></th>
                        <th><utilize:translatelabel runat='server' ID="col_invoice_description" Text="Omschrijving"></utilize:translatelabel></th>
                        <th class="date"><utilize:translatelabel runat='server' ID="col_invoice_date" Text="Factuurdatum"></utilize:translatelabel></th>
                        <th class="date"><utilize:translatelabel runat='server' ID="col_invoice_expiration_date" Text="Vervaldatum"></utilize:translatelabel></th>
                        <th><utilize:translatelabel runat='server' ID="col_invoice_pdf" Text="Kopiefactuur"></utilize:translatelabel></th>
                        <th class="amount text-right"><utilize:translatelabel runat='server' ID="col_invoice_amount" Text="Factuurbedrag"></utilize:translatelabel></th>
                        <th class="amount text-right"><utilize:translatelabel runat='server' ID="col_open_amount" Text="Open bedrag"></utilize:translatelabel></th>
                    </tr>
        </HeaderTemplate>
        <ItemTemplate>
                <tr class="line">
                    <td class="number"><%# DataBinder.Eval(Container.DataItem, "inv_nr")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "inv_desc")%></td>
                    <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "inv_date"), "d")%></td>
                    <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "exp_date"), "d")%></td>
                    <td>
                        <utilize:imagebutton style="background:none; height:21px; width:62px; margin: 0;" 
                            OnClientClick='<%# "window.open(""OpenDocument.aspx?DocType=invoice&DocNr=" + DataBinder.Eval(Container.DataItem, "inv_nr") + """);return false;" %>' 
                            ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' 
                            runat="server" ID="button_copy_invoice" 
                            Visible='<%# CheckDocument(DataBinder.Eval(Container.DataItem, "inv_nr")) %>'
                            />
                        <utilize:imagebutton runat="server" ID="button_copy_greyed"                                                         
                            ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf_gray.gif") %>' 
                            Visible='<%# not CheckDocument(DataBinder.Eval(Container.DataItem, "inv_nr")) %>' />
                    </td>
                    <td class="amount text-right"><%# get_order_total(DataBinder.Eval(Container.DataItem, "inv_amt"))%></td>
                    <td class="amount text-right"><%# get_order_total(DataBinder.Eval(Container.DataItem, "inv_amt") - DataBinder.Eval(Container.DataItem, "paid_amt"))%></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
                </tbody>
                </table>
            </div>
        </FooterTemplate>                                    
        </utilize:repeater>
        </div>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_open_items" Visible='false'>
        <h1><utilize:translatetitle runat='server' id="title_no_open_items" Text="Geen openstaande posten"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_openitems" Text="Er zijn op dit moment geen posten dit nog moeten worden voldaan."></utilize:translatetext>
    </utilize:placeholder>  
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>
