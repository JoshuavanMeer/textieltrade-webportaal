﻿
Partial Class Webshop_Account_AccountBlocks_uc_orders
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control


    Private Function get_so_orders() As Utilize.Data.DataTable
        Dim udt_orders As New Utilize.Data.DataTable
        udt_orders.table_name = "uws_so_orders"
        udt_orders.table_init()
        If Not Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            udt_orders.add_where("and cust_id = @cust_id")
            udt_orders.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            udt_orders.select_order = "av_ord_nr desc"
        Else
            udt_orders.add_where("and user_id = @user_id")
            udt_orders.add_where_parameter("user_id", Me.global_ws.user_information.user_id)
            udt_orders.select_order = "ord_nr desc"
        End If

        udt_orders.table_load()

        Return udt_orders
    End Function
    Private Function get_so_order_lines(ByVal ord_nr As String) As Utilize.Data.DataTable
        Dim udt_orders As New Utilize.Data.DataTable
        udt_orders.table_name = "uws_so_order_lines"
        udt_orders.table_init()

        udt_orders.add_where("and hdr_id = @hdr_id")
        udt_orders.add_where_parameter("hdr_id", ord_nr)

        udt_orders.table_load()

        Return udt_orders
    End Function

    Private price_type As Integer = 0
    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

    Private dt_order As System.Data.DataTable = Nothing

    Private Sub set_order_information(ByVal order_number As String)
        Dim udt_data_Table As New Utilize.Data.DataTable
        udt_data_Table.table_name = "uws_so_orders"
        udt_data_Table.table_init()
        If Not Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            udt_data_Table.add_where("and ord_nr = @ord_nr and cust_id = @cust_id")
            udt_data_Table.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            udt_data_Table.add_where_parameter("ord_nr", order_number)
        Else
            udt_data_Table.add_where("and ord_nr = @ord_nr and user_id = @user_id")
            udt_data_Table.add_where_parameter("user_id", Me.global_ws.user_information.user_id)
            udt_data_Table.add_where_parameter("ord_nr", order_number)
            udt_data_Table.select_order = "ord_nr desc"
        End If
        udt_data_Table.table_load()

        ' Betalingskorting
        ' Toon de betalingskorting alleen als deze instelling aan staan in de webshop instellingen
        Me.ph_pay_disc_amt_excl.Visible = Me.global_ws.get_webshop_setting("use_payment_discount")
        Me.ph_pay_disc_amt_incl.Visible = Me.global_ws.get_webshop_setting("use_payment_discount")

        If udt_data_Table.data_row_count > 0 Then
            dt_order = udt_data_Table.data_table

            Me.lbl_order_date.Text = Format(udt_data_Table.field_get("ord_date"), "d")
            Me.lbl_order_number.Text = udt_data_Table.field_get("av_ord_nr")
            Me.lbl_order_description.Text = udt_data_Table.field_get("order_description")

            If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                ' Betalingskorting
                Me.lt_pay_disc_amt_excl.Text = Format(udt_data_Table.field_get("pay_disc_amt"), "c")
                Me.lt_pay_disc_amt_incl.Text = Format(udt_data_Table.field_get("pay_disc_amt"), "c")

                Me.lt_order_subtotal_excl_vat.Text = Format(udt_data_Table.field_get("order_total"), "c")
                Me.lt_order_subtotal_incl_vat.Text = Format((udt_data_Table.field_get("order_total") + udt_data_Table.field_get("vat_total")), "c")

                Me.lt_order_shipping_costs_excl_vat.Text = Format(udt_data_Table.field_get("shipping_costs"), "c")
                Me.lt_order_shipping_costs_incl_vat.Text = Format((udt_data_Table.field_get("shipping_costs") + udt_data_Table.field_get("vat_ship")), "c")

                Me.lt_order_costs_excl_vat.Text = Format(udt_data_Table.field_get("order_costs"), "c")
                Me.lt_order_costs_incl_vat.Text = Format((udt_data_Table.field_get("order_costs") + udt_data_Table.field_get("vat_ord")), "c")

                Dim ln_vat_total As Decimal = (udt_data_Table.field_get("vat_total") + udt_data_Table.field_get("vat_ship") + udt_data_Table.field_get("vat_ord"))
                Dim ln_order_total_incl As Decimal = (udt_data_Table.field_get("order_total") + udt_data_Table.field_get("shipping_costs") + udt_data_Table.field_get("order_costs") - udt_data_Table.field_get("pay_disc_amt") + udt_data_Table.field_get("vat_total") + udt_data_Table.field_get("vat_ship") + udt_data_Table.field_get("vat_ord"))

                If Not String.IsNullOrEmpty(udt_data_Table.field_get("cpn_code")) Then
                    Me.lt_coupon_incl.Text = Format(udt_data_Table.field_get("cpn_amt") + udt_data_Table.field_get("cpn_vat_amt"), "c")
                    Me.lt_coupon_excl.Text = Format(udt_data_Table.field_get("cpn_amt"), "c")

                    ln_vat_total -= udt_data_Table.field_get("cpn_vat_amt")
                    ln_order_total_incl -= (udt_data_Table.field_get("cpn_amt") + udt_data_Table.field_get("cpn_vat_amt"))
                    lit_used_coupon_code.Text = udt_data_Table.field_get("cpn_code")
                Else
                    Me.ph_coupon_excl.Visible = False
                    Me.ph_coupon_incl.Visible = False
                    Me.ph_used_coupon_code.Visible = False
                End If

                Me.lt_order_vat_total.Text = Format(ln_vat_total, "c")
                Me.lt_order_total_incl_vat.Text = Format(ln_order_total_incl, "c")
                Me.lt_order_total_incl_vat1.Text = Format(ln_order_total_incl, "c")

            Else
                    ' Betalingskorting
                    Me.lt_pay_disc_amt_excl.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("pay_disc_amt"))
                Me.lt_pay_disc_amt_incl.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("pay_disc_amt"))

                Me.lt_order_subtotal_excl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_total"))
                Me.lt_order_subtotal_incl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_total") + udt_data_Table.field_get("vat_total"))

                Me.lt_order_shipping_costs_excl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("shipping_costs"))
                Me.lt_order_shipping_costs_incl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("shipping_costs") + udt_data_Table.field_get("vat_ship"))

                Me.lt_order_costs_excl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_costs"))
                Me.lt_order_costs_incl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_costs") + udt_data_Table.field_get("vat_ord"))

                Me.lt_order_vat_total.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("vat_total") + udt_data_Table.field_get("vat_ship_cost") + udt_data_Table.field_get("vat_order_cost"))
                Me.lt_order_total_incl_vat.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_total") + udt_data_Table.field_get("shipping_costs") + udt_data_Table.field_get("order_costs") - udt_data_Table.field_get("pay_disc_amt") + udt_data_Table.field_get("vat_total") + udt_data_Table.field_get("vat_ship") + udt_data_Table.field_get("vat_ord"))
                Me.lt_order_total_incl_vat1.Text = global_ws.convert_amount_to_credits(udt_data_Table.field_get("order_total") + udt_data_Table.field_get("shipping_costs") + udt_data_Table.field_get("order_costs") - udt_data_Table.field_get("pay_disc_amt") + udt_data_Table.field_get("vat_total") + udt_data_Table.field_get("vat_ship") + udt_data_Table.field_get("vat_ord"))
            End If
            Me.ph_orders.Visible = False
            Me.ph_order_details.Visible = True
        End If

        Me.rpt_order_details.DataSource = Me.get_so_order_lines(order_number).data_table
        Me.rpt_order_details.DataBind()
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.price_type = Me.global_ws.get_webshop_setting("price_type")

        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        Me.rpt_orders.DataSource = Me.get_so_orders().data_table
        Me.rpt_orders.DataBind()

        Me.set_style_sheet("uc_orders.css", Me)

        Me.block_css = False
        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        If Not Me.IsPostBack() Then
            ' Haal hier uit de webshop instellingen op of de prijzen inclusief of exclusief BTW zijn.
            Me.ph_prices_vat_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1
            Me.ph_prices_vat_incl.Visible = Me.global_ws.get_webshop_setting("price_type") = 2
        End If

        Me.button_order_report.OnClientClick = "window.open('" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/webshop/account/orderreport.aspx") + "', '', 'width=800, height=600'); return false;"
        Me.button_order_report.Visible = Me.rpt_orders.Items.Count > 0 And Me.global_ws.get_webshop_setting("price_type") = 1
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack() Then
            Select Case True
                Case Not Me.get_page_parameter("OrderId") = ""
                    Me.set_order_information(Me.get_page_parameter("OrderId"))
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.button_close_details.Visible = Me.ph_order_details.Visible

        ' Haal hier uit de webshop instellingen op of de prijzen inclusief of exclusief BTW zijn.
        Me.ph_prices_vat_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1 And Not prices_not_visible
        Me.ph_prices_vat_incl.Visible = Me.global_ws.get_webshop_setting("price_type") = 2 And Not prices_not_visible

        Me.ph_order_costs_excl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")
        Me.ph_order_costs_incl_vat.Visible = Me.global_ws.get_webshop_setting("show_order_costs")
    End Sub

    Protected Sub rpt_orders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_orders.ItemCommand
        If e.CommandName = "details" Then
            Dim lc_ord_nr As String = e.CommandArgument

            Me.set_order_information(lc_ord_nr)
        End If
    End Sub
    Protected Sub rpt_orders_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_orders.ItemCreated
        Dim ph_confirmation As placeholder = e.Item.FindControl("ph_confirmation")

        If Not ph_confirmation Is Nothing Then
            ph_confirmation.Visible = Me.global_ws.get_webshop_setting("req_conf_available") And prices_not_visible = False
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_order_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible

            If Me.price_type = 1 Then
                ' Order total excluding vat
                lt_literal.Text = Format(e.Item.DataItem("order_total"), "c")
            Else
                ' Order total including vat
                lt_literal.Text = Format(e.Item.DataItem("order_total") + e.Item.DataItem("vat_total"), "c")
            End If
        End If
    End Sub

    Protected Sub rpt_order_details_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_order_details.ItemCreated
        Dim ph_backorder_quantity As placeholder = e.Item.FindControl("ph_backorder_quantity")

        If Not ph_backorder_quantity Is Nothing Then
            ph_backorder_quantity.Visible = Me.global_ws.get_webshop_setting("show_back_qty")
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            e.Item.FindControl("ph_size").Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            e.Item.FindControl("ph_size").Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")

            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")

            Try
                If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                    If Me.price_type = 1 Then
                        lt_literal.Text = Format(e.Item.DataItem("row_amt"), "c")
                    Else
                        lt_literal.Text = Format((e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt")), "c")
                    End If
                Else
                    If Me.price_type = 1 Then
                        lt_literal.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt"))
                    Else
                        lt_literal.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt"))
                    End If
                End If

            Catch ex As Exception

            End Try

            ' Decimalen
            Try
                Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

                ' <%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0) %>
                Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")
                If lt_ord_qty IsNot Nothing Then
                    lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If

                ' <%# Decimal.Round(DataBinder.Eval(Container.DataItem, "back_qty"), 0)%>
                Dim lt_back_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_back_qty")
                If lt_back_qty IsNot Nothing Then
                    lt_back_qty.Text = Format(Decimal.Round(e.Item.DataItem("back_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If
            Catch ex As Exception

            End Try
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If
    End Sub

    Public Function get_order_total(ByVal ld_amount As Decimal) As String
        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
            Return Format(ld_amount, "c")
        Else
            Return global_ws.convert_amount_to_credits(ld_amount).ToString()
        End If
    End Function

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.ph_order_details.Visible = False
        Me.ph_orders.Visible = True
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lc_user_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function get_full_name_for_user_id(ByVal lc_user_id As String) As String
        If Not lc_user_id = "" Then
            Dim lo_qsc As New Utilize.Data.QuerySelectClass()
            lo_qsc.select_fields = "fst_name, lst_name, infix, title, rec_id"
            lo_qsc.select_from = "uws_customers_users"
            lo_qsc.select_where = "rec_id = @rec_id"
            lo_qsc.add_parameter("@rec_id", lc_user_id)

            Dim dt_current_user As System.Data.DataTable = lo_qsc.execute_query()

            If dt_current_user.Rows.Count > 0 Then
                Return (dt_current_user.Rows(0).Item("fst_name") + " " + dt_current_user.Rows(0).Item("infix")).trim() + " " + dt_current_user.Rows(0).Item("lst_name")
            Else
                Return "Niet gevonden"
            End If
        End If

        Return ""
    End Function
End Class
