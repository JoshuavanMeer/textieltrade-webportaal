﻿// Add product row with details from row_result
function addRow(tableID, row_result) {
    if (tableID !== '' && row_result !== '') {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var reverseorder = $('#txt_barcode').data("reverseorder");
        if (reverseorder === "True") {
            rowCount = 1;
        }
        var row = table.insertRow(rowCount);
        row.innerHTML = row_result;
    }
}

function setBarcodeScript() {
    // Execute change when enter pressed
    $("#txt_barcode").keypress(function (event) {
        if (event.which == 13) {
            setTimeout(function () {
                $('#txt_barcode').change();
            }, 250);
        }
    });

    // When input changed, then add product if valid
    $("#txt_barcode").change(function () {
        if ($('#txt_barcode').val() !== '') {
            var elementCheck = "txt_barcode_prod_" + $.trim($('#txt_barcode').val()).replace(" ", "_|_").toUpperCase();
            var existingProduct = document.getElementById(elementCheck);
            var barcodesingleline = $('#txt_barcode').data("singleline");
            var element = $("input[data-barcode-target='" + $('#txt_barcode').val() + "']");
            var linesetting;

             if (barcodesingleline === "True") {
             linesetting = element.length === 0 && existingProduct === null;
            }
            if (barcodesingleline === "False") {
                linesetting = element.length === 0 || element.length !== 0;
            }

            if (linesetting) {
                    $.ajax({
                        cache: false,
                        type: "POST",
                        url: "/webshop/account/barcodescanning.aspx/getProductDetails",
                        data: '{TextId: "' + $('#txt_barcode').attr("id") + '", Barcode: "' + $('#txt_barcode').val() + '", Quantity: 1}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d !== '') {
                                addRow('dataTable', data.d);
                                $('#txt_barcode').val('');
                            }
                            else {
                                $('#txt_barcode').val('');
                            }
                            $('#txt_barcode').focus();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#txt_barcode').val('');
                            $('#txt_barcode').focus();
                        }
                    });
                }

                else {
                    if (existingProduct !== null) {
                        existingProduct.value = parseInt(existingProduct.value, 10) + 1;
                    } else if (element.length > 0) {
                        element.first().val(parseInt(element.first().val(), 10) + 1);
                    }
                    $('#txt_barcode').val('');
                    $('#txt_barcode').focus();
                }
        }
    });
}

//data: '{TextId: "' + $('#txt_barcode').attr("id") + '", Barcode: "' + $('#txt_barcode').val() + '", Quantity: 1}',
//data: '{TextId: "", Barcode: "", Quantity: 1}',