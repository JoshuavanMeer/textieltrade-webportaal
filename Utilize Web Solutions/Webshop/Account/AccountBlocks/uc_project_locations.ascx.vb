﻿
Partial Class uc_project_locations
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Function get_data_source(Optional ByVal remove_filter As Boolean = False) As System.Data.DataTable
        Dim qsc_project_locations As New Utilize.Data.QuerySelectClass
        qsc_project_locations.select_fields = "*"
        qsc_project_locations.select_from = "uws_project_location"

        qsc_project_locations.select_where = "cust_id = @cust_id "
        qsc_project_locations.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

        qsc_project_locations.select_order = "location_name asc"

        If Me.IsPostBack() And Not remove_filter Then
            Me.button_filter_remove.Visible = False

            '''''''''''''''''''''''''''''''''''''''''''
            ' Regel hieronder de zoeken functionaliteit
            '''''''''''''''''''''''''''''''''''''''''''
            If Not String.IsNullOrEmpty(Request.Form(Me.txt_project_location_name.UniqueID)) Then
                ' Als een ordernummer is ingevuld
                qsc_project_locations.select_where += " and (location_name like '%' @project_location_name + '%') "
                qsc_project_locations.add_parameter("project_location_name", Request.Form(Me.txt_project_location_name.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_project_location_address.UniqueID)) Then
                ' Als een klant referentie is ingevuld
                qsc_project_locations.select_where += " and address like '%' + @address + '%'"
                qsc_project_locations.add_parameter("address", Request.Form(Me.txt_project_location_address.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_project_location_zip_code.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_project_locations.select_where += " and zip_code like @zip_code + '%'"
                qsc_project_locations.add_parameter("zip_code", Request.Form(Me.txt_project_location_zip_code.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_project_location_city.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_project_locations.select_where += " and city like @city + '%'"
                qsc_project_locations.add_parameter("city", Request.Form(Me.txt_project_location_city.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If
        End If

        Return qsc_project_locations.execute_query()
    End Function

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.txt_zip_code.Style.Add("text-transform", "uppercase")

        Me.rpt_project_locations.EnableViewState = False
        Me.rpt_project_locations.DataSource = Me.get_data_source()
        Me.rpt_project_locations.DataBind()

        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.block_css = False
        Me.set_style_sheet("uc_project_locations.css", Me)

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
        Me.button_close_details.Visible = False

        ' Vertaal hier de checkbox
        Me.chk_blocked.Text = global_trans.translate_label("label_project_location_blocked", "Geblokkeerd", Me.global_ws.Language)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Me.ph_project_locations.Visible = Me.rpt_project_locations.Items.Count > 0
            Me.ph_no_project_locations.Visible = Me.rpt_project_locations.Items.Count = 0

            ' Zoeken alleen beschikbaar wanneer nodig de module inkoopcombinaties aan staat
            Me.button_search.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Me.rpt_project_locations.Items.Count > Me.rpt_project_locations.page_size
        End If

        Me.lbl_project_location_name.Text = Me.label_project_location_name.Text
        Me.lbl_project_location_address.Text = Me.label_address.Text
        Me.lbl_project_location_city.Text = Me.label_city.Text
        Me.lbl_project_location_zip_code.Text = Me.label_zip_code.Text
    End Sub

    Protected Sub rpt_project_locations_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_project_locations.ItemCommand
        ' Als een gebruiker gewijzigd wordt
        Select Case e.CommandName
            Case "change"
                Dim lc_rec_id As String = e.CommandArgument

                ' Haal dan de gebruikersgegevens op
                Dim udt_data_Table As New Utilize.Data.DataTable
                udt_data_Table.table_name = "uws_project_location"
                udt_data_Table.table_init()
                udt_data_Table.add_where("and rec_id = @rec_id")
                udt_data_Table.add_where_parameter("rec_id", lc_rec_id)
                udt_data_Table.table_load()

                ' Als de gebruikersgegevens gevonden zijn
                If udt_data_Table.data_row_count > 0 Then
                    ' Verberg en toon de juiste onderdelen 
                    Me.ph_project_locations.Visible = False
                    Me.ph_project_location_details.Visible = True

                    Me.button_close_details.Visible = True
                    Me.button_add_project_location.Visible = False

                    Me.title_change_project_location.Visible = True
                    Me.text_change_project_location.Visible = True

                    ' Vul de beschikbare velden 
                    Me.txt_rec_id.Text = lc_rec_id
                    Me.txt_location_name.Text = udt_data_Table.field_get("location_name")
                    Me.txt_address.Text = udt_data_Table.field_get("address")
                    Me.txt_zip_code.Text = udt_data_Table.field_get("zip_code").toUpper()
                    Me.txt_city.Text = udt_data_Table.field_get("city")
                    Me.chk_blocked.Checked = udt_data_Table.field_get("blocked")
                End If
            Case Else
                Exit Select
        End Select
    End Sub

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.message_fields_not_filled.Visible = False
        Me.ph_project_location_details.Visible = False

        Me.ph_project_locations.Visible = Me.rpt_project_locations.Items.Count > 0
        Me.ph_no_project_locations.Visible = Me.rpt_project_locations.Items.Count = 0

        Me.button_close_details.Visible = False
        Me.button_add_project_location.Visible = True

        Me.text_change_project_location.Visible = False
        Me.text_add_project_location.Visible = False

        Me.title_add_project_location.Visible = False
        Me.title_change_project_location.Visible = False
    End Sub
    Protected Sub button_add_project_location_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_project_location.Click
        ' Verberg en toon de juiste onderdelen 
        Me.ph_project_locations.Visible = False
        Me.ph_no_project_locations.Visible = False
        Me.ph_project_location_details.Visible = True

        Me.button_close_details.Visible = True
        Me.button_add_project_location.Visible = False

        Me.title_add_project_location.Visible = True
        Me.title_change_project_location.Visible = False

        Me.text_change_project_location.Visible = True
        Me.text_add_project_location.Visible = False

        ' Maak hier de velden leeg
        Me.txt_rec_id.Text = ""
        Me.txt_location_name.Text = ""
        Me.txt_address.Text = ""
        Me.txt_zip_code.Text = ""
        Me.txt_city.Text = ""
    End Sub
    Protected Sub button_save_project_location_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_save_project_location.Click
        Dim ll_resume As Boolean = True
        Me.message_fields_not_filled.Visible = False

        Dim ubo_project_location As Utilize.Data.BusinessObject = Nothing

        If ll_resume Then
            ll_resume = Me.check_fields_filled()

            If Not ll_resume Then
                Me.message_fields_not_filled.Visible = True
            End If
        End If

        If ll_resume Then
            ubo_project_location = Utilize.Data.DataProcedures.CreateRecordObject("uws_project_location", Me.txt_rec_id.Text, "rec_id")
        End If

        If ll_resume Then
            If Me.txt_rec_id.Text = "" Then
                ll_resume = ubo_project_location.table_insert_row("uws_project_location", Me.global_ws.user_information.user_id)
            Else
                ll_resume = ubo_project_location.table_locate("uws_project_location", "rec_id = '" + Me.txt_rec_id.Text + "'")
            End If

            If Not ll_resume Then
                ' Fout afhandeling
            End If
        End If

        If ll_resume Then
            ' Zet hier de velden
            ubo_project_location.field_set("uws_project_location.location_name", Me.txt_location_name.Text)
            ubo_project_location.field_set("uws_project_location.cust_id", Me.global_ws.user_information.user_customer_id)
            ubo_project_location.field_set("uws_project_location.address", Me.txt_address.Text)
            ubo_project_location.field_set("uws_project_location.zip_code", Me.txt_zip_code.Text.ToUpper())
            ubo_project_location.field_set("uws_project_location.city", Me.txt_city.Text)
            ubo_project_location.field_set("uws_project_location.blocked", Me.chk_blocked.Checked)

            ll_resume = ubo_project_location.table_update(Me.global_ws.user_information.user_id)

            If Not ll_resume Then
                ' Fout afhandeling
            End If
        End If

        If ll_resume Then
            Me.ph_project_location_details.Visible = False
            Me.ph_project_locations.Visible = True

            Me.button_close_details.Visible = False
            Me.button_add_project_location.Visible = True

            Me.text_change_project_location.Visible = False
            Me.text_add_project_location.Visible = False

            Me.title_add_project_location.Visible = False
            Me.title_change_project_location.Visible = False

            Me.rpt_project_locations.DataSource = Me.get_data_source()
            Me.rpt_project_locations.DataBind()
        End If

        If ll_resume Then
            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub button_filter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter.Click
        ' Als het zoekpanel zichtbaar is, dan moet de zoekopdracht uitgevoerd worden
        If Me.ph_search_project_locations.Visible Then
            Me.rpt_project_locations.page_nr = 0

            Me.rpt_project_locations.DataSource = Me.get_data_source()
            Me.rpt_project_locations.DataBind()

            Me.txt_project_location_name.Focus()
        End If

        Me.button_filter_remove.Visible = True
    End Sub
    Protected Sub button_filter_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter_remove.Click
        Me.txt_project_location_name.Text = ""
        Me.txt_project_location_address.Text = ""
        Me.txt_project_location_city.Text = ""
        Me.txt_project_location_zip_code.Text = ""

        Me.rpt_project_locations.DataSource = get_data_source(True)
        Me.rpt_project_locations.page_nr = 0
        Me.rpt_project_locations.DataBind()

        Me.button_filter.Visible = True
        Me.button_filter_remove.Visible = False

        Me.txt_project_location_name.Focus()
    End Sub

    Protected Sub button_search_Click(sender As Object, e As EventArgs) Handles button_search.Click
        ' Zowieso moet het panel zichtbaar zijn als er gezocht gaat worden
        Me.ph_search_project_locations.Visible = True
        Me.button_search.Visible = False

        Me.txt_project_location_name.Focus()
    End Sub

    Private Function check_fields_filled() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.txt_location_name.Text = ""
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_address.Text = ""
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_zip_code.Text = ""
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_city.Text = ""
        End If

        Return ll_resume
    End Function
End Class
