﻿
Partial Class uc_customer_data
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _change_company_info As Integer = 0
    Private uc_form_block As Base.base_usercontrol_base = Nothing

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me._change_company_info = Me.global_ws.get_webshop_setting("change_company_info")

        ' Er zijn 3 mogelijkheden tbv het factuuradres:
        ' 1: adres wijzigen toegestaan
        ' 2: adres wijzigen niet toegestaan
        ' 3: adres wijziging doorgeven via formulier
        Select Case _change_company_info
            Case 2
                Me.uc_address_block1.enabled = False
                Me.uc_address_block2.enabled = False
                Me.ph_changes_buttons.Visible = False
                Me.button_save_changes.Visible = False

                Me.ph_delivery_address.Visible = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.del_address_custom") = True
            Case 3
                ' 3: adres wijziging doorgeven via formulier
                ' Zorg dat de wijzigingen doorgeven knop zichtbaar is
                Me.uc_address_block1.enabled = False
                Me.uc_address_block2.enabled = False

                Me.ph_delivery_address.Visible = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.del_address_custom") = True
                Me.button_save_changes.Visible = False

                Me.ph_changes_buttons.Visible = True

                ' Voeg hier het formulier blok toe
                uc_form_block = LoadControl("/CMS/Modules/Forms/uc_form_block.ascx")
                uc_form_block.set_property("block_id", Me.global_ws.get_webshop_setting("change_company_form"))
                uc_form_block.set_property("block_css", False)

                ' Haal de skin id op en zet deze wanneer deze niet leeg is
                Dim lc_skin_id As String = Utilize.Data.DataProcedures.GetValue("ucm_form_blocks", "skin_id", Me.global_ws.get_webshop_setting("change_company_form"))
                uc_form_block.set_property("skin", lc_skin_id)

                Me.ph_form_block.Controls.Add(uc_form_block)
            Case Else
                ' Maak de knop om wijzigingen door te geven onzichtbaar
                Me.ph_changes_buttons.Visible = False
        End Select

        Me.uc_address_block1.set_address()
        Me.uc_address_block2.set_address()

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
        Me.set_style_sheet("uc_customer_data.css", Me)
        Me.block_css = False
    End Sub

#Region "Buttons"
    Protected Sub button_save_changes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_save_changes.Click
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Controleer de adressen
            ll_testing = Me.uc_address_block1.check_address() And Me.uc_address_block2.check_address()
        End If

        If ll_testing Then
            ' Sla de adressen op in het business object
            ll_testing = Me.uc_address_block1.save_address() And Me.uc_address_block2.save_address()

            If Not ll_testing Then
                Throw New Exception("An error occured while saving information to the business object: " + Me.global_ws.user_information.ubo_customer.error_message)
            End If
        End If

        If ll_testing Then
            ll_testing = Me.global_ws.user_information.save_user_information()

            If Not ll_testing Then
                Utilize.Web.UI.UIProcedures.ShowMessage(Me.page, Me.global_ws.user_information.error_message.Replace("'", "\'"))
                Throw New Exception("An error occured while saving information to the database: " + Me.global_ws.user_information.error_message)
            End If
        End If

        If ll_testing Then
            Try
                Dim WsAvConn As New WSAvConn.WSAvConn With {
                        .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                    }
                WsAvConn.GetWebshopInformation()
            Catch ex As Exception

            End Try
        End If

        If ll_testing Then
            ' Maak hier de diverse onderdelen zichtbaar/onzichtbaar
            Me.button_save_changes.Visible = False
            Me.ph_change_address.Visible = False
            Me.ph_change_confirm.Visible = True
        End If
    End Sub
#End Region

    Protected Sub button_send_changes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_send_changes.Click
        Me.ph_change_address.Visible = False
        Me.ph_change_confirm.Visible = False
        Me.ph_form_block.Visible = True

        Me.button_send_changes.Visible = False
        Me.button_save_changes.Visible = False
    End Sub
End Class
