﻿﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_concept_orders.ascx.vb" Inherits="uc_concept_orders" %>
 
 <div class="block account uc_concept_orders">
    <utilize:placeholder runat='server' ID="ph_concept_orders">
        <h1><utilize:translatetitle runat='server' ID="title_concept_orders_overview" Text="Overzicht concept bestellingen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_concept_orders_overview" Text="Onderstaand vindt u een overzicht van concept bestellingen. Klik op het ordernummer om een overzicht van de bewaarde bestelling te bekijken."></utilize:translatetext>    

        <div class="row">
            <div class="col-md-3"><utilize:translatelabel runat="server" ID="label_select_concept_order" Text="Geselecteerde conceptbestelling:"></utilize:translatelabel></div>
            <div class="col-md-9"><utilize:dropdownlist runat="server" ID="ddl_select_concept_order" CssClass="select dropdownlist form-control selectboxtype"></utilize:dropdownlist></div>
        </div>

        <div class="overview margin-top-20">
            <utilize:repeater runat="server" ID="rpt_concept_orders" pager_enabled="true" page_size="20" pagertype="bottom">
                <HeaderTemplate>
                    <div class="table-responsive">
                        <table class="overview table table-striped">
                            <tbody>
                                <tr>
                                    <th class="number">
                                        <utilize:translatelabel runat='server' ID="col_concept_order_number" Text="Ordernummer"></utilize:translatelabel></th>
                                    <th class="date">
                                        <utilize:translatelabel runat='server' ID="col_concept_order_date" Text="Datum"></utilize:translatelabel></th>
                                    <th>
                                        <utilize:translatelabel runat='server' ID="col_concept_order_description" Text="Omschrijving"></utilize:translatelabel></th>
                                    <th class="amount text-right">
                                        <utilize:translatelabel runat='server' ID="col_concept_order_amount" Text="Bedrag"></utilize:translatelabel></th>
                                    <th class="delete"></th>
                                </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="line">
                        <td class="number">
                            <utilize:linkbutton runat="server" ID="lb_concept_order_details" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' CommandName="details"><%# DataBinder.Eval(Container.DataItem, "rec_id")%></utilize:linkbutton></td>
                            <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "order_description") %></td>
                        <td class="amount text-right">
                            <utilize:literal runat="server" ID="lt_row_amount" Text='<%# Format(DataBinder.Eval(Container.DataItem, "order_total"), "c")%>'></utilize:literal></td>
                        <td class="delete">
                            <div class="actionbuttons">
                                <utilize:translatelinkbutton runat="server" ID="link_pdf_order" Text="PDF Openen" CssClass="btn-u btn-u-sea-shop" CommandName="openpdf" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'/>
                                <utilize:translatelinkbutton runat="server" ID="link_finish_order" Text="Bestellen" CssClass="btn-u btn-u-sea-shop" CommandName="finish" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'></utilize:translatelinkbutton>
                                <utilize:button runat="server" CssClass="delete_row" ID="link_delete_order" CommandName="delete_order" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>' />
                            </div>
                        </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                            </tbody>
                        </table>
                    </div>
                </FooterTemplate>                                    
            </utilize:repeater>
        </div>
    </utilize:placeholder>
        <utilize:placeholder runat='server' ID="ph_no_concept_orders" Visible='false'>
        <h1>
            <utilize:translatetitle runat='server' ID="title_no_concept_orders" Text="Geen conceptbestellingen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_concept_orders" Text="Er zijn bij ons op dit moment geen concept bestellingen."></utilize:translatetext>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_concept_order_details" Visible="false">
        <h1>
            <utilize:translatetitle runat='server' ID="title_concept_order_detail" Text="Overzicht concept bestelling"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_concept_order_details" Text="Onderstaand vind u het overzicht van de concept bestelling."></utilize:translatetext>
        <div class="row margin-bottom-20"> 
           <div class="col-md-12">
               <div class="row">
                    <div class="col-md-2">
                        <utilize:translatelabel runat='server' ID="label_concept_order_number" Text="Ordernummer"></utilize:translatelabel></div>
                    <div class="col-md-2">
                        <utilize:label runat='server' ID="lbl_concept_order_number" CssClass="text-right"></utilize:label></div>
                </div>
                
               <div class="row">
                    <div class="col-md-2">
                        <utilize:translatelabel runat='server' ID="label_concept_order_date" Text="Orderdatum"></utilize:translatelabel></div>
                    <div class="col-md-2">
                        <utilize:label runat='server' ID="lbl_concept_order_date"></utilize:label></div>
               </div>

               <div class="row">
                    <div class="col-md-2">
                        <utilize:translatelabel runat='server' ID="label_concept_order_description" Text="Omschrijving"></utilize:translatelabel></div>
                    <div class="col-md-2">
                        <utilize:label runat='server' ID="lbl_concept_order_description"></utilize:label></div>
                </div>
            </div>
        </div>
        <utilize:repeater runat="server" ID="rpt_concept_order_details">
            <HeaderTemplate>
                <div class="table-responsive">
                    <table class="lines table table-striped">
                        <tbody>
                            <tr class="">
                                <th class="code">
                                    <utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th>
                                    <utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <th class="quantity text-center">
                                    <utilize:translatelabel runat='server' ID="col_concept_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <th class="amount text-right">
                                    <utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                        <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                    <td class="quantity text-center"><utilize:literal runat="server" ID="lt_ord_qty"></utilize:literal></td>
                    <td class="amount text-right">
                        <utilize:literal runat="server" ID="lt_row_amount" Text='<%# Format(IIf(Me.price_type = 1, DataBinder.Eval(Container.DataItem, "row_amt"), (DataBinder.Eval(Container.DataItem, "row_amt") + DataBinder.Eval(Container.DataItem, "vat_amt"))), "c")%>'></utilize:literal></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                        </tbody>
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
        <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
            <div class="col-md-6 price-overview">
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_subtotal_excl_vat"></utilize:literal>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_costs_excl_vat"></utilize:literal>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_shipping_costs_excl_vat"></utilize:literal>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></b>
                        </div>    
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_vat_total"></utilize:literal>
                        </div>    
                        </div>    
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></b>
                        </div>    
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_total_incl_vat"></utilize:literal>
                        </div>    
                </div>
            </div>
            
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prices_vat_incl">
            <div class="col-md-6 price-overview">
                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_subtotal_incl_vat" Text="Subtotaal incl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_subtotal_incl_vat"></utilize:literal>
                    </div>
                            </div>

                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_order_costs_incl_vat" Text="Orderkosten incl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_costs_incl_vat"></utilize:literal>
                    </div>
                            </div>

                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_shipping_costs_incl_vat" Text="Verzendkosten incl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_shipping_costs_incl_vat"></utilize:literal>
                    </div>
                            </div>

                <div class="row">
                    <div class="col-md-6">
                        <b><utilize:translateliteral runat="server" ID="totals_total_incl_vat1" Text="Totaal incl. BTW"></utilize:translateliteral></b>
                    </div>
                    <div class="col-md-6">
                        <utilize:literal runat="server" ID="lt_concept_order_total_incl_vat1"></utilize:literal>
                            </div>
                </div>
            </div>
        </utilize:placeholder>
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_continue_shopping" Text="Verder winkelen" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>