﻿Imports System.IO
Imports System.Web.UI.WebControls

Partial Class Webshop_Account_AccountBlocks_uc_return_request
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control
    'de klasse van het autcomplete field
    
<Serializable()>
Public Class product_code
        Inherits Utilize.Web.UI.WebControls.webcontrol

        Private _product_code As String = ""
        Friend WithEvents _input_control As New Utilize.Web.UI.WebControls.textbox
        Public ReadOnly Property input_control As Utilize.Web.UI.WebControls.textbox
            Get
                Return _input_control
            End Get
        End Property

        Private _input_control_extender As New AjaxControlToolkit.AutoCompleteExtender
        Private _input_javascript As New Utilize.Web.UI.WebControls.literal

        Public customer_id As String = ""
        Public language As String = ""

        Public Sub New()
            Me._input_control_extender.MinimumPrefixLength = "2"
            Me._input_control_extender.CompletionInterval = "100"
            Me._input_control_extender.CompletionListCssClass = "autocomplete_completionListElement"
            Me._input_control_extender.CompletionListItemCssClass = "autocomplete_listItem"
            Me._input_control_extender.CompletionListHighlightedItemCssClass = "autocomplete_highlightedListItem"
            Me._input_control_extender.ServiceMethod = "GetItems"
            Me._input_control_extender.FirstRowSelected = False
        End Sub

        Private Sub product_code_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            _input_control.Attributes.Add("class", "form-control")
            _input_control.Attributes.Add("style", "width: 100%")
            Me.Controls.Add(_input_control)
            Me.Controls.Add(_input_control_extender)
            Me.Controls.Add(_input_javascript)

            Me._input_control.ID = "txt_product_code"
            Me._input_control.AutoPostBack = True

            Me._input_control_extender.OnClientItemSelected = "product_code_get_code"
            Me._input_control_extender.TargetControlID = "txt_product_code"
            Me._input_control_extender.ID = "ext_product_code"

        End Sub
        Private Sub product_code_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            Me._input_control_extender.ContextKey = Me.customer_id + "|" + Me.language

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "get_prodcode", "function product_code_get_code" + "(source, eventArgs) {var lnValue = eventArgs.get_value(); if (lnValue == null){} else {document.getElementById(""" + Me._input_control.ClientID + """).value = lnValue.substr(0, 20).trim(); document.getElementById(""" + Me._input_control.ClientID + """).blur();} }", True)

            Me._input_control.Width = 120
            Me._input_control.Style.Add("text-transform", "uppercase")
        End Sub

        Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Public Property Text As String
            Get
                Return Me._input_control.Text
            End Get
            Set(ByVal value As String)
                Me._input_control.Text = value.ToUpper()
            End Set
        End Property

        Protected Sub _input_control_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _input_control.TextChanged
            RaiseEvent TextChanged(sender, e)
        End Sub

        Public Overrides Sub Focus()
            Me._input_control.Focus()
        End Sub
    End Class

    
<Serializable()>
Public Class product_code_bulk
        Inherits Utilize.Web.UI.WebControls.webcontrol

        Private _product_code As String = ""
        Friend WithEvents _input_control As New Utilize.Web.UI.WebControls.textbox
        Public ReadOnly Property input_control As Utilize.Web.UI.WebControls.textbox
            Get
                Return _input_control
            End Get
        End Property

        Private _input_control_extender As New AjaxControlToolkit.AutoCompleteExtender
        Private _input_javascript As New Utilize.Web.UI.WebControls.literal

        Public customer_id As String = ""
        Public language As String = ""

        Public Sub New()
            Me._input_control_extender.MinimumPrefixLength = "2"
            Me._input_control_extender.CompletionInterval = "100"
            Me._input_control_extender.CompletionListCssClass = "autocomplete_completionListElement"
            Me._input_control_extender.CompletionListItemCssClass = "autocomplete_listItem"
            Me._input_control_extender.CompletionListHighlightedItemCssClass = "autocomplete_highlightedListItem"
            Me._input_control_extender.ServiceMethod = "GetBulkItems"
            Me._input_control_extender.FirstRowSelected = False
        End Sub

        Private Sub product_code_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            _input_control.Attributes.Add("class", "form-control")
            _input_control.Attributes.Add("style", "width: 100%")
            Me.Controls.Add(_input_control)
            Me.Controls.Add(_input_control_extender)
            Me.Controls.Add(_input_javascript)

            Me._input_control.ID = "txt_product_code_bulk"
            Me._input_control.AutoPostBack = True

            Me._input_control_extender.OnClientItemSelected = "product_code_get_code"
            Me._input_control_extender.TargetControlID = "txt_product_code_bulk"
            Me._input_control_extender.ID = "ext_product_code_bulk"

        End Sub
        Private Sub product_code_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            Me._input_control_extender.ContextKey = Me.customer_id + "|" + Me.language

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "get_prodcode", "function product_code_get_code" + "(source, eventArgs) {var lnValue = eventArgs.get_value(); if (lnValue == null){} else {document.getElementById(""" + Me._input_control.ClientID + """).value = lnValue.substr(0, 20).trim(); document.getElementById(""" + Me._input_control.ClientID + """).blur();} }", True)

            Me._input_control.Width = 120
            Me._input_control.Style.Add("text-transform", "uppercase")
        End Sub

        Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Public Property Text As String
            Get
                Return Me._input_control.Text
            End Get
            Set(ByVal value As String)
                Me._input_control.Text = value.ToUpper()
            End Set
        End Property

        Protected Sub _input_control_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _input_control.TextChanged
            RaiseEvent TextChanged(sender, e)
        End Sub

        Public Overrides Sub Focus()
            Me._input_control.Focus()
        End Sub
    End Class

    
<Serializable()>
Public Class button_remove_file
        Inherits Utilize.Web.UI.WebControls.linkbutton

        Public UserControl As Webshop_Account_AccountBlocks_uc_return_request = Nothing
        Public ph_images As Utilize.Web.UI.WebControls.placeholder

        Private Sub button_remove_file_Click(sender As Object, e As EventArgs) Handles Me.Click
            Dim filepath As String = CommandArgument
            Dim filename As String = CommandName

            Dim tempPath As String = filepath
            Dim directory As New DirectoryInfo(tempPath)
            Dim files As FileInfo() = directory.GetFiles()
            Dim singleFile As FileInfo

            If files.Length > 0 Then
                For Each singleFile In files
                    If singleFile.Name.Contains(filename) Then
                        singleFile.Delete()
                    End If
                Next
            End If

            'ph_images.Controls.Clear()

            If filename.Contains("{") Then
                UserControl.set_bulk_images()
            Else
                UserControl.set_images()
            End If
        End Sub
    End Class

    Private dt_reason As New System.Data.DataTable
#Region "Page functions"
    Public Function get_order_total(ByVal ld_amount As Decimal) As String
        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
            Return Format(ld_amount, "c")
        Else
            Return global_ws.convert_amount_to_credits(ld_amount).ToString()
        End If
    End Function

    Private Function get_order(Optional ByVal search As Boolean = False) As System.Data.DataTable
        Dim trn_nr As String = "1"

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_history_del") Then
            trn_nr = "9"
        End If

        Dim qsc_orders As New Utilize.Data.QuerySelectClass
        qsc_orders.select_fields = "*"
        qsc_orders.select_from = "uws_history"
        qsc_orders.select_where = "cust_id = @cust_id and trn_type = @trn_nr"
        qsc_orders.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)
        qsc_orders.add_parameter("@trn_nr", trn_nr)

        'check of de functie is aangeroepen voor een zoekopdracht of niet, en een check of het een postback is.
        If search And Me.IsPostBack() Then

            'check op order nummer
            If trn_nr = "1" Then
                If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Then
                    qsc_orders.select_where += " and doc_nr like @doc_nr"
                    qsc_orders.add_parameter("@doc_nr", String.Concat(Me.txt_order_code.Text, "%"))
                End If
            End If
            If trn_nr = "9" Then
                If Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
                    qsc_orders.select_where += " and doc_nr like @doc_nr"
                    qsc_orders.add_parameter("@doc_nr", Me.txt_invoice_nr.Text + "%")
                End If
            End If

            If Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                Dim req_date As String = ""

                'bouw de string voor de search op de request date hier op
                If String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                    req_date += "%-"
                Else
                    req_date += Me.txt_request_year.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_month.Text) Then
                    req_date += "%-"
                Else
                    req_date += "%" + Me.txt_request_month.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_day.Text) Then
                    req_date += "%"
                Else
                    req_date += "%" + Me.txt_request_day.Text + "%"
                End If

                'check of er al een optie gebruikt is of niet
                qsc_orders.select_where += " AND CONVERT(VARCHAR, ord_date, 120) like @ord_date"
                qsc_orders.add_parameter("@ord_date", req_date)
            End If
        Else

        End If
        qsc_orders.select_order = "crec_id desc"

        Dim udt_order_table As New System.Data.DataTable
        udt_order_table = qsc_orders.execute_query()

        udt_order_table = check_availablity(udt_order_table)

        Return udt_order_table
    End Function

    Private Function get_order_detail(ord_id As String) As System.Data.DataTable
        Dim qsc_order_detail As New Utilize.Data.QuerySelectClass
        qsc_order_detail.select_fields = "*"
        qsc_order_detail.select_from = "uws_history_lines"
        qsc_order_detail.select_where = "hdr_id = @hdr_id"
        qsc_order_detail.add_parameter("@hdr_id", ord_id)

        Dim udt_order_detail_table As New System.Data.DataTable
        udt_order_detail_table = qsc_order_detail.execute_query()
        Return udt_order_detail_table
    End Function

    Private Function calc_row_amt(price As Decimal, amt As Integer, quantity As Integer) As Decimal
        Dim result As New Decimal
        result = price / amt
        result = result * quantity
        Return result
    End Function

    Private Function calc_row_btw(vat_amt As Decimal, input_amt As Decimal, quantity As Integer) As Decimal
        Dim result As New Decimal

        result = vat_amt / input_amt
        result *= quantity

        Return result
    End Function

    Private Function check_availablity(ByVal dt_table As System.Data.DataTable) As System.Data.DataTable
        Dim ll_testing As Boolean = True
        Dim dt_orders As System.Data.DataTable = dt_table

        ll_testing = dt_table.Rows.Count > 0

        Dim dt_rmas As New System.Data.DataTable
        If ll_testing Then
            'haal alle rma's op van de klant die niet afgekeurd zijn
            Dim qsc_rmas As New Utilize.Data.QuerySelectClass
            qsc_rmas.select_fields = "*"
            qsc_rmas.select_from = "uws_rma"
            qsc_rmas.select_where = "cust_id = @cust_id and not req_status = 'afgekeurd'"
            qsc_rmas.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)
            qsc_rmas.select_order = "rma_code DESC"
            dt_rmas = qsc_rmas.execute_query()

            ll_testing = dt_rmas.Rows.Count > 0
        End If

        Dim dt_order_lines As New System.Data.DataTable
        If ll_testing Then
            'haal alle history lines op van de klant
            Dim qsc_order_lines As New Utilize.Data.QuerySelectClass
            qsc_order_lines.select_fields = "*"
            qsc_order_lines.select_from = "uws_history_lines"
            qsc_order_lines.select_where = "hdr_id in (select crec_id from uws_history where cust_id = @cust_id)"
            qsc_order_lines.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)
            dt_order_lines = qsc_order_lines.execute_query()

            ll_testing = dt_order_lines.Rows.Count > 0
        End If

        Dim dt_rma_lines As New System.Data.DataTable
        If ll_testing Then
            'haal alle rma lines op van de klant
            Dim qsc_rma_lines As New Utilize.Data.QuerySelectClass
            qsc_rma_lines.select_fields = "*"
            qsc_rma_lines.select_from = "uws_rma_line"
            qsc_rma_lines.select_where = "hdr_id in (select rma_code from uws_rma where cust_id = @cust_id)"
            qsc_rma_lines.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)
            dt_rma_lines = qsc_rma_lines.execute_query()

            ll_testing = dt_rma_lines.Rows.Count > 0
        End If

        If ll_testing Then
            'maak een list aan waarin alle lines indexes van de lines komen te staan waarbij er geen mogelijkheid meer is om een retour aan te maken
            Dim deletable_rows As New List(Of Integer)

            'loop door de order history heen
            For i As Integer = 0 To dt_orders.Rows.Count - 1
                'selecteer de rma's die de crec id hebben van de order waarvoor op het moment gecheckt wordt
                'Dim dt_selected_rmas() As System.Data.DataRow = dt_rmas.Select("ord_nr = '" + dt_orders.Rows(i).Item("crec_id").ToString() + "'")

                Dim selected_rma_lines() As System.Data.DataRow = dt_rma_lines.Select("ord_nr = '" + dt_orders.Rows(i).Item("crec_id").ToString() + "'")

                'haal de order lines op van de huidige order
                Dim dt_selected_order_lines() As System.Data.DataRow = dt_order_lines.Select("hdr_id = '" + dt_orders.Rows(i).Item("crec_id").ToString() + "'")

                Dim has_rma As Boolean = False
                'check of deze order al een rma heeft
                If Not selected_rma_lines.GetLength(0) = 0 Then
                    has_rma = True
                End If

                'als de order een rma heeft wordt er gecontroleerd of er nog producten zijn die nog niet retour gestuurd zijn
                If has_rma Then
                    'maak een datatabel aan waarin de hoeveelheden van de producten bijgehouden kan worden
                    Dim order_lines As New System.Data.DataTable
                    Dim col1 As System.Data.DataColumn = New System.Data.DataColumn("prod_code", System.Type.GetType("System.String"))
                    Dim col2 As System.Data.DataColumn = New System.Data.DataColumn("current", System.Type.GetType("System.Int32"))
                    Dim col3 As System.Data.DataColumn = New System.Data.DataColumn("max", System.Type.GetType("System.Int32"))
                    order_lines.Columns.Add(col1)
                    order_lines.Columns.Add(col2)
                    order_lines.Columns.Add(col3)

                    'voor elke order regel van deze order wordt er een max geset
                    For Each row As System.Data.DataRow In dt_selected_order_lines
                        Dim dr_new As System.Data.DataRow = order_lines.NewRow()
                        dr_new.Item("prod_code") = row.Item("prod_code")
                        dr_new.Item("current") = 0
                        dr_new.Item("max") = row.Item("ord_qty")
                        order_lines.Rows.Add(dr_new)
                    Next

                    'haal de rma lines op die bij de huidige rma hoort

                    'voeg het aantal geretourneerde producten toe aan de huidige hoeveelheid
                    For Each rma_line As System.Data.DataRow In selected_rma_lines
                        For Each order_line As System.Data.DataRow In order_lines.Rows
                            If rma_line.Item("prod_code") = order_line.Item("prod_code") Then
                                order_line.Item("current") += rma_line.Item("return_qty")
                            End If
                        Next
                    Next

                    Dim any_left As Boolean = False
                    'check of er nog producten zijn in de order die geretourneerd kunnen worden
                    For Each row As System.Data.DataRow In order_lines.Rows
                        If Integer.Parse(row.Item("current")) < Integer.Parse(row.Item("max")) Then
                            any_left = True
                        End If
                    Next
                    'als de order geen lines meer over heeft wordt de index toegevoegd aan de list van indexen die verwijderd moeten worden
                    If Not any_left Then
                        deletable_rows.Add(i)
                    End If
                End If
            Next
            'loop door alle indexen heen die verwijderd moeten worden en verwijder deze 
            If deletable_rows.Count > 0 Then
                For Each index As Integer In deletable_rows
                    dt_table.Rows(index).Delete()
                Next
            End If

        End If

        'geef de tabel terug waar de lines die niet meer gebruikt kunnen worden niet meer in staan
        Return dt_table
    End Function
#End Region

    Private WithEvents txt_product_code As New product_code
    Private WithEvents txt_bulk_product As New product_code_bulk

#Region "Page subs"

    Protected Sub pageInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.lb_tab_orders.Attributes.Add("href", "#" + Me.pnl_orders.ClientID)
        Me.lb_tab_bulk.Attributes.Add("href", "#" + Me.pnl_bulk_return.ClientID)

        Me.lb_tab_orders.Attributes.Add("onclick", "$('#" + Me.hih_active_tab.ClientID + "').val('pnl_orders');")
        Me.lb_tab_bulk.Attributes.Add("onclick", "$('#" + Me.hih_active_tab.ClientID + "').val('pnl_bulk_return');")

        'als de user niet is ingelogd dan wordt de gebruiker geredirect naar de login page
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/" + me.global_ws.language + "/Webshop/login.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.ph_product_code_alt.Controls.Add(txt_product_code)
        txt_product_code.customer_id = Me.global_ws.user_information.user_customer_id
        txt_product_code.language = Me.global_ws.Language
        txt_product_code.ID = "product_code"

        Me.ph_product_search.Controls.Add(txt_bulk_product)
        txt_bulk_product.customer_id = Me.global_ws.user_information.user_customer_id
        txt_bulk_product.language = Me.global_ws.Language
        txt_bulk_product.ID = "product_code_bulk"

        Me.set_style_sheet("uc_return_request.css", Me)
        Me.set_style_sheet("~/RMA/Styles/dropzone.css", Me)
        Me.set_javascript("uc_return_request.js", Me)
        Me.set_javascript("~/RMA/Scripts/dropzone.js", Me)

        If Not Me.IsPostBack Then
            Dim dt_orders As New System.Data.DataTable
            dt_orders = Me.get_order()

            'Controle of de klant wel bestellingen heeft
            If dt_orders.Rows.Count > 0 Then
                rpt_return_orders.DataSource = dt_orders
                rpt_return_orders.DataBind()
            Else
                Me.ph_returns.Visible = False
                Me.ph_no_orders.Visible = True
            End If
        End If


        Me.rpt_bulk_detail.DataSource = Me.global_rma.ubo_bulk_return.data_tables("uws_rma_line").data_table

        If Not Me.IsPostBack Then
            Me.global_rma.ubo_alt_product = Utilize.Data.DataProcedures.CreateRecordObject("uws_rma", "", "rma_code")
            Me.global_rma.ubo_alt_product.table_insert_row("uws_rma", Me.global_ws.user_information.user_id)
            Me.lbl_current_rma_code.Text = Me.global_rma.ubo_alt_product.field_get("uws_rma.rma_code")

            Me.global_rma.ubo_bulk_return = Utilize.Data.DataProcedures.CreateRecordObject("uws_rma", "", "rma_code")
            Me.global_rma.ubo_bulk_return.table_insert_row("uws_rma", Me.global_ws.user_information.user_id)
            Me.lbl_current_rma_bulk.Text = Me.global_rma.ubo_bulk_return.field_get("uws_rma.rma_code")
        End If

        If Me.IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "reinstate_after_postback", "reinstate_after_postback('" + Me.ResolveUrl("~/RMA/UploadImages.ashx") + "', '" + Me.global_rma.ubo_bulk_return.field_get("uws_rma.rma_code") + "', '" + Me.global_rma.ubo_alt_product.field_get("uws_rma.rma_code") + "', '" + Me.li_bulk.ClientID + "', '" + Me.li_orders.ClientID + "');", True)
            check_bulk_list()
        End If

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        'set de redenen
        Dim qsc_reasons As New Utilize.Data.QuerySelectClass
        qsc_reasons.select_fields = "distinct reason_code, reason_desc_" + Me.global_ws.Language + ", row_ord"
        qsc_reasons.select_from = "uws_rma_reason"
        qsc_reasons.select_order = "row_ord"

        dt_reason = qsc_reasons.execute_query()

        Dim trn_nr As String = "1"

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_history_del") Then
            trn_nr = "9"
        End If

        If trn_nr = "1" Then
            Me.ph_search_invoice_nr.Visible = False
            Me.col_order_number.Text = global_trans.translate_label("label_order_number", "Ordernummer", Me.global_ws.Language)
        ElseIf trn_nr = "9" Then
            Me.ph_search_order_nr.Visible = False
            col_order_number.Text = global_trans.translate_label("label_invoice_number", "Factuurnummer", Me.global_ws.Language)
        End If

        If Not Me.get_page_parameter("order") = "" Then
            Me.set_order_details(Me.get_page_parameter("order"))
        End If
    End Sub

    Protected Sub change_tab(sender As Object, e As System.EventArgs) Handles lb_tab_orders.Click, lb_tab_bulk.Click
        If Me.button_add.Visible Then
            Me.button_add.Visible = False
        Else
            Me.button_add.Visible = True
        End If
    End Sub

    Protected Sub txt_product_code_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_product_code.TextChanged
        Dim lc_product_description As String = ""

        Dim ll_resume As Boolean = True
        Me.txt_product_code.Text = Me.txt_product_code.Text.ToUpper()

        If ll_resume Then
            ll_resume = Not Me.txt_product_code.Text = ""

            If Not ll_resume Then
                Me.txt_product_code.Text = ""
            End If
        End If

        If ll_resume Then
            lc_product_description = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_desc", Me.txt_product_code.Text)

            ll_resume = Not lc_product_description.Trim() = ""

            If Not ll_resume Then
                Me.txt_product_code.Text = ""

            End If
        End If
    End Sub

    Protected Sub show_photo_upload(sender As Object, e As System.EventArgs)

        For Each item In Me.rpt_return_order_detail.Items
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("cb_add_photo")

            Dim label_product_code As System.Web.UI.WebControls.Label
            label_product_code = item.findcontrol("row_prod_code")

            Dim ph_photo_upload As Utilize.Web.UI.WebControls.placeholder
            ph_photo_upload = item.findcontrol("ph_photo_upload")

            Dim div_dropzone As System.Web.UI.HtmlControls.HtmlGenericControl
            div_dropzone = item.findcontrol("dropzoneForm")

            If DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text = label_product_code.Text Then
                If Not ph_photo_upload.Visible Then
                    ph_photo_upload.Visible = True
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "register_upload_control" + cb_line.UniqueID, "dropzone_init('" + div_dropzone.ClientID + "','" + Me.ResolveUrl("~/RMA/UploadImages.ashx") + "?rmacode=" + Me.global_rma.ubo_alt_product.field_get("rma_code") + "&prodcode=" + label_product_code.Text + "')", True)
                Else
                    ph_photo_upload.Visible = False
                End If
            End If
        Next
    End Sub

    Protected Sub set_general_reason(sender As Object, e As System.EventArgs)
        'For Each item In Me.rpt_return_order_detail.Items
        '    Dim cb_line As System.Web.UI.WebControls.CheckBox
        '    cb_line = item.findcontrol("row_check")

        '    Dim dd_return_reason_single As dropdownlist
        '    dd_return_reason_single = item.findcontrol("dd_return_reason_single")

        '    If cb_line.Checked Then
        '        If Not Me.dd_return_reason.SelectedValue = "DEFAULT" And dd_return_reason_single.SelectedValue = "DEFAULT" Then
        '            dd_return_reason_single.SelectedValue = Me.dd_return_reason.SelectedValue
        '        End If
        '    End If
        'Next
    End Sub

    Protected Sub show_input(sender As Object, e As System.EventArgs)
        Dim has_one_checked As Boolean = False
        For Each item In Me.rpt_return_order_detail.Items
            'cast de controls van elke item naar het item wat ze zijn zodat ze gebruikt kunnen worden
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")

            Dim label_product_code As System.Web.UI.WebControls.Label
            label_product_code = item.findcontrol("row_prod_code")

            Dim txt_amount As TextBox
            txt_amount = item.findcontrol("row_input_amt")

            'checkbox voor de photo upload
            Dim cb_photo As System.Web.UI.WebControls.CheckBox
            cb_photo = item.findcontrol("cb_add_photo")

            Dim dd_return_reason_single As DropDownList
            dd_return_reason_single = item.findcontrol("dd_return_reason_single")

            If cb_line.Checked Then
                has_one_checked = True
            End If

            'Check of de sender en de current row overeen komen om de textbox visible te maken
            If DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text = label_product_code.Text Then
                If Not txt_amount.Visible Then
                    txt_amount.Visible = True
                    cb_photo.Visible = True
                    dd_return_reason_single.Visible = True

                    If dd_return_reason_single.Items.Count = 0 Then
                        dd_return_reason_single.DataSource = dt_reason
                        dd_return_reason_single.DataTextField = "reason_desc_" + Me.global_ws.Language
                        dd_return_reason_single.DataValueField = "reason_code"
                        dd_return_reason_single.DataBind()

                        dd_return_reason_single.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language), .Value = "DEFAULT"})
                    End If
                    'If Not Me.dd_return_reason.SelectedValue = "DEFAULT" Then
                    '    dd_return_reason_single.SelectedValue = Me.dd_return_reason.SelectedValue
                    'End If
                Else
                    txt_amount.Visible = False
                    cb_photo.Visible = False
                    dd_return_reason_single.Visible = False

                    Dim ph_photo_upload As Utilize.Web.UI.WebControls.placeholder
                    ph_photo_upload = item.findcontrol("ph_photo_upload")

                    If ph_photo_upload.Visible Then
                        ph_photo_upload.Visible = False
                        cb_photo.Checked = False
                    End If
                End If
            End If
        Next
        col_amount_select.Visible = has_one_checked
        label_photo_upload.Visible = has_one_checked
        label_return_reason.Visible = has_one_checked
        set_sub_totals()
    End Sub

    Private Sub check_all_CheckedChanged(sender As Object, e As EventArgs) Handles check_all.CheckedChanged
        'deze sub loopt door alle checkboxes heen in de repeater met de producten van de order en check alle boxes die nog niet gechecked zijn
        If Me.check_all.Checked = True Then
            For Each item In Me.rpt_return_order_detail.Items
                Dim cb_line As System.Web.UI.WebControls.CheckBox
                cb_line = item.findcontrol("row_check")

                Dim txt_amount As TextBox
                txt_amount = item.findcontrol("row_input_amt")

                Dim cb_photo As System.Web.UI.WebControls.CheckBox
                cb_photo = item.findcontrol("cb_add_photo")

                Dim dd_return_reason_single As DropDownList
                dd_return_reason_single = item.findcontrol("dd_return_reason_single")

                If cb_line.Visible Then
                    cb_line.Checked = True
                    cb_photo.Visible = True
                    txt_amount.Visible = True
                    col_amount_select.Visible = True
                    label_photo_upload.Visible = True
                    label_return_reason.Visible = True
                    dd_return_reason_single.Visible = True

                    If dd_return_reason_single.Items.Count = 0 Then
                        dd_return_reason_single.DataSource = dt_reason
                        dd_return_reason_single.DataTextField = "reason_desc_" + Me.global_ws.Language
                        dd_return_reason_single.DataValueField = "reason_code"
                        dd_return_reason_single.DataBind()

                        dd_return_reason_single.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language), .Value = "DEFAULT"})

                    End If

                    'If Not Me.dd_return_reason.SelectedValue = "DEFAULT" Then
                    '    dd_return_reason_single.SelectedValue = Me.dd_return_reason.SelectedValue
                    'End If

                End If
            Next
        Else
            For Each item In Me.rpt_return_order_detail.Items
                Dim cb_line As System.Web.UI.WebControls.CheckBox
                cb_line = item.findcontrol("row_check")
                Dim txt_amount As TextBox
                txt_amount = item.findcontrol("row_input_amt")

                Dim cb_photo As System.Web.UI.WebControls.CheckBox
                cb_photo = item.findcontrol("cb_add_photo")

                Dim ph_photo_upload As Utilize.Web.UI.WebControls.placeholder
                ph_photo_upload = item.findcontrol("ph_photo_upload")

                Dim dd_return_reason_single As DropDownList
                dd_return_reason_single = item.findcontrol("dd_return_reason_single")

                cb_photo.Visible = False
                cb_line.Checked = False
                txt_amount.Visible = False
                label_photo_upload.Visible = False
                col_amount_select.Visible = False
                label_return_reason.Visible = False
                dd_return_reason_single.Visible = False

                If ph_photo_upload.Visible Then
                    ph_photo_upload.Visible = False
                    cb_photo.Checked = False
                End If
            Next
        End If

        set_sub_totals()
    End Sub

    Private Sub set_order_details(ord_id As String)
        ph_returns_detail.Visible = True
        ph_returns.Visible = False
        button_close_details.Visible = True
        button_proceed.Visible = True

        Me.txt_return_reason_desc.Text = ""
        Me.ph_amount_error.Visible = False
        Me.ph_reason_comp_error.Visible = False
        Me.ph_reason_error.Visible = False

        Me.hid_ord_nr.Value = ord_id

        Dim qsc_order As New Utilize.Data.QuerySelectClass
        qsc_order.select_fields = "*"
        qsc_order.select_from = "uws_history"
        qsc_order.select_where = "crec_id = @crec_id"
        qsc_order.add_parameter("@crec_id", ord_id)
        Dim udt_order As New System.Data.DataTable
        udt_order = qsc_order.execute_query()

        If udt_order.Rows.Count > 0 Then
            lbl_order_nummer.Text = udt_order.Rows(0).Item("doc_nr")
            lbl_order_date.Text = udt_order.Rows(0).Item("ord_date")
            lbl_order_description.Text = udt_order.Rows(0).Item("ord_desc")
        End If

        rpt_return_order_detail.DataSource = Me.get_order_detail(ord_id)
        rpt_return_order_detail.DataBind()

        'set de placeholder voor de "aantal" textvelden
        For Each item In Me.rpt_return_order_detail.Items
            Dim txt_amount As TextBox
            txt_amount = item.findcontrol("row_input_amt")
            txt_amount.Attributes.Add("placeholder", "aantal")
        Next

        'set de de compensaties 
        Dim qsc_comps As New Utilize.Data.QuerySelectClass
        qsc_comps.select_fields = "*"
        qsc_comps.select_from = "uws_rma_compensation"
        'qsc_comps.select_where = "1=0"

        Dim dt_comp As New System.Data.DataTable
        dt_comp = qsc_comps.execute_query()

        Me.dd_return_comp.DataSource = dt_comp
        Me.dd_return_comp.DataTextField = "comp_desc_" + Me.global_ws.Language
        Me.dd_return_comp.DataValueField = "comp_code"
        Me.dd_return_comp.DataBind()

        dd_return_comp.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_compensation_def", "-selecteer een compensatie methode-", Me.global_ws.Language), .Value = "0000"})


        'Me.dd_return_reason.DataSource = dt_reason
        'Me.dd_return_reason.DataTextField = "reason_desc_" + Me.global_ws.Language
        'Me.dd_return_reason.DataValueField = "reason_code"
        'Me.dd_return_reason.DataBind()

        Me.check_all.Checked = False

        check_availability_detail(ord_id)
    End Sub

    'Protected Sub fill_compensation(sender As Object, e As System.EventArgs)
    '    'set de de compensaties van de reden in de lijst
    '    Dim qsc_comps As New Utilize.Data.QuerySelectClass
    '    qsc_comps.select_fields = "*"
    '    qsc_comps.select_from = "uws_rma_compensation"
    '    qsc_comps.select_where = "comp_code in (select comp_code from uws_rma_rsn_comp where " + Me.dd_return_reason.DataValueField + " = '" + Me.dd_return_reason.SelectedItem.Value + "')"

    '    Dim dt_comp As New System.Data.DataTable
    '    dt_comp = qsc_comps.execute_query()

    '    'voeg een extra regel toe als dummy waarde
    '    Dim top_row_default_comp As System.Data.DataRow = dt_comp.NewRow()
    '    top_row_default_comp("comp_desc_" + Me.global_ws.Language) = global_trans.translate_label("label_compensation_def", "-selecteer een compensatie methode-", Me.global_ws.Language)
    '    top_row_default_comp("comp_code") = "0000"
    '    dt_comp.Rows.InsertAt(top_row_default_comp, 0)

    '    Me.dd_return_comp.DataSource = dt_comp
    '    Me.dd_return_comp.DataTextField = "comp_desc_" + Me.global_ws.Language
    '    Me.dd_return_comp.DataValueField = "comp_code"
    '    Me.dd_return_comp.DataBind()

    '    check_alternative_product()
    'End Sub

    Protected Sub check_alternative_product(dd_item As DropDownList)
        'Alternative product check if to show visible or not
        Dim qsc_comp As New Utilize.Data.QuerySelectClass
        qsc_comp.select_fields = "comp_type"
        qsc_comp.select_from = "uws_rma_compensation"
        qsc_comp.select_where = "comp_code = @comp_code"
        qsc_comp.add_parameter("@comp_code", dd_item.SelectedItem.Value)

        Dim dt_comp_check As System.Data.DataTable = qsc_comp.execute_query()

        If dt_comp_check.Rows.Count > 0 Then
            If dt_comp_check.Rows(0).Item("comp_type").ToString() = "1" Then
                Me.ph_alt_product_selectie.Visible = True
            Else
                Me.ph_alt_product_selectie.Visible = False
            End If
        Else
            Me.ph_alt_product_selectie.Visible = False
        End If
    End Sub

    Protected Sub alternative_product(sender As Object, e As System.EventArgs)
        check_alternative_product(Me.dd_return_comp)
        set_sub_totals()
    End Sub

    Protected Sub alternative_product_bulk(sender As Object, e As System.EventArgs)
        Dim alt_prod_before As Boolean = Me.ph_alt_product_selectie.Visible
        check_alternative_product(Me.dd_bulk_comp_method)
        If Me.ph_alt_product_selectie.Visible And Not alt_prod_before Then
            set_sub_totals_bulk()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "move_alt_product", "move_product_selector();", True)
        ElseIf Not Me.ph_alt_product_selectie.Visible And alt_prod_before Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "move_alt_product_back", "move_product_selector_back();", True)
        End If
    End Sub

    Protected Sub rpt_orders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_return_orders.ItemCommand
        If e.CommandName = "number" Then
            Dim lc_order_nr = e.CommandArgument

            Me.set_order_details(lc_order_nr)
        End If
    End Sub

    Private Sub check_availability_detail(ord_nr As String)
        Dim ll_testing As Boolean = True

        'haal alle rma's op die het bij de order horen die het meegegeven order nummer heeft
        Dim qsc_rma As New Utilize.Data.QuerySelectClass
        qsc_rma.select_fields = "*"
        qsc_rma.select_from = "uws_rma"
        qsc_rma.select_where = "ord_nr = @ord_nr and not req_status = 'Afgekeurd'"
        qsc_rma.add_parameter("@ord_nr", ord_nr)
        Dim dt_rma As System.Data.DataTable = qsc_rma.execute_query()

        Dim qsc_rma_lines As New Utilize.Data.QuerySelectClass
        qsc_rma_lines.select_fields = "*"
        qsc_rma_lines.select_from = "uws_rma_line"
        qsc_rma_lines.select_where = "hdr_id in (select rma_code from uws_rma where not req_status = 'Afgekeurd') and ord_nr = @ord_nr"
        qsc_rma_lines.add_parameter("@ord_nr", ord_nr)
        Dim dt_rma_lines As System.Data.DataTable = qsc_rma_lines.execute_query()

        ll_testing = dt_rma.Rows.Count > 0 Or dt_rma_lines.Rows.Count > 0

        'als er meer dan 1 rma is doe dan de volgende code
        If ll_testing Then

            'zet de placeholders voor de hoeveelheden die over zijn op zichtbaar
            Me.ph_amount_left_col.Visible = True
            For Each item In rpt_return_order_detail.Items
                Dim placeholder_row_amt As New Utilize.Web.UI.WebControls.placeholder
                placeholder_row_amt = item.findcontrol("ph_amount_left_row")
                placeholder_row_amt.Visible = True
            Next

            Dim dt_history_lines As System.Data.DataTable = get_order_detail(ord_nr)
            For Each history_row As System.Data.DataRow In dt_history_lines.Rows
                Dim dr_rows() As System.Data.DataRow = dt_rma_lines.Select("prod_code = '" + history_row.Item("prod_code").ToString() + "'")
                Dim prod_amount As Integer = 0
                Dim total_amount As Integer = Convert.ToInt32(history_row.Item("ord_qty"))
                For Each rma_row As System.Data.DataRow In dr_rows
                    prod_amount += Convert.ToInt32(rma_row.Item("return_qty"))
                Next
                Dim amt_available = total_amount - prod_amount
                For Each item In rpt_return_order_detail.Items
                    Dim row_prod_code As New Utilize.Web.UI.WebControls.label
                    row_prod_code = item.findcontrol("row_prod_code")
                    If row_prod_code.Text = history_row.Item("prod_code") Then
                        Dim row_amt_left As New Utilize.Web.UI.WebControls.label
                        row_amt_left = item.findcontrol("row_amt_left")
                        row_amt_left.Text = amt_available.ToString()

                        If amt_available = 0 Then
                            Dim checkbox As New System.Web.UI.WebControls.CheckBox
                            checkbox = item.findcontrol("row_check")
                            checkbox.Visible = False
                        End If
                    End If
                Next
            Next
        End If

        If Not ll_testing Then
            Me.ph_amount_left_col.Visible = False
            For Each item In rpt_return_order_detail.Items
                Dim placeholder_row_amt As New Utilize.Web.UI.WebControls.placeholder
                placeholder_row_amt = item.findcontrol("ph_amount_left_row")
                placeholder_row_amt.Visible = False
            Next
        End If

    End Sub

    Protected Sub show_bulk_input(sender As Object, e As System.EventArgs)
        Dim has_one_checked As Boolean = False
        For Each item In Me.rpt_bulk_return.Items
            'cast de controls van elke item naar het item wat ze zijn zodat ze gebruikt kunnen worden
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")

            Dim label_product_code As System.Web.UI.WebControls.Label
            label_product_code = item.findcontrol("row_prod_code")

            Dim label_doc_nr As System.Web.UI.WebControls.Label
            label_doc_nr = item.findcontrol("row_doc_nr")

            Dim ph_return_bulk As Utilize.Web.UI.WebControls.placeholder
            ph_return_bulk = item.findcontrol("ph_return_bulk")

            Dim txt_amount As TextBox
            txt_amount = item.findcontrol("row_input_amt")

            Dim return_amount As translatelabel
            return_amount = item.findcontrol("label_bulk_return_amount")

            Dim label_bulk_reason As translatelabel
            label_bulk_reason = item.findcontrol("label_bulk_reason")

            Dim label_bulk_photo_upload As translatelabel
            label_bulk_photo_upload = item.findcontrol("label_bulk_photo_upload")

            'checkbox voor de photo upload
            Dim cb_photo As System.Web.UI.WebControls.CheckBox
            cb_photo = item.findcontrol("cb_add_photo")

            Dim dd_return_reason_single As DropDownList
            dd_return_reason_single = item.findcontrol("dd_return_reason_single")

            If cb_line.Checked Then
                has_one_checked = True
            End If

            Dim row_prod_code As String = DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.Substring(0, DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.LastIndexOf("&"))
            Dim row_doc_nr As String = DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.Substring(DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.LastIndexOf("&") + 1)
            Dim ord_nr As String = item.findcontrol("hid_ord_id").text
            'Check of de sender en de current row overeen komen om de textbox visible te maken
            If row_prod_code = label_product_code.Text And row_doc_nr = label_doc_nr.Text Then
                If Not ph_return_bulk.Visible Then
                    ph_return_bulk.Visible = True

                    If dd_return_reason_single.Items.Count = 0 Then
                        dd_return_reason_single.DataSource = dt_reason
                        dd_return_reason_single.DataTextField = "reason_desc_" + Me.global_ws.Language
                        dd_return_reason_single.DataValueField = "reason_code"
                        dd_return_reason_single.DataBind()

                        dd_return_reason_single.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_reason_select", "-selecteer een reden-", Me.global_ws.Language), .Value = "DEFAULT"})
                    End If
                    'If Not Me.dd_return_reason.SelectedValue = "DEFAULT" Then
                    '    dd_return_reason_single.SelectedValue = Me.dd_return_reason.SelectedValue
                    'End If
                Else
                    ph_return_bulk.Visible = False

                    Dim ph_photo_upload As Utilize.Web.UI.WebControls.placeholder
                    ph_photo_upload = item.findcontrol("ph_photo_upload")

                    If ph_photo_upload.Visible Then
                        ph_photo_upload.Visible = False
                        cb_photo.Checked = False
                    End If
                End If
            End If
        Next
        set_sub_totals_bulk()
    End Sub

    Protected Sub checkBulkAmount(sender As Object, e As System.EventArgs)
        'loop door alle elementen heen
        Dim found As Boolean = False
        For Each item In Me.rpt_bulk_return.Items
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")

            If cb_line.Checked Then

                Dim txt_amount As TextBox
                txt_amount = item.findcontrol("row_input_amt")
                'check of de id van de text boxes gelijk zijn aan elkaar, als dit waar is zit je in de goede item
                If txt_amount.UniqueID = CType(sender, TextBox).UniqueID And Not found Then
                    found = True
                    Dim label_amount_left As New Utilize.Web.UI.WebControls.label
                    label_amount_left = item.findcontrol("row_amt_left")

                    'als het getal in de textbox hoger is dan het getal wat orgineel besteld was dan wordt het getal geset naar het orgineel bestelde aantal.
                    Dim textbox_number As Int32 = Nothing
                    If Not CType(sender, TextBox).Text = "" Then
                        If Int32.TryParse(CType(sender, TextBox).Text, textbox_number) Then
                            If textbox_number > Integer.Parse(label_amount_left.Text) Or textbox_number < 0 Then
                                CType(sender, TextBox).Text = label_amount_left.Text
                            End If
                        Else
                            CType(sender, TextBox).Text = label_amount_left.Text
                        End If
                    End If
                    'Dim qsc_comp As New Utilize.Data.QuerySelectClass
                    'qsc_comp.select_fields = "comp_type"
                    'qsc_comp.select_from = "uws_rma_compensation"
                    'qsc_comp.select_where = "comp_code = @comp_code"
                    'qsc_comp.add_parameter("@comp_code", Me.dd_return_comp.SelectedItem.Value)

                    'Dim dt_comp_check As System.Data.DataTable = qsc_comp.execute_query()
                    'If dt_comp_check.Rows.Count > 0 Then
                    '    If dt_comp_check.Rows(0).Item("comp_type").ToString() = "1" Then
                    '        set_sub_totals()
                    '    End If
                    'End If
                End If
            End If
        Next
    End Sub

    Protected Sub show_photo_upload_bulk(sender As Object, e As System.EventArgs)

        For Each item In Me.rpt_bulk_return.Items
            Dim cb_row_check As System.Web.UI.WebControls.CheckBox
            cb_row_check = item.findcontrol("row_check")

            If cb_row_check.Checked Then
                Dim cb_line As System.Web.UI.WebControls.CheckBox
                cb_line = item.findcontrol("cb_add_photo")

                Dim label_product_code As System.Web.UI.WebControls.Label
                label_product_code = item.findcontrol("row_prod_code")

                Dim label_doc_nr As System.Web.UI.WebControls.Label
                label_doc_nr = item.findcontrol("row_doc_nr")

                Dim ph_photo_upload As Utilize.Web.UI.WebControls.placeholder
                ph_photo_upload = item.findcontrol("ph_photo_upload")

                Dim div_dropzone As System.Web.UI.HtmlControls.HtmlGenericControl
                div_dropzone = item.findcontrol("dropzoneForm")

                Dim row_prod_code As String = DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.Substring(0, DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.LastIndexOf("&"))
                Dim row_doc_nr As String = DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.Substring(DirectCast(sender, System.Web.UI.WebControls.CheckBox).Text.LastIndexOf("&") + 1)

                If row_prod_code = label_product_code.Text And row_doc_nr = label_doc_nr.Text Then
                    If Not ph_photo_upload.Visible Then
                        ph_photo_upload.Visible = True
                        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "register_upload_control" + cb_line.UniqueID, "dropzone_init('" + div_dropzone.ClientID + "','" + Me.ResolveUrl("~/RMA/UploadImages.ashx") + "?rmacode=" + Me.global_rma.ubo_alt_product.field_get("rma_code") + "&prodcode=" + label_product_code.Text + "')", True)
                    Else
                        ph_photo_upload.Visible = False
                    End If
                End If
            End If

        Next
    End Sub

    Protected Sub button_add_bulk_item_click(sender As Object, e As System.EventArgs) Handles button_add1.Click
        Dim ll_resume As Boolean = True
        Dim found As Boolean = False
        Dim ll_no_input_empty As Boolean = True
        Dim ll_no_reason As Boolean = True

        If ll_resume Then
            For Each item In rpt_bulk_return.Items
                Dim cb_row_check As System.Web.UI.WebControls.CheckBox
                cb_row_check = item.findcontrol("row_check")
                If cb_row_check.Checked Then
                    found = True

                    Dim txt_amount As TextBox
                    txt_amount = item.findcontrol("row_input_amt")

                    If txt_amount.Text = "" Then
                        ll_no_input_empty = False
                    End If

                    Dim dd_return_reason_single As DropDownList
                    dd_return_reason_single = item.findcontrol("dd_return_reason_single")

                    If dd_return_reason_single.SelectedValue = "DEFAULT" Then
                        ll_no_reason = False
                    End If

                End If
            Next
            ll_resume = found And ll_no_input_empty And ll_no_reason
        End If

        If Not ll_resume Then
            If Not found Then
                alert_none_selected.Text = global_trans.translate_message("text_none_selected", "Er zijn geen producten geselecteerd", Me.global_cms.Language)
                alert_none_selected.Visible = True
            End If

            If ll_no_reason Then
                alert_none_selected.Text = global_trans.translate_message("text_no_reason", "In een van de regels is geen reden meegegeven", Me.global_cms.Language)
                alert_none_selected.Visible = True
            End If

            If ll_no_input_empty Then
                alert_none_selected.Text = global_trans.translate_message("text_no_input", "In een van de regels is geen hoeveelheid ingevuld", Me.global_cms.Language)
                alert_none_selected.Visible = True
            End If
        Else
            alert_none_selected.Visible = False
        End If

        If ll_resume Then
            For Each item In Me.rpt_bulk_return.Items
                Dim cb_line As System.Web.UI.WebControls.CheckBox
                cb_line = item.findcontrol("row_check")

                If cb_line.Checked Then
                    'cast alle webcontrols uit het item naar het juiste type
                    Dim label_product_code As System.Web.UI.WebControls.Label
                    label_product_code = item.findcontrol("row_prod_code")

                    Dim label_doc_nr As Label
                    label_doc_nr = item.findcontrol("row_doc_nr")

                    Dim label_product_description As System.Web.UI.WebControls.Label
                    label_product_description = item.findcontrol("row_prod_desc")

                    Dim label_order_quantity As System.Web.UI.WebControls.Label
                    label_order_quantity = item.findcontrol("row_ord_qty")

                    Dim label_row_amount As System.Web.UI.WebControls.Label
                    label_row_amount = item.findcontrol("row_row_amt")

                    Dim txt_amount As TextBox
                    txt_amount = item.findcontrol("row_input_amt")

                    Dim label_vat_amt As System.Web.UI.WebControls.Label
                    label_vat_amt = item.findcontrol("hid_btw_amt")

                    Dim label_crec_id As System.Web.UI.WebControls.Label
                    label_crec_id = item.findcontrol("hid_crec_id")

                    Dim dd_return_reason_single As DropDownList
                    dd_return_reason_single = item.findcontrol("dd_return_reason_single")

                    Dim lt_reason_code As String = dd_return_reason_single.SelectedValue

                    Dim qsc_history_order_lines As New Utilize.Data.QuerySelectClass
                    qsc_history_order_lines.select_fields = "*"
                    qsc_history_order_lines.select_from = "uws_history_lines"
                    qsc_history_order_lines.select_where = "crec_id = @crec_id"
                    qsc_history_order_lines.add_parameter("@crec_id", label_crec_id.Text)
                    Dim dt_history_lines As System.Data.DataTable = qsc_history_order_lines.execute_query()

                    Dim lt_total_return_amt As Decimal = calc_row_amt(dt_history_lines.Rows(0).Item("row_amt"), dt_history_lines.Rows(0).Item("ord_qty"), Convert.ToInt32(txt_amount.Text))
                    Dim lt_row_btw As String = calc_row_btw(dt_history_lines.Rows(0).Item("vat_amt"), dt_history_lines.Rows(0).Item("ord_qty"), Convert.ToInt32(txt_amount.Text)).ToString()

                    Dim qsc_vat_code As New Utilize.Data.QuerySelectClass
                    qsc_vat_code.select_fields = "vat_code"
                    qsc_vat_code.select_from = "uws_products"
                    qsc_vat_code.select_where = "prod_code = @prod_code"
                    qsc_vat_code.add_parameter("@prod_code", label_product_code.Text)
                    Dim dt_vat_code As System.Data.DataTable = qsc_vat_code.execute_query()

                    'maakt een nieuwe rma line row aan en vul deze
                    ll_resume = Me.global_rma.ubo_bulk_return.table_insert_row("uws_rma_line", Me.global_ws.user_information.user_id)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.prod_code", label_product_code.Text)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.prod_desc", label_product_description.Text)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.doc_nr", label_doc_nr.Text)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.return_qty", txt_amount.Text)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.org_return_qty", txt_amount.Text)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.row_amt", lt_total_return_amt)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.org_row_amt", lt_total_return_amt)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.ord_nr", dt_history_lines.Rows(0).Item("hdr_id"))
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.vat_code", dt_vat_code.Rows(0).Item("vat_code"))
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.vat_amt", lt_row_btw)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.org_vat_amt", lt_row_btw)
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.reason_code", lt_reason_code)

                    Me.global_rma.ubo_bulk_return.table_update(Me.global_ws.user_information.user_id)
                End If
            Next
        End If

        If ll_resume Then
            If Not Me.ph_bulk_details.Visible Then
                'haal compensaties op
                Dim qsc_comps As New Utilize.Data.QuerySelectClass
                qsc_comps.select_fields = "*"
                qsc_comps.select_from = "uws_rma_compensation"
                'qsc_comps.select_where = "1=0"

                Dim dt_comp As New System.Data.DataTable
                dt_comp = qsc_comps.execute_query()

                Me.dd_bulk_comp_method.DataSource = dt_comp
                Me.dd_bulk_comp_method.DataTextField = "comp_desc_" + Me.global_ws.Language
                Me.dd_bulk_comp_method.DataValueField = "comp_code"
                Me.dd_bulk_comp_method.DataBind()

                dd_bulk_comp_method.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_compensation_def", "-selecteer een compensatie methode-", Me.global_ws.Language), .Value = "0000"})

                Me.ph_bulk_details.Visible = True
            End If
            Me.rpt_bulk_detail.DataBind()
            check_bulk_list()
            If Me.ph_alt_product_selectie.Visible Then
                set_sub_totals_bulk()
            End If
            set_bulk_images()

        End If
    End Sub

    Public Sub check_bulk_list()
        If Me.ph_bulk_details.Visible Then
            Dim dt_rpt_bulk_list As System.Data.DataView = Me.rpt_bulk_return.DataSource
            Dim ll_move_next As Boolean = Me.global_rma.ubo_bulk_return.move_first("uws_rma_line")

            For Each rpt_item As System.Web.UI.WebControls.RepeaterItem In Me.rpt_bulk_return.Items
                If rpt_item.ItemType = UI.WebControls.ListItemType.Item Or rpt_item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
                    Dim lc_prod_code As String = CType(rpt_item.FindControl("row_prod_code"), Label).Text
                    Dim lc_doc_nr As String = CType(rpt_item.FindControl("row_doc_nr"), Label).Text

                    Me.global_rma.ubo_bulk_return.move_first("uws_rma_line")

                    If Me.global_rma.ubo_bulk_return.table_locate("uws_rma_line", "prod_code = '" + lc_prod_code + "' and doc_nr = '" + lc_doc_nr + "'") Then
                        rpt_item.Visible = False
                        Dim cb_row_check As System.Web.UI.WebControls.CheckBox
                        cb_row_check = rpt_item.FindControl("row_check")
                        cb_row_check.Checked = False
                        Dim txt_amount As TextBox
                        txt_amount = rpt_item.FindControl("row_input_amt")

                        Dim ph_bulk_return As Utilize.Web.UI.WebControls.placeholder
                        ph_bulk_return = rpt_item.FindControl("ph_return_bulk")

                        'checkbox voor de photo upload
                        Dim cb_photo As System.Web.UI.WebControls.CheckBox
                        cb_photo = rpt_item.FindControl("cb_add_photo")

                        Dim dd_return_reason_single As DropDownList
                        dd_return_reason_single = rpt_item.FindControl("dd_return_reason_single")
                        ph_bulk_return.Visible = False
                    Else
                        rpt_item.Visible = True
                    End If
                End If

            Next

            'Do While ll_move_next
            '    dt_rpt_bulk_list.RowFilter += " and (prod_code != '" + Me.global_rma.ubo_alt_product.field_get("uws_rma_line.prod_code") + "' and doc_nr != '" + Me.global_rma.ubo_alt_product.field_get("uws_rma_line.doc_nr") + "')"


            '    ll_move_next = Me.global_rma.ubo_alt_product.move_next("uws_rma_line")
            'Loop
            'Me.rpt_bulk_return.DataSource = dt_rpt_bulk_list
            'Me.rpt_bulk_return.DataBind()
        End If
    End Sub

    Protected Sub rpt_bulk_detail_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_bulk_detail.ItemCommand
        If e.CommandName = "remove" Then
            Dim lc_commandArgument = e.CommandArgument
            'verwijder de line uit het business object
            Dim ll_move_next As Boolean = Me.global_rma.ubo_bulk_return.move_first("uws_rma_line")

            Dim row_prod_code As String = lc_commandArgument.Substring(0, lc_commandArgument.LastIndexOf("&"))
            Dim row_doc_nr As String = lc_commandArgument.Substring(lc_commandArgument.LastIndexOf("&") + 1)


            Do While ll_move_next
                If Me.global_rma.ubo_bulk_return.field_get("uws_rma_line.prod_code") = row_prod_code And Me.global_rma.ubo_bulk_return.field_get("uws_rma_line.doc_nr") = row_doc_nr Then
                    Me.global_rma.ubo_bulk_return.table_delete_row("uws_rma_line", False, Me.global_ws.user_information.user_customer_id)
                    Me.global_rma.ubo_bulk_return.move_first("uws_rma_line")
                End If

                ll_move_next = Me.global_rma.ubo_bulk_return.move_next("uws_rma_line")
            Loop
            Me.rpt_bulk_detail.DataSource = Me.global_rma.ubo_bulk_return.data_tables("uws_rma_line").data_table.DefaultView
            rpt_bulk_detail.DataBind()
            check_bulk_list()
            If Me.ph_alt_product_selectie.Visible Then
                set_sub_totals_bulk()
            End If
            set_bulk_images()
        End If
    End Sub

    Private Sub rpt_bulk_return_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_bulk_return.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lt_row_amount As Utilize.Web.UI.WebControls.label = e.Item.FindControl("row_row_amt")
            lt_row_amount.Visible = Not Me.prices_not_visible
        End If
    End Sub


    Private Sub rpt_bulk_detail_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_bulk_detail.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lt_row_amount As Utilize.Web.UI.WebControls.label = e.Item.FindControl("row_row_amt")
            lt_row_amount.Visible = Not Me.prices_not_visible
        End If
    End Sub
#End Region

    Protected Sub txt_bulk_product_textchanged(sender As Object, e As System.EventArgs) Handles txt_bulk_product.TextChanged
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.txt_bulk_product.Text = ""
            If Not ll_resume Then
                Me.alert_no_found.Text = global_trans.translate_label("label_no_input", "Het zoekveld was leeg", Me.global_cms.Language)
                Me.alert_no_found.Visible = True
            Else
                Me.alert_no_found.Visible = False
            End If
        End If

        If ll_resume Then
            Dim qsc_bulk_products As New Utilize.Data.QuerySelectClass
            qsc_bulk_products.select_fields = "uws_history_lines.crec_id as line_crec, vat_amt, prod_code, prod_desc, row_amt ,ord_qty, hdr_id, uws_history.doc_nr, uws_history.ord_date, uws_history.crec_id"
            qsc_bulk_products.select_from = "uws_history_lines"
            qsc_bulk_products.add_join("left join", "uws_history", "uws_history_lines.hdr_id = uws_history.crec_id")
            qsc_bulk_products.select_where = "uws_history.cust_id = @cust_id and prod_code = @prod_code"
            qsc_bulk_products.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            qsc_bulk_products.add_parameter("prod_code", Me.txt_bulk_product.Text)
            Dim dt_bulk_products As System.Data.DataTable = qsc_bulk_products.execute_query()
            Dim col_amount_left As New System.Data.DataColumn("amt_left")
            col_amount_left.DataType = System.Type.GetType("System.Decimal")
            dt_bulk_products.Columns.Add(col_amount_left)

            Dim qsc_rma_lines As New Utilize.Data.QuerySelectClass
            qsc_rma_lines.select_fields = "hdr_id, uws_rma.req_status, uws_rma_line.ord_nr, return_qty"
            qsc_rma_lines.select_from = "uws_rma_line"
            qsc_rma_lines.add_join("left join", "uws_rma", "uws_rma_line.hdr_id = uws_rma.rma_code")
            qsc_rma_lines.select_where = "prod_code = @prod_code and uws_rma.cust_id = @cust_id and not uws_rma.req_status = 'Afgekeurd'"
            qsc_rma_lines.add_parameter("prod_code", Me.txt_bulk_product.Text)
            qsc_rma_lines.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            Dim dt_rma_lines As System.Data.DataTable = qsc_rma_lines.execute_query()

            For Each row As System.Data.DataRow In dt_bulk_products.Rows
                Dim dr_rows() As System.Data.DataRow = dt_rma_lines.Select("ord_nr = '" + row.Item("crec_id").ToString() + "'")
                Dim prod_amount As Integer = 0
                Dim total_amount As Integer = Convert.ToInt32(row.Item("ord_qty"))

                For Each rma_line_row As System.Data.DataRow In dr_rows
                    prod_amount += Convert.ToInt32(rma_line_row.Item("return_qty"))
                Next

                Dim amt_available As Integer = total_amount - prod_amount
                row.Item("amt_left") = amt_available

            Next

            If dt_bulk_products.Rows.Count > 0 Then
                Dim dv_bulk_products As System.Data.DataView = dt_bulk_products.DefaultView
                dv_bulk_products.RowFilter = "amt_left > 0"

                Me.rpt_bulk_return.DataSource = dv_bulk_products
                Me.rpt_bulk_return.DataBind()
                check_bulk_list()
            Else
                ll_resume = False
            End If

            If Not ll_resume Then
                Me.alert_no_found.Text = global_trans.translate_label("label_no_found", "Geen order regels gevonden", Me.global_cms.Language)
                Me.alert_no_found.Visible = True
            Else
                Me.alert_no_found.Visible = False
            End If

        End If
    End Sub

#Region "Button subs"
    Protected Sub save_request(sender As Object, e As System.EventArgs) Handles button_proceed.Click
        Dim ll_testing = True
        Me.message_error.Visible = False

        'test om te controleren of de reden en compensatie ingevult zijn
        If ll_testing Then
            'If Me.dd_return_comp.Text = "0000" Or Me.dd_return_reason.Text = "0000" Then
            If Me.dd_return_comp.Text = "0000" Then
                ll_testing = False
            End If

            ' On 20 May 2016 it is asked not to check on overall reason and reason_description by Marieke
            'If Me.txt_return_reason_desc.Text = "" Then
            '    ll_testing = False
            'End If

            If Not ll_testing Then
                Me.ph_no_items_selected.Visible = False
                Me.ph_reason_comp_error.Visible = True
                Me.ph_amount_error.Visible = False
                Me.ph_reason_error.Visible = False
            End If
        End If

        If ll_testing Then
            'test of er wel een van de checkboxes aangeklikt is
            If ll_testing Then
                Dim bool_test = False

                For Each item In Me.rpt_return_order_detail.Items
                    Dim cb_line As System.Web.UI.WebControls.CheckBox
                    cb_line = item.findcontrol("row_check")
                    If cb_line.Checked Then
                        bool_test = True
                    End If
                    If bool_test Then Exit For
                Next

                ll_testing = bool_test
            End If

            'maakt error panel voor geen checkboxes geselecteerd zichtbaar
            If Not ll_testing Then
                Me.ph_no_items_selected.Visible = True
                Me.ph_reason_comp_error.Visible = False
                Me.ph_amount_error.Visible = False
                Me.ph_reason_error.Visible = False
            End If
        End If

        If ll_testing Then
            'check voor het aantal om te te kijken of het wel is ingevult en of het niet boven het aantal besteld gaat.
            If ll_testing Then
                Dim bool_test = True
                For Each item In Me.rpt_return_order_detail.Items
                    Try
                        Dim cb_line As System.Web.UI.WebControls.CheckBox
                        cb_line = item.findcontrol("row_check")
                        If bool_test Then
                            If cb_line.Checked Then
                                Dim txt_amount As TextBox
                                txt_amount = item.findcontrol("row_input_amt")

                                Dim label_order_quantity As System.Web.UI.WebControls.Label
                                label_order_quantity = item.findcontrol("row_ord_qty")

                                If Convert.ToInt32(txt_amount.Text) <= 0 Then
                                    bool_test = False
                                End If

                                If Convert.ToInt32(txt_amount.Text) > Convert.ToInt32(label_order_quantity.Text) Then
                                    bool_test = False
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        bool_test = False
                    End Try
                Next

                ll_testing = bool_test
            End If

            If Not ll_testing Then
                Me.ph_no_items_selected.Visible = False
                Me.ph_reason_comp_error.Visible = False
                Me.ph_amount_error.Visible = True
                Me.ph_reason_error.Visible = False
            End If
        End If

        ' On 20 May 2016 it is asked not to check on overall reason and reason_description by Marieke
        'If ll_testing Then
        '    'Check if individual reason is filled
        '    If ll_testing Then
        '        ll_testing = Not Me.txt_return_reason_desc.Text.Length = 0
        '    End If

        '    If Not ll_testing Then
        '        Me.ph_no_items_selected.Visible = False
        '        Me.ph_reason_comp_error.Visible = False
        '        Me.ph_amount_error.Visible = False
        '        Me.ph_reason_error.Visible = True
        '    End If
        'End If

        'haal het comp_type op van het retour
        Dim qsc_comp As New Utilize.Data.QuerySelectClass
        qsc_comp.select_fields = "comp_type"
        qsc_comp.select_from = "uws_rma_compensation"
        qsc_comp.select_where = "comp_code = @comp_code"
        qsc_comp.add_parameter("@comp_code", Me.dd_return_comp.SelectedItem.Value)

        Dim dt_comp_check As System.Data.DataTable = qsc_comp.execute_query()
        'test of er wel alternatieve producten opgegeven zijn
        If ll_testing Then
            If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                If Not Me.rpt_alt_prod.Items.Count > 0 Then
                    ll_testing = False
                End If
            End If

            If Not ll_testing Then
                Me.ph_no_alt_products.Visible = True
            Else
                Me.ph_no_alt_products.Visible = False
            End If
        End If

        Dim total As Decimal = set_total()
        Dim max As Decimal = set_sub_totals()

        'check of het bedrag van de alternatieve producten niet hoger is dan het maximale bedrag
        If ll_testing Then
            If dt_comp_check.Rows(0).Item("comp_type") = "1" Then


                If total > max Then
                    ll_testing = False
                End If

                If Not ll_testing Then
                    Me.ph_alt_prod_to_high.Visible = True
                Else
                    Me.ph_alt_prod_to_high.Visible = False
                End If

            End If
        End If

        'verwerk de gegevens en maak een nieuwe retour aan
        If ll_testing Then
            'Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma")
            'll_testing = ubo_bus_obj.table_insert_row("uws_rma")
            Dim lt_rma_code As String = Me.global_rma.ubo_alt_product.field_get("rma_code")

            If ll_testing Then
                Me.global_rma.ubo_alt_product.field_set("uws_rma.rma_code", lt_rma_code)
                'Me.global_rma.ubo_alt_product.field_set("uws_rma.reason_desc_" + Me.global_ws.Language, Me.dd_return_reason.SelectedItem.Text)
                Me.global_rma.ubo_alt_product.field_set("uws_rma.req_date", String.Format("{0:dd/MM/yyyy}", DateTime.Now))
                Me.global_rma.ubo_alt_product.field_set("uws_rma.req_status", "Verzoek")
                Me.global_rma.ubo_alt_product.field_set("uws_rma.cust_id", Me.global_ws.user_information.user_customer_id)
                'Me.global_rma.ubo_alt_product.field_set("uws_rma.reason_code", "") ' Me.dd_return_reason.Text)
                Me.global_rma.ubo_alt_product.field_set("uws_rma.comp_code", Me.dd_return_comp.Text)
                Me.global_rma.ubo_alt_product.field_set("uws_rma.reason_comm", Me.txt_return_reason_desc.Text)
                Me.global_rma.ubo_alt_product.field_set("uws_rma.ord_nr", Me.hid_ord_nr.Value)
                Me.global_rma.ubo_alt_product.field_set("uws_rma.lng_code", Me.global_ws.Language)

                If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                    Me.global_rma.ubo_alt_product.field_set("uws_rma.return_amount", max - total)
                ElseIf dt_comp_check.Rows(0).Item("comp_type") = "4" Then
                    Me.global_rma.ubo_alt_product.field_set("uws_rma.return_amount", max)
                End If

                Dim qsc_history_order_lines As New Utilize.Data.QuerySelectClass
                qsc_history_order_lines.select_fields = "*"
                qsc_history_order_lines.select_from = "uws_history_lines"
                qsc_history_order_lines.select_where = "hdr_id = @ord_nr"
                qsc_history_order_lines.add_parameter("@ord_nr", Me.hid_ord_nr.Value)
                Dim dt_history_lines As System.Data.DataTable = qsc_history_order_lines.execute_query()

                'loopt door alle detail lijnen heen en slaat de gene op met een gecheckte checkbox
                For Each item In Me.rpt_return_order_detail.Items
                    Dim cb_line As System.Web.UI.WebControls.CheckBox
                    cb_line = item.findcontrol("row_check")

                    If cb_line.Checked Then
                        'cast alle webcontrols uit het item naar het juiste type
                        Dim label_product_code As System.Web.UI.WebControls.Label
                        label_product_code = item.findcontrol("row_prod_code")

                        Dim label_product_description As System.Web.UI.WebControls.Label
                        label_product_description = item.findcontrol("row_prod_desc")

                        Dim label_order_quantity As System.Web.UI.WebControls.Label
                        label_order_quantity = item.findcontrol("row_ord_qty")

                        Dim label_row_amount As System.Web.UI.WebControls.Label
                        label_row_amount = item.findcontrol("row_row_amt")

                        Dim txt_amount As TextBox
                        txt_amount = item.findcontrol("row_input_amt")

                        Dim label_vat_amt As System.Web.UI.WebControls.Label
                        label_vat_amt = item.findcontrol("hid_btw_amt")

                        Dim label_crec_id As System.Web.UI.WebControls.Label
                        label_crec_id = item.findcontrol("hid_crec_id")

                        Dim dd_return_reason_single As DropDownList
                        dd_return_reason_single = item.findcontrol("dd_return_reason_single")

                        Dim lt_reason_code As String = dd_return_reason_single.SelectedValue
                        'If lt_reason_code = "DEFAULT" Then
                        '    lt_reason_code = Me.dd_return_reason.SelectedValue
                        'End If

                        Dim history_line() As System.Data.DataRow = dt_history_lines.Select("crec_id = '" + label_crec_id.Text + "'")

                        Dim lt_total_return_amt As Decimal = calc_row_amt(history_line(0).Item("row_amt"), history_line(0).Item("ord_qty"), Convert.ToInt32(txt_amount.Text))
                        Dim lt_row_btw As String = calc_row_btw(history_line(0).Item("vat_amt"), history_line(0).Item("ord_qty"), Convert.ToInt32(txt_amount.Text)).ToString()

                        Dim qsc_vat_code As New Utilize.Data.QuerySelectClass
                        qsc_vat_code.select_fields = "vat_code"
                        qsc_vat_code.select_from = "uws_products"
                        qsc_vat_code.select_where = "prod_code = @prod_code"
                        qsc_vat_code.add_parameter("@prod_code", label_product_code.Text)
                        Dim dt_vat_code As System.Data.DataTable = qsc_vat_code.execute_query()

                        'maakt een nieuwe rma line row aan en vul deze
                        ll_testing = Me.global_rma.ubo_alt_product.table_insert_row("uws_rma_line", Me.global_ws.user_information.user_id)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.hdr_id", lt_rma_code)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.prod_code", label_product_code.Text)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.prod_desc", label_product_description.Text)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.return_qty", txt_amount.Text)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.org_return_qty", txt_amount.Text)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.row_amt", lt_total_return_amt)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.org_row_amt", lt_total_return_amt)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.vat_code", dt_vat_code.Rows(0).Item("vat_code"))
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.vat_amt", lt_row_btw)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.org_vat_amt", lt_row_btw)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.reason_code", lt_reason_code)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line.ord_nr", Me.hid_ord_nr.Value)


                        If ll_testing Then
                            If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                                Dim ll_move_next As Boolean = Me.global_rma.ubo_alt_product.move_first("uws_rma_alt_prod")

                                Do While ll_move_next

                                    Me.global_rma.ubo_alt_product.field_set("uws_rma_alt_prod.rma_code", lt_rma_code)

                                    ll_move_next = Me.global_rma.ubo_alt_product.move_next("uws_rma_alt_prod")
                                Loop
                            End If

                            ll_testing = Me.global_rma.ubo_alt_product.table_update(Me.global_ws.user_information.user_id)


                            If ll_testing Then
                                handle_uploaded_images(lt_rma_code, Me.global_rma.ubo_alt_product.field_get("uws_rma_line.rec_id"), label_product_code.Text)
                            End If

                            If Not ll_testing Then
                                Me.message_error.Text = Me.global_rma.ubo_alt_product.error_message
                                Me.message_error.Visible = True

                                Exit For
                            End If
                        End If
                    End If
                Next

                If ll_testing Then
                    Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
                    ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)
                    ubo_change.field_set("uws_rma_changes.change_user", "klant" + Me.global_ws.user_information.ubo_customer.field_get("bus_name"))
                    ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
                    ubo_change.field_set("uws_rma_changes.rma_code", lt_rma_code)
                    ubo_change.field_set("uws_rma_changes.change_status", "verzoek")
                    ubo_change.field_set("uws_rma_changes.lng_code", Me.global_ws.Language)

                    ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        Me.message_error.Text = Me.global_rma.ubo_alt_product.error_message
                        Me.message_error.Visible = True
                    End If
                End If

                If ll_testing Then
                    Me.ph_alt_pro_totals.Visible = False
                    Me.ph_alt_product_selectie.Visible = False
                    Me.ph_tabs.Visible = False
                    Me.ph_confirmation.Visible = True
                    Me.ph_returns_detail.Visible = False
                    Me.button_proceed.Visible = False

                    Me.global_rma.ubo_alt_product = Utilize.Data.DataProcedures.CreateRecordObject("uws_rma", "", "rma_code")
                    Me.global_rma.ubo_alt_product.table_insert_row("uws_rma", Me.global_ws.user_information.user_id)
                End If

            End If
        End If

    End Sub

    Protected Sub handle_uploaded_images(rma_code As String, rma_line_id As String, prod_code_actual As String, Optional ord_nr As String = "")
        Dim rmaLocation As String = Server.MapPath("~/Documents/RMA/")

        If Not System.IO.Directory.Exists(rmaLocation) Then
            System.IO.Directory.CreateDirectory(rmaLocation)
        End If

        Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")
        Dim directory As New DirectoryInfo(tempPath)
        Dim files As FileInfo() = directory.GetFiles()
        Dim singleFile As FileInfo

        If files.Length > 0 Then
            Dim ubo_images As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_rma_line_images", "", "rec_id")

            Dim newLocation As String = Server.MapPath("~/Documents/RMA/images/")

            If Not System.IO.Directory.Exists(newLocation) Then
                System.IO.Directory.CreateDirectory(newLocation)
            End If

            For Each singleFile In files
                Dim fileWithoutProdcode = singleFile.Name.Substring(0, singleFile.Name.LastIndexOf("]"c))

                Dim start As Integer = singleFile.Name.LastIndexOf("]"c)

                If singleFile.Name.Contains("{") Then
                    Dim prod_code = singleFile.Name.Substring(start + 1, singleFile.Name.LastIndexOf("{"c) - start - 1)
                    Dim extention As String = singleFile.Name.Substring(singleFile.Name.LastIndexOf("."c))

                    Dim filename = fileWithoutProdcode + extention

                    If prod_code = prod_code_actual And fileWithoutProdcode.Contains(rma_code) And singleFile.Name.Contains(ord_nr) Then
                        Me.global_rma.ubo_bulk_return.table_insert_row("uws_rma_line_images", Me.global_ws.user_information.user_id)

                        singleFile.MoveTo(newLocation + filename)

                        Me.global_rma.ubo_bulk_return.field_set("uws_rma_line_images.hdr_id", rma_line_id)
                        Me.global_rma.ubo_bulk_return.field_set("uws_rma_line_images.image", Me.ResolveUrl("~/Documents/RMA/images/" + filename))
                        Me.global_rma.ubo_bulk_return.table_update(Me.global_ws.user_information.user_id)
                    End If
                Else
                    Dim prod_code = singleFile.Name.Substring(start + 1, singleFile.Name.LastIndexOf("."c) - start - 1)
                    Dim extention As String = singleFile.Name.Substring(singleFile.Name.LastIndexOf("."c))

                    Dim filename = fileWithoutProdcode + extention

                    If prod_code = prod_code_actual And fileWithoutProdcode.Contains(rma_code) Then
                        Me.global_rma.ubo_alt_product.table_insert_row("uws_rma_line_images", Me.global_ws.user_information.user_id)

                        singleFile.MoveTo(newLocation + filename)

                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line_images.hdr_id", rma_line_id)
                        Me.global_rma.ubo_alt_product.field_set("uws_rma_line_images.image", Me.ResolveUrl("~/Documents/RMA/images/" + filename))
                        Me.global_rma.ubo_alt_product.table_update(Me.global_ws.user_information.user_id)
                    End If
                End If


            Next
        End If

    End Sub

    Protected Sub dispose_images(sender As Object, e As System.EventArgs) Handles button_remove_images.Click
        Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")
        Dim directory As New DirectoryInfo(tempPath)
        Dim files As FileInfo() = directory.GetFiles()
        Dim singleFile As FileInfo

        If files.Length > 0 Then
            For Each singleFile In files
                If singleFile.Name.Contains(Me.global_rma.ubo_alt_product.field_get("rma_code")) Then
                    singleFile.Delete()
                End If
            Next
        End If

    End Sub

    Protected Sub button_close_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        ph_returns.Visible = True
        ph_returns_detail.Visible = False
        button_close_details.Visible = False
        button_proceed.Visible = False
        Me.ph_alt_product_selectie.Visible = False

        Dim ubo_active As Utilize.Data.BusinessObject = Nothing

        If Not Me.ph_bulk_details.Visible Then
            ubo_active = Me.global_rma.ubo_alt_product
        Else
            ubo_active = Me.global_rma.ubo_bulk_return
        End If

        If Me.ph_order_search.Visible Then
            If Not Me.txt_order_code.Text = "" Then
                rpt_return_orders.DataSource = Me.get_order(True)
                rpt_return_orders.DataBind()
            Else
                Me.ph_order_search.Visible = False
                Me.button_search.Visible = True
                Me.button_close_search.Visible = False
            End If
        End If

        Dim ll_move_next As Boolean = ubo_active.move_first("uws_rma_alt_prod")

        Do While ll_move_next
            ubo_active.table_delete_row("uws_rma_alt_prod", True, Me.global_ws.user_information.user_customer_id)

            ll_move_next = ubo_active.move_next("uws_rma_alt_prod")
        Loop
        Me.rpt_alt_prod.DataSource = ubo_active.data_tables("uws_rma_alt_prod").data_table.DefaultView
        rpt_alt_prod.DataBind()

        set_sub_totals()

        If Me.ph_confirmation.Visible Then
            Me.ph_tabs.Visible = True
            Me.ph_confirmation.Visible = False
        End If
    End Sub

    Protected Sub button_search_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search.Click
        Me.ph_order_search.Visible = True
        Me.button_search.Visible = False
        Me.button_close_search.Visible = True
    End Sub

    Protected Sub button_filter_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter.Click
        If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Or Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Or Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
            Me.button_filter_remove.Visible = True

            Dim dt_orders As New System.Data.DataTable
            dt_orders = Me.get_order(True)

            'Controle of er orders zijn met de zoek waarde
            If dt_orders.Rows.Count > 0 Then
                Me.ph_no_found.Visible = False
                rpt_return_orders.DataSource = Me.get_order(True)
                rpt_return_orders.DataBind()
            Else
                'geef een placeholder weer met tekst dat er niets gevonden is
                Me.rpt_return_orders.Visible = False
                Me.ph_no_found.Visible = True
                Me.button_filter_remove.Visible = False
            End If
        Else
            Me.ph_no_filter.Visible = True
        End If
    End Sub

    Protected Sub button_close_search_click(sender As Object, e As System.EventArgs) Handles button_close_search.Click
        'verberg de placeholder voor het zoekveld en reset de pagina zodat alle orders van de klant weer zichtbaar zijn
        Me.ph_order_search.Visible = False
        Me.button_search.Visible = True
        Me.button_close_search.Visible = False
        Me.txt_order_code.Text = ""
        Me.get_order()
    End Sub

    Protected Sub button_filter_remove_click(sender As Object, e As System.EventArgs) Handles button_filter_remove.Click
        'reset de order lijst en maakt het zoekveld leeg
        Me.get_order()
        Me.button_filter_remove.Visible = False
        Me.txt_order_code.Text = ""
    End Sub
#End Region

#Region "Alternatief product subs"
    Protected Sub button_add_Click(sender As Object, e As EventArgs) Handles button_add.Click
        Dim ubo_active As Utilize.Data.BusinessObject = Nothing
        Dim ll_testing As Boolean = True
        If ll_testing Then
            'check of er wel een productcode ingevult is
            ll_testing = Not CType(Me.ph_product_code_alt.Controls(0), product_code).input_control.Text = ""
        End If

        If ll_testing Then
            'check of er wel een hoeveelheid is ingvuld
            ll_testing = Not text_prod_qty.Text = ""
        End If

        If ll_testing Then
            'check of de hoeveelheid wel boven de 0 is
            ll_testing = Integer.Parse(text_prod_qty.Text) > 0
        End If

        If ll_testing Then
            'haal de product informatie
            Dim qsc_product As New Utilize.Data.QuerySelectClass
            qsc_product.select_fields = "*"
            qsc_product.select_from = "uws_products"
            qsc_product.select_where = "prod_code = @prod_code"
            qsc_product.add_parameter("@prod_code", Me.txt_product_code.Text)
            Dim dt_product As System.Data.DataTable = qsc_product.execute_query()
            ll_testing = dt_product.Rows.Count > 0

            Dim dt_vat As System.Data.DataTable = Nothing

            If ll_testing Then
                'haal de btw gegevens op van het lement
                Dim qsc_vat As New Utilize.Data.QuerySelectClass
                qsc_vat.select_fields = "*"
                qsc_vat.select_from = "uws_vat"
                qsc_product.select_where = "vat_code = @vat_code"
                qsc_product.add_parameter("@vat_code", dt_product.Rows(0).Item("vat_code"))
                dt_vat = qsc_vat.execute_query()
                ll_testing = dt_vat.Rows.Count > 0
            End If

            If ll_testing Then
                Dim found = False

                If Not Me.ph_bulk_details.Visible Then
                    ubo_active = Me.global_rma.ubo_alt_product
                Else
                    ubo_active = Me.global_rma.ubo_bulk_return
                End If

                Dim ll_move_next As Boolean = Me.global_rma.ubo_alt_product.move_first("uws_rma_alt_prod")

                'loop door alle producten heen die er op het moment al aanwezig zijn, en als deze aanwezig is dan worden de prijs en hoeveelheden bij elkaar opgeteld
                Do While ll_move_next
                    If ubo_active.field_get("uws_rma_alt_prod.prod_code") = Me.txt_product_code.Text Then
                        ubo_active.field_set("uws_rma_alt_prod.prod_qty", ubo_active.field_get("uws_rma_alt_prod.prod_qty") + Integer.Parse(Me.text_prod_qty.Text))
                        ubo_active.field_set("uws_rma_alt_prod.row_amt", ubo_active.field_get("uws_rma_alt_prod.row_amt") + (Integer.Parse(Me.text_prod_qty.Text) * dt_product.Rows(0).Item("prod_price")))
                        ubo_active.field_set("uws_rma_alt_prod.row_vat", ubo_active.field_get("uws_rma_alt_prod.row_vat") + (Integer.Parse(Me.text_prod_qty.Text) * dt_product.Rows(0).Item("prod_price")) * (dt_vat.Rows(0).Item("vat_pct") / 100))
                        found = True
                    End If
                    ll_move_next = ubo_active.move_next("uws_rma_alt_prod")
                Loop

                'als het product nog niet bestaat dan wordt het hier aangemaakt
                If ll_testing And Not found Then
                    ubo_active.table_insert_row("uws_rma_alt_prod", Me.global_ws.user_information.user_id)

                    ubo_active.field_set("uws_rma_alt_prod.prod_code", Me.txt_product_code.Text)
                    ubo_active.field_set("uws_rma_alt_prod.prod_desc", dt_product.Rows(0).Item("prod_desc"))
                    ubo_active.field_set("uws_rma_alt_prod.prod_price", dt_product.Rows(0).Item("prod_price"))
                    ubo_active.field_set("uws_rma_alt_prod.prod_qty", Integer.Parse(Me.text_prod_qty.Text))
                    ubo_active.field_set("uws_rma_alt_prod.vat_code", dt_product.Rows(0).Item("vat_code"))
                    ubo_active.field_set("uws_rma_alt_prod.row_vat", (Integer.Parse(Me.text_prod_qty.Text) * dt_product.Rows(0).Item("prod_price")) * (dt_vat.Rows(0).Item("vat_pct") / 100))
                    ubo_active.field_set("uws_rma_alt_prod.row_amt", Integer.Parse(Me.text_prod_qty.Text) * dt_product.Rows(0).Item("prod_price"))
                End If
            End If

            ubo_active.table_update(Me.global_ws.user_information.user_id)

            If ll_testing Then
                If Not ph_alt_pro_totals.Visible Then
                    ph_alt_pro_totals.Visible = True
                End If

                If Me.ph_bulk_details.Visible Then
                    set_sub_totals_bulk()
                Else
                    set_sub_totals()
                End If
                Me.txt_product_code.Text = ""
                Me.text_prod_qty.Text = ""
            End If
        End If


        If ll_testing Then
            rpt_alt_prod.DataSource = ubo_active.data_tables("uws_rma_alt_prod").data_table
            rpt_alt_prod.DataBind()
        End If
    End Sub

    Protected Function set_total() As Decimal
        'set het bedrag van alle alternatieve producten samen

        Dim ubo_active As Utilize.Data.BusinessObject = Nothing

        If Not Me.ph_bulk_details.Visible Then
            ubo_active = Me.global_rma.ubo_alt_product
        Else
            ubo_active = Me.global_rma.ubo_bulk_return
        End If

        Dim ll_move_next As Boolean = ubo_active.move_first("uws_rma_alt_prod")

        Me.lbl_order_total_excl_vat.Text = Decimal.Parse("0.00").ToString("F")
        If Not ll_move_next Then
            Me.lbl_order_total_excl_vat.Text = Decimal.Parse("0.00").ToString("F")
        End If

        Do While ll_move_next
            Me.lbl_order_total_excl_vat.Text = Decimal.Parse(Me.lbl_order_total_excl_vat.Text) + (Double.Parse(ubo_active.field_get("uws_rma_alt_prod.row_amt")))
            Me.lbl_order_total_excl_vat.Text = Decimal.Parse(Me.lbl_order_total_excl_vat.Text).ToString("F")

            ll_move_next = ubo_active.move_next("uws_rma_alt_prod")
        Loop

        Dim result As Decimal = Decimal.Parse(Me.lbl_order_total_excl_vat.Text).ToString("F")

        Me.lbl_order_total_excl_vat.Text = Format(result, "c")

        Return result
    End Function

    Protected Function set_sub_totals_bulk() As Decimal
        Me.ph_alt_pro_totals.Visible = True

        Dim total As Decimal = set_total()
        Dim max_amount As Decimal = 0.0

        For Each item As System.Web.UI.WebControls.RepeaterItem In Me.rpt_bulk_detail.Items
            Dim row_amt_label As Label = item.FindControl("row_row_amt")
            Dim row_amt As Decimal = Decimal.Parse(row_amt_label.Text.Substring(1))
            max_amount += row_amt
        Next

        Me.lbl_max_amount.Text = Format(max_amount, "c")

        'set de styling van het totaal bedrag van de alternatieve producten aan de hand van of het hoger of lager is dan het maximaal uit te geven bedrag
        If total > max_amount Then
            Me.lbl_order_total_excl_vat.CssClass = "value-to-high"
            Me.lt_icon.Text = "<i class='fa fa-times-circle value-to-high'></i>"
        Else
            Me.lbl_order_total_excl_vat.CssClass = "value-normal"
            Me.lt_icon.Text = "<i class='fa fa-check-circle value-normal'></i>"
        End If

        'set de hoeveelheid die nog over is en te gebruiken voor de klant, of dient als de hoeveelheid die de klant uiteindelijk nog terug zal krijgen via coupon of als betaling
        Me.lbl_amount_left.Text = Format(max_amount - total, "c")

        Return max_amount

    End Function

    Protected Function set_sub_totals() As Decimal

        Me.ph_alt_pro_totals.Visible = True

        Dim total As Decimal = set_total()
        Dim max_amount As Decimal = 0.0

        Dim qsc_history_order_lines As New Utilize.Data.QuerySelectClass
        qsc_history_order_lines.select_fields = "*"
        qsc_history_order_lines.select_from = "uws_history_lines"
        qsc_history_order_lines.select_where = "hdr_id = @ord_nr"
        qsc_history_order_lines.add_parameter("@ord_nr", Me.hid_ord_nr.Value)
        Dim dt_history_lines As System.Data.DataTable = qsc_history_order_lines.execute_query()

        'bereken wat het maximale bedrag is wat gebruikt kan worden voor de alternatieve producten
        For Each item In Me.rpt_return_order_detail.Items
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")
            If cb_line.Checked Then
                'cast alle webcontrols uit het item naar het juiste type
                Dim label_order_quantity As System.Web.UI.WebControls.Label
                label_order_quantity = item.findcontrol("row_ord_qty")
                Dim label_row_amount As System.Web.UI.WebControls.Label
                label_row_amount = item.findcontrol("row_row_amt")
                Dim txt_amount As TextBox
                txt_amount = item.findcontrol("row_input_amt")
                Dim label_vat_amt As System.Web.UI.WebControls.Label
                label_vat_amt = item.findcontrol("hid_btw_amt")
                Dim label_crec_id As System.Web.UI.WebControls.Label
                label_crec_id = item.findcontrol("hid_crec_id")

                If Not txt_amount.Text = "" Then
                    Dim history_line() As System.Data.DataRow = dt_history_lines.Select("crec_id = '" + label_crec_id.Text + "'")
                    Dim lt_total_return_amt As Decimal = (history_line(0).Item("row_amt") / history_line(0).Item("ord_qty")) * Integer.Parse(txt_amount.Text)
                    'Dim lt_row_btw As String = calc_row_btw(history_line(0).Item("vat_amt"), history_line(0).Item("ord_qty"), Convert.ToInt32(txt_amount.Text)).ToString()

                    max_amount += lt_total_return_amt '+ lt_row_btw
                End If
            End If
        Next

        Me.lbl_max_amount.Text = Format(max_amount, "c")

        'set de styling van het totaal bedrag van de alternatieve producten aan de hand van of het hoger of lager is dan het maximaal uit te geven bedrag
        If total > max_amount Then
            Me.lbl_order_total_excl_vat.CssClass = "value-to-high"
            Me.lt_icon.Text = "<i class='fa fa-times-circle value-to-high'></i>"
        Else
            Me.lbl_order_total_excl_vat.CssClass = "value-normal"
            Me.lt_icon.Text = "<i class='fa fa-check-circle value-normal'></i>"
        End If

        'set de hoeveelheid die nog over is en te gebruiken voor de klant, of dient als de hoeveelheid die de klant uiteindelijk nog terug zal krijgen via coupon of als betaling
        Me.lbl_amount_left.Text = Format(max_amount - total, "c")

        Return max_amount

    End Function

    Protected Sub rpt_alt_prod_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_alt_prod.ItemCommand
        If e.CommandName = "remove" Then
            Dim lc_prod_nr = e.CommandArgument
            'verwijder de line uit het business object

            Dim ubo_active As Utilize.Data.BusinessObject = Nothing

            If Not Me.ph_bulk_details.Visible Then
                ubo_active = Me.global_rma.ubo_alt_product
            Else
                ubo_active = Me.global_rma.ubo_bulk_return
            End If

            Dim ll_move_next As Boolean = ubo_active.move_first("uws_rma_alt_prod")

            Do While ll_move_next
                If ubo_active.field_get("uws_rma_alt_prod.prod_code") = lc_prod_nr Then
                    ubo_active.table_delete_row("uws_rma_alt_prod", False, Me.global_ws.user_information.user_customer_id)
                    ubo_active.move_first("uws_rma_alt_prod")
                End If

                ll_move_next = ubo_active.move_next("uws_rma_alt_prod")
            Loop
            Me.rpt_alt_prod.DataSource = ubo_active.data_tables("uws_rma_alt_prod").data_table.DefaultView
            rpt_alt_prod.DataBind()


            If Me.ph_bulk_details.Visible Then
                set_sub_totals_bulk()
            Else
                set_sub_totals()
            End If
        End If
    End Sub

    Protected Sub checkAmount(sender As Object, e As System.EventArgs)
        'loop door alle elementen heen
        Dim found As Boolean = False
        For Each item In Me.rpt_return_order_detail.Items
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")

            If cb_line.Checked Then

                Dim txt_amount As TextBox
                txt_amount = item.findcontrol("row_input_amt")
                'check of de id van de text boxes gelijk zijn aan elkaar, als dit waar is zit je in de goede item
                If txt_amount.UniqueID = CType(sender, TextBox).UniqueID And Not found Then
                    found = True
                    Dim label_order_quantity As System.Web.UI.WebControls.Label
                    label_order_quantity = item.findcontrol("row_ord_qty")

                    Dim placeholder_amount_left As New Utilize.Web.UI.WebControls.placeholder
                    placeholder_amount_left = item.findcontrol("ph_amount_left_row")

                    'als het getal in de textbox hoger is dan het getal wat orgineel besteld was dan wordt het getal geset naar het orgineel bestelde aantal.
                    Dim textbox_number As Int32 = Nothing
                    If Not CType(sender, TextBox).Text = "" Then
                        If Int32.TryParse(CType(sender, TextBox).Text, textbox_number) Then
                            If placeholder_amount_left.Visible Then
                                Dim label_amount_left As New Utilize.Web.UI.WebControls.label
                                label_amount_left = item.findcontrol("row_amt_left")

                                If textbox_number > Integer.Parse(label_amount_left.Text) Or textbox_number < 0 Then
                                    CType(sender, TextBox).Text = label_amount_left.Text
                                End If
                            Else
                                If textbox_number > Integer.Parse(label_order_quantity.Text) Then
                                    CType(sender, TextBox).Text = label_order_quantity.Text
                                End If
                            End If
                        Else
                            CType(sender, TextBox).Text = label_order_quantity.Text
                        End If
                    End If
                    Dim qsc_comp As New Utilize.Data.QuerySelectClass
                    qsc_comp.select_fields = "comp_type"
                    qsc_comp.select_from = "uws_rma_compensation"
                    qsc_comp.select_where = "comp_code = @comp_code"
                    qsc_comp.add_parameter("@comp_code", Me.dd_return_comp.SelectedItem.Value)

                    Dim dt_comp_check As System.Data.DataTable = qsc_comp.execute_query()
                    If dt_comp_check.Rows.Count > 0 Then
                        If dt_comp_check.Rows(0).Item("comp_type").ToString() = "1" Then
                            set_sub_totals()
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub Webshop_Account_AccountBlocks_uc_return_request_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            If Me.IsPostBack Then
                'dit is code die door javascript uitgevoerd wordt als iemand op een row klikt in het overzicht
                Dim eventTarget As String
                Dim eventArgument As String

                If ((Me.Request("__EVENTTARGET") Is Nothing)) Then
                    eventTarget = String.Empty
                Else
                    eventTarget = Me.Request("__EVENTTARGET")
                End If
                If ((Me.Request("__EVENTARGUMENT") Is Nothing)) Then
                    eventArgument = String.Empty
                Else
                    eventArgument = Me.Request("__EVENTARGUMENT")
                End If

                If eventTarget = "remove_image" Then
                    Dim file_info = eventArgument
                    Dim rma_code As String = file_info.Substring(file_info.LastIndexOf("&") + 1)
                    Dim file_name As String = file_info.Substring(0, file_info.LastIndexOf("&"))

                    Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")
                    Dim directory As New DirectoryInfo(tempPath)
                    Dim files As FileInfo() = directory.GetFiles()
                    Dim singleFile As FileInfo

                    If files.Length > 0 Then
                        For Each singleFile In files
                            If singleFile.Name.Contains(file_name) And singleFile.Name.Contains(rma_code) Then
                                singleFile.Delete()
                            End If
                        Next
                    End If

                End If
            End If

            If Me.IsPostBack And Me.ph_alt_product_selectie.Visible Then
                Dim ubo_active As Utilize.Data.BusinessObject = Nothing

                If Not Me.ph_bulk_details.Visible Then
                    ubo_active = Me.global_rma.ubo_alt_product
                Else
                    ubo_active = Me.global_rma.ubo_bulk_return
                End If

                Me.rpt_alt_prod.DataSource = ubo_active.data_tables("uws_rma_alt_prod").data_table
                Me.rpt_alt_prod.DataBind()
            End If

            If Me.IsPostBack And Me.ph_alt_product_selectie.Visible And Me.ph_bulk_details.Visible Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "move_alt_product", "move_product_selector();", True)
            End If
        End If
        set_images()
        set_bulk_images()

    End Sub

    Public Sub set_images()
        If Me.IsPostBack And Me.ph_returns_detail.Visible Then
            For Each item In Me.rpt_return_order_detail.Items
                'cast de controls van elke item naar het item wat ze zijn zodat ze gebruikt kunnen worden
                Dim cb_line As System.Web.UI.WebControls.CheckBox
                cb_line = item.findcontrol("row_check")

                If cb_line.Checked Then
                    Dim cb_photo As System.Web.UI.WebControls.CheckBox
                    cb_photo = item.findcontrol("cb_add_photo")

                    If cb_photo.Checked Then
                        Dim prod_code As String = item.findcontrol("row_prod_code").text

                        Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")

                        If Not System.IO.Directory.Exists(tempPath) Then
                            System.IO.Directory.CreateDirectory(tempPath)
                        End If

                        Dim directory As New DirectoryInfo(tempPath)
                        Dim files As FileInfo() = directory.GetFiles()
                        Dim singleFile As FileInfo

                        Dim ph_uploaded_images As Utilize.Web.UI.WebControls.placeholder
                        ph_uploaded_images = item.findcontrol("ph_uploaded_images")
                        ph_uploaded_images.Controls.Clear()

                        Dim row As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                        row.Attributes.Add("class", "row")
                        Dim col_counter As Integer = 0

                        If files.Length > 0 Then
                            For Each singleFile In files
                                If singleFile.Name.Contains(Me.global_rma.ubo_alt_product.field_get("rma_code")) Then

                                    Dim start As Integer = singleFile.Name.LastIndexOf("]"c)

                                    Dim prod_code_from_file = singleFile.Name.Substring(start + 1, singleFile.Name.LastIndexOf("."c) - start - 1)

                                    If prod_code_from_file = prod_code Then
                                        Dim fileExtention = singleFile.Name.Substring(singleFile.Name.LastIndexOf("."c))
                                        Dim fileWithoutProdcode = singleFile.Name.Substring(0, singleFile.Name.LastIndexOf("]"c))

                                        Dim filename As String = singleFile.Name

                                        Dim col As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                                        col.Attributes.Add("class", "col-md-4")

                                        Dim image As New Utilize.Web.UI.WebControls.image
                                        image.Attributes.Add("class", "img-responsive")
                                        image.ImageUrl = Me.ResolveUrl("~/documents/rma/tempImages/") + filename

                                        Dim remove_link As New button_remove_file
                                        remove_link.CommandArgument = Server.MapPath("~/documents/rma/tempImages/")
                                        remove_link.CommandName = filename
                                        remove_link.UserControl = Me
                                        remove_link.ph_images = ph_uploaded_images
                                        remove_link.CssClass = "dropzone-remove-link"
                                        remove_link.Text = global_trans.translate_button("link_remove_file", "Verwijder bestand", Me.global_cms.Language)

                                        col.Controls.Add(image)
                                        col.Controls.Add(remove_link)
                                        row.Controls.Add(col)

                                        col_counter += 1
                                        If col_counter = 3 Then
                                            Dim clearfix As New HtmlGenericControl("div")
                                            clearfix.Attributes.Add("class", "clearfix")
                                            row.Controls.Add(clearfix)
                                            col_counter = 0
                                        End If

                                        If Not ph_uploaded_images.Visible Then
                                            ph_uploaded_images.Visible = True
                                        End If

                                    End If
                                End If
                            Next
                        End If
                        ph_uploaded_images.Controls.Add(row)
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub set_bulk_images()
        If Me.IsPostBack And Me.ph_bulk_details.Visible Then
            For Each item In Me.rpt_bulk_detail.Items
                'cast de controls van elke item naar het item wat ze zijn zodat ze gebruikt kunnen worden
                Dim prod_code As String = item.findcontrol("row_prod_code").text
                Dim ord_nr As String = item.findcontrol("row_ord_nr").text

                Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")

                If Not System.IO.Directory.Exists(tempPath) Then
                    System.IO.Directory.CreateDirectory(tempPath)
                End If

                Dim directory As New DirectoryInfo(tempPath)
                Dim files As FileInfo() = directory.GetFiles()
                Dim singleFile As FileInfo

                Dim ph_uploaded_images As Utilize.Web.UI.WebControls.placeholder
                ph_uploaded_images = item.findcontrol("ph_images")
                ph_uploaded_images.Controls.Clear()

                Dim ph_images As Utilize.Web.UI.WebControls.placeholder
                ph_images = item.findcontrol("ph_bulk_images")

                Dim row As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                row.Attributes.Add("class", "row")
                Dim col_counter As Integer = 0

                If files.Length > 0 Then
                    For Each singleFile In files
                        If singleFile.Name.Contains(Me.global_rma.ubo_bulk_return.field_get("rma_code")) Then

                            Dim start As Integer = singleFile.Name.LastIndexOf("]"c)
                            Dim start_ord_nr As Integer = singleFile.Name.LastIndexOf("{"c)

                            Dim prod_code_from_file = singleFile.Name.Substring(start + 1, singleFile.Name.LastIndexOf("{"c) - start - 1)
                            Dim ord_nr_from_file = singleFile.Name.Substring(start_ord_nr + 1, singleFile.Name.LastIndexOf("."c) - start_ord_nr - 1)

                            If prod_code_from_file = prod_code And ord_nr = ord_nr_from_file Then
                                Dim filename As String = singleFile.Name

                                Dim col As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                                col.Attributes.Add("class", "col-md-4")

                                Dim image As New Utilize.Web.UI.WebControls.image
                                image.Attributes.Add("class", "img-responsive")
                                image.ImageUrl = Me.ResolveUrl("~/documents/rma/tempImages/") + filename

                                Dim remove_link As New button_remove_file
                                remove_link.CommandArgument = Server.MapPath("~/documents/rma/tempImages/")
                                remove_link.CommandName = filename
                                remove_link.UserControl = Me
                                remove_link.ph_images = ph_uploaded_images
                                remove_link.CssClass = "dropzone-remove-link"
                                remove_link.Text = global_trans.translate_button("link_remove_file", "Verwijder bestand", Me.global_cms.Language)

                                col.Controls.Add(image)
                                col.Controls.Add(remove_link)
                                row.Controls.Add(col)

                                col_counter += 1
                                If col_counter = 3 Then
                                    Dim clearfix As New HtmlGenericControl("div")
                                    clearfix.Attributes.Add("class", "clearfix")
                                    row.Controls.Add(clearfix)
                                    col_counter = 0
                                End If

                                If Not ph_uploaded_images.Visible Then
                                    ph_uploaded_images.Visible = True
                                End If

                                If Not ph_images.Visible Then
                                    ph_images.Visible = True
                                End If

                            End If
                        End If
                    Next
                End If
                ph_uploaded_images.Controls.Add(row)
            Next
        End If
        For Each item In Me.rpt_bulk_return.Items
            'cast de controls van elke item naar het item wat ze zijn zodat ze gebruikt kunnen worden
            Dim cb_line As System.Web.UI.WebControls.CheckBox
            cb_line = item.findcontrol("row_check")

            If cb_line.Checked Then
                Dim cb_photo As System.Web.UI.WebControls.CheckBox
                cb_photo = item.findcontrol("cb_add_photo")

                If cb_photo.Checked Then
                    Dim ord_nr As String = item.findcontrol("hid_ord_id").text
                    Dim prod_code As String = item.findcontrol("row_prod_code").text

                    Dim tempPath As String = Server.MapPath("~/Documents/RMA/tempImages/")

                    If Not System.IO.Directory.Exists(tempPath) Then
                        System.IO.Directory.CreateDirectory(tempPath)
                    End If

                    Dim directory As New DirectoryInfo(tempPath)
                    Dim files As FileInfo() = directory.GetFiles()
                    Dim singleFile As FileInfo

                    Dim ph_uploaded_images As Utilize.Web.UI.WebControls.placeholder
                    ph_uploaded_images = item.findcontrol("ph_uploaded_images")
                    ph_uploaded_images.Controls.Clear()

                    Dim row As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                    row.Attributes.Add("class", "row")
                    Dim col_counter As Integer = 0

                    If files.Length > 0 Then
                        For Each singleFile In files
                            If singleFile.Name.Contains(Me.global_rma.ubo_bulk_return.field_get("rma_code")) Then

                                Dim start As Integer = singleFile.Name.LastIndexOf("]"c)
                                Dim start_ord_nr As Integer = singleFile.Name.LastIndexOf("{"c)

                                Dim prod_code_from_file = singleFile.Name.Substring(start + 1, singleFile.Name.LastIndexOf("{"c) - start - 1)
                                Dim ord_nr_from_file = singleFile.Name.Substring(start_ord_nr + 1, singleFile.Name.LastIndexOf("."c) - start_ord_nr - 1)

                                If prod_code_from_file = prod_code And ord_nr = ord_nr_from_file Then
                                    Dim filename As String = singleFile.Name

                                    Dim col As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
                                    col.Attributes.Add("class", "col-md-4")

                                    Dim image As New Utilize.Web.UI.WebControls.image
                                    image.Attributes.Add("class", "img-responsive")
                                    image.ImageUrl = Me.ResolveUrl("~/documents/rma/tempImages/") + filename

                                    Dim remove_link As New button_remove_file
                                    remove_link.CommandArgument = Server.MapPath("~/documents/rma/tempImages/")
                                    remove_link.CommandName = filename
                                    remove_link.UserControl = Me
                                    remove_link.ph_images = ph_uploaded_images
                                    remove_link.CssClass = "dropzone-remove-link"
                                    remove_link.Text = global_trans.translate_button("link_remove_file", "Verwijder bestand", Me.global_cms.Language)

                                    col.Controls.Add(image)
                                    col.Controls.Add(remove_link)
                                    row.Controls.Add(col)

                                    col_counter += 1
                                    If col_counter = 3 Then
                                        Dim clearfix As New HtmlGenericControl("div")
                                        clearfix.Attributes.Add("class", "clearfix")
                                        row.Controls.Add(clearfix)
                                        col_counter = 0
                                    End If

                                    If Not ph_uploaded_images.Visible Then
                                        ph_uploaded_images.Visible = True
                                    End If

                                End If
                            End If
                        Next
                    End If
                    ph_uploaded_images.Controls.Add(row)
                End If
            End If
        Next
    End Sub

    Private Sub Webshop_Account_AccountBlocks_uc_return_request_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Select Case hih_active_tab.Value
            Case "pnl_orders"
                Me.li_orders.Attributes.Add("class", "active")
                Me.li_bulk.Attributes.Remove("class")

                Me.pnl_orders.CssClass = "tab-pane active"
                Me.pnl_bulk_return.CssClass = "tab-pane"
            Case "pnl_bulk_return"
                Me.li_bulk.Attributes.Add("class", "active")
                Me.li_orders.Attributes.Remove("class")

                Me.pnl_orders.CssClass = "tab-pane"
                Me.pnl_bulk_return.CssClass = "tab-pane active"
            Case Else
                Exit Select
        End Select

        Me.col_row_amount.Visible = Not prices_not_visible
        Me.col_amount_select.Visible = Not prices_not_visible

        Me.label_row_amt_bulk.Visible = Not prices_not_visible
        Me.label_row_amount.Visible = Not prices_not_visible
    End Sub

    Private Sub save_bulk_request(sender As Object, e As EventArgs) Handles button_create_return.Click
        Dim ll_testing = True
        Me.message_error.Visible = False

        'test om te controleren of de reden en compensatie ingevult zijn
        If ll_testing Then
            'If Me.dd_return_comp.Text = "0000" Or Me.dd_return_reason.Text = "0000" Then
            If Me.dd_bulk_comp_method.Text = "0000" Then
                ll_testing = False
            End If

            If Not ll_testing Then
                Me.alert_comp_method.Visible = True
                Me.alert_comp_method.Text = global_trans.translate_message("msg_no_comp", "Er is geen compensatie geselecteerd.", Me.global_cms.Language)
            End If
        End If

        'haal het comp_type op van het retour
        Dim qsc_comp As New Utilize.Data.QuerySelectClass
        qsc_comp.select_fields = "comp_type"
        qsc_comp.select_from = "uws_rma_compensation"
        qsc_comp.select_where = "comp_code = @comp_code"
        qsc_comp.add_parameter("@comp_code", Me.dd_bulk_comp_method.SelectedItem.Value)

        Dim dt_comp_check As System.Data.DataTable = qsc_comp.execute_query()
        'test of er wel alternatieve producten opgegeven zijn
        If ll_testing Then
            If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                If Not Me.rpt_alt_prod.Items.Count > 0 Then
                    ll_testing = False
                End If
            End If

            If Not ll_testing Then
                Me.ph_no_alt_products.Visible = True
            Else
                Me.ph_no_alt_products.Visible = False
            End If
        End If

        Dim total As Decimal = set_total()
        Dim max As Decimal = set_sub_totals_bulk()

        'check of het bedrag van de alternatieve producten niet hoger is dan het maximale bedrag
        If ll_testing Then
            If dt_comp_check.Rows(0).Item("comp_type") = "1" Then


                If total > max Then
                    ll_testing = False
                End If

                If Not ll_testing Then
                    Me.ph_alt_prod_to_high.Visible = True
                Else
                    Me.ph_alt_prod_to_high.Visible = False
                End If

            End If
        End If

        'verwerk de gegevens en maak een nieuwe retour aan
        If ll_testing Then
            Dim lt_rma_code As String = Me.global_rma.ubo_bulk_return.field_get("rma_code")

            If ll_testing Then
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.rma_code", lt_rma_code)
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.req_date", String.Format("{0:dd/MM/yyyy}", DateTime.Now))
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.req_status", "Verzoek")
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.cust_id", Me.global_ws.user_information.user_customer_id)
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.comp_code", Me.dd_bulk_comp_method.Text)
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.reason_comm", Me.text_bulk_reason.Text)
                Me.global_rma.ubo_bulk_return.field_set("uws_rma.lng_code", Me.global_ws.Language)

                If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma.return_amount", max - total)
                ElseIf dt_comp_check.Rows(0).Item("comp_type") = "4" Then
                    Me.global_rma.ubo_bulk_return.field_set("uws_rma.return_amount", max)
                End If

                If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                    Dim ll_move_next As Boolean = Me.global_rma.ubo_bulk_return.move_first("uws_rma_alt_prod")

                    Do While ll_move_next

                        Me.global_rma.ubo_bulk_return.field_set("uws_rma_alt_prod.rma_code", lt_rma_code)

                        ll_move_next = Me.global_rma.ubo_bulk_return.move_next("uws_rma_line")
                    Loop
                End If

                Dim ll_move_next_lines As Boolean = Me.global_rma.ubo_bulk_return.move_first("uws_rma_line")

                Do While ll_move_next_lines

                    Me.global_rma.ubo_bulk_return.field_set("uws_rma_line.rma_code", lt_rma_code)
                    Me.global_rma.ubo_bulk_return.table_update(Me.global_ws.user_information.user_id)

                    handle_uploaded_images(lt_rma_code, Me.global_rma.ubo_bulk_return.field_get("uws_rma_line.rec_id"), Me.global_rma.ubo_bulk_return.field_get("uws_rma_line.prod_code"), Me.global_rma.ubo_bulk_return.field_get("uws_rma_line.ord_nr"))

                    ll_move_next_lines = Me.global_rma.ubo_bulk_return.move_next("uws_rma_line")
                Loop
                Me.global_rma.ubo_bulk_return.table_update(Me.global_ws.user_information.user_id)

                If ll_testing Then
                    Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
                    ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)
                    ubo_change.field_set("uws_rma_changes.change_user", "klant" + Me.global_ws.user_information.ubo_customer.field_get("bus_name"))
                    ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
                    ubo_change.field_set("uws_rma_changes.rma_code", lt_rma_code)
                    ubo_change.field_set("uws_rma_changes.change_status", "verzoek")
                    ubo_change.field_set("uws_rma_changes.lng_code", Me.global_ws.Language)

                    ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)

                    If Not ll_testing Then
                        Me.message_error.Text = Me.global_rma.ubo_bulk_return.error_message
                        Me.message_error.Visible = True
                    End If
                End If

                If ll_testing Then
                    Me.ph_alt_pro_totals.Visible = False
                    Me.pnl_bulk_return.Visible = False
                    Me.ph_confirmation.Visible = True
                    Me.ph_tabs.Visible = False
                    Me.ph_returns_detail.Visible = False
                    Me.button_proceed.Visible = False
                    Me.button_bulk_return_close.Visible = True
                    Me.global_rma.ubo_bulk_return = Utilize.Data.DataProcedures.CreateRecordObject("uws_rma", "", "rma_code")
                    Me.global_rma.ubo_bulk_return.table_insert_row("uws_rma", Me.global_ws.user_information.user_id)
                    Me.rpt_bulk_detail.DataBind()
                    If dt_comp_check.Rows(0).Item("comp_type") = "1" Then
                        Me.rpt_alt_prod.DataBind()
                        Me.ph_alt_product_selectie.Visible = False
                    End If
                    Me.rpt_bulk_return.DataBind()
                    Me.ph_bulk_details.Visible = False
                    Me.txt_bulk_product.Text = ""
                    Me.text_bulk_reason.Text = ""
                End If

            End If
        End If
    End Sub

    Private Sub button_bulk_return_close_Click(sender As Object, e As EventArgs) Handles button_bulk_return_close.Click
        Me.ph_confirmation.Visible = False
        Me.pnl_bulk_return.Visible = True
        Me.ph_tabs.Visible = True
        Me.button_bulk_return_close.Visible = False
    End Sub

#End Region
    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

    Protected Sub rpt_return_orders_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_return_orders.ItemCreated

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_order_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If
    End Sub

    Protected Sub rpt_return_order_detail_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_return_order_detail.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            If e.Item.Visible = True Then
                Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("row_row_amt")
                lt_literal.Visible = Not prices_not_visible
            End If
        End If
    End Sub

    Protected Sub rpt_alt_prod_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_alt_prod.ItemCreated

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lbl_label As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_prod_price1")
            lbl_label.Visible = Not prices_not_visible
            Dim lbl_label1 As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_row_amount1")
            lbl_label1.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("row_prod_price")
            lt_literal.Visible = Not prices_not_visible
            Dim lt_literal1 As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("row_amount")
            lt_literal1.Visible = Not prices_not_visible
        End If
    End Sub
End Class