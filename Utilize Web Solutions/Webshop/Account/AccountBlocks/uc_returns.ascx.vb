﻿
Imports System.Web.UI.WebControls

Partial Class Webshop_Account_AccountBlocks_uc_returns
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Public Function get_returns(Optional search As Boolean = False) As System.Data.DataTable
        Dim qsc_returns As New Utilize.Data.QuerySelectClass
        qsc_returns.select_fields = "uws_rma.*,uws_customers.*,uws_rma_compensation.*, uws_history.*"
        qsc_returns.select_from = "uws_rma"
        qsc_returns.add_join("left join", "uws_customers", "uws_rma.cust_id = uws_customers.rec_id")
        qsc_returns.add_join("left join", "uws_rma_compensation", "uws_rma.comp_code = uws_rma_compensation.comp_code")
        qsc_returns.add_join("left join", "uws_history", "uws_rma.ord_nr = uws_history.crec_id")
        qsc_returns.select_where = "uws_rma.cust_id = @cust_id"
        qsc_returns.select_order = "rma_code DESC"
        qsc_returns.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)
        qsc_returns.select_order = "rma_code DESC"

        'If a status is selected in the search filter, then add it
        If Me.ddl_search_status.SelectedIndex > 0 Then
            qsc_returns.select_where += " AND req_status = @status"
            qsc_returns.add_parameter("@status", Me.ddl_search_status.SelectedValue)
        ElseIf Not Me.get_page_parameter("filter") = "" Then
            If Me.get_page_parameter("filter").ToLowerInvariant = "afgerond" Then
                qsc_returns.select_where += " AND req_status = 'Afgerond'"
            Else
                qsc_returns.select_where += " AND NOT (req_status = 'Afgerond')"
            End If
        End If

        'als de optionele parameter true is wordt er gecheckt op de search functies
        If search Then
            'filter op rma code
            If Not String.IsNullOrEmpty(Me.txt_rma_code.Text) Then
                qsc_returns.select_where += " AND RMA_CODE like @rma_code"
                qsc_returns.add_parameter("@rma_code", Me.txt_rma_code.Text + "%")
            End If

            'check op factuurnummer
            If Not String.IsNullOrEmpty(Me.txt_invoice_nr.Text) Then
                qsc_returns.select_where += " AND rma_code in (select hdr_id from uws_rma_line where doc_nr = @doc_nr) OR ord_nr in (select crec_id from uws_history where doc_nr = @doc_nr)"
                qsc_returns.add_parameter("@doc_nr", Me.txt_invoice_nr.Text)
            End If

            'check op ordernummer
            If Not String.IsNullOrEmpty(Me.txt_order_code.Text) Then
                qsc_returns.select_where += " AND uws_rma.ord_nr like @ord_nr OR uws_rma.rma_code in (select hdr_id from uws_rma_line where ord_nr like @ord_nr)"
                qsc_returns.add_parameter("@ord_nr", "%" + Me.txt_order_code.Text + "%")
            End If

            'check op datum
            If Not String.IsNullOrEmpty(Me.txt_request_day.Text) Or Not String.IsNullOrEmpty(Me.txt_request_month.Text) Or Not String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                Dim req_date As String = ""

                'bouw de string voor de search op de request date hier op
                If String.IsNullOrEmpty(Me.txt_request_year.Text) Then
                    req_date += "%-"
                Else
                    req_date += Me.txt_request_year.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_month.Text) Then
                    req_date += "%-"
                Else
                    req_date += "%" + Me.txt_request_month.Text + "-"
                End If

                If String.IsNullOrEmpty(Me.txt_request_day.Text) Then
                    req_date += "%"
                Else
                    req_date += "%" + Me.txt_request_day.Text + "%"
                End If

                qsc_returns.select_where += " AND CONVERT(VARCHAR, req_date, 120) like @req_date"
                qsc_returns.add_parameter("@req_date", req_date)
            End If
        End If

        Dim dt_returns As New System.Data.DataTable
        dt_returns = qsc_returns.execute_query()

        'vertaal de status naar de juiste taal
        For Each row In dt_returns.Rows
            dim lc_req_status as string = row.item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "Verzoek"
                    row.item("req_status") = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "Afgekeurd"
                    row.item("req_status") = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                Case "Afgerond"
                    row.item("req_status") = global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                Case "In behandeling"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    row.item("req_status") = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select
        Next

        Return dt_returns
    End Function

#Region "Page subs"

    Protected Sub pageInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/" + me.global_ws.language + "/Webshop/login.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        'als er een page parameter is ga naar de rma met de matchende rma code
        If Not Me.get_page_parameter("rmacode") = "" Then
            If Utilize.Data.DataProcedures.GetValue("uws_rma", "cust_id", Me.get_page_parameter("rmacode")) = Me.global_ws.user_information.user_customer_id Then
                set_return_details(Me.get_page_parameter("rmacode"))
            Else
                Me.ph_return_detail.Visible = False
                Me.ph_returns.Visible = False
                Me.ph_wrong_page_param.Visible = True
                Me.button_my_account.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_reject_reason.Visible = False
            End If
        End If

        Me.set_style_sheet("uc_returns.css", Me)
        Me.set_javascript("uc_returns.js", Me)

        Dim dt_returns As New System.Data.DataTable
        dt_returns = Me.get_returns()

        'check of er retouren aanwezig zijn
        If dt_returns.Rows.Count() > 0 Then
            rpt_returns.DataSource = dt_returns
            rpt_returns.DataBind()
            For Each item In rpt_returns.Items
                Dim rma_link As New LinkButton
                rma_link = item.findcontrol("row_rma_code")
                'maak een postback trigger aan
                Dim loPostbackTrigger As New System.Web.UI.PostBackTrigger
                loPostbackTrigger.ControlID = rma_link.UniqueID

                CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
                CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loPostbackTrigger)

                System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(rma_link)

            Next
            set_status_styling()
        Else
            Me.ph_no_returns.Visible = True
            Me.ph_returns.Visible = False
        End If

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        Me.show_upload_image.Text = global_trans.translate_label("label_add_file", "Een bestand bijvoegen", Me.global_ws.Language)

        If Me.global_rma.get_rma_setting("allow_images") Then
            ' Voeg hier de postbacktriggers toe
            Dim loUploadTrigger As New System.Web.UI.PostBackTrigger
            loUploadTrigger.ControlID = button_rma_user_input.UniqueID

            CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
            CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loUploadTrigger)

            System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(button_rma_user_input)
        End If

        set_status_styling()

        'Set status
        Dim qsc_status As New Utilize.Data.QuerySelectClass
        qsc_status.select_fields = "distinct req_status"
        qsc_status.select_from = "uws_rma"

        Dim dt_status As New System.Data.DataTable
        dt_status = qsc_status.execute_query()

        'Insert status selection
        Dim top_row_default_comp As System.Data.DataRow = dt_status.NewRow()
        top_row_default_comp("req_status") = global_trans.translate_label("label_status_select", "-selecteer een status-", Me.global_ws.Language)
        dt_status.Rows.InsertAt(top_row_default_comp, 0)

        Me.ddl_search_status.DataSource = dt_status
        Me.ddl_search_status.DataTextField = "req_status"
        Me.ddl_search_status.DataValueField = "req_status"
        Me.ddl_search_status.DataBind()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "setChange", "$(document).ready(function () {set_change('" + Me.show_upload_image.ClientID + "');});", True)
    End Sub

    Protected Sub show_input(sender As Object, e As System.EventArgs)
        'If DirectCast(sender, System.Web.UI.WebControls.CheckBox).Checked = True Then
        '    Me.ph_show_upload.Visible = True
        'Else
        '    Me.ph_show_upload.Visible = False
        'End If
    End Sub

    Private Sub set_status_styling()
        For Each item In Me.rpt_returns.Items
            Dim cb_status As System.Web.UI.WebControls.Label
            cb_status = item.findcontrol("row_status")

            'zet per soort status de styling
            Select Case cb_status.Text
                Case global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-default")
                Case global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-danger")
                Case global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-primary")
                Case global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-success")
                Case global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                    cb_status.Attributes.Add("class", "label rounded label-warning")
                Case Else
                    Exit Select
            End Select
        Next
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_width", "$(document).ready(function () {set_status_width();});", True)
    End Sub

    Private Sub set_return_details(rma_code As String)
        Me.ph_return_detail.Visible = True
        Me.ph_returns.Visible = False
        Me.button_close_details.Visible = True
        Me.ph_assigned_comp.Visible = False
        Me.ph_reject_reason.Visible = False
        Me.text_respond.Text = ""

        'get het retour wat het meegegeven rma code heeft
        Dim qsc_return_details As New Utilize.Data.QuerySelectClass
        qsc_return_details.select_fields = "uws_rma.*,uws_history.doc_nr,uws_customers.bus_name"
        qsc_return_details.select_from = "uws_rma, uws_history,uws_customers"
        qsc_return_details.select_where = "rma_code = @rma_code and uws_rma.cust_id = uws_customers.rec_id "
        qsc_return_details.add_parameter("@rma_code", rma_code)
        qsc_return_details.select_order = "rma_code DESC"
        Dim dt_return_details As New System.Data.DataTable
        dt_return_details = qsc_return_details.execute_query()

        If dt_return_details.Rows.Count > 0 Then

            'get de compensatie omschrijving van de compensatie die aan het retour gekoppeld is
            Dim qsc_comp_text As New Utilize.Data.QuerySelectClass
            qsc_comp_text.select_fields = "*"
            qsc_comp_text.select_from = "UWS_RMA_COMPENSATION"
            qsc_comp_text.select_where = "comp_code = @comp_code"
            qsc_comp_text.add_parameter("@comp_code", dt_return_details.Rows(0).Item("comp_code"))
            Dim dt_comp_text As New System.Data.DataTable
            dt_comp_text = qsc_comp_text.execute_query()

            Dim lc_comp_text = dt_comp_text.Rows(0).Item("comp_desc_" + Me.global_ws.Language)

            'set de label waardes naar de informatie uit de database
            Me.lbl_return_number.Text = dt_return_details.Rows(0).Item("rma_code")
            Me.lbl_return_customer.Text = dt_return_details.Rows(0).Item("bus_name")
            Me.lbl_return_order_number.Text = dt_return_details.Rows(0).Item("doc_nr")
            Me.lbl_compensation.Text = lc_comp_text
            Me.lbl_req_date.Text = dt_return_details.Rows(0).Item("req_date")

            dim lc_req_status as string = dt_return_details.Rows(0).Item("req_status")
            Select Case lc_req_status
                Case "Goedgekeurd"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_accepted", "Goedgekeurd", Me.global_ws.Language)
                Case "Verzoek"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_return_request", "Verzoek", Me.global_ws.Language)
                Case "Afgekeurd"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language)
                Case "Afgerond"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_complete", "Afgerond", Me.global_ws.Language)
                Case "In behandeling"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case "In behandeling na ontvangst"
                    Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language)
                Case Else
                    Exit Select
            End Select

            Me.lbl_reason_description.Text = dt_return_details.Rows(0).Item("reason_comm")

            'check of de status afgewezen is
            If dt_return_details.Rows(0).Item("req_status") = global_trans.translate_label("label_rejected", "Afgekeurd", Me.global_ws.Language) Then
                Me.ph_reject_reason.Visible = True
                Me.lbl_reject_reason.Text = dt_return_details.Rows(0).Item("reject_reason")
            End If

            'check of de compensatie een alternatief product is
            If dt_comp_text.Rows(0).Item("comp_type") = "1" Then
                Me.ph_alt_prod_table.Visible = True
                Dim qsc_alt_prods As New Utilize.Data.QuerySelectClass
                qsc_alt_prods.select_fields = "*"
                qsc_alt_prods.select_from = "uws_rma_alt_prod"
                qsc_alt_prods.select_where = "rma_code = @rma_code"
                qsc_alt_prods.add_parameter("@rma_code", rma_code)
                Dim dt_alt_prods As System.Data.DataTable = qsc_alt_prods.execute_query()

                Me.rpt_alt_prod.DataSource = dt_alt_prods
                Me.rpt_alt_prod.DataBind()
            Else
                Me.ph_alt_prod_table.Visible = False
            End If

            'check of er een compensatie toegewezen is door een medewerker
            If Not dt_return_details.Rows(0).Item("asgn_comp_code") = "" Then
                Me.ph_assigned_comp.Visible = True

                Dim qsc_assigned As New Utilize.Data.QuerySelectClass
                qsc_assigned.select_fields = "comp_desc_" + Me.global_ws.Language
                qsc_assigned.select_from = "uws_rma_compensation"
                qsc_assigned.select_where = "comp_code = @asgn_comp_code"
                qsc_assigned.add_parameter("@asgn_comp_code", dt_return_details.Rows(0).Item("asgn_comp_code"))

                Dim dt_asgn_compensation As System.Data.DataTable
                dt_asgn_compensation = qsc_assigned.execute_query()

                Me.lbl_assigned_compensation.Text = dt_asgn_compensation.Rows(0).Item("comp_desc_" + Me.global_ws.Language)

            End If

            'check of de status in behandeling is
            If Me.lbl_req_status.Text = global_trans.translate_label("label_in_progress", "In behandeling", Me.global_ws.Language) Then
                Me.ph_respond_block.Visible = True
                If Me.global_rma.get_rma_setting("allow_images") Then
                    Me.ph_upload_image.Visible = True
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "setChange", "$(document).ready(function () {set_change('" + Me.show_upload_image.ClientID + "');});", True)
                Else
                    Me.ph_upload_image.Visible = False
                End If
            End If

            'get de bij behorende detail gegevens van het retour
            Dim qsc_return_lines As New Utilize.Data.QuerySelectClass
            qsc_return_lines.select_fields = "*"
            qsc_return_lines.select_from = "uws_rma_line"
            qsc_return_lines.select_where = "hdr_id = @hdr_id"
            qsc_return_lines.add_parameter("@hdr_id", rma_code)
            Dim dt_return_lines As New System.Data.DataTable
            dt_return_lines = qsc_return_lines.execute_query()

            Me.rpt_return_detail.DataSource = dt_return_lines
            Me.rpt_return_detail.DataBind()

            Me.uc_change_overview.set_property("rma_code", rma_code)
            Me.ph_change_overview.Visible = True

            If dt_return_details.Rows(0).Item("req_status") = "Goedgekeurd" Or dt_return_details.Rows(0).Item("req_status") = "In behandeling na ontvangst" Then
                ph_rma_form.Visible = True
            Else
                ph_rma_form.Visible = False
            End If
        Else
            Me.ph_return_detail.Visible = False
            Me.ph_returns.Visible = False
            Me.ph_wrong_page_param.Visible = True
            Me.button_my_account.Visible = False
            Me.button_close_details.Visible = False
            Me.ph_reject_reason.Visible = False
        End If

        'Set prices visible or not, depending on the prices_not_visible boolean below
        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        Me.col_row_amt.Visible = Not prices_not_visible
        Me.col_btw_amt.Visible = Not prices_not_visible
        Dim order_details As Integer = 0
        For Each row_item As UI.WebControls.RepeaterItem In rpt_return_detail.Items
            Dim lt_amount_literal As Utilize.Web.UI.WebControls.literal = row_item.FindControl("lt_return_detail_row_amount")
            lt_amount_literal.Visible = Not prices_not_visible

            Dim lt_vat_literal As Utilize.Web.UI.WebControls.literal = row_item.FindControl("lt_return_detail_row_vat")
            lt_vat_literal.Visible = Not prices_not_visible
        Next

        Me.col_row_amt1.Visible = Not prices_not_visible
        Me.col_btw_amt1.Visible = Not prices_not_visible
        Dim order_alts As Integer = 0
        For Each row_item As UI.WebControls.RepeaterItem In rpt_alt_prod.Items
            Dim lt_amount_literal As Utilize.Web.UI.WebControls.literal = row_item.FindControl("lt_return_alt_row_amount")
            lt_amount_literal.Visible = Not prices_not_visible

            Dim lt_vat_literal As Utilize.Web.UI.WebControls.literal = row_item.FindControl("lt_return_alt_row_vat")
            lt_vat_literal.Visible = Not prices_not_visible
        Next
    End Sub

    Protected Sub rpt_orders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_returns.ItemCommand
        If e.CommandName = "Return" Then
            Dim lc_rma_code = e.CommandArgument
            Me.set_return_details(lc_rma_code)
        End If
    End Sub

#End Region

    Protected Sub button_cancel_clicked(sender As Object, e As System.EventArgs) Handles button_close_details.Click
        'sluit de retour detail pagina
        Me.ph_returns.Visible = True
        Me.ph_return_detail.Visible = False
        Me.button_close_details.Visible = False
        Me.ph_change_overview.Visible = False
        Me.ph_respond_block.Visible = False

        Dim dt_returns As New System.Data.DataTable
        'dit zorgt er voor dat als er een search gedaan was voordat het de details geopend waren weer terug keert
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If

        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_search_clicked(sender As Object, e As System.EventArgs) Handles button_search.Click
        'open de search
        Me.ph_search.Visible = True
        Me.button_search.Visible = False
        Me.button_filter_remove.Visible = False
        Me.button_close_search.Visible = True
        Me.ddl_search_status.SelectedIndex = 0
    End Sub

    Protected Sub button_search_close_clicked(sender As Object, e As System.EventArgs) Handles button_close_search.Click
        'sluit de search box
        Me.ph_search.Visible = False
        Me.button_search.Visible = True
        Me.button_close_search.Visible = False

        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()
        Me.rpt_returns.DataSource = dt_returns
        Me.rpt_returns.DataBind()
        set_status_styling()

        If Me.ph_no_found.Visible Then
            Me.ph_no_found.Visible = False
            Me.ph_return_table.Visible = True
        End If

        'reset de search fields zodat ze weer leeg zijn
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""
        Me.button_filter_remove.Visible = False
    End Sub

    Protected Sub button_filter_clicked(sender As Object, e As System.EventArgs) Handles button_filter.Click
        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        'reset de pagina zodat alles weer zichtbaar is
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            'als het no found zichtbaar is dan wordt deze weer ontzichtbaar gemaakt en de normale weergave wordt weer zichtbaar
            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If

    End Sub

    Protected Sub button_filter_remove_clicked(sender As Object, e As System.EventArgs) Handles button_filter_remove.Click
        Me.ddl_search_status.SelectedIndex = 0
        Dim dt_returns As New System.Data.DataTable
        dt_returns = get_returns()

        'reset de search fields zodat ze leeg zijn
        Me.button_filter_remove.Visible = False
        Me.txt_rma_code.Text = ""
        Me.txt_request_year.Text = ""
        Me.txt_request_month.Text = ""
        Me.txt_request_day.Text = ""

        'rebind de repeater zodat de table weer alle retouren weer geeft
        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack() Then
            Me.set_status_styling()

        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "set_data_targets" + Me.UniqueID, "$(document).ready(function () {set_caret_links();});", True)
    End Sub

    Protected Sub button_rma_overview_Click(sender As Object, e As EventArgs) Handles button_rma_overview.Click
        'sluit de retour detail pagina
        Me.ph_returns.Visible = True
        Me.ph_wrong_page_param.Visible = False
        Me.ph_respond_block.Visible = False
        Me.button_my_account.Visible = True

        Me.remove_page_parameter("rmacode")

        Dim dt_returns As New System.Data.DataTable
        'dit zorgt er voor dat als er een search gedaan was voordat het de details geopend waren weer terug keert
        dt_returns = get_returns(True)

        Me.button_filter_remove.Visible = True

        If dt_returns.Rows.Count > 0 Then
            Me.rpt_returns.DataSource = dt_returns
            Me.rpt_returns.DataBind()
            set_status_styling()

            If Me.ph_no_found.Visible Then
                Me.ph_no_found.Visible = False
                Me.ph_return_table.Visible = True
            End If

        Else
            Me.ph_no_found.Visible = True
            Me.ph_return_table.Visible = False
        End If
    End Sub

    Protected Sub button_respond_click() Handles button_rma_user_input.Click
        Dim ll_testing As Boolean = True
        Me.label_upload_error.Text = ""
        Me.ph_upload_error.Visible = False

        'check of de response box niet leeg is
        If ll_testing Then
            ll_testing = Not Me.text_respond.Text = ""
            If Not ll_testing Then
                Me.show_upload_image.Checked = False
                Me.ph_respond_error.Visible = True
            End If
        End If

        If Me.ph_upload_image.Visible Then
            If Me.show_upload_image.Checked Then
                If ll_testing Then
                    ll_testing = FileUpload1.HasFile And Not FileUpload1.FileName = ""

                    If Not ll_testing Then
                        Me.label_upload_error.Text += global_trans.translate_message("message_import_file_not_selected", "Er is geen import bestand geselecteerd.", Me.global_ws.Language)
                        Me.show_upload_image.Checked = False
                        Me.ph_upload_error.Visible = True
                    End If
                End If

                If ll_testing Then
                    If FileUpload1.FileName.ToLower().EndsWith(".png") Or FileUpload1.FileName.ToLower().EndsWith(".jpg") Or FileUpload1.FileName.ToLower().EndsWith(".jpeg") Then
                        ll_testing = True
                    Else
                        ll_testing = False
                    End If

                    If Not ll_testing Then
                        Me.label_upload_error.Text = global_trans.translate_message("message_import_file_not_image", "Het geselecteerde bestand is geen png, jpg of jpeg bestand.", Me.global_ws.Language)
                        Me.show_upload_image.Checked = False
                        Me.ph_upload_error.Visible = True
                    End If
                End If
            End If
        End If


        If ll_testing Then
            'maak een business object aan om een rma_change te maken
            Dim ubo_change As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_rma_changes")
            Dim filename As String = ""

            If Me.ph_upload_image.Visible Then
                If Me.show_upload_image.Checked Then
                    Dim path As String = Server.MapPath("~/Documents/RMA/")
                    filename = Me.lbl_return_number.Text + " - " + Me.FileUpload1.FileName
                    Try
                        Me.FileUpload1.PostedFile.SaveAs(path & filename)
                    Catch ex As Exception
                        Me.label_upload_error.Text = global_trans.translate_message("message_import_file_error", "Uploaden mislukt.", Me.global_ws.Language)
                        Me.ph_upload_error.Visible = True
                        ll_testing = False
                    End Try
                End If
            End If

            If ll_testing Then
                ll_testing = ubo_change.table_insert_row("uws_rma_changes", Me.global_ws.user_information.user_id)
                If ll_testing Then
                    'het business object wordt hier gevult met de informatie die nodig is om een meer informatie bericht aan te maken vanuit de klant
                    ubo_change.field_set("uws_rma_changes.change_user", "klant" + Me.global_ws.user_information.ubo_customer.field_get("bus_name"))
                    ubo_change.field_set("uws_rma_changes.change_date", DateTime.Now)
                    ubo_change.field_set("uws_rma_changes.rma_code", Me.lbl_return_number.Text)
                    ubo_change.field_set("uws_rma_changes.change_status", "meer informatie")
                    ubo_change.field_set("uws_rma_changes.change_text", Me.text_respond.Text)

                    If Me.ph_upload_image.Visible Then
                        If Me.show_upload_image.Checked Then
                            ubo_change.field_set("uws_rma_changes.user_image_url", "Documents/RMA/" + filename)
                        End If
                    End If

                    ll_testing = ubo_change.table_update(Me.global_ws.user_information.user_id)
                End If
            End If
        End If

        If ll_testing Then
            Dim dt_returns As New System.Data.DataTable
            dt_returns = Me.get_returns()

            'check of er retouren aanwezig zijn
            If dt_returns.Rows.Count() > 0 Then
                rpt_returns.DataSource = dt_returns
                rpt_returns.DataBind()

                Me.show_upload_image.Checked = False
                Me.ph_no_returns.Visible = False
                Me.ph_returns.Visible = True
                Me.ph_return_detail.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_respond_block.Visible = False
                Me.ph_respond_error.Visible = False
                Me.ph_change_overview.Visible = False
            Else
                Me.show_upload_image.Checked = False
                Me.ph_no_returns.Visible = True
                Me.ph_returns.Visible = False
                Me.ph_return_detail.Visible = False
                Me.button_close_details.Visible = False
                Me.ph_respond_error.Visible = False
                Me.ph_respond_block.Visible = False
                Me.ph_change_overview.Visible = False
            End If
            Me.set_status_styling()
        End If
    End Sub



    Protected Sub button_rma_form_Click(sender As Object, e As EventArgs) Handles button_rma_form.Click
        Dim querystring As String = Me.ResolveCustomUrl("~/rma/RMAForm.aspx?rmacode=" + Me.lbl_return_number.Text)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "generate_rma_form", "popupwindow = window.open('" + querystring + "');", True)
    End Sub

    Private Sub rpt_return_detail_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_return_detail.ItemCreated
        If Not IsNothing(e.Item.DataItem) Then

            If Not e.Item.DataItem("org_return_qty") = e.Item.DataItem("return_qty") Then
                Me.ph_col_org_amt.Visible = True
                e.Item.FindControl("ph_row_org_amount").Visible = True
            Else
                Me.ph_col_org_amt.Visible = False
                e.Item.FindControl("ph_row_org_amount").Visible = False
            End If

            Dim uc_image_overview As ASP.rma_usercontrols_uc_image_overview_ascx = e.Item.FindControl("uc_image_overview")
            Dim qsc_images As New Utilize.Data.QuerySelectClass
            qsc_images.select_fields = "image"
            qsc_images.select_from = "uws_rma_line_images"
            qsc_images.select_where = "hdr_id = @hdr_id"
            qsc_images.add_parameter("@hdr_id", e.Item.DataItem("rec_id"))

            Dim dt_images As System.Data.DataTable = qsc_images.execute_query()
            uc_image_overview.dt_images = dt_images

            If dt_images.Rows.Count > 0 Then
                If Not Me.col_caret_down.Visible Then
                    Me.col_caret_down.Visible = True
                End If
                Dim td_caret_down As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("td_caret_down")
                td_caret_down.Visible = True
            End If
        End If
    End Sub
End Class
