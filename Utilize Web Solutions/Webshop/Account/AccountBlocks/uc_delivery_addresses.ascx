﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_delivery_addresses.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_orders" %>
<%@ Register Src="~/CMS/Modules/Text/uc_text_block.ascx" TagName="uc_text_block" tagprefix="uc" %>
<%@ Register Src="~/Webshop/Account/AccountBlocks/uc_change_address_block.ascx" tagname="uc_change_address_block" tagprefix="utilize" %>

 <div class="block account">
    <utilize:modalpanel runat="server" ID="pnl_overlay_delivery" Visible="false">
        <div class="modal-content">
            <div class="modal-header">
                <h2><utilize:translatetitle runat="server" ID="title_change_add_delivery_address" Text="Afleveradres toevoegen/wijzigen"></utilize:translatetitle></h2>
            </div>
            <div class="modal-body">
                <utilize:uc_change_address_block ID="uc_change_address_block" runat="server" address_type="shoppingcart_delivery_address" />
            </div>
                    
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_cancel1" Text="Annuleren" CssClass="left btn-u btn-u-sea-shop btn-u-lg" /> 
                <utilize:translatebutton runat="server" ID="button_change3" Text="Opslaan" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
            </div>
        </div>
    </utilize:modalpanel>

    <utilize:placeholder runat='server' ID="ph_delivery_addresses">
        <h1><utilize:translatetitle runat='server' id="title_delivery_address_overview" text="Overzicht afleveradressen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_delivery_addresses_overview" Text="Onderstaand vindt u een overzicht van afleveradressen die door u bewaard zijn."></utilize:translatetext>
        
        <utilize:repeater runat="server" ID="rpt_delivery_addresses" pager_enabled="true" page_size="10" pagertype="bottom">
            <HeaderTemplate>
                <div class="table-responsive">
                <table class="overview table table-striped">
                    <thead>
                        <tr class="header">
                            <th><utilize:translatelabel runat='server' ID="col_bus_name" Text="Bedrijfsnaam"></utilize:translatelabel></th>
                            <th><utilize:translatelabel runat='server' ID="col_zip_code" Text="Postcode"></utilize:translatelabel></th>
                            <th><utilize:translatelabel runat='server' ID="col_city" Text="Plaats"></utilize:translatelabel></th>
                            <th><utilize:translatelabel runat='server' ID="col_country" Text="Land"></utilize:translatelabel></th>
                            <th></th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td><%# DataBinder.Eval(Container.DataItem, "del_bus_name")%></td>
                        <td><%# DataBinder.Eval(Container.DataItem, "del_zip_code")%></td>
                        <td><%# DataBinder.Eval(Container.DataItem, "del_city")%></td>
                        <td><%# Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_ws.Language, DataBinder.Eval(Container.DataItem, "del_cnt_code")) %></td>
                        <td><utilize:translatelinkbutton CommandName="Edit" AutoPostBack="true" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id") %>' ID="link_change_selected_address" Text="Wijzig"></utilize:translatelinkbutton>&nbsp;&nbsp;<utilize:translatelinkbutton CommandName="Delete" OnClientClick="return confirm('Weet u zeker dat u dit adres wilt verwijderen?');" runat="server" ID="link_remove_selected_address" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id") %>' Text="Verwijder"></utilize:translatelinkbutton></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
    </utilize:placeholder>

    <utilize:placeholder runat='server' ID="ph_no_delivery_addresses" Visible='false'>
        <h1><utilize:translatetitle runat='server' id="title_no_delivery_addresses" text="Geen afleveradressen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_delivery_addresses" Text="Er zijn op dit moment geen afleveradressen door u vastgelegd."></utilize:translatetext>
    </utilize:placeholder>

    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_add_delivery_address" Text="Toevoegen" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>