﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_entry.ascx.vb" Inherits="uc_order_entry" %>

<h1><utilize:translatetitle runat='server' ID="title_order_entry" Text="Snel bestellen"></utilize:translatetitle></h1>
<utilize:panel runat="server" ID="pnl_order_entry" cssclass="uc_order_entry" DefaultButton="button_add_to_cart">
    <div class="table-responsive">
        <table class="overview table">
            <tr class="header">
                <th style="width: 140px; text-align: left;"><utilize:translateliteral runat='server' ID="label_product" text="Product"></utilize:translateliteral></th>
                <th style="text-align: left;"><utilize:translateliteral runat='server' ID="label_product_description" text="Productomschrijving"></utilize:translateliteral></th>
            
                <utilize:placeholder runat='server' ID="ph_sizes_title" Visible="false">
                    <th style="width: 140px; text-align: center;"><utilize:translatelabel runat='server' ID="label_sizes_title" Text="Maat"></utilize:translatelabel></th>
                </utilize:placeholder> 
                <th style="width: 100px; text-align: right;"><utilize:translatelabel runat="server" ID="label_product_price" text="Prijs"></utilize:translatelabel> </th>
                <th style="text-align: center;"><utilize:translateliteral runat='server' ID="label_order_quantity" Text="Aantal"></utilize:translateliteral></th>
            </tr>
            <tr>
                <td><utilize:placeholder runat="server" ID="ph_product_code"></utilize:placeholder></td>
                <td>
                    <utilize:literal runat='server' ID="lt_product_description"></utilize:literal> <br />
                    <utilize:placeholder runat='server' ID="ph_stock" Visible="True">
                        <utilize:placeholder runat="server" ID="ph_stock_control" Visible="False"></utilize:placeholder>
                    </utilize:placeholder>
                </td>
            
                <utilize:placeholder runat='server' ID="ph_sizes" Visible="False">
                    <td style="width: 100px; text-align: center;"><asp:DropDownList ID="ddl_sizes" runat="server" Visible="False" AutoPostBack='true'></asp:DropDownList></td>
                </utilize:placeholder> 
                <td style="width: 100px; text-align: right;"><utilize:literal runat='server' ID="lt_product_price"></utilize:literal></td>
           
                <td style="text-align: center;">
                    <utilize:placeholder runat="server" ID="ph_order_product"></utilize:placeholder>
                </td>
            </tr>
        </table>
    </div>

    <utilize:label runat='server' ID="label_error_message" CssClass="message" Visible='false'></utilize:label>

    <utilize:translatebutton runat='server' ID="button_add_to_cart" Text="Toevoegen aan winkelwagen" CssClass="account btn-u btn-u-sea-shop btn-u-lg pull-right" />

    <div class="margin-bottom-20"></div>

    <div class="shopping-cart">
        <utilize:placeholder runat="server" ID="ph_shopping_cart_overview"></utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_shopping_cart_totals"></utilize:placeholder>
    </div>

<div class="row">
    <div class="col-md-12 buttons">
        <utilize:translatebutton runat='server' ID="button_shopping_cart" Text="Winkelwagen"  CssClass="left account btn-u btn-u-sea-shop btn-u-lg pull-right" />
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>
</utilize:panel>