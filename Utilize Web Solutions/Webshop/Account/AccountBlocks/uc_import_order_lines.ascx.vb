﻿Imports System.Data.OleDb

Partial Class Bespoke_Account_AccountBlocks_uc_import_order_lines
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _success_list As New List(Of String)
    Private _failure_list As New List(Of String)
    Private _error_string As String

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if order import module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_import") Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.set_style_sheet("uc_import_order_lines.css", Me)
        Me.set_javascript("uc_import_order_lines.js", Me)
        Me.ph_result.Visible = False

        ' Voeg hier de postbacktriggers toe
        Dim loUploadTrigger As New System.Web.UI.PostBackTrigger
        loUploadTrigger.ControlID = button_import.UniqueID

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loUploadTrigger)

        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(button_import)
        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lb_download_csv)
        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lb_download_xml)
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack() Then
            Me.ph_csv.Visible = Me.opg_import_type.SelectedValue = "2"
            Me.ph_xml.Visible = Me.opg_import_type.SelectedValue = "1"
        End If
    End Sub

    Protected Sub button_import_Click(sender As Object, e As EventArgs) Handles button_import.Click
        Me.ph_result.Visible = False

        Me.lt_text_import_errors.Text = ""
        Me.label_error_message.Text = ""

        Select Case opg_import_type.SelectedValue
            Case "1"
                Dim ll_resume As Boolean = True

                If ll_resume Then
                    ll_resume = Me.CheckXML()
                End If

                If ll_resume Then
                    ll_resume = Me.ReadXML()
                End If

                If ll_resume Then
                    ' En maak de controls onzichtbaar
                    Me.ph_select_import_format.Visible = False
                    Me.ph_csv.Visible = False
                    Me.ph_xml.Visible = False
                    Me.button_import.Visible = False
                End If
            Case "2"
                Dim ll_resume As Boolean = True

                If ll_resume Then
                    ll_resume = Me.CheckCSV()
                End If

                If ll_resume Then
                    ll_resume = Me.ReadCSV()
                End If

                If ll_resume Then
                    ' En maak de controls onzichtbaar
                    Me.ph_select_import_format.Visible = False
                    Me.ph_csv.Visible = False
                    Me.ph_xml.Visible = False
                    Me.button_import.Visible = False
                End If
            Case Else
                Exit Select
        End Select

        Import()
    End Sub

    Private Function Import() As Boolean
        Dim ll_resume As Boolean = True

        _success_list = New List(Of String)
        _failure_list = New List(Of String)

        ' Bepaal de rec_id
        For Each line As System.Data.DataRow In _OrderLines.Rows
            Dim lc_unit_code As String = ""

            If Not _OrderLines.Columns.Item("unit_code") Is Nothing Then
                lc_unit_code = line.Item("unit_code")
            End If

            If Me.global_ws.shopping_cart.product_add(line.Item("prod_code"), line.Item("qty"), "", lc_unit_code, "") Then
                _success_list.Add(line.Item(0))
            Else
                _failure_list.Add(line.Item(0))
                _error_string += global_trans.translate_message("message_product_not_found", "Het product met code [1] is niet gevonden", Me.global_ws.Language).Replace("[1]", line.Item(0)) + "<br>"
            End If
        Next

        show_result_screen()

        Return ll_resume
    End Function

    Private Sub show_result_screen()
        Me.ph_progress.Visible = False

        Dim ln_row_succesful As Integer = _success_list.Count
        Dim ln_row_failed As Integer = _failure_list.Count
        Dim lc_error_string As String = _error_string

        ' Toon de juiste resultaten
        Me.ph_result.Visible = True
        Me.pnl_success.Visible = True
        Me.pnl_error.Visible = ln_row_failed > 0

        ' Titels en teksten goedzetten me juiste informatie
        Me.lt_title_import_succesful.Text = global_trans.translate_title("title_import_succesful", "[1] artikelen succesvol aan uw winkelwagen toegevoegd.", Me.global_ws.Language)
        Me.lt_title_import_succesful.Text = Me.lt_title_import_succesful.Text.Replace("[1]", ln_row_succesful.ToString())

        Me.lt_title_import_unsuccesful.Text = global_trans.translate_text("title_import_unsuccesful", "[1] artikelen niet succesvol aan uw winkelwagen toegevoegd.", Me.global_ws.Language)
        Me.lt_title_import_unsuccesful.Text = Me.lt_title_import_unsuccesful.Text.Replace("[1]", ln_row_failed.ToString())

        Me.lt_text_import_succesful.Text = global_trans.translate_text("text_import_succesful", "Wij hebben [1] orderregels succesvol ingelezen.<br>Deze orderregels zijn aan uw winkelwagen toegevoegd.", Me.global_ws.Language)
        Me.lt_text_import_succesful.Text = Me.lt_text_import_succesful.Text.Replace("[1]", ln_row_succesful.ToString())

        Me.lt_text_import_unsuccesful.Text = global_trans.translate_text("text_import_unsuccesful", "Wij hebben [1] orderregels niet succesvol ingelezen.<br>Deze orderregels zijn niet aan uw winkelwagen toegevoegd.", Me.global_ws.Language)
        Me.lt_text_import_unsuccesful.Text = Me.lt_text_import_unsuccesful.Text.Replace("[1]", ln_row_failed.ToString())

        Me.lt_text_import_errors.Text = lc_error_string

        Me.ph_csv.Visible = Me.opg_import_type.SelectedValue = "2"
        Me.ph_xml.Visible = Me.opg_import_type.SelectedValue = "1"
        Me.ph_select_import_format.Visible = True

        Me.button_import.Visible = True
    End Sub

    Private _XmlDocument As New System.Xml.XmlDocument
    Private _OrderLines As System.Data.DataTable = Nothing
    
    Private Function CheckXML() As Boolean
        Dim ll_resume As Boolean = True
        Dim lc_error_message As String = ""

        Me.label_error_message.Visible = False

        ' Controleer of een bestand is geselecteerd
        If ll_resume Then
            ll_resume = flu_order_lines_xml.HasFile And Not flu_order_lines_xml.FileName = ""

            If Not ll_resume Then
                Me.label_error_message.Text = global_trans.translate_message("message_import_file_not_selected", "Er is geen import bestand geselecteerd.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        ' Controleer of het om een XML bestand gaat
        If ll_resume Then
            ll_resume = flu_order_lines_xml.FileName.ToLower().EndsWith(".xml")

            If Not ll_resume Then
                Me.label_error_message.Text = global_trans.translate_message("message_import_file_not_xml", "Het geselecteerde bestand is geen XML bestand.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        Dim lc_xml_string As String = ""

        If ll_resume Then
            ' Lees het bestand in
            Dim str As System.IO.Stream = flu_order_lines_xml.PostedFile.InputStream()
            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(str)

            lc_xml_string = sr.ReadToEnd()

            Try
                _XmlDocument.LoadXml(lc_xml_string)
            Catch ex As Exception
                Me.label_error_message.Text = global_trans.translate_message("message_import_read_xml_faild", "Het is niet gelukt het XML bestand in te lezen.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End Try
        End If

        Return ll_resume
    End Function
    Private Function ReadXML() As Boolean
        _OrderLines = New System.Data.DataTable
        _OrderLines.TableName = "OrderLines"

        Dim dc_prod_code As New System.Data.DataColumn("prod_code")
        Dim dc_unit_code As New System.Data.DataColumn("unit_code")
        Dim dc_ord_qty As New System.Data.DataColumn("qty")

        _OrderLines.Columns.Add(dc_prod_code)
        _OrderLines.Columns.Add(dc_unit_code)
        _OrderLines.Columns.Add(dc_ord_qty)

        ' Hier gaan we de productcodes controleren.
        Dim loXmlNodePath As System.Xml.XmlNodeList = _XmlDocument.SelectNodes("order/products/product")

        For Each lo_node As System.Xml.XmlNode In loXmlNodePath
            Dim dr_data_row As System.Data.DataRow = _OrderLines.NewRow()
            dr_data_row.Item("prod_code") = lo_node.Item("code").InnerText
            If Not lo_node.Item("unit") Is Nothing Then
                dr_data_row.Item("unit_code") = lo_node.Item("unit").InnerText
            Else
                dr_data_row.Item("unit_code") = ""
            End If
            dr_data_row.Item("qty") = lo_node.Item("quantity").InnerText


            _OrderLines.Rows.Add(dr_data_row)
        Next

        Return True
    End Function

    Private Function CheckCSV() As Boolean
        Dim ll_resume As Boolean = True
        Dim lc_error_message As String = ""
        Me.label_error_message.Visible = False

        ' Controleer of een bestand is geselecteerd
        If ll_resume Then
            ll_resume = flu_order_lines_csv.HasFile And Not flu_order_lines_csv.FileName = ""

            If Not ll_resume Then
                Me.label_error_message.Text = global_trans.translate_message("message_import_file_not_selected", "Er is geen import bestand geselecteerd.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        ' Controleer of het om een XML bestand gaat
        If ll_resume Then
            ll_resume = flu_order_lines_csv.FileName.ToLower().EndsWith(".csv")

            If Not ll_resume Then
                Me.label_error_message.Text = global_trans.translate_message("message_import_file_not_csv", "Het geselecteerde bestand is geen CSV bestand.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        Return ll_resume
    End Function
    Private Function ReadCSV() As Boolean
        _OrderLines = New System.Data.DataTable
        _OrderLines.TableName = "OrderLines"

        Dim ll_resume As Boolean = True
        Dim lc_csv_string As String = ""

        ' Hier gaan we het bestand inlezen
        If ll_resume Then
            ' Lees het bestand in
            Dim str As System.IO.Stream = flu_order_lines_csv.PostedFile.InputStream()
            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(str)

            lc_csv_string = sr.ReadToEnd()
        End If

        If ll_resume Then
            Try
                Dim lines As String() = lc_csv_string.Split(ControlChars.Lf)
                Dim sArr As String() = lines(0).Split(";")
                Dim startLine As Integer = 1

                If lines(0).Contains("prod_code") Then
                    For Each s As String In sArr
                        _OrderLines.Columns.Add(New System.Data.DataColumn(s.Trim()))
                    Next
                Else
                    _OrderLines.Columns.Add(New System.Data.DataColumn("prod_code"))
                    If sArr.Length > 2 Then
                        _OrderLines.Columns.Add(New System.Data.DataColumn("unit_code"))
                    End If
                    _OrderLines.Columns.Add(New System.Data.DataColumn("qty"))
                    startLine = 0
                End If

                Dim row As System.Data.DataRow

                Dim finalLine As String = ""

                For I As Integer = 1 To lines.Length - 1
                    finalLine = lines(I)

                    ' Check if the line is empty
                    If Not String.IsNullOrEmpty(finalLine) Then
                        row = _OrderLines.NewRow()
                        row.ItemArray = finalLine.Split(";")
                        _OrderLines.Rows.Add(row)
                    End If
                Next

                _OrderLines.AcceptChanges()
            Catch ex As Exception
                ll_resume = False
            End Try
        End If

        If Not ll_resume Then
            Me.label_error_message.Text = global_trans.translate_message("message_import_incorrect_csv_file", "Het CSV bestand is niet juist opgebouwd.", Me.global_ws.Language)
            Me.label_error_message.Visible = True
        End If

        Return ll_resume
    End Function

    Protected Sub opg_import_type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opg_import_type.SelectedIndexChanged
        Me.ph_csv.Visible = Me.opg_import_type.SelectedValue = "2"
        Me.ph_xml.Visible = Me.opg_import_type.SelectedValue = "1"
    End Sub

    Private Sub lb_download_csv_Click(sender As Object, e As EventArgs) Handles lb_download_csv.Click
        Response.Clear()
        Response.ContentType = "text/csv"
        Response.WriteFile(Server.MapPath("~/webshop/account/exampleFiles/CsvExample.csv"))
        Response.AppendHeader("Content-Disposition", "attachment; filename=CsvExample.csv")
        Response.Flush()
        Response.End()
    End Sub

    Private Sub lb_download_xml_Click(sender As Object, e As EventArgs) Handles lb_download_xml.Click
        Response.Clear()
        Response.ContentType = "text/xml"
        Response.WriteFile(Server.MapPath("~/webshop/account/exampleFiles/XmlExample.xml"))
        Response.AppendHeader("Content-Disposition", "attachment; filename=XmlExample.Xml")
        Response.Flush()
        Response.End()
    End Sub
End Class
