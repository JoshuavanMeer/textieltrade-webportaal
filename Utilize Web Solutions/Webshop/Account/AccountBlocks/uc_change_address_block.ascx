﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_change_address_block.ascx.vb" Inherits="uc_change_address_block" %>

<div class="uc_change_address_block">
    <utilize:placeholder runat="server" ID="ph_address_information">
    <asp:HiddenField ID="txt_rec_id_field" Value="" runat="server"></asp:HiddenField>
        <div class="address_row country">
            <label><span class="description country"><utilize:translateliteral ID="label_address_country" runat="server" Text="Land"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
            <utilize:dropdownlist ID="cbo_address_country" runat="server" Cssclass="value country form-control" AutoPostBack="true"></utilize:dropdownlist>            
        </div>
        <utilize:placeholder runat='server' ID="ph_bus_name">
            <div class="address_row bus_name">
                 <label><span class="description bus_name"><utilize:translateliteral ID="label_address_bus_name" runat="server" Text="Bedrijfsnaam"></utilize:translateliteral></span>
                <utilize:placeholder runat='server' ID="ph_bus_name_mandatory">
                    <span class="mandatory_sign">&nbsp;*</span>
                </utilize:placeholder></label>
                <utilize:textbox ID="txt_address_bus_name" runat="server" Cssclass="value bus_name form-control"></utilize:textbox>

            </div>
        </utilize:placeholder>
        <div class="row">
            <div class="col-md-4">
                 <label><span class="description fst_name"><utilize:translateliteral ID="label_address_fst_name" runat="server" Text="Voornaam"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_fst_name" runat="server" Cssclass="value fst_name form-control"></utilize:textbox>
            </div>
            <div class="col-md-3">
                 <label><span class="description infix"><utilize:translateliteral ID="label_address_infix" runat="server" Text="Tussenvoegsel"></utilize:translateliteral></span></label>
                <utilize:textbox ID="txt_address_infix" runat="server" CssClass="value infix form-control"></utilize:textbox>
            </div>
            <div class="col-md-5">
                 <label><span class="description lst_name"><utilize:translateliteral ID="label_address_lst_name" runat="server" Text="Achternaam"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_lst_name" runat="server" Cssclass="value lst_name form-control"></utilize:textbox>
            
            </div>
        </div>

         <utilize:placeholder runat="server" ID="ph_address_0">
            <div class="address_row address">
                <label><span class="description address"><utilize:translateliteral ID="label_address_address" runat="server" Text="Adres"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_address" runat="server" Cssclass="value address form-control" ></utilize:textbox>
                
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_address_1">
            <div class="address_row address">
                 <label><span class="description street"><utilize:translateliteral ID="label_address_street" runat="server" Text="Straat"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_street1" runat="server" Cssclass="value street form-control"></utilize:textbox>
            </div>
            <div class="address_row address">
                <label><span class="description housenr"><utilize:translateliteral ID="label_address_housenr" runat="server" Text="Huisnr."></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_housenr1" runat="server" Cssclass="value housenr form-control" ></utilize:textbox>
                <label><span class="description houseadd"><utilize:translateliteral ID="label_address_housenr_add" runat="server" Text="Toev."></utilize:translateliteral></span></label>
                <utilize:textbox ID="txt_address_houseadd1" runat="server" Cssclass="value housenradd form-control"></utilize:textbox>
                <br style="clear:both;" />
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_address_2">
            <div class="address_row address">
                <span class="description housenr"><utilize:literal ID="label_address_housenr1" runat="server" Text="Huisnr."></utilize:literal></span>
                <utilize:textbox ID="txt_address_housenr2" runat="server" Cssclass="value housenr" MaxLength="10"></utilize:textbox><span class="mandatory_sign">&nbsp;*</span>
            </div>
            <div class="address_row address">
                <span class="description street"><utilize:literal ID="label_address_street1" runat="server" Text="Straat"></utilize:literal></span>
                <utilize:textbox ID="txt_address_street2" runat="server" Cssclass="value street" MaxLength="80"></utilize:textbox><span class="mandatory_sign">&nbsp;*</span>
            </div>
            <div class="address_row address">
                <span class="description houseadd2"><utilize:literal ID="label_address_housenr_add1" runat="server" Text="Toev."></utilize:literal></span>
                <utilize:textbox ID="txt_address_houseadd2" runat="server" Cssclass="value housenradd" MaxLength="10"></utilize:textbox>
            </div>
        </utilize:placeholder>
        <div class="row">
            <div class="col-md-2">
                 <label><span class="description zip_code"><utilize:translateliteral ID="label_address_zip_code" runat="server" Text="Postcode"></utilize:translateliteral></span><span class="mandatory_sign">*</span></label>
                <utilize:textbox ID="txt_address_zip_code" runat="server" Cssclass="value zip_code form-control"></utilize:textbox>
            
            </div>
            <div class="col-md-10">
                 <label><span class="description city"><utilize:translateliteral ID="label_address_city" runat="server" Text="Plaats"></utilize:translateliteral></span><span class="mandatory_sign">&nbsp;*</span></label>
                <utilize:textbox ID="txt_address_city" runat="server" Cssclass="value city form-control"></utilize:textbox>
            
            </div>
        </div>
         

        <utilize:label ID="message_error" runat="server" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message" Visible="false" />
    </utilize:placeholder>
    <br style="clear:both;" />
</div>

