﻿﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_history.ascx.vb" Inherits="uc_order_history" %>

<div class="block account uc_order_history">
    <asp:HiddenField runat="server" ID="hih_active_tab" Value="pnl_orders" />
    <utilize:placeholder runat="server" ID="ph_tabs">
          <ul class="nav nav-tabs" role="tablist">
            <li runat="server" id="li_orders" role="presentation"><utilize:hyperlink runat="server" id="hl_tab_orders" aria-controls="orders" role="tab" data-toggle="tab"><utilize:translateliteral runat="server" ID="label_history_orders" Text="Bestellingen"></utilize:translateliteral></utilize:hyperlink></li>
            <li runat="server" id="li_deliveries"  role="presentation"><utilize:hyperlink runat="server" id="hl_tab_deliveries" aria-controls="deliveries" role="tab" data-toggle="tab"><utilize:translateliteral runat="server" ID="label_history_deliveries" Text="Leveringen"></utilize:translateliteral></utilize:hyperlink></li>
          </ul>
    </utilize:placeholder>
      
    <div class="tab-content">
        <utilize:panel runat="server" role="tabpanel" class="tab-pane active" id="pnl_orders">
            <utilize:placeholder runat="server" ID="ph_orders_overview">
                <utilize:placeholder runat='server' ID="ph_orders">
                    <h1><utilize:translatetitle runat='server' id="title_order_history" Text="Eerder geplaatste bestellingen"></utilize:translatetitle></h1>
                    <utilize:translatetext runat='server' id="text_order_history" Text="Onderstaand vindt u een overzicht van bestellingen die u eerder bij ons heeft gedaan. Klik op het factuurnummer om het overzicht van de bestelling op te vragen."></utilize:translatetext>

                    <utilize:placeholder runat="server" ID="ph_search_history" Visible='false'>
                        <utilize:translatetext runat='server' ID="text_order_history_search" Text="Vul hieronder uw criteria in en kies voor zoeken om de zoekopdracht uit te voeren"></utilize:translatetext>

                        <utilize:panel runat='server' ID="pnl_search" DefaultButton="button_filter">
                            <div class="col-md-12 margin-bottom-10">
                                <div class="input-border row">
                                    <div class="row margin-bottom-10">
                                        <div class="col-md-6 col-xs-12">
                                            <utilize:translatelabel runat='server' ID="label_order_number_startswith" Text="Ordernummer begint met"></utilize:translatelabel>
                                            <utilize:textbox runat='server' ID="txt_ord_nr" CssClass="form-control"></utilize:textbox>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <utilize:translatelabel runat='server' ID="label_reference_contains" Text="Referentie bevat"></utilize:translatelabel>
                                            <utilize:textbox runat='server' ID="txt_cust_ref" CssClass="form-control"></utilize:textbox>
                                        </div>                          
                                    </div>
                                    <utilize:placeholder runat="server" ID="ph_normal" Visible="false">
                                        <div class="row margin-bottom-10">
                                            <div class="col-md-4 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="lable_year_number" Text="Jaartal"></utilize:translatelabel>
                                                <utilize:textbox runat="server" ID="txt_year" MaxLength="4" CssClass="form-control"></utilize:textbox>
                                            </div>                            
                                            <div class="col-md-4 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="lable_day_number" Text="Dag"></utilize:translatelabel>
                                                <utilize:textbox runat="server" ID="txt_day" MaxLength="2" CssClass="form-control"></utilize:textbox>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="label_month_number" Text="Maand"></utilize:translatelabel>
                                                <utilize:textbox runat="server" ID="txt_month" MaxLength="2" CssClass="form-control"></utilize:textbox>
                                            </div>                            
                                        </div>
                                        <div class="row margin-bottom-10">
                                            <div class="col-md-6 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="label_product_code_startswith" Text="Productcode begint met"></utilize:translatelabel>
                                                <utilize:textbox runat='server' ID="txt_prod_code" CssClass="form-control"></utilize:textbox>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="label_product_desc_contains" Text="Productomschr. bevat"></utilize:translatelabel>
                                                <utilize:textbox runat='server' ID="txt_prod_desc" CssClass="form-control"></utilize:textbox>
                                            </div>
                                        </div>
                                    </utilize:placeholder>                                    
                                    <utilize:placeholder runat="server" ID="ph_intermediate" Visible="false">
                                        <div class="row margin-bottom-10">
                                            <div class="col-md-6 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="lbl_start_date" Text="Datum vanaf"></utilize:translatelabel>
                                                <utilize:textbox runat="server" ID="txt_from_date" CssClass ="from-control" type="date"></utilize:textbox>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <utilize:translatelabel runat='server' ID="lbl_to_date" Text="Datum tot"></utilize:translatelabel>
                                                <utilize:textbox runat="server" ID="txt_to_date" CssClass ="from-control" type="date"></utilize:textbox>
                                            </div>
                                        </div>
                                    </utilize:placeholder>                                    
                                </div>
                            </div>
                        </utilize:panel>

                        <div class="buttons">
                            <utilize:translatebutton runat='server' ID="button_remove_filter" CssClass=" btn-u btn-u-sea-shop btn-u-lg margin-right" text="Filter opheffen" Visible="false" />
                            <utilize:translatebutton runat='server' ID="button_filter" CssClass="btn-u btn-u-sea-shop btn-u-lg button-width" Text="Filter" />
                        </div>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_search_button">
                        <div class="buttons">
                             <utilize:translatebutton runat='server' ID="button_search" CssClass="btn-u btn-u-sea-shop btn-u-lg button-width" Text="Zoeken" />
                        </div>
                    </utilize:placeholder>
                    <div class="overview">
                    <utilize:pagedrepeater runat="server" ID="rpt_orders" pager_enabled="true" page_size="20">
                        <HeaderTemplate>
                            <div class="table-responsive">
                                <table class="overview table table-striped">
                                    <tbody>
                                        <tr>
                                            <th class="number"><utilize:translatelabel runat='server' ID="col_invoice_number" Text="Factuurnummer"></utilize:translatelabel></th>
                                            <th class="date"><utilize:translatelabel runat='server' ID="col_order_date" Text="Datum"></utilize:translatelabel></th>
                                            <th><utilize:translatelabel runat='server' ID="col_order_description" Text="Omschrijving"></utilize:translatelabel></th>
                                            <th class="pdf"></th>
                                                <utilize:placeholder runat="server" ID="ph_pdf" visible='<%#(Not Me.prices_not_visible Or Not Me.on_demand_sync) And Not Me.hide_pdf %>'><th class="pdf"><utilize:translatelabel runat='server' ID="col_invoice_pdf" Text="Kopiefactuur"></utilize:translatelabel></th></utilize:placeholder>
                                            <utilize:placeholder runat="server" ID="ph_backorders">
                                                <th class="backorder"><utilize:translatelabel runat='server' ID="col_backorder" Text="Bevat backorderregels"></utilize:translatelabel></th>
                                            </utilize:placeholder>                                                
                                                <th class="link"></th>
                                            <utilize:placeholder runat="server" ID="ph_return_request">
                                                <th class="link"></th>
                                            </utilize:placeholder>
                                        </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                                        <tr class="line">
                                            <td class="number"><utilize:linkbutton runat="server" ID="lb_order_details" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="details"><%# DataBinder.Eval(Container.DataItem, "doc_nr")%></utilize:linkbutton></td>
                                            <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                                            <td><%# DataBinder.Eval(Container.DataItem, "ord_desc")%></td>
                                            <utilize:placeholder runat="server" ID="ph_backorders">
                                                <td class="backorder"><utilize:literal runat="server" ID="lt_has_backorder_lines" Text= '<%# IIf(DataBinder.Eval(Container.DataItem, "has_backorder_lines"), global_trans.translate_label("lbl_yes", "Ja", Me.global_ws.Language), "") %>'></utilize:literal></td>
                                            </utilize:placeholder>
                                                <utilize:placeholder runat="server" ID="ph_pdf" visible='<%#(Not Me.prices_not_visible Or Not Me.on_demand_sync) And Not Me.hide_pdf %>'>
                                                    <td class="pdf">
                                                        <utilize:imagebutton runat="server" ID="button_copy_invoice" 
                                                            OnClientClick='<%# "window.open(""OpenDocument.aspx?DocType=invoice&DocNr=" + DataBinder.Eval(Container.DataItem, "doc_nr") + """);return false;" %>' 
                                                            ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' 
                                                            Visible='<%# CheckDocument(DataBinder.Eval(Container.DataItem, "doc_nr")) %>' />
                                                        <utilize:imagebutton runat="server" ID="button_copy_greyed"                                                         
                                                            ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf_gray.gif") %>' 
                                                            Visible='<%# not CheckDocument(DataBinder.Eval(Container.DataItem, "doc_nr")) %>' />
                                                    </td>
                                                </utilize:placeholder>
                                                <td class="link"><utilize:translatelinkbutton runat="server" ID="link_copy_history_order" Text="Opnieuw bestellen" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="copy"></utilize:translatelinkbutton></td>
                                            <utilize:placeholder runat="server" ID="ph_return_request">
                                                <td class="link"><utilize:translatelinkbutton runat="server" ID="link_account_rma_return_request" Text="Retour verzoek indienen" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="return"></utilize:translatelinkbutton></td>                                            
                                            </utilize:placeholder>
                                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-bottom-10">
                                <utilize:repeaterpager runat="server" ID="rp_orders"></utilize:repeaterpager>
                            </div>
                        </FooterTemplate>                                    
                    </utilize:pagedrepeater>
                    </div>
                </utilize:placeholder>
                <utilize:placeholder runat='server' ID="ph_no_orders" Visible='false'>
                    <h1><utilize:translatetitle runat='server' ID="title_no_history" Text="Geen eerder geplaatste bestellingen"></utilize:translatetitle></h1>
                    <utilize:translatetext runat='server' ID="text_no_order_history" Text="Wij hebben geen gegevens online beschikbaar van bestellingen die u eerder bij ons heeft gedaan."></utilize:translatetext>
                </utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_order_details" Visible="false">
                    <h1><utilize:translatetitle runat='server' ID="title_history_order_details" Text="Overzicht van eerder geplaatste bestelling"></utilize:translatetitle></h1>
                    <utilize:translatetext runat='server' ID="text_history_order_details" Text="Onderstaand vind u de gegevens van de bestelling die u in het verleden bij ons hebt geplaatst."></utilize:translatetext>
                
                    <div class="row margin-bottom-20"> 
                       <div class="col-md-12">
                           <div class="row">
                                <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_number" Text="Ordernummer"></utilize:translatelabel></div>
                                <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_number"></utilize:label></div>
                            </div>
                           <div class="row">
                                <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_date" Text="Datum"></utilize:translatelabel></div>
                                <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_date"></utilize:label></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_description" Text="Omschrijving"></utilize:translatelabel></div>
                                <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_description"></utilize:label></div>
                            </div>
                        </div>
                    </div>

                    <utilize:repeater runat="server" ID="rpt_order_details">
                        <HeaderTemplate>
                            <div class="table-responsive">
                            <table class="lines table table-striped">
                                <tbody>
                                <tr class="">
                                    <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                    <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                    <utilize:placeholder runat="server" ID="ph_is_backorder">
                                        <th class="backorder"><utilize:translatelabel runat='server' ID="col_is_backorder" Text="Is backorderregel"></utilize:translatelabel></th>
                                    </utilize:placeholder>
                                    <th class="quantity text-center"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                    <th class="amount text-right"><utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr class="line" >
                                    <td class="code"><a href="<%# Me.ResolveCustomUrl("~" + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")) %>"><%# DataBinder.Eval(Container.DataItem, "prod_code") %></a></td>
                                    <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                                    <utilize:placeholder runat="server" ID="ph_is_backorder">
                                        <td class="is-backorder"><utilize:literal runat="server" ID="lt_is_backorder" Text= '<%# IIf(DataBinder.Eval(Container.DataItem, "is_backorder"), global_trans.translate_label("lbl_yes", "Ja", Me.global_ws.Language), "") %>'></utilize:literal></td>
                                    </utilize:placeholder>
                                    <td class="quantity text-center"><utilize:literal runat="server" ID="lt_ord_qty"></utilize:literal></td>
                                    <td class="amount text-right"><utilize:literal runat="server" ID="lt_row_amount"></utilize:literal></td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                                </table>
                            </div>
                        </FooterTemplate>                                    
                    </utilize:repeater>

                    <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
                        <div class="row">
                            <div class="col-md-6 price-overview">
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></b>
                                    </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_subtotal_excl_vat"></utilize:literal>
                                    </div>
                                </div>
                                <utilize:placeholder runat="server" ID="ph_order_costs_excl_vat">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></b>
                                        </div>
                                        <div class="col-md-6">
                                            <utilize:literal runat="server" ID="lt_order_costs_excl_vat"></utilize:literal>
                                        </div>
                                    </div>
                                </utilize:placeholder>
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></b>
                                    </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_shipping_costs_excl_vat"></utilize:literal>
                                    </div>
                                </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></b>
                                    </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_vat_total"></utilize:literal>
                                    </div>    
                                    </div>    
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></b>
                                    </div>    
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_total_incl_vat"></utilize:literal>
                                    </div>    
                                    </div>    
                            </div>
                        </div>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_prices_vat_incl">
                        <div class="row">
                            <div class="col-md-6 price-overview">
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_subtotal_incl_vat" Text="Subtotaal incl. BTW"></utilize:translateliteral></b>
                                    </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_subtotal_incl_vat"></utilize:literal>
                                    </div>
                                </div>
                                <utilize:placeholder runat="server" ID="ph_order_costs_incl_vat">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b><utilize:translateliteral runat="server" ID="totals_order_costs_incl_vat" Text="Orderkosten incl. BTW"></utilize:translateliteral></b>
                                        </div>
                                        <div class="col-md-6">
                                            <utilize:literal runat="server" ID="lt_order_costs_incl_vat"></utilize:literal>
                                        </div>
                                    </div>
                                </utilize:placeholder>
                            <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_shipping_costs_incl_vat" Text="Verzendkosten incl. BTW"></utilize:translateliteral></b>
                                    </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_shipping_costs_incl_vat"></utilize:literal>
                                    </div>
                                        </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><utilize:translateliteral runat="server" ID="totals_total_incl_vat1" Text="Totaal incl. BTW"></utilize:translateliteral></b>
                                        </div>
                                    <div class="col-md-6">
                                        <utilize:literal runat="server" ID="lt_order_total_incl_vat1"></utilize:literal>
                                        </div>
                                        </div>
                            </div>
                        </div>
                    </utilize:placeholder>
                    <br style="clear: both;" />
                    <div class="buttons">
                        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="btn-u btn-u-sea-shop btn-u-lg account" />
                    </div>
                </utilize:placeholder>
            </utilize:placeholder>
        </utilize:panel>
        <utilize:panel runat="server" role="tabpanel" class="tab-pane" id="pnl_deliveries">
            <utilize:placeholder runat="server" ID="ph_deliveries_overview">
            <utilize:placeholder runat='server' ID="ph_deliveries">
                <h1><utilize:translatetitle runat='server' id="title_delivery_history" Text="Overzicht leveringen"></utilize:translatetitle></h1>
                <utilize:translatetext runat='server' id="text_delivery_history" Text="Onderstaand vindt u een overzicht van leveringen. Klik op het pakbonnummer om de details van de levering op te vragen."></utilize:translatetext>

                <utilize:pagedrepeater runat="server" ID="rpt_deliveries" pager_enabled="true" page_size="20">
                    <HeaderTemplate>
                        <div class="table-responsive">
                            <table class="overview table table-striped">
                                <tbody>
                                    <tr>
                                        <th class="number"><utilize:translatelabel runat='server' ID="col_deliver_number" Text="Pakbonnummer"></utilize:translatelabel></th>
                                        <th class="date"><utilize:translatelabel runat='server' ID="col_delivery_date" Text="Leverdatum"></utilize:translatelabel></th>
                                        <th><utilize:translatelabel runat='server' ID="col_delivery_description" Text="Omschrijving"></utilize:translatelabel></th>
                                        <th><utilize:translatelabel runat='server' ID="col_delivery_pdf" Text="Pakbon"></utilize:translatelabel></th>
                                        <th></th>
                                    </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                                    <tr class="line">
                                        <td class="number"><utilize:linkbutton runat="server" ID="lb_delivery_details" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="details"><%# DataBinder.Eval(Container.DataItem, "doc_nr")%></utilize:linkbutton></td>
                                        <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "ord_desc")%></td>
                                        <td class="pdf"><utilize:imagebutton style="background:none; height:21px; width:62px; margin: 0;" OnClientClick='<%# "window.open(""OpenDocument.aspx?DocType=delivery&DocNr=" + DataBinder.Eval(Container.DataItem, "doc_nr") + """);return false;" %>' ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/pdf.gif") %>' runat="server" ID="button_copy_invoice" /></td>
                                        <td class="link"><utilize:translatelinkbutton runat="server" ID="link_copy_history_delivery" Text="Opnieuw bestellen" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="copy"></utilize:translatelinkbutton></td>
                                    </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                                </tbody>
                            </table>
                        </div>
                        <div class="margin-bottom-10">
                            <utilize:repeaterpager runat="server" ID="rp_deliveries" />
                        </div>
                    </FooterTemplate>                                    
                </utilize:pagedrepeater>
            </utilize:placeholder>
            <utilize:placeholder runat='server' ID="ph_no_deliveries" Visible='false'>
                <h1><utilize:translatetitle runat='server' ID="title_no_delivery_history" Text="Geen eerdere leveringen"></utilize:translatetitle></h1>
                <utilize:translatetext runat='server' ID="text_no_delivery_history" Text="Wij hebben geen gegevens online beschikbaar leveringen die gedaan zijn."></utilize:translatetext>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_delivery_details" Visible="false">
                <h1><utilize:translatetitle runat='server' ID="title_history_delivery_details" Text="Overzicht van levering"></utilize:translatetitle></h1>
                <utilize:translatetext runat='server' ID="text_history_delivery_details" Text="Onderstaand vind u de gegevens van de levering."></utilize:translatetext>
                
                <div class="row"> 
                   <div class="col-md-12">
                       <div class="row">
                            <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_delivery_number" Text="Pakbonnummer"></utilize:translatelabel></div>
                            <div class="col-md-2"><utilize:label runat='server' ID="lbl_delivery_number"></utilize:label></div>
                        </div>
                       <div class="row">
                            <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_delivery_date" Text="Leverdatum"></utilize:translatelabel></div>
                            <div class="col-md-2"><utilize:label runat='server' ID="lbl_delivery_date"></utilize:label></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_delivery_description" Text="Omschrijving"></utilize:translatelabel></div>
                            <div class="col-md-2"><utilize:label runat='server' ID="lbl_delivery_description"></utilize:label></div>
                        </div>
                    </div>
                </div>

                <utilize:repeater runat="server" ID="rpt_delivery_details">
                    <HeaderTemplate>
                        <div class="table-responsive">
                        <table class="lines table table-striped">
                            <tbody>
                            <tr>
                                <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <th class="quantity text-center"><utilize:translatelabel runat='server' ID="col_delivery_quantity" Text="Aantal"></utilize:translatelabel></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="line">
                                <td class="code"><a href="<%# Me.ResolveCustomUrl("~" + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")) %>"><%# DataBinder.Eval(Container.DataItem, "prod_code") %></a></td>
                                <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                                <td class="quantity text-center"><utilize:literal runat="server" id="lt_ord_qty"></utilize:literal></td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                            </table>
                        </div>
                    </FooterTemplate>                                    
                </utilize:repeater>

                <div class="buttons">
                    <utilize:button runat="server" ID="button_close_delivery_details" CssClass="btn-u btn-u-sea-shop btn-u-lg account" />
                </div>
            </utilize:placeholder>
        </utilize:placeholder>
        </utilize:panel>
    </div>

    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="btn-u btn-u-sea-shop btn-u-lg account" />
    </div>
</div>
