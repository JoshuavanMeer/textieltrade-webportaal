﻿Imports Utilize.Data
Imports System.Data

Partial Class uc_change_address_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _register_type As Integer = 0

#Region " Public address functions"
    ''' <summary>
    ''' This function checks wether all mandatory information is filled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function check_address() As Boolean
        Dim ll_testing As Boolean = True

        Me.message_error.Visible = False
        Me.message_error.Text = ""

        If ll_testing Then
            ' Controleer eerst alle verplichte velden

            ' De bedrijfsnaam is afhankelijk van de webshop instellingen
            Select Case _register_type
                Case 2
                    ' Niet verplicht
                Case 3
                    ' Niet weergeven
                Case Else
                    ' Verplicht dus controleren of deze is gevuld
                    ll_testing = Not String.IsNullOrEmpty(Me.txt_address_bus_name.Text.Trim)
            End Select

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_fst_name.Text.Trim)
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_lst_name.Text.Trim)
            End If

            ' Haal de gewenste adres invoer op
            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

            ' Controleer de juiste gegevens
            Select Case ln_address_layout
                Case 0
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_address.Text.Trim)
                    End If
                Case 1
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_street1.Text.Trim)
                    End If

                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_housenr1.Text.Trim)
                    End If
                Case 2
                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_street2.Text.Trim)
                    End If

                    If ll_testing Then
                        ll_testing = Not String.IsNullOrEmpty(Me.txt_address_housenr2.Text.Trim)
                    End If
                Case Else
                    Exit Select
            End Select

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_zip_code.Text.Trim)
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.txt_address_city.Text.Trim)
            End If

            If ll_testing Then
                ll_testing = Not String.IsNullOrEmpty(Me.cbo_address_country.SelectedValue.Trim)
            End If

            If Not ll_testing Then
                Me.message_error.Visible = True
                Me.message_error.Text = global_trans.translate_message("message_mandatory_fields_not_filled", "Een of meer verplichte velden zijn niet gevuld.", Me.global_ws.Language)
            End If
        End If

        Return ll_testing
    End Function
    ''' <summary>
    ''' This function saves the address information in the business object but does not update the table.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function save_address() As Boolean
        Dim ll_testing As Boolean = True

        ll_testing = Me.save_customer_delivery_address()

        Return ll_testing
    End Function

    Public Sub clear_fields()
        Me.txt_rec_id_field.Value = ""
        Me.txt_address_bus_name.Text = ""
        Me.txt_address_fst_name.Text = ""
        Me.txt_address_infix.Text = ""
        Me.txt_address_lst_name.Text = ""
        Me.txt_address_address.Text = ""
        Me.txt_address_zip_code.Text = ""
        Me.txt_address_city.Text = ""
        Me.cbo_address_country.SelectedValue = ""
    End Sub
#End Region

#Region "Private address functions"
    Private Function get_countries() As Utilize.Data.DataTable
        ' Haal de landen op
        Dim udt_data_table As New Utilize.Data.DataTable
        udt_data_table.table_name = "uws_countries"
        udt_data_table.table_init()
        udt_data_table.select_fields = "cnt_code, cnt_desc_" + Me.global_ws.Language + " as cnt_desc"
        udt_data_table.add_where("and cnt_show = @cnt_show")
        udt_data_table.add_where_parameter("cnt_show", True)
        udt_data_table.table_load()

        ' Maak een lege regel aan
        Dim dr_data_row As System.Data.DataRow = udt_data_table.data_table.NewRow()
        dr_data_row.Item("cnt_code") = ""
        dr_data_row.Item("cnt_desc") = global_trans.translate_label("label_country_not_selected", "Niet geselecteerd", Me.global_ws.Language)
        udt_data_table.data_table.Rows.InsertAt(dr_data_row, 0)

        Return udt_data_table
    End Function

    Private Function save_customer_delivery_address() As Boolean
        Dim ubo_customer_address As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

        Dim ll_testing As Boolean = True

        If ll_testing Then
            ll_testing = ubo_customer_address.data_tables("uws_customers").data_row_count = 1

            If Not ll_testing Then

            End If
        End If

        If ll_testing Then
            If String.IsNullOrEmpty(Me.txt_rec_id_field.Value.Trim) Then
                ubo_customer_address.table_insert_row("uws_customers_addr", Me.global_ws.user_information.user_id)
            Else
                ll_testing = ubo_customer_address.table_locate("uws_customers_addr", "rec_id = '" + Me.txt_rec_id_field.Value + "'")

                If Not ll_testing Then
                    ' Dan is de regel niet gevonden, mag eigenlijk niet....
                End If
            End If
        End If

        If ll_testing Then
            ' Sla hier de gegevens van het adresboek op
            ubo_customer_address.field_set("uws_customers_addr.del_bus_name", Me.txt_address_bus_name.Text)
            ubo_customer_address.field_set("uws_customers_addr.del_fst_name", Me.txt_address_fst_name.Text)
            ubo_customer_address.field_set("uws_customers_addr.del_infix", Me.txt_address_infix.Text)
            ubo_customer_address.field_set("uws_customers_addr.del_lst_name", Me.txt_address_lst_name.Text)


            ' Toon op basis van de iso landcode de gewenste adresinvoer.
            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

            Select Case ln_address_layout
                Case 0
                    ubo_customer_address.field_set("uws_customers_addr.del_address", Me.txt_address_address.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_street", "")
                    ubo_customer_address.field_set("uws_customers_addr.del_house_nr", "")
                    ubo_customer_address.field_set("uws_customers_addr.del_house_add", "")
                Case 1
                    ubo_customer_address.field_set("uws_customers_addr.del_address", "")
                    ubo_customer_address.field_set("uws_customers_addr.del_street", Me.txt_address_street1.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr1.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd1.Text)
                Case 2
                    ubo_customer_address.field_set("uws_customers_addr.del_address", Me.txt_address_address.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_street", Me.txt_address_street2.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_house_nr", Me.txt_address_housenr2.Text)
                    ubo_customer_address.field_set("uws_customers_addr.del_house_add", Me.txt_address_houseadd2.Text)
                Case Else
                    Exit Select
            End Select

            ubo_customer_address.field_set("uws_customers_addr.del_zip_code", Me.txt_address_zip_code.Text.ToUpper())
            ubo_customer_address.field_set("uws_customers_addr.del_city", Me.txt_address_city.Text)
            ubo_customer_address.field_set("uws_customers_addr.del_cnt_code", Me.cbo_address_country.SelectedValue)

            ll_testing = ubo_customer_address.table_update(Me.global_ws.user_information.user_id)
        End If

        Return ll_testing
    End Function
#End Region

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_change_address_block.css", Me)

        If Not Me.IsPostBack() Then
            Me.cbo_address_country.DataTextField = "cnt_desc"
            Me.cbo_address_country.DataValueField = "cnt_code"
            Me.cbo_address_country.DataSource = Me.get_countries().data_table
            Me.cbo_address_country.DataBind()
        End If

        ' Haal de manier van omgaan met bedrijfsnaam op
        Me._register_type = Me.global_ws.get_webshop_setting("register_type")

        Select Case _register_type
            Case 2
                ' Bedrijfsnaam beschikbaar maar niet verplicht
                Me.ph_bus_name.Visible = True
                Me.ph_bus_name_mandatory.Visible = False
            Case 3
                ' Bedrijfsnaam verbergen
                Me.ph_bus_name.Visible = False
                Me.ph_bus_name_mandatory.Visible = False
            Case Else
                ' Bedrijfsnaam verplicht
                Me.ph_bus_name.Visible = True
                Me.ph_bus_name_mandatory.Visible = True
        End Select
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Wanneer het om een afleveradres gaat, dan moeten de adresvelden alleen beschikbaar zijn wanneer de checkbox is aangevinkt.
        Me.ph_address_information.Visible = True

        ' Toon op basis van de iso landcode de gewenste adresinvoer.
        Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", Me.cbo_address_country.SelectedValue)

        Me.ph_address_0.Visible = False
        Me.ph_address_1.Visible = False
        Me.ph_address_2.Visible = False

        Select Case ln_address_layout
            Case 0
                Me.ph_address_0.Visible = True
            Case 1
                Me.ph_address_1.Visible = True
            Case 2
                Me.ph_address_2.Visible = True
            Case Else
                Exit Select
        End Select
    End Sub

    ''' <summary>
    ''' This function will fill all the fields associated with this control
    ''' with address data obtained from the looked up customer address 
    ''' </summary>
    ''' <param name="lc_address_id">The address ID that will be used to look up the address in the UWS_CUSTOMERS_ADDR table</param>
    ''' <remarks></remarks>
    Public Sub SetDisplayedCustomerAddress(ByVal lc_address_id As String)
        txt_rec_id_field.Value = lc_address_id

        Dim udt_customer_addresses As New Utilize.Data.DataTable()
        udt_customer_addresses.table_name = "uws_customers_addr"
        udt_customer_addresses.add_where("AND rec_id = @rec_id")
        udt_customer_addresses.add_where_parameter("rec_id", lc_address_id)
        udt_customer_addresses.table_init()
        udt_customer_addresses.table_load()

        If udt_customer_addresses.data_row_count > 0 Then
            Dim dt_customer_address As System.Data.DataTable = udt_customer_addresses.data_table

            For Each dr_address As DataRow In dt_customer_address.Rows
                Me.cbo_address_country.Text = dr_address("del_cnt_code")
                Me.txt_address_bus_name.Text = dr_address("del_bus_name")
                Me.txt_address_fst_name.Text = dr_address("del_fst_name")
                Me.txt_address_infix.Text = dr_address("del_infix")
                Me.txt_address_lst_name.Text = dr_address("del_lst_name")
                Me.txt_address_address.Text = dr_address("del_address")

                Me.txt_address_street1.Text = dr_address("del_street")
                Me.txt_address_housenr1.Text = dr_address("del_house_nr")
                Me.txt_address_houseadd1.Text = dr_address("del_house_add")

                Me.txt_address_street2.Text = dr_address("del_street")
                Me.txt_address_housenr2.Text = dr_address("del_house_nr")
                Me.txt_address_houseadd2.Text = dr_address("del_house_add")

                Me.txt_address_zip_code.Text = dr_address("del_zip_code")
                Me.txt_address_city.Text = dr_address("del_city")

            Next
        End If
    End Sub
End Class
