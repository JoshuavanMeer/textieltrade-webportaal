﻿
Partial Class uc_sales_customers
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Function get_data_source(Optional ByVal remove_filter As Boolean = False) As System.Data.DataTable
        Dim qsc_customers As New Utilize.Data.QuerySelectClass
        qsc_customers.select_fields = "*"
        qsc_customers.select_from = "uws_customers"

        qsc_customers.select_order = "av_nr"

        ' We willen de huidige ingelogde gebruiker/klant niet in het overzicht zien
        qsc_customers.select_where += " not rec_id = @rec_id"

        qsc_customers.select_where += " and blocked = 0"

        ' Zet eventueel een filter op medewerker wanneer deze is ingevuld
        If Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.emp_nr") = "" Then
            qsc_customers.select_where += " and emp_nr = @emp_nr "
            qsc_customers.add_parameter("emp_nr", Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.emp_nr"))
        End If

        qsc_customers.add_parameter("rec_id", Me.global_ws.user_information.user_customer_id)

        If Me.IsPostBack() And Not remove_filter Then
            Me.button_filter_remove.Visible = False

            '''''''''''''''''''''''''''''''''''''''''''
            ' Regel hieronder de zoeken functionaliteit
            '''''''''''''''''''''''''''''''''''''''''''
            If Not String.IsNullOrEmpty(Request.Form(Me.txt_av_nr.UniqueID)) Then
                ' Als een ordernummer is ingevuld
                qsc_customers.select_where += " and av_nr like @av_nr + '%'"
                qsc_customers.add_parameter("av_nr", Request.Form(Me.txt_av_nr.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_bus_name.UniqueID)) Then
                ' Als een klant referentie is ingevuld
                qsc_customers.select_where += " and bus_name like '%' + @bus_name + '%'"
                qsc_customers.add_parameter("bus_name", Request.Form(Me.txt_bus_name.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_zip_code.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_customers.select_where += " and zip_code like @zip_code + '%'"
                qsc_customers.add_parameter("zip_code", Request.Form(Me.txt_zip_code.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If

            If Not String.IsNullOrEmpty(Request.Form(Me.txt_city.UniqueID)) Then
                ' Als een productcode is ingevuld
                qsc_customers.select_where += " and city like @city + '%'"
                qsc_customers.add_parameter("city", Request.Form(Me.txt_city.UniqueID).Trim())

                Me.button_filter_remove.Visible = True
            End If
        End If

        Return qsc_customers.execute_query()
    End Function

    Private uc_invoice_address_block As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_delivery_address_block As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_customers_overview.css", Me)
        Me.block_css = True

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        ' Bouw de lijst met klanten op
        rpt_customers.EnableViewState = False
        rpt_customers.DataSource = Me.get_data_source()
        rpt_customers.DataBind()

        If Me.IsPostBack Then
            rpt_customers.page_nr = Request.Form(rpt_customers.hih_page_nr.UniqueID)
            rpt_customers.DataBind()
        End If

        ' Voeg hier de control toe voor het toevoegen van een nieuwe klant
        ' Factuuradres
        uc_invoice_address_block = LoadUserControl("~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx")
        uc_invoice_address_block.set_property("address_type", 1)
        uc_invoice_address_block.set_property("sales_module", True)
        uc_invoice_address_block.set_property("use_login", True)

        Me.ph_new_customer_invoice_address.Controls.Add(uc_invoice_address_block)

        ' Afleveradres
        uc_delivery_address_block = LoadUserControl("~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx")
        uc_delivery_address_block.set_property("address_type", 2)
        uc_delivery_address_block.set_property("sales_module", True)

        Me.ph_new_customer_delivery_address.Controls.Add(uc_delivery_address_block)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Me.ph_customers.Visible = Me.rpt_customers.Items.Count > 0
            Me.ph_no_customers.Visible = Me.rpt_customers.Items.Count = 0
        End If
    End Sub

    Protected Sub rpt_customers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_customers.ItemCommand
        If e.CommandName = "login" Then
            Dim lc_cust_id As String = CType(e.Item.FindControl("lt_rec_id"), literal).Text

            Me.login(lc_cust_id)
        End If
    End Sub

    Protected Sub button_filter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter.Click
        ' Als het zoekpanel zichtbaar is, dan moet de zoekopdracht uitgevoerd worden
        If Me.ph_search_customers.Visible Then
            Me.rpt_customers.page_nr = 0

            Me.rpt_customers.DataSource = Me.get_data_source()
            Me.rpt_customers.DataBind()

            Me.txt_av_nr.Focus()
        End If

        Me.button_filter_remove.Visible = True
    End Sub
    Protected Sub button_filter_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_filter_remove.Click
        Me.txt_av_nr.Text = ""
        Me.txt_bus_name.Text = ""
        Me.txt_city.Text = ""
        Me.txt_zip_code.Text = ""

        Me.rpt_customers.DataSource = get_data_source(True)
        Me.rpt_customers.page_nr = 0
        Me.rpt_customers.DataBind()

        Me.button_filter.Visible = True
        Me.button_filter_remove.Visible = False

        Me.txt_av_nr.Focus()
    End Sub

    Protected Sub button_search_Click(sender As Object, e As EventArgs) Handles button_search.Click
        ' Zowieso moet het panel zichtbaar zijn als er gezocht gaat worden
        Me.ph_search_customers.Visible = True
        Me.button_search.Visible = False

        Me.txt_av_nr.Focus()
    End Sub

#Region " Klant toevoegen buttons"
    Protected Sub button_add_customer_Click(sender As Object, e As EventArgs) Handles button_add_customer.Click
        Me.pnl_add_customer.Visible = True
    End Sub
    Protected Sub button_cancel_Click(sender As Object, e As EventArgs) Handles button_cancel.Click
        Me.pnl_add_customer.Visible = False
    End Sub
    Protected Sub button_save_Click(sender As Object, e As EventArgs) Handles button_save.Click
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ll_testing = uc_invoice_address_block.execute_function("check_address")
        End If

        If ll_testing Then
            ll_testing = uc_delivery_address_block.execute_function("check_address")
        End If

        If ll_testing Then
            Dim ubo_customer As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", "", "rec_id")
            ubo_customer.table_insert_row("uws_customers", Me.global_ws.user_information.user_id)
            ubo_customer.table_insert_row("uws_customers_users", Me.global_ws.user_information.user_id)

            If ll_testing Then
                ' Zet de medewerkers code
                Dim lc_emp_nr As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.emp_nr")
                ubo_customer.field_set("uws_customers.emp_nr", lc_emp_nr)
            End If

            If ll_testing Then
                uc_invoice_address_block.set_property("uboCustomer", ubo_customer)
                ll_testing = uc_invoice_address_block.execute_function("save_address")
            End If

            If ll_testing Then
                uc_delivery_address_block.set_property("uboCustomer", ubo_customer)
                ll_testing = uc_delivery_address_block.execute_function("save_address")
            End If

            If ll_testing Then
                ' Sla de klant op in de database
                ll_testing = ubo_customer.table_update(Me.global_ws.user_information.user_id)
            End If

            If ll_testing Then
                ' Fetch saved password and sent email to user

                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sales_module") = True And Me.global_cms.get_cms_setting("eml_from_address") = Me.global_ws.user_information.ubo_customer.field_get("email_address") Then
                    ' Sent email to customer user
                    Dim ws_mail_password As New ws_email_password
                    ws_mail_password.email_address = ubo_customer.field_get("uws_customers_users.email_address")

                    ws_mail_password.password = uc_invoice_address_block.get_property("customer_user_password")
                    ws_mail_password.language = Me.global_ws.Language

                    ws_mail_password.send_email()

                End If
            End If

            If ll_testing Then
                Try
                    Dim WsAvConn As New WSAvConn.WSAvConn With {
                            .Url = FrameWork.FrameworkProcedures.ReadConfiguration("WSAvConn.WSAvConn")
                        }
                    WsAvConn.GetWebshopInformation()
                Catch ex As Exception

                End Try
            End If

            If ll_testing Then
                Dim lc_cust_id As String = ubo_customer.field_get("uws_customers.rec_id")

                Me.login(lc_cust_id)
            End If

            If Not ll_testing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "login_message", "alert('" + ubo_customer.error_message.Replace("'", "\'") + "');", True)
            End If
        End If
    End Sub
#End Region

#Region " Inloggen namens een klant"
    Private Function login(ByVal cust_id As String) As Boolean
        ' We loggen altijd in onder het emailadres wat in de website instellingen is vasttgelegd.
        Dim lc_email_address As String = global_cms.get_cms_setting("eml_from_address").ToString().Trim()
        Dim lc_login_code As String = Guid.NewGuid().ToString().Substring(0, 10)

        ' Sla hieronder een aantal gegevens op
        Dim lc_cust_id As String = cust_id
        Dim lc_password As String = Guid.NewGuid().ToString()
        Dim lc_rec_id As String = ""

        Dim ll_resume As Boolean = True

        ' Declareer het business objects
        Dim ubo_customer As Utilize.Data.BusinessObject = Nothing

        If ll_resume Then
            ' Zet hier de id van de oorspronkelijk ingelogde gebruiker
            Me.global_ws.user_information.user_id_original = Me.global_ws.user_information.user_id
        End If

        If ll_resume Then
            ' Als de logincode niet gevonden is, dan maken we een nieuwe webshop gebruiker aan
            ubo_customer = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", lc_cust_id, "rec_id")
            ubo_customer.table_insert_row("uws_customers_users", Me.global_ws.user_information.user_id)

            ' Sla de sleutelwaarde van de nieuw aangemaakte gebruiker op
            lc_rec_id = ubo_customer.field_get("uws_customers_users.rec_id")

            ' Zet de velden
            ubo_customer.field_set("uws_customers_users.fst_name", Me.global_ws.user_information.user_fst_name)
            ubo_customer.field_set("uws_customers_users.infix", Me.global_ws.user_information.user_infix)
            ubo_customer.field_set("uws_customers_users.lst_name", Me.global_ws.user_information.user_lst_name)

            ubo_customer.field_set("uws_customers_users.email_address", lc_email_address)
            ubo_customer.field_set("uws_customers_users.login_code", lc_login_code)
            ubo_customer.field_set("uws_customers_users.login_password", lc_password)
            ubo_customer.field_set("uws_customers_users.type_code", 1)

            ' Sla de gebruiker op
            ll_resume = ubo_customer.table_update(Me.global_ws.user_information.user_id)

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "login_message", "alert('" + ubo_customer.error_message.Replace("'", "\'") + "');", True)
            End If
        End If

        If ll_resume Then
            ' Haal het wachtwoord van de gebruiker op
            lc_login_code = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_code", lc_login_code + lc_cust_id, "login_code + hdr_id")

            ' Login onder de juiste gebruikers gegevens
            ll_resume = Me.global_ws.user_information.login(1, lc_login_code, lc_password)

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "login_message", "alert('" + Me.global_ws.user_information.error_message.Replace("'", "\'") + "');", True)
            End If
        End If

        If ll_resume Then
            ' We verwijderen de gebruiker meteen weer want we willen niet allemaal zwevende gebruikers hebben
            ubo_customer.move_first("uws_customers_users")

            If ubo_customer.table_locate("uws_customers_users", "rec_id = '" + lc_rec_id + "'") Then
                ubo_customer.table_delete_row("uws_customers_users", False, Me.global_ws.user_information.user_customer_id)
                ubo_customer.table_update(Me.global_ws.user_information.user_id)
            End If
        End If

        If ll_resume Then
            ' En maak een nieuwe winkelwagen aan
            Me.global_ws.shopping_cart.create_shopping_cart()

            Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
        End If

        Return ll_resume
    End Function
#End Region
End Class
