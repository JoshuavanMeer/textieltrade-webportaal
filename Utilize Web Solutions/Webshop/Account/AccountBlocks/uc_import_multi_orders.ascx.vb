﻿Imports System.Data.OleDb

Partial Class Bespoke_Account_AccountBlocks_uc_multi_orders
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Public Function get_base_url() As String
        Return System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath
    End Function

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if multi order import module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book") And Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_MULTIORDER") Then
            Response.Redirect("~/home.aspx", True)
        End If

        Me.set_style_sheet("uc_import_multi_orders.css", Me)
        Me.set_javascript("uc_import_multi_orders.js", Me)

        Me.set_style_sheet("~/RMA/Styles/dropzone.css", Me)
        Me.set_javascript("~/RMA/Scripts/dropzone.js", Me)

        ' Voeg hier de postbacktriggers toe
        Dim loUploadTrigger As New System.Web.UI.PostBackTrigger
        loUploadTrigger.ControlID = button_export_template.UniqueID

        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional
        CType(Me.Page.Master.FindControl("upd_update_panel"), System.Web.UI.UpdatePanel).Triggers.Add(loUploadTrigger)

        System.Web.UI.ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(button_export_template)
    End Sub


    Private _SessionId As String = Nothing
    Private Sub Bespoke_Account_AccountBlocks_uc_multi_orders_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack() Then
            ViewState("ImportSessionId") = Guid.NewGuid().ToString()
        End If

        _SessionId = ViewState("ImportSessionId")

        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "do_init", "$( document ).ready(function() {dropzone_init('" + Me.dropzoneForm.ClientID + "','" + Me.button_import.ClientID + "', '" + Me.get_base_url() + "/webshop/account/MultiOrderUploadFile.ashx?a=" + Me.global_ws.user_information.user_customer_id + "&b=" + Me.global_ws.user_information.user_id + "&c=" + _SessionId + "&d=" + Me.global_ws.Language + " ')});", True)
    End Sub

    ''' <summary>
    ''' Handles exported csv file of customer customers
    ''' </summary>
    Protected Sub button_export_template_click() Handles button_export_template.Click
        Dim stringbuilder_csv As New StringBuilder
        stringbuilder_csv.AppendLine("Productcode;Productdescription;Quantity;Ordernumber;Email;Name;Address;Postalcode;City;Countrycode;Phonenumber;")

        ' Fetch all addresses
        Dim qsc_customer_details As New Utilize.Data.QuerySelectClass
        qsc_customer_details.select_fields = "*"
        qsc_customer_details.select_from = "uws_customers"
        qsc_customer_details.select_where = "rec_id = @rec_id and blocked = 0"
        qsc_customer_details.add_parameter("@rec_id", Me.global_ws.user_information.user_customer_id)

        Dim qsc_customer_users_details As New Utilize.Data.QuerySelectClass
        qsc_customer_users_details.select_fields = "*"
        qsc_customer_users_details.select_from = "uws_customers_users"
        qsc_customer_users_details.select_where = "hdr_id = @hdr_id and blocked = 0"
        qsc_customer_users_details.add_parameter("@hdr_id", Me.global_ws.user_information.user_customer_id)

        Dim qsc_customer_addresses As New Utilize.Data.QuerySelectClass
        qsc_customer_addresses.select_fields = "*"
        qsc_customer_addresses.select_from = "uws_customers_addr"
        qsc_customer_addresses.select_where = "cust_id = @cust_id"
        qsc_customer_addresses.add_parameter("@cust_id", Me.global_ws.user_information.user_customer_id)

        Dim dt_customer_details As System.Data.DataTable = qsc_customer_details.execute_query()
        Dim dt_customer_users_details As System.Data.DataTable = qsc_customer_users_details.execute_query()
        Dim dt_customer_addresses As System.Data.DataTable = qsc_customer_addresses.execute_query()

        'Fill csv with details if any
        If dt_customer_details.Rows.Count > 0 Then
            For Each row In dt_customer_details.Rows
                Dim name As String = row.item("bus_name") + " - " + row.item("fst_name") + " " + row.item("infix") + " " + row.item("lst_name")
                Dim adress As String = row.item("address") + " " + row.item("house_nr")
                name = name.Trim()
                adress = adress.Trim()
                If name.EndsWith("-") Then
                    name = name.Substring(0, name.Length - 2)
                ElseIf name.StartsWith("-") Then
                    name = name.Substring(2)
                End If

                stringbuilder_csv.AppendLine("; ; ; ; " + row.item("email_address") + "; " + name + "; " + adress + "; " + row.item("zip_code") + ";" + row.item("city") + "; " + row.item("cnt_code") + "; " + row.item("phone_nr") + ";")
            Next

            If dt_customer_users_details.Rows.Count > 0 Then

                For Each row In dt_customer_users_details.Rows
                    Dim name As String = row.item("fst_name") + " " + row.item("infix") + " " + row.item("lst_name")
                    Dim adress As String = row.item("address")
                    name = name.Replace("  ", " ").Trim()
                    adress = adress.Trim()

                    stringbuilder_csv.AppendLine("; ; ; ; " + row.item("email_address") + "; " + name + "; " + adress + "; " + row.item("zip_code") + ";" + row.item("city") + "; ; ;")
                Next
            End If

            If dt_customer_addresses.Rows.Count > 0 Then
                For Each row In dt_customer_addresses.Rows
                    Dim name As String = row.item("del_bus_name") + " - " + row.item("del_fst_name") + " " + row.item("del_infix") + " " + row.item("del_lst_name")
                    Dim adress As String = row.item("del_address") + " " + row.item("del_house_nr")
                    name = name.Trim()
                    adress = adress.Trim()
                    If name.EndsWith("-") Then
                        name = name.Substring(0, name.Length - 2)
                    ElseIf name.StartsWith("-") Then
                        name = name.Substring(2)
                    End If
                    stringbuilder_csv.AppendLine("; ; ; ; ; " + name + "; " + adress + "; " + row.item("del_zip_code") + ";" + row.item("del_city") + "; " + row.item("del_cnt_code") + "; ;")
                Next
            End If
        End If

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.Write(stringbuilder_csv.ToString)
        Response.AddHeader("Content-Disposition", "attachment;filename=template_multiorder.csv")
        Response.End()
    End Sub

    Protected Sub button_refresh_Click(sender As Object, e As EventArgs) Handles button_refresh.Click
        Dim loImportObject As New ImportObject(_SessionId)

        Dim ln_row_succesful As Integer = loImportObject.SuccessCount
        Dim ln_row_failed As Integer = loImportObject.ErrorCount
        Dim lc_error_string As String = loImportObject.ErrorText

        ' Toon de juiste resultaten
        Me.ph_result.Visible = True
        Me.pnl_success.Visible = True
        Me.pnl_error.Visible = ln_row_failed > 0 Or lc_error_string <> ""

        ' Titels en teksten goedzetten me juiste informatie
        Me.lt_title_import_succesful.Text = global_trans.translate_title("title_import_multi_order_succesful", "[1] orders succesvol ingelezen.", Me.global_ws.Language)
        Me.lt_title_import_succesful.Text = Me.lt_title_import_succesful.Text.Replace("[1]", ln_row_succesful.ToString())

        Me.lt_title_import_unsuccesful.Text = global_trans.translate_text("title_import_multi_order_unsuccesful", "[1] orders niet succesvol ingelezen.", Me.global_ws.Language)
        Me.lt_title_import_unsuccesful.Text = Me.lt_title_import_unsuccesful.Text.Replace("[1]", ln_row_failed.ToString())

        Me.lt_text_import_succesful.Text = global_trans.translate_text("text_import_multi_order_succesful", "Wij hebben [1] orders succesvol ingelezen.", Me.global_ws.Language)
        Me.lt_text_import_succesful.Text = Me.lt_text_import_succesful.Text.Replace("[1]", ln_row_succesful.ToString())

        Me.lt_text_import_unsuccesful.Text = global_trans.translate_text("text_import_multi_order_unsuccesful", "Wij hebben [1] orders niet succesvol ingelezen.", Me.global_ws.Language)
        Me.lt_text_import_unsuccesful.Text = Me.lt_text_import_unsuccesful.Text.Replace("[1]", ln_row_failed.ToString())

        Me.lt_text_import_errors.Text = lc_error_string

        Me.button_import.Visible = True
    End Sub

    Private Sub button_import_Click(sender As Object, e As EventArgs) Handles button_import.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.ph_multi_orders.Visible = False
            Me.SetImportObject()
            Me.SetProgressFrame()
        End If

    End Sub

    Private Sub SetImportObject()
    End Sub

    Private Sub SetProgressFrame()
        ' Maak het progress IFrame aan
        Dim hgc_iframe As New HtmlGenericControl("iframe")
        hgc_iframe.Style.Add("width", "100%")
        hgc_iframe.Style.Add("height", "100px")
        hgc_iframe.Attributes.Add("frameborder", "0")
        hgc_iframe.Attributes.Add("src", Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/account/importmultiorderprogress.aspx?SessionId=" + _SessionId + "&RefreshId=" + Me.button_refresh.ClientID)

        Me.ph_progress.Controls.Add(hgc_iframe)
    End Sub

End Class



