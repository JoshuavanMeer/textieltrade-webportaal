﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_report.ascx.vb" Inherits="uc_order_report" %>

<utilize:repeater runat="server" id="rpt_orders">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
        <h1><utilize:translateliteral runat='server' ID="label_order_number" Text="Ordernummer"></utilize:translateliteral>: <utilize:literal runat='server' ID="lbl_order_number" text='<%# DataBinder.Eval(Container.DataItem, "ord_nr")%>'></utilize:literal></h1>
        <table class="details" cellspacing="0" cellpadding="0">
            <tr>
                <td class="text"><utilize:translatelabel runat='server' ID="label_order_date" Text="Orderdatum"></utilize:translatelabel></td>
                <td class="value"><utilize:label runat='server' ID="lbl_order_date" text='<%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%>'></utilize:label></td>
            </tr>
            <tr>
                <td class="text"><utilize:translatelabel runat='server' ID="label_order_description" Text="Omschrijving"></utilize:translatelabel></td>
                <td class="value"><utilize:label runat='server' ID="lbl_order_description" text='<%# DataBinder.Eval(Container.DataItem, "order_description") %>'></utilize:label></td>
            </tr>
        </table>
        <br />
                <utilize:repeater runat="server" ID="rpt_order_details">
                    <HeaderTemplate>
                        <table class="lines" cellspacing="0" cellpadding="0">
                            <tr class="header">
                                <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" ID="ph_size" Visible="false" >
                                    <th class="quantity"><utilize:translatelabel runat='server' ID="col_size" Text="Maat"></utilize:translatelabel></th>
                                </utilize:placeholder>
                                <th class="quantity"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <th class="amount"><utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="line">
                                <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                                <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                                <utilize:placeholder runat="server" ID="ph_size" Visible="false">
                                    <td class="quantity"><%# DataBinder.Eval(Container.DataItem, "size")%></td>
                                </utilize:placeholder>
                                <td class="quantity"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0) %></td>
                                <td class="amount"><utilize:literal runat="server" ID="lt_row_amount"></utilize:literal></td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </table>
                    </FooterTemplate>                                    
                </utilize:repeater>
                <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
                    <table class="totals">
                        <tr class="subtotal_excl_vat">
                            <td><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></td>
                            <td class="amount"><utilize:literal runat="server" ID="lt_order_subtotal_excl_vat"></utilize:literal></td>
                        </tr>
                        <tr class="total_order_cost_excl_vat">
                            <td><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></td>
                            <td class="amount"><utilize:literal runat="server" ID="lt_order_costs_excl_vat"></utilize:literal></td>
                        </tr>
                        <tr class="total_shipping_cost_excl_vat">
                            <td><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></td>
                            <td class="amount"><utilize:literal runat="server" ID="lt_order_shipping_costs_excl_vat"></utilize:literal></td>
                        </tr>
                        <tr class="total_vat_amt">
                            <td><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></td>
                            <td class="amount"><utilize:literal runat="server" ID="lt_order_vat_total"></utilize:literal></td>
                        </tr>
                        <tr class="total_amount_incl_vat">
                            <td><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></td>
                            <td class="amount"><utilize:literal runat="server" ID="lt_order_total_incl_vat"></utilize:literal></td>
                        </tr>            
                    </table>
                    <br />
                </utilize:placeholder>
                    <br style="clear: both;" />
    </ItemTemplate>
    <FooterTemplate>
    </FooterTemplate>
</utilize:repeater>