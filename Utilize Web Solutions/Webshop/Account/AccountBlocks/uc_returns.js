﻿function set_caret_links() {
    $(".image-caret").each(function () {
        $(this).attr("data-target", "#" + $(this).parent().parent().next().find(".collapse").attr("id"));
    });
}

function show_upload() {
    if ($("#div_upload_image").attr("class") == "hidden") {
        $("#div_upload_image").attr("class", "");
    } else {
        $("#div_upload_image").attr("class", "hidden");
    }
}

function set_change(id) {
    $('#' + id).change(function () {
        show_upload();
    });
}

function set_status_width() {
    if ($(".label.rounded").length) {
        var widest = 0;
        $(".label.rounded").each(function () {
            var current_width = $(this).css("width");
            if (current_width.substring(0, current_width.length - 2) > widest) {
                widest = parseInt(current_width.substring(0, current_width.length - 2));
                widest += parseInt($(this).css("padding-left").substring(0, $(this).css("padding-left").length - 2));
                widest += parseInt($(this).css("padding-right").substring(0, $(this).css("padding-right").length - 2));
                widest = widest.toString() + "px";
            }
        });

        $(".label.rounded").each(function () {
            var current_width = $(this).css("width");
            if (current_width.substring(0, current_width.length - 2) != widest.substring(0, widest.length - 2)) {
                var difference = widest.substring(0, widest.length - 2) - current_width.substring(0, current_width.length - 2);
                var padding = difference / 2;
                $(this).css("padding-left", padding + "px");
                $(this).css("padding-right", padding + "px");
            }
        });
    }
}