﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_lists.ascx.vb" Inherits="uc_order_lists" %>

<h1><utilize:translatetitle runat='server' ID="title_order_list" Text="Bestellijst"></utilize:translatetitle></h1>
<utilize:panel runat="server" ID="pnl_order_list" cssclass="uc_order_list">
    <asp:hiddenfield runat="server" ID="show_my_order_list_products" Value="0" />

    <div class="menu-buttons">
        <utilize:translatebutton runat='server' ID="button_show_all_products" Text="Toon alle producten"  CssClass="left account btn-u btn-u-sea-shop btn-u-lg"/>
        <utilize:translatebutton runat="server" ID="button_show_my_order_list" Text="Toon mijn bestellijst" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_search_products" Text="Zoeken" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
    
    <utilize:placeholder runat="server" ID="ph_search_products" Visible='false'>
        <div class="search-products">
            <utilize:translatetext runat='server' ID="text_products_search" Text="Vul hieronder uw criteria in en kies voor zoeken om de zoekopdracht uit te voeren"></utilize:translatetext>
            <utilize:panel runat='server' ID="pnl_search" DefaultButton="button_search" CssClass="row margin-bottom-20">
                <div class="col-md-3">
                    <utilize:translatelabel runat='server' ID="label_product_code" Text="Artikelcode"></utilize:translatelabel>
                    <utilize:textbox runat='server' ID="txt_prod_code" CssClass="form-control" ></utilize:textbox>
                </div>
                <div class="col-md-3">
                    <utilize:translatelabel runat='server' ID="label_product_description" Text="Omschrijving"></utilize:translatelabel>
                    <utilize:textbox runat='server' ID="txt_prod_desc" CssClass="form-control"></utilize:textbox>
                </div>
                <div class="col-md-3">
                    <utilize:translatelabel runat='server' ID="label_categories" Text="Categorieën"></utilize:translatelabel>
                    <utilize:dropdownlist runat="server" ID="ddl_categories" CssClass="form-control" AutoPostBack="true"></utilize:dropdownlist>
                </div>
                <asp:PlaceHolder runat="server" ID="ph_brands">
                    <div class="col-md-3">
                        <utilize:translatelabel runat='server' ID="label_brands" Text="Merken"></utilize:translatelabel>
                        <utilize:dropdownlist runat="server" ID="ddl_brands" CssClass="form-control" AutoPostBack="true"></utilize:dropdownlist>
                    </div>
                </asp:PlaceHolder>
            </utilize:panel>
            
            <div class="filter-buttons">
                <utilize:translatebutton runat='server' ID="button_search" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" Text="Zoek" />
                <utilize:translatebutton runat='server' ID="button_search_remove" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" text="Opheffen" Visible="false" />
                <br style="clear: both" />
            </div>
        </div>
    </utilize:placeholder>

    <!-- Pagesize -->
    <div class="pages pull-right">            
        <asp:HiddenField runat="server" id="hidden_item_size" value=""/>                       
        <div class="pull-right">
            <h3><utilize:translatelabel runat="server" CssClass="page_size" ID="label_items_per_page" Text="Aantal resultaten per pagina"></utilize:translatelabel></h3>
            <utilize:dropdownlist runat='server' CssClass="cbo_items_per_page btn btn-default dropdown-toggle" ID="cbo_items_per_page1" AutoPostBack="true"></utilize:dropdownlist>
        </div>
    </div>

    <utilize:placeholder runat="server" ID="ph_product_list">
        <table class="product-list table table-responsive">
            <thead>
                <tr class="header">
                    <th class="image"></th>
                    <th class="code-description-stock">
                        <utilize:translateliteral runat="server" ID="col_product_description" Text="Omschrijving"></utilize:translateliteral>
                    </th>
                    <utilize:placeholder runat='server' ID="ph_sizes_header" Visible="false">
                        <th class="sizes">
                            <utilize:translatelabel runat='server' ID="label_sizes_title" Text="Maat"></utilize:translatelabel>
                        </th>
                    </utilize:placeholder>
                    <utilize:placeholder runat='server' ID="ph_price_header" Visible="false">
                        <th class="price">
                            <utilize:translatelabel runat="server" ID="label_product_price" text="Prijs"></utilize:translatelabel>
                        </th>
                    </utilize:placeholder>
                    <th class="quantity">
                        <utilize:translateliteral runat='server' ID="label_order_quantity" Text="Aantal"></utilize:translateliteral>
                    </th>
                    <th class="extra-information">
                        <utilize:translateliteral runat='server' ID="label_extra_information" Text="Extra informatie"></utilize:translateliteral>
                    </th>
                    <th class="edit-row"></th>
                </tr>
            </thead>
            <tbody>
                <utilize:repeater runat="server" id="rpt_products" pager_enabled="true" page_size="15" pagertype="topandbottom">
                    <ItemTemplate>
                        <tr class="normal">
                            <td class="image">
                                <img src='<%# Me.get_image(DataBinder.Eval(Container.DataItem, "prod_code"))%>' runat="server" ID="img_prod_img" class="mCS_img_loaded" />
                            </td>
                            <td class="code-description-stock">
                                <utilize:literal runat="server" ID="lt_prod_desc" Text='<%# DataBinder.Eval(Container.DataItem, "prod_desc")%>'></utilize:literal></br>
                                <utilize:literal runat='server' ID="lt_prod_code" Text='<%#DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:literal>
                                <utilize:placeholder runat='server' ID="ph_stock" Visible="false"><br /></utilize:placeholder>
                            </td>
                            <utilize:placeholder runat='server' ID="ph_sizes" Visible="false">
                                <td class="sizes">
                                    <utilize:placeholder runat="server" ID="ph_sizes_ddl"></utilize:placeholder>
                                </td>
                            </utilize:placeholder> 
                            <utilize:placeholder runat='server' ID="ph_price" Visible="false">
                                <td class="price">
                                    <utilize:literal runat='server' ID="lt_prod_price"></utilize:literal>
                                </td>
                            </utilize:placeholder>
                            <td class="quantity">
                                <utilize:placeholder runat="server" ID="ph_order_product"></utilize:placeholder>
                            </td>
                            <td class="extra-information">
                                <utilize:literal runat="server" ID="lt_extra_information" Text='<%# get_extra_information(DataBinder.Eval(Container.DataItem, "prod_code"))%>'></utilize:literal></br>
                            </td>
                            <utilize:placeholder runat='server' ID="ph_delete_row" Visible="<%# Me.ShowType = 0 %>">
                                <td class="remove_row">
                                    <utilize:linkbutton runat="server" CssClass="close" ID="link_delete_row" text="x" CommandName="delete_row" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' />
                                </td>
                            </utilize:placeholder>
                            <utilize:placeholder runat='server' ID="ph_add_row" Visible="<%# Me.ShowType = 1 %>">
                                <td class="add_row">
                                    <utilize:linkbutton runat="server" CssClass="close" ID="link_add_row" text="+" CommandName="add_row" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' />
                                </td>
                            </utilize:placeholder>
                        </tr>
                    </itemTemplate>
                </utilize:repeater> 
            </tbody>
        </table>

        <utilize:label runat='server' ID="label_error_message" CssClass="message" Visible='false'></utilize:label>

        <div class="buttons">
            <utilize:translatebutton runat='server' ID="button_add_to_cart" Text="Toevoegen aan winkelwagen" CssClass="right account btn-u btn-u-sea-shop btn-u-lg pull-right" />
        </div>

        <div class="clearfix"></div>

        <div class="shopping-cart">
            <utilize:placeholder runat="server" ID="ph_shopping_cart_totals"></utilize:placeholder>
        </div>
    </utilize:placeholder>

    <!-- no items text -->
    <utilize:placeholder runat="server" ID="ph_no_products">
        <div class="no_products_found">
            <utilize:translatetext runat='server' ID="text_no_products" Text="Er zijn geen producten gevonden."></utilize:translatetext>
        </div>
    </utilize:placeholder>

    <div class="row">
        <div class="col-md-12 buttons">
            <utilize:translatebutton runat='server' ID="button_shopping_cart" Text="Winkelwagen"  CssClass="right account btn-u btn-u-sea-shop btn-u-lg pull-right" />
            <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg pull-left" />
        </div>
    </div>
</utilize:panel>

<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
        <%--Processing...--%>
    </ProgressTemplate>
</asp:UpdateProgress>