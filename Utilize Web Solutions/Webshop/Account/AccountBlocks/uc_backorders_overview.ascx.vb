﻿
Imports System.Web.UI.WebControls

Partial Class uc_backorders_overview
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Dim qsc_orders As New Utilize.Data.QuerySelectClass
        qsc_orders.select_fields = "uws_so_orders.ord_date, uws_so_orders.av_ord_nr, uws_so_order_lines.*"
        qsc_orders.select_from = "uws_so_orders"
        qsc_orders.select_order = "av_ord_nr asc"
        qsc_orders.select_where = "cust_id = @cust_id and not back_qty = 0"

        qsc_orders.add_join("left outer join", "uws_so_order_lines", "uws_so_orders.ord_nr = uws_so_order_lines.hdr_id")
        qsc_orders.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

        Me.rpt_backorders.DataSource = qsc_orders.execute_query()
        Me.rpt_backorders.DataBind()

        Me.ph_backorders.Visible = Me.rpt_backorders.Items.Count > 0
        Me.ph_no_backorders.Visible = Me.rpt_backorders.Items.Count = 0

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
        Me.set_style_sheet("uc_backorders_overview.css", Me)
        Me.block_css = False
    End Sub

    Private Sub rpt_backorders_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_backorders.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Try
                ' Decimalen
                Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

                '<%# System.Math.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0).ToString()%>
                Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")
                If lt_ord_qty IsNot Nothing Then
                    lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If

                '<%# System.Math.Round(DataBinder.Eval(Container.DataItem, "back_qty"), 0).ToString()%>
                Dim lt_back_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_back_qty")
                If lt_back_qty IsNot Nothing Then
                    lt_back_qty.Text = Format(Decimal.Round(e.Item.DataItem("back_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class
