﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_import_order_lines.ascx.vb" Inherits="Bespoke_Account_AccountBlocks_uc_import_order_lines" %>

<div class="block account uc_import_order_lines">
    <utilize:placeholder runat='server' ID="ph_concept_orders">
        <h1><utilize:translatetitle runat='server' ID="title_import_order_lines" Text="Orderregels importeren in winkelwagen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_import_order_lines" Text="Selecteer het gewenste XML bestand en kies voor importeren om de orderregels aan de winkelwagen toe te voegen."></utilize:translatetext>
    
        <div class="example-files margin-bottom-20">
            <utilize:translatelabel runat="server" ID="lbl_example_files" Text="Hieronder vind u de voorbeeldbestanden"></utilize:translatelabel>
            <ul>
                <li>
                    <utilize:translatelinkbutton runat="server" ID="lb_download_csv" Text="csv voorbeeld"></utilize:translatelinkbutton>
                </li>
                <li>
                    <utilize:translatelinkbutton runat="server" ID="lb_download_xml" Text="xml voorbeeld"></utilize:translatelinkbutton>
                </li>
            </ul>
        </div>

        <utilize:placeholder runat="server" ID="ph_select_import_format">
            <div class="row">
                <utilize:translatelabel runat="server" ID="label_select_import_type" Text="Kies het gewenste formaat" cssclass="col-md-3"></utilize:translatelabel>

                <div class="col-md-9">
                    <utilize:optiongroup runat="server" ID="opg_import_type" AutoPostBack="true">
                        <asp:ListItem Text="XML" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="CSV" Value="2"></asp:ListItem>
                    </utilize:optiongroup>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_xml" Visible="false">
            <div class="row margin-bottom-20">
                <utilize:translatelabel runat="server" ID="label_select_xml_file" Text="XML bestand:" CssClass="col-md-3"></utilize:translatelabel>

                <div class="col-md-9">
                    <utilize:fileupload runat="server" ID="flu_order_lines_xml" />
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_csv" Visible="false">
            <div class="row margin-bottom-20">
                <utilize:translatelabel runat="server" ID="label_select_csv_file" Text="CSV bestand:" CssClass="col-md-3"></utilize:translatelabel>

                <div class="col-md-9">
                    <utilize:fileupload runat="server" ID="flu_order_lines_csv" />
                </div>
            </div>
        </utilize:placeholder>
        
        <utilize:alert AlertType="danger" runat="server" ID="label_error_message" CssClass="message"></utilize:alert>

        <div class="buttons margin-top-20">
            <utilize:translatebutton runat="server" ID="button_import" Text="Importeren" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" OnClientClick="show_import_loader(); return true;" />
        </div>


        <utilize:placeholder runat="server" ID="ph_progress">
            
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_result">
            <h1><utilize:translatetitle runat="server" ID="title_file_processed" Text="Uw bestand is verwerkt"></utilize:translatetitle></h1>
            
            <utilize:panel runat="server" ID="pnl_success">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <utilize:image ID="Image1" runat="server" ImageUrl="~/DefaultImages/icon_confirm.png" />
                        </td>
                        <td class="success_title">
                            <utilize:literal runat="server" ID="lt_title_import_succesful"></utilize:literal></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <utilize:literal runat="server" ID="lt_text_import_succesful"></utilize:literal>
                        </td>
                    </tr>
                </table>
            </utilize:panel>

            <utilize:panel runat="server" ID="pnl_error">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <utilize:image runat="server" ID="img_error" ImageUrl="~/DefaultImages/icon_error.png" />
                        </td>
                        <td class="error_title">
                            <utilize:literal runat="server" ID="lt_title_import_unsuccesful"></utilize:literal></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <utilize:literal runat="server" ID="lt_text_import_unsuccesful"></utilize:literal>
                            <i>
                                <utilize:label runat="server" ID="lt_text_import_errors"></utilize:label></i>
                        </td>
                    </tr>
                </table>
            </utilize:panel>
        </utilize:placeholder>
    </utilize:placeholder>

    <div id="load-spinner">
        <div class="loader-wrapper">
            <div class="loader"></div>
            <utilize:translatelabel runat="server" ID="label_loader_import" Text="Regels worden geimporteerd..."></utilize:translatelabel>
        </div>
    </div>

    <utilize:button runat="server" ID="button_refresh" CssClass="refreshbutton" />
</div>
