﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_training.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_training" %>
<div class="uc_training">
    <h1><utilize:translatetitle runat="server" ID="title_training" Text="Overzicht trainingen"></utilize:translatetitle></h1>
    <div class="testimonials-v6 testimonials-wrap">        
        <utilize:repeater runat="server" ID="rpt_training">
            <ItemTemplate>
                <utilize:literal runat="server" id='lt_start_row' Text='<div class="row margin-bottom-50">' Visible="false"></utilize:literal>
                <div class="col-md-6 md-margin-bottom-50">
                    <utilize:hyperlink runat="server" ID="hl_target_url" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "url")%>'>
                        <div class="testimonials-info rounded-bottom">
                            <utilize:image runat="server" ID="img_training_logo"/>
                            <div class="testimonials-desc">
                                    <strong>
                                        <%# DataBinder.Eval(Container.DataItem, "title")%>
                                    </strong>
                                    <p>
                                        <%# DataBinder.Eval(Container.DataItem, "description")%>
                                    </p>
                            </div>
                        </div>            
                    </utilize:hyperlink>                        
                </div>
                <utilize:literal runat="server" id='lt_end_row' Text="</div>" Visible="false"></utilize:literal>
            </ItemTemplate>
        </utilize:repeater>
                <utilize:literal runat="server" id='lt_backup_end_row' Text="</div>" Visible="false"></utilize:literal>
    </div>
    <div class="buttons margin-bottom-20">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>    
</div>