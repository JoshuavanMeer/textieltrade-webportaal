﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_project_locations.ascx.vb" Inherits="uc_project_locations" %>
<div class="block account">
    <utilize:placeholder runat='server' ID="ph_project_locations">
        <h1><utilize:translatetitle runat='server' id="title_project_locations" Text="Overzicht projectlocaties"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_project_locations" Text="Onderstaand vindt u een overzicht van projectlocaties."></utilize:translatetext>
        <utilize:placeholder runat="server" ID="ph_search_project_locations" Visible='false'>
            <utilize:translatetext runat='server' ID="text_project_locations_search" Text="Vul hieronder uw criteria in en kies voor zoeken om de zoekopdracht uit te voeren"></utilize:translatetext>
            <utilize:panel runat='server' ID="pnl_search" DefaultButton="button_filter">
                <table class="overview search" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 25%"><utilize:label runat='server' ID="lbl_project_location_name" Text="Naam locatie"></utilize:label></td>
                        <td style="width: 25%"><utilize:textbox runat='server' ID="txt_project_location_name" Width="200px"></utilize:textbox></td>
                        <td style="width: 25%"><utilize:label runat='server' ID="lbl_project_location_address" Text="Adres"></utilize:label></td>
                        <td style="width: 25%"><utilize:textbox runat='server' ID="txt_project_location_address" Width="200px"></utilize:textbox></td>
                    </tr>
                    <tr>
                        <td><utilize:label runat='server' ID="lbl_project_location_zip_code" Text="Postcode"></utilize:label></td>
                        <td><utilize:textbox runat="server" ID="txt_project_location_zip_code" Width="80px"></utilize:textbox></td>
                        <td><utilize:label runat='server' ID="lbl_project_location_city" Text="Plaats"></utilize:label></td>
                        <td><utilize:textbox runat="server" ID="txt_project_location_city" Width="100px"></utilize:textbox></td>
                    </tr>
                </table>                
            </utilize:panel>
            <br style="clear: both;" />
            <div class="buttons">
                <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="left" text="Filter opheffen" Visible="false" />
                <utilize:translatebutton runat='server' ID="button_filter" CssClass="right" Text="Filter" />
                <br style="clear: both" />
            </div>
        </utilize:placeholder>
        <div class="buttons">
            <utilize:translatebutton runat='server' ID="button_search" CssClass="right" Text="Zoeken" />
            <br style="clear: both" />
        </div>

        <br style="clear: both" />
        <utilize:repeater runat="server" ID="rpt_project_locations" pager_enabled="true" page_size="10">
            <HeaderTemplate>
                <table class="overview" cellpadding=0 cellspacing=0>
                    <tr class="header">
                        <th><utilize:translatelabel runat='server' ID="col_project_location_name" Text="Naam locatie"></utilize:translatelabel></th>
                        <th><utilize:translatelabel runat='server' ID="col_project_location_address" Text="Adres"></utilize:translatelabel></th>
                        <th><utilize:translatelabel runat='server' ID="col_project_location_zip_code" Text="Postcode"></utilize:translatelabel></th>
                        <th><utilize:translatelabel runat='server' ID="col_project_location_city" Text="Plaats"></utilize:translatelabel></th>
                        <th class="quantity"><utilize:translatelabel runat="server" id="col_project_location_blocked" Text="Geblokkeerd"></utilize:translatelabel></th>
                        <th></th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td><utilize:literal runat='server' ID="lt_project_location_name" Text='<%# DataBinder.Eval(Container.DataItem, "location_name")%> '></utilize:literal></td>
                        <td><utilize:literal runat='server' ID="lt_project_location_address" Text='<%# DataBinder.Eval(Container.DataItem, "address")%> '></utilize:literal></td>
                        <td><utilize:literal runat='server' ID="lt_project_location_zip_code" Text='<%# DataBinder.Eval(Container.DataItem, "zip_code")%> '></utilize:literal></td>
                        <td><utilize:literal runat='server' ID="lt_project_location_city" Text='<%# DataBinder.Eval(Container.DataItem, "city")%> '></utilize:literal></td>
                        <td class="quantity"><utilize:checkbox runat='server' checked='<%# DataBinder.Eval(Container.DataItem, "blocked")%> ' Enabled="false"></utilize:checkbox></td>
                        <td class="change"><utilize:linkbutton runat="server" ID="link_change_project_location" Text="Wijzigen" CommandName="change" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'></utilize:linkbutton></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
            </FooterTemplate>                                    
        </utilize:repeater>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_project_locations" Visible='false'>
        <h1><utilize:translatetitle runat='server' id="title_no_project_locations" Text="Geen project locaties"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_project_locations" Text="Er zijn op dit moment geen project locaties vastgelegd."></utilize:translatetext>
    </utilize:placeholder>  
    <utilize:placeholder runat="server" ID="ph_project_location_details" Visible="false">
        <h1><utilize:translatetitle runat='server' id="title_change_project_location" Text="Projectlocatie wijzigen" Visible="false"></utilize:translatetitle><utilize:translatetitle runat='server' id="title_add_project_location" Text="Projectlocatie toevoegen" Visible="false"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_change_project_location" Text="Onderstaand kunt u de gegevens van de projectlocatie wijzigen." Visible="false"></utilize:translatetext><utilize:translatetext runat='server' ID="text_add_project_location" Text="Onderstaand kunt u de gegevens van de project locatie invullen." Visible="false"></utilize:translatetext>
        <utilize:textbox runat='server' ID="txt_rec_id" Visible='false'></utilize:textbox>
        <table>
            <tr>
                <td class="description"><utilize:translateliteral runat="server" ID="label_project_location_name" Text="Naam projectlocatie"></utilize:translateliteral></td>
                <td colspan="3"><utilize:textbox Runat="server" ID="txt_location_name" MaxLength="80" Width="300px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span></td>
            </tr>
            <tr>
                <td><utilize:translateliteral runat="server" ID="label_address" Text="Adres"></utilize:translateliteral></td>
                <td colspan="3"><utilize:textbox Runat="server" ID="txt_address" MaxLength="80" Width="300px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span></td>
            </tr>
            <tr>
                <td><utilize:translateliteral runat="server" ID="label_zip_code" Text="Postcode"></utilize:translateliteral></td>
                <td style="width: 85px;"><utilize:textbox Runat="server" ID="txt_zip_code" MaxLength="20" Width="60px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span></td>
                <td style="width: 60px;"><utilize:translateliteral runat="server" ID="label_city" Text="Plaats"></utilize:translateliteral></td>
                <td><utilize:textbox Runat="server" ID="txt_city" MaxLength="80" Width="150px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3"><utilize:checkbox runat="server" ID="chk_blocked"></utilize:checkbox></td>
            </tr>
        </table>
        <utilize:translatemessage runat="server" id="message_fields_not_filled" Text="Een of meer verplichte velden zijn niet gevuld" Visible="false"></utilize:translatemessage>
        <div class="buttons">
            <utilize:translatebutton runat="server" ID="button_save_project_location" Text="Gegevens bewaren" CssClass="right account" />
        </div>
        <br style="clear: both;" />
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account" /><utilize:translatebutton runat="server" ID="button_add_project_location" Text="Locatie toevoegen" CssClass="right account" /><utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right account" />
    </div>
</div>