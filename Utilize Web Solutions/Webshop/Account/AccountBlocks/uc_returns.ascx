﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_returns.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_returns" %>

<%@ Register Src="~/RMA/PageBlocks/uc_change_overview.ascx" TagName="uc_change_overview" TagPrefix="uc1" %>
<%@ Register Src="~/RMA/UserControls/uc_image_overview.ascx" TagName="uc_image_overview" TagPrefix="uc2" %>

<div class="uc_returns">
    <utilize:placeholder runat="server" ID="ph_no_returns" Visible="false">
        <h1>
            <utilize:translatetitle runat="server" ID="title_no_return" Text="Geen retouren"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_no_return" Text="Er zijn geen Retouren aanwezig"></utilize:translatetext>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_wrong_page_param" Visible="false">
        <h3>
            <utilize:translatetitle runat="server" ID="title_wrong_page_param" Text="Geen retour"></utilize:translatetitle></h3>
        <utilize:translatetext runat="server" ID="text_wrong_page_param" Text="Er is geen retour gevonden die het meegegeven parameter als retournummer hebben"></utilize:translatetext>
        <utilize:translatebutton runat="server" ID="button_rma_overview" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Terug naar overzicht" />
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_returns">
        <h1>
            <utilize:translatetitle runat="server" ID="title_return" Text="Overzicht retouren"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_return" Text="Onderstaand vind u een overzicht van de retouren"></utilize:translatetext>

        <div class="buttons margin-bottom-10">
            <utilize:translatebutton runat='server' ID="button_search" CssClass="right btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken" />
            <utilize:translatebutton runat="server" ID="button_close_search" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken sluiten" Visible="false" />
        </div>
        <utilize:placeholder runat="server" ID="ph_search" Visible="false">
            <div class="input-border">
                <div class="row">
                    <utilize:translatetext runat="server" ID="text_search" Text="Vul onderstaand uw zoekcriteria in."></utilize:translatetext>
                </div>

                <utilize:placeholder runat="server" ID="ph_no_filter" Visible="false">
                    <div class="">
                        <div class="col-md-6 alert alert-danger">
                            <utilize:translateliteral runat="server" ID="lt_no_filter_error" Text="U heeft geen filter opgegeven"></utilize:translateliteral>
                        </div>
                    </div>
                </utilize:placeholder>

                <div class="row margin-bottom-20">
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_return_code" Text="Retournummer"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_rma_code" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_order_code" Text="Ordernummer"></utilize:translatelabel></b>
                        <utilize:textbox runat="server" ID="txt_order_code" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat="server" ID="label_invoice_nr" Text="Factuurnummer"></utilize:translatelabel></b>
                        <utilize:textbox runat="server" ID="txt_invoice_nr" CssClass="form-control"></utilize:textbox>
                    </div>
                </div>
                <div class="row margin-bottom-20">
                    <div class="col-md-1">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_day" Text="Aanvraag dag"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_day" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                    </div>
                    <div class="col-md-1">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_month" Text="Aanvraag maand"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_month" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                    </div>
                    <div class="col-md-2">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_request_year" Text="Aanvraag jaar"></utilize:translatelabel></b>
                        <utilize:textbox runat='server' ID="txt_request_year" CssClass="form-control" MaxLength="4" type="number" onkeypress="return this.value.length<=3"></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                        <b>
                            <utilize:translatelabel runat='server' ID="label_search_status" Text="Status"></utilize:translatelabel></b>
                        <utilize:dropdownlist runat='server' ID="ddl_search_status" CssClass="form-control"></utilize:dropdownlist>
                    </div>
                </div>                
                <div class="row">
                    <utilize:translatebutton runat='server' ID="button_filter" CssClass="right btn-u btn-u-sea-shop btn-u-lg margin-right-5" Text="Filter" />
                    <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="left btn-u btn-u-sea-shop btn-u-lg" Text="Filter opheffen" Visible="false" />
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_no_found" Visible="false">
            <utilize:translatetext runat="server" ID="txt_no_found_returns" Text="Er zijn geen retouren gevonden met deze zoek waardes"></utilize:translatetext>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_return_table">
            <div class="table-responsive">
                <table class="table overview">
                    <thead>
                        <tr>
                            <th class="col-md-2">
                                <utilize:translatelabel runat="server" ID="col_rma_code" Text="Retournummer"></utilize:translatelabel></th>
                            <th class="col-md-2">
                                <utilize:translatelabel runat="server" ID="col_req_date" Text="Aanvraag datum"></utilize:translatelabel></th>                            
                            <th class="col-md-2">
                                <utilize:translatelabel runat="server" ID="col_compensation" Text="Compensatie"></utilize:translatelabel></th>
                            <th class="col-md-2">
                                <utilize:translatelabel runat="server" ID="col_status" Text="Status"></utilize:translatelabel></th>
                        </tr>
                    </thead>
                    <tbody>
                        <utilize:repeater runat="server" ID="rpt_returns" pager_enabled="true" page_size="20" pagertype="bottom">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <utilize:linkbutton runat="server" ID="row_rma_code" Text='<%# DataBinder.Eval(Container.DataItem, "rma_code")%>' CommandName="Return" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rma_code")%>'></utilize:linkbutton></td>                                    
                                    <td>
                                        <utilize:label runat="server" ID="row_req_date" Text='<%# Format(DataBinder.Eval(Container.DataItem, "req_date"), "d")%>'>'></utilize:label></td>                                    
                                    <td>
                                        <utilize:label runat="server" ID="row_compensation" Text='<%# DataBinder.Eval(Container.DataItem, "comp_desc_" + Me.global_ws.Language)%>'></utilize:label></td>
                                    <td>
                                        <utilize:label runat="server" ID="row_status" Text='<%# DataBinder.Eval(Container.DataItem, "req_status")%>'>'></utilize:label></td>
                                </tr>
                            </ItemTemplate>
                        </utilize:repeater>
                    </tbody>
                </table>
            </div>
        </utilize:placeholder>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_return_detail" Visible="false">
        <h1>
            <utilize:translatetitle runat="server" ID="title_return_detail" Text="overzicht retour"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_return_detail" Text="Onderstaand vind u de informatie over het retour wat u geselecteerd heeft"></utilize:translatetext>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_return_number" Text="Retournummer: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_number"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_return_customername" Text="Klantnaam: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_customer"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_return_order_nummer" Text="Ordernummer: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_return_order_number"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_req_date" Text="Aanvraag datum: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_req_date"></utilize:label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_req_status" Text="Status: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_req_status"></utilize:label>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_compensation" Text="Compensatie: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:label runat="server" ID="lbl_compensation"></utilize:label>
                    </div>
                </div>
                <utilize:placeholder runat="server" ID="ph_assigned_comp" Visible="false">
                    <div class="row">
                        <div class="col-md-4">
                            <b>
                                <utilize:translatelabel runat="server" ID="label_assigned_compensation" Text="Toegewezen compensatie:"></utilize:translatelabel></b>
                        </div>
                        <div class="col-md-4">
                            <b>
                                <utilize:label runat="server" ID="lbl_assigned_compensation"></utilize:label></b>
                        </div>
                    </div>
                </utilize:placeholder>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <utilize:translatelabel runat="server" ID="label_reason_description" Text="Reden omschrijving: "></utilize:translatelabel>
                    </div>
                    <div class="col-md-8">
                        <utilize:label runat="server" ID="lbl_reason_description"></utilize:label>
                    </div>
                </div>
                <utilize:placeholder runat="server" ID="ph_reject_reason" Visible="false">
                    <div class="row">
                        <div class="col-md-4">
                            <utilize:translatelabel runat="server" ID="label_reject_reason" Text="Afkeurings reden: "></utilize:translatelabel>
                        </div>
                        <div class="col-md-8">
                            <utilize:label runat="server" ID="lbl_reject_reason"></utilize:label>
                        </div>
                    </div>
                </utilize:placeholder>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table overview">
                        <thead>
                            <tr>
                                <th class="col-md-1">
                                    <utilize:translatelabel runat="server" ID="col_prod_code" Text="Productcode"></utilize:translatelabel></th>
                                <th class="col-md-3">
                                    <utilize:translatelabel runat="server" ID="col_prod_desc" Text="Product beschrijving"></utilize:translatelabel></th>
                                <th class="col-md-2 text-center">
                                    <utilize:translatelabel runat="server" ID="label_order_number" Text="Ordernummer"></utilize:translatelabel>
                                </th>
                                <th class="col-md-2 text-center">
                                    <utilize:translatelabel runat="server" ID="label_return_reason_single" Text="Reden"></utilize:translatelabel>
                                </th>                                
                                <th class="col-md-2 text-center">
                                    <utilize:translatelabel runat="server" ID="col_amount" Text="Aantal"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" id="ph_col_org_amt" Visible="false">
                                    <th class="text-center">
                                        <utilize:translatelabel runat="server" ID="label_org_amount" Text="Origineel aantal"></utilize:translatelabel>
                                    </th>
                                </utilize:placeholder>
                                <th class="col-md-1 text-right">
                                    <utilize:translatelabel runat="server" ID="col_row_amt" Text="Regelbedrag"></utilize:translatelabel></th>
                                <th class="col-md-1 text-right">
                                    <utilize:translatelabel runat="server" ID="col_btw_amt" Text="btw bedrag"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" ID="col_caret_down" Visible="false"><th style="width: 5%"><utilize:translateliteral runat="server" ID="lt_click_here" Text="Foto's weergeven"></utilize:translateliteral></th></utilize:placeholder>
                            </tr>
                        </thead>
                        <tbody>
                            <utilize:repeater runat="server" ID="rpt_return_detail">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                                        <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%></td>
                                        <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "doc_nr") %></td>
                                        <td class="text-center"><%# Utilize.Data.DataProcedures.GetValue("uws_rma_reason", "reason_desc_" + Me.global_cms.Language, DataBinder.Eval(Container.DataItem, "reason_code"))%></td>
                                        <td class="text-center"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0)%></td>
                                        <utilize:placeholder runat="server" ID="ph_row_org_amount" Visible="false">
                                            <td class="text-center"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "org_return_qty"), 0)%></td>
                                        </utilize:placeholder>
                                        <td class="text-right"><utilize:literal runat="server" ID="lt_return_detail_row_amount" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:literal></td>
                                        <td class="text-right"><utilize:literal runat="server" ID="lt_return_detail_row_vat" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "vat_amt"), 2), "c")%>'></utilize:literal></td>
                                        <utilize:placeholder runat="server" ID="td_caret_down" Visible="false">
                                            <td class="text-center"><a data-toggle="collapse" aria-expanded="false" id="link_caret" runat="server" class="image-caret"><i class="fa fa-caret-down"></i></a></td>
                                        </utilize:placeholder>
                                    </tr>
                                    <uc2:uc_image_overview runat="server" id="uc_image_overview" />
                                </ItemTemplate>
                            </utilize:repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <utilize:placeholder runat="server" ID="ph_alt_prod_table" Visible="false">
            <div class="row">
                <div class="col-md-12">
                    <h4>
                        <utilize:translatetitle runat="server" ID="title_alt_products" Text="De alternatieve producten"></utilize:translatetitle></h4>
                    <div class="table-responsive">
                        <table class="table overview">
                            <thead>
                                <tr>
                                    <th class="col-md-1">
                                        <utilize:translatelabel runat="server" ID="col_prod_code1" Text="Productcode"></utilize:translatelabel></th>
                                    <th class="col-md-3">
                                        <utilize:translatelabel runat="server" ID="col_prod_desc1" Text="Product beschrijving"></utilize:translatelabel></th>                                    
                                    <th class="col-md-2 text-center">
                                        <utilize:translatelabel runat="server" ID="col_amount1" Text="Aantal"></utilize:translatelabel></th>
                                    <th class="col-md-1 text-right">
                                        <utilize:translatelabel runat="server" ID="col_row_amt1" Text="Regelbedrag"></utilize:translatelabel></th>
                                    <th class="col-md-1 text-right">
                                        <utilize:translatelabel runat="server" ID="col_btw_amt1" Text="btw bedrag"></utilize:translatelabel></th>
                                </tr>
                            </thead>
                            <tbody>
                                <utilize:repeater runat="server" ID="rpt_alt_prod">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                                            <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%></td>
                                            <td class="text-center"><%# Decimal.Round(DataBinder.Eval(Container.DataItem, "prod_qty"), 0)%></td>
                                            <td class="text-right"><utilize:literal runat="server" ID="lt_return_alt_row_amount" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:literal></td>
                                            <td class="text-right"><utilize:literal runat="server" ID="lt_return_alt_row_vat" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_vat"), 2), "c")%>'></utilize:literal></td>
                                        </tr>
                                    </ItemTemplate>
                                </utilize:repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
    </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_rma_form" Visible="false">
            <div class="row">
                <div class="col-md-12">
                    <utilize:translatebutton runat="server" ID="button_rma_form" Text="Retour formulier genereren" CssClass="account btn-u btn-u-sea-shop btn-u-lg"></utilize:translatebutton>
                </div>
            </div>
                <br /> <br />
        </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_change_overview" Visible="false">
        <uc1:uc_change_overview runat="server" ID="uc_change_overview" />
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_respond_block" Visible="false">
        <utilize:placeholder runat="server" ID="ph_respond_error" Visible="false">
            <div class="row">
                <div class="col-md-6">
                    <div class="alert alert-danger">
                        <utilize:translateliteral runat="server" ID="txt_more_information_error" Text="U heeft geen bericht opgegeven"></utilize:translateliteral>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
        <div class="row margin-bottom-20">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h3>
                            <utilize:translatetitle runat="server" ID="title_user_message" Text="Bericht terug sturen"></utilize:translatetitle></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 form-group">
                        <utilize:textarea runat="server" ID="text_respond" CssClass="form-control"></utilize:textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7 form-group">
                        <utilize:placeholder runat="server" ID="ph_upload_image" Visible="false">
                            <asp:CheckBox runat="server" ID="show_upload_image" Checked="false" />
                            <utilize:placeholder runat="server" ID="ph_show_upload" Visible="true">
                                <div id="div_upload_image" class="hidden">
                                    <utilize:placeholder runat="server" ID="ph_upload_error" Visible="false">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="alert alert-danger">
                                                    <utilize:label runat="server" ID="label_upload_error"></utilize:label>
                                                </div>
                                            </div>
                                        </div>
                                    </utilize:placeholder>
                                    <utilize:fileupload ID="FileUpload1" runat="server" accept="image/png, image/jpeg" CssClass="form-control"></utilize:fileupload>
                                </div>
                            </utilize:placeholder>
                        </utilize:placeholder>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <utilize:translatebutton runat="server" ID="button_rma_user_input" Text="Versturen" CssClass="account btn-u btn-u-sea-shop btn-u-lg"></utilize:translatebutton>
                    </div>
                </div>

            </div>
        </div>
    </utilize:placeholder>

    <div class="buttons margin-bottom-20">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="account btn-u btn-u-sea-shop btn-u-lg" Visible="false" />
    </div>

</div>
