﻿
Partial Class uc_order_report
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Function get_so_orders() As Utilize.Data.DataTable
        Dim udt_orders As New Utilize.Data.DataTable
        udt_orders.table_name = "uws_so_orders"
        udt_orders.table_init()

        udt_orders.add_where("and cust_id = @cust_id")
        udt_orders.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        udt_orders.select_order = "av_ord_nr desc"

        udt_orders.table_load()

        Return udt_orders
    End Function
    Private Function get_so_order_lines(ByVal ord_nr As String) As Utilize.Data.DataTable
        Dim udt_orders As New Utilize.Data.DataTable
        udt_orders.table_name = "uws_so_order_lines"
        udt_orders.table_init()

        udt_orders.add_where("and hdr_id = @hdr_id")
        udt_orders.add_where_parameter("hdr_id", ord_nr)

        udt_orders.table_load()

        Return udt_orders
    End Function

    Private price_type As Integer = 0
    Private dt_order As System.Data.DataTable = Nothing
    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Voeg hier de stylesheet toe aan de pagina
        Dim lt_stylesheet As New literal
        lt_stylesheet.Text = "   <link href=""" & Me.ResolveCustomUrl("uc_order_report.css") & """ rel=""stylesheet"" type=""text/css"" />" + Chr(13)

        Dim hgc_head As System.Web.UI.WebControls.PlaceHolder = Me.Page.FindControl("styles")
        hgc_head.Controls.Add(lt_stylesheet)

        Me.rpt_orders.DataSource = Me.get_so_orders().data_table
        Me.rpt_orders.DataBind()
    End Sub

    Protected Sub rpt_orders_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_orders.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            ' Zet hier de order informatie
            Dim lt_order_subtotal_excl_vat As literal = e.Item.FindControl("lt_order_subtotal_excl_vat")
            Dim lt_order_costs_excl_vat As literal = e.Item.FindControl("lt_order_costs_excl_vat")
            Dim lt_order_shipping_costs_excl_vat As literal = e.Item.FindControl("lt_order_shipping_costs_excl_vat")
            Dim lt_order_vat_total As literal = e.Item.FindControl("lt_order_vat_total")
            Dim lt_order_total_incl_vat As literal = e.Item.FindControl("lt_order_total_incl_vat")

            lt_order_subtotal_excl_vat.Text = Format(e.Item.DataItem("order_total"), "c")
            lt_order_costs_excl_vat.Text = Format(e.Item.DataItem("order_costs"), "c")
            lt_order_shipping_costs_excl_vat.Text = Format(e.Item.DataItem("shipping_costs"), "c")
            lt_order_vat_total.Text = Format(e.Item.DataItem("vat_total"), "c")
            lt_order_total_incl_vat.Text = Format((e.Item.DataItem("order_total") + e.Item.DataItem("shipping_costs") + e.Item.DataItem("order_costs") - e.Item.DataItem("pay_disc_amt") + e.Item.DataItem("vat_total") + e.Item.DataItem("vat_ship") + e.Item.DataItem("vat_ord")), "c")

            If prices_not_visible Then
                Dim ph_prices_vat_excl As placeholder = e.Item.FindControl("ph_prices_vat_excl")
                ph_prices_vat_excl.Visible = False
            End If

            Dim rpt_order_details As repeater = e.Item.FindControl("rpt_order_details")
            rpt_order_details.DataSource = Me.get_so_order_lines(e.Item.DataItem("ord_nr")).data_table
            rpt_order_details.DataBind()

            ' Voeg een handler toe voor het aanmaken van de items
            AddHandler rpt_order_details.ItemCreated, AddressOf Me.rpt_order_details_ItemCreated
        End If
    End Sub

    Protected Sub rpt_order_details_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = UI.WebControls.ListItemType.Header Or e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim ph_size As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_size")
            ph_size.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Text = Format(e.Item.DataItem("row_amt"), "c")
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If
    End Sub
End Class
