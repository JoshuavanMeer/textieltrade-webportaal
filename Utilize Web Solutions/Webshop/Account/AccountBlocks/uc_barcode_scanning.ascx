﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_barcode_scanning.ascx.vb" Inherits="uc_barcode_scanning" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_overview.ascx" TagName="uc_shopping_cart_overview" TagPrefix="uc" %>
<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_totals.ascx" TagName="uc_shopping_cart_totals" TagPrefix="uc" %>

<div class="uc_barcode_scanning">
    <h1>
        <utilize:translatetitle runat='server' ID="title_barcode_scanning" Text="Via barcode bestellen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_barcode_scanning" Text="Onderstaand vindt u een tekstvak waarin u de barcodes kunt scannen. De producten worden toegevoegd aan een lijst waarna nu deze aan de winkelwagen kunt toevoegen."></utilize:translatetext>    
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <p><utilize:translateliteral runat='server' ID="label_scan_barcode" Text="Scan hier uw barcode:"></utilize:translateliteral></p>
            </div>
            <div class="col-md-12">
                <input id="txt_barcode" type="text" data-singleline="<%=Me.barcodesingleline %>" data-reverseorder="<%=Me.reverseorder %>" class="form-control">
            </div>
        </div>

        <utilize:alert runat="server" ID="alert_error" Visible="false" AlertType="danger"></utilize:alert>
    <utilize:alert runat="server" ID="success" Visible="false" AlertType="succes"></utilize:alert>

        <div class="table-responsive">
            <table id="dataTable" class="overview table">
                <tr class="header">
                    <th style="width: 140px; text-align: left;">
                        <utilize:translateliteral runat='server' ID="label_product" Text="Product"></utilize:translateliteral></th>
                    <th style="text-align: left;">
                        <utilize:translateliteral runat='server' ID="label_product_description" Text="Productomschrijving"></utilize:translateliteral></th>

                    <th style="width: 100px; text-align: right;">
                        <utilize:translatelabel runat="server" ID="label_product_price_per_product" Text="Prijs/stuk"></utilize:translatelabel>
                    </th>
                    <th style="text-align: center; width: 160px;">
                        <utilize:translateliteral runat='server' ID="label_order_quantity" Text="Aantal"></utilize:translateliteral></th>
                    <th style="text-align: center; width: 20px;"></th>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <utilize:label runat='server' ID="label_error_message" CssClass="message" Visible='false'></utilize:label>
        <br />
        <utilize:translatebutton runat='server' ID="button_add_to_cart" Text="Toevoegen aan winkelwagen" CssClass="account btn-u btn-u-sea-shop btn-u-lg pull-right" Visible="True" />

    <div class="margin-bottom-20"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            $('#txt_barcode').focus();
        }, 100);
    });
</script>