﻿Imports System.Data
Imports System.Web.UI.WebControls

Partial Class uc_barcode_scanning
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control
    Public Property barcodesingleline As Boolean = False
    Public Property reverseorder As Boolean = False
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Creating the session for the line numbers which added in barcodescanning.ascx.vb
        If Not IsPostBack Then
            Session("product_line") = "0"
        End If

        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        ' Check if barcode scan module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_barcode_scan") Then
            Response.Redirect("~/home.aspx", True)
        End If

        Me.set_javascript("uc_barcode_scanning.js", Me)
        Me.set_style_sheet("uc_barcode_scanning.css", Me)
        Me.block_css = False

        Dim prices_not_visible As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        If prices_not_visible Then
            Me.label_product_price_per_product.Visible = False
        Else
            Me.label_product_price_per_product.Visible = True
        End If

        'Setting the single line or multiline for Barcode and line order
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_barcode_scan") Then
            barcodesingleline = global_ws.get_webshop_setting("barcodesingleline")
            reverseorder = global_ws.get_webshop_setting("barcoderevorder")
        End If

    End Sub

    Private Sub uc_barcode_scanning_Load(sender As Object, e As EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "setBarcodeScript", "$(document).ready(function () {setBarcodeScript();});", True)
    End Sub

    Protected Sub button_add_to_cart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_to_cart.Click
        Me.alert_error.Visible = False

        Try
            For Each form_item As String In Me.Request.Form.AllKeys

                If (form_item.StartsWith("txt_barcode_prod_")) Then
                    Dim lcProdCode As String = ""

                    If Not (String.IsNullOrEmpty(Me.Request.Form(form_item)) Or Me.Request.Form(form_item) = "0") Then
                        If global_ws.get_webshop_setting("barcodesingleline") Then
                            lcProdCode = form_item.Replace("txt_barcode_prod_", "").Replace("_|_", " ")
                        Else
                            lcProdCode = form_item.Split(New Char() {"_"c})(4).Replace("_|_", " ")
                        End If
                        Dim lnQuantity As Integer = CInt(Me.Request.Form(form_item))
                        Me.global_ws.shopping_cart.product_add(lcProdCode, lnQuantity, "", "", "")
                    End If
                End If
            Next

            If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                Me.global_ws.shopping_cart.save_shopping_cart()

                Me.success.Visible = True
                Me.success.Text = global_trans.translate_message("message_barcode_add_to_cart", "Gescande producten toegevoegd aan de winkelwagen.", Me.global_ws.Language)

            Else
                Me.alert_error.Visible = True
                Me.alert_error.Text = global_trans.translate_message("message_barcode_add_to_cart_failed", "Gescande producten konden niet toegevoegd worden aan de winkelwagen. Fout details:", Me.global_ws.Language) + " " + Me.global_ws.shopping_cart.error_message
            End If
        Catch ex As Exception
            Me.alert_error.Visible = True
            Me.alert_error.Text = global_trans.translate_message("message_barcode_add_to_cart_failed", "Gescande producten konden niet toegevoegd worden aan de winkelwagen. Fout details:", Me.global_ws.Language) + " " + ex.Message
        End Try
    End Sub
End Class
