﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_backorders_overview.ascx.vb" Inherits="uc_backorders_overview" %>

<div class="block account">
    <h1><utilize:translatetitle runat='server' id="title_backorders_overview" Text="Backorder overzicht"></utilize:translatetitle></h1>

    <utilize:placeholder runat='server' ID="ph_no_backorders" Visible='false'>
        <utilize:translatetext runat='server' ID="text_no_backorders" Text="Er zijn op dit moment geen backorders"></utilize:translatetext>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_backorders" Visible="false">
        <utilize:translatetext runat='server' ID="text_backorders_overview" Text="Onderstaand vindt u een overzicht van bestelregels die in backorder staan."></utilize:translatetext>
        <utilize:repeater runat="server" ID="rpt_backorders">
            <HeaderTemplate>
                <div class="table-responsive">
                    <table class="lines table">
                        <thead>
                            <tr class="header">
                                <th class="number"><utilize:translatelabel runat='server' ID="col_order_number" Text="Ordernummer"></utilize:translatelabel></th>
                                <th class="date"><utilize:translatelabel runat='server' ID="col_order_date" Text="Datum"></utilize:translatelabel></th>
                                <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <th class="quantity text-center"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <th class="quantity text-center"><utilize:translatelabel runat='server' ID="col_backorder_quantity" Text="Aantal in backorder"></utilize:translatelabel></th>
                            </tr>
                        </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td class="number"><%# DataBinder.Eval(Container.DataItem, "av_ord_nr")%></td>
                        <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                        <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code") %></td>
                        <td><%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %></td>
                        <td class="quantity text-center"><utilize:literal runat="server" ID="lt_ord_qty"></utilize:literal></td>
                        <td class="quantity text-center"><utilize:literal runat="server" ID="lt_back_qty"></utilize:literal></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>