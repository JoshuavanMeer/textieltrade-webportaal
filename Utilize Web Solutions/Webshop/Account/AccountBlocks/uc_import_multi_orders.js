﻿function dropzone_init(id, buttonId, url_loc) {
 
    Dropzone.autoDiscover = true;

    var mydropzone = new Dropzone("#" + id, {
        url: url_loc,
        maxFilesize: 10,
        acceptedFiles: ".csv",
        addRemoveLinks: true,
        uploadMultiple: true,
        autoProcessQueue: false,
        success: function (file, response) {
            file.previewElement.classList.add("dz-success");
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        },
        //uploadprogress: function (file, progress, bytesSent) {
         
        //},
        //sendingmultiple: function () {           
        //    $("#multiorderProgressModal").css('display', 'block');
        //    $('#multiorderProgressModal').appendTo("body").modal({backdrop:'static',keyboard:false,show:true});
        //},
        //completemultiple: function (files, response) {
        //    location.reload(true);
        //},
        //errormultiple: function (files, response) {
        //    alert("Import error");
        //    location.reload(true);
        //},
        init: function () {
            var myDropzone = this; 

            var submitButton = document.querySelector('#' + buttonId)
            myDropzone = this; // closure

            submitButton.addEventListener("click", function () {
                    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
            });
        }
    });    
}

$(window).unload(function () {
    $(".remove_images").click();
});

