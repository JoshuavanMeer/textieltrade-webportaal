﻿
Partial Class uc_concept_orders
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
    Private printing_visible As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_conc_or_prin")

    Private Function get_orders() As Utilize.Data.DataTable
        Dim udt_concept_orders As New Utilize.Data.DataTable
        udt_concept_orders.table_name = "uws_orders"
        udt_concept_orders.table_init()

        udt_concept_orders.add_where("and cust_id = @cust_id")
        udt_concept_orders.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        udt_concept_orders.select_order = "rec_id desc"

        udt_concept_orders.table_load()

        Return udt_concept_orders
    End Function
    Private Function get_order_lines(ByVal ord_nr As String) As Utilize.Data.DataTable
        Dim udt_concept_orders As New Utilize.Data.DataTable
        udt_concept_orders.table_name = "uws_order_lines"
        udt_concept_orders.table_init()

        udt_concept_orders.add_where("and hdr_id = @hdr_id")
        udt_concept_orders.add_where_parameter("hdr_id", ord_nr)

        udt_concept_orders.table_load()

        Return udt_concept_orders
    End Function
    Private Function get_order_list() As Utilize.Data.DataTable
        ' Vul hier de lijst met conceptbestellingen
        Dim dt_data_table As Utilize.Data.DataTable = Me.get_orders()

        ' Enlarge rec_id to make changes possible
        dt_data_table.data_table.Columns.Item("rec_id").MaxLength = dt_data_table.data_table.Columns.Item("rec_id").MaxLength + 40
        dt_data_table.data_table.AcceptChanges()

        dt_data_table.data_table.AcceptChanges()

        Return dt_data_table
    End Function

    Protected price_type As Integer = 0
    Private dt_order As System.Data.DataTable = Nothing

    Private Sub set_concept_order_information(ByVal order_number As String)
        Dim udt_data_table As New Utilize.Data.DataTable
        udt_data_table.table_name = "uws_orders"
        udt_data_table.table_init()
        udt_data_table.add_where("and rec_id = @rec_id")
        udt_data_table.add_where_parameter("rec_id", order_number)
        udt_data_table.table_load()

        If udt_data_table.data_row_count > 0 Then
            dt_order = udt_data_table.data_table

            Me.lbl_concept_order_date.Text = Format(udt_data_table.field_get("ord_date"), "d")
            Me.lbl_concept_order_number.Text = udt_data_table.field_get("rec_id")
            Me.lbl_concept_order_description.Text = udt_data_table.field_get("order_description")

            Me.lt_concept_order_subtotal_excl_vat.Text = Format(udt_data_table.field_get("order_total"), "c")
            Me.lt_concept_order_subtotal_incl_vat.Text = Format((udt_data_table.field_get("order_total") + udt_data_table.field_get("vat_total")), "c")

            Me.lt_concept_order_shipping_costs_excl_vat.Text = Format(udt_data_table.field_get("shipping_costs"), "c")
            Me.lt_concept_order_shipping_costs_incl_vat.Text = Format((udt_data_table.field_get("shipping_costs") + udt_data_table.field_get("vat_ship")), "c")

            Me.lt_concept_order_costs_excl_vat.Text = Format(udt_data_table.field_get("order_costs"), "c")
            Me.lt_concept_order_costs_incl_vat.Text = Format((udt_data_table.field_get("order_costs") + udt_data_table.field_get("vat_ord")), "c")

            Me.lt_concept_order_vat_total.Text = Format((udt_data_table.field_get("vat_total") + udt_data_table.field_get("vat_ship_cost") + udt_data_table.field_get("vat_order_cost")), "c")
            Me.lt_concept_order_total_incl_vat.Text = Format((udt_data_table.field_get("order_total") + udt_data_table.field_get("shipping_costs") + udt_data_table.field_get("order_costs") + udt_data_table.field_get("vat_total") + udt_data_table.field_get("vat_ship") + udt_data_table.field_get("vat_ord")), "c")
            Me.lt_concept_order_total_incl_vat1.Text = Format((udt_data_table.field_get("order_total") + udt_data_table.field_get("shipping_costs") + udt_data_table.field_get("order_costs") + udt_data_table.field_get("vat_total") + udt_data_table.field_get("vat_ship") + udt_data_table.field_get("vat_ord")), "c")

            Me.ph_concept_orders.Visible = False
            Me.ph_concept_order_details.Visible = True
        End If

        Me.rpt_concept_order_details.DataSource = Me.get_order_lines(order_number).data_table
        Me.rpt_concept_order_details.DataBind()
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.price_type = Me.global_ws.get_webshop_setting("price_type")

        Me.rpt_concept_orders.DataSource = Me.get_orders().data_table
        Me.rpt_concept_orders.DataBind()

        Me.set_style_sheet("uc_concept_orders.css", Me)
        Me.block_css = False

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        If Not Me.IsPostBack() Then
            ' Vul hier de lijst met conceptbestellingen

            Me.ddl_select_concept_order.DataSource = Me.get_order_list().data_table
            Me.ddl_select_concept_order.DataTextField = "rec_id"
            Me.ddl_select_concept_order.DataValueField = "PrimaryKey"
            Me.ddl_select_concept_order.DataBind()
            Me.ddl_select_concept_order.AutoPostBack = True

            ' Maak een nieuwe regel aan met "Nieuwe conceptbestelling"
            ddl_select_concept_order.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = global_trans.translate_label("label_new_concept_order", "Nieuwe conceptbestelling", Me.global_ws.Language), .Value = ""})

            Try
                ' Probeer hier de juiste conceptbestelling te vullen
                Me.ddl_select_concept_order.SelectedValue = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("rec_id")
            Catch ex As Exception

            End Try

            ' Toon een melding voor het wijzigen
            ' Me.ddl_select_concept_order.Attributes.Add("onChange", "return confirm('Weet u zeker dat u de conceptbestelling wilt wijzigen?');")
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.button_close_details.Visible = Me.ph_concept_order_details.Visible

        ' Haal hier uit de webshop instellingen op of de prijzen inclusief of exclusief BTW zijn.
        Me.ph_prices_vat_excl.Visible = Me.global_ws.get_webshop_setting("price_type") = 1 And Not prices_not_visible
        Me.ph_prices_vat_incl.Visible = Me.global_ws.get_webshop_setting("price_type") = 2 And Not prices_not_visible
        Me.button_continue_shopping.Visible = Not Me.ph_concept_order_details.Visible
    End Sub

    Protected Sub rpt_concept_orders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_concept_orders.ItemCommand
        Select Case e.CommandName
            Case "details"
                Dim lc_ord_nr As String = e.CommandArgument

                Me.set_concept_order_information(lc_ord_nr)
            Case "delete_order"
                ' Referentie naar de conceptbestelling
                Dim lc_ord_nr As String = e.CommandArgument

                ' Als deze hetzelfde is als de winkelwagen, dan meot er een nieuwe winkelwagen aangemaakt worden
                If lc_ord_nr = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("rec_id") Then
                    Me.global_ws.shopping_cart.create_shopping_cart()
                End If

                ' Verwijder hier de orders
                Dim ubo_business_object As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_orders", lc_ord_nr, "rec_id")
                ubo_business_object.table_delete_row("uws_orders", Me.global_ws.user_information.user_id)

                ' Bouw de lijst opnieuw op
                Me.rpt_concept_orders.DataSource = Me.get_orders().data_table
                Me.rpt_concept_orders.DataBind()

                ' Vul hier de lijst met conceptbestellingen
                Me.ddl_select_concept_order.DataSource = Me.get_order_list().data_table
                Me.ddl_select_concept_order.DataBind()

                Me.ddl_select_concept_order.SelectedIndex = 0
            Case "finish"
                ' Referentie naar de conceptbestelling
                Dim lc_ord_nr As String = e.CommandArgument
                Me.global_ws.shopping_cart.load_shopping_cart(lc_ord_nr)

                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Case "openpdf"
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/Account/OpenDocument.aspx?DocType=concorder&DocNr=" + e.CommandArgument, True)
            Case Else
                Exit Select
        End Select
    End Sub

    Protected Sub rpt_concept_orders_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_concept_orders.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_concept_order_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible

            Dim link_delete_order As Utilize.Web.UI.WebControls.button = e.Item.FindControl("link_delete_order")
            link_delete_order.OnClientClick = "return confirm('" + global_trans.translate_message("message_delete_concept_order", "Weet u zeker dat u de conceptbestelling wilt verwijderen?", Me.global_ws.Language) + "');"

            Dim link_print_pdf As translatelinkbutton = e.Item.FindControl("link_pdf_order")
            link_print_pdf.Visible = printing_visible
        End If
    End Sub

    Protected Sub rpt_concept_order_details_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_concept_order_details.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Header Then
            Dim lt_literal As Utilize.Web.UI.WebControls.label = e.Item.FindControl("col_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")
            lt_literal.Visible = Not prices_not_visible
        End If

        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            Dim lt_literal As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_row_amount")

            Try
                If Me.price_type = 1 Then
                    lt_literal.Text = Format(e.Item.DataItem("row_amt"), "c")
                Else
                    lt_literal.Text = Format((e.Item.DataItem("row_amt") + e.Item.DataItem("vat_amt")), "c")
                End If

            Catch ex As Exception

            End Try

            Try
                ' Decimalen
                Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(e.Item.DataItem("prod_code"))

                '<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0) %>
                Dim lt_ord_qty As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_ord_qty")
                If lt_ord_qty IsNot Nothing Then
                    lt_ord_qty.Text = Format(Decimal.Round(e.Item.DataItem("ord_qty"), ln_decimal_numbers), "N" + ln_decimal_numbers.ToString())
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub button_close_details_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_close_details.Click
        Me.ph_concept_order_details.Visible = False
        Me.ph_concept_orders.Visible = True
    End Sub

    Protected Sub ddl_select_concept_order_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_select_concept_order.SelectedIndexChanged
        If Me.ddl_select_concept_order.SelectedValue = "" Then
            ' Als een nieuwe conceptbestelling is geselecteerd
            Me.global_ws.shopping_cart.create_shopping_cart()
        Else
            Me.global_ws.shopping_cart.load_shopping_cart(Me.ddl_select_concept_order.SelectedValue)
        End If
    End Sub

    Private Sub button_continue_shopping_Click(sender As Object, e As EventArgs) Handles button_continue_shopping.Click
        If Not Session("ProductPage") Is Nothing Then
            Response.Redirect(Session("ProductPage"))
        Else
            Response.Redirect("~/home.aspx")
        End If
    End Sub
End Class
