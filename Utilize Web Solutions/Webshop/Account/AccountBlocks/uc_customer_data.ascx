﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_customer_data.ascx.vb" Inherits="uc_customer_data" %>

<%@ Register Src="~/Webshop/PaymentProcess/Blocks/uc_address_block.ascx" tagname="uc_address_block" tagprefix="uc1" %>

<h1><utilize:translatetitle runat="server" ID="title_customer_change_address" Text="Gegevens wijzigen"></utilize:translatetitle></h1>

<div class="block account">
    <utilize:placeholder runat="server" ID="ph_change_address">
            <utilize:translatetext runat="server" ID="text_customer_change_address" Text="Onderstaand vind u de gegevens zoals deze bij ons bekend zijn en kunt u deze wijzigen."></utilize:translatetext>
            <uc1:uc_address_block ID="uc_address_block1" runat="server" address_type="customer_invoice_address" use_login="false" block_css="false" />

            <utilize:placeholder runat="server" ID="ph_changes_buttons" Visible='false'>
                <div class="buttons">
                    <utilize:translatebutton runat='server' ID="button_send_changes" Text="Wijzigingen doorgeven" CssClass="right account send_changes btn-u btn-u-sea-shop btn-u-lg" />
                    <br style="clear: both;">
                    <br style="clear: both;">
                </div>
            </utilize:placeholder>
        
            <utilize:placeholder runat="server" ID="ph_delivery_address">
                <h3><utilize:translatetitle runat="server" ID="title_different_delivery_address" Text="Wilt u uw bestelling standaard op een ander adres af laten leveren?"></utilize:translatetitle></h3>
                <utilize:translatetext runat="server" ID="text_different_delivery_address" Text="Wanneer u uw bestelling op een ander adres wilt af laten leveren dan de factuur, dan kunt u dat hieronder aangeven."></utilize:translatetext>
                <uc1:uc_address_block ID="uc_address_block2" runat="server" address_type="customer_delivery_address" block_css="false" />
            </utilize:placeholder>
    </utilize:placeholder>

    <utilize:placeholder runat='server' ID="ph_form_block" Visible="false">
        
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_change_confirm" Visible="false">
        <utilize:translatetext runat="server" ID="text_customer_adddress_changed" Text="Uw gegevens zijn gewijzigd."></utilize:translatetext>
    </utilize:placeholder>

    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_save_changes" Text="Wijzigingen bewaren" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>
