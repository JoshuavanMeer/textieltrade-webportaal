﻿Imports System.Linq
Imports Utilize.Data.API.Webshop

Partial Class uc_account_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Public on_demand_sync As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And IntermediateServiceProcedures.get_setting("on_demand_hist_sync")

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        If Session("log_in_app") Then
            If Not Me.global_ws.user_information.user_logged_on Then
                Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
            End If
        End If

        If DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.chk_show_prices.Text = global_trans.translate_label("label_show_prices", "Toon prijzen", Me.global_ws.Language)

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_price_multip") Then
            Me.chk_use_price_multiplier.Text = global_trans.translate_label("label_use_price_multiplier", "Prijs vermenigvuldiger gebruiken", Me.global_ws.Language)
        End If

        Me.set_style_sheet("uc_account_block.css", Me)

        Me.lt_welcome.Text = global_trans.translate_title("title_welcome", "Welkom", Me.global_ws.Language) + " " + Me.global_ws.user_information.user_full_name

        ph_order_overview.Visible = Not on_demand_sync

        ' Zet hier de links goed.
        Me.link_account_orders_overview.NavigateUrl = "~/" + Me.global_ws.Language + "/account/orders.aspx"
        Me.link_account_order_history.NavigateUrl = "~/" + Me.global_ws.Language + "/account/orderhistory.aspx"
        Me.link_account_open_items.NavigateUrl = "~/" + Me.global_ws.Language + "/account/openitems.aspx"
        Me.link_account_favorites.NavigateUrl = "~/" + Me.global_ws.Language + "/account/favorites.aspx"
        Me.link_account_change_password.NavigateUrl = "~/" + Me.global_ws.Language + "/account/changepassword.aspx"
        Me.link_account_customer_data.NavigateUrl = "~/" + Me.global_ws.Language + "/account/customerdata.aspx"
        Me.link_account_delivery_addresses.NavigateUrl = "~/" + Me.global_ws.Language + "/account/deliveryaddresses.aspx"
        Me.link_account_users.NavigateUrl = "~/" + Me.global_ws.Language + "/account/users.aspx"
        Me.link_account_customers_overview.NavigateUrl = "~/" + Me.global_ws.Language + "/account/customersoverview.aspx"
        Me.link_account_order_entry.NavigateUrl = "~/" + Me.global_ws.Language + "/account/orderentry.aspx"
        Me.link_account_order_lists.NavigateUrl = "~/" + Me.global_ws.Language + "/account/orderlists.aspx"
        Me.link_account_concept_orders_overview.NavigateUrl = "~/" + Me.global_ws.Language + "/account/conceptorders.aspx"
        Me.link_account_project_locations.NavigateUrl = "~/" + Me.global_ws.Language + "/account/projectlocations.aspx"
        Me.link_account_backorders_overview.NavigateUrl = "~/" + Me.global_ws.Language + "/account/backordersoverview.aspx"
        Me.link_account_barcode_scanning.NavigateUrl = "~/" + Me.global_ws.Language + "/account/barcodescanning.aspx"
        Me.link_account_import_order_lines.NavigateUrl = "~/" + Me.global_ws.Language + "/account/importorderlines.aspx"
        Me.link_account_rma_return_request.NavigateUrl = "~/" + Me.global_ws.Language + "/account/returnrequest.aspx"
        Me.link_account_rma_returns_in_process.NavigateUrl = "~/" + Me.global_ws.Language + "/account/returns.aspx?filter=inprocess"
        Me.link_account_rma_returns_finished.NavigateUrl = "~/" + Me.global_ws.Language + "/account/returns.aspx?filter=afgerond"
        Me.link_account_export_products.NavigateUrl = "~/" + Me.global_ws.Language + "/webshop/feed/assortmentfeeddownload.aspx"
        Me.link_account_multi_orders.NavigateUrl = "~/" + Me.global_ws.Language + "/account/ImportMultiOrders.aspx"
        Me.link_account_training.NavigateUrl = "~/" + Me.global_ws.Language + "/account/Training.aspx"

        Me.ph_order_history.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_history")
        Me.ph_openitems.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_openitems")

        ' Stel multi order import en export template indien multi order module aan staat en ook het address boek
        Me.ph_multi_order.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book") And Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_MULTIORDER")

        ' Stel de Barcode scanning beschikbaar als de module aan staat
        Me.ph_barcode_scanning.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_barcode_scan")

        ' Stel de Order Import beschikbaar als deze module aan staat
        Me.ph_order_import.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_import")

        ' Stel Order Entry (Snel bestellen) beschikbaar als deze module aanstaat
        Me.ph_order_entry.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_entry")

        ' Stel Bestellijsten beschikbaar als deze module aan staat.
        Me.ph_order_lists.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_lists")

        ' Maak de conceptbestellingen zichtbaar als de module aan staat.
        Me.ph_concept_orders.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_conc_orders")

        ' Stel Favorieten beschikbaar als deze module aan staat.
        Me.ph_product_favorites.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_favorites")

        ' Adresboek beschikbaar als deze module aan staat.
        Me.ph_delivery_addresses.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book")

        ' Gebruikers zichtbaar als de gebruiker rechten heeft en de module aan staat.
        Me.ph_user_rights.Visible = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb")) And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_usr")
        Me.ph_project_locations.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Me.global_ws.user_information.ubo_customer.field_get("uws_customers.ploc_enab")
        Me.ph_customers_overview.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sales_module") And Me.global_cms.get_cms_setting("eml_from_address") = Me.global_ws.user_information.ubo_customer.field_get("email_address")
        Me.ph_product_export.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_export")
        Me.ph_e_catalogus.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_e_catalog")

        ' Backordersoverzicht wordt alleen getoond wanneer een klant aangegeven heeft dat hij backorder aantallen wil zien
        Me.ph_back_orders.Visible = Me.global_ws.get_webshop_setting("show_back_qty")

        ' RMA onderdeel zichtbaar wanneer de module aan staat
        Me.ph_rma.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_rma")

        'Trainingen worden zichtbaar als de module aan staat
        Me.ph_training.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cus_training")
        If Me.ph_training.Visible Then
            Dim qsc_training As New Utilize.Data.QuerySelectClass
            qsc_training.select_fields = "distinct title_" + Me.global_ws.Language + " as title, desc_" + Me.global_ws.Language + " as description"
            qsc_training.select_from = "uws_training"
            qsc_training.add_join("left join", "uws_so_order_lines", "uws_training.prod_code = uws_so_order_lines.prod_code")
            qsc_training.add_join("left join", "uws_history_lines", "uws_training.prod_code = uws_history_lines.prod_code")
            qsc_training.select_where = "(uws_so_order_lines.hdr_id in (select ord_nr from uws_so_orders where cust_id = @cust_id) and uws_history_lines.hdr_id in (select crec_id from uws_history where cust_id = @cust_id)) or uws_training.prod_code = ''"
            qsc_training.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            Dim dt_training As System.Data.DataTable = qsc_training.execute_query()

            Me.ph_training.Visible = dt_training.Rows.Count > 0
        End If

        ' Gegevens wijzigen zichtbaar als de module gebruikersrechten niet aan staat of als de gebruiker recehten heeft
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            Me.ph_barcode_scanning.Visible = Me.ph_barcode_scanning.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
            Me.ph_order_entry.Visible = Me.ph_order_entry.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
            Me.ph_order_lists.Visible = Me.ph_order_lists.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
            Me.ph_order_import.Visible = Me.ph_order_import.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
            Me.ph_multi_order.Visible = Me.ph_multi_order.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
            Me.ph_customer_data.Visible = Me.ph_customer_data.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_cm")
            Me.ph_openitems.Visible = Me.ph_openitems.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_oi")
            Me.ph_order_history.Visible = Me.ph_order_history.Visible And Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_poh")
        End If

        If checkSsoUser() Then
            ph_change_password.Visible = False
        End If

        Me.ph_show_prices_option.Visible = Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter")
        Me.ph_use_price_multiplier.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_price_multip") And Me.global_ws.user_information.ubo_customer.field_get("uws_customers.allow_multiplier")

        If Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Then
            Me.chk_show_prices.Checked = Me.global_ws.show_prices
        End If

        If Me.ph_use_price_multiplier.Visible Then
            Me.chk_use_price_multiplier.Checked = Session("use_price_multiplier")
        End If

        ' Hide the complete panel when the openitems and history are not visible
        If ph_order_history.Visible = False And ph_openitems.Visible = False Then
            Me.ph_history.Visible = False
        End If

    End Sub

    Private Function checkSsoUser() As Boolean
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sso") Then
            Dim qsc_debtors As New Utilize.Data.QuerySelectClass
            With qsc_debtors
                .select_fields = "main_debtor"
                .select_from = "uws_single_sign_on"
            End With

            Dim debtors As List(Of String) = qsc_debtors.execute_query.Select.AsEnumerable _
                .Select(Function(x) x.Item("main_debtor").ToString).ToList

            Return debtors.Contains(Me.global_ws.user_information.user_customer_id)
        End If
        Return False
    End Function

    ''' <summary>
    ''' Show or not show the prices to the customer
    ''' </summary>
    ''' <param name="sender">Mobile link or buttons</param>
    ''' <param name="e"></param>
    Protected Sub Show_Hide_Prices(sender As Object, e As EventArgs) Handles chk_show_prices.CheckedChanged
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Then
            Me.global_ws.show_prices = Me.chk_show_prices.Checked
        End If
    End Sub

    Protected Sub use_multiplier(sender As Object, e As EventArgs) Handles chk_use_price_multiplier.CheckedChanged
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_price_multip") Then
            Session("use_price_multiplier") = Me.chk_use_price_multiplier.Checked
            RecalculateShoppingCart()
        End If
    End Sub

    ''' <summary>
    ''' Deze functie bepaalt het aantal zichtbare kolommen.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetColumnSize() As String
        Dim ln_visible_count As Integer = 0

        If Me.ph_customers_overview.Visible Then
            ln_visible_count += 1
        End If

        If Me.ph_product_export.Visible Then
            ln_visible_count += 1
        End If

        If Me.ph_e_catalogus.Visible Then
            ln_visible_count += 1
        End If

        If Me.ph_change_password.Visible Then
            ln_visible_count += 1
        End If

        Return "col-md-" + (12 / ln_visible_count).ToString()
    End Function

    Protected Sub link_account_e_catalogus_Click(sender As Object, e As EventArgs)
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        qsc_categories.select_fields = "*"
        qsc_categories.select_from = "uws_categories"
        qsc_categories.select_order = "row_ord"
        qsc_categories.select_where = "uws_categories.cat_code in (select cat_code from uws_categories_prod)"

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        Me.opt_catalogus_categories.DataSource = qsc_categories.execute_query

        Me.opt_catalogus_categories.DataTextField = "cat_desc"
        Me.opt_catalogus_categories.DataValueField = "cat_code"

        Me.opt_catalogus_categories.DataBind()

        Me.pnl_e_catalogus.Visible = True
    End Sub

    Protected Sub button_generate_Click(sender As Object, e As EventArgs)
        Dim selected_category_codes As String = ""

        For Each category In Me.opt_catalogus_categories.Items
            If category.Selected = True Then
                selected_category_codes = selected_category_codes + category.Value + ","
            End If
        Next

        Me.pnl_e_catalogus.Visible = False

        If String.IsNullOrEmpty(selected_category_codes) Then
            Response.Redirect("~/" + Me.global_ws.Language + "/account/ecatalogus.aspx?headertext=" + System.Web.HttpUtility.UrlEncode(txtb_configure_headertext.Text.ToString), False)
        Else
            Response.Redirect("~/" + Me.global_ws.Language + "/account/ecatalogus.aspx?headertext=" + System.Web.HttpUtility.UrlEncode(txtb_configure_headertext.Text.ToString) + "&categoriescodes=" + selected_category_codes, False)
        End If

    End Sub

    Protected Sub button_cancel_Click(sender As Object, e As EventArgs)
        Me.pnl_e_catalogus.Visible = False
    End Sub

    Private Sub uc_account_block_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_price_multip") Then
            ph_multiplier.Visible = chk_use_price_multiplier.Checked

            txt_price_multiplier.Text = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.price_multiplier").ToString
        End If
    End Sub

    Private Sub txt_price_multiplier_TextChanged(sender As Object, e As EventArgs) Handles txt_price_multiplier.TextChanged
        Dim ln_value As Decimal = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.price_multiplier")
        Dim lc_text_value As String = txt_price_multiplier.Text.Replace(".", ",")

        If Decimal.TryParse(lc_text_value, ln_value) Then
            If ln_value < 10 Then
                Me.global_ws.user_information.ubo_customer.field_set("uws_customers.price_multiplier", ln_value)

                Me.global_ws.user_information.ubo_customer.table_update("price_multiplier")

                RecalculateShoppingCart()
            End If
        End If
    End Sub

    Private Sub RecalculateShoppingCart()
        Dim ll_move_next As Boolean = Me.global_ws.shopping_cart.ubo_shopping_cart.move_first("uws_order_lines")

        While ll_move_next
            Dim ln_quantity As Decimal = Me.global_ws.shopping_cart.ubo_shopping_cart.field_get("uws_order_lines.ord_qty")

            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("uws_order_lines.ord_qty", ln_quantity)

            ll_move_next = Me.global_ws.shopping_cart.ubo_shopping_cart.move_next("uws_order_lines")
        End While
    End Sub
End Class
