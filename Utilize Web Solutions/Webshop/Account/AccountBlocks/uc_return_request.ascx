﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_return_request.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_return_request" %>

<div class="uc_return_request">
    <asp:HiddenField runat="server" ID="hih_active_tab" Value="pnl_orders" />
    <utilize:placeholder runat="server" ID="ph_tabs">
        <ul class="nav nav-tabs" role="tablist">
            <li runat="server" id="li_orders" role="presentation" class="orders">
                <utilize:translatelinkButton runat="server" id="lb_tab_orders" Text="Bestellingen" aria-controls="orders" role="tab" data-toggle="tab">
                </utilize:translatelinkButton>
            </li>
            <li runat="server" id="li_bulk" role="presentation" class="bulk">
                <utilize:translatelinkButton runat="server" id="lb_tab_bulk" Text="Bulk retouneren" aria-controls="orders" role="tab" data-toggle="tab" >
                </utilize:translatelinkButton>
            </li>
        </ul>
    </utilize:placeholder>

    <div class="tab-content">
        <utilize:panel runat="server" id="pnl_orders" role="tabpanel" class="tab-pane active">
            <utilize:placeholder runat="server" ID="ph_returns">
                <h1>
                    <utilize:translatetitle runat="server" ID="title_returns_request" Text="Orderoverzicht"></utilize:translatetitle></h1>
                <utilize:translatetext runat="server" ID="text_return_orders" Text="Onderstaand treft u een overzicht van uw bestellingen. Klik op het ordernummer om voor de betreffende order de producten te selecteren die u wilt retourneren."></utilize:translatetext>

                <div class="buttons margin-bottom-10">
                    <utilize:translatebutton runat='server' ID="button_search" CssClass="right btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken" />
                    <utilize:translatebutton runat="server" ID="button_close_search" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken sluiten" Visible="false" />
                </div>
                <utilize:placeholder runat="server" ID="ph_order_search" Visible="false">
                    <div class="input-border">

                        <utilize:placeholder runat="server" ID="ph_no_filter" Visible="false">
                            <div class="row">
                                <div class="col-md-6 alert alert-danger">
                                    <utilize:translateliteral runat="server" ID="lt_no_filter_error" Text="U heeft geen filter opgegeven"></utilize:translateliteral>
                                </div>
                            </div>
                        </utilize:placeholder>
                        <div class="row margin-bottom-20">
                            <utilize:placeholder runat="server" ID="ph_search_order_nr">
                                <div class="col-md-3">
                                    <b>
                                        <utilize:translatelabel runat="server" ID="label_order_code" Text="Ordernummer"></utilize:translatelabel></b>
                                    <utilize:textbox runat="server" ID="txt_order_code" CssClass="form-control"></utilize:textbox>
                                </div>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_search_invoice_nr">
                                <div class="col-md-3">
                                    <b>
                                        <utilize:translatelabel runat="server" ID="label_invoice_nr" Text="Factuurnummer"></utilize:translatelabel></b>
                                    <utilize:textbox runat="server" ID="txt_invoice_nr" CssClass="form-control"></utilize:textbox>
                                </div>
                            </utilize:placeholder>                                                        
                            <div class="col-md-1">
                                <b>
                                    <utilize:translatelabel runat='server' ID="label_request_day" Text="Dag"></utilize:translatelabel></b>
                                <utilize:textbox runat='server' ID="txt_request_day" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                            </div>
                            <div class="col-md-1">
                                <b>
                                    <utilize:translatelabel runat='server' ID="label_request_month" Text="Maand"></utilize:translatelabel></b>
                                <utilize:textbox runat='server' ID="txt_request_month" CssClass="form-control" MaxLength="2" type="number" onkeypress="return this.value.length<=1"></utilize:textbox>
                            </div>
                            <div class="col-md-2">
                                <b>
                                    <utilize:translatelabel runat='server' ID="label_request_year" Text="Jaar"></utilize:translatelabel></b>
                                <utilize:textbox runat='server' ID="txt_request_year" CssClass="form-control" MaxLength="4" type="number" onkeypress="return this.value.length<=3"></utilize:textbox>
                            </div>
                        </div>
                        <div class="buttons">
                            <utilize:translatebutton runat='server' ID="button_filter" CssClass="right btn-u btn-u-sea-shop btn-u-lg margin-right-5" Text="Zoeken" />
                            <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="left btn-u btn-u-sea-shop btn-u-lg" Text="Opheffen" Visible="false" />
                        </div>
                    </div>
                </utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_no_found" Visible="false">
                    <utilize:translatetext runat="server" ID="txt_no_found" Text="Er zijn geen orders gevonden met deze zoek waarde"></utilize:translatetext>
                </utilize:placeholder>

                <div class="table-responsive">
                    <table class="overview table">
                        <thead>
                            <tr>
                                <th class="col-md-1">
                                    <utilize:label runat="server" ID="col_order_number" Text="Ordernummer"></utilize:label></th>
                <utilize:pagedrepeater runat="server" ID="rpt_return_orders">
                    <HeaderTemplate>                        
                                <th class="col-md-1">
                                    <utilize:translatelabel runat="server" ID="col_order_date" Text="Datum"></utilize:translatelabel></th>
                                <th class="col-md-3">
                                    <utilize:translatelabel runat="server" ID="col_order_description" Text="Omschrijving"></utilize:translatelabel></th>
                                <th class="text-right">
                                    <utilize:translatelabel runat="server" ID="col_order_amount" Text="Bedrag"></utilize:translatelabel></th>
                            </tr>
                        </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <utilize:translatelinkbutton runat="server" ID="lb_order_nr" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>' CommandName="number" Text='<%# DataBinder.Eval(Container.DataItem, "doc_nr")%>'>  </utilize:translatelinkbutton></td>
                            <td><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                            <td><%# DataBinder.Eval(Container.DataItem, "ord_desc") %></td>
                            <td class="text-right"><utilize:literal runat="server" ID="lt_row_amount" Text='<%# get_order_total(DataBinder.Eval(Container.DataItem, "ord_tot"))%>'></utilize:literal></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <utilize:repeaterpager runat="server" id="rpg_return_orders"></utilize:repeaterpager>
                    </FooterTemplate>
                </utilize:pagedrepeater>
                        </table>                        
                </div>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_no_orders" Visible="false">
                <h1>
                    <utilize:translatetitle runat="server" ID="title_no_returns" Text="Geen bestellingen"></utilize:translatetitle></h1>
                <utilize:translatetext runat="server" ID="text_no_returns" Text="Er zijn geen bestellingen aanwezig in uw account"></utilize:translatetext>
            </utilize:placeholder>

            <utilize:placeholder runat="server" ID="ph_returns_detail" Visible="false">
                <h1>
                    <utilize:translatetitle runat="server" ID="title_return_detail" Text="Retour Aanvraag"></utilize:translatetitle></h1>
                <utilize:translatetext runat="server" ID="text_return_details" Text="Onderstaand vind u een overzicht van de informatie van de geselecteerde order. Vul de extra informatie in voor het aanvragen van een retour. Als een product geselecteerd is zal daar een invulveld verschijnen voor de hoeveelheid die retour gedaan mag worden"></utilize:translatetext>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <asp:HiddenField runat="server" ID="hid_ord_nr" />
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_return_order_nummer" Text="Ordernummer: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-2">
                                <utilize:label runat="server" ID="lbl_order_nummer"></utilize:label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_return_order_date" Text="Orderdatum: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-2">
                                <utilize:label runat="server" ID="lbl_order_date"></utilize:label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_return_order_description" Text="Omschrijving: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-2">
                                <utilize:label runat="server" ID="lbl_order_description"></utilize:label>
                            </div>
                        </div>
                        <utilize:placeholder runat="server" ID="ph_reason_comp_error" Visible="false">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="alert alert-danger">
                                        <utilize:translateliteral runat="server" ID="literal_reason_comp_error" Text="Kies de compensatiemethode van uw voorkeur"></utilize:translateliteral>
                                    </div>
                                </div>
                            </div>
                        </utilize:placeholder>
        <%--                <div class="row margin-bottom-5">
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_general_return_reason" Text="Algemene reden: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-4">
                                <utilize:dropdownlist runat="server" ID="dd_return_reason" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="set_general_reason"></utilize:dropdownlist>
                            </div>
                        </div>--%>
                        <div class="row margin-bottom-5">
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_return_comp" Text="Compensatiemethode: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-4">
                                <utilize:dropdownlist runat="server" ID="dd_return_comp" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="alternative_product"></utilize:dropdownlist>
                            </div>
                        </div>
                        <utilize:placeholder runat="server" ID="ph_reason_error" Visible="false">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="alert alert-danger">
                                        <utilize:translateliteral runat="server" ID="literal_reason_error" Text="U bent vergeten een uitleg van wat er mis is op te geven"></utilize:translateliteral>
                                    </div>
                                </div>
                            </div>
                        </utilize:placeholder>
                        <div class="row margin-bottom-5">
                            <div class="col-md-2">
                                <utilize:translatelabel runat="server" ID="label_return_reason_desc" Text="Opmerkingen: "></utilize:translatelabel>
                            </div>
                            <div class="col-md-4">
                                <utilize:textarea runat="server" ID="txt_return_reason_desc" CssClass="form-control"></utilize:textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_no_items_selected" Visible="false">
                    <div class="row">
                        <div class="alert alert-danger col-md-7">
                            <utilize:translateliteral runat="server" ID="literal_no_items_selected" Text="Er zijn geen producten geselecteerd"></utilize:translateliteral>
                        </div>
                    </div>
                </utilize:placeholder>
                <utilize:placeholder runat="server" ID="ph_amount_error" Visible="false">
                    <div class="row">
                        <div class="alert alert-danger col-md-7">
                            <utilize:translateliteral runat="server" ID="rma_amount_error" Text="U bent een aantal vergeten in te vullen of het getal was te hoog"></utilize:translateliteral>
                        </div>
                    </div>
                </utilize:placeholder>

                <div class="table-responsive">
                    <table class="overview table">
                        <thead>
                            <tr>
                                <th style="width: 3%"><span class="select_all">
                                    <utilize:CheckBox runat="server" ID="check_all" Checked="false" AutoPostBack="true"></utilize:CheckBox></span></th>
                                <th class="col-md-1">
                                    <utilize:translatelabel runat="server" ID="col_prod_code" Text="Productcode"></utilize:translatelabel></th>
                                <th class="col-md-3">
                                    <utilize:translatelabel runat="server" ID="col_prod_desc" Text="productbeschrijving"></utilize:translatelabel></th>
                                <th class="col-md-1 text-center">
                                    <utilize:translatelabel runat="server" ID="col_prod_amt" Text="Aantal"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" ID="ph_amount_left_col" Visible="false">
                                    <th class="col-md-1 text-center">
                                        <utilize:translatelabel runat="server" ID="col_amt_left" Text="Nog te retouneren"></utilize:translatelabel></th>
                                </utilize:placeholder>
                                <th style="width: 15%" class="text-right">
                                    <utilize:translatelabel runat="server" ID="col_row_amount" Text="regelBedrag"></utilize:translatelabel></th>
                                <th style="width: 15%">
                                    <utilize:translatelabel Visible="false" runat="server" ID="col_amount_select" Text="Retourhoeveelheid"></utilize:translatelabel>
                                </th>
                                <th style="width: 15%">
                                    <utilize:translatelabel Visible="false" runat="server" ID="label_return_reason" Text="Reden"></utilize:translatelabel>
                                </th>
                                <th style="width: 3%; text-align: center;">
                                    <utilize:translatelabel Visible="false" runat="server" ID="label_photo_upload" Text="Foto bijvoegen"></utilize:translatelabel></th>
                                <th style="display: none"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <utilize:pagedrepeater runat="server" ID="rpt_return_order_detail" pager_enabled="false">
                                <HeaderTemplate>

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="hiddentext">
                                        <td class="text-center">
                                            <span class="check-box">
                                                <asp:CheckBox runat="server" ID="row_check" Checked="false" AutoPostBack="true" OnCheckedChanged="show_input" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' />
                                            </span>
                                        </td>
                                        <td>
                                            <utilize:label runat="server" ID="row_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label>
                                        </td>
                                        <td>
                                            <utilize:label runat="server" ID="row_prod_desc" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label>
                                        </td>
                                        <td class="text-center">
                                            <utilize:label runat="server" ID="row_ord_qty" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%>'></utilize:label>
                                        </td>
                                        <utilize:placeholder runat="server" ID="ph_amount_left_row" Visible="false">
                                            <td class="text-center">
                                                <utilize:label runat="server" ID="row_amt_left"></utilize:label>
                                            </td>
                                        </utilize:placeholder>
                                        <td class="text-right">
                                            <utilize:label runat="server" ID="row_row_amt" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:label>
                                        </td>
                                        <td>
                                            <utilize:textbox Visible="false" runat="server" ID="row_input_amt" CssClass="form-control" type="number" OnTextChanged="checkAmount" AutoPostBack="true"></utilize:textbox>
                                        </td>
                                        <td>
                                            <utilize:dropdownlist runat="server" ID="dd_return_reason_single" CssClass="form-control" Visible="false"></utilize:dropdownlist>
                                        </td>
                                        <td class="text-center">
                                            <span class="check-box">
                                                <asp:CheckBox Visible="false" runat="server" ID="cb_add_photo" Checked="false" AutoPostBack="true" OnCheckedChanged="show_photo_upload" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' />
                                            </span>
                                        </td>
                                        <td style="display: none;">
                                            <utilize:label runat="server" ID="hid_btw_amt" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "vat_amt")%>'></utilize:label>
                                        </td>
                                        <td style="display: none;">
                                            <utilize:label runat="server" ID="hid_crec_id" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "crec_id")%>'></utilize:label>
                                        </td>
                                    </tr>
                                    <utilize:placeholder runat="server" ID="ph_photo_upload" Visible="false">
                                        <tr>
                                            <td colspan="8" class="upload-td">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <utilize:placeholder runat="server" ID="ph_uploaded_images" Visible="false"></utilize:placeholder>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="dropzone" id="dropzoneForm" runat="server" >
                                                            <div class="dz-default dz-message ">
                                                                <utilize:translatetext runat="server" ID="text_drag_drop" Text="Sleep een bestand hierin of klik hierop om een bestand te uploaden. Maximale grote 10MB"></utilize:translatetext>
                                                            </div>
                                                            <div class="fallback hidden" runat="server">
                                                                <input name="file" type="file" multiple="multiple" />
                                                                <input type="submit" value="Upload" />
                                                            </div>
                                                        </div>
                                                
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </utilize:placeholder>
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                    
                                </FooterTemplate>
                            </utilize:pagedrepeater>
                        </tbody>
                    </table>
                </div>                

                <div class="row">
                    <div class="buttons pull-right margin-top-10">
                        <div class="col-xs-12">
                            <utilize:translatebutton runat="server" ID="button_proceed" Text="Retourverzoek indienen" CssClass="account btn-u btn-u-sea-shop btn-u-lg" Visible="true" />
                        </div>
                    </div>
                </div>

            </utilize:placeholder>            
        </utilize:panel>

        <utilize:panel ID="pnl_bulk_return" Class="tab-pane" runat="server" role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        <utilize:translatetitle runat="server" ID="title_bulk_return" Text="Bulk retourneren"></utilize:translatetitle>
                    </h1>
                    <utilize:translatetext runat="server" ID="text_bulk_return" Text="Onderstaand vind u een zoekveld waarmee u een productcode of een productomschrijving kan opzoeken, hierna komen de producten uit uw bestellingen naar voren die u daarna kan aanmelden voor een retour."></utilize:translatetext>
                </div>
            </div>
            <utilize:placeholder runat="server" ID="ph_bulk_details" Visible="false">
                <div class="bulk-details">
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:alert runat="server" ID="alert_comp_method" AlertType="danger"></utilize:alert>
                            <div class="row margin-bottom-5">
                                <div class="col-md-2">
                                    <utilize:translatelabel runat="server" ID="label_bulk_comp_method" Text="Compensatiemethode: "></utilize:translatelabel>
                                </div>
                                <div class="col-md-4">
                                    <utilize:dropdownlist runat="server" ID="dd_bulk_comp_method" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="alternative_product_bulk"></utilize:dropdownlist>
                                </div>
                            </div>
                            <utilize:alert runat="server" ID="alert_bulk_no_reason" AlertType="danger"></utilize:alert>
                            <div class="row margin-bottom-5">
                                <div class="col-md-2">
                                    <utilize:translatelabel runat="server" ID="label_bulk_reason_desc" Text="Opmerkingen: "></utilize:translatelabel>
                                </div>
                                <div class="col-md-4">
                                    <utilize:textarea runat="server" ID="text_bulk_reason" CssClass="form-control"></utilize:textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        
                            <div class="table-responsive">
                                <table class="overview table">
                                    <thead>
                                        <tr>
                                            <th><utilize:translateliteral runat="server" ID="lt_prod_code_bulk" Text="Productcode"></utilize:translateliteral></th>
                                            <th><utilize:translateliteral runat="server" ID="lt_prod_desc_bulk" Text="Productomschrijving"></utilize:translateliteral></th>
                                            <th><utilize:translateliteral runat="server" ID="lt_ord_nr_bulk" Text="Ordernummer"></utilize:translateliteral></th>                                         
                                            <th style="width: 15%" class="text-right">
                                                <utilize:translatelabel runat="server" ID="label_row_amt_bulk" Text="Regelbedrag"></utilize:translatelabel>
                                            </th>
                                            <th style="width: 15%">
                                                <utilize:translatelabel runat="server" ID="label_amt_bulk" Text="Retourhoeveelheid"></utilize:translatelabel>
                                            </th>
                                            <th style="width: 15%">
                                                <utilize:translatelabel runat="server" ID="label_rsn_bulk" Text="Reden"></utilize:translatelabel>
                                            </th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <utilize:repeater runat="server" ID="rpt_bulk_detail">
                                            <ItemTemplate>
                                                <tr class="hiddentext">                                                
                                                    <td><utilize:label runat="server" ID="row_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label></td>                                            
                                                    <td><utilize:label runat="server" ID="row_prod_desc" text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label></td>                                            
                                                    <td><utilize:label runat="server" ID="row_doc_nr"  text='<%# DataBinder.Eval(Container.DataItem, "doc_nr")%>'></utilize:label></td>                                                                                                                
                                                    <td class="text-right">
                                                        <utilize:label runat="server" ID="row_row_amt" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:label>
                                                    </td>                                                
                                                    <td>
                                                        <utilize:label runat="server" ID="row_return_amount" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "return_qty"), 0) %>'></utilize:label>
                                                    </td>
                                                    <td class="hidden">
                                                        <utilize:label runat="server" ID="row_ord_nr" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ord_nr") %>'></utilize:label>
                                                    </td>
                                                    <td>
                                                        <utilize:label runat="server" id="row_return_reason" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_rma_reason", "reason_desc_" + Me.global_cms.Language, DataBinder.Eval(Container.DataItem, "reason_code")) %>'></utilize:label>
                                                    </td>
                                                    <td>
                                                        <utilize:linkbutton CssClass="link_button_red" runat="server" ID="linkbutton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "prod_code") + "&" + DataBinder.Eval(Container.DataItem, "doc_nr")%>' CommandName="remove" Text="X"></utilize:linkbutton>
                                                    </td>
                                                </tr> 
                                                <utilize:placeholder runat="server" ID="ph_bulk_images" Visible="false">
                                                    <tr>
                                                        <td class="upload-td" colspan="6">
                                                            <utilize:placeholder runat="server" ID="ph_images"></utilize:placeholder>
                                                        </td>
                                                    </tr>
                                                </utilize:placeholder>                                           
                                            </ItemTemplate>
                                        </utilize:repeater>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                    <div class="row button">
                        <div class="col-md-12">
                            <utilize:translatebutton runat="server" ID="button_create_return" Text="Retour aanmaken" CssClass="btn-u btn-u-sea-shop btn-u-lg btn-u-house"/>
                        </div>
                    </div>
                </div>
            </utilize:placeholder>            
            
            <div class="product-selector">
                <div class="row">
                    <div class="col-md-3 margin-bottom-20">
                        <utilize:alert runat="server" ID="alert_no_found" visible="false" AlertType="danger"></utilize:alert>

                        <div class="input-group">
                            <%--<utilize:textbox placeholder="Productcode" runat="server" ID="input_prod_code" CssClass="form-control"></utilize:textbox>--%>
                            <utilize:placeholder runat="server" ID="ph_product_search"></utilize:placeholder>
                            <span class="input-group-btn">
                                <button class="btn btn-default" onclick="return false;"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 margin-bottom-10">
                        <utilize:alert AlertType="danger" Visible="false" runat="server" ID="alert_none_selected"></utilize:alert>
                        <div class="table-responsive">
                            <table class="overview table">
                                <thead>
                                    <tr>
                                        <th style="width: 3%"></th>
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_prod_code" Text="Productcode"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_prod_desc" Text="Product omschrijving"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_order_nr" Text="Ordernummer"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_order_date" Text="Besteldatum"></utilize:translateliteral></th>                                    
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_ordered_amt" Text="Aantal besteld"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="lt_bulk_amt_available" Text="Mogelijk te retouneren"></utilize:translateliteral></th>
                                        <th class="text-right">
                                            <utilize:translatelabel runat="server" ID="label_row_amount" Text="Regelbedrag"></utilize:translatelabel>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:repeater runat="server" ID="rpt_bulk_return">
                                        <ItemTemplate>
                                            <tr class="hiddentext">
                                                <td class="text-center">
                                                    <span class="check-box">
                                                        <asp:CheckBox runat="server" ID="row_check" Checked="false" AutoPostBack="true" OnCheckedChanged="show_bulk_input" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code") + "&" + DataBinder.Eval(Container.DataItem, "doc_nr") %>' />
                                                    </span>
                                                </td>
                                                <td><utilize:label runat="server" ID="row_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label></td>                                            
                                                <td><utilize:label runat="server" ID="row_prod_desc" text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label></td>                                            
                                                <td><utilize:label runat="server" ID="row_doc_nr"  text='<%# DataBinder.Eval(Container.DataItem, "doc_nr")%>'></utilize:label></td>                                            
                                                <td><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>                                                                                        
                                                <td><utilize:label runat="server" ID="row_ord_qty" text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "ord_qty"), 0)%>' ></utilize:label></td>                                            
                                                <td><utilize:label runat="server" ID="row_amt_left" text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "amt_left"), 0)%>'></utilize:label></td>   
                                                <td class="text-right">
                                                    <utilize:label runat="server" ID="row_row_amt" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:label>
                                                </td>
                                                <td style="display: none;">
                                                    <utilize:label runat="server" ID="hid_btw_amt" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "vat_amt")%>'></utilize:label>
                                                </td>
                                                <td style="display: none;">
                                                    <utilize:label runat="server" ID="hid_crec_id" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "line_crec")%>'></utilize:label>
                                                </td>
                                                <td style="display: none;" class="hdr_id">
                                                    <utilize:label runat="server" ID="hid_ord_id" Text='<%# DataBinder.Eval(Container.DataItem, "hdr_id")%>'></utilize:label>
                                                </td>
                                            </tr>
                                            <utilize:placeholder runat="server" ID="ph_return_bulk" Visible="false">
                                                <tr class="hiddentext no-border-top">
                                                    <td></td>
                                                    <td>
                                                        <b><utilize:translatelabel runat="server" ID="label_bulk_return_amount" Text="Retourhoeveelheid"></utilize:translatelabel></b>
                                                    </td>
                                                    <td>
                                                        <utilize:textbox runat="server" ID="row_input_amt" CssClass="form-control" type="number" OnTextChanged="checkBulkAmount" AutoPostBack="true"></utilize:textbox>
                                                    </td>
                                                    <td>
                                                        <b><utilize:translatelabel runat="server" ID="label_bulk_reason" Text="Reden"></utilize:translatelabel></b>
                                                    </td>
                                                    <td colspan="3">
                                                        <utilize:dropdownlist runat="server" ID="dd_return_reason_single" CssClass="form-control"></utilize:dropdownlist>
                                                    </td>                                                
                                                    <td colspan="2">
                                                        <div class="table-checkbox">
                                                            <asp:CheckBox runat="server" ID="cb_add_photo" Checked="false" AutoPostBack="true" OnCheckedChanged="show_photo_upload_bulk" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code") + "&" + DataBinder.Eval(Container.DataItem, "doc_nr") %>' />
                                                        </div>
                                                        <utilize:translatelabel runat="server" ID="label_bulk_photo_upload" Text="Foto bijvoegen"></utilize:translatelabel>
                                                    </td>                                         
                                                </tr>
                                                <utilize:placeholder runat="server" ID="ph_photo_upload" Visible="false">
                                                    <tr>
                                                        <td colspan="8" class="upload-td">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <utilize:placeholder runat="server" ID="ph_uploaded_images" Visible="false"></utilize:placeholder>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="dropzone" id="dropzoneForm" runat="server" >
                                                                        <div class="dz-default dz-message ">
                                                                            <utilize:translatetext runat="server" ID="text_drag_drop" Text="Sleep een bestand hierin of klik hierop om een bestand te uploaden. Maximale grote 10MB"></utilize:translatetext>
                                                                        </div>
                                                                        <div class="fallback hidden" runat="server">
                                                                            <input name="file" type="file" multiple="multiple" />
                                                                            <input type="submit" value="Upload" />
                                                                        </div>
                                                                    </div>
                                                
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </utilize:placeholder>
                                            </utilize:placeholder>
                                        </ItemTemplate>
                                    </asp:repeater>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
                <div class="row margin-bottom-10 button">
                    <div class="col-md-12">
                        <utilize:translatebutton runat="server" ID="button_add1" Text="Toevoegen" CssClass="btn-u btn-u-sea-shop btn-u-lg" />                
                    </div>
                </div>
            </div>
        </utilize:panel>
    </div>

    <utilize:label runat="server" ID="lbl_current_rma_bulk" CssClass="current_rma_bulk"></utilize:label>
    <utilize:label runat="server" ID="lbl_current_rma_code" CssClass="current_rma"></utilize:label>
    
    <utilize:placeholder runat="server" ID="ph_alt_product_selectie" Visible="false">
        <div class="alt_product_selector">            
            <div class="row">
                <div class="col-md-7">
                    <utilize:translatetext runat="server" ID="text_alt_product" Text="Geef hieronder de productcode weer van het alternatieve product wat u wilt"></utilize:translatetext>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-2">
                        <utilize:translatelabel runat="server" ID="lt_prod_code" Text="Product (menuoptie)"></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:placeholder runat="server" ID="ph_product_code_alt"></utilize:placeholder>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2">

                        <utilize:translatelabel runat="server" ID="lt_return_quantity" Text="Hoeveelheid"></utilize:translatelabel>
                    </div>
                    <div class="col-md-4">
                        <utilize:textbox runat="server" ID="text_prod_qty" CssClass="form-control" type="number"></utilize:textbox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <utilize:translatebutton runat="server" ID="button_add" Text="Toevoegen" CssClass="account btn-u btn-u-sea-shop btn-u-lg"></utilize:translatebutton>
                    </div>
                </div>
            </div>

            <utilize:placeholder runat="server" ID="ph_no_alt_products" Visible="false">
                <div class="row">
                    <div class="alert alert-danger col-md-7">
                        <utilize:translateliteral runat="server" ID="lt_no_alt_products" Text="Er zijn geen alternatieve producten aangegeven"></utilize:translateliteral>
                    </div>
                </div>
            </utilize:placeholder>

            <utilize:repeater runat="server" ID="rpt_alt_prod">
                <HeaderTemplate>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="overview table">
                                <thead>
                                    <tr>
                                        <th style="width: 4%"></th>
                                        <th class="col-md-1">
                                            <utilize:translatelabel runat="server" ID="col_prod_code1" Text="Productcode"></utilize:translatelabel></th>
                                        <th class="col-md-3">
                                            <utilize:translatelabel runat="server" ID="col_prod_desc1" Text="Omschrijving"></utilize:translatelabel></th>
                                        <th class="col-md-1 text-center">
                                            <utilize:translatelabel runat="server" ID="col_prod_amt1" Text="Aantal"></utilize:translatelabel></th>
                                        <th style="width: 21%" class="text-right">
                                            <utilize:translatelabel runat="server" ID="col_prod_price1" Text="Prijs"></utilize:translatelabel></th>
                                        <th style="width: 12%" class="text-right">
                                            <utilize:translatelabel runat="server" ID="col_row_amount1" Text="Regel Bedrag"></utilize:translatelabel></th>
                                        <th style="width: 35%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td></td>
                        <td>
                            <utilize:label runat="server" ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>'></utilize:label></td>
                        <td>
                            <utilize:label runat="server" ID="Label2" Text='<%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code")%>'></utilize:label></td>
                        <td class="text-center">
                            <utilize:label runat="server" ID="amount_input" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "prod_qty"), 0)%>'></utilize:label></td>
                        <td class="text-right">
                            <utilize:label runat="server" ID="Label5" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "prod_price"), 2), "c")%>'></utilize:label></td>
                        <td class="text-right">
                            <utilize:label runat="server" ID="row_amount" Text='<%# Format(Decimal.Round(DataBinder.Eval(Container.DataItem, "row_amt"), 2), "c")%>'></utilize:label></td>
                        <td>
                            <utilize:linkbutton CssClass="link_button_red" runat="server" ID="linkbutton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' CommandName="remove" Text="X"></utilize:linkbutton></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                                </table>
                            </div>
                        </div>
                </FooterTemplate>
            </utilize:repeater>
            <utilize:placeholder runat="server" ID="ph_alt_pro_totals" Visible="false">

                <utilize:placeholder runat="server" ID="ph_alt_prod_to_high" Visible="false">
                    <div class="row">
                        <div class="alert alert-danger col-md-7">
                            <utilize:translateliteral runat="server" ID="lt_alt_prod_to_high" Text="Het totale bedrag van de alternatieve producten is te hoog"></utilize:translateliteral>
                        </div>
                    </div>
                </utilize:placeholder>

                <div class="row">
                    <div class="col-md-6 pull-right">
                        <div class="pull-right">
                            <span class="total-price-title">
                                <utilize:translateliteral runat="server" ID="lt_max_amount" Text="Totaal Retouren"></utilize:translateliteral></span>
                            <div class="total-result-in">
                                <span>
                                    <utilize:label runat="server" ID="lbl_max_amount" Text="0.00"></utilize:label></span>
                            </div>
                            <span class="total-price-title">
                                <utilize:translateliteral runat="server" ID="lt_return_total_excl_vat" Text="Totaal Alternatieve producten"></utilize:translateliteral></span>
                            <div class="total-result-in">
                                <span>
                                    <utilize:label runat="server" ID="lbl_order_total_excl_vat" Text="0.00"></utilize:label><utilize:literal runat="server" ID="lt_icon"></utilize:literal></span>
                            </div>
                            <span class="total-price-title">
                                <utilize:translateliteral runat="server" ID="lt_return_amount_left" Text="Restant"></utilize:translateliteral></span>
                            <div class="total-result-in">
                                <span>
                                    <utilize:label runat="server" ID="lbl_amount_left" Text="0.00"></utilize:label></span>
                            </div>
                        </div>
                    </div>
                </div>
            </utilize:placeholder>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_confirmation" Visible="false">
        <h1>
            <utilize:translatetitle runat="server" ID="title_rma_request_confirm" Text="Uw verzoek is ingediend"></utilize:translatetitle>
        </h1>
        <utilize:translatetext runat="server" ID="text_rma_request_confirm" Text="Stuur uw producten A.U.B. nog niet op, maar wacht op de bevestiging"></utilize:translatetext>
    </utilize:placeholder>

    <div class="row buttons">
        <div class="col-md-12">
            <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
            <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" Visible="false" />
            <utilize:translatebutton runat="server" ID="button_bulk_return_close" Text="Sluiten" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" Visible="false"/>
        </div>
    </div>

    <utilize:alert runat="server" ID="message_error" Visible="false" AlertType="danger"></utilize:alert>
    <utilize:button runat="server" ID="button_remove_images" CssClass="hidden remove_images" />
</div>
