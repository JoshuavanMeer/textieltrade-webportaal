﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_users.ascx.vb" Inherits="uc_users" %>
<div class="block account">
    <utilize:placeholder runat='server' ID="ph_users">
        <h1><utilize:translatetitle runat='server' id="title_users" Text="Gebruikers overzicht"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_users" Text="Onderstaand vindt u een overzicht geregistreerde gebruikers."></utilize:translatetext>
        <utilize:placeholder runat="server" ID="ph_search_users" Visible='false'>
            <utilize:translatetext runat='server' ID="text_users_search" Text="Vul hieronder uw criteria in en kies voor zoeken om de zoekopdracht uit te voeren"></utilize:translatetext>
            <utilize:panel runat='server' ID="pnl_search" DefaultButton="button_filter">
                <table class="overview search" >
                    <tr>
                        <td style="width: 25%"><utilize:translatelabel runat='server' ID="label_search_user_name" Text="Naam gebruiker"></utilize:translatelabel></td>
                        <td style="width: 25%"><utilize:textbox runat='server' ID="txt_search_user_name" Width="200px"></utilize:textbox></td>
                        <td style="width: 25%"><utilize:translatelabel runat='server' ID="label_search_user_address" Text="Adres"></utilize:translatelabel></td>
                        <td style="width: 25%"><utilize:textbox runat='server' ID="txt_search_user_address" Width="200px"></utilize:textbox></td>
                    </tr>
                    <tr>
                        <td><utilize:translatelabel runat='server' ID="label_search_user_zip_code" Text="Postcode"></utilize:translatelabel></td>
                        <td><utilize:textbox runat="server" ID="txt_search_user_zip_code" Width="80px"></utilize:textbox></td>
                        <td><utilize:translatelabel runat='server' ID="label_search_user_city" Text="Plaats"></utilize:translatelabel></td>
                        <td><utilize:textbox runat="server" ID="txt_search_user_city" Width="100px"></utilize:textbox></td>
                    </tr>
                </table>                
            </utilize:panel>
            <br style="clear: both;" />
            <div class="buttons">
                <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="btn-u btn-u-sea-shop btn-u-lg" text="Filter opheffen" Visible="false" />
                <utilize:translatebutton runat='server' ID="button_filter" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Filter" />
                <br style="clear: both" />
            </div>
        </utilize:placeholder>
        <div class="buttons">
            <utilize:translatebutton runat='server' ID="button_search" CssClass="btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken" />
            <br style="clear: both" />
        </div>

        <br style="clear: both" />
        <utilize:repeater runat="server" ID="rpt_users" pager_enabled="true" pagertype="bottom" page_size="10">
            <HeaderTemplate>
                <div class="table-responsive">
                <table class="overview table">
                    <thead>
                        <tr class="header">
                            <th><utilize:translatelabel runat='server' ID="col_user_name" Text="Naam gebruiker"></utilize:translatelabel></th>
                            <utilize:placeholder runat="server" ID="ph_email_address" Visible="false">
                                <th><utilize:translatelabel runat='server' ID="col_user_email" Text="Email adres"></utilize:translatelabel></th>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_purchase_combinations" Visible="false">
                                <th><utilize:translatelabel runat='server' ID="col_user_address" Text="Adres"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_user_zip_code" Text="Postcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_user_city" Text="Plaats"></utilize:translatelabel></th>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_credits" Visible="false">
                                <th class="credits"><utilize:translatelabel runat='server' ID="col_user_credits" Text="Credits"></utilize:translatelabel></th>
                            </utilize:placeholder>
                            <th></th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td><%# DataBinder.Eval(Container.DataItem, "fst_name") + " " + DataBinder.Eval(Container.DataItem, "infix") + " " + DataBinder.Eval(Container.DataItem, "lst_name")%></td>
                        <utilize:placeholder runat="server" ID="ph_email_address" Visible="false">
                            <td><%# DataBinder.Eval(Container.DataItem, "email_address")%></td>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_purchase_combinations" Visible="false">
                            <td><utilize:literal runat='server' ID="lt_user_address" Text='<%# DataBinder.Eval(Container.DataItem, "address")%> '></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lt_user_zip_code" Text='<%# DataBinder.Eval(Container.DataItem, "zip_code")%> '></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lt_user_city" Text='<%# DataBinder.Eval(Container.DataItem, "city")%> '></utilize:literal></td>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_credits" Visible="false">
                            <td class="credits"><utilize:literal runat='server' ID="lt_credits" Text='<%# Format(DataBinder.Eval(Container.DataItem, "credits_available"), "N0") %> '></utilize:literal></td>
                        </utilize:placeholder>
                        <td class="change"><utilize:translatelinkbutton runat="server" ID="link_change_user" Text="Wijzigen" CommandName="change" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'></utilize:translatelinkbutton><utilize:translatelinkbutton runat="server" ID="link_select_user" Text="Selecteren" CommandName="select" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rec_id")%>'></utilize:translatelinkbutton></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_users" Visible='false'>
        <h1><utilize:translatetitle runat='server' id="title_no_users" Text="Geen gebruikers"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_users" Text="Er zijn op dit moment geen geregistreerde gebruikers."></utilize:translatetext>
    </utilize:placeholder>  
    <utilize:placeholder runat="server" ID="ph_user_details" Visible="false">
        <h1><utilize:translatetitle runat='server' id="title_change_user" Text="Gebruiker wijzigen" Visible="false"></utilize:translatetitle><utilize:translatetitle runat='server' id="title_add_user" Text="Gebruiker toevoegen" Visible="false"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_change_user" Text="Onderstaand kunt u de gegevens van de gebruiker wijzigen." Visible="false"></utilize:translatetext><utilize:translatetext runat='server' ID="text_add_user" Text="Onderstaand kunt u de gegevens van de gebruiker invullen." Visible="false"></utilize:translatetext>
        <utilize:textbox runat='server' ID="txt_rec_id" Visible='false'></utilize:textbox>
        
        <div class="row input-border">
            <div class="col-md-4 margin-bottom">                    
                <section>
                    <label><utilize:translateliteral runat="server" ID="label_users_fst_name" Text="Voornaam"></utilize:translateliteral><span class="mandatory_field">&nbsp;*</span></label>

                    <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_fst_name" MaxLength="80" CssClass="form-control"></utilize:textbox></label>
                </section>

                <section>
                    <label><utilize:translateliteral runat="server" ID="label_users_infix" Text="Tussenvoegsel"></utilize:translateliteral></label>
                    <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_infix" MaxLength="10"  CssClass="form-control"></utilize:textbox></label>
                </section>

                <section>
                    <label><utilize:translateliteral runat="server" ID="label_users_lst_name" Text="Achternaam"></utilize:translateliteral><span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_lst_name" MaxLength="80" CssClass="form-control"></utilize:textbox></label>
                </section>

                <utilize:placeholder runat="server" ID="ph_address_information" Visible="false">
                    
                        <label><utilize:translatelabel runat='server' ID="label_user_address" Text="Adres"></utilize:translatelabel></label>
                        <utilize:textbox runat='server' ID="txt_user_address" Width="200px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span>
                  
                        <label><utilize:translatelabel runat='server' ID="label_user_zip_code" Text="Postcode"></utilize:translatelabel></label>
                        <utilize:textbox runat="server" ID="txt_user_zip_code" Width="80px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span>
                    
                        <label><utilize:translatelabel runat='server' ID="label_user_city" Text="Plaats"></utilize:translatelabel></label>
                        <utilize:textbox runat="server" ID="txt_user_city" Width="100px"></utilize:textbox><span class="mandatory_field">&nbsp;*</span>
                </utilize:placeholder>
                
                <section>
                    <label><utilize:translateliteral runat="server" ID="label_users_email_address" Text="Email adres"></utilize:translateliteral><span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_email_address" MaxLength="80"  CssClass="form-control"></utilize:textbox></label>
                </section>

                <section>
                    <label><utilize:translateliteral runat="server" ID="label_user_login_code" Text="Login code"></utilize:translateliteral><span class="mandatory_field">&nbsp;*</span></label>
                    <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_login_code" MaxLength="80" CssClass="form-control"></utilize:textbox></label>
                </section>

                <utilize:placeholder runat="server" ID="ph_password" Visible='false'>
                    <section>
                        <label><utilize:translateliteral runat="server" ID="label_users_password" Text="Wachtwoord"></utilize:translateliteral><span class="mandatory_field">&nbsp;*</span></label>
                        <label class="input login-input no-border-top"><utilize:textbox Runat="server" ID="txt_password" MaxLength="80" CssClass="form-control" TextMode="Password"></utilize:textbox></label>
                    </section>
                
                </utilize:placeholder>
            </div>
            <div class="col-md-4">
                <utilize:placeholder runat="server" ID="ph_user_rights">
                        <label class="checkbox"><utilize:checkbox runat="server" ID="chk_rights_order" Text="Bestellen toegestaan" /></label>
                        <label class="checkbox"><utilize:checkbox runat="server" ID="chk_rights_openitems" Text="Openstaande posten bekijken toegestaan" /></label>
                        <label class="checkbox"><utilize:checkbox runat="server" ID="chk_rights_history" Text="Historie bekijken toegestaan" /></label>
                        <label class="checkbox"><utilize:checkbox runat="server" ID="chk_rights_customer_data" Text="Gegevens wijzigen toegestaan" /></label>
                        <label class="checkbox"><utilize:checkbox runat="server" ID="chk_rights_users" Text="Gebruikers aanmaken/wijzigen toegestaan" /></label>
                </utilize:placeholder>
            </div>
        

  
        <div class="buttons col-md-12">
            <utilize:translatebutton runat="server" ID="button_save_user" Text="Gegevens bewaren" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </div>
        <div class="alert alert-danger" runat="server" id="alert_fields_error" visible="false">
            <utilize:translatemessage runat='server' ID="message_mandatory_fields_not_filled" Visible="true" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message"></utilize:translatemessage>
            <utilize:translatemessage runat='server' ID="message_login_code_not_accepted" Visible="false" Text="Uw login code is niet geaccepteerd." CssClass="message"></utilize:translatemessage> 
        </div>
        <br style="clear: both;" />
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_add_user" Text="Gebruiker toevoegen" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>