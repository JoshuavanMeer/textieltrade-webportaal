﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_favorites_overview.ascx.vb" Inherits="uc_favorites_overview" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_order_product.ascx" tagname="uc_order_product" tagprefix="uc2" %>
<%@ Register Src="~/webshop/modules/ProductInformation/uc_customer_product_codes.ascx" tagname="uc_customer_product_codes" tagprefix="uc1" %>

<div class="uc_favorites_overview">
    <h1><utilize:translatetitle runat="server" id="title_favorites_overview" text="Overzicht favoriete producten"></utilize:translatetitle></h1>
    <utilize:placeholder runat="server" ID="ph_product_list">
        <utilize:translatetext runat="server" ID="text_favorites_overview" text="Hieronder vindt u een overzicht van uw favoriete producten"></utilize:translatetext>
                <div class="content">
            <asp:Repeater runat="server" ID="rpt_product_list">
                <ItemTemplate>
                    <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>
                </ItemTemplate>
            </asp:Repeater>
        </div>

    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_no_items">
        <div class="no_products_found">
            <utilize:translatetext runat='server' ID="text_no_favorites" Text="Er zijn geen producten binnen uw favorietenlijst beschikbaar."></utilize:translatetext>
        </div>
    </utilize:placeholder>
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>