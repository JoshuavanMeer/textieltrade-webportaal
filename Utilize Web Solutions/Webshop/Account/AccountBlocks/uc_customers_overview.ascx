﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_customers_overview.ascx.vb" Inherits="uc_sales_customers" %>

<div class="account uc_sales_customers">
    <h1 style="border-bottom:none; padding:0 !important; line-height:normal;"><utilize:translatetitle runat="server" ID="title_customers_overview" Text="Klanten overzicht"></utilize:translatetitle></h1>

    <utilize:modalpanel runat="server" ID="pnl_add_customer" Visible="false">
        <div class="modal-content">
            <div class="modal-header">
                <h2><utilize:translatetitle runat="server" ID="title_add_new_customer" Text="Nieuwe klant toevoegen"></utilize:translatetitle></h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 mb-margin-bottom-30">
                    <div class="block">
                        <utilize:translatetext runat="server" ID="text_add_new_customer" Text="Vul hieronder de factuurgegevens van de klant in."></utilize:translatetext>
                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_new_customer_invoice_address">

                </utilize:placeholder>
                    
                <div class="col-md-12 mb-margin-bottom-30">
                    <div class="block">
                        <h3><utilize:translatetitle runat="server" ID="title_add_customer_delivery_address" Text="Moet de bestelling standaard op een ander adres afgeleverd worden?"></utilize:translatetitle></h3>
                        <utilize:translatetext runat="server" ID="text_add_customer_delivery_address" Text="Wanneer de bestelling standaard op een ander adres afgeleverd moet worden, dan kunt u dat hieronder aangeven."></utilize:translatetext>
                    </div>
                </div>

                <utilize:placeholder runat="server" ID="ph_new_customer_delivery_address">

                </utilize:placeholder>
            </div>
            <div class="modal-footer">
                <utilize:translatebutton runat="server" ID="button_cancel" Text="Annuleren" CssClass="left btn-u btn-u-sea-shop btn-u-lg" /> 
                <utilize:translatebutton runat="server" ID="button_save" Text="Bewaren" CssClass="right btn-u btn-u-sea-shop btn-u-lg" />
            </div>
        </div>
    </utilize:modalpanel>

    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_add_customer" Text="Klant toevoegen" CssClass="pull-right btn-u btn-u-sea-shop btn-u-lg" />
    </div>

    <utilize:placeholder runat="server"  ID="ph_customers" Visible="false">
        <utilize:placeholder runat="server" ID="ph_search_customers" Visible='false'>
                <utilize:translatetext runat='server' ID="text_customers_search" Text="Vul hieronder uw criteria in en kies voor zoeken om de zoekopdracht uit te voeren"></utilize:translatetext>
                <utilize:panel runat='server' ID="pnl_search" DefaultButton="button_filter" CssClass="row margin-bottom-20">
                    <div class="col-md-3">
                            <utilize:translatelabel runat='server' ID="label_customer_number" Text="Klantnummer"></utilize:translatelabel>
                            <utilize:textbox runat='server' ID="txt_av_nr" CssClass="form-control" ></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                            <utilize:translatelabel runat='server' ID="label_customer_name" Text="Naam klant"></utilize:translatelabel>
                            <utilize:textbox runat='server' ID="txt_bus_name" CssClass="form-control"></utilize:textbox>
                    </div>
                    <div class="col-md-3">    
                            <utilize:translatelabel runat='server' ID="label_customer_zip_code" Text="Postcode"></utilize:translatelabel>
                            <utilize:textbox runat="server" ID="txt_zip_code" CssClass="form-control" ></utilize:textbox>
                    </div>
                    <div class="col-md-3">
                            <utilize:translatelabel runat='server' ID="label_customer_city" Text="Plaats"></utilize:translatelabel>
                            <utilize:textbox runat="server" ID="txt_city" CssClass="form-control" ></utilize:textbox>
                    </div>
                </utilize:panel>
                
                <div class="buttons">
                    <utilize:translatebutton runat='server' ID="button_filter_remove" CssClass="left btn-u btn-u-sea-shop btn-u-lg" text="Filter opheffen" Visible="false" />
                    <utilize:translatebutton runat='server' ID="button_filter" CssClass="right btn-u btn-u-sea-shop btn-u-lg" Text="Filter" />
                    <br style="clear: both" />
                </div>
            </utilize:placeholder>
            <div class="buttons">
                 <utilize:translatebutton runat='server' ID="button_search" CssClass="right btn-u btn-u-sea-shop btn-u-lg" Text="Zoeken" />
                <br style="clear: both" />
            </div>
            <br style="clear: both" />

            <div class="overview">
                <utilize:repeater runat="server" id="rpt_customers" pager_enabled="true" pagertype="bottom" page_size="20">
                    <HeaderTemplate>
                        <div class="table-responsive">
                            <table class="overview table">
                                <thead>
                                    <tr class="header">
                                        <th><utilize:translateliteral runat="server" ID="label_customer_number" Text="Klantnummer"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="label_customer_name" Text="Naam klant"></utilize:translateliteral></b></th>
                                        <th><utilize:translateliteral runat="server" ID="label_customer_email_address" Text="E-mail adres"></utilize:translateliteral></b></th>
                                        <th><utilize:translateliteral runat="server" ID="label_customer_zip_code" Text="Postcode"></utilize:translateliteral></th>
                                        <th><utilize:translateliteral runat="server" ID="label_customer_city" Text="Plaats"></utilize:translateliteral></th>
                                        <th></th>
                                    </tr>
                                </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="line">
                            <td><utilize:literal runat='server' ID="lt_customer_number" Text='<%# DataBinder.Eval(Container.DataItem, "av_nr") %>'></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lt_customer_name" Text='<%# DataBinder.Eval(Container.DataItem, "bus_name") %>'></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lb_customer_email_address" Text='<%# DataBinder.Eval(Container.DataItem, "email_address")  %>'></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lt_customer_zipcode" Text='<%# DataBinder.Eval(Container.DataItem, "zip_code")  %>'></utilize:literal></td>
                            <td><utilize:literal runat='server' ID="lt_customer_city" Text='<%# DataBinder.Eval(Container.DataItem, "city") %>'></utilize:literal></td>
                            <td style="padding-top: 5px;"><utilize:translatebutton runat='server' ID="button_login" CssClass="button btn-u btn-u-sea-shop" CommandName="login" /><utilize:literal runat='server' ID="lt_rec_id" Text='<%# DataBinder.Eval(Container.DataItem, "rec_id") %>' Visible="false"></utilize:literal></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </table>
                        </div>
                    </FooterTemplate>       
                </utilize:repeater>
            </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server"  ID="ph_no_customers" Visible="false">
        <utilize:translatetext runat="server" id="text_no_customers" Text="Er zijn geen klanten beschikbaar."></utilize:translatetext>
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>