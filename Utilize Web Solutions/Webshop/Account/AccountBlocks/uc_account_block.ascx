﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_account_block.ascx.vb" Inherits="uc_account_block" %>

<div class="uc_account_block">
    <div class="margin-bottom-10">
        <div class="row">
            <div class="col-md-9">
                <h1>
                    <utilize:literal runat="server" ID="lt_welcome"></utilize:literal>
                </h1>
            </div>
            <div class="col-md-3">
                <utilize:placeholder runat="server" ID="ph_show_prices_option">
                    <span class="checkbox">
                        <utilize:checkbox runat="server" ID="chk_show_prices" AutoPostBack="true" />
                    </span>
                </utilize:placeholder>
                <utilize:placeholder runat="server" ID="ph_use_price_multiplier">
                    <span class="checkbox">
                        <utilize:checkbox runat="server" ID="chk_use_price_multiplier" AutoPostBack="true" />
                    </span>
                    <utilize:placeholder runat="server" ID="ph_multiplier">
                        <div class="multiplier form-inline">
                            <div class="form-group">
                                <label><utilize:translateliteral runat="server" ID="lt_factor" Text="Multiplier"></utilize:translateliteral></label>
                                <utilize:textbox runat="server" ID="txt_price_multiplier" AutoPostBack="true" CssClass="form-control"></utilize:textbox>
                            </div>                        
                        </div>
                    </utilize:placeholder>                    
                </utilize:placeholder>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <utilize:translatetext runat="server" ID="text_account_information" Text="Welkom op onze webshop. Om het bestellen bij ons zo gemakkelijk mogelijk te maken en u inzicht te geven in uw bestellingen hebben wij hieronder diverse opties voor u op een rijtje gezet."></utilize:translatetext>
            </div>
        </div>
    </div>
    <div class="service-box-v1 margin-bottom-15">
        <div class="list md-margin-bottom-40">
            <div class="col-md-12 service-block service-block-default margin-bottom-10">
                <div class="col-md-3">
                    <i class="icon-lg rounded-x icon-line glyphicon glyphicon-shopping-cart"></i>
                    <h2 class="heading-sm">
                        <utilize:translatetitle runat="server" ID="title_account_orders" Text="Bestellingen"></utilize:translatetitle></h2>
                    <p></p>
                </div>
                <div class="col-md-9">
                    <ul class="list-unstyled">
                        <utilize:placeholder runat="server" ID="ph_order_entry" Visible="False">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_order_entry" Text="Snel bestellen"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_order_entry" Text="Kies deze optie om snel producten te bestellen"></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_order_lists" Visible="False">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_order_lists" Text="Bestellijsten"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_order_lists" Text="Kies deze optie om snel producten te bestellen uit een lijst"></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat='server' ID="ph_concept_orders" Visible="false">
                            <li class="text-left second-child">
                                <utilize:translatelink runat='server' ID="link_account_concept_orders_overview" Text="Overzicht concept bestellingen"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_concept_orders_overview" Text="Kies deze optie om een overzicht te bekijken van uw concept bestellingen."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_order_overview" >
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_orders_overview" Text="Overzicht bestellingen"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_orders_overview" Text="Kies deze optie om een overzicht te bekijken van uw geplaatste bestellingen."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>                        

                        <utilize:placeholder runat="server" ID="ph_back_orders" Visible="True">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_backorders_overview" Text="Overzicht backorders"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_backorders_overview" Text="Kies deze optie om een overzicht te bekijken bestelregels die bij ons in backorder staan."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat='server' ID="ph_barcode_scanning">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_barcode_scanning" Text="Barcode scanning"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_barcode_scanning" Text="Kies deze optie om uw winkelwagen te door middel van het scannen van barcodes."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat='server' ID="ph_order_import">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_import_order_lines" Text="Orderregels importeren"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_import_order_lines" Text="Kies deze optie om uw winkelwagen te vullen op basis van een XML bestand of CSV bestand."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat='server' ID="ph_multi_order">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_multi_orders" Text="Meerdere orders importeren"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_multi_orders" Text="Kies deze optie om meerdere orders te importeren op basis van een CSV bestand."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>
                    </ul>
                </div>
            </div>
        </div>
        <utilize:placeholder runat="server" ID="ph_rma">
            <div class="list md-margin-bottom-40">
                <div class="col-md-12 service-block service-block-default margin-bottom-10">
                    <div class="col-md-3">
                        <i class="icon-lg rounded-x icon-line fa fa-reply"></i>
                        <h2 class="heading-sm">
                            <utilize:translatetitle runat="server" ID="title_account_returns" Text="Retouren"></utilize:translatetitle></h2>
                        <p></p>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-unstyled">
                            <utilize:placeholder runat="server" ID="ph_rma_request">
                                <li class="text-left">
                                    <utilize:translatelink runat="server" ID="link_account_rma_return_request" Text="Retourverzoek indienen"></utilize:translatelink>
                                    <div class="sub-text text-left">
                                        <utilize:translatetext runat="server" ID="text_account_return_request" Text="Kies deze optie om een retour aan te vragen"></utilize:translatetext>
                                    </div>
                                </li>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_rma_overview_in_process">
                                <li class="text-left">
                                    <utilize:translatelink runat="server" ID="link_account_rma_returns_in_process" Text="Openstaande retouren"></utilize:translatelink>
                                    <div class="sub-text text-left">
                                        <utilize:translatetext runat="server" ID="text_account_return_overview_in_process" Text="Kies deze optie voor een overzicht van de openstaande retouren"></utilize:translatetext>
                                    </div>
                                </li>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_rma_overview_finish">
                                <li class="text-left">
                                    <utilize:translatelink runat="server" ID="link_account_rma_returns_finished" Text="Afgeronde retouren"></utilize:translatelink>
                                    <div class="sub-text text-left">
                                        <utilize:translatetext runat="server" ID="text_account_return_overview_finished" Text="Kies deze optie voor een overzicht van de afgeronde retouren"></utilize:translatetext>
                                    </div>
                                </li>
                            </utilize:placeholder>

                        </ul>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_history">
            <div class="list md-margin-bottom-40">
                <div class="col-md-12 service-block service-block-default margin-bottom-10 ">
                    <div class="col-md-3">
                        <i class="icon-lg rounded-x icon-line fa fa-book"></i>
                        <h2 class="heading-sm">
                            <utilize:translatetitle runat="server" ID="title_account_history" Text="Historie"></utilize:translatetitle></h2>
                        <p></p>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-unstyled">
                            <utilize:placeholder runat="server" ID="ph_openitems" Visible="True">

                                <li class="text-left ">
                                    <utilize:translatelink runat='server' ID="link_account_open_items" Text="Openstaande posten overzicht"></utilize:translatelink>
                                    <div class="sub-text text-left">
                                        <utilize:translatetext runat="server" ID="text_account_open_items_overview" Text="Hier vindt u een een overzicht van de bestellingen welke nog niet zijn voldaan."></utilize:translatetext>
                                    </div>
                                </li>

                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_order_history" Visible="True">

                                <li class="text-left">
                                    <utilize:translatelink runat='server' ID="link_account_order_history" Text="Bestel historie"></utilize:translatelink>
                                    <div class="sub-text text-left">
                                        <utilize:translatetext runat="server" ID="text_account_order_history" Text="Bekijk hier bestellingen die u in het verleden heeft geplaatst om of kopie facturen op te vragen."></utilize:translatetext>
                                    </div>
                                </li>

                            </utilize:placeholder>
                        </ul>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <div class="list md-margin-bottom-40">
            <div class="col-md-12 service-block service-block-default no-margin-bottom">
                <div class="col-md-3">
                    <i class="icon-lg rounded-x icon-line fa fa-user"></i>
                    <h2 class="heading-sm">
                        <utilize:translatetitle runat="server" ID="title_account_my_account" Text="Mijn account"></utilize:translatetitle></h2>
                    <p></p>
                </div>
                <div class="col-md-9">
                    <ul class="list-unstyled">
                        <utilize:placeholder runat="server" ID="ph_customer_data" Visible="True">

                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_customer_data" Text="Uw gegevens"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_customer_data" Text="Kies deze optie om uw standaard gegevens bekijken en wijzigen, zoals uw standaard factuuradres en afleveradres."></utilize:translatetext>
                                </div>
                            </li>

                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_user_rights" Visible="false">

                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_users" Text="Uw gebruikers"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_users" Text="Kies deze optie om uw gebruikers toe te voegen en/of te wijzigen."></utilize:translatetext>
                                </div>
                            </li>

                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_delivery_addresses" Visible="false">

                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_delivery_addresses" Text="Uw afleveradressen"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_delivery_addresses" Text="Kies deze optie om uw standaard afleveradressen te bekijken en wijzigen."></utilize:translatetext>
                                </div>
                            </li>

                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_project_locations" Visible="false">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_project_locations" Text="Uw project locaties"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_project_locations" Text="Kies deze optie om uw projectlocaties toe te voegen en/of te wijzigen."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>
                        
                        <utilize:placeholder ID="ph_product_favorites" runat="server" Visible="false">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_favorites" Text="Uw favoriete producten"></utilize:translatelink>
                                <div class="text-left sub-text">
                                    <utilize:translatetext runat="server" ID="text_account_favorites" Text="Bekijk hier uw favorieten producten."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_training" Visible="false">
                            <li class="text-left">
                                <utilize:translatelink runat='server' ID="link_account_training" Text="Uw trainingen"></utilize:translatelink>
                                <div class="sub-text text-left">
                                    <utilize:translatetext runat="server" ID="text_account_training" Text="Kies deze optie om uw trainingen te zien."></utilize:translatetext>
                                </div>
                            </li>
                        </utilize:placeholder>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="service-box-v2 row">
        <utilize:placeholder runat="server" ID="ph_change_password">
            <div class="<%=Me.GetColumnSize()%>">
                <div class="service-block-v7" style="margin-top: 30px">
                    <i class="fa fa-lock"></i>
                    <h2>
                        <utilize:translatelink runat='server' ID="link_account_change_password" Text="Wachtwoord wijzigen"></utilize:translatelink></h2>
                    <utilize:translatetext runat="server" ID="text_account_change_password" Text="Kies deze optie om uw wachtwoord te wijzigen."></utilize:translatetext>
                </div>
            </div>
        </utilize:placeholder>        
        <utilize:placeholder runat='server' ID="ph_customers_overview">
            <div class="<%=Me.GetColumnSize()%>">
                <div class="service-block-v7 " style="margin-top: 30px">
                    <i class="fa fa-users"></i>
                    <h2>
                        <utilize:translatelink runat='server' ID="link_account_customers_overview" Text="Klantenoverzicht"></utilize:translatelink></h2>
                    <utilize:translatetext runat="server" ID="text_account_customers_overview" Text="Kies deze optie voor een overzicht van klanten en om namens klanten in te loggen."></utilize:translatetext>
                </div>
            </div>
        </utilize:placeholder>
        <utilize:placeholder runat='server' ID="ph_product_export">
            <div class="<%=Me.GetColumnSize()%>">
                <div class="service-block-v7 " style="margin-top: 30px">
                    <i class="glyphicon glyphicon-export"></i>
                    <h2>
                        <utilize:translatelink runat='server' ID="link_account_export_products" Text="Producten exporteren"></utilize:translatelink></h2>
                    <utilize:translatetext runat="server" ID="text_account_export_products" Text="Kies deze optie voor een export van producten te downloaden in CSV formaat."></utilize:translatetext>
                </div>
            </div>
        </utilize:placeholder>
        <utilize:placeholder runat='server' ID="ph_e_catalogus">
            <div class="<%=Me.GetColumnSize()%>">
                <div class="service-block-v7 " style="margin-top: 30px">
                    <i class="glyphicon glyphicon-book"></i>
                    <h2>
                        <utilize:translatelinkbutton runat='server' ID="link_account_e_catalogus" Text="E-Catalogus" OnClick="link_account_e_catalogus_Click"></utilize:translatelinkbutton></h2>
                    <utilize:translatetext runat="server" ID="text_account_e_catalogus" Text="Kies deze optie voor een catalogus in pdf."></utilize:translatetext>
                </div>
            </div>

            <utilize:modalpanel runat="server" ID="pnl_e_catalogus" Visible="false">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2>
                            <utilize:translatetitle runat="server" ID="title_configure_catalog" Text="Stel uw wensen in voor de catalogus"></utilize:translatetitle></h2>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 mb-margin-bottom-30">
                            <div class="block">
                                <utilize:translatetext runat="server" ID="text_configure_headertext" Text="Vul hier de gewenste bedrijfsnaam of titel in van de catalogus:"></utilize:translatetext>
                                <utilize:textbox runat="server" ID="txtb_configure_headertext"></utilize:textbox>
                            </div>
                        </div>

                        <div class="col-md-12 mb-margin-bottom-30">
                            <div class="block">
                                <br />
                                <utilize:translatetext runat="server" ID="text_configure_categories" Text="Vul hieronder de gewenste categorieen in of laat ze leeg voor alle producten."></utilize:translatetext>
                            </div>
                        </div>

                        <div class="col-md-12 mb-margin-bottom-30">
                            <div class="block">
                                <asp:CheckBoxList runat="server" ID="opt_catalogus_categories" RepeatDirection="Vertical" CssClass="searchtype input-group-addon" RepeatColumns="2" RepeatLayout="Flow"></asp:CheckBoxList>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <utilize:translatebutton runat="server" ID="button_close" Text="Sluiten" CssClass="left btn-u btn-u-sea-shop btn-u-lg" OnClick="button_cancel_Click" />
                        <utilize:translatebutton runat="server" ID="button_generate" Text="Catalogus aanmaken" CssClass="right btn-u btn-u-sea-shop btn-u-lg" OnClick="button_generate_Click" />
                    </div>
                </div>
            </utilize:modalpanel>

        </utilize:placeholder>
    </div>

</div>

