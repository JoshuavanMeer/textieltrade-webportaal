﻿Imports System.Data
Imports System.Linq
Imports System.Web.UI.WebControls

Partial Class uc_order_entry
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control


    <Serializable()>
    Public Class product_code
        Inherits Utilize.Web.UI.WebControls.webcontrol

        Private _product_code As String = ""
        Friend WithEvents _input_control As New Utilize.Web.UI.WebControls.textbox
        Private _input_control_extender As New AjaxControlToolkit.AutoCompleteExtender
        Private _input_javascript As New Utilize.Web.UI.WebControls.literal

        Public customer_id As String = ""
        Public language As String = ""

        Public Sub New()
            Me._input_control_extender.CompletionSetCount = 50
            Me._input_control_extender.MinimumPrefixLength = "2"
            Me._input_control_extender.CompletionInterval = "250"
            Me._input_control_extender.CompletionListCssClass = "autocomplete_completionListElement"
            Me._input_control_extender.CompletionListItemCssClass = "autocomplete_listItem"
            Me._input_control_extender.CompletionListHighlightedItemCssClass = "autocomplete_highlightedListItem"
            Me._input_control_extender.ServiceMethod = "GetItems"
            Me._input_control_extender.FirstRowSelected = False
        End Sub

        Private Sub product_code_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Me.Controls.Add(_input_control)
            Me.Controls.Add(_input_control_extender)
            Me.Controls.Add(_input_javascript)

            Me._input_control.ID = "txt_product_code"
            Me._input_control.AutoPostBack = True
            Me._input_control.CssClass = "form-control"

            Me._input_control_extender.OnClientItemSelected = "product_code_get_code"
            Me._input_control_extender.TargetControlID = "txt_product_code"
            Me._input_control_extender.ID = "ext_product_code"

        End Sub
        Private Sub product_code_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            Me._input_control_extender.ContextKey = Me.customer_id + "|" + Me.language


            Dim lcJavascript As String = ""
            lcJavascript = lcJavascript + "<script type=""text/javascript"" language=""javascript"">" + System.Environment.NewLine()
            lcJavascript = lcJavascript + "function product_code_get_code" + "(source, eventArgs)" + System.Environment.NewLine()
            lcJavascript = lcJavascript + " {" + System.Environment.NewLine()
            lcJavascript = lcJavascript + "     var lnValue = eventArgs.get_value();" + System.Environment.NewLine()
            lcJavascript = lcJavascript + "     if (lnValue == null){} else {"
            lcJavascript = lcJavascript + "     document.getElementById(""" + Me._input_control.ClientID + """).value = lnValue.substr(0, 20).trim();" + System.Environment.NewLine()
            lcJavascript = lcJavascript + "     document.getElementById(""" + Me._input_control.ClientID + """).blur();}" + System.Environment.NewLine()
            lcJavascript = lcJavascript + " }" + System.Environment.NewLine()
            lcJavascript = lcJavascript + "</script>" + System.Environment.NewLine()

            _input_javascript.Text = lcJavascript

            Me._input_control.Width = 120
            Me._input_control.Style.Add("text-transform", "uppercase")
        End Sub

        Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Public Property Text As String
            Get
                Return Me._input_control.Text
            End Get
            Set(ByVal value As String)
                Me._input_control.Text = value.ToUpper()
            End Set
        End Property

        Protected Sub _input_control_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _input_control.TextChanged
            RaiseEvent TextChanged(sender, e)
        End Sub
        Public Overrides Sub Focus()
            Me._input_control.Focus()
        End Sub
    End Class

    Private WithEvents txt_product_code As New product_code
    Private WithEvents uc_order_stock As Utilize.Web.Solutions.Webshop.ws_user_control = Nothing
    Private _sizeview_active As Boolean = False
    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
    Private uc_order_product As Webshop.ws_user_control = Nothing
    Private uc_shopping_cart_overview As Webshop.ws_user_control = Nothing
    Private uc_shopping_cart_totals As Webshop.ws_user_control = Nothing

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if order entry module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_entry") Then
            Response.Redirect("~/home.aspx", True)
        End If

        Me.ph_product_code.Controls.Add(txt_product_code)
        txt_product_code.customer_id = Me.global_ws.user_information.user_customer_id
        txt_product_code.language = Me.global_ws.Language
        txt_product_code.ID = "product_code"
        txt_product_code.Focus()

        ' Load the shopping cart overview user control and add it to the page in a placeholder
        uc_shopping_cart_overview = LoadUserControl("~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_overview.ascx")
        uc_shopping_cart_overview.set_property("show_delete_item", True)
        uc_shopping_cart_overview.set_property("show_quantity_box", False)
        uc_shopping_cart_overview.set_property("order_entry", True)
        Me.ph_shopping_cart_overview.Controls.Add(uc_shopping_cart_overview)

        ' Load the shopping cart totals user control and add it to the page in a placeholder
        uc_shopping_cart_totals = LoadUserControl("~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_totals.ascx")
        Me.ph_shopping_cart_totals.Controls.Add(uc_shopping_cart_totals)

        ' Load the order product user control and add it to the page in a placeholder
        uc_order_product = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
        If Me.IsPostBack() Then
            uc_order_product.set_property("product_code", Request.Form(txt_product_code._input_control.UniqueID))
        End If
        ph_order_product.Controls.Add(uc_order_product)

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        ' Laad hier de usercontrol in en voeg deze toe aan de placeholder
        uc_order_stock = LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_stock.ascx")
        Me.ph_stock_control.Controls.Add(uc_order_stock)

        ' Zet hier de regionale variabele om te controleren of sizeview actief is.
        Me._sizeview_active = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview") = True

        Me.set_style_sheet("uc_order_entry.css", Me)
        Me.block_css = False
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.button_shopping_cart.Visible = Me.global_ws.shopping_cart.product_count > 0
        Me.button_shopping_cart.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx") + "'; return false;")
        Me.button_shopping_cart.Style.Add("float", "right")

        Dim rpt_shopping_cart As Repeater = uc_shopping_cart_overview.FindControl("rpt_shopping_cart")
        rpt_shopping_cart.DataSource = Me.global_ws.shopping_cart.product_table
        rpt_shopping_cart.DataBind()

        ' Zorg hier dat een en ander zichtbaar wordt obv instellingen
        Dim ln_stock_show As Integer = Me.global_ws.get_webshop_setting("show_stock")

        If ln_stock_show = 3 Then
            Me.ph_stock_control.Visible = False
            Me.ph_stock.Visible = False
        Else
            Me.ph_stock_control.Visible = True
            Me.ph_stock.Visible = True
        End If

        ' Maak hier de placeholders zichtbaar obv de module actief/inactief
        Me.ph_sizes_title.Visible = Me._sizeview_active
        Me.ph_sizes.Visible = Me._sizeview_active

        Me.lt_product_price.Visible = Not prices_not_visible
        Me.label_product_price.Visible = Not prices_not_visible

        If Not Me.txt_product_code.Text = "" Then
            Me.uc_order_stock.set_property("Visible", True)
            Me.uc_order_stock.set_property("product_code", Me.txt_product_code.Text)
            Me.uc_order_stock.set_property("size", Me.ddl_sizes.SelectedValue)

            Me.uc_order_product.Visible = True
        Else
            Me.uc_order_product.Visible = False
        End If

        uc_order_product.FindControl("button_order").Visible = False
        uc_order_product.FindControl("button_order1").Visible = False
        uc_order_product.FindControl("button_order2").Visible = False
    End Sub

    Private Function get_sizes(ByVal product_code As String) As DataTable
        Dim lc_sizebar_code As String = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_code", product_code, "prod_code")

        ' Maak een class met maten aan
        ' Zet een filter
        Dim qsc_sizes As New Utilize.Data.QuerySelectClass
        With qsc_sizes
            .select_fields = "size_code, size_desc_" + Me.global_ws.Language + ", row_ord"
            .select_from = "uws_sizeview_sizes"
            .select_where = "sizebar_code = @sizebar_code and not size_code in (select size_code from uws_sizeview_sizepro where prod_code = @prod_code and blocked = 1)"
            .select_order = "row_ord"
        End With
        qsc_sizes.add_parameter("sizebar_code", lc_sizebar_code)
        qsc_sizes.add_parameter("prod_code", product_code)

        Dim dt_sizes As DataTable = qsc_sizes.execute_query()

        Return dt_sizes
    End Function

    Protected Sub txt_product_code_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_product_code.TextChanged
        Dim lc_product_description As String = ""

        Dim ll_resume As Boolean = True

        Me.label_error_message.Visible = False
        Me.txt_product_code.Text = Me.txt_product_code.Text.ToUpper()

        If ll_resume Then
            ll_resume = Not Me.txt_product_code.Text = ""

            If Not ll_resume Then
                Me.txt_product_code.Text = ""
                Me.lt_product_description.Text = ""
                Me.lt_product_price.Text = ""
                Me.txt_product_code.Focus()
            End If
        End If

        If ll_resume Then
            ll_resume = CheckProduct(txt_product_code.Text)

            If Not ll_resume Then
                Me.txt_product_code.Text = ""
                Me.lt_product_description.Text = ""
                Me.lt_product_price.Text = ""
                Me.txt_product_code.Focus()

                Me.label_error_message.Text = global_trans.translate_message("message_invalid_product_code", "De ingevoerde productcode is ongeldig.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        If ll_resume Then
            lc_product_description = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", Me.txt_product_code.Text + Me.global_ws.Language, "prod_code + lng_code")

            ll_resume = Not lc_product_description.Trim() = ""

            If Not ll_resume Then
                Me.txt_product_code.Text = ""
                Me.lt_product_description.Text = ""
                Me.lt_product_price.Text = ""
                Me.txt_product_code.Focus()

                Me.label_error_message.Text = global_trans.translate_message("message_invalid_product_code", "De ingevoerde productcode is ongeldig.", Me.global_ws.Language)
                Me.label_error_message.Visible = True
            End If
        End If

        If ll_resume And Me._sizeview_active = True Then
            ' Als de module sizeview actief is, dan moet een datatable gevuld worden
            Dim ll_sizeview_product As Boolean = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_article", Me.txt_product_code.Text.Trim().ToUpper()) = True

            If ll_sizeview_product Then
                ' Het is een sizeview product, haal de maten op
                Dim dt_sizes As System.Data.DataTable = Nothing

                ' Als het goed gaat, dan gaan we de maten ophalen
                dt_sizes = Me.get_sizes(Me.txt_product_code.Text.Trim())

                Me.ddl_sizes.DataSource = dt_sizes
                Me.ddl_sizes.DataTextField = "size_desc_" + Me.global_ws.Language
                Me.ddl_sizes.DataValueField = "size_code"
                Me.ddl_sizes.DataBind()

                ' Maak hier de uitklaplijst zichtbaar/onzichtbaar obv het aantal regels
                Me.ddl_sizes.Visible = dt_sizes.Rows.Count > 0
            Else
                Me.ddl_sizes.Visible = False
            End If
        End If

        If ll_resume Then
            Me.lt_product_price.Text = Format(Me.global_ws.get_product_price(Me.txt_product_code.Text, ""), "c")
        End If

        If ll_resume Then
            Me.uc_order_stock.set_property("Visible", True)
            Me.uc_order_stock.set_property("product_code", Me.txt_product_code.Text)
            Me.uc_order_stock.set_property("size", Me.ddl_sizes.SelectedValue)
            Me.uc_order_product.set_property("textboxvalue", Decimal.Parse("1"))

            Me.lt_product_description.Text = lc_product_description

            Me.uc_order_product.Focus()
        End If

        Me.button_add_to_cart.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", Me.txt_product_code.Text) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", Me.txt_product_code.Text, "mainitemmatrix") = Me.txt_product_code.Text)
    End Sub

    Private Function CheckProduct(text As String) As Boolean
        Dim qsc_products As New Utilize.Data.QuerySelectClass
        With qsc_products
            .select_fields = "prod_code"
            .select_from = "uws_products"
            .select_where = "prod_show = 1 and prod_code = @prod_code"
        End With

        qsc_products.add_parameter("prod_code", text)

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cat_excl") = True Then
            Dim userCategoriesModel = New Utilize.Data.API.Webshop.UserCategoriesModel With {
                .Language = Me.global_ws.Language,
                .ShopCode = Me.global_ws.shop.get_shop_code,
                .UserId = Me.global_ws.user_information.user_id,
                .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                .UserInformation = Me.global_ws.user_information.ubo_customer,
                .CatParent = "",
                .GetAllCategories = True
        }
            Dim dt_data_Table = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)

            Dim lc_category_string As String = ""
            dt_data_Table.AsEnumerable.ToList.ForEach(Sub(x) lc_category_string += ",'" + x.Item("cat_code") + "'")
            lc_category_string = lc_category_string.Substring(1)

            qsc_products.select_where += " and uws_products.prod_code in (select prod_code from uws_categories_prod where cat_code in (" + lc_category_string + "))"
        End If

        Return qsc_products.execute_query.Rows.Count > 0
    End Function

    Protected Sub button_add_to_cart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_to_cart.Click
        Dim ll_resume As Boolean = True
        Dim ln_order_quantity As Integer = 0

        Me.label_error_message.Visible = False

        If ll_resume Then
            ll_resume = Me.order_product()
        End If

        If ll_resume Then
            Me.lt_product_description.Text = ""
            Me.lt_product_price.Text = ""
            Me.txt_product_code.Text = ""
            Me.uc_order_product.set_property("Value", Decimal.Parse("1"))

            Me.ddl_sizes.Visible = False

            Me.txt_product_code.Focus()
        End If
    End Sub

    Protected Sub ddl_sizes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_sizes.SelectedIndexChanged
        ' Zet hier de nieuwe properties voor de voorraad waarden
        Me.uc_order_stock.set_property("Visible", True)
        Me.uc_order_stock.set_property("product_code", Me.txt_product_code.Text)
        Me.uc_order_stock.set_property("size", Me.ddl_sizes.SelectedValue)

        Me.txt_product_code.Focus()
    End Sub

    Public Function order_product() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim method As System.Reflection.MethodInfo
            Dim mytype As System.Type = uc_order_product.GetType()
            method = mytype.GetMethod("order_products")

            If Not method Is Nothing Then
                ll_resume = method.Invoke(uc_order_product, Nothing)
            End If
        End If

        Return ll_resume
    End Function
End Class
