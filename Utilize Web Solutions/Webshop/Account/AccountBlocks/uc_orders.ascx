﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_orders.ascx.vb" Inherits="Webshop_Account_AccountBlocks_uc_orders" %>
<%@ Register Src="~/CMS/Modules/Text/uc_text_block.ascx" TagName="uc_text_block" tagprefix="uc" %>
 
 <div class="block account uc_orders">
    <utilize:placeholder runat='server' ID="ph_orders">
        <h1><utilize:translatetitle runat='server' id="title_orders_overview" text="Overzicht bestellingen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_orders_overview" Text="Onderstaand vindt u een overzicht van bestellingen die bij ons in behandeling zijn. Klik op het ordernummer om een overzicht van de bestelling te bekijken."></utilize:translatetext>
        <div class="buttons margin-bottom-10">
            <utilize:translatebutton runat="server" id="button_order_report" text="Bestellingen rapport" cssclass="right order_report btn-u btn-u-sea-shop btn-u-lg"></utilize:translatebutton>
        </div>
        <div class="overview">
        <utilize:repeater runat="server" ID="rpt_orders" pager_enabled="true" page_size="20" pagertype="bottom">
            <HeaderTemplate>
                <div class="table-responsive">
                <table class="overview table"> 
                    <thead>
                        <tr class="header">
                            <th class="number"><utilize:translatelabel runat='server' ID="col_order_number" Text="Ordernummer"></utilize:translatelabel></th>
                            <th class="date"><utilize:translatelabel runat='server' ID="col_order_date" Text="Datum"></utilize:translatelabel></th>
                                <th class="description"><utilize:translatelabel runat='server' ID="col_order_description" Text="Omschrijving"></utilize:translatelabel></th>

                            <th class="amount text-right"><utilize:translatelabel runat='server' ID="col_order_amount" Text="Bedrag"></utilize:translatelabel></th>
                            <utilize:placeholder runat='server' ID="ph_confirmation" Visible="false">
                                <th class="number"></th>
                            </utilize:placeholder>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td class="number"><utilize:linkbutton runat="server" ID="lb_order_details" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ord_nr")%>' CommandName="details"><%# DataBinder.Eval(Container.DataItem, "av_ord_nr")%></utilize:linkbutton></td>
                        <td class="date"><%# Format(DataBinder.Eval(Container.DataItem, "ord_date"), "d")%></td>
                            <td class="description"><%# DataBinder.Eval(Container.DataItem, "order_description") %></td>
                        <td class="amount text-right"><utilize:literal runat="server" ID="lt_row_amount"></utilize:literal></td>
                        <utilize:placeholder runat='server' ID="ph_confirmation" Visible="false">
                            <td class="number" style="text-align: center"><utilize:imagebutton OnClientClick='<%# "window.open(""OpenDocument.aspx?DocType=confirmation&DocNr=" + DataBinder.Eval(Container.DataItem, "ord_nr") + """, """", ""width=1000, height=800"");return false;" %>' runat="server" ID="button_order_confirmation" Visible='<%# Not DataBinder.Eval(Container.DataItem, "order_confirmation") = "" %>' ImageUrl='<%# Me.ResolveCustomUrl("~/DefaultImages/Email.png") %>'></utilize:imagebutton></td>
                        </utilize:placeholder>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
        </div>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_no_orders" Visible='false'>
        <h1><utilize:translatetitle runat='server' id="title_no_orders" text="Geen lopende bestellingen"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_no_orders" Text="Er zijn bij ons op dit moment geen bestellingen van u in behandeling."></utilize:translatetext>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_order_details" Visible="false">
        <h1><utilize:translatetitle runat='server' id="title_order_detail" text="Overzicht bestelling"></utilize:translatetitle></h1>
        <utilize:translatetext runat='server' ID="text_order_details" Text="Onderstaand vind u het overzicht van de door u geplaatste bestelling."></utilize:translatetext>
        <div class="row margin-bottom-20"> 
           <div class="col-md-12">
               <div class="row">
                    <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_number" Text="Ordernummer"></utilize:translatelabel></div>
                    <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_number"></utilize:label></div>
                </div>
               <div class="row">
                    <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_date" Text="Orderdatum"></utilize:translatelabel></div>
                    <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_date"></utilize:label></div>
               </div>
               <utilize:placeholder runat="server" ID="ph_used_coupon_code">
                    <div class="row">
                        <div class="col-md-2"><utilize:translatelabel runat='server' ID="lt_used_coupon_code" Text="Gebruikte coupon code"></utilize:translatelabel></div>
                        <div class="col-md-2"><utilize:label runat='server' ID="lit_used_coupon_code"></utilize:label></div>
                   </div>
               </utilize:placeholder>               
               <div class="row description">
                    <div class="col-md-2"><utilize:translatelabel runat='server' ID="label_order_description" Text="Omschrijving"></utilize:translatelabel></div>
                    <div class="col-md-2"><utilize:label runat='server' ID="lbl_order_description"></utilize:label></div>
                </div>
            </div>
        </div>
        <utilize:repeater runat="server" ID="rpt_order_details">
            <HeaderTemplate>
                <div class="table-responsive">
                    <table class="lines table">
                        <thead>
                            <tr class="header">
                                <th class="code"><utilize:translatelabel runat='server' ID="col_product_code" Text="Productcode"></utilize:translatelabel></th>
                                <th><utilize:translatelabel runat='server' ID="col_product_description" Text="Omschrijving"></utilize:translatelabel></th>

                                <utilize:placeholder runat='server' ID="ph_size" Visible="false">
                                    <th class="number"><utilize:translatelabel runat='server' ID="col_size" Text="Maat"></utilize:translatelabel></th>
                                </utilize:placeholder>

                                <th class="quantity"><utilize:translatelabel runat='server' ID="col_order_quantity" Text="Aantal"></utilize:translatelabel></th>
                                <utilize:placeholder runat="server" ID="ph_backorder_quantity">
                                    <th class="quantity"><utilize:translatelabel runat='server' ID="col_backorder_quantity" Text="Aantal in backorder"></utilize:translatelabel></th>
                                </utilize:placeholder>
                                <th class="amount text-right"><utilize:translatelabel runat='server' ID="col_row_amount" Text="Regelbedrag"></utilize:translatelabel></th>
                            </tr>
                        </thead>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="line">
                        <td class="code"><%# DataBinder.Eval(Container.DataItem, "prod_code")%></td>
                        <td>
                            <%# Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", DataBinder.Eval(Container.DataItem, "prod_code") + Me.global_ws.Language, "prod_code + lng_code") %>
                                <utilize:placeholder  Visible='<%# DataBinder.Eval(Container.DataItem, "project_location") <> ""%>' runat="server" ID="ph_project_location">
                                        <br /><utilize:translatelabel runat="server" ID="label_project_location" Text="Project locatie"></utilize:translatelabel><utilize:literal runat='server' ID="lt_project_location" Text='<%# ": " + Utilize.Data.DataProcedures.GetValue("uws_project_location", "location_name", DataBinder.Eval(Container.DataItem, "project_location")) %>'></utilize:literal>
                                </utilize:placeholder>
                                <utilize:placeholder runat='server' ID="ph_row_employee" Visible='<%# DataBinder.Eval(Container.DataItem, "user_id") <> ""%>'>
                                    <div class="row_comments">
                                        <utilize:translateliteral runat="server" ID="label_ordered_for_employee" Text="Besteld voor medewerker"></utilize:translateliteral>:&nbsp;<utilize:literal runat="server" ID="lt_employee" Text='<%# Me.get_full_name_for_user_id(DataBinder.Eval(Container.DataItem, "user_id"))%>'></utilize:literal>
                                    </div>
                                </utilize:placeholder>
                        </td>

                        <utilize:placeholder runat='server' ID="ph_size" Visible="false">
                            <td class="quantity"><%# DataBinder.Eval(Container.DataItem, "size")%></td>
                        </utilize:placeholder>


                        <td class="quantity"><utilize:literal runat="server" ID="lt_ord_qty"></utilize:literal></td>
                        <utilize:placeholder runat="server" ID="ph_backorder_quantity">
                            <td class="quantity"><utilize:literal runat="server" ID="lt_back_qty"></utilize:literal></td>
                        </utilize:placeholder>
                        <td class="amount text-right"><utilize:literal runat="server" ID="lt_row_amount"></utilize:literal></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>                                    
        </utilize:repeater>
        <utilize:placeholder runat="server" ID="ph_prices_vat_excl">
            <div class="col-md-4 pull-right shopping-cart">
                <div class="row">
                    <ul class="list-inline total-result">
                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_subtotal_excl_vat" Text="Subtotaal excl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_subtotal_excl_vat"></utilize:literal></span>
                        </div>    
                    </li>

                    <li>
                        <utilize:placeholder runat="server" ID="ph_order_costs_excl_vat">
                            <h4><utilize:translateliteral runat="server" ID="totals_order_costs_excl_vat" Text="Orderkosten excl. BTW"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span><utilize:literal runat="server" ID="lt_order_costs_excl_vat"></utilize:literal></span>
                            </div>    
                        </utilize:placeholder>
                    </li>

                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_shipping_costs_excl_vat" Text="Verzendkosten excl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_shipping_costs_excl_vat"></utilize:literal></span>
                        </div>    
                    </li>

                    <utilize:placeholder runat='server' ID="ph_pay_disc_amt_excl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_pay_disc_amt_excl" Text="Betalingskorting"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_pay_disc_amt_excl"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_coupon_excl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_coupon_excl" Text="Couponkorting"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_coupon_excl"></utilize:literal></span>
                            </div>    
                        </li>
                    </utilize:placeholder>
                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_total_vat_amount" Text="BTW Bedrag"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_vat_total"></utilize:literal></span>
                        </div>    
                    </li>

                    <li>
                        <h4><utilize:translateliteral runat="server" ID="totals_total_incl_vat" Text="Totaal incl. BTW"></utilize:translateliteral></h4>
                        <div class="total-result-in">
                            <span><utilize:literal runat="server" ID="lt_order_total_incl_vat"></utilize:literal></span>
                        </div>    
                    </li>
                </ul>
            </div>
        </div>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prices_vat_incl">
            <div class="col-md-4 pull-right shopping-cart">
                <div class="row">
                    <ul class="list-inline total-result">
                         <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_subtotal_incl_vat" Text="Subtotaal incl. BTW"></utilize:translateliteral></h4>
                             <div class="total-result-in">
                                <span><utilize:literal runat="server" ID="lt_order_subtotal_incl_vat"></utilize:literal></span>
                            </div>
                        </li>
                        <utilize:placeholder runat="server" ID="ph_order_costs_incl_vat">
                            <li>
                                <h4><utilize:translateliteral runat="server" ID="totals_order_costs_incl_vat" Text="Orderkosten incl. BTW"></utilize:translateliteral></h4>
                                <div class="total-result-in">
                                    <span><utilize:literal runat="server" ID="lt_order_costs_incl_vat"></utilize:literal></span>
                                </div>
                            </li>
                        </utilize:placeholder>
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_shipping_costs_incl_vat" Text="Verzendkosten incl. BTW"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span><utilize:literal runat="server" ID="lt_order_shipping_costs_incl_vat"></utilize:literal></span>
                            </div>
                        </li>
                        <utilize:placeholder runat='server' ID="ph_pay_disc_amt_incl">
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_pay_disc_amt_incl" Text="Betalingskorting"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span>-<utilize:literal runat="server" ID="lt_pay_disc_amt_incl"></utilize:literal></span>
                            </div>    
                        </li>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_coupon_incl">
                            <li>
                                <h4><utilize:translateliteral runat="server" ID="totals_coupon_incl" Text="Couponkorting"></utilize:translateliteral></h4>
                                <div class="total-result-in">
                                    <span>-<utilize:literal runat="server" ID="lt_coupon_incl"></utilize:literal></span>
                                </div>    
                            </li>
                        </utilize:placeholder>
                        <li>
                            <h4><utilize:translateliteral runat="server" ID="totals_total_incl_vat1" Text="Totaal incl. BTW"></utilize:translateliteral></h4>
                            <div class="total-result-in">
                                <span><utilize:literal runat="server" ID="lt_order_total_incl_vat1"></utilize:literal></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </utilize:placeholder>
    </utilize:placeholder>
    <br style="clear: both;" />
    <div class="buttons">
        <utilize:translatebutton runat="server" ID="button_my_account" Text="Terug naar Account" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        <utilize:translatebutton runat="server" ID="button_close_details" Text="Details sluiten" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" />
    </div>
</div>