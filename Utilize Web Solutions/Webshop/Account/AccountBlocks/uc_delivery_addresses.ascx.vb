﻿
Partial Class Webshop_Account_AccountBlocks_uc_orders
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control


    Private Function get_addresses() As System.Data.DataTable
        Dim udt_addresses As New Utilize.Data.DataTable
        udt_addresses.table_name = "uws_customers_addr"
        udt_addresses.table_init()

        udt_addresses.add_where("and cust_id = @cust_id")
        udt_addresses.add_where_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

        udt_addresses.table_load()

        Return udt_addresses.data_table
    End Function

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.rpt_delivery_addresses.DataSource = Me.get_addresses()
        Me.rpt_delivery_addresses.DataBind()

        Me.set_style_sheet("uc_delivery_addresses.css", Me)
        Me.block_css = False

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ph_delivery_addresses.Visible = Me.rpt_delivery_addresses.Items.Count > 0
        Me.ph_no_delivery_addresses.Visible = Me.rpt_delivery_addresses.Items.Count = 0

        Me.button_add_delivery_address.Visible = True

        If Me.IsPostBack() Then
            rpt_delivery_addresses.DataSource = Me.get_addresses()
            rpt_delivery_addresses.DataBind()
        End If
    End Sub

    Protected Sub rpt_delivery_addresses_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_delivery_addresses.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Me.uc_change_address_block.SetDisplayedCustomerAddress(e.CommandArgument)
                pnl_overlay_delivery.Visible = True
            Case "Delete"
                ' Hier moeten we een nieuw business object aanmaken want het kunnen beschikbare of gewijzigde gegevens zijn.
                Dim ubo_customer_address As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

                Dim ll_testing As Boolean = True

                If ll_testing Then
                    ll_testing = ubo_customer_address.data_tables("uws_customers").data_row_count = 1

                    If Not ll_testing Then

                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_customer_address.table_locate("uws_customers_addr", "rec_id = '" & e.CommandArgument & "'")

                    If Not ll_testing Then

                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_customer_address.table_delete_row("uws_customers_addr", False, Me.global_ws.user_information.user_customer_id)

                    If Not ll_testing Then

                    End If
                End If

                If ll_testing Then
                    ll_testing = ubo_customer_address.table_update(Me.global_ws.user_information.user_id)

                End If

                rpt_delivery_addresses.DataSource = Me.get_addresses()
                rpt_delivery_addresses.DataBind()
            Case Else
                Exit Select
        End Select
    End Sub

    Protected Sub button_change_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_change3.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Me.uc_change_address_block.check_address()
        End If

        If ll_resume Then
            ll_resume = Me.uc_change_address_block.save_address()
        End If

        If ll_resume Then
            Me.pnl_overlay_delivery.Visible = False
        End If

        rpt_delivery_addresses.DataSource = Me.get_addresses()
        rpt_delivery_addresses.DataBind()
    End Sub
    Protected Sub button_cancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_cancel1.Click
        Me.pnl_overlay_delivery.Visible = False
    End Sub

    Protected Sub button_add_delivery_address_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_delivery_address.Click
        uc_change_address_block.clear_fields()

        Me.pnl_overlay_delivery.Visible = True
    End Sub
End Class
