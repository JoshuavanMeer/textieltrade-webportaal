﻿<%@ Control Language="VB" AutoEventWireup="true" CodeFile="uc_import_multi_orders.ascx.vb" Inherits="Bespoke_Account_AccountBlocks_uc_multi_orders" %>
<asp:Button ID="dummybtnPivotGridupdate" runat="server" Style="display: none;" />

<div class="block account">
    <utilize:placeholder runat='server' ID="ph_multi_orders">
        <h1>
            <utilize:translatetitle runat='server' ID="title_multi_orders" Text="Meerdere orders importeren"></utilize:translatetitle></h1>
        <form onsubmit="dropzone.processQueue();return false;">
            <div class="dropzone" id="dropzoneForm" runat="server">
                <div class="dz-default dz-message ">
                    <utilize:translatetext runat="server" ID="text_drag_drop_csv" Text="Sleep een gevuld CSV bestand hierin of klik hierop om een bestand te uploaden. Maximale grote 10MB"></utilize:translatetext>
                </div>
                <div class="fallback hidden" runat="server">
                    <input name="file" type="file" multiple="multiple" />
                    <input type="submit" value="Upload" />
                </div>

            </div>
            <utilize:alert AlertType="danger" runat="server" ID="label_error_message" CssClass="message"></utilize:alert>

            <div class="buttons margin-top-20">
                <utilize:translatebutton runat="server" ID="button_import" Text="Importeren" CssClass="right account btn-u btn-u-sea-shop btn-u-lg" UseSubmitBehavior="true" />
            </div>
        </form>
        <div class="buttons margin-top-20">
            <utilize:translatebutton runat="server" ID="button_export_template" Text="Export voorbeeld CSV" CssClass="left account btn-u btn-u-sea-shop btn-u-lg" />
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_progress">
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_result" Visible="false">
        <h1>
            <utilize:translatetitle runat="server" ID="title_files_processed" Text="De bestanden zijn verwerkt"></utilize:translatetitle></h1>

        <utilize:panel runat="server" ID="pnl_success">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <utilize:image ID="Image1" runat="server" ImageUrl="~/DefaultImages/icon_confirm.png" />
                    </td>
                    <td class="success_title">
                        <utilize:literal runat="server" ID="lt_title_import_succesful"></utilize:literal></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <utilize:literal runat="server" ID="lt_text_import_succesful"></utilize:literal>
                    </td>
                </tr>
            </table>
        </utilize:panel>

        <utilize:panel runat="server" ID="pnl_error">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <utilize:image runat="server" ID="img_error" ImageUrl="~/DefaultImages/icon_error.png" />
                    </td>
                    <td class="error_title">
                        <utilize:literal runat="server" ID="lt_title_import_unsuccesful"></utilize:literal></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <utilize:literal runat="server" ID="lt_text_import_unsuccesful"></utilize:literal>
                        <i>
                            <utilize:label runat="server" ID="lt_text_import_errors"></utilize:label></i>
                    </td>
                </tr>
            </table>
        </utilize:panel>
    </utilize:placeholder>

    <utilize:button runat="server" ID="button_refresh" CssClass="refreshbutton" />
</div>
