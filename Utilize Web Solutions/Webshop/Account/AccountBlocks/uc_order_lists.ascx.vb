﻿Imports System.Data
Imports System.Web.UI.WebControls

Partial Class uc_order_lists
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Friend ShowType As Integer = 0
    Private dt_categories As System.Data.DataTable = Nothing
    Private _page_range_1 As Integer = 0
    Private _page_range_2 As Integer = 0
    Private _page_range_3 As Integer = 0
    Private _page_range_all As Integer = 999999
    Private _page_item_count As Integer = 10
    Private prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
    Private uc_shopping_cart_totals As Webshop.ws_user_control = Nothing

#Region "Page events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Me.button_show_all_products.Visible = Me.global_ws.get_webshop_setting("hide_all_orderlist")

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Paginering
        ' Laden van de javascript die de bottom paging goed zet 
        Me.set_javascript("uc_order_lists.js", Me)
        ' Opslaan paginering voor in de dropdown
        _page_range_1 = CInt(global_trans.translate_label("label_product_count_1", "15", Me.global_ws.Language))
        _page_range_2 = CInt(global_trans.translate_label("label_product_count_2", "30", Me.global_ws.Language))
        _page_range_3 = CInt(global_trans.translate_label("label_product_count_3", "45", Me.global_ws.Language))
        _page_range_all = 999999
        ' Aanmaken van de dropdown en paginering bepalen
        Me.set_pager_ranges()

        ' Dummy user control anders wordt de javascript niet uitgevoerd
        Dim uc_dummy_order_product As Webshop.ws_user_control = LoadUserControl("~/webshop/modules/productinformation/uc_order_product.ascx")
        uc_dummy_order_product.Attributes.Add("style", "display: none")
        Me.pnl_order_list.Controls.Add(uc_dummy_order_product)

        ' Kijk of de module aan staat en de gebruiker ingelogd is
        Me.Visible = Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_lists")

        If Me.Visible Then
            ' Laad de style sheet
            Me.set_style_sheet("uc_order_lists.css", Me)

            Me.button_show_all_products.OnClientClick = "$('#" + Me.show_my_order_list_products.ClientID + "').val('1');"
            Me.button_show_my_order_list.OnClientClick = "$('#" + Me.show_my_order_list_products.ClientID + "').val('0');"

            ' Load the shopping cart totals user control and add it to the page in a placeholder
            uc_shopping_cart_totals = LoadUserControl("~/Webshop/PaymentProcess/Blocks/uc_shopping_cart_totals.ascx")
            Me.ph_shopping_cart_totals.Controls.Add(uc_shopping_cart_totals)

            ' Koppel de artikelen aan de lijst
            Dim dt_products As DataTable = Me.get_products()
            Me.rpt_products.EnableViewState = False
            Me.rpt_products.DataSource = dt_products
            Me.rpt_products.DataBind()

            If Me.IsPostBack() Then
                rpt_products.page_nr = Request.Form(rpt_products.hih_page_nr.UniqueID)
                rpt_products.page_size = _page_item_count
                rpt_products.DataBind()
            End If

            '''''''''''''''
            ' CATEGORIEEN '
            '''''''''''''''
            If Not Me.IsPostBack() Then
                ' Ophalen van de categorieen
                Dim dt_categories As System.Data.DataTable = Me.get_webshop_categories()

                ' Voeg categorieen toe aan de dropdownlist
                Me.ddl_categories.DataSource = dt_categories
                Me.ddl_categories.DataValueField = "cat_code"
                Me.ddl_categories.DataTextField = "cat_desc"
                Me.ddl_categories.DataBind()

                ' Voeg een lege regel toe aan het begin
                If dt_categories.Rows.Count > 0 Then
                    ddl_categories.Items.Insert(0, New System.Web.UI.WebControls.ListItem With {.Text = "", .Value = ""})
                    Me.ddl_categories.SelectedValue = ""
                End If
            End If

            ' Merken
            If Not Me.IsPostBack() Then
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brands") Then
                    ' Ophalen van de merken
                    Dim dt_brands As System.Data.DataTable = Me.get_brands()

                    ' Voeg een lege regel toe aan het begin
                    If dt_brands.Rows.Count > 0 Then
                        Dim dr As DataRow = dt_brands.NewRow()
                        dr("brand_code") = ""
                        dr("brand_desc") = ""
                        dt_brands.Rows.InsertAt(dr, 0)
                        Me.ddl_brands.SelectedValue = ""
                    End If

                    ' Voeg merken toe aan de dropdownlist
                    Me.ddl_brands.DataSource = dt_brands
                    Me.ddl_brands.DataValueField = "brand_code"
                    Me.ddl_brands.DataTextField = "brand_desc"
                    Me.ddl_brands.DataBind()
                End If
            End If

            ''''''''''''''
            ' PAGINERING '
            ''''''''''''''
            ' Stel de maximale page size in als in de instellingen is aangegeven dat alle producten in een lijst getoond moeten worden
            If Me.global_ws.get_webshop_setting("show_all_products") Then
                _page_item_count = 999999
                Me.hidden_item_size.Value = _page_item_count
                Me.label_items_per_page.Visible = False
                Me.cbo_items_per_page1.Visible = False
            End If
            ' Stel in hoeveel producten per pagina getoond moeten worden
            Me.rpt_products.page_size = _page_item_count
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Koppel de winkelwagen pagina aan de knop en toon deze alleen als er artikelen in de winkelwagen staan
        Me.button_shopping_cart.Visible = Me.global_ws.shopping_cart.product_count > 0
        Me.button_shopping_cart.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx") + "'; return false;")

        ' Koppel the account pagina aan de knop
        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")

        ' Maak hier de placeholders zichtbaar obv de module actief/inactief
        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
        Me.ph_sizes_header.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")
        Me.ph_price_header.Visible = Not prices_not_visible

        ' Toon de lijst met producten of de tekst dat er geen producten zijn
        Me.ph_product_list.Visible = rpt_products.Items.Count > 0
        Me.ph_no_products.Visible = rpt_products.Items.Count = 0

        ' Toon de paginering alleen als er producten zijn
        Me.label_items_per_page.Visible = rpt_products.Items.Count > 0
        Me.cbo_items_per_page1.Visible = rpt_products.Items.Count > 0

        ' Toon het merken filter alleen als de module aan staat
        Me.ph_brands.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brands")
    End Sub
#End Region

#Region "Event handlers"
    ''' <summary>
    ''' Deze functie voegt de artikelen toe aan de winkelwagen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub button_add_to_cart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_to_cart.Click
        Dim ll_resume As Boolean = True

        Me.label_error_message.Visible = False

        If ll_resume Then
            ' Loop door de producten in de lijst
            For Each item As RepeaterItem In Me.rpt_products.Items
                If ll_resume Then
                    ' Ophalen van de control waarmee het artikel besteld kan worden
                    Dim uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control = CType(item.FindControl("uc_order_product"), Utilize.Web.Solutions.Webshop.ws_user_control)

                    ' Voer de functie order_products() uit als het aantal is ingevuld
                    If uc_order_product.get_property("Value") > 0 Then
                        If uc_order_product.execute_function("order_products") Then
                            ' Zet het aantal naar 0 als het toevoegen is gelukt
                            uc_order_product.set_property("Value", Decimal.Parse("0"))
                        Else
                            Me.label_error_message.Text = Me.global_ws.shopping_cart.ubo_shopping_cart.error_message
                            Me.label_error_message.Visible = True
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Deze functie vangt de event in de repeater af en roept de juiste functies aan
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rpt_products_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_products.ItemCommand
        Select Case e.CommandName.ToLower()
            Case "delete_row"
                Dim lc_prod_code As String = e.CommandArgument

                ' Verwijder het product uit de bestellijst
                Me.remove_product_from_order_list(lc_prod_code)

                ' En bouw de repeater opnieuw op
                Me.rpt_products.DataSource = Me.get_products()
                Me.rpt_products.DataBind()
            Case "add_row"
                Dim lc_prod_code As String = e.CommandArgument

                ' Voeg het product toe aan de bestellijst
                Me.add_product_to_order_list(lc_prod_code)
            Case Else
                Exit Select
        End Select
    End Sub

    ''' <summary>
    ''' Voeg per item in de repeater de user controls toe
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rpt_products_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_products.ItemCreated
        ' Voer de code alleen uit voor items in de repeater
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            ' Ophalen van de product code
            Dim lc_prod_code As String = e.Item.DataItem("prod_code")

            If (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", lc_prod_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", lc_prod_code, "mainitemmatrix") = lc_prod_code) Then
                Return
            End If
            '''''''''
            ' PRIJS '
            '''''''''
            ' Ophalen van de controls die nodig zijn
            Dim ph_price As PlaceHolder = CType(e.Item.FindControl("ph_price"), PlaceHolder)
            ph_price.Visible = Not prices_not_visible

            If ph_price.Visible Then
                Dim lt_prod_price As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_prod_price")

                Dim ln_prod_price As Decimal = 0

                If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                    ln_prod_price = Me.global_ws.get_product_price(e.Item.DataItem("prod_code"), "")
                Else
                    ln_prod_price = Me.global_ws.get_product_price_incl_vat(e.Item.DataItem("prod_code"), "")
                End If

                lt_prod_price.Text = Format(ln_prod_price, "c")
            End If

            ''''''''''''
            ' VOORRAAD '
            ''''''''''''
            ' Zorg hier dat voorraad zichtbaar wordt obv instellingen
            If Not Me.global_ws.get_webshop_setting("show_stock") = 3 Then
                ' Ophalen van de controls die nodig zijn
                Dim ph_stock As PlaceHolder = CType(e.Item.FindControl("ph_stock"), PlaceHolder)
                Dim ddl_sizes As DropDownList = CType(e.Item.FindControl("ddl_sizes"), DropDownList)

                ' Als de control is gevonden
                If ph_stock IsNot Nothing Then
                    ' Laad de user control
                    Dim uc_order_stock As Utilize.Web.Solutions.Webshop.ws_user_control = Nothing
                    uc_order_stock = LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_stock.ascx")
                    uc_order_stock.ID = "uc_product_stock"

                    ' Zet de eigenschappen van de user control
                    uc_order_stock.set_property("Visible", True)
                    uc_order_stock.set_property("product_code", lc_prod_code)
                    ' Zet de maat eigenschap
                    Dim lc_size As String = ""
                    If ddl_sizes IsNot Nothing Then
                        lc_size = ddl_sizes.SelectedValue
                    End If
                    uc_order_stock.set_property("size", lc_size)

                    ' Voeg de user control toe aan de placeholder
                    ph_stock.Controls.Add(uc_order_stock)

                    ' Toon de placeholder met de user control
                    ph_stock.Visible = True
                End If
            End If

            '''''''''''''
            ' BESTELLEN '
            '''''''''''''
            ' Ophalen van de controls die nodig zijn
            Dim ph_order_product As PlaceHolder = e.Item.FindControl("ph_order_product")

            ' Laad de user control
            Dim uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control = Nothing
            uc_order_product = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
            uc_order_product.ID = "uc_order_product"

            ' Zet de eigenschappen van de user control
            uc_order_product.set_property("product_code", lc_prod_code)
            uc_order_product.FindControl("button_order").Visible = False
            uc_order_product.FindControl("button_order1").Visible = False
            uc_order_product.FindControl("button_order2").Visible = False
            uc_order_product.set_property("Value", Decimal.Parse("0"))

            ' Voeg de user control toe aan de placeholder
            ph_order_product.Controls.Add(uc_order_product)

            '''''''''
            ' MATEN '
            '''''''''
            ' Ophalen van de placeholder
            Dim ph_sizes As PlaceHolder = CType(e.Item.FindControl("ph_sizes"), PlaceHolder)
            ' Toon de placeholder alleen als de sizeview module actief is
            ph_sizes.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview") = True
            ' Voeg de dropdownlist met maten toe
            If ph_sizes.Visible Then
                ' Ophalen van de placeholder waar de dropdown in moet komen
                Dim ph_sizes_dll As PlaceHolder = CType(e.Item.FindControl("ph_sizes_dll"), PlaceHolder)
                ' Aanmaken van de dropdownlist met maten
                Dim ddl As DropDownList = Me.create_sizes_dropdownlist(lc_prod_code)
                ' Als de dropdownlist niet is aangemaakt
                If ddl Is Nothing Then
                    ' Niet tonen van de placeholder
                    ph_sizes.Visible = False
                Else
                    ' Als de dropdownlist is aangemaakt
                    ' Toevoegen van de dropdownlist aan de placeholder
                    ph_sizes_dll.Controls.Add(ddl)
                End If
            End If

            '''''''''''''''''''''
            ' VERWIJDEREN REGEL '
            '''''''''''''''''''''
            Dim link_delete_row As LinkButton = e.Item.FindControl("link_delete_row")
            link_delete_row.OnClientClick = "return confirm('" + global_trans.translate_message("message_delete_order_list_row", "Weet u zeker dat u het geselecteerde product uit uw bestellijst wilt verwijderen?", Me.global_ws.Language) + "');"
        End If
    End Sub

    ''' <summary>
    ''' Hiermee wordt de zoek functionaliteit getoond/verborgen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub button_search_products_Click(sender As Object, e As EventArgs) Handles button_search_products.Click
        ' Zowieso moet het panel zichtbaar zijn als er gezocht gaat worden
        Me.ph_search_products.Visible = Not Me.ph_search_products.Visible

        Me.txt_prod_code.Focus()
    End Sub

    ''' <summary>
    ''' Hiermee worden de producten gefilterd op de ingevulde zoektermen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub button_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search.Click
        Me.rpt_products.page_nr = 0
        Me.rpt_products.page_size = _page_item_count
        Me.rpt_products.DataSource = Me.get_products()
        Me.rpt_products.DataBind()

        Me.txt_prod_code.Focus()
    End Sub

    ''' <summary>
    ''' Hiermee wordt het filter verwijderd
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub button_search_remove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_search_remove.Click
        Me.txt_prod_code.Text = ""
        Me.txt_prod_desc.Text = ""
        Me.ddl_categories.SelectedValue = ""
        Me.ddl_brands.SelectedValue = ""

        Me.rpt_products.page_nr = 0
        If Not Integer.TryParse(Me.hidden_item_size.Value, Me.rpt_products.page_size) Then
            Me.rpt_products.page_size = 15
        End If
        Me.rpt_products.DataSource = Me.get_products(True)
        Me.rpt_products.DataBind()

        Me.button_search.Visible = True
        Me.button_search_remove.Visible = False

        Me.txt_prod_code.Focus()
    End Sub

    Private Sub cbo_items_per_page1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_items_per_page1.SelectedIndexChanged
        Me.hidden_item_size.Value = cbo_items_per_page1.SelectedValue

        Me.rpt_products.page_nr = 0
        Me.rpt_products.page_size = Me.hidden_item_size.Value
        Me.rpt_products.DataSource = Me.get_products()
        Me.rpt_products.DataBind()
    End Sub
#End Region

#Region "Private/Protected methods"
    ''' <summary>
    ''' Deze functie wordt in de ascx aangeroepen en bepaald de url van de afbeelding van het artikel
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function get_image(ByVal prod_code As String) As String
        Dim qsc_product As New Utilize.Data.QuerySelectClass
        With qsc_product
            .select_fields = "image_small"
            .select_from = "uws_products"
            .select_where = "prod_code = @prod_code"
        End With
        qsc_product.add_parameter("@prod_code", prod_code)

        Dim dt_image As System.Data.DataTable = qsc_product.execute_query()

        Dim result As String = ""
        If Not dt_image.Rows(0).Item("image_small") = "" Then
            result = Me.ResolveCustomUrl("~/" + dt_image.Rows(0).Item("image_small"))
        Else
            result = Me.ResolveCustomUrl("~/Documents/ProductImages/not-available-small.jpg")
        End If

        Return result
    End Function

    Protected Function get_extra_information(ByVal prod_code As String) As String
        Return Me.get_categories_of_product(prod_code)
    End Function

    ''' <summary>
    ''' Deze functie haalt producten op die getoond moeten worden
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_products(Optional ByVal remove_filter As Boolean = False) As DataTable
        ' Roep de functie aan in de javascript code
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "move_navigation", "$(document).ready(function () {move_navigation();});", True)

        Select Case postback_control
            Case Me.button_show_my_order_list.UniqueID
                Me.ShowType = 0
            Case Me.button_show_all_products.UniqueID
                Me.ShowType = 1
            Case Else
                If Not String.IsNullOrEmpty(Request.Form(Me.show_my_order_list_products.UniqueID)) Then
                    Me.ShowType = CInt(Request.Form(Me.show_my_order_list_products.UniqueID))
                End If
        End Select

        Dim dt As DataTable = Nothing

        If Me.ShowType = 0 Then
            dt = Me.get_my_order_list_products()
        Else
            dt = Me.get_all_webshop_products()
        End If

        If Me.IsPostBack() And Not remove_filter And dt IsNot Nothing Then
            Me.button_search_remove.Visible = False

            '''''''''''''''''''''''''''''''''''''''''''
            ' Regel hieronder de zoeken functionaliteit
            '''''''''''''''''''''''''''''''''''''''''''
            If Request.Form(Me.txt_prod_code.UniqueID) IsNot Nothing And dt.Rows.Count > 0 Then
                Dim lc_prod_code As String = Request.Form(Me.txt_prod_code.UniqueID).Trim()

                If Not String.IsNullOrEmpty(lc_prod_code) Then
                    ' Als een artikelcode is ingevuld
                    Me.filter_data_table(dt, String.Format("prod_code like '%{0}%'", lc_prod_code))

                    For Each datarow As DataRow In dt.Rows
                        If Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", datarow.Item("prod_code")) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", datarow.Item("prod_code"), "mainitemmatrix") = datarow.Item("prod_code") Then
                            dt.Rows.Remove(datarow)
                        End If
                    Next

                    Me.button_search_remove.Visible = True
                End If
            End If

            If Request.Form(Me.txt_prod_desc.UniqueID) IsNot Nothing And dt.Rows.Count > 0 Then
                Dim lc_prod_desc As String = Request.Form(Me.txt_prod_desc.UniqueID).Trim()
                If Not String.IsNullOrEmpty(lc_prod_desc) Then
                    ' Als een omschrijving is ingevuld
                    Me.filter_data_table(dt, String.Format("prod_desc like '%{0}%'", lc_prod_desc))
                    Me.button_search_remove.Visible = True
                End If
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Hier worden de producten gefilterd op categorie '
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            If Request.Form(Me.ddl_categories.UniqueID) IsNot Nothing And dt.Rows.Count > 0 Then
                Dim lc_cat_code As String = Request.Form(Me.ddl_categories.UniqueID).Trim()

                If Not String.IsNullOrEmpty(lc_cat_code) Then
                    ' Als een categorie is gekozen
                    Me.filter_data_table(dt, String.Format("prod_code IN ({0})", Me.get_products_in_category(lc_cat_code)))
                    Me.button_search_remove.Visible = True
                End If
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''
            ' Hier worden de producten gefilterd op merk '
            ''''''''''''''''''''''''''''''''''''''''''''''
            If Request.Form(Me.ddl_brands.UniqueID) IsNot Nothing And dt.Rows.Count > 0 Then
                Dim lc_brand_code As String = Request.Form(Me.ddl_brands.UniqueID).Trim()

                If Not String.IsNullOrEmpty(lc_brand_code) Then
                    ' Als een merk is gekozen
                    Me.filter_data_table(dt, String.Format("brand_code = '{0}'", lc_brand_code))
                    Me.button_search_remove.Visible = True
                End If
            End If
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Filter een datatable met een expression
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="filter_expression"></param>
    ''' <remarks></remarks>
    Private Sub filter_data_table(ByRef dt As DataTable, ByVal filter_expression As String)
        Dim dr() As DataRow = dt.Select(filter_expression)
        If dr.Length > 0 Then
            dt = dr.CopyToDataTable()
        Else
            dt.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Deze functie haalt alle producten op die in de bestellijst van de klant staan
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_my_order_list_products() As DataTable
        ' Aanmaken van een nieuwe product filter
        ' Zet de eigenschappen van de product filter
        ' Stel in hoeveel producten opgehaald moeten worden
        Dim pf As New ws_productfilter
        With pf
            .include_sub_categories = True
            .page_size = 999999
            .extra_where = " and uws_products.prod_code in (select prod_code from uws_prod_order_list where user_id = @USER_ID)"
        End With

        ' Controleer of de module Product groepering aan staat
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group") Then
            Dim ln_group_products As Integer = Me.global_ws.get_webshop_setting("group_products")
            Select Case ln_group_products
                Case 0
                    ' Geen groepering
                Case 1
                    ' Standaard groepering op shp_code
                    pf.group_products = Webshop.group_types.group
                Case 2
                    ' Standaard groepering op waarde 1
                    pf.group_products = Webshop.group_types.group_val1
                Case 3
                    ' Standaard groepering op waarde 2
                    pf.group_products = Webshop.group_types.group_val2
                Case Else
                    Exit Select
            End Select
        End If

        pf.AddParameter("@user_id", Me.global_ws.user_information.user_id)

        ' Ophalen van de producten
        Return pf.get_products(Me.global_ws.Language)
    End Function

    ''' <summary>
    ''' Deze functie haalt alle webshop producten op mbv het ws_productfilter
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_all_webshop_products() As DataTable
        ' Aanmaken van een nieuwe product filter
        ' Zet de eigenschappen van de product filter
        ' Stel in hoeveel producten opgehaald moeten worden
        Dim pf As New ws_productfilter
        With pf
            .include_sub_categories = True
            .page_size = 999999
        End With

        ' Controleer of de module Product groepering aan staat
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group") Then
            Dim ln_group_products As Integer = global_ws.get_webshop_setting("group_products")
            Select Case ln_group_products
                Case 0
                    ' Geen groepering
                Case 1
                    ' Standaard groepering op shp_code
                    pf.group_products = Webshop.group_types.group
                Case 2
                    ' Standaard groepering op waarde 1
                    pf.group_products = Webshop.group_types.group_val1
                Case 3
                    ' Standaard groepering op waarde 2
                    pf.group_products = Webshop.group_types.group_val2
                Case Else
                    Exit Select
            End Select
        End If

        ' Ophalen van de producten
        Return pf.get_products(Me.global_ws.Language)
    End Function

    ''' <summary>
    ''' De functie haalt de webshop categorieen op rekening houdend met klant assortimenten
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_webshop_categories(Optional ByVal exclude_empty_categories As Boolean = True) As DataTable
        ' Declareer een nieuwe datatable
        Dim dt_categories As System.Data.DataTable = Nothing

        ' Haal de categorieën op
        ' Filter op categorieen die getoond mogen worden en de huidige taal
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        With qsc_categories
            .select_fields = "uws_categories_tran.cat_code, uws_categories_tran.cat_desc, uws_categories.cat_parent"
            .select_from = "uws_categories"
            .select_order = "row_ord"
            .select_where = "lng_code = @lng_code and cat_show = 1"
        End With
        ' Voeg de categorie vertalingen toe
        qsc_categories.add_join("inner join", "uws_categories_tran", "uws_categories_tran.cat_code = uws_categories.cat_code")

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        If exclude_empty_categories Then
            qsc_categories.select_where += " and uws_categories.cat_code in (select cat_code from uws_categories_prod) "
        End If

        qsc_categories.add_parameter("lng_code", Me.global_ws.Language)

        ' Sorteer op regelvolgorde
        qsc_categories.select_order = "uws_categories_tran.cat_desc asc"

        ' Voer de query uit
        dt_categories = qsc_categories.execute_query()

        ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
            Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

            If Not String.IsNullOrEmpty(Me.global_ws.user_information.ubo_customer.field_get("ass_code")) Then
                ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True Then
                    ' Ophalen van de categorieen die aan de klant zijn toegekend
                    Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                    If Not lc_category_string = "" Then
                        For Each dr_data_row As System.Data.DataRow In dt_categories.Rows
                            If Not lc_category_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                                dr_data_row.Delete()
                            End If
                        Next

                        dt_categories.AcceptChanges()
                    End If
                End If

                ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                    ' Ophalen van de categorieen die voor klant zijn uitgesloten
                    Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)

                    ' Loop door de categorieen
                    For Each dr_data_row As System.Data.DataRow In dt_categories.Rows
                        ' Controleer of de categorie is uitgesloten
                        If lc_category_exclusion_string.Contains("|" + dr_data_row.Item("cat_code") + "|") Then
                            ' Verwijder de categorie
                            dr_data_row.Delete()
                        End If
                    Next
                    ' Voer de wijzigingen door
                    dt_categories.AcceptChanges()
                End If
            End If
        End If

        Return dt_categories
    End Function

    ''' <summary>
    ''' Ophalen van de producten in een categorie
    ''' </summary>
    ''' <param name="cat_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_products_in_category(ByVal cat_code As String) As String
        ' Declareer een nieuwe datatable
        Dim dt_category_products As System.Data.DataTable = Nothing

        ' Haal de producten in een categorie op
        ' Filter op producten in de huidige categorie
        Dim qsc_category_products As New Utilize.Data.QuerySelectClass
        With qsc_category_products
            .select_fields = "prod_code"
            .select_from = "uws_categories_prod"
            .select_where = "cat_code = @cat_code"
        End With
        qsc_category_products.add_parameter("cat_code", cat_code)

        dt_category_products = qsc_category_products.execute_query()

        Dim lc_products_in_category As String = ""
        For Each dr As DataRow In dt_category_products.Rows
            lc_products_in_category += IIf(String.IsNullOrEmpty(lc_products_in_category), "", ", ") + "'" + dr.Item("prod_code").ToString.Trim() + "'"
        Next

        If String.IsNullOrEmpty(lc_products_in_category) Then
            lc_products_in_category = "''"
        End If

        Return lc_products_in_category
    End Function

    ''' <summary>
    ''' Deze functie verwijderd het product uit de bestellijst
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <remarks></remarks>
    Private Sub remove_product_from_order_list(ByVal prod_code As String)
        Dim lc_rec_id As String = Me.get_id_product_in_order_list(prod_code)

        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_order_list")

        ' Test flag
        Dim ll_testing As Boolean = Not String.IsNullOrEmpty(lc_rec_id)

        ' obtain the row
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_seek("uws_prod_order_list", lc_rec_id)
        End If

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.table_delete_row("uws_prod_order_list", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
        End If
    End Sub

    ''' <summary>
    ''' Deze functie voegt een product toe aan de bestellijst
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <remarks></remarks>
    Private Sub add_product_to_order_list(ByVal prod_code As String)
        Dim lc_rec_id As String = Me.get_id_product_in_order_list(prod_code)

        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_order_list")

        ' Test flag
        Dim ll_testing As Boolean = String.IsNullOrEmpty(lc_rec_id)

        ' Insert a new row in the business object for table uws_prod_order_list
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_insert_row("uws_prod_order_list", Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.field_set("prod_code", prod_code)
            ubo_bus_obj.field_set("user_id", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)
        End If
    End Sub

    ''' <summary>
    ''' Deze functie haalt de rec_id uit de bestellijst op van het huidige product
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_id_product_in_order_list(ByVal prod_code As String) As String
        ' Haal de producten in de bestellijst op
        ' Filter op categorieen die getoond mogen worden en de huidige taal
        Dim qc_products As New Utilize.Data.QuerySelectClass
        With qc_products
            .select_fields = "rec_id"
            .select_from = "uws_prod_order_list"
            .select_where = "user_id = @user_id and prod_code = @prod_code"
        End With
        qc_products.add_parameter("user_id", Me.global_ws.user_information.user_id)
        qc_products.add_parameter("prod_code", prod_code)

        Dim dt_products As DataTable = qc_products.execute_query()

        If dt_products.Rows.Count > 0 Then
            Return dt_products.Rows(0).Item("rec_id")
        End If

        Return ""
    End Function

    ''' <summary>
    ''' Haalt een string met categorieen op waar het product is in ingedeeld
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_categories_of_product(ByVal prod_code As String) As String
        ' Haal  alle categorie codes op waar het product in is ingedeeld
        ' Filter op producten in de huidige categorie
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        With qsc_categories
            .select_fields = "uws_categories.cat_code, uws_categories_tran.cat_desc, uws_categories.cat_parent"
            .select_from = "uws_categories"
            .select_order = "row_ord"
            .select_where = "uws_categories_prod.prod_code = @prod_code and uws_categories_tran.lng_code = @lng_code and uws_categories.cat_show = 1"
        End With
        qsc_categories.add_join("left join", "uws_categories_prod", "uws_categories_prod.cat_code = uws_categories.cat_code")
        qsc_categories.add_join("left join", "uws_categories_tran", "uws_categories_tran.cat_code = uws_categories.cat_code")
        qsc_categories.add_parameter("prod_code", prod_code)
        qsc_categories.add_parameter("lng_code", Me.global_ws.Language)

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        ' Voer de query uit
        Dim dt_categories_prod As System.Data.DataTable = qsc_categories.execute_query()

        Dim lc_products_in_category As String = ""

        ' Loop door de categorieen van het product
        For Each dr As DataRow In dt_categories_prod.Rows
            ' Ophalen van het pad van de categorie
            Dim lc_cat_string As String = ""
            Me.get_category_path(dr.Item("cat_code"), lc_cat_string)
            ' Voeg het pad toe aan de string
            lc_products_in_category += IIf(String.IsNullOrEmpty(lc_products_in_category), "", "<br/>") + lc_cat_string
        Next

        Return lc_products_in_category
    End Function

    ''' <summary>
    ''' Deze functie haalt het complete pad van een categorie op
    ''' </summary>
    ''' <param name="cat_code"></param>
    ''' <param name="cat_string"></param>
    ''' <remarks></remarks>
    Private Sub get_category_path(ByVal cat_code As String, ByRef cat_string As String)
        If dt_categories Is Nothing Then
            dt_categories = Me.get_webshop_categories(False)
        End If

        If Not String.IsNullOrEmpty(cat_code) Then
            Dim dr() As DataRow = dt_categories.Select("cat_code = '" + cat_code + "'")
            If dr.Length > 0 Then
                cat_string = dr(0).Item("cat_desc") + IIf(String.IsNullOrEmpty(cat_string), "", " / ") + cat_string
                If Not String.IsNullOrEmpty(dr(0).Item("cat_parent")) Then
                    Me.get_category_path(dr(0).Item("cat_parent"), cat_string)
                End If
            End If
        End If
    End Sub

    Private Sub set_pager_ranges()
        Dim dt_data_table As New System.Data.DataTable

        ' Maak een nieuwe datacolumn aan
        Dim dc_column As New System.Data.DataColumn
        With dc_column
            .ColumnName = "page_size"
            .DataType = System.Type.GetType("System.Int32")
        End With
        dt_data_table.Columns.Add(dc_column)

        ' Maak een nieuwe datacolumn aan
        Dim dc_column2 As New System.Data.DataColumn
        With dc_column2
            .ColumnName = "page_size_lbl"
            .DataType = System.Type.GetType("System.String")
        End With
        dt_data_table.Columns.Add(dc_column2)

        Dim dr_data_row1 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row1.Item("page_size") = _page_range_1
        dr_data_row1.Item("page_size_lbl") = _page_range_1.ToString()
        dt_data_table.Rows.Add(dr_data_row1)

        Dim dr_data_row2 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row2.Item("page_size") = _page_range_2
        dr_data_row2.Item("page_size_lbl") = _page_range_2.ToString()
        dt_data_table.Rows.Add(dr_data_row2)

        Dim dr_data_row3 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row3.Item("page_size") = _page_range_3
        dr_data_row3.Item("page_size_lbl") = _page_range_3.ToString()
        dt_data_table.Rows.Add(dr_data_row3)

        Dim dr_data_row_all As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row_all.Item("page_size") = _page_range_all
        dr_data_row_all.Item("page_size_lbl") = global_trans.translate_label("label_product_count_all", "Toon alles", Me.global_ws.Language)
        dt_data_table.Rows.Add(dr_data_row_all)

        Me.cbo_items_per_page1.DataTextField = "page_size_lbl"
        Me.cbo_items_per_page1.DataValueField = "page_size"
        Me.cbo_items_per_page1.DataSource = dt_data_table
        Me.cbo_items_per_page1.DataBind()

        Try
            Me.cbo_items_per_page1.SelectedValue = Request.Form(Me.hidden_item_size.UniqueID)
            _page_item_count = Request.Form(Me.hidden_item_size.UniqueID)
        Catch ex As Exception
            _page_item_count = _page_range_1
            Me.hidden_item_size.Value = _page_item_count
        End Try
    End Sub
#End Region

#Region "Brands"
    ''' <summary>
    ''' De functie haalt de merken op rekening houdend met klant assortimenten
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_brands() As DataTable
        ' Declareer een nieuwe datatable
        Dim dt_brands As System.Data.DataTable = Nothing

        ' Haal de categorieën op
        ' Sorteer op regelvolgorde
        Dim qsc_brands As New Utilize.Data.QuerySelectClass
        With qsc_brands
            .select_fields = "uws_brands.brand_code, uws_brands.brand_desc_" + Me.global_ws.Language + " as brand_desc"
            .select_from = "uws_brands"
            .select_order = "uws_brands.brand_desc_" + Me.global_ws.Language + " asc"
        End With

        ' Als de module merkuisluitingen per klant aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_brand_excl") = True Then
            qsc_brands.select_where = "uws_brands.brand_code NOT IN (SELECT brand_code from uws_customers_brands WHERE cust_id = @cust_id)"
            qsc_brands.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        End If

        dt_brands = qsc_brands.execute_query()

        Return dt_brands
    End Function
#End Region

#Region "SizeView sizes"
    ''' <summary>
    ''' Deze functie haalt de maten op van het artikel
    ''' </summary>
    ''' <param name="product_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_sizes(ByVal product_code As String) As DataTable
        Dim lc_sizebar_code As String = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_code", product_code.Trim(), "prod_code")

        ' Maak een class met maten aan
        ' Zet een filter
        Dim qsc_sizes As New Utilize.Data.QuerySelectClass
        With qsc_sizes
            .select_fields = "size_code, size_desc_" + Me.global_ws.Language + ", row_ord"
            .select_from = "uws_sizeview_sizes"
            .select_where = "sizebar_code = @sizebar_code and not size_code in (select size_code from uws_sizeview_sizepro where prod_code = @prod_code and blocked = 1)"
            .select_order = "row_ord"
        End With
        qsc_sizes.add_parameter("sizebar_code", lc_sizebar_code)
        qsc_sizes.add_parameter("prod_code", product_code)

        Dim dt_sizes As DataTable = qsc_sizes.execute_query()

        Return dt_sizes
    End Function

    ''' <summary>
    ''' Deze functie maakt een dropdownlist aan voor de maten van een artikel
    ''' </summary>
    ''' <param name="product_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function create_sizes_dropdownlist(ByVal product_code As String) As DropDownList
        ' Als de module sizeview actief is, dan moet een datatable gevuld worden
        Dim ll_sizeview_product As Boolean = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_article", product_code.Trim().ToUpper()) = True

        If Not ll_sizeview_product Then
            Return Nothing
        End If

        If ll_sizeview_product Then
            ' Het is een sizeview product, haal de maten op
            Dim dt_sizes As System.Data.DataTable = Me.get_sizes(product_code)

            ' Maak een nieuwe dropdownlist aan
            Dim ddl_sizes As New DropDownList
            With ddl_sizes
                .ID = "ddl_sizes"
                .DataSource = dt_sizes
                .DataTextField = "size_desc_" + Me.global_ws.Language
                .DataValueField = "size_code"
            End With
            ddl_sizes.DataBind()

            AddHandler ddl_sizes.SelectedIndexChanged, AddressOf ddl_sizes_SelectedIndexChanged

            Return ddl_sizes

            ' Als er geen maten zijn
            If dt_sizes.Rows.Count > 0 Then
                Return Nothing
            End If
        End If

        Return Nothing
    End Function

    ''' <summary>
    ''' Deze functie wijzigt de waarden van de order stock control als er een andere maat wordt geselecteerd
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddl_sizes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl_sizes As DropDownList = DirectCast(sender, DropDownList)
        Dim item As RepeaterItem = DirectCast(ddl_sizes.NamingContainer, RepeaterItem)
        Dim lc_prod_code As String = item.DataItem("prod_code")
        Dim uc_order_stock As Webshop.ws_user_control = DirectCast(item.FindControl("uc_order_stock"), Webshop.ws_user_control)

        ' Zet hier de nieuwe properties voor de voorraad waarden
        uc_order_stock.set_property("Visible", True)
        uc_order_stock.set_property("product_code", lc_prod_code)
        uc_order_stock.set_property("size", ddl_sizes.SelectedValue)
    End Sub

#End Region

End Class
