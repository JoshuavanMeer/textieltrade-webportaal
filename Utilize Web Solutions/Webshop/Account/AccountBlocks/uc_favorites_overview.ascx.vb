﻿
Partial Class uc_favorites_overview
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _customer_product_codes As Boolean = False

    Private Sub set_product_list()
        Dim _pf_product_filter As New ws_productfilter
        ' Bouw de product filter class op

        _pf_product_filter.extra_where = "and uws_products.prod_code in (select prod_code from uws_prod_favorites where user_id = '" + Me.global_ws.user_information.user_id + "')"
        _pf_product_filter.page_size = 999

        ' Voer de query uit en bouw de repeater op
        Me.rpt_product_list.DataSource = _pf_product_filter.get_products(Me.global_ws.Language)
        Me.rpt_product_list.DataBind()

        Me.ph_product_list.Visible = True
        Me.ph_no_items.Visible = False

        If Me.rpt_product_list.Items.Count = 0 Then
            Me.ph_product_list.Visible = False
            Me.ph_no_items.Visible = True
        End If
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not Me.global_ws.user_information.user_logged_on And Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_favorites") Then
            Response.Redirect("~/home.aspx", True)
        End If

        If Not Me.global_ws.user_information.user_logged_on Then
            ' Als je niet bent ingelogd, dan is deze onzichtbaar, anders is deze obv licentie
            Me._customer_product_codes = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_prod")
        End If

        Me.set_style_sheet("uc_favorites_overview.css", Me)
        Me.block_css = True

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Me.button_my_account.Visible = False
        End If

        Me.set_product_list()
        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
    End Sub

    Friend Function get_image(ByVal product_code As String) As String
        Dim lc_return_image As String = Utilize.Data.DataProcedures.GetValue("uws_products", "thumbnail_normal", product_code)

        If lc_return_image = "" Then
            lc_return_image = "Documents/ProductImages/not-available-small.jpg"
        End If

        Return "~/" + lc_return_image
    End Function

    Protected Sub rpt_product_list_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_product_list.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim uc_product_row As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
            uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
            uc_product_row.set_property("bulk_order", False)
            uc_product_row.set_property("customer_product_codes", Me._customer_product_codes)

            Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
            ph_product_row.Controls.Add(uc_product_row)
        End If
    End Sub
End Class
