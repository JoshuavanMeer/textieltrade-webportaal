﻿
Imports System.Web.UI.WebControls

Partial Class Webshop_Account_AccountBlocks_uc_training
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private item_counter As Integer = 1
    Private Sub rpt_training_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_training.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim img_training_logo As Utilize.Web.UI.WebControls.image = e.Item.FindControl("img_training_logo")
            img_training_logo.Visible = Not e.Item.DataItem("no_image")
            If img_training_logo.Visible Then
                If Not e.Item.DataItem("image_url") = "" Then
                    img_training_logo.ImageUrl = Me.ResolveCustomUrl("~/" + e.Item.DataItem("image_url"))
                Else
                    If Not e.Item.DataItem("prod_code") = "" Then
                        img_training_logo.ImageUrl = Me.ResolveCustomUrl("~/" + Utilize.Data.DataProcedures.GetValue("uws_products", "image_large", e.Item.DataItem("prod_code")))
                    Else
                        img_training_logo.Visible = False
                    End If
                End If
            End If

            Select Case item_counter
                Case 1
                    Dim lt_start_row As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_start_row")
                    lt_start_row.Visible = True
                    item_counter = 2
                Case 2
                    Dim lt_end_row As Utilize.Web.UI.WebControls.literal = e.Item.FindControl("lt_end_row")
                    lt_end_row.Visible = True
                    item_counter = 1
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Private Sub Webshop_Account_AccountBlocks_uc_training_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_training.css", Me)

        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/" + me.global_ws.language + "/Webshop/login.aspx", True)
        End If

        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/product_list.aspx", True)
        End If

        Dim qsc_training As New Utilize.Data.QuerySelectClass
        qsc_training.select_fields = "distinct title_" + Me.global_ws.Language + " as title, desc_" + Me.global_ws.Language + " as description, tgt_url_" + Me.global_ws.Language + " as url, image_url, no_image, uws_training.prod_code"
        qsc_training.select_from = "uws_training"
        qsc_training.add_join("left join", "uws_so_order_lines", "uws_training.prod_code = uws_so_order_lines.prod_code")
        qsc_training.add_join("left join", "uws_history_lines", "uws_training.prod_code = uws_history_lines.prod_code")
        qsc_training.select_where = "(uws_so_order_lines.hdr_id in (select ord_nr from uws_so_orders where cust_id = @cust_id) and uws_history_lines.hdr_id in (select crec_id from uws_history where cust_id = @cust_id)) or uws_training.prod_code = ''"
        qsc_training.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
        Dim dt_training As System.Data.DataTable = qsc_training.execute_query()

        If dt_training.Rows.Count = 0 Then
            Response.Redirect("~/" + me.global_ws.language + "/Webshop/Account/Account.aspx", True)
        End If

        Me.rpt_training.DataSource = dt_training
        rpt_training.DataBind()

        Dim int_row_count As Integer = dt_training.Rows.Count
        Dim int_check As Integer = 0

        int_check = int_row_count Mod 2

        If Not int_check = 0 Then
            lt_backup_end_row.Visible = True
        End If

        Me.button_my_account.Attributes.Add("onClick", "location.replace('" + Me.Page.ResolveUrl("~/") + Me.global_ws.Language + "/account/account.aspx" + "');return false;")
    End Sub
End Class
