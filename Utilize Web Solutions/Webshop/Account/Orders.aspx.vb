﻿
Partial Class Webshop_Account_Orders
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_orders_overview", "Overzicht bestellingen", Me.global_ws.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_orders As Webshop.ws_user_control = LoadUserControl("AccountBlocks/uc_orders.ascx")
        ph_zone.Controls.Add(uc_orders)
    End Sub
End Class
