﻿Imports System.IO
Imports ExpertPdf.HtmlToPdf

Partial Class Webshop_ecatalogus
    Inherits Utilize.Web.Solutions.Base.base_page_base

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        ' In het geval van "log in app" in session
        If Session("log_in_app") Then
            ' Controleer of de gebruiker is ingelogd, anders naar login
            If Not Me.global_ws.user_information.user_logged_on Then
                Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
            End If
        End If

        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte en genereren van deze e-catalogus wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if there is a e-catalog module bought
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_e_catalog") Then
            Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
        End If

        Dim strb_html_catalog As StringBuilder = ConstructHtmlCatalog()

        ' Set up pdf generator
        Dim lb_return As Byte() = Nothing

        Dim loPDFGenerate As New ExpertPdf.HtmlToPdf.PdfConverter
        loPDFGenerate.LicenseKey = "Apxofk9W45ihltLQtKYelQ7h9+0Jm3o+D/mGrPycnO7rHozfB9KwzbnQ7M6ZjIGE"

        loPDFGenerate.PdfHeaderOptions.DrawHeaderLine = False
        loPDFGenerate.PdfHeaderOptions.HeaderSubtitleText = ""
        loPDFGenerate.PdfHeaderOptions.HeaderTextYLocation = 15

        If Not Request.Params("headertext") = Nothing Then
            loPDFGenerate.PdfHeaderOptions.HeaderText = System.Web.HttpUtility.UrlDecode(Request.Params("headertext").ToString())
        Else
            loPDFGenerate.PdfHeaderOptions.HeaderText = ""
        End If

        loPDFGenerate.PdfFooterOptions.DrawFooterLine = False
        loPDFGenerate.PdfFooterOptions.FooterText = ""
        loPDFGenerate.PdfFooterOptions.PageNumberText = ""
        
        loPDFGenerate.PdfDocumentOptions.ShowFooter = True
        loPDFGenerate.PdfDocumentOptions.ShowHeader = True
        'loPDFGenerate.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best
        loPDFGenerate.PdfDocumentOptions.GenerateSelectablePdf = True
        ' Create pdf without empty table
        Dim html_pdf_catalog As String = strb_html_catalog.ToString
        html_pdf_catalog = html_pdf_catalog.Replace("<table>" & vbCrLf & "</table><table style=""width:100%;page-break-before: always;"">", "<table style=""width:100%;"">")
        lb_return = loPDFGenerate.GetPdfBytesFromHtmlString(html_pdf_catalog)

        ' Send pdf result to webbrowser
        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "attachment;filename=ecatalogus.pdf")
        Response.BufferOutput = True
        Response.BinaryWrite(lb_return)
        Response.End()
    End Sub

    Private Function ConstructHtmlCatalog() As StringBuilder
        Dim dt_data_table As System.Data.DataTable = FetchCatalogData()

        ' Build the html
        Dim strb_html_catalog As New StringBuilder

        strb_html_catalog.AppendLine("<html><body style=""font-family: Raleway, sans-serif!important; font-style:10pt;""><table>")

        Dim page_count As Integer = -1
        Dim articles_count_per_page As Integer = 14

        Dim physical_location_of_webshop As String = Server.MapPath("~/")

        For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
            If articles_count_per_page > 13 Then
                articles_count_per_page = 0
                page_count += 1

                strb_html_catalog.AppendLine("</table><table style=""width:100%;page-break-before: always;"">")
            End If

            If dr_data_row.Item("prod_show") = True Then

                Dim str_image_tag As String = ""
                If dr_data_row.Item("image_small") = "" Then
                    str_image_tag = "<img src=""" + Me.relative_url + "/Documents/ProductImages/Not-available-small.jpg"" style=""Height:150px;"" />"
                Else
                    If File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/") + dr_data_row.Item("image_small")) Then
                        str_image_tag = "<img src=""" + Me.relative_url + dr_data_row.Item("image_small").replace("\", "/") + """ style=""Height:150px;"" />"
                    Else
                        str_image_tag = "<img src=""" + Me.relative_url + "/Documents/ProductImages/Not-available-small.jpg"" style=""Height:150px;""/>"
                    End If
                End If

                Dim combined_string As String = "<table><tr><td style=""min-width:350px;width:350px;max-width:350px;""><table>"
                combined_string += "<tr><td style=""margin-left:25px;margin-right:25px; vertical-align: top;"">Artikelnummer:</td><td>" + dr_data_row.Item("prod_code").ToString + "</td></tr>"
                combined_string += "<tr><td style=""margin-left:25px;margin-right:25px; vertical-align: top;"">Omschrijving:</td><td> " + dr_data_row.Item("prod_desc").ToString + "</td></tr>"
                combined_string += "<tr><td style=""margin-left:25px;margin-right:25px; vertical-align: top;"">Aantal in doos:</td><td> " + Utilize.Data.API.Webshop.product_manager.get_product_unit_quantity(dr_data_row.Item("prod_code"), "DOOS").ToString() + "</td></tr>"
                combined_string += "<tr><td style=""margin-left:25px;margin-right:25px; vertical-align: top;"">Barcode:</td><td> " + dr_data_row.Item("bar_code").ToString + "</td></tr>"
                combined_string += "</table></td><td style=""min-width:140px;width:140px;max-width:140px; vertical-align: top;"">" + str_image_tag + "</td></tr></table>"


                ' If articles count per page is even, then add start row tag
                If articles_count_per_page Mod 2 = 0 Then
                    strb_html_catalog.AppendLine("<tr>")
                    strb_html_catalog.AppendLine("<td style=""text-align: left;height:170px;border-bottom: solid thin #000000;"">")
                    strb_html_catalog.AppendLine(combined_string)
                    strb_html_catalog.AppendLine("</td>")
                    strb_html_catalog.AppendLine("<td style=""min-width:100px;max-width:100px;""></td>")
                Else
                    ' If articles count per page is odd, the add end row tag
                    strb_html_catalog.AppendLine("<td style=""text-align: left;height:170px;border-bottom: solid thin #000000;;"">")
                    strb_html_catalog.AppendLine(combined_string)
                    strb_html_catalog.AppendLine("</td>")
                    strb_html_catalog.AppendLine("</tr>")
                End If

                articles_count_per_page += 1
            End If
        Next

        ' If there are an odd number of articles on the last page, then add end row tag
        If articles_count_per_page Mod 2 <> 0 Then
            strb_html_catalog.AppendLine("</tr>")
        End If

        ' End html the correct way
        strb_html_catalog.AppendLine("</table>")
        strb_html_catalog.AppendLine("</body></html>")
        Return strb_html_catalog
    End Function

    Private Function FetchCatalogData() As System.Data.DataTable
        ' Construct query and execute it
        Dim lcLanguage As String = "nl"
        Dim lcCategoriesCodes As String() = Nothing

        If Not Request.Params("language") = Nothing Then
            lcLanguage = Request.Params("language").ToString().ToLower()
        End If

        If Not Request.Params("categoriescodes") = Nothing Then
            lcCategoriesCodes = Request.Params("categoriescodes").ToString().Split(",")
        End If

        Dim qsc_products As New Utilize.Data.QuerySelectClass
        qsc_products.select_fields = "uws_products.prod_desc, uws_products.bar_code, uws_products.fl_code2, uws_products.fl_code3, uws_products.brand_code, uws_products.prod_code, uws_products_tran.prod_desc2, uws_products.fl_code4, uws_products_tran.ff_desc2, uws_products_tran.prod_comm, uws_products.prod_show, 'Categerie 1' as category, "
        qsc_products.select_fields += "'px_pchs' as px_pchs, uws_products.prod_group, uws_products.fl_code1, uws_products_tran.ff_desc3, 'Export carton quantity' as ecq, uws_products.ff_desc4, uws_products.ff_desc5, uws_products_tran.ff_desc6, uws_products_tran.ff_desc7, uws_products.ship_days, "
        qsc_products.select_fields += "uws_products.shp_grp, uws_products_tran.grp_desc1, uws_products.grp_val1, uws_products.image_large, "
        qsc_products.select_fields += "uws_products.prod_prio, uws_products.image_small, uws_products.image_normal, uws_products.image_large"
        qsc_products.select_from = "uws_products"
        qsc_products.add_join("left outer join", "uws_products_tran", "uws_products_tran.prod_code = uws_products.prod_code")

        If IsNothing(lcCategoriesCodes) Then
            qsc_products.select_where = "uws_products.prod_code in (select prod_code from uws_categories_prod) "
        Else
            qsc_products.select_where = "uws_products.prod_code in (select prod_code from uws_categories_prod where "
            For Each categoriecode As String In lcCategoriesCodes
                If Not String.IsNullOrEmpty(categoriecode) Then
                    qsc_products.select_where += "cat_code = '" + categoriecode + "' OR "
                End If
            Next
            qsc_products.select_where = qsc_products.select_where.Substring(0, qsc_products.select_where.Length - 3) + ") "
        End If

        qsc_products.select_where += "and lng_code = @lng_code "
        qsc_products.select_where += "and prod_show = 1 "
        qsc_products.add_parameter("lng_code", lcLanguage)
        qsc_products.select_order = "uws_products.prod_code"

        Dim dt_data_table As System.Data.DataTable = qsc_products.execute_query()
        Return dt_data_table
    End Function
End Class
