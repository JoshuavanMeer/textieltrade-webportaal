﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OrderReport.aspx.vb" Inherits="OrderReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Order report</title>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/plugins/bootstrap/css/bootstrap.min.css") %>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/css/style.css")%>" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/css/shop.style.css") %>" />

    <link href="<%=ResolveUrl("~/Styles/GlobalStyle.min.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Styles/GlobalAccountStyle.min.css")%>" rel="stylesheet" type="text/css" />

    <!-- styles -->
    <utilize:PlaceHolder id="styles" runat="server">
    </utilize:PlaceHolder>
    <script>
        window.print();
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="account">
        <utilize:placeholder runat="server" id="ph_order_report">

        </utilize:placeholder>
    </div>
    </form>
</body>
</html>
