﻿
Partial Class Webshop_Account_ImportMultiOrderProgress
    Inherits Utilize.Web.Solutions.Base.base_page_base

    Friend ImportObject As ImportObject
    Friend RefreshId As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ImportObject = New ImportObject(Me.get_page_parameter("SessionId"))
        RefreshId = Me.get_page_parameter("RefreshId")

        If ImportObject.Finished = True Then
            Me.tmr_timer.Enabled = False
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ParentRefresh", "window.parent.document.getElementById('" + RefreshId + "').click();", True)
        End If
    End Sub

    Protected Sub tmr_timer_Tick(sender As Object, e As EventArgs) Handles tmr_timer.Tick

    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Session("KeepAliveGuid") = Guid.NewGuid().ToString()

        Me.lbl_progress.Text = global_trans.translate_message("message_multi_order_import_progress", "Bezig met importeren order [1] van [2]", Me.global_ws.Language)
        Me.lbl_progress.Text = Me.lbl_progress.Text.Replace("[1]", ImportObject.CurrentRow.ToString())
        Me.lbl_progress.Text = Me.lbl_progress.Text.Replace("[2]", ImportObject.RowCount.ToString())

        If ImportObject.Finished Then
            Me.lbl_progress.Text = global_trans.translate_message("message_multi_order_import_process", "Bezig met verwerken orders", Me.global_ws.Language)
        End If
    End Sub
End Class
