﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OpenDocument.aspx.vb" Inherits="Webshop_Account_open_document" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Styles/GlobalAccountStyle.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat='server' ID="sm_scriptmanager"></asp:ScriptManager>
    <div class="opendocument">
        <utilize:literal runat="server" ID="lt_html_string"></utilize:literal>
    </div>
    </form>
</body>
</html>
