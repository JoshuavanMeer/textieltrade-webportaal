﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ImportMultiOrderProgress.aspx.vb" Inherits="Webshop_Account_ImportMultiOrderProgress" %>

<!DOCTYPE html>

<!doctype html>
<html lang="en">
<head runat="server">
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/_THMAssets/plugins/bootstrap/css/bootstrap.min.css") %>">

</head>
<body>
    <form id="form1" runat="server">
        <utilize:toolkitscriptmanager runat="server" ID="sm_progress"></utilize:toolkitscriptmanager>

        <utilize:updatepanel runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Timer runat="server" ID="tmr_timer" Interval="2000"></asp:Timer>

                <div class="container">
                    <div class="row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<%= Me.ImportObject.Percentage.ToString()%>" aria-valuemin="0" aria-valuemax="100" style="width:<%= Me.ImportObject.Percentage.ToString()%>%"></div>
                        </div>
                        <div style="text-align: center">
                            <utilize:label runat="server" ID="lbl_progress"></utilize:label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </utilize:updatepanel>
    </form>
</body>
</html>
