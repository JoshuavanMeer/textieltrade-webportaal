﻿Partial Class Logout
    Inherits CMSPageTemplate

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ll_resume As Boolean = True

        ' Als er een oorspronkelijke id is, dan is de gebruiker namens een klant ingelogd.
        ' Sla deze id op in een variabele
        Dim lc_original_id As String = Me.global_ws.user_information.user_id_original

        ' Als de oorspronkelijke id niet bekend is, dan wordt de cookie ook leeggemaakt
        If ll_resume And lc_original_id = "" Then
            Me.cookie_write("")
        End If

        If ll_resume Then
            'Destroy the Categories helper Session, so a new will be created there
            If Not Session("UserCategoriesModel") Is Nothing Then
                Session.Remove("UserCategoriesModel")
            End If
            Dim asd = Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId()
            If Not Cache.Get(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel") Is Nothing Then
                Cache.Remove(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel")
            End If
            'Probeer uit te loggen
            ll_resume = Me.global_ws.user_information.logout()
        End If

        If ll_resume And Not lc_original_id = "" Then
            ' Haal de gegevens op om opnieuw namens de oorspronkelijke gebruiker in te kunnen loggen.
            Dim lc_user_id As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "rec_id", lc_original_id)
            Dim lc_cust_id As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", lc_original_id)
            Dim lc_login_code As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_code", lc_original_id)
            Dim lc_password As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_password", lc_original_id)
            Dim ln_user_type As Integer = Utilize.Data.DataProcedures.GetValue("uws_customers", "type_code", lc_cust_id)

            ' Log in namens de oorspronkelijke gebruiker
            ll_resume = Me.global_ws.user_information.sales_login(ln_user_type, lc_login_code, lc_cust_id, lc_user_id)

            If ll_resume Then
                ' Als de module inkoopcombinaties aan staat, ga dan terug naar home
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
                    If Me.global_ws.purchase_combination.user_is_manager Then
                        Response.Redirect("~/" + me.global_ws.language + "/account/account.aspx", False)
                    Else
                        Response.Redirect("~/home.aspx", False)
                    End If
                Else
                    Response.Redirect("~/" + Me.global_ws.Language + "/account/customersoverview.aspx", True)
                End If
            End If
        End If


        'Als geen redirect is aangeroepen, doen dan een poging naar account.aspx. Deze bepaalt of ingelogd of uitgelogd
        Response.Redirect("~/" + me.global_ws.language + "/account/account.aspx", True)
    End Sub

    Private Function cookie_write(ByVal user_id As String) As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            'Reseting the ccockie by setting empty user and 10 days validity (10x24h)
            WriteCookieUtc("UserLoginId", user_id, 240)
        End If

        Return ll_resume
    End Function
End Class