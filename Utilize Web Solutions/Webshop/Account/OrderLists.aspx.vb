﻿Imports System.Web.Services

Partial Class order_lists
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_my_order_lists", "Bestellijst", Me.global_ws.Language)

        ' Controleer of de gebruiker is ingelogd
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim uc_account_block As Webshop.ws_user_control = Nothing
        uc_account_block = LoadUserControl("~/Webshop/Account/AccountBlocks/uc_order_lists.ascx")

        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone
        ph_zone.Controls.Add(uc_account_block)
    End Sub
End Class
