﻿
Partial Class OpenItems
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_open_items", "Openstaande posten overzicht", Me.global_ws.Language)

        ' Openstaande zichtbaar als de module gebruikersrechten niet aan staat of als de gebruiker recehten heeft.
        ' Anders omleiden naar de account pagina
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_oi") Then
                Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
            End If
        End If
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_openitems As Webshop.ws_user_control = LoadUserControl("AccountBlocks/uc_openitems.ascx")
        ph_zone.Controls.Add(uc_openitems)
    End Sub
End Class
