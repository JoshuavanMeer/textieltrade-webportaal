﻿Imports System.Web.Services

Partial Class order_entry
    Inherits CMSPageTemplate

    <WebMethod()> _
    Public Shared Function GetItems(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As Array
        Dim lst_items As ArrayList = New ArrayList()

        Dim lc_customer_id As String = contextKey.Substring(0, contextKey.IndexOf("|"))
        Dim lc_language As String = contextKey.Substring(contextKey.IndexOf("|") + 1)
        Dim lc_search_string As String = prefixText

        Dim _pf_product_filter As New ws_productfilter
        _pf_product_filter.include_sub_categories = True
        _pf_product_filter.use_categories = True
        _pf_product_filter.group_products = False
        _pf_product_filter.prod_desc = lc_search_string
        _pf_product_filter.prod_code = lc_search_string
        _pf_product_filter.prod_desc2 = lc_search_string
        _pf_product_filter.prod_comm = lc_search_string
        _pf_product_filter.prod_keywords = lc_search_string

        _pf_product_filter.page_size = count
        _pf_product_filter.page_number = 0

        Dim lc_sort_field As String = ""
        lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"

        ' Voer de query uit en bouw de repeater op
        Dim dvDataView As System.Data.DataView = _pf_product_filter.get_products(lc_language, lc_sort_field).DefaultView

        Dim lnTeller As Integer = 0
        For Each drDataRow As System.Data.DataRowView In dvDataView
            lnTeller = lnTeller + 1

            Dim lc_description As String = drDataRow.Item("prod_desc").ToString()

            If Not drDataRow.Item("grp_val1") = "" Then
                lc_description += " - " + drDataRow.Item("grp_val1")
            End If

            If Not drDataRow.Item("grp_val2") = "" Then
                lc_description += " - " + drDataRow.Item("grp_val2")
            End If


            Dim loItem As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("<span class=""prod_code"">" + drDataRow.Item("prod_code").ToString() + "</span><span class=""prod_desc"">" + lc_description + "</span>", drDataRow.Item("prod_code").ToString().PadRight(20, " "))

            If lnTeller = count Then
                Exit For
            End If

            lst_items.Add(loItem)
        Next

        Return lst_items.ToArray()
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if order entry module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_entry") Then
            Response.Redirect("~/home.aspx", True)
        End If

        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_my_order_entry", "Snel bestellen", Me.global_ws.Language)
    End Sub

    Private uc_account_block As Webshop.ws_user_control = Nothing
    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        uc_account_block = LoadUserControl("~/Webshop/Account/AccountBlocks/uc_order_entry.ascx")
        ph_zone.Controls.Add(uc_account_block)
    End Sub    
End Class
