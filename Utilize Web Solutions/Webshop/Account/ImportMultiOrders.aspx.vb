﻿
Partial Class ImportMultiOrders
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Controleer of de gebruiker is ingelogd, anders naar home
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        Else
            ' Als de module inkoopcombinaties aan staat, dan mag een gebruiker naar het mijn account gedeelte wanneer een gebruiker superuser is.
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") And Not Me.global_ws.purchase_combination.user_is_manager Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If

        ' Check if multi order import module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_address_book") And Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_MULTIORDER") Then
            Response.Redirect("~/home.aspx", True)
        End If

        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_multi_order", "Meerdere orders importeren", Me.global_ws.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_sales_customers As Webshop.ws_user_control = LoadUserControl("AccountBlocks/uc_import_multi_orders.ascx")
        ph_zone.Controls.Add(uc_sales_customers)

        If Not Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_MULTIORDER") Then
            Response.Redirect("~/" + Me.global_ws.Language + "/account/account.aspx", True)
        End If
    End Sub
End Class
