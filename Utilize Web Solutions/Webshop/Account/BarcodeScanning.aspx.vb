﻿Imports System.Web.Services

Partial Class BarcodeScanning
    Inherits CMSPageTemplate

    <WebMethod()> _
    Public Shared Function getProductDetails(ByVal TextId As String, ByVal Barcode As String, ByVal Quantity As Integer) As String
        Dim write_product_line As New StringBuilder
        Dim loGlobalWs As Utilize.Web.Solutions.Webshop.global_ws = Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws()

        ' Check if user has been logged on
        If loGlobalWs.user_information.user_logged_on Then
            ' Check if prod_code value has been sent
            If Not String.IsNullOrEmpty(Barcode) Then
                Dim product_code As String = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_code", Barcode, "bar_code")
                If product_code = "" Then product_code = Barcode

                Dim product_order_value As String = "1"

                'Add an extra line number from session to make elements ids unique, so we can add multiple lines with 
                'same product number in the shopping cart
                Dim new_line = CInt(HttpContext.Current.Session("product_line")) + 1
                HttpContext.Current.Session("product_line") = new_line

                Dim qsc_product As New Utilize.Data.QuerySelectClass
                qsc_product.select_fields = "*"
                qsc_product.select_from = "uws_products"
                qsc_product.select_where = "prod_show = 1 and prod_code = @prod_code"
                qsc_product.add_parameter("prod_code", product_code)

                Dim dt_product As System.Data.DataTable = qsc_product.execute_query()
                ' If prod_code valid, then ad product line to table
                If dt_product.Rows.Count > 0 Then
                    Dim sales_quantity As String = Utilize.Data.API.Webshop.product_manager.get_product_unit_quantity(product_code, "")
                    sales_quantity = sales_quantity.ToString().TrimEnd("0")
                    If sales_quantity.EndsWith(",") Then sales_quantity = sales_quantity.Substring(0, sales_quantity.Length - 1)

                    Dim text_prod_amount As New System.Web.UI.WebControls.TextBox
                    text_prod_amount.Attributes.Add("data-barcode-target", dt_product.Rows(0).Item("bar_code").replace(" ", "_|_"))

                    If Utilize.Data.DataProcedures.GetValue("uws_settings", "barcodesingleline", "UWS_SETTINGS") Then
                        text_prod_amount.ID = "txt_barcode_prod_" + dt_product.Rows(0).Item("prod_code").replace(" ", "_|_")
                    Else
                        text_prod_amount.ID = "txt_barcode_prod_" & new_line & "_" + dt_product.Rows(0).Item("prod_code").replace(" ", "_|_")
                    End If

                    text_prod_amount.CssClass = "form-control"
                    text_prod_amount.Style.Add("display", "inline-block")
                    text_prod_amount.Style.Add("text-align", "center")
                    text_prod_amount.Attributes.Add("Type", "Number")
                    text_prod_amount.Attributes.Add("Value", product_order_value)
                    text_prod_amount.Attributes.Add("min", "0")
                    text_prod_amount.Attributes.Add("runat", "server")
                    text_prod_amount.Width = New UI.WebControls.Unit(80)

                    Dim sb_prod_amount As New System.Text.StringBuilder()
                    Dim stWriter_prod_amount As New System.IO.StringWriter(sb_prod_amount)
                    Dim htmlWriter_prod_amount As New System.Web.UI.HtmlTextWriter(stWriter_prod_amount)

                    Try
                        text_prod_amount.RenderControl(htmlWriter_prod_amount)
                    Catch ex As Exception

                    End Try
                    write_product_line.AppendLine("<td style=""width: 140px; text-align: left;""><div id='div1_" + dt_product.Rows(0).Item("prod_code") + "'>" + dt_product.Rows(0).Item("prod_code") + "</div></td>")
                    write_product_line.AppendLine("<td style=""text-align: left;""><div id='div2_" + dt_product.Rows(0).Item("prod_code") + "'>" + dt_product.Rows(0).Item("prod_desc") + "</div></td>")
                    write_product_line.AppendLine("<td style=""width: 100px; text-align: right;"">")

                    Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not loGlobalWs.show_prices And loGlobalWs.user_information.user_logged_on
                    If Not prices_not_visible Then
                        write_product_line.AppendLine("<div id='div3_" + dt_product.Rows(0).Item("prod_code") + "'>" + Format(dt_product.Rows(0).Item("prod_price"), "c").ToString + "</div>")
                    End If

                    write_product_line.AppendLine("</td>")
                    write_product_line.AppendLine("<td style=""text-align: center;""><div id='div4_" + dt_product.Rows(0).Item("prod_code") + "'>" + sb_prod_amount.ToString + "&nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;" + sales_quantity + "</div></td>")
                    write_product_line.AppendLine("<td><input type=""submit"" value=""x"" id=""sub_remove_" + dt_product.Rows(0).Item("prod_code").replace(" ", "_|_") + """ class=""inputbutton close"" onclick=""$('#div1_" + dt_product.Rows(0).Item("prod_code") + "').remove();$('#div2_" + dt_product.Rows(0).Item("prod_code") + "').remove();$('#div3_" + dt_product.Rows(0).Item("prod_code") + "').remove();$('#div4_" + dt_product.Rows(0).Item("prod_code") + "').remove();$(this).remove();return false;""></td>")
                Else
                    'Write error in other way than previously
                    write_product_line.AppendLine("<td style=""width: 140px; text-align: left;"">" + product_code)

                    write_product_line.AppendLine("</td><td style=""text-align: left;"">")
                    write_product_line.AppendLine(global_trans.translate_message("message_barcode_added_product_incorrect", "is niet gevonden in systeem.", loGlobalWs.Language))
                    write_product_line.AppendLine("</td><td style=""width: 100px; text-align: right;"" >")
                    write_product_line.AppendLine("</td><td style=""text-align: center;""></td><td></td>")
                End If
            End If
        End If

        Return write_product_line.ToString
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("account_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_my_barcode_entry", "Barcodes scannen", Me.global_ws.Language)

        ' Check if barcode scan module is active
        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_barcode_scan") Then
            Response.Redirect("~/home.aspx", True)
        End If

        ' Controleer of de gebruiker is ingelogd
        If Not Me.global_ws.user_information.user_logged_on Then
            Response.Redirect("~/home.aspx", True)
        End If

        ' Controleer of gebruiker mag bestellen
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            If Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") Then
                Response.Redirect("~/home.aspx", True)
            End If
        End If
    End Sub

    Private uc_barcode_Scanning As Webshop.ws_user_control = Nothing
    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        uc_barcode_Scanning = LoadUserControl("~/Webshop/Account/AccountBlocks/uc_barcode_scanning.ascx")
        ph_zone.Controls.Add(uc_barcode_Scanning)
    End Sub
End Class
