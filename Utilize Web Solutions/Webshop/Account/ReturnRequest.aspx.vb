﻿Imports System.Web.Services

Partial Class Webshop_Account_ReturnRequest
    Inherits CMSPageTemplate

    <WebMethod()> _
    Public Shared Function GetItems(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As Array
        Dim lst_items As ArrayList = New ArrayList()

        Dim lc_customer_id As String = contextKey.Substring(0, contextKey.IndexOf("|"))
        Dim lc_language As String = contextKey.Substring(contextKey.IndexOf("|") + 1)
        Dim lc_search_string As String = prefixText

        Dim _pf_product_filter As New ws_productfilter
        _pf_product_filter.include_sub_categories = True
        _pf_product_filter.use_categories = True
        _pf_product_filter.group_products = False
        _pf_product_filter.prod_desc = lc_search_string
        _pf_product_filter.prod_code = lc_search_string
        _pf_product_filter.prod_desc2 = lc_search_string
        _pf_product_filter.prod_comm = lc_search_string
        _pf_product_filter.prod_keywords = lc_search_string

        _pf_product_filter.page_size = count
        _pf_product_filter.page_number = 0

        ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
            ' Ophalen assortimentcode gekoppeld aan de ingelogde klant
            Dim lc_ass_code As String = Utilize.Data.DataProcedures.GetValue("uws_customers", "ass_code", lc_customer_id, "rec_id")

            ' Als de klant een assortimentcode heeft
            If Not String.IsNullOrEmpty(lc_ass_code) Then
                ' Pas hier het klantspecifieke assortimenten toe
                _pf_product_filter.use_categories = True

                ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True Then
                    Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                    If Not lc_category_string = "" Then
                        _pf_product_filter.extra_where += " and uws_products.prod_code in (select prod_code from uws_categories_prod where '" + lc_category_string + "' like '%|' + uws_categories_prod.cat_code + '|%')"
                    End If
                End If

                ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                    Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)
                    _pf_product_filter.extra_where += " and uws_products.prod_code not in (select prod_code from uws_categories_prod where '" + lc_category_exclusion_string + "' like '%|' + uws_categories_prod.cat_code + '|%')"
                End If
            End If
        End If

        ' Als de module landspecifieke assortimenten aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cnt_ass") = True Then
            Dim lc_cnt_code As String = Utilize.Data.DataProcedures.GetValue("uws_customers", "cnt_code", lc_customer_id, "rec_id")
            Dim lc_ass_code As String = Utilize.Data.DataProcedures.GetValue("uws_countries", "ass_code", lc_cnt_code)

            If Not lc_ass_code = "" Then
                _pf_product_filter.extra_where += " and uws_products.prod_code not in (select prod_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
                _pf_product_filter.extra_where += " and uws_products.prod_group not in (select grp_code from uws_cnt_ass_excl where ass_code = '" + lc_ass_code + "')"
            End If
        End If

        Dim lc_sort_field As String = ""
        lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"

        ' Voer de query uit en bouw de repeater op
        Dim dvDataView As System.Data.DataView = _pf_product_filter.get_products(lc_language, lc_sort_field).DefaultView

        Dim lnTeller As Integer = 0
        For Each drDataRow As System.Data.DataRowView In dvDataView
            lnTeller = lnTeller + 1

            Dim lc_description As String = drDataRow.Item("prod_desc").ToString()

            If Not drDataRow.item("grp_val1") = "" Then
                lc_description += " - " + drDataRow.item("grp_val1")
            End If

            If Not drDataRow.item("grp_val2") = "" Then
                lc_description += " - " + drDataRow.item("grp_val2")
            End If


            Dim loItem As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("<span class=""prod_code"">" + drDataRow.Item("prod_code").ToString() + "</span><span class=""prod_desc"">" + lc_description + "</span>", drDataRow.Item("prod_code").ToString().PadRight(20, " "))

            If lnTeller = count Then
                Exit For
            End If

            lst_items.Add(loItem)
        Next

        Return lst_items.ToArray()
    End Function

    <WebMethod()> Public Shared Function GetBulkItems(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As Array
        Dim lst_items As ArrayList = New ArrayList()

        Dim lc_customer_id As String = contextKey.Substring(0, contextKey.IndexOf("|"))
        Dim lc_language As String = contextKey.Substring(contextKey.IndexOf("|") + 1)
        Dim lc_search_string As String = prefixText

        Dim _pf_product_filter As New ws_productfilter
        _pf_product_filter.include_sub_categories = True
        _pf_product_filter.use_categories = True
        _pf_product_filter.group_products = False
        _pf_product_filter.prod_desc = lc_search_string
        _pf_product_filter.prod_code = lc_search_string
        _pf_product_filter.prod_desc2 = lc_search_string
        _pf_product_filter.prod_comm = lc_search_string
        _pf_product_filter.prod_keywords = lc_search_string

        _pf_product_filter.page_size = count
        _pf_product_filter.page_number = 0

        _pf_product_filter.extra_where += " and uws_products.prod_code in (select prod_code from uws_history_lines where hdr_id in (select crec_id from uws_history where cust_id = '" + lc_customer_id + "'))"


        Dim lc_sort_field As String = ""
        lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"

        ' Voer de query uit en bouw de repeater op
        Dim dvDataView As System.Data.DataView = _pf_product_filter.get_products(lc_language, lc_sort_field).DefaultView

        Dim lnTeller As Integer = 0
        For Each drDataRow As System.Data.DataRowView In dvDataView
            lnTeller = lnTeller + 1

            Dim lc_description As String = drDataRow.Item("prod_desc").ToString()

            If Not drDataRow.Item("grp_val1") = "" Then
                lc_description += " - " + drDataRow.Item("grp_val1")
            End If

            If Not drDataRow.Item("grp_val2") = "" Then
                lc_description += " - " + drDataRow.Item("grp_val2")
            End If


            Dim loItem As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("<span class=""prod_code"">" + drDataRow.Item("prod_code").ToString() + "</span><span class=""prod_desc"">" + lc_description + "</span>", drDataRow.Item("prod_code").ToString().PadRight(20, " "))

            If lnTeller = count Then
                Exit For
            End If

            lst_items.Add(loItem)
        Next

        Return lst_items.ToArray()
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim lc_template_code As String = Me.global_rma.get_rma_setting("rma_template")

        If Session("log_in_app") Then
            Me.MasterPageFile = "~/CMS/MasterPages/AccountAppMasterPage_1.master"
            lc_template_code = Me.global_ws.get_webshop_setting("account_app_template")
        End If

        If lc_template_code.Trim() = "" Then
            lc_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        End If

        Me.cms_template_code = lc_template_code
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_returns_request", "Retour Aanvragen", Me.global_ws.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_orders As Webshop.ws_user_control = LoadUserControl("AccountBlocks/uc_return_request.ascx")

        ph_zone.Controls.Add(uc_orders)
    End Sub
End Class
