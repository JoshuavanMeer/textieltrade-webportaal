﻿
Imports System.Data

Partial Class Webshop_Account_open_document
    Inherits Utilize.Web.Solutions.CMS.cms_page_base

    Private _doc_nr As String = ""
    Private _cust_id As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True
        Dim ll_printing_visible As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_conc_or_prin")

        Dim lc_document_type As String = Me.get_page_parameter("DocType")
        Dim lb_document As Byte() = Nothing

        Select Case lc_document_type.ToLower()
            Case "invoice"
                ' Tonen van een kopiefactuur in PDF
                If ll_resume Then
                    _doc_nr = Me.get_page_parameter("DocNr")
                    _cust_id = Me.global_ws.user_information.user_customer_id

                    lb_document = Me.get_invoice()
                End If

                If ll_resume Then
                    ll_resume = Not lb_document Is Nothing

                    If Not ll_resume Then
                        ' Dan een melding geven dat het document niet gevonden/geopend kan worden
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentError", "alert('" + global_trans.translate_message("message_document_error", "Het document is niet gevonden.", Me.global_ws.Language) + "'); window.close();", True)
                    End If
                End If

                If ll_resume Then
                    Dim lc_file_name As String = _doc_nr.Trim() + ".pdf"
                    Me.write_document(lb_document, lc_file_name)
                End If
            Case "delivery"
                ' Tonen van een kopiefactuur in PDF
                If ll_resume Then
                    _doc_nr = Me.get_page_parameter("DocNr")
                    _cust_id = Me.global_ws.user_information.user_customer_id

                    lb_document = Me.get_invoice()
                End If

                If ll_resume Then
                    ll_resume = Not lb_document Is Nothing

                    If Not ll_resume Then
                        ' Dan een melding geven dat het document niet gevonden/geopend kan worden
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentError", "alert('" + global_trans.translate_message("message_document_error", "Het document is niet gevonden.", Me.global_ws.Language) + "'); window.close();", True)
                    End If
                End If

                If ll_resume Then
                    Dim lc_file_name As String = _doc_nr.Trim() + ".pdf"
                    Me.write_document(lb_document, lc_file_name)
                End If
            Case "confirmation"
                ' Tonen van een orderbevestiging in HTML
                If ll_resume Then
                    _doc_nr = Me.get_page_parameter("DocNr")
                    _cust_id = Me.global_ws.user_information.user_customer_id
                End If

                If ll_resume Then
                    Me.lt_html_string.Text = Utilize.Data.DataProcedures.GetValue("uws_so_orders", "order_confirmation", _doc_nr + _cust_id, "ord_nr + cust_id")

                    ll_resume = Not Me.lt_html_string.Text.Trim() = ""

                    If Not ll_resume Then
                        ' Dan een melding geven dat het document niet gevonden/geopend kan worden
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentError", "alert('" + global_trans.translate_message("message_document_error", "Het document is niet gevonden.", Me.global_ws.Language) + "'); window.close();", True)
                    End If
                End If
            Case "productdocument"
                ' Tonen van een product document
                ' Tonen van een kopiefactuur in PDF
                If ll_resume Then
                    _doc_nr = Me.get_page_parameter("DocNr")
                    _cust_id = Me.global_ws.user_information.user_customer_id

                    lb_document = Me.get_product_document()
                End If

                If ll_resume Then
                    ll_resume = Not lb_document Is Nothing

                    If Not ll_resume Then
                        ' Dan een melding geven dat het document niet gevonden/geopend kan worden
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentError", "alert('" + global_trans.translate_message("message_document_error", "Het document is niet gevonden.", Me.global_ws.Language) + "'); window.close();", True)
                    End If
                End If

                If ll_resume Then
                    Dim lc_file_name As String = Utilize.Data.DataProcedures.GetValue("uws_product_doc", "file_name", Me._doc_nr, "crec_id")
                    Me.write_document(lb_document, lc_file_name)
                End If
            Case "concorder"
                ' Tonen van een conceptfactuur in PDF
                If ll_resume Then
                    ll_resume = ll_printing_visible
                End If

                If ll_resume Then
                    _doc_nr = Me.get_page_parameter("DocNr")
                    _cust_id = Me.global_ws.user_information.user_customer_id

                    lb_document = Me.get_concept_order_document()
                End If

                If ll_resume Then
                    ll_resume = Not lb_document Is Nothing

                    If Not ll_resume Then
                        ' Dan een melding geven dat het document niet gevonden/geopend kan worden
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentError", "alert('" + global_trans.translate_message("message_document_error", "Het document is niet gevonden.", Me.global_ws.Language) + "'); window.close();", True)
                    End If
                End If

                If ll_resume Then
                    Dim lc_file_name As String = _doc_nr.Trim() + ".pdf"
                    Me.write_document(lb_document, lc_file_name)
                End If

            Case Else
                Exit Select
        End Select
    End Sub

#Region " Invoice functions"
    Private Function get_invoice() As Byte()
        Dim lb_return As Byte() = Nothing

        Try
            Dim udt_data_table As New Utilize.Data.DataTable
            udt_data_table.table_name = "uws_history"
            udt_data_table.table_init()

            udt_data_table.add_where("and doc_nr = @doc_nr")
            udt_data_table.add_where("and cust_id = @cust_id")

            udt_data_table.add_where_parameter("doc_nr", _doc_nr)
            udt_data_table.add_where_parameter("cust_id", _cust_id)

            udt_data_table.table_load()

            If udt_data_table.data_row_count > 0 Then
                lb_return = CType(udt_data_table.field_get("ord_document"), Byte())
            End If
        Catch ex As Exception

        End Try

        Return lb_return
    End Function

    Private Sub write_document(ByVal lb_document As Byte(), ByVal lc_file_name As String)

        Response.Clear()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-length", lb_document.Length.ToString())
        Response.AddHeader("Content-Disposition", "attachment;filename=" + lc_file_name)

        Response.BinaryWrite(lb_document)
        Response.End()
    End Sub
#End Region

#Region " Product document functies"
    Private Function get_product_document() As Byte()
        Dim lb_return As Byte() = Nothing

        Try
            Dim udt_data_table As New Utilize.Data.DataTable
            udt_data_table.table_name = "uws_product_doc"
            udt_data_table.table_init()

            udt_data_table.add_where("and crec_id = @crec_id")

            udt_data_table.add_where_parameter("crec_id", _doc_nr)
            udt_data_table.table_load()

            If udt_data_table.data_row_count > 0 Then
                lb_return = CType(udt_data_table.field_get("doc_content"), Byte())
            End If
        Catch ex As Exception

        End Try

        Return lb_return
    End Function
#End Region

#Region "Concept order document functies"
    Private Function get_concept_order_document() As Byte()
        Dim lb_return As Byte() = Nothing
        Dim ll_print As Boolean = False
        Dim sb_html_concept_order As New StringBuilder

        ' Fetch print pdf layout if any
        Dim qsc_print_pdf As New Utilize.Data.QuerySelectClass
        qsc_print_pdf.select_fields = "*"
        qsc_print_pdf.select_from = "uws_concept_print"

        Dim dt_print_pdf As DataTable = qsc_print_pdf.execute_query

        If Not IsNothing(dt_print_pdf) Then
            If dt_print_pdf.Rows.Count > 0 Then
                ll_print = True
            End If
        End If

        If ll_print Then
            ' Convert concept order
            sb_html_concept_order.AppendLine(convert_variables(dt_print_pdf.Rows(0).Item("body_nl"), dt_print_pdf.Rows(0).Item("order_line_style")))
        End If

        If ll_print Then
            ' Print pdf to result
            Dim loPDFGenerate As New ExpertPdf.HtmlToPdf.PdfConverter
            loPDFGenerate.LicenseKey = "Apxofk9W45ihltLQtKYelQ7h9+0Jm3o+D/mGrPycnO7rHozfB9KwzbnQ7M6ZjIGE"

            loPDFGenerate.PdfHeaderOptions.DrawHeaderLine = False
            loPDFGenerate.PdfFooterOptions.DrawFooterLine = False

            ' Create pdf
            If sb_html_concept_order.Length > 0 Then
                lb_return = loPDFGenerate.GetPdfBytesFromHtmlString(sb_html_concept_order.ToString)
            End If
        End If

        Return lb_return
    End Function

    Private Function get_invoice_address(ByVal datarow_concept_order As DataRow) As String
        Dim lc_name As String = datarow_concept_order.Item("inv_fst_name") + " " + (datarow_concept_order.Item("inv_infix") + " " + datarow_concept_order.Item("inv_lst_name")).trim()

        Dim lc_invoice_adress As String = ""
        lc_invoice_adress += datarow_concept_order.Item("inv_bus_name") + "<br />"

        If Not lc_name.Trim() = "" Then
            lc_invoice_adress += lc_name.Trim() + "<br />"
        End If

        Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", datarow_concept_order.Item("inv_cnt_code"))

        Select Case ln_address_layout
            Case 0
                lc_invoice_adress += datarow_concept_order.Item("inv_address") + " " + "<br />"
            Case 1
                lc_invoice_adress += datarow_concept_order.Item("inv_street") + " "
                lc_invoice_adress += datarow_concept_order.Item("inv_house_nr") + ""
                lc_invoice_adress += datarow_concept_order.Item("inv_house_add") + " " + "<br />"
            Case 2
                lc_invoice_adress += datarow_concept_order.Item("inv_house_nr") + " "
                lc_invoice_adress += datarow_concept_order.Item("inv_street") + " "
                lc_invoice_adress += datarow_concept_order.Item("inv_house_add") + " " + "<br />"
            Case Else
                Exit Select
        End Select

        lc_invoice_adress += datarow_concept_order.Item("inv_zip_code") + " "
        lc_invoice_adress += datarow_concept_order.Item("inv_city") + " " + "<br />"
        lc_invoice_adress += Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_ws.Language, datarow_concept_order.Item("inv_cnt_code"))

        Return lc_invoice_adress
    End Function

    Private Function get_delivery_address(ByVal datarow_concept_order As DataRow) As String
        Dim lc_delivery_address As String = ""

        ' Als er een aangepast afleveradres is, dan de juiste gegevens retourneren
        If datarow_concept_order.Item("del_address_custom") Then
            Dim lc_name As String = datarow_concept_order.Item("del_fst_name") + " " + (datarow_concept_order.Item("del_infix") + " " + datarow_concept_order.Item("del_lst_name")).trim()

            lc_delivery_address += datarow_concept_order.Item("del_bus_name") + "<br />"

            If Not lc_name.Trim() = "" Then
                lc_delivery_address += lc_name + "<br />"
            End If

            Dim ln_address_layout As Integer = Utilize.Data.DataProcedures.GetValue("uws_countries", "address_layout", datarow_concept_order.Item("del_cnt_code"))

            Select Case ln_address_layout
                Case 0
                    lc_delivery_address += datarow_concept_order.Item("del_address") + " " + "<br />"
                Case 1
                    lc_delivery_address += datarow_concept_order.Item("del_street") + " "
                    lc_delivery_address += datarow_concept_order.Item("del_house_nr") + ""
                    lc_delivery_address += datarow_concept_order.Item("del_house_add") + " " + "<br />"
                Case 2
                    lc_delivery_address += datarow_concept_order.Item("del_house_nr") + " "
                    lc_delivery_address += datarow_concept_order.Item("del_street") + " "
                    lc_delivery_address += datarow_concept_order.Item("del_house_add") + " " + "<br />"
                Case Else
                    Exit Select
            End Select

            lc_delivery_address += datarow_concept_order.Item("del_zip_code") + " "
            lc_delivery_address += datarow_concept_order.Item("del_city") + " " + "<br />"

            lc_delivery_address += Utilize.Data.DataProcedures.GetValue("uws_countries", "cnt_desc_" + Me.global_ws.Language, datarow_concept_order.Item("del_cnt_code"))
        Else
            ' Geen aangepast afleveradres dus factuuradres retourneren
            lc_delivery_address = get_invoice_address(datarow_concept_order)
        End If

        Return lc_delivery_address
    End Function

    Private Function convert_variables(ByVal convert_string As String, Optional ByVal line_style As String = Nothing) As String
        Dim lc_cc_empty As String = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_coupon_code_empty", "Geen couponcode toegepast", Me.global_ws.Language)
        Dim ll_concept_order As Boolean = False

        Dim qsc_concept_orders As New Utilize.Data.QuerySelectClass
        qsc_concept_orders.select_fields = "*"
        qsc_concept_orders.select_from = "uws_orders"
        qsc_concept_orders.select_where = "rec_id = @rec_id and cust_id = @cust_id"
        qsc_concept_orders.add_parameter("@rec_id", _doc_nr)
        qsc_concept_orders.add_parameter("@cust_id", _cust_id)

        Dim dt_concept_order As DataTable = qsc_concept_orders.execute_query
        Dim datarow_concept_order As DataRow = Nothing
        If Not IsNothing(dt_concept_order) Then
            If dt_concept_order.Rows.Count > 0 Then
                ll_concept_order = True
                datarow_concept_order = dt_concept_order.Rows(0)
            End If
        End If

        If ll_concept_order Then
            Dim lc_coupon_code As String = datarow_concept_order.Item("cpn_code").ToString()

            ' Als er geen couponcode is, meld dan dat er geen couponcode is
            If lc_coupon_code.Length < 1 Then
                convert_string = convert_string.Replace("==COUPONCODE==", lc_cc_empty)
            Else
                convert_string = convert_string.Replace("==COUPONCODE==", lc_coupon_code)
            End If

            convert_string = convert_string.Replace("==COUPONDISCOUNT_EXCL_VAT==", Format(datarow_concept_order.Item("cpn_amt"), "c"))
            convert_string = convert_string.Replace("==COUPONDISCOUNT_INCL_VAT==", Format(datarow_concept_order.Item("cpn_amt") + datarow_concept_order.Item("cpn_vat_amt"), "c"))

            convert_string = convert_string.Replace("==ORDERNUMBER==", datarow_concept_order.Item("rec_id"))
            convert_string = convert_string.Replace("==ORDERNUMBERAV==", "")
            convert_string = convert_string.Replace("==ORDERDATE==", Format(datarow_concept_order.Item("ord_date"), "d"))
            convert_string = convert_string.Replace("==ORDERDESCRIPTION==", datarow_concept_order.Item("order_description"))
            convert_string = convert_string.Replace("==ORDERCOMMENTS==", datarow_concept_order.Item("order_comments"))

            convert_string = convert_string.Replace("==PAYMENTMETHOD==", Utilize.Data.DataProcedures.GetValue("uws_pay_types", "pt_desc_" + Me.global_ws.Language, datarow_concept_order.Item("payment_type")))
            convert_string = convert_string.Replace("==SHIPMENTMETHOD==", Utilize.Data.DataProcedures.GetValue("uws_ship_methods", "sm_desc", datarow_concept_order.Item("sm_code")))

            convert_string = convert_string.Replace("==FULLNAME==", Me.global_ws.user_information.user_full_name)
            convert_string = convert_string.Replace("==INVOICEADDRESS==", Me.get_invoice_address(datarow_concept_order))
            convert_string = convert_string.Replace("==DELIVERYADDRESS==", Me.get_delivery_address(datarow_concept_order))

            convert_string = convert_string.Replace("==SHIPPINGCOSTS_EXCL_VAT==", Format(datarow_concept_order.Item("shipping_costs"), "c"))
            convert_string = convert_string.Replace("==SHIPPINGCOSTS_INCL_VAT==", Format(datarow_concept_order.Item("shipping_costs") + datarow_concept_order.Item("vat_ship"), "c"))

            convert_string = convert_string.Replace("==ORDERCOSTS_EXCL_VAT==", Format(datarow_concept_order.Item("order_costs"), "c"))
            convert_string = convert_string.Replace("==ORDERCOSTS_INCL_VAT==", Format(datarow_concept_order.Item("order_costs") + datarow_concept_order.Item("vat_ord"), "c"))

            convert_string = convert_string.Replace("==SUBTOTAL_EXCL_VAT==", Format(datarow_concept_order.Item("order_total"), "c"))
            convert_string = convert_string.Replace("==SUBTOTAL_INCL_VAT==", Format(datarow_concept_order.Item("vat_total") + datarow_concept_order.Item("order_total"), "c"))

            convert_string = convert_string.Replace("==ORDERTOTAL_EXCL_VAT==", Format(datarow_concept_order.Item("shipping_costs") + datarow_concept_order.Item("order_costs") + datarow_concept_order.Item("order_total") - datarow_concept_order.Item("cpn_amt"), "c"))
            convert_string = convert_string.Replace("==ORDERTOTAL_INCL_VAT==", Format(datarow_concept_order.Item("shipping_costs") + datarow_concept_order.Item("vat_ship") + datarow_concept_order.Item("order_costs") + datarow_concept_order.Item("vat_ord") + datarow_concept_order.Item("vat_total") + datarow_concept_order.Item("order_total") - datarow_concept_order.Item("cpn_amt") - datarow_concept_order.Item("cpn_vat_amt"), "c"))

            convert_string = convert_string.Replace("==VATTOTAL==", Format(datarow_concept_order.Item("vat_total"), "c"))
            convert_string = convert_string.Replace("==VATTOTAL_ORDER==", Format(datarow_concept_order.Item("vat_total") + datarow_concept_order.Item("vat_ship") + datarow_concept_order.Item("vat_ord") - datarow_concept_order.Item("cpn_vat_amt"), "c"))

            ' Write signature if any
            ' TODO: check if it ends up in pdf
            If datarow_concept_order.Item("signature") = "" Then
                convert_string = convert_string.Replace("==SIGNATURE==", "")
            Else
                Dim base64string As String = datarow_concept_order.Item("signature")
                Dim ord_nr As String = datarow_concept_order.Item("ord_nr")
                Dim path As String = System.Web.HttpContext.Current.Server.MapPath("~/Documents/Signatures")

                If Not System.IO.Directory.Exists(path) Then
                    System.IO.Directory.CreateDirectory(path)
                End If

                If Not System.IO.File.Exists(path + "\" + ord_nr.Trim() + ".jpg") Then
                    Dim binaryData() As Byte = Convert.FromBase64String(base64string)
                    Dim fs As New System.IO.FileStream(path + "\" + ord_nr.Trim() + ".jpg", System.IO.FileMode.CreateNew)

                    ' write it out
                    fs.Write(binaryData, 0, binaryData.Length)

                    ' close it down.
                    fs.Close()
                End If
                Dim lc_convert_string As String = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_signature", "Handtekening:", Me.global_ws.Language)
                lc_convert_string = lc_convert_string + "<br><img src=""cid:" + datarow_concept_order.Item("ord_nr") + """ />"

                convert_string = convert_string.Replace("==SIGNATURE==", lc_convert_string)
            End If

            ' Convert orderlines if wished 
            If Not IsNothing(line_style) Then
                convert_string = convert_string.Replace("==ORDERLINES==", Me.create_order_lines(line_style))
            End If
        End If

        ' ==ORDERNUMBER==
        ' ==FULLNAME==,
        ' ==ORDERLINES==
        ' ==ORDERTOTAL_EXCL_VAT==
        ' ==VATTOTAL==
        ' ==ORDERTOTAL_INCL_VAT==
        ' ==INVOICEADDRESS==
        ' ==DELIVERYADRESS==
        Return convert_string
    End Function

    Private Function create_order_lines(ByVal line_style As String) As String
        Dim lc_line_string As String = ""
        Dim ll_concept_order_lines As Boolean = False

        Dim qsc_concept_order_lines As New Utilize.Data.QuerySelectClass
        qsc_concept_order_lines.select_fields = "*"
        qsc_concept_order_lines.select_from = "uws_concept_pri_line"
        qsc_concept_order_lines.select_where = "hdr_id in (select top 1 rec_id from uws_concept_print)"

        Dim dt_concept_order_lines As DataTable = qsc_concept_order_lines.execute_query

        If Not IsNothing(dt_concept_order_lines) Then
            If dt_concept_order_lines.Rows.Count > 0 Then
                ll_concept_order_lines = True
            End If
        End If

        If ll_concept_order_lines Then
            lc_line_string += "<table style=""" + line_style + """> "

            ''''''''''''''''''''''''''''''''''''''''''''''''
            '' Bouw hier de kolommen op
            ''''''''''''''''''''''''''''''''''''''''''''''''
            ' Maak een nieuwe tabelregel aan
            lc_line_string += "<tr>"

            ' Loop door de headers heen
            For Each col_row As DataRow In dt_concept_order_lines.Rows
                ' Maak een nieuwe kolom aan en zorg dat deze correct uitgelijnd wordt
                Dim lc_fld_align As String = col_row.Item("fld_align")
                Select Case lc_fld_align
                    Case "1"
                        lc_line_string += "<th style=""text-align: left; " + col_row.Item("col_style") + """ >"
                    Case "2"
                        lc_line_string += "<th style=""text-align: center; " + col_row.Item("col_style") + """ >"
                    Case "3"
                        lc_line_string += "<th style=""text-align: right; " + col_row.Item("col_style") + """ >"
                    Case Else
                        Exit Select
                End Select

                lc_line_string += col_row.Item("col_title_" + Me.global_ws.Language)
                lc_line_string += "</th>"
            Next

            lc_line_string += "</tr>"

            ''''''''''''''''''''''''''''''''''''''''''''''''
            '' Bouw hier de regels op
            ''''''''''''''''''''''''''''''''''''''''''''''''
            Dim qsc_order_lines As New Utilize.Data.QuerySelectClass
            qsc_order_lines.select_fields = "*"
            qsc_order_lines.select_from = "uws_order_lines"
            qsc_order_lines.select_where = "hdr_id = @hdr_id"
            qsc_order_lines.add_parameter("hdr_id", _doc_nr)

            Dim dt_order_lines As DataTable = qsc_order_lines.execute_query

            If Not IsNothing(dt_order_lines) Then
                If dt_concept_order_lines.Rows.Count > 0 Then
                    ' Loop door de orderregels heen
                    For Each order_row As DataRow In dt_order_lines.Rows
                        ' Maak een nieuwe tabelregel aan
                        lc_line_string += "<tr>"

                        ' Loop voor iedere orderregel door de mail regels
                        For Each mail_row As DataRow In dt_concept_order_lines.Rows
                            ' Maak een nieuwe kolom aan en zorg dat deze correct uitgelijnd wordt
                            Dim ln_fld_align As Integer = mail_row.Item("fld_align")
                            Select Case ln_fld_align
                                Case "1"
                                    lc_line_string += "<td style=""text-align: left; " + mail_row.Item("fld_style") + """>"
                                Case "2"
                                    lc_line_string += "<td style=""text-align: center; " + mail_row.Item("fld_style") + """>"
                                Case "3"
                                    lc_line_string += "<td style=""text-align: right; " + mail_row.Item("fld_style") + """>"
                                Case Else
                                    Exit Select
                            End Select

                            'Vul hier de juiste waarde in in de kolom
                            Dim lc_fld_code As String = mail_row.Item("fld_code")
                            Select Case lc_fld_code
                                Case "PROD_IMAGE1"
                                    Dim lc_image_url As String = Utilize.Data.DataProcedures.GetValue("uws_products", "thumbnail_normal", order_row.Item("prod_code"), "prod_code")

                                    If lc_image_url = "" Then
                                        lc_image_url = "Documents/ProductImages/not-available-thumbnail-small.jpg"
                                    End If

                                    lc_line_string += "<img src=""http://" + Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("website_domain") + "/" + lc_image_url + """ border=0 /><br>" + order_row.Item("prod_code")
                                Case "PROD_IMAGE2"
                                    Dim lc_image_url As String = Utilize.Data.DataProcedures.GetValue("uws_products", "thumbnail_large", order_row.Item("prod_code"), "prod_code")

                                    If lc_image_url = "" Then
                                        lc_image_url = "Documents/ProductImages/not-available-thumbnail-normal.jpg"
                                    End If

                                    lc_line_string += "<img src=""http://" + Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("website_domain") + "/" + lc_image_url + """ border=0 /><br>" + order_row.Item("prod_code")
                                Case "PROD_CODE"
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(order_row.Item("PROD_CODE"))
                                Case "PROD_DESC"
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(order_row.Item("PROD_DESC")).Replace("&lt;br&gt;", "<br>")
                                Case "SIZE"
                                    ' Maat
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(order_row.Item("SIZE"))
                                Case "PROD_DESC_COMM"
                                    ' Omschrijving product + regelopmerkingen
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(order_row.Item("PROD_DESC")).Replace("&lt;br&gt;", "<br>")

                                    If Not order_row.Item("user_id") = "" Then
                                        Dim lc_translation As String = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_ordered_for_employee", "Besteld voor:", Me.global_ws.Language) + "&nbsp;"
                                        lc_line_string += "<br>" + lc_translation

                                        If Not order_row.Item("user_id") = "" Then
                                            Dim lo_qsc As New Utilize.Data.QuerySelectClass()
                                            lo_qsc.select_fields = "fst_name, lst_name, infix, title, rec_id"
                                            lo_qsc.select_from = "uws_customers_users"
                                            lo_qsc.select_where = "rec_id = @rec_id"
                                            lo_qsc.add_parameter("@rec_id", order_row.Item("user_id"))

                                            Dim dt_current_user As System.Data.DataTable = lo_qsc.execute_query()

                                            If dt_current_user.Rows.Count > 0 Then
                                                lc_line_string += (dt_current_user.Rows(0).Item("fst_name") + " " + dt_current_user.Rows(0).Item("infix")).trim() + " " + dt_current_user.Rows(0).Item("lst_name")
                                            End If
                                        End If
                                    End If

                                    If Not order_row.Item("comment").ToString().Trim() = "" Then
                                        Dim lc_translation As String = Utilize.Web.Solutions.Translations.global_trans.translate_label("label_product_comment", "Opmerkingen:", Me.global_ws.Language) + "&nbsp;"
                                        lc_line_string += "<br>" + lc_translation + System.Web.HttpUtility.HtmlEncode(order_row.Item("COMMENT"))
                                    End If
                                Case "ORD_QTY"
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(System.Math.Round(order_row.Item("ORD_QTY"), 0).ToString())
                                Case "BACK_QTY"
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(System.Math.Round(order_row.Item("BACK_QTY"), 0).ToString())
                                Case "PROD_PRICE_ORIGINAL"
                                    ' Oorspronkelijke product prijs
                                    Dim ln_prod_price As Decimal = Utilize.Data.DataProcedures.GetValue("UWS_PRODUCTS", "PROD_PRICE", order_row.Item("PROD_CODE"), "prod_code")

                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_prod_price, "c"))
                                Case "PROD_PRICE_INCL_VAT"
                                    Dim ln_vat As Decimal = (order_row.Item("vat_amt") / order_row.Item("ord_qty"))
                                    Dim ln_prod_price As Decimal = order_row.Item("prod_price")

                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_vat + ln_prod_price, "c"))
                                Case "PROD_PRICE_EXCL_VAT"
                                    Dim ln_prod_price As Decimal = order_row.Item("prod_price")
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_prod_price, "c"))
                                Case "PROD_VAT"
                                    Dim ln_vat As Decimal = (order_row.Item("vat_amt") / order_row.Item("ord_qty"))
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_vat, "c"))
                                Case "VAT_TOTAL"
                                    Dim ln_vat_total As Decimal = order_row.Item("vat_amt")
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_vat_total, "c"))
                                Case "ROW_TOT_EXCL_VAT"
                                    Dim ln_row_tot As Decimal = order_row.Item("row_amt")
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_row_tot, "c"))
                                Case "ROW_TOT_INCL_VAT"
                                    Dim ln_row_tot As Decimal = order_row.Item("row_amt")
                                    Dim ln_vat_total As Decimal = order_row.Item("vat_amt")
                                    lc_line_string += System.Web.HttpUtility.HtmlEncode(Format(ln_row_tot + ln_vat_total, "c"))
                                Case Else
                                    lc_line_string += ""
                            End Select

                            ' End field
                            lc_line_string += "</td>"
                        Next

                        ' End data row line
                        lc_line_string += "</tr>"
                    Next
                End If
            End If

            lc_line_string += "</table>"
        End If

        Return lc_line_string
    End Function
#End Region

End Class
