﻿Imports System.Web.UI.WebControls

Partial Class uc_payment_request
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private testmode As Boolean = False
    Private dt_payment_methods As System.Data.DataTable = Nothing
    Private qc_payment_methods As New Utilize.Data.QueryCustomSelectClass

    Private WithEvents _page As System.Web.UI.Page = Nothing

    Public ReadOnly Property payment_type As String
        Get
            Dim str As String = Me.get_payment_code()
            Return str.Split("|")(0).Trim()
        End Get
    End Property

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Hier gaan we een aantal dingen controleren
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Utilize.Data.DataProcedures.GetValue("uws_so_orders", "cust_id", Me.get_page_parameter("orderid"), "av_ord_nr + payment_id") = ""
        End If

        If ll_resume Then
            set_payment_methods()
            Me.set_style_sheet("uc_payment_request.css", Me)
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub

    Private Sub set_payment_methods()

        ' Ophalen betalingsmethoden van Ogone
        Dim lc_query As String = ""
        lc_query += "SELECT pt_code AS code, pt_desc_" & Me.global_cms.Language & " AS description, logo"
        lc_query += " FROM uws_pay_types"
        lc_query += " WHERE hdr_id IN (SELECT rec_id FROM uws_pay WHERE PAY_PROVIDER = 'OGONE')"
        lc_query += " and pt_online = 1 and on_account_enab = 0"
        lc_query += " ORDER BY ROW_ORD"

        ' Uitvoeren sql query
        qc_payment_methods.QueryString = lc_query
        dt_payment_methods = qc_payment_methods.execute_query(Me.global_ws.user_information.user_id)

        If dt_payment_methods.Rows.Count > 0 Then
            Dim ht_html_table As New HtmlTable

            For Each dr_data_row In dt_payment_methods.Rows
                Dim htr_table_row As New HtmlTableRow

                ' Afbeelding
                Dim htc_table_cell1 As New HtmlTableCell
                If Not dr_data_row.item("logo").trim() = "" Then
                    htc_table_cell1.InnerHtml = "<img src=""" + Me.ResolveCustomUrl("~/" + dr_data_row.item("logo")) + """ />"
                End If

                ' Radiobutton
                Dim htc_table_cell2 As New HtmlTableCell
                Dim rb_radio_button As New RadioButton
                rb_radio_button.ID = dr_data_row.item("code")
                rb_radio_button.AutoPostBack = True

                AddHandler rb_radio_button.CheckedChanged, AddressOf rb_check_changed

                htc_table_cell2.Controls.Add(rb_radio_button)

                ' Omschrijving
                Dim htc_table_cell3 As New HtmlTableCell
                htc_table_cell3.InnerText = dr_data_row.item("description")

                ' Voeg de cellen toe
                htr_table_row.Controls.Add(htc_table_cell1)
                htr_table_row.Controls.Add(htc_table_cell2)
                htr_table_row.Controls.Add(htc_table_cell3)

                ht_html_table.Controls.Add(htr_table_row)
            Next

            ' Voeg de tabel toe
            Me.pnl_payment_types.Controls.Add(ht_html_table)

            ' Zet hier de standaard
            Dim rb_radio_active As RadioButton = ht_html_table.FindControl(dt_payment_methods.Rows(0).Item("code"))
            rb_radio_active.Checked = True
        End If
    End Sub

    Private Function get_payment_code() As String
        For Each dr_data_row As System.Data.DataRow In dt_payment_methods.Rows
            Dim rb_radio_button As RadioButton = Me.FindControl(dr_data_row.Item("code"))

            If rb_radio_button.Checked Then
                Return rb_radio_button.ID
            End If
        Next

        Return ""
    End Function

    Private Sub rb_check_changed(ByVal sender As Object, ByVal e As Object)
        For Each dr_data_row As System.Data.DataRow In dt_payment_methods.Rows
            Dim rb_radio_button As RadioButton = Me.FindControl(dr_data_row.Item("code"))
            rb_radio_button.Checked = False
        Next

        CType(sender, RadioButton).Checked = True
    End Sub

    Protected Sub button_finish_order_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_finish_order.Click
        If IsPostBack Then
            Me.pnl_process_order.Visible = True
            Me.button_finish_order.Visible = False
            Me.pnl_error.Visible = False

            Dim lc_brand As String = Me.get_payment_code()
            Dim lc_payment_type As String = Utilize.Data.DataProcedures.GetValue("uws_pay_types", "pt_type", lc_brand)

            Dim lc_order_id As String = Me.get_page_parameter("orderid")
            Response.Redirect("~/" + Me.global_ws.Language + "/webshop/paymentprocess/services/ogone_redirect.aspx?payment_type=" + lc_payment_type + "&payment_brand=" + lc_brand + "&order_id=" + lc_order_id)
        End If
    End Sub
End Class
