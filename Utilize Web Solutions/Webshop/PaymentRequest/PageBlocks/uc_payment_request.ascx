﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_payment_request.ascx.vb" Inherits="uc_payment_request" %>

<utilize:placeholder runat="server" ID="ph_ogone">
    <div class="uc_payment_request">
        <h1>
            <utilize:translatetitle runat="server" ID="title_pay_order" Text="Bestelling afrekenen"></utilize:translatetitle></h1>
        <div class="row">
            <utilize:placeholder runat="server" ID="ph_payment_methods">
                <div class="col-md-8">
                    <h3>
                        <utilize:translatetitle runat="server" ID="title_payment_services" Text="Betaalmethode"></utilize:translatetitle></h3>
                    <utilize:translatetext runat="server" ID="text_payment_services" Text="Geef rechts aan op welke manier u de bestelling wilt afrekenen."></utilize:translatetext>
                </div>
                <div class="col-md-4">
                    <utilize:panel runat="server" ID="pnl_payment_types" CssClass="paymenttypes" />
                </div>
            </utilize:placeholder>
        </div>

        <div class="row">
            <div class="col-md-12">
                <utilize:translatetext runat="server" ID="text_payment_conditions" Text="Wanneer u kiest voor Betalen, dan gaat u akkoord met onze Algemene Verkoopvoorwaarden."></utilize:translatetext>
            </div>
        </div>

        <div class="buttons">
            <utilize:translatebutton runat='server' ID="button_finish_order" Text="Afrekenen" CssClass="right btn-u btn-u-sea-shop btn-u-lg"/>
        </div>

        <utilize:panel runat="server" ID="pnl_error" CssClass="uc_ogone_error" Visible="false"></utilize:panel>
        <utilize:modalpanel runat="server" ID="pnl_process_order" Visible="false">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
            <utilize:translatetext runat="server" ID="text_payment_processing_order" Text="Order wordt verwerkt."></utilize:translatetext>
        </utilize:modalpanel>
    </div>
</utilize:placeholder>
