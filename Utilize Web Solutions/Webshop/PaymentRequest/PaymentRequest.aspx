﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CMS/MasterPages/MasterPage_1.master" AutoEventWireup="false" CodeFile="PaymentRequest.aspx.vb" Inherits="Webshop_PaymentRequest_PaymentRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="styles" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="column_1" ContentPlaceHolderID="column_1" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_1"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_2" ContentPlaceHolderID="column_2" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_2"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_3" ContentPlaceHolderID="column_3" Runat="Server">
    <utilize:placeholder runat="server" ID="ph_blocks_3"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_4" ContentPlaceHolderID="column_4" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_4"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_5" ContentPlaceHolderID="column_5" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_5"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_6" ContentPlaceHolderID="column_6" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_6"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_7" ContentPlaceHolderID="column_7" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_7"></utilize:placeholder>
</asp:Content>

<asp:Content ID="column_8" ContentPlaceHolderID="column_8" Runat="Server">
        <utilize:placeholder runat="server" ID="ph_blocks_8"></utilize:placeholder>
</asp:Content>
