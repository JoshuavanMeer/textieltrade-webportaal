﻿
Partial Class Webshop_PaymentRequest_PaymentRequest
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("pay_proc_template")
        
        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_payment_request", "Betalingsverzoek", Me.global_ws.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_zone As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        Dim uc_account_block As Webshop.ws_user_control = LoadUserControl("PageBlocks/uc_payment_request.ascx")
        ph_zone.Controls.Add(uc_account_block)
    End Sub
End Class