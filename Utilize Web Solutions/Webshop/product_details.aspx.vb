﻿

<Serializable()>
Public Class product_details
    Inherits CMSPageTemplate

    Public WriteOnly Property product_code As String
        Set(ByVal value As String)
            uc_block.set_property("product_code", value)
        End Set
    End Property

    Public WriteOnly Property size As String
        Set(ByVal value As String)
            uc_block.set_property("size", value)
        End Set
    End Property

    Private uc_block As Base.base_usercontrol_base = Nothing

    Private Function get_page_template(ByVal current_cat As String) As String
        Dim lc_page_template As String = ""

        If Not current_cat = "" Then
            Dim qc_select As New Utilize.Data.QuerySelectClass
            qc_select.select_fields = "cat_code, cat_parent, page_template"
            qc_select.select_from = "uws_categories"
            qc_select.select_where = "cat_code = @cat_code"
            qc_select.add_parameter("cat_code", current_cat)

            Dim dt_data_table As System.Data.DataTable = qc_select.execute_query()

            If dt_data_table.Rows.Count > 0 Then
                Dim lc_cat_code As String = dt_data_table.Rows(0).Item("cat_code")
                Dim lc_parent_cat As String = dt_data_table.Rows(0).Item("cat_parent")

                If dt_data_table.Rows(0).Item("page_template").ToString.Trim() = "" Then
                    lc_page_template = Me.get_page_template(lc_parent_cat)
                Else
                    lc_page_template = dt_data_table.Rows(0).Item("page_template").ToString.Trim()
                End If
            End If
        Else
            lc_page_template = Me.global_ws.get_webshop_setting("product_det_template")
        End If

        Return lc_page_template
    End Function

    Private Function get_template_code() As String
        Dim lc_template_code As String = Me.global_ws.get_webshop_setting("product_det_template")
        Dim lc_prod_code As String = Me.get_page_parameter("prod_code")

        Dim lc_cat_code As String = Utilize.Data.DataProcedures.GetValue("uws_categories_prod", "cat_code", lc_prod_code, "prod_code")

        If Not lc_cat_code = "" Then
            lc_template_code = Me.get_page_template(lc_cat_code)
        End If

        Return lc_template_code
    End Function

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Vul de pagina template uit de categorieën
        Dim lc_template_code As String = Me.get_template_code()

        ' Vul de pagina template
        Me.cms_template_code = lc_template_code
        Me.meta_description = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", Me.get_page_parameter("prod_code").Trim() + Me.global_ws.Language, "prod_code + lng_code")
        Me.meta_keywords = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_keywords", Me.get_page_parameter("prod_code").Trim() + Me.global_ws.Language, "prod_code + lng_code")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", Me.get_page_parameter("prod_code").Trim() + Me.global_ws.Language, "prod_code + lng_code")
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("product_details.css")

        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        If global_ws.get_webshop_setting("use_prod_detail_v2") Then
            uc_block = LoadUserControl("~/Webshop/Modules/ProductDetails/uc_product_details_v2.ascx")
        Else
            uc_block = LoadUserControl("~/Webshop/Modules/ProductDetails/uc_product_details.ascx")
        End If

        uc_block.ID = "uc_product_details"

        ph_target_block.Controls.AddAt(ph_target_block.Controls.Count, uc_block)
    End Sub
End Class
