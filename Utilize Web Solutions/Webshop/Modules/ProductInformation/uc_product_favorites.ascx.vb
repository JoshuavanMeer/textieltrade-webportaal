﻿
Partial Class uc_product_favorites
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private prod_code As String = ""

    Public Property product_code As String
        Get
            Return prod_code
        End Get
        Set(ByVal value As String)
            prod_code = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' kijken of de module aan staat en de gebruiker ingelogd is
        Me.Visible = Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_favorites")

        If Me.Visible Then
            Me.set_style_sheet("uc_product_favorites.css", Me)
        End If
    End Sub

    Protected Sub button_add_to_favorites_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_to_favorites.Click
        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_favorites")

        ' Test flag
        Dim ll_testing As Boolean = True

        ' Insert a new row in the business object for table uws_product_favorite
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_insert_row("uws_prod_favorites", Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.field_set("prod_code", Me.button_add_to_favorites.CommandArgument)
            ubo_bus_obj.field_set("user_id", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            ' change buttons
            Me.button_add_to_favorites.CommandArgument = ""
            Me.button_add_to_favorites.Visible = False

            Me.button_remove_from_favorites.CommandArgument = ubo_bus_obj.field_get("rec_id")
            Me.button_remove_from_favorites.Visible = True

        End If
    End Sub

    Protected Sub button_remove_from_favorites_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_remove_from_favorites.Click
        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_favorites")

        ' Test flag
        Dim ll_testing As Boolean = True

        ' obtain the row
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_seek("uws_prod_favorites", Me.button_remove_from_favorites.CommandArgument)
        End If
        '        

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.table_delete_row("uws_prod_favorites", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            ' change buttons
            Me.button_add_to_favorites.CommandArgument = prod_code
            Me.button_add_to_favorites.Visible = True

            Me.button_remove_from_favorites.CommandArgument = ""
            Me.button_remove_from_favorites.Visible = False
        End If
    End Sub

    Private Sub uc_product_favorites_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim dt_product_favorites As System.Data.DataTable = Nothing
        Dim qc_product_favorites As New Utilize.Data.QueryCustomSelectClass

        qc_product_favorites.QueryString = "SELECT rec_id FROM uws_prod_favorites WHERE user_id = @user_id AND prod_code = @prod_code"
        qc_product_favorites.add_parameter("user_id", Me.global_ws.user_information.user_id)
        qc_product_favorites.add_parameter("prod_code", prod_code)

        dt_product_favorites = qc_product_favorites.execute_query(Me.global_ws.user_information.user_id)

        If dt_product_favorites.Rows.Count > 0 Then
            Me.button_add_to_favorites.CommandArgument = ""
            Me.button_add_to_favorites.Visible = False

            Me.button_remove_from_favorites.CommandArgument = dt_product_favorites.Rows(0).Item("rec_id")
            Me.button_remove_from_favorites.Visible = True
        Else
            Me.button_add_to_favorites.CommandArgument = prod_code
            Me.button_add_to_favorites.Visible = True

            Me.button_remove_from_favorites.CommandArgument = ""
            Me.button_remove_from_favorites.Visible = False
        End If
    End Sub
End Class
