﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_price.ascx.vb" Inherits="Webshop_Modules_ProductInformation_uc_product_price" %>

<div class="uc_product_price">
    <div class="prod_price">
        <utilize:placeholder runat="server" ID="ph_prod_price1">
            <ul runat="server" ID="ph_prices_wrapper1" class="price list-inline shop-product-prices">
                <li class="excl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price1_excl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price1_original_excl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price1_excl_vat" Visible="false" Text="Excl. BTW" CssClass="vat-label"></utilize:label>
                </li>

                <li class="incl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price1_incl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price1_original_incl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price1_incl_vat" Visible="false" Text="Incl. BTW" CssClass="vat-label"></utilize:label>
                </li>
            </ul>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prod_price2">
            <ul runat="server" ID="ph_prices_wrapper2" class="price1 list-inline shop-product-prices">
                <li class="excl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price2_excl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price2_original_excl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price2_excl_vat" Visible="false" Text="Excl. BTW" CssClass="vat-label"></utilize:label>
                </li>

                <li class="incl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price2_incl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price2_original_incl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price2_incl_vat" Visible="false" Text="Incl. BTW" CssClass="vat-label"></utilize:label>
                </li>
            </ul>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prod_price3">
            <ul runat="server" ID="ph_prices_wrapper3" class="price2 list-inline shop-product-prices">
                <li class="excl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price3_excl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price3_original_excl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price3_excl_vat" Visible="false" Text="Excl. BTW" CssClass="vat-label"></utilize:label>
                </li>

                <li class="incl-vat">
                    <utilize:label runat="server" ID="lbl_prod_price3_incl" CssClass="value actual"></utilize:label>
                    <utilize:label runat="server" ID="lbl_prod_price3_original_incl" CssClass="value original"></utilize:label>
                    <utilize:label runat="server" ID="label_price3_incl_vat" Visible="false" Text="Incl. BTW" CssClass="vat-label"></utilize:label>
                </li>
            </ul>
        </utilize:placeholder>
    </div>
</div>