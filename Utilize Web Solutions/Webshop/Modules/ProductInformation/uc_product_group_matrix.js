﻿function update_quantity(unitQuantityCtrl, unitCtrl, quantityCtrl) {
    var unit_quantity = 0;
    var unit = 0;
    var quantity = 0;

    unit_quantity = document.getElementById(unitQuantityCtrl).value;
    unit = document.getElementById(unitCtrl).innerHTML;

    quantity = unit_quantity * unit;
    document.getElementById(quantityCtrl).innerHTML = quantity;
}


function increment(fldId) {
    fld = document.getElementById(fldId);

    if (fld.value == '-') {
        fld.value = 1;
    }
    else {
        fld.value = parseFloat(fld.value) + 1;
    }
}

function decrement(fldId) {

    fld = document.getElementById(fldId);

    if (fld.value == '-' || fld.value == '1') {
        fld.value = 1;
    }
    else {
        fld.value = parseFloat(fld.value) - 1;
    }
}
