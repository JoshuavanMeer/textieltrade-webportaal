﻿
Partial Class uc_product_order_list
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private prod_code As String = ""

    Public Property product_code As String
        Get
            Return prod_code
        End Get
        Set(ByVal value As String)
            prod_code = value

            ' Declareer een nieuwe datatable
            Try
                Dim dt_products As System.Data.DataTable = Me.get_id_product_in_order_list(prod_code)

                Me.button_add_to_order_list.Visible = dt_products.Rows.Count = 0
                Me.button_remove_from_order_list.Visible = dt_products.Rows.Count > 0

                If dt_products.Rows.Count > 0 Then
                    Me.button_add_to_order_list.CommandArgument = ""
                    Me.button_remove_from_order_list.CommandArgument = dt_products.Rows(0).Item("rec_id")
                Else
                    Me.button_add_to_order_list.CommandArgument = prod_code
                    Me.button_remove_from_order_list.CommandArgument = ""
                End If
            Catch ex As Exception
                'Me.button_add_to_order_list.Enabled = False
                'Me.button_remove_from_order_list.Enabled = False
                Me.button_add_to_order_list.Visible = False
                Me.button_remove_from_order_list.Visible = False
            End Try
        End Set
    End Property

    ''' <summary>
    ''' Deze functie haalt de rec_id uit de bestellijst op van het huidige product
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_id_product_in_order_list(ByVal prod_code As String) As System.Data.DataTable
        ' Haal de producten in de bestellijst op
        Dim qc_products As New Utilize.Data.QuerySelectClass
        qc_products.select_fields = "rec_id"
        qc_products.select_from = "uws_prod_order_list"

        ' Filter op categorieen die getoond mogen worden en de huidige taal
        qc_products.select_where = "user_id = @user_id and prod_code = @prod_code"
        qc_products.add_parameter("user_id", Me.global_ws.user_information.user_id)
        qc_products.add_parameter("prod_code", prod_code)

        ' Sorteer op regelvolgorde
        qc_products.select_order = "row_ord"

        ' Voer de query uit
        Return qc_products.execute_query()
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Kijk of de gebruiker ingelogd is en of de module aan staat
        Me.Visible = Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_lists")

        If Me.Visible Then
            Me.set_style_sheet("uc_product_order_list.css", Me)
        End If
    End Sub

    Protected Sub button_add_to_order_list_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_add_to_order_list.Click
        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_order_list")

        ' Test flag
        Dim ll_testing As Boolean = True

        ' Insert a new row in the business object for table uws_prod_order_list
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_insert_row("uws_prod_order_list", Me.global_ws.user_information.user_id)
        End If

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.field_set("prod_code", Me.button_add_to_order_list.CommandArgument)
            ubo_bus_obj.field_set("user_id", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            ' change buttons
            Me.button_add_to_order_list.CommandArgument = ""
            Me.button_add_to_order_list.Visible = False

            Me.button_remove_from_order_list.CommandArgument = ubo_bus_obj.field_get("rec_id")
            Me.button_remove_from_order_list.Visible = True
        End If
    End Sub

    Protected Sub button_remove_from_order_list_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_remove_from_order_list.Click
        ' Create a new business object
        Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_prod_order_list")

        ' Test flag
        Dim ll_testing As Boolean = True

        ' obtain the row
        If ll_testing Then
            ll_testing = ubo_bus_obj.table_seek("uws_prod_order_list", Me.button_remove_from_order_list.CommandArgument)
        End If

        If ll_testing Then
            ' Set the different field values
            ubo_bus_obj.table_delete_row("uws_prod_order_list", Me.global_ws.user_information.user_id)

            ' Save the changes
            ll_testing = ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            ' change buttons
            Me.button_add_to_order_list.CommandArgument = prod_code
            Me.button_add_to_order_list.Visible = True

            Me.button_remove_from_order_list.CommandArgument = ""
            Me.button_remove_from_order_list.Visible = False
        End If
    End Sub
End Class
