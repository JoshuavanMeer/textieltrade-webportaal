﻿function update_quantity(unitQuantityCtrl, unitCtrl, quantityCtrl, decimals) {
    //console.log("update_quantity");
    //console.log("- decimals: " + decimals);
    var unit_quantity = 0;
    var unit = 0;
    var quantity = 0;

    unit_quantity = document.getElementById(unitQuantityCtrl).value;
    var changeToComma = unit_quantity.indexOf(".") == -1;
    unit_quantity = unit_quantity.replace(",", ".");
    
    //console.log("unit_quantity: " + unit_quantity);
    unit = document.getElementById(unitCtrl).innerHTML;
    changeToComma = unit.indexOf(".") == -1;
    unit = unit.replace(",", ".");

    //console.log("unit: " + unit);

    quantity = unit_quantity * unit;
    quantity = quantity.toFixed(decimals);
    if (changeToComma) {
        quantity = quantity.toString().replace(".", ",");
    }
    //console.log("quantity: " + quantity);
    document.getElementById(quantityCtrl).innerHTML = quantity;
}


function increment(fldId, decimals) {
    fld = document.getElementById(fldId);

    var changeToComma = fld.value.indexOf(".") == -1;

    if (fld.value == '-' || fld.value == '')
    {
        var newValue = 1;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace(".", ",");
        }
        fld.value = newValue;
    }
    else
    {
        var newValue = fld.value.replace(",", ".");
        newValue = parseFloat(newValue) + 1;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace('.', ',')
        }
        fld.value = newValue;
    }
}

function decrement(fldId, decimals) {
    fld = document.getElementById(fldId);

    var changeToComma = fld.value.indexOf(".") == -1;

    if (fld.value == '-' || fld.value == '1' || fld.value == '' || fld.value == '0') {
        var newValue = 0;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace(".", ",");
        }
        fld.value = newValue;
    }
    else {
        var newValue = fld.value.replace(",", ".");
        newValue = parseFloat(newValue) - 1;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace('.', ',')
        }
        fld.value = newValue;
    }
}


function incrementzero(fldId, decimals) {
    fld = document.getElementById(fldId);

    var changeToComma = fld.value.indexOf(".") == -1;

    if (fld.value == '-' || fld.value == '') {
        var newValue = 0;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace(".", ",");
        }
        fld.value = newValue;
    }
    else {
        var newValue = fld.value.replace(",", ".");
        newValue = parseFloat(newValue) + 1;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace('.', ',')
        }
        fld.value = newValue;
    }
}

function decrementzero(fldId, decimals) {
    fld = document.getElementById(fldId);

    var changeToComma = fld.value.indexOf(".") == -1;

    if (fld.value == '-' || fld.value == '0' || fld.value == '') {
        var newValue = 0;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace(".", ",");
        }
        fld.value = newValue;
    }
    else {
        var newValue = fld.value.replace(",", ".");
        newValue = parseFloat(newValue) - 1;
        newValue = newValue.toFixed(decimals);
        if (changeToComma) {
            newValue = newValue.replace('.', ',')
        }
        fld.value = newValue;
    }
}
