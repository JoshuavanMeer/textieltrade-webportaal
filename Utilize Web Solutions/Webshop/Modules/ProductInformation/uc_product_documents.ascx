﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_documents.ascx.vb" Inherits="uc_product_documents" %>
<div class="uc_product_documents">
    <h2><utilize:translatetitle runat="server" ID="title_product_documents" Text="Product documenten"></utilize:translatetitle></h2>
    
    <utilize:repeater runat="server" ID="rpt_product_documents">
        <HeaderTemplate>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><a target="_blank" href='<%# Me.get_product_document_link(DataBinder.Eval(Container.DataItem, "crec_id"))%>'><%# DataBinder.Eval(Container.DataItem, "doc_desc")%> </a></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </utilize:repeater>
    
    <utilize:repeater runat="server" ID="rpt_product_group_documents">
        <HeaderTemplate>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><a target="_blank" href='<%# Me.get_product_group_document_link(DataBinder.Eval(Container.DataItem, "crec_id"))%>'><%# DataBinder.Eval(Container.DataItem, "doc_desc")%> </a></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </utilize:repeater>
</div>
