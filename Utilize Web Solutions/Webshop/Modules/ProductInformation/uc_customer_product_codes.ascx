﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_customer_product_codes.ascx.vb" Inherits="uc_customer_product_codes" %>

<div class="uc_customer_product_codes">
    <utilize:placeholder runat="server" ID="ph_customer_edit">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <utilize:textbox runat='server' ID="txt_product_code" CssClass="input form-control"></utilize:textbox>
                    <span class="input-group-btn">
                        <utilize:translatebutton runat='server' ID="button_save" Text="Bewaren" CssClass="btn-u btn-u-sea-shop btn-u-lg" />
                        </span>
                </div>
            </div>
        </div>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_customer_text">
        <span><utilize:translateliteral runat="server" ID="label_customer_your_product_code" Text="Uw productcode"></utilize:translateliteral>:&nbsp;<utilize:literal runat="server" ID="lt_product_code"></utilize:literal></span>
    </utilize:placeholder>
</div>