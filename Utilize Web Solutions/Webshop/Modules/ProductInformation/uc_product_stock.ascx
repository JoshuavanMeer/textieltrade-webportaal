﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_stock.ascx.vb" Inherits="uc_product_stock" %>
<div class="uc_product_stock">
    <ul class="list-inline" runat="server" id="prod_stock_list">
            <li class="text">
                <utilize:translateliteral runat="server" ID="label_stock_indicator" Text="Voorraad"></utilize:translateliteral>:
            </li>
        <li class="value">
            <utilize:placeholder runat="server" ID="ph_stock_description" Visible="false">
                <utilize:label runat="server" ID="txt_stock_description"></utilize:label>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_stock_indicator" Visible="false">
                <utilize:image runat="server" ID="img_stock_value"></utilize:image>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_stock_value" Visible="false">
                <utilize:label runat="server" ID="lt_stock_value"></utilize:label>
            </utilize:placeholder>
        </li>
    </ul>

    <utilize:placeholder runat="server" ID="ph_signal_me_stock" Visible="false">
        <div class="margin-bottom-20">
            <utilize:translatelinkbutton runat="server" ID="link_signal_me_stock" Text="Stel mij op de hoogte wanneer het product weer leverbaar is."></utilize:translatelinkbutton>
        </div>
        <utilize:modalpanel runat="server" ID="modal_signal_stock" Visible="false">
            <div class="modal-content">
                <div class="modal-header center">
                    <h1><utilize:translatetitle runat="server" ID="title_email_signal_header" Text="Geef uw notificatie verzoek aan ons door"></utilize:translatetitle></h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:translatetext runat="server" ID="text_email_signal_stock" Text="Geef hieronder uw emailadres aan waarop u de notificatie wilt ontvangen."></utilize:translatetext>
                        </div>
                    </div>
                    <div class="row margin-bottom-20">
                        <div class="col-md-12">
                            <utilize:textbox runat="server" ID="txtbx_email_to_signal_to"></utilize:textbox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:translatetext runat="server" ID="text_signal_stock_removed_after" Text="Deze aanvraag zal worden verwerkt en bewaard voor xxx weken, daarna wordt deze automatisch verwijderd."></utilize:translatetext>
                        </div>
                    </div>
                    <utilize:placeholder runat="server" ID="ph_incorrect_email_at_signal_stock" Visible="false"  >
                        <utilize:alert runat ="server" ID="alert_incorrect_email" AlertType="danger"></utilize:alert>
                    </utilize:placeholder>
                </div>
                <div class="modal-footer">
                    <utilize:translatebutton runat="server" ID="button_signal_confirm" Text="Verzenden" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg" />
                    <utilize:translatebutton runat="server" ID="button_signal_cancel" Text="Annuleren" CssClass="inputbutton btn-u btn-u-sea-shop btn-u-lg" />
                </div>
            </div>

        </utilize:modalpanel>

    </utilize:placeholder>
</div>
