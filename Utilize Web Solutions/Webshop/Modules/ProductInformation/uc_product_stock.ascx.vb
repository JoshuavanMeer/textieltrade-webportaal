﻿
Imports System.Net.Mail
Imports System.Linq

Partial Class uc_product_stock
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    Private _size As String = ""
    Public Property size As String
        Get
            Return _size
        End Get
        Set(ByVal value As String)
            _size = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ll_testing = Not Me.product_code = ""
        End If

        If ll_testing Then
            Dim ln_stock_show As Integer = Me.global_ws.get_webshop_setting("show_stock")

            Select Case ln_stock_show
                Case 1
                    ll_testing = True
                Case 2
                    ll_testing = Me.global_ws.user_information.user_logged_on
                Case Else
                    ll_testing = False
            End Select
        End If

        If ll_testing Then
            Dim ln_stock As Decimal = Utilize.Web.Solutions.Webshop.ws_procedures.get_stock(Me.product_code, Me.size)
            Dim ln_stock_type = Me.global_ws.get_webshop_setting("stock_type")

            If Not Me.global_ws.user_information.ubo_customer.field_get("stock_type") = 0 Then
                ln_stock_type = Me.global_ws.user_information.ubo_customer.field_get("stock_type")
            End If

            Dim ln_select_value As Integer = 2
            Dim dt_indicator As New System.Data.DataTable
            Dim ln_stock_limit As Decimal = 0

            If Utilize.Data.DataProcedures.CheckModule("utlz_ws_stock_limit") Then
                ' Als de module stock limit aan staat en bij een product is een limiet vastgelegd (hoger dan 0)
                ' limiet is het ideale voorraad aantal van een product.
                ' De standaard indicatoren obv voorraad aantal worden overschreven.
                ' Als voorraad klein of gelijk aan 0 toon dan de laagste (stock_quantity asc)
                ' Als limiet kleiner of gelijk aan voorraad toon dan de hoogste (stock_quantity desc)
                ' Als limiet hoger dan voorraad toon de 1-na-hoogste indicator
                ' stock indicators: 0 = rood, 5 = oranje, 10 = groen
                ' stock limit: 100, stock: 20 > oranje
                ' stock limit: 20, stock: 21 > groen
                ' stock limit: x, stock: 0 > rood
                ln_stock_limit = Utilize.Data.DataProcedures.GetValue("uws_products", "stock_limit", Me.product_code)
                If Not ln_stock_limit = 0 Then
                    Dim select_id As String = ""
                    Dim qsc_indicator As New Utilize.Data.QuerySelectClass
                    qsc_indicator.select_fields = "*"
                    qsc_indicator.select_from = "uws_stock_indicators"
                    qsc_indicator.select_order = "stock_quantity desc"

                    Select Case True
                        Case ln_stock <= 0
                            qsc_indicator.select_fields = "*"
                            qsc_indicator.select_from = "uws_stock_indicators"
                            qsc_indicator.select_order = "stock_quantity asc"
                        Case ln_stock_limit <= ln_stock
                            ' Keep the default highest stock quantity indicator
                        Case ln_stock_limit > ln_stock
                            Dim dt_temp As System.Data.DataTable = qsc_indicator.execute_query()
                            Dim highest_id As String = dt_temp.Rows(0).Item("rec_id")

                            qsc_indicator.select_fields = "top 1 *"
                            qsc_indicator.select_from = "uws_stock_indicators"
                            qsc_indicator.select_where = "not rec_id = @highest_id"
                            qsc_indicator.add_parameter("highest_id", highest_id)
                            qsc_indicator.select_order = "stock_quantity desc"
                        Case Else
                            Exit Select
                    End Select

                    dt_indicator = qsc_indicator.execute_query()
                End If
            End If

            If ln_stock_type = 2 Or ln_stock_type = 3 Or ln_stock_type = 4 Then
                ' Hier de voorraad indicator of de omschrijving
                ' 2 is indicator
                ' 3 is omschrijving
                If Utilize.Data.DataProcedures.CheckModule("utlz_ws_stock_limit") And Not ln_stock_limit = 0 Then
                    ' Als er regels zijn zet dan alle beschikbare velden
                    If dt_indicator.Rows.Count > 0 Then
                        Me.img_stock_value.ImageUrl = Me.ResolveCustomUrl("~/" + dt_indicator.Rows(0).Item("stock_image"))
                        Me.img_stock_value.AlternateText = dt_indicator.Rows(0).Item("stock_desc_" + Me.global_ws.Language)

                        Me.txt_stock_description.Text = dt_indicator.Rows(0).Item("stock_desc_" + Me.global_ws.Language)
                    End If
                Else
                    Dim qsc_data_table As New Utilize.Data.QuerySelectClass
                    qsc_data_table.select_fields = "top 1 *"
                    qsc_data_table.select_from = "uws_stock_indicators"
                    qsc_data_table.select_where = "stock_quantity >= @stock_quantity"
                    qsc_data_table.add_parameter("stock_quantity", ln_stock)
                    qsc_data_table.select_order = "stock_quantity asc"

                    Dim dt_data_table As System.Data.DataTable = qsc_data_table.execute_query()

                    ' Als er regels zijn zet dan alle beschikbare velden
                    If dt_data_table.Rows.Count > 0 Then
                        Me.img_stock_value.ImageUrl = Me.ResolveCustomUrl("~/" + dt_data_table.Rows(0).Item("stock_image"))
                        Me.img_stock_value.AlternateText = dt_data_table.Rows(0).Item("stock_desc_" + Me.global_ws.Language)

                        Me.txt_stock_description.Text = dt_data_table.Rows(0).Item("stock_desc_" + Me.global_ws.Language)
                    Else
                        Dim qsc_highest As New Utilize.Data.QuerySelectClass
                        qsc_highest.select_fields = "top 1 *"
                        qsc_highest.select_from = "uws_stock_indicators"
                        qsc_highest.select_order = "stock_quantity desc"

                        Dim dt_highest As System.Data.DataTable = qsc_highest.execute_query()

                        Me.img_stock_value.ImageUrl = Me.ResolveCustomUrl("~/" + dt_highest.Rows(0).Item("stock_image"))
                        Me.img_stock_value.AlternateText = dt_highest.Rows(0).Item("stock_desc_" + Me.global_ws.Language)

                        Me.txt_stock_description.Text = dt_highest.Rows(0).Item("stock_desc_" + Me.global_ws.Language)

                    End If
                End If

                ' Hier het juiste onderdeel zichtbaar maken
                Me.ph_stock_indicator.Visible = ln_stock_type = 2 Or ln_stock_type = 4
                Me.ph_stock_description.Visible = ln_stock_type = 3 Or ln_stock_type = 4
            Else
                ' Zet hier de voorraad waarde.
                If ln_stock < 0 Then
                    ln_stock = 0
                End If

                Me.lt_stock_value.Text = Decimal.Round(ln_stock, 0).ToString()
                Me.ph_stock_value.Visible = True
                Me.prod_stock_list.Attributes("class") = Me.prod_stock_list.Attributes("class") + " stock-totals"
            End If

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_stocksignal") Then
                Me.alert_incorrect_email.Text = global_trans.translate_message("message_incorrect_email_address", "Geen of incorrect emailadres ingevuld", Me.global_ws.Language)

                If ln_stock < 1 Then
                    Me.ph_signal_me_stock.Visible = True
                End If
            End If
        End If

        If ll_testing Then
            Me.set_style_sheet("uc_product_stock.css", Me)
        End If

        If Not ll_testing Then
            Me.Visible = False
        End If

        ' Advanded product filters aka product configurator
        If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") = True Then
            ' Don't show this control if the product is part of a set
            Me.Visible = Not Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_part_of_set(Me.product_code)
        End If
    End Sub

    Public Sub link_signal_me_stock_Click(sender As Object, e As EventArgs) Handles link_signal_me_stock.Click
        Me.modal_signal_stock.Visible = True

        ' Extra service to logged in user, to pre-fill known email_address
        If Me.global_ws.user_information.user_logged_on Then
            Me.txtbx_email_to_signal_to.Text = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "email_address", Me.global_ws.user_information.user_id, "rec_id")
        End If

        Me.text_signal_stock_removed_after.Text = Me.text_signal_stock_removed_after.Text.Replace("xxx", Me.global_ws.get_webshop_setting("signal_stock_remove"))
    End Sub

    Public Sub button_signal_confirm_Click(sender As Object, e As EventArgs) Handles button_signal_confirm.Click
        If Not String.IsNullOrEmpty(Me.txtbx_email_to_signal_to.Text.Trim()) Then
            ' Try to save request and send email when successfull
            Try
                Dim email_address_check As MailAddress = New MailAddress(Me.txtbx_email_to_signal_to.Text)

                Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("UWS_SIGNAL_STOCK", "", "rec_id")
                If ubo_bus_obj.table_insert_row("UWS_SIGNAL_STOCK", Me.global_ws.user_information.user_id) Then

                    If Me.global_ws.user_information.user_logged_on Then
                        ubo_bus_obj.field_set("CUSTOMER_USERS_ID", Me.global_ws.user_information.user_id)
                    End If

                    ubo_bus_obj.field_set("EMAIL_ADDRESS", Me.txtbx_email_to_signal_to.Text.Trim())
                    ubo_bus_obj.field_set("REQUEST_DATE", Date.Now)
                    ubo_bus_obj.field_set("PROD_CODE", _product_code)
                    ubo_bus_obj.field_set("lng_code", Me.global_ws.Language)

                    If ubo_bus_obj.table_update(Me.global_ws.user_information.user_id) Then

                        Dim ws_email_signal_confirmation As New Utilize.Web.Solutions.Webshop.Email.ws_email_signal_confirmation
                        ws_email_signal_confirmation.prod_code = _product_code
                        ws_email_signal_confirmation.email_address = Me.txtbx_email_to_signal_to.Text.Trim()
                        ws_email_signal_confirmation.language = Me.global_ws.Language

                        If Not ws_email_signal_confirmation.send_email() Then
                            Me.writelog("Fout bij verzenden 'confirmation' voorraadsignalering", ws_email_signal_confirmation.error_message)
                        End If

                        Me.modal_signal_stock.Visible = False
                        Me.ph_incorrect_email_at_signal_stock.Visible = False
                    Else
                        Me.ph_incorrect_email_at_signal_stock.Visible = True
                    End If
                Else
                    Me.ph_incorrect_email_at_signal_stock.Visible = True
                End If
            Catch ex As Exception
                Me.ph_incorrect_email_at_signal_stock.Visible = True
                Me.writelog("Exceptie bij verzenden 'confirmation' voorraadsignalering", ex.Message)
            End Try
        Else
            Me.ph_incorrect_email_at_signal_stock.Visible = True
        End If
    End Sub

    Public Sub button_signal_cancel_Click(sender As Object, e As EventArgs) Handles button_signal_cancel.Click
        Me.modal_signal_stock.Visible = False
        Me.ph_incorrect_email_at_signal_stock.Visible = False
    End Sub

    Private Sub writelog(ByVal Message As String, ByVal ErrorDetails As String)
        ' Create the business object
        Dim ubo_err_log As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("utlz_err_log") ', "TEST", "rec_id")

        ' Insert a new row
        ubo_err_log.table_insert_row("utlz_error_log", Me.global_ws.user_information.user_id)

        ' Set the details
        ubo_err_log.field_set("err_date", System.DateTime.Now())
        ubo_err_log.field_set("err_msg", Message)
        ubo_err_log.field_set("err_details", ErrorDetails)

        ' Save the information
        ubo_err_log.table_update(Me.global_ws.user_information.user_id)

        ' Release the error object
        ubo_err_log = Nothing
    End Sub

    Private Sub uc_product_stock_Init(sender As Object, e As EventArgs) Handles Me.Load
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_boc") And Utilize.Data.API.Webshop.IntermediateServiceProcedures.get_setting("on_demand_stock_sync") And Not String.IsNullOrEmpty(product_code) Then
            If Not Me.IsPostBack And Me.global_ws.user_information.user_logged_on Then
                Dim intermediateService = Utilize.Data.API.Webshop.IntermediateServiceFactory.GetIntermediateConnector

                Dim lc_external_id As String = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.av_nr")
                If Not String.IsNullOrEmpty(lc_external_id) Then
                    Dim dict_product As New List(Of KeyValuePair(Of String, String))
                    dict_product.Add(New KeyValuePair(Of String, String)(product_code, Utilize.Data.DataProcedures.GetValue("uws_products", "prod_unit", product_code)))

                    Dim stockList = intermediateService.GetStock(lc_external_id, dict_product)

                    If Not stockList Is Nothing Then
                        Dim stock As Decimal = stockList.FirstOrDefault.ProductStock

                        Dim ubo_product As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_products", product_code, "prod_code")

                        ubo_product.field_set("uws_products.prod_stock", stock)

                        ubo_product.table_update(lc_external_id)
                    End If
                End If
            End If
        End If
    End Sub
End Class
