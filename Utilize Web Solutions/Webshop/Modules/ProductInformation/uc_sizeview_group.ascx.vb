﻿Imports System.Data
Imports System.Web.UI.WebControls

Partial Class uc_sizeview_group
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _sizebar_code As String = ""
    Private _color_code As String = ""
    Private _group_code As String = ""
    Private _product_code As String = ""

    Private dt_products As System.Data.DataTable = Nothing
    Private dt_sizes As System.Data.DataTable = Nothing
    Private dt_colors As System.Data.DataTable = Nothing

    ''' <summary>
    ''' Property voor het product wat geselecteerd is.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            If Not _product_code = "" Then
                ' Haal de groepcode en maatbalkcode op
                _group_code = Utilize.Data.DataProcedures.GetValue("uws_products", "shp_grp", _product_code, "prod_code")
                _sizebar_code = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_code", _product_code, "prod_code")
            End If
        End Set
    End Property
    Protected Sub Page_Init1() Handles Me.InitComplete
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Doe hier de licentie controle, dit houdt in dat we checken of de module actief is
            ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")

            If Not ll_testing Then
                Me.Visible = False
            End If
        End If

        If ll_testing Then
            Me.product_code = Me.get_page_parameter("prod_code")
        End If

        If ll_testing Then
            ' Zet de stylesheet
            Me.set_style_sheet("uc_sizeview_group.css", Me)
        End If

        If ll_testing Then
            'Haal de producten op
            ' Maak een class met producten aan
            Dim qsc_products As New Utilize.Data.QuerySelectClass
            qsc_products.select_fields = "uws_sizeview_colors.color_desc_" + Me.global_ws.Language + ", uws_products.prod_code"
            qsc_products.select_from = "uws_products"
            qsc_products.add_join("inner join", "uws_sizeview_colors", "uws_products.color_code = uws_sizeview_colors.color_code")

            ' Zet een filter
            qsc_products.select_where = ""
            qsc_products.select_where += "shp_grp = @shp_grp and sizebar_code = @sizebar_code and prod_show = 1 "
            qsc_products.select_where += "and prod_code in (select prod_code from uws_categories_prod)"

            qsc_products.add_parameter("shp_grp", _group_code)
            qsc_products.add_parameter("sizebar_code", _sizebar_code)

            qsc_products.select_order = "uws_sizeview_colors.color_desc_" + Me.global_ws.Language

            ' Voer de query uit
            dt_products = qsc_products.execute_query()


            ' Bind datatable to dropdownlist kleur
            dd_color.DataSource = dt_products
            dd_color.DataTextField = "color_desc_" + Me.global_ws.Language
            dd_color.DataValueField = "prod_code"
            dd_color.DataBind()

            Me.dd_color.SelectedValue = Me.get_page_parameter("prod_code")
            Me.ph_colors.Visible = Me.dd_color.Items.Count > 0
        End If

        If ll_testing Then
            'Als er geen kleuren zijn ingevuld, dan kunnen er als nog maatartikelen 
            If Me.dd_color.Items.Count > 0 Then
                dd_size.DataSource = Me.get_sizes(dd_color.SelectedValue)
            Else
                dd_size.DataSource = Me.get_sizes(Me.get_page_parameter("prod_code"))
            End If

            dd_size.DataTextField = "size_desc_" + Me.global_ws.Language
            dd_size.DataValueField = "size_code"
            dd_size.DataBind()
        End If

        If ll_testing Then
            Me.ph_sizes.Visible = dd_size.Items.Count > 0
        End If

        If ll_testing Then
            Me.Visible = Me.ph_colors.Visible Or Me.ph_sizes.Visible
        End If

        If ll_testing And Me.IsPostBack() Then
            If Me.postback_control = Me.dd_color.UniqueID Then
                SetProperty(Me.Page, "product_code", Request.Form(Me.dd_color.UniqueID))
            End If

            If Not Request.Form(Me.dd_size.UniqueID) Is Nothing Then
                SetProperty(Me.Page, "size", Request.Form(Me.dd_size.UniqueID))
            End If
        Else
            SetProperty(Me.Page, "size", Me.dd_size.SelectedValue)
        End If
    End Sub

    Protected Sub dd_color_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_color.SelectedIndexChanged
        dt_sizes = Me.get_sizes(dd_color.SelectedValue)
        dd_size.DataSource = dt_sizes
        dd_size.DataTextField = "size_desc_" + Me.global_ws.Language
        dd_size.DataValueField = "size_code"
        dd_size.DataBind()

        If dt_sizes.Rows.Count > 0 Then
            SetProperty(Me.Page, "size", dt_sizes.Rows(0).Item("size_code"))
        Else
            SetProperty(Me.Page, "size", "")
        End If
    End Sub
    Private Function get_sizes(ByVal product_code As String) As DataTable
        Dim lc_sizebar_code As String = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_code", product_code, "prod_code")

        ' Maak een class met maten aan
        Dim qsc_sizes As New Utilize.Data.QuerySelectClass
        qsc_sizes.select_fields = "size_code, size_desc_" + Me.global_ws.Language + ", row_ord"
        qsc_sizes.select_from = "uws_sizeview_sizes"

        ' Zet een filter
        qsc_sizes.select_where = "sizebar_code = @sizebar_code"
        qsc_sizes.select_where += " and not size_code in (select size_code from uws_sizeview_sizepro where prod_code = @prod_code and blocked = 1) "

        qsc_sizes.add_parameter("sizebar_code", lc_sizebar_code)
        qsc_sizes.add_parameter("prod_code", product_code)

        qsc_sizes.select_order = "row_ord"

        ' Voer de query uit
        Dim dt_sizes As DataTable = qsc_sizes.execute_query()

        Return dt_sizes
    End Function

    Function SetProperty(ByVal obj As Object, ByVal propertyName As String, ByVal val As Object) As Boolean
        Try
            ' get a reference to the PropertyInfo, exit if no property with that 
            ' name
            Dim pi As System.Reflection.PropertyInfo = obj.GetType().GetProperty(propertyName)

            If pi Is Nothing Then Return False
            ' convert the value to the expected type
            val = Convert.ChangeType(val, pi.PropertyType)
            ' attempt the assignment
            pi.SetValue(obj, val, Nothing)

            Return True
        Catch
            Return False
        End Try
    End Function
End Class
