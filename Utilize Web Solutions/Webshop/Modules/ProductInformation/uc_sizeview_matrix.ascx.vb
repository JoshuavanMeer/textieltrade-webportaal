﻿Imports System.Data
Imports System.Web.UI.WebControls

Partial Class uc_sizeview_matrix
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _sizebar_code As String = ""
    Private _group_code As String = ""
    Private _product_code As String = ""
    Private _sizeview_product As Boolean = False

    Friend Class SizeColumn
        Inherits System.Data.DataColumn

        Private _size_code As String = ""
        Public Property size_code As String
            Get
                Return _size_code
            End Get
            Set(ByVal value As String)
                _size_code = value
            End Set
        End Property

        Private _size_desc As String = ""
        Public Property size_desc As String
            Get
                Return _size_desc
            End Get
            Set(ByVal value As String)
                _size_desc = value
            End Set
        End Property
    End Class
    ''' <summary>
    ''' Property voor het product wat geselecteerd is.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            If Not _product_code = "" Then
                ' Haal de groepcode en maatbalkcode op
                _group_code = Utilize.Data.DataProcedures.GetValue("uws_products", "shp_grp", _product_code, "prod_code")
                _sizebar_code = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_code", _product_code, "prod_code")
                _sizeview_product = Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_article", Me.product_code)
            End If
        End Set
    End Property

    Private dt_products As System.Data.DataTable = Nothing
    Private dt_sizes As System.Data.DataTable = Nothing
    Private dt_size_colors As System.Data.DataTable = Nothing

    Protected Sub Page_Init1() Handles Me.Init
        Dim ll_testing As Boolean = True

        If ll_testing Then
            ' Doe hier de licentie controle, dit houdt in dat we checken of de module actief is
            ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview")

            If Not ll_testing Then
                Me.Visible = False
            End If
        End If

        If ll_testing Then
            If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_row_ref") And Request.Url.ToString.ToLower.Contains("product_details.aspx") Then
                Me.ph_row_reference.Visible = True
                Me.txt_product_comment.watermark = global_trans.translate_label("label_product_comment", "Opmerkingen", Me.global_ws.Language)
            End If
        End If

        If ll_testing Then
            If Not Me.global_ws.user_information.user_logged_on And Me.Visible Then
                ' De bestelknop is zichtbaar wanneer je niet bent ingelogd en de instelling bestellen wanneer niet ingelogd aan staat.
                Me.Visible = Me.global_ws.get_webshop_setting("order_not_logged_on")
            End If
        End If

        If ll_testing Then
            ' Zet de stylesheet
            Me.set_style_sheet("uc_sizeview_matrix.css", Me)
            Me.set_javascript("uc_sizeview_matrix.js", Me)
        End If

        If ll_testing Then
            Trace.Write("Productdetails ophalen - Begin")

            ' Maak een class met producten aan
            Dim qsc_products As New Utilize.Data.QuerySelectClass
            qsc_products.select_fields = "prod_code, color_code"
            qsc_products.select_from = "uws_products"

            ' Zet een filter
            qsc_products.select_where = ""
            qsc_products.select_where += "shp_grp = @shp_grp and sizebar_code = @sizebar_code and prod_show = 1 "
            qsc_products.select_where += "and prod_code in (select prod_code from uws_categories_prod)"

            qsc_products.add_parameter("shp_grp", _group_code)
            qsc_products.add_parameter("sizebar_code", _sizebar_code)

            qsc_products.select_order = "prod_prio"

            ' Voer de query uit
            dt_products = qsc_products.execute_query()
            Trace.Write("Productdetails ophalen - Eind")

            Trace.Write("Productdetails - maten ophalen - Begin")
            ' Haal de maten op, dit kun je doen door in de tabel uws_sizeview_sizes te kijken en een selectie te zetten op sizebar_code
            ' Maak een class met maten aan
            Dim qsc_sizes As New Utilize.Data.QuerySelectClass
            qsc_sizes.select_fields = "size_code, size_desc_" + Me.global_ws.Language + ", row_ord"
            qsc_sizes.select_from = "uws_sizeview_sizes"

            ' Zet een filter
            qsc_sizes.select_where = "sizebar_code = @sizebar_code"
            qsc_sizes.add_parameter("sizebar_code", _sizebar_code)

            qsc_sizes.select_order = "row_ord"

            ' Voer de query uit
            dt_sizes = qsc_sizes.execute_query()
            Trace.Write("Productdetails - maten ophalen - Eind")

            ' Als het geen sizeview product is, dan maken we een lege kolom aan.
            If Not _sizeview_product Then
                Dim dr_data_row As System.Data.DataRow = dt_sizes.NewRow()
                dr_data_row.Item("row_ord") = 0

                For Each dc_column As DataColumn In dt_sizes.Columns
                    If Not dc_column.ColumnName.ToLower() = "row_ord" Then
                        dr_data_row.Item(dc_column.ColumnName) = ""
                    End If
                Next

                dt_sizes.Rows.Add(dr_data_row)
                dt_products.AcceptChanges()
            End If

            ' Maak de size colors tabel aan (1)
            dt_size_colors = dt_products.Copy()


            ' Maak de sizeview tabel en control aan
            Trace.Write("Productdetails - matentabel aanmaken - Begin")
            Me.create_sizecolor_table()
            Me.create_sizecolor_control()
            Trace.Write("Productdetails - matentabel aanmaken - Eind")

            If ll_testing Then
                ' Doe hier de licentie controle, dit houdt in dat we checken of de module actief is
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview") = True Then
                    Me.ph_legend.Visible = True
                Else
                    Me.ph_legend.Visible = False
                End If

                Dim dt_data_table As System.Data.DataTable = Nothing

                If ll_testing Then
                    Dim qsc_data_table As New Utilize.Data.QuerySelectClass
                    qsc_data_table.select_fields = "*"
                    qsc_data_table.select_from = "uws_stock_indicators"
                    qsc_data_table.select_order = "stock_quantity desc"

                    dt_data_table = qsc_data_table.execute_query()
                End If

                If ll_testing Then
                    Me.rpt_legend.DataSource = dt_data_table
                    Me.rpt_legend.DataBind()
                End If

                Me.Visible = ll_testing

            End If

        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ll_testing As Boolean = Me.Visible

        If ll_testing Then
            ' Hier gaan we de totalen invullen bij een postback
            Dim ht_table As HtmlTable = Me.ph_sizes.FindControl("tbl_sizeview_table")

            For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows
                'Dim lo_total As Integer = 0
                Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))

                Dim ln_total As Integer = 0

                ' Loop door de kolommen heen
                For Each dt_data_column As DataColumn In dt_size_colors.Columns
                    If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                        ' Lees de textboxen
                        Dim txt_quantity As TextBox = hr_table_row.FindControl("qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))

                        Try
                            ln_total = ln_total + CInt(txt_quantity.Text)
                        Catch ex As Exception

                        End Try
                    End If
                Next

                ' Zet de waarde van het totaal
                CType(hr_table_row.FindControl("totalcolumn" + "_" + dr_data_row.Item("prod_code")), HtmlTableCell).InnerHtml = "<span></span>"
            Next
        End If
    End Sub

    Sub calc_total(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ll_testing As Boolean = Me.Visible

        If ll_testing Then
            ' Hier gaan we de totalen invullen bij een postback
            Dim ht_table As HtmlTable = Me.ph_sizes.FindControl("tbl_sizeview_table")

            For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows
                'Dim lo_total As Integer = 0
                Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))

                Dim ln_total As Integer = 0

                ' Loop door de kolommen heen
                For Each dt_data_column As DataColumn In dt_size_colors.Columns
                    If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                        ' Lees de textboxen
                        Dim txt_quantity As TextBox = hr_table_row.FindControl("qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))

                        Try
                            ln_total = ln_total + CInt(txt_quantity.Text)
                        Catch ex As Exception

                        End Try
                    End If
                Next

                ' Zet de waarde van het totaal
                CType(hr_table_row.FindControl("totalcolumn" + "_" + dr_data_row.Item("prod_code")), HtmlTableCell).InnerHtml = "<span></span>"
            Next
        End If
    End Sub

    Private Sub create_sizecolor_control()
        Dim ht_table As New HtmlTable
        ht_table.ID = "tbl_sizeview_table"
        ht_table.Attributes.Add("class", "sizeview_matrix")
        ht_table.CellPadding = 0
        ht_table.CellSpacing = 0
        ht_table.Border = 1
        ht_table.Style.Add("border-collapse", "collapse")

        Me.ph_sizes.Controls.Add(ht_table)

        ' Nieuwe table row voor de headers
        Dim hr_header_row As New HtmlTableRow
        ht_table.Rows.Add(hr_header_row)

        Trace.Write("Productdetails - matentabel headers aanmaken - Begin")

        ' Eerst de headers aanmaken
        For Each dt_data_column As DataColumn In dt_size_colors.Columns
            ' Nieuwe table cell en voeg deze toe aan de row
            Dim hc_header_cell As New HtmlTableCell
            hr_header_row.Cells.Add(hc_header_cell)

            If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                hc_header_cell.InnerText = CType(dt_data_column, SizeColumn).size_desc.Replace("Maat", "")
                hc_header_cell.ID = "sizecolumn_" + CType(dt_data_column, SizeColumn).size_code
                hc_header_cell.Attributes.Add("class", "header")
            Else
                Select Case dt_data_column.ColumnName
                    Case "prod_code"
                        hc_header_cell.Visible = False
                    Case "color_code"
                    Case Else
                        Exit Select
                End Select
            End If
        Next

        Trace.Write("Productdetails - matentabel headers aanmaken - Eind")

        ' Maak header totaal
        ' Nieuwe table cell en voeg deze toe aan de row
        Dim hc_header_cell_totaal As New HtmlTableCell
        hr_header_row.Cells.Add(hc_header_cell_totaal)
        hc_header_cell_totaal.InnerText = global_trans.translate_label("LABEL_TOTAL", "Totaal ", Me.global_ws.Language)
        hc_header_cell_totaal.Attributes.Add("class", "total")

        Trace.Write("Productdetails - matentabel controls aanmaken - Begin")
        ' Loop door de regels heen
        For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows
            ' Nieuwe table row en voeg deze toe aan de tabel
            Dim hr_table_row As New HtmlTableRow
            hr_table_row.ID = "hr_" + dr_data_row.Item("prod_code")
            ht_table.Rows.Add(hr_table_row)

            ' Een integer voor het totaal
            Dim ln_total As Integer = 0

            Dim ln_teller As Integer = 0

            ' Loop door de kolommen heen

            For Each dt_data_column As DataColumn In dt_size_colors.Columns
                ' Nieuwe table cell en voeg deze toe aan de row
                Dim hc_table_cell As New HtmlTableCell

                'Je zou zeggen dat het 0 moet zijn aangezien we de eerste kolom willen hebben.
                'Het werkt echter, dit is geen bug.
                If ln_teller = 1 Then
                    hc_table_cell.Attributes.Add("class", "color")
                Else
                    hc_table_cell.Attributes.Add("class", "size")
                End If

                hr_table_row.Cells.Add(hc_table_cell)

                If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                    hc_table_cell.ID = "tcs_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code")
                    hc_table_cell.Attributes.Add("style", "white-space:nowrap;")

                    ' Als het een sizeview kolom is, dan moet een textbox toegevoegd worden
                    Dim txt_quantity As New Utilize.Web.UI.WebControls.textbox
                    txt_quantity.ID = "qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code")

                    Dim button_add As New Utilize.Web.UI.WebControls.button
                    button_add.Text = "+"
                    button_add.CssClass = "inputbutton btn-u btn-u-sea-shop"
                    AddHandler button_add.Click, AddressOf calc_total

                    Dim button_subtract As New Utilize.Web.UI.WebControls.button
                    button_subtract.Text = "-"
                    button_subtract.CssClass = "inputbutton btn-u btn-u-sea-shop"

                    hc_table_cell.Controls.Add(button_subtract)
                    hc_table_cell.Controls.Add(txt_quantity)
                    hc_table_cell.Controls.Add(button_add)

                    txt_quantity.Text = "-"
                    txt_quantity.MaxLength = 4
                    txt_quantity.CssClass = "incrementcount"

                    button_add.OnClientClick = "increment('" + txt_quantity.ClientID + "'); return false;"
                    button_subtract.OnClientClick = "decrement('" + txt_quantity.ClientID + "'); return false;"


                    ' Bepaal de achtergrond kleur afhankelijk van de voorraad
                    Dim stock As Integer = get_product_stock(dr_data_row.Item("prod_code").ToString(), CType(dt_data_column, SizeColumn).size_code)
                    txt_quantity.Style.Add("background-color", get_stock_color(stock))

                    'If Utilize.Data.DataProcedures.GetValue("uws_sizeview_sizepro", "blocked", dr_data_row.Item("prod_code") + CType(dt_data_column, SizeColumn).size_code, "prod_code + size_code") = True Then
                    If get_blocked_product(dr_data_row.Item("prod_code"), CType(dt_data_column, SizeColumn).size_code) Then
                        button_add.Enabled = False
                        button_subtract.Enabled = False

                        txt_quantity.Enabled = False
                        txt_quantity.ReadOnly = True
                    End If
                Else
                    Select Case dt_data_column.ColumnName
                        Case "prod_code"
                            hc_table_cell.InnerText = dr_data_row.Item(dt_data_column.ColumnName)
                            hc_table_cell.Visible = False
                        Case "color_code"
                            Dim loSpan As New HtmlGenericControl("div")
                            loSpan.Attributes.Add("class", "color_desc")
                            loSpan.InnerText = Utilize.Data.DataProcedures.GetValue("uws_sizeview_colors", "color_desc_" + Me.global_ws.Language, dr_data_row.Item(dt_data_column.ColumnName))

                            hc_table_cell.Controls.Add(loSpan)
                        Case Else
                            Exit Select
                    End Select
                End If

                ln_teller = ln_teller + 1

            Next

            ' Nieuwe table cell -totaal van rij- en voeg deze toe aan de row
            Dim hc_table_total_cell As New HtmlTableCell
            hr_table_row.Cells.Add(hc_table_total_cell)
            hc_table_total_cell.ID = "totalcolumn" + "_" + dr_data_row.Item("prod_code")
            hc_table_total_cell.Attributes.Add("class", "total")
        Next
        Trace.Write("Productdetails - matentabel controls aanmaken - Eind")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Onderstaand stuk code zorgt ervoor dat geblokkeerde maten niet getoond worden
        Trace.Write("Productdetails - matentabel blokkades toevoegen - Begin")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        For Each dt_data_column As DataColumn In dt_size_colors.Columns
            If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                Dim ll_enabled As Boolean = False

                ' Loop door de rijzen heen
                For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows()
                    Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))

                    ' Zoek de quantity tekstbox op
                    Dim txt_quantity As Utilize.Web.UI.WebControls.textbox = hr_table_row.FindControl("qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))

                    ' Als een van de regels enabled is, dan moet de kolom zichtbaar gemaakt worden
                    If txt_quantity.Enabled Then
                        ll_enabled = True

                        Exit For
                    End If
                Next

                If Not ll_enabled Then
                    ' Zoek eerst de kolom cell op en maak deze onzichtbaar
                    Dim hc_header_cell As HtmlTableCell = ht_table.FindControl("sizecolumn_" + CType(dt_data_column, SizeColumn).size_code)
                    hc_header_cell.Visible = False

                    ' Loop door de rijzen heen
                    For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows()
                        Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))
                        Dim hc_table_cell1 As HtmlTableCell = hr_table_row.FindControl("tcs_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))
                        hc_table_cell1.Visible = False
                    Next
                End If
            End If
        Next

        Trace.Write("Productdetails - matentabel blokkades toevoegen - Begin")
    End Sub



    Private Sub create_sizecolor_table()
        ' Bouw de dt_size_colors tabel op en voeg de maten toe als kolom. (2)
        Dim lo_count As Integer = 0

        For Each drDataRow As DataRow In dt_sizes.Rows
            lo_count = lo_count + 1

            ' Voeg de maatkolommen toe
            Dim dcSizeColumn As New SizeColumn
            dcSizeColumn.DataType = System.Type.GetType("System.String")
            dcSizeColumn.size_code = drDataRow.Item("size_code")
            dcSizeColumn.size_desc = drDataRow.Item("size_desc_" + Me.global_ws.Language)
            dcSizeColumn.ColumnName = "maat_" & lo_count
            dcSizeColumn.ReadOnly = False
            dcSizeColumn.DefaultValue = ""

            ' Voeg de kolom toe aan de DataColumnCollection
            dt_size_colors.Columns.Add(dcSizeColumn)
        Next

    End Sub

    Protected Sub button_order_sizes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_order.Click
        Dim ll_testing As Boolean = True

        ' Controleer eerst of voor alle textboxes de juiste waarde is ingevuld. 
        Dim ht_table As HtmlTable = Me.ph_sizes.FindControl("tbl_sizeview_table")

        If ll_testing Then
            'Check bestellingen
            ' Loop door de regels heen
            For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows
                Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))

                ' Loop door de kolommen heen
                For Each dt_data_column As DataColumn In dt_size_colors.Columns

                    If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                        ' Lees de textboxen
                        Dim txt_quantity As Utilize.Web.UI.WebControls.textbox = hr_table_row.FindControl("qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))

                        If Not txt_quantity.Text = "-" Then
                            If Not txt_quantity.Text = "" Then
                                Try
                                    If CInt(txt_quantity.Text) < 0 Then
                                        ll_testing = False
                                    End If
                                Catch ex As Exception
                                    ll_testing = False
                                End Try
                            End If
                        End If

                        ' ER is een fout opgetreden, ga uit de loop
                        If Not ll_testing Then
                            Exit For
                        End If

                    End If

                Next

                ' ER is een fout opgetreden, ga uit de loop
                If Not ll_testing Then
                    Exit For
                End If
            Next

            If Not ll_testing Then
                ' De aantallen kloppen niet, geef een foutmelding
                Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                ' Error melding tonen
                If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                End If
            End If
        End If

        If ll_testing Then
            'Voeg bestellingen toe aan winkelwagen 
            ' Loop door de regels heen
            For Each dr_data_row As System.Data.DataRow In dt_size_colors.Rows
                'Dim lo_total As Integer = 0
                Dim hr_table_row As HtmlTableRow = ht_table.FindControl("hr_" + dr_data_row.Item("prod_code"))

                ' Loop door de kolommen heen
                For Each dt_data_column As DataColumn In dt_size_colors.Columns

                    If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                        ' Lees de textboxen
                        Dim txt_quantity As Utilize.Web.UI.WebControls.textbox = hr_table_row.FindControl("qty_" + dt_data_column.ColumnName + "_" + dr_data_row.Item("prod_code"))

                        If Not txt_quantity.Text = "-" Then
                            If Not txt_quantity.Text = "" Then
                                ' Controleer hier of de credits overschreden worden, of niet.
                                ll_testing = Me.global_ws.purchase_combination.check_limit(Me.global_ws.purchase_combination.user_id, dr_data_row.Item("prod_code"), CType(dt_data_column, SizeColumn).size_code, CInt(txt_quantity.Text))

                                If Not ll_testing Then
                                    Select Case global_ws.price_mode
                                        Case Webshop.price_mode.Credits
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_credit_reached", "Het maximum aantal toegekende credits wordt overschreden.", Me.global_ws.Language) + "');", True)
                                        Case Webshop.price_mode.Money
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_amount_reached", "Het maximum toegekend bedrag wordt overschreden.", Me.global_ws.Language) + "');", True)
                                        Case Webshop.price_mode.Unlimited
                                            Exit Select
                                        Case Else
                                            Exit Select
                                    End Select

                                    Exit For
                                Else
                                    Me.global_ws.shopping_cart.product_add(dr_data_row.Item("prod_code"), CInt(txt_quantity.Text), CType(dt_data_column, SizeColumn).size_code, "", "", "", Me.global_ws.purchase_combination.user_id)
                                End If
                            End If
                        End If
                    End If
                Next
            Next
        End If

        If ll_testing Then
            ' Als er in de webshop instellingen is aangegegeven dat je na het bestellen direct naar de winkelwagen gaat, dan omleiden.
            If Me.global_ws.get_webshop_setting("order_redirect") = True Then
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "$('#order_message').fadeIn().delay(1000).fadeOut();", True)
            End If
        End If
    End Sub

    Public Function order_fields() As Boolean
        Dim ll_testing As Boolean = True

        ' Eerst controleren of de aantallen correct zijn
        For Each dt_data_column As DataColumn In dt_size_colors.Columns
            If dt_data_column.GetType().Name.ToLower() = "sizecolumn" Then
                If Not get_blocked_product(Me.product_code, CType(dt_data_column, SizeColumn).size_code) Then
                    Dim txt_quantity As Utilize.Web.UI.WebControls.textbox = Me.ph_sizes.FindControl("qty_" + dt_data_column.ColumnName + "_" + Me.product_code)

                    Try
                        If Not txt_quantity.Text.Trim() = "" And Not txt_quantity.Text.Trim() = "-" And Not txt_quantity.Text.Trim() = "0" Then
                            If CInt(txt_quantity.Text) > 0 Then
                                ' Controleer hier of de credits overschreden worden, of niet.
                                ll_testing = Me.global_ws.purchase_combination.check_limit(Me.global_ws.purchase_combination.user_id, product_code, CType(dt_data_column, SizeColumn).size_code, CInt(txt_quantity.Text))

                                If Not ll_testing Then
                                    Select Case global_ws.price_mode
                                        Case Webshop.price_mode.Credits
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_credit_reached", "Het maximum aantal toegekende credits wordt overschreden.", Me.global_ws.Language) + "');", True)
                                        Case Webshop.price_mode.Money
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + global_trans.translate_message("message_max_amount_reached", "Het maximum toegekend bedrag wordt overschreden.", Me.global_ws.Language) + "');", True)
                                        Case Webshop.price_mode.Unlimited
                                            Exit Select
                                        Case Else
                                            Exit Select
                                    End Select

                                    Exit For
                                Else
                                    Me.global_ws.shopping_cart.product_add(_product_code, CInt(txt_quantity.Text), CType(dt_data_column, SizeColumn).size_code, "", "", "", Me.global_ws.purchase_combination.user_id)
                                End If
                            End If
                        End If
                    Catch ex As Exception

                        Exit For
                    End Try
                End If
            End If
        Next

        Return ll_testing
    End Function

#Region " Functies om gegevens op te halen"
    Private Function get_stock_color(ByVal stock_number As Integer) As String
        Dim color As String = "#ffffff"

        Dim qsc_stock As New Utilize.Data.QuerySelectClass
        qsc_stock.select_fields = "stock_color, stock_quantity"
        qsc_stock.select_from = "uws_stock_indicators"
        qsc_stock.select_order = "stock_quantity asc"

        qsc_stock.select_where = "stock_quantity >= @stock_quantity"
        qsc_stock.add_parameter("stock_quantity", stock_number)

        Dim dt_data_table As System.Data.DataTable = qsc_stock.execute_query()

        If dt_data_table.Rows.Count > 0 Then
            color = dt_data_table.Rows(0).Item("stock_color")
        End If

        Return color
    End Function

    Private ds_blocked As New System.Data.DataSet
    Private Function get_blocked_product(ByVal product_code As String, ByVal size As String) As Boolean
        If ds_blocked.Tables(product_code) Is Nothing Then
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "prod_code, size_code, blocked, prod_stock"
            qsc_select.select_from = "uws_sizeview_sizepro"
            qsc_select.select_where = "prod_code = @prod_code"
            qsc_select.add_parameter("prod_code", product_code)

            ' Voer de query toe
            Dim dt_data_table As System.Data.DataTable = qsc_select.execute_query()
            dt_data_table.TableName = product_code

            ' Voeg de tabel toe
            ds_blocked.Tables.Add(dt_data_table)
        End If

        Dim ll_blocked As Boolean = True

        Dim dr_data_row() As System.Data.DataRow = ds_blocked.Tables(product_code).Select("prod_code = '" + product_code + "' and size_code = '" + size + "'")

        If dr_data_row.Length > 0 Then
            ll_blocked = dr_data_row(0).Item("blocked")
        End If

        Return ll_blocked
    End Function
    Private Function get_product_stock(ByVal product_code As String, ByVal size As String) As Decimal
        If ds_blocked.Tables(product_code) Is Nothing Then
            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "prod_code, size_code, blocked, prod_stock"
            qsc_select.select_from = "uws_sizeview_sizepro"
            qsc_select.select_where = "prod_code = @prod_code"
            qsc_select.add_parameter("prod_code", product_code)

            ' Voer de query toe
            Dim dt_data_table As System.Data.DataTable = qsc_select.execute_query()
            dt_data_table.TableName = product_code

            ' Voeg de tabel toe
            ds_blocked.Tables.Add(dt_data_table)
        End If

        Dim ln_stock As Decimal = 0

        Dim dr_data_row() As System.Data.DataRow = ds_blocked.Tables(product_code).Select("prod_code = '" + product_code + "' and size_code = '" + size + "'")

        If dr_data_row.Length > 0 Then
            ln_stock = dr_data_row(0).Item("prod_stock")
        End If

        Return ln_stock
    End Function
#End Region

End Class
