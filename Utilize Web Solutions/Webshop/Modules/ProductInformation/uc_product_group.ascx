﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_group.ascx.vb" Inherits="uc_product_group" %>

<div class="uc_product_group margin-bottom-10">
    <utilize:placeholder runat="server" id="ph_group_1">
        <div class="row margin-bottom-10">
            <div class="col-md-4 col-xs-4">
                <span class="allign"><utilize:literal ID="lt_label_desc1" runat="server"></utilize:literal>:</span>
            </div>
            <div class="col-md-8 col-xs-8">
                <utilize:dropdownlist ID="dd_group_1" runat="server" AutoPostBack="true" CssClass="form-control"></utilize:dropdownlist>
            </div>
        </div>
            
                
    </utilize:placeholder>
    <utilize:placeholder runat="server" id="ph_group_2">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <span class="allign"><utilize:literal ID="lt_label_desc2" runat="server"></utilize:literal>:</span>
            </div>
            <div class="col-md-8 col-xs-8">
                <utilize:dropdownlist ID="dd_group_2" runat="server" AutoPostBack="true" CssClass="form-control"></utilize:dropdownlist>                    
            </div>
        </div>
    </utilize:placeholder>
</div>