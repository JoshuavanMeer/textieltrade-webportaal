﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_sizeview_group.ascx.vb" Inherits="uc_sizeview_group" %>
<div class="uc_sizeview_group col-md-12">
    <utilize:placeholder runat='server' ID="ph_colors">
        <div class="row">    
            <label><utilize:translatelabel runat="server" ID="label_select_color" Text="Kies een kleur"></utilize:translatelabel></label>
            <utilize:dropdownlist runat="server" ID="dd_color" AutoPostBack="True" CssClass="form-control"></utilize:dropdownlist>
        </div>
    </utilize:placeholder>
    <utilize:placeholder runat='server' ID="ph_sizes">
        <div class="row margin-bottom-5">
            <label><utilize:translatelabel runat="server" ID="label_select_size" Text="Kies een maat"></utilize:translatelabel></label>
            <utilize:dropdownlist runat="server" ID="dd_size" AutoPostBack='true' CssClass="form-control"></utilize:dropdownlist>
        </div>
    </utilize:placeholder>
</div>