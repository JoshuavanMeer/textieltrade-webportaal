﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_order_product.ascx.vb" Inherits="uc_order_product" %>

<div class="uc_order_product">

    <utilize:placeholder runat="server" ID="ph_row_reference" Visible="false">
       <utilize:textbox runat="server" CssClass="product_comment form-control" ID="txt_product_comment" ></utilize:textbox><br>
    </utilize:placeholder>
    
    <utilize:placeholder runat="server" ID="ph_order">
        <div class="order">
            <utilize:panel runat="server" ID="ph_quantity_sale_unit" Visible='false' CssClass="sale_unit " data-order-target="order-prod-control">
                <utilize:placeholder runat="server" ID="ph_quantity">
                    <div class="product-quantity ">
                        <utilize:button runat="server" CssClass="increment quantity-button quantity-min" ID="img_min" Text="-"></utilize:button>
                        <utilize:textbox runat="server" ID="txt_quantity_sale_unit" MaxLength="4" Text="1" CssClass="quantity quantity-field" data-order-target="order-prod-quantity"></utilize:textbox>
                        <utilize:label runat="server" ID="lt_quantity_sale_unit" visible="false" CssClass="quantity-multiplier"></utilize:label>
                        <utilize:button runat="server" CssClass="increment quantity-button quantity-plus" ID="img_plus" Text="+"></utilize:button>
                    </div>
                    <div class="quantity-multiplier">
                        <span class="x">X</span>
                        <utilize:label runat="server" ID="lbl_sale_unit" Text="1" CssClass="sale_unit"></utilize:label>
                        <span class="is">=</span>
                        <utilize:label runat="server" ID="lbl_quantity_sale_unit" Text="1" CssClass="order_quantity"></utilize:label>
                    </div>
                </utilize:placeholder>
                <utilize:button runat="server" CssClass="btn-u btn-u-sea-shop btn-u-lg" ID="button_order" Text="Bestellen" CommandName="order" UseSubmitBehavior="true" data-order-target="order-prod-button"/>
            </utilize:panel>
        
            <utilize:panel runat="server" ID="ph_quantity_product_unit" CssClass="units-container" data-order-target="order-prod-control">
                <div class="units">
                    <utilize:repeater runat="server" ID="rpt_units">
                        <HeaderTemplate>
                            <table>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td>
                                        <utilize:literal runat="server" id="lt_unit_code" visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "unit_code") %>' ></utilize:literal>
                                        <utilize:button runat="server" ID="img_min_unit" CssClass="increment quantity-button quantity-min" imageurl="~/DefaultImages/button_min.jpg" text="-" />
                                        <utilize:textbox runat="server" ID="txt_quantity_unit" MaxLength="4" Text="0" CssClass="quantity quantity-field" data-order-target="order-prod-quantity"></utilize:textbox>
                                        <utilize:button runat="server" ID="img_plus_unit" CssClass="increment quantity-button quantity-plus" imageurl="~/DefaultImages/button_plus.jpg" text="+" />
                                    </td>
                                    <td class="description">
                                        <%# Utilize.Data.DataProcedures.GetValue("uws_units", "unit_desc_" + Me.global_ws.Language, DataBinder.Eval(Container.DataItem, "unit_code")) + " (" %> <utilize:translateliteral runat="server" ID="label_unit_content" text="Inhoud"></utilize:translateliteral> : <%# String.Format("{0:G29}", DataBinder.Eval(Container.DataItem, "unit_size")).ToString() + " " %> <utilize:translateliteral runat="server" Text="Stuks" ID="label_unit_pieces"></utilize:translateliteral>)
                                    </td>
                                </tr>
                        </ItemTemplate>            
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </utilize:repeater>
                </div>

                <utilize:button runat="server" cssclass="btn-u btn-u-sea-shop btn-u-lg" ID="button_order2" Text="Bestellen" CommandName="order" UseSubmitBehavior="true" data-order-target="order-prod-button"/>
            </utilize:panel>

            <utilize:panel runat="server" ID="ph_quantity_normal" CssClass="normal" data-order-target="order-prod-control">
                <utilize:placeholder runat="server" ID="ph_quantity1">
                    <div class="product-quantity">
                        <utilize:button runat="server" CssClass="increment quantity-button quantity-min" ID="img_min_normal" Text="-" UseSubmitBehavior="false"></utilize:button>
                        <utilize:textbox runat="server" ID="txt_quantity_normal" MaxLength="4" Text="1" CssClass="quantity quantity-field" data-order-target="order-prod-quantity"></utilize:textbox>
                        <utilize:label runat="server" ID="lt_quantity_normal" visible="false" CssClass="quantity-multiplier"></utilize:label>
                        <utilize:button runat="server" CssClass="increment quantity-button quantity-plus" ID="img_plus_normal" Text="+" UseSubmitBehavior="false"></utilize:button>
                    </div>
                </utilize:placeholder>

                <utilize:button runat="server" CssClass="btn-u btn-u-sea-shop btn-u-lg" ID="button_order1" Text="Bestellen" CommandName="order" UseSubmitBehavior="true" data-order-target="order-prod-button"/>
            </utilize:panel>

            <utilize:label runat="server" ID="lbl_quantity_normal" Text="1" CssClass="order_quantity" Visible="false"></utilize:label>
        </div>
    </utilize:placeholder>
</div>