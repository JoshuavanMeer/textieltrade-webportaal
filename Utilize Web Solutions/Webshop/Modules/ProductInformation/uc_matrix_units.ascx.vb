﻿
Partial Class Webshop_Modules_ProductInformation_uc_matrix_units
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _ProductCode As String = ""
    Public Property product_code As String
        Get
            Return _ProductCode
        End Get
        Set(ByVal value As String)
            _ProductCode = value
        End Set
    End Property

    Private _size As String = ""
    Public Property size As String
        Get
            Return _size
        End Get
        Set(ByVal value As String)
            _size = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim ll_resume As Boolean = True
        Dim dt_y_units As System.Data.DataTable = Nothing
        Dim dt_x_units As System.Data.DataTable = Nothing
        Dim dt_find_main_item As System.Data.DataTable = nothing

        Dim qsc_x_units As New Utilize.Data.QuerySelectClass
        Dim qsc_y_units As New Utilize.Data.QuerySelectClass

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") = True
        End If

        If ll_resume Then
            ' Change to mainitemmatrix prod_code if not mainitemmatrix product code
            Dim qsc_find_main_item As New Utilize.Data.QuerySelectClass
            qsc_find_main_item.select_fields = "*"
            qsc_find_main_item.select_from = "uws_products"
            qsc_find_main_item.select_where = "prod_code = @prod_code and prod_show = 1"
            qsc_find_main_item.add_parameter("@prod_code", _ProductCode)

            dt_find_main_item = qsc_find_main_item.execute_query()

            ll_resume = dt_find_main_item.Rows.Count > 0
        End If

        If ll_resume Then
            ' Als het niet het hoofdartikel is, dan wordt de productcode vervangen door de productcode van het hoofdartikel
            If dt_find_main_item.Rows.Count > 0 Then
                If dt_find_main_item.Rows(0).Item("ismainitemmatrix") = False Then
                    _ProductCode = dt_find_main_item.Rows(0).Item("mainitemmatrix")
                End If
            End If
        End If

        If ll_resume Then
            ' Find the matrix units
            qsc_x_units.select_fields = "unit_code, description_" + Me.global_ws.Language
            qsc_x_units.select_from = "uws_products, uws_prod_mtx_unit_tr"
            qsc_x_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixxunit group by description_" + Me.global_ws.Language + ", unit_code"
            qsc_x_units.add_parameter("@prod_code", _ProductCode)

            dt_x_units = qsc_x_units.execute_query()
        End If

        If ll_resume Then
            qsc_y_units.select_fields = "unit_code, description_" + Me.global_ws.Language
            qsc_y_units.select_from = "uws_products, uws_prod_mtx_unit_tr"
            qsc_y_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixyunit group by description_" + Me.global_ws.Language + ", unit_code"
            qsc_y_units.add_parameter("@prod_code", _ProductCode)

            dt_y_units = qsc_y_units.execute_query()
        End If

        If ll_resume Then
            ' Check of er regels gevonden zijn
            ll_resume = dt_x_units.Rows.Count > 0 Or dt_y_units.Rows.Count > 0
        End If

        If ll_resume Then

            If dt_x_units.Rows.Count < dt_y_units.Rows.Count Then
                Me.ddl_unit_1.DataSource = dt_x_units

                'Set second unit to available values of first one
                qsc_y_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixyunit and matrixxunit = @matrixxunit group by description_" + Me.global_ws.Language + ", unit_code"

                If dt_find_main_item.Rows.Count > 0 And dt_find_main_item.Rows(0).Item("ismainitemmatrix") = False Then
                    qsc_y_units.add_parameter("@matrixxunit", dt_find_main_item.Rows(0).Item("matrixxunit"))
                Else
                    qsc_y_units.add_parameter("@matrixxunit", dt_x_units.Rows(0).Item("unit_code"))
                End If

                dt_y_units = qsc_y_units.execute_query()

                Me.ddl_unit_2.DataSource = dt_y_units
            Else
                Me.ddl_unit_1.DataSource = dt_y_units

                'Set second unit to available values of first one
                qsc_x_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixxunit and matrixyunit = @matrixyunit group by description_" + Me.global_ws.Language + ", unit_code"

                If dt_find_main_item.Rows.Count > 0 And dt_find_main_item.Rows(0).Item("ismainitemmatrix") = False Then
                    qsc_x_units.add_parameter("@matrixyunit", dt_find_main_item.Rows(0).Item("matrixyunit"))
                Else
                    qsc_x_units.add_parameter("@matrixyunit", dt_y_units.Rows(0).Item("unit_code"))
                End If
                dt_x_units = qsc_x_units.execute_query()

                Me.ddl_unit_2.DataSource = dt_x_units
            End If

            Me.ddl_unit_1.DataValueField = "unit_code"
            Me.ddl_unit_1.DataTextField = "Description_" + Me.global_ws.Language
            Me.ddl_unit_1.DataBind()
            Me.ddl_unit_1.SelectedIndex = 0

            Me.ddl_unit_2.DataValueField = "unit_code"
            Me.ddl_unit_2.DataTextField = "Description_" + Me.global_ws.Language
            Me.ddl_unit_2.DataBind()
            Me.ddl_unit_2.SelectedIndex = 0

            'If prod_code is sub product, then set dropdownlist to same selection
            If dt_find_main_item.Rows.Count > 0 And dt_find_main_item.Rows(0).Item("ismainitemmatrix") = False Then
                If dt_x_units.Rows.Count < dt_y_units.Rows.Count Then
                    Me.ddl_unit_1.SelectedValue = dt_find_main_item.Rows(0).Item("matrixxunit")
                    Me.ddl_unit_2.SelectedValue = dt_find_main_item.Rows(0).Item("matrixyunit")
                Else
                    Me.ddl_unit_1.SelectedValue = dt_find_main_item.Rows(0).Item("matrixyunit")
                    Me.ddl_unit_2.SelectedValue = dt_find_main_item.Rows(0).Item("matrixxunit")
                End If
            Else
                'If prod_code is main product, then add - for selection to first row of unit dropdown lists
                Me.ddl_unit_1.Items.Insert(0, "-")
                Me.ddl_unit_2.Items.Clear()
                Me.ddl_unit_1.SelectedIndex = 0

                If dt_x_units.Rows.Count < dt_y_units.Rows.Count Then
                    'Set second unit to all available values
                    qsc_y_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixyunit group by description_" + Me.global_ws.Language + ", unit_code"
                    dt_y_units = qsc_y_units.execute_query()

                    Me.ddl_unit_2.DataSource = dt_y_units
                Else
                    'Set second unit to all available values
                    qsc_x_units.select_where = "mainitemmatrix = @prod_code and prod_show = 1 and unit_code = matrixxunit group by description_" + Me.global_ws.Language + ", unit_code"
                    dt_x_units = qsc_x_units.execute_query()

                    Me.ddl_unit_2.DataSource = dt_x_units
                End If

                Me.ddl_unit_2.DataBind()
                Me.ddl_unit_2.Items.Insert(0, "-")
                Me.ddl_unit_2.SelectedIndex = 0
            End If

            Me.pnl_matrix_unit.Visible = True
        End If
    End Sub

    Protected Sub ddl_unit_1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_unit_1.SelectedIndexChanged
        redirect_to_selection(1)
    End Sub

    Protected Sub ddl_unit_2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_unit_2.SelectedIndexChanged
        redirect_to_selection(2)
    End Sub

    Private Sub redirect_to_selection(ByVal unit_number As Integer)
        Dim qsc_prodcode_from_sub As New Utilize.Data.QuerySelectClass
        qsc_prodcode_from_sub.select_fields = "prod_code"
        qsc_prodcode_from_sub.select_from = "uws_products"
        qsc_prodcode_from_sub.select_where = "mainitemmatrix = @prod_code and prod_show = 1 And ((matrixxunit = @matrix_u1_1 And matrixyunit = @matrix_u2_1) Or (matrixxunit = @matrix_u2_2 and matrixyunit = @matrix_u1_2))"
        qsc_prodcode_from_sub.add_parameter("@prod_code", _ProductCode)
        qsc_prodcode_from_sub.add_parameter("@matrix_u1_1", Me.ddl_unit_1.SelectedValue)
        qsc_prodcode_from_sub.add_parameter("@matrix_u1_2", Me.ddl_unit_1.SelectedValue)
        qsc_prodcode_from_sub.add_parameter("@matrix_u2_1", Me.ddl_unit_2.SelectedValue)
        qsc_prodcode_from_sub.add_parameter("@matrix_u2_2", Me.ddl_unit_2.SelectedValue)

        Dim dt_prodcode_from_sub As New System.Data.DataTable
        dt_prodcode_from_sub = qsc_prodcode_from_sub.execute_query()

        If dt_prodcode_from_sub.Rows.Count > 0 Then

            Dim uc_product_Details = TryCast(Me.Parent.Parent.Parent.Parent.Controls.Item(0), Utilize.Web.Solutions.Webshop.ws_user_control)
            If uc_product_Details IsNot Nothing Then
                uc_product_Details.execute_function("SetProductCode", dt_prodcode_from_sub.Rows(0).Item("prod_code"))
            End If
        Else

            Dim qsc_prodcode_to_sub As New Utilize.Data.QuerySelectClass
            qsc_prodcode_to_sub.select_fields = "prod_code"
            qsc_prodcode_to_sub.select_from = "uws_products"
            qsc_prodcode_to_sub.select_where = "mainitemmatrix in (select mainitemmatrix from uws_products where prod_code = @prod_code) and prod_show = 1 And ((matrixxunit = @matrix_u1_1 And matrixyunit = @matrix_u2_1) Or (matrixxunit = @matrix_u2_2 and matrixyunit = @matrix_u1_2))"
            qsc_prodcode_to_sub.add_parameter("@prod_code", _ProductCode)
            qsc_prodcode_to_sub.add_parameter("@matrix_u1_1", Me.ddl_unit_1.SelectedValue)
            qsc_prodcode_to_sub.add_parameter("@matrix_u1_2", Me.ddl_unit_1.SelectedValue)
            qsc_prodcode_to_sub.add_parameter("@matrix_u2_1", Me.ddl_unit_2.SelectedValue)
            qsc_prodcode_to_sub.add_parameter("@matrix_u2_2", Me.ddl_unit_2.SelectedValue)

            Dim dt_prodcode_to_sub As New System.Data.DataTable
            dt_prodcode_to_sub = qsc_prodcode_to_sub.execute_query()

            If dt_prodcode_to_sub.Rows.Count > 0 Then
                Dim uc_product_Details = TryCast(Me.Parent.Parent.Parent.Parent.Controls.Item(0), Utilize.Web.Solutions.Webshop.ws_user_control)

                If uc_product_Details IsNot Nothing Then
                    uc_product_Details.execute_function("SetProductCode", dt_prodcode_to_sub.Rows(0).Item("prod_code"))
                End If
            ElseIf unit_number = 1 Or unit_number = 2 Then
                Dim qsc_prodcode_to_sub_unit As New Utilize.Data.QuerySelectClass
                qsc_prodcode_to_sub_unit.select_fields = "prod_code"
                qsc_prodcode_to_sub_unit.select_from = "uws_products"
                qsc_prodcode_to_sub_unit.select_where = "mainitemmatrix in (select mainitemmatrix from uws_products where prod_code = @prod_code or mainitemmatrix = @mainitemmatrix) and prod_show = 1 And (matrixxunit = @matrix_unit1 or matrixyunit = @matrix_unit2)"
                qsc_prodcode_to_sub_unit.add_parameter("@prod_code", _ProductCode)
                qsc_prodcode_to_sub_unit.add_parameter("@mainitemmatrix", _ProductCode)

                If unit_number = 1 Then
                    qsc_prodcode_to_sub_unit.add_parameter("@matrix_unit1", Me.ddl_unit_1.SelectedValue)
                    qsc_prodcode_to_sub_unit.add_parameter("@matrix_unit2", Me.ddl_unit_1.SelectedValue)
                Else
                    qsc_prodcode_to_sub_unit.add_parameter("@matrix_unit1", Me.ddl_unit_2.SelectedValue)
                    qsc_prodcode_to_sub_unit.add_parameter("@matrix_unit2", Me.ddl_unit_2.SelectedValue)
                End If

                Dim dt_prodcode_to_sub_unit As New System.Data.DataTable
                dt_prodcode_to_sub_unit = qsc_prodcode_to_sub_unit.execute_query()

                If dt_prodcode_to_sub_unit.Rows.Count > 0 Then
                    Dim uc_product_Details = TryCast(Me.Parent.Parent.Parent.Parent.Controls.Item(0), Utilize.Web.Solutions.Webshop.ws_user_control)

                    If uc_product_Details IsNot Nothing Then
                        uc_product_Details.execute_function("SetProductCode", dt_prodcode_to_sub_unit.Rows(0).Item("prod_code"))
                    End If
                End If
            End If
        End If
    End Sub
End Class
