﻿
Partial Class Webshop_Modules_ProductInformation_uc_product_price
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _sales_action As Boolean = False
    Private _ProductCode As String = ""

    Public Property product_code As String
        Get
            Return _ProductCode
        End Get
        Set(ByVal value As String)
            _ProductCode = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.block_css = False
        Me.set_style_sheet("uc_product_price.css", Me)

        Dim pf_product_details As New ws_productdetails
        Dim dt_product_details As System.Data.DataTable = pf_product_details.get_product_details(_ProductCode, Me.global_ws.Language)

        If dt_product_details.Rows.Count > 0 Then
            Me._sales_action = global_ws.check_sales_action(Me._ProductCode)

            Dim ll_sales_action_1 As Boolean = False
            Dim ll_sales_action_2 As Boolean = False
            Dim ll_sales_action_3 As Boolean = False

            Dim price_type = Me.global_ws.get_webshop_setting("price_type")

            Select Case price_type
                Case 1 ' excl
                    prices_excl(ll_sales_action_1, ll_sales_action_2, ll_sales_action_3)
                Case 2 ' incl
                    prices_incl(ll_sales_action_1, ll_sales_action_2, ll_sales_action_3, False)
                Case 3 'both
                    prices_incl(ll_sales_action_1, ll_sales_action_2, ll_sales_action_3, True)
                    prices_excl(ll_sales_action_1, ll_sales_action_2, ll_sales_action_3)
                Case Else 'incl
                    prices_incl(ll_sales_action_1, ll_sales_action_2, ll_sales_action_3, False)
            End Select

            If ll_sales_action_1 And Me._sales_action Then
                'excl
                Me.lbl_prod_price1_excl.CssClass = Me.lbl_prod_price1_excl.CssClass + " salesaction_to"
                Me.lbl_prod_price1_original_excl.CssClass = Me.lbl_prod_price1_original_incl.CssClass + " salesaction_for"
                'incl
                Me.lbl_prod_price1_incl.CssClass = Me.lbl_prod_price1_excl.CssClass + " salesaction_to"
                Me.lbl_prod_price1_original_incl.CssClass = Me.lbl_prod_price1_original_incl.CssClass + " salesaction_for"
            End If

            If ll_sales_action_2 And Me._sales_action Then
                'excl
                Me.lbl_prod_price2_excl.CssClass = Me.lbl_prod_price2_excl.CssClass + " salesaction_to"
                Me.lbl_prod_price2_original_excl.CssClass = Me.lbl_prod_price2_original_excl.CssClass + " salesaction_for"
                'incl
                Me.lbl_prod_price2_incl.CssClass = Me.lbl_prod_price2_incl.CssClass + " salesaction_to"
                Me.lbl_prod_price2_original_incl.CssClass = Me.lbl_prod_price2_original_incl.CssClass + " salesaction_for"
            End If

            If ll_sales_action_3 And Me._sales_action Then
                'excl
                Me.lbl_prod_price3_excl.CssClass = Me.lbl_prod_price3_excl.CssClass + " salesaction_to"
                Me.lbl_prod_price3_original_excl.CssClass = Me.lbl_prod_price3_original_excl.CssClass + " salesaction_for"
                'incl
                Me.lbl_prod_price3_incl.CssClass = Me.lbl_prod_price3_incl.CssClass + " salesaction_to"
                Me.lbl_prod_price3_original_incl.CssClass = Me.lbl_prod_price3_original_incl.CssClass + " salesaction_for"
            End If

            If Not Me._sales_action And Not ll_sales_action_1 Then
                'excl
                Me.lbl_prod_price1_original_excl.CssClass = "value original originaldefault"
                Me.lbl_prod_price1_excl.CssClass = "value actual actualdefault"
                'incl
                Me.lbl_prod_price1_original_incl.CssClass = "value original originaldefault"
                Me.lbl_prod_price1_incl.CssClass = "value actual actualdefault"
            End If

            If Not Me._sales_action And Not ll_sales_action_2 Then
                'excl
                Me.lbl_prod_price2_original_excl.CssClass = "value original originaldefault"
                Me.lbl_prod_price2_excl.CssClass = "value actual actualdefault"
                'incl
                Me.lbl_prod_price2_original_incl.CssClass = "value original originaldefault"
                Me.lbl_prod_price2_incl.CssClass = "value actual actualdefault"
            End If

            If Not Me._sales_action And Not ll_sales_action_3 Then
                'excl
                Me.lbl_prod_price3_original_excl.CssClass = "value original originaldefault"
                Me.lbl_prod_price3_excl.CssClass = "value actual actualdefault"
                'incl
                Me.lbl_prod_price3_original_incl.CssClass = "value original originaldefault"
                Me.lbl_prod_price3_incl.CssClass = "value actual actualdefault"
            End If

            ' De prijzen mogen niet zichtbaar zijn indien de baliefunctie actief is, en het UserShowPrices attribuut in de sessie op false staat.
            Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
            If prices_not_visible Then
                Me.ph_prod_price1.Visible = False
                Me.ph_prod_price2.Visible = False
                Me.ph_prod_price3.Visible = False
            Else
                Me.ph_prod_price1.Visible = IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
                Me.ph_prod_price2.Visible = Me.global_ws.get_webshop_setting("prod_price1_avail") And IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
                Me.ph_prod_price3.Visible = Me.global_ws.get_webshop_setting("prod_price2_avail") And IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
            End If

            ' Als er extra prijzen gebruikt moeten worden en er is aangegeven dat dat de prijs niet getoond moet worden wanneer deze 0 is
            If Not Me.global_ws.get_webshop_setting("prod_price1_show") And Me.lbl_prod_price2_excl.Visible Or Me.lbl_prod_price2_incl.Visible Then
                If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_price1", _ProductCode) = 0 Then
                    Me.ph_prod_price2.Visible = False
                End If
            End If

            ' Als er extra prijzen gebruikt moeten worden en er is aangegeven dat dat de prijs niet getoond moet worden wanneer deze 0 is
            If Not Me.global_ws.get_webshop_setting("prod_price2_show") And Me.lbl_prod_price3_excl.Visible Or Me.lbl_prod_price3_incl.Visible Then
                If Utilize.Data.DataProcedures.GetValue("uws_products", "prod_price2", _ProductCode) = 0 Then
                    Me.ph_prod_price3.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub prices_incl(ByRef ll_sales_action_1 As Boolean, ByRef ll_sales_action_2 As Boolean, ByRef ll_sales_action_3 As Boolean, ll_both As Boolean)

        If ll_both Then
            Me.ph_prices_wrapper1.Attributes.Add("class", Me.ph_prices_wrapper1.Attributes.Item("class") + " both-vat-prices")
            Me.ph_prices_wrapper2.Attributes.Add("class", Me.ph_prices_wrapper2.Attributes.Item("class") + " both-vat-prices")
            Me.ph_prices_wrapper3.Attributes.Add("class", Me.ph_prices_wrapper3.Attributes.Item("class") + " both-vat-prices")

            Me.label_price1_excl_vat.Visible = True
            Me.label_price1_incl_vat.Visible = True
            Me.label_price2_excl_vat.Visible = True
            Me.label_price2_incl_vat.Visible = True
            Me.label_price3_excl_vat.Visible = True
            Me.label_price3_incl_vat.Visible = True
        End If

        ' Incl. BTW
        Dim ln_prod_price1_incl As Decimal = Me.global_ws.get_product_price_incl_vat(_ProductCode, "")
        Dim ln_prod_price2_incl As Decimal = Me.global_ws.get_product_price1_incl_vat(_ProductCode, "")
        Dim ln_prod_price3_incl As Decimal = Me.global_ws.get_product_price2_incl_vat(_ProductCode, "")

        Dim ln_prod_original_price1_incl As Decimal = Me.global_ws.get_product_price_default_incl_vat(_ProductCode)
        Dim ln_prod_original_price2_incl As Decimal = Me.global_ws.get_product_price1_default_incl_vat(_ProductCode)
        Dim ln_prod_original_price3_incl As Decimal = Me.global_ws.get_product_price2_default_incl_vat(_ProductCode)

        If Not ll_both Then
            ll_sales_action_1 = ln_prod_original_price1_incl > ln_prod_price1_incl
            ll_sales_action_2 = ln_prod_original_price2_incl > ln_prod_price2_incl
            ll_sales_action_3 = ln_prod_original_price3_incl > ln_prod_price3_incl
        End If

        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
            ' Standaard prijzen
            Me.lbl_prod_price1_incl.Text = Format(ln_prod_price1_incl, "c")
            Me.lbl_prod_price2_incl.Text = Format(ln_prod_price2_incl, "c")
            Me.lbl_prod_price3_incl.Text = Format(ln_prod_price3_incl, "c")

            Me.lbl_prod_price1_original_incl.Text = Format(ln_prod_original_price1_incl, "c")
            Me.lbl_prod_price2_original_incl.Text = Format(ln_prod_original_price2_incl, "c")
            Me.lbl_prod_price3_original_incl.Text = Format(ln_prod_original_price3_incl, "c")
        Else
            ' Standaard prijzen
            Me.lbl_prod_price1_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price1_incl)
            Me.lbl_prod_price2_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price2_incl)
            Me.lbl_prod_price3_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price3_incl)

            Me.lbl_prod_price1_original_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price1_incl)
            Me.lbl_prod_price2_original_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price2_incl)
            Me.lbl_prod_price3_original_incl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price3_incl)
        End If
    End Sub

    Private Sub prices_excl(ByRef ll_sales_action_1 As Boolean, ByRef ll_sales_action_2 As Boolean, ByRef ll_sales_action_3 As Boolean)
        ' Excl. BTW
        Dim ln_prod_price1_excl As Decimal = Me.global_ws.get_product_price(_ProductCode, "")
        Dim ln_prod_price2_excl As Decimal = Me.global_ws.get_product_price1(_ProductCode, "")
        Dim ln_prod_price3_excl As Decimal = Me.global_ws.get_product_price2(_ProductCode, "")

        Dim ln_prod_original_price1_excl As Decimal = Me.global_ws.get_product_price_default(_ProductCode)
        Dim ln_prod_original_price2_excl As Decimal = Me.global_ws.get_product_price1_default(_ProductCode)
        Dim ln_prod_original_price3_excl As Decimal = Me.global_ws.get_product_price2_default(_ProductCode)

        ll_sales_action_1 = ln_prod_original_price1_excl > ln_prod_price1_excl
        ll_sales_action_2 = ln_prod_original_price2_excl > ln_prod_price2_excl
        ll_sales_action_3 = ln_prod_original_price3_excl > ln_prod_price3_excl

        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
            ' Standaard prijzen
            Me.lbl_prod_price1_excl.Text = Format(ln_prod_price1_excl, "c")
            Me.lbl_prod_price2_excl.Text = Format(ln_prod_price2_excl, "c")
            Me.lbl_prod_price3_excl.Text = Format(ln_prod_price3_excl, "c")

            Me.lbl_prod_price1_original_excl.Text = Format(ln_prod_original_price1_excl, "c")
            Me.lbl_prod_price2_original_excl.Text = Format(ln_prod_original_price2_excl, "c")
            Me.lbl_prod_price3_original_excl.Text = Format(ln_prod_original_price3_excl, "c")
        Else
            ' Standaard prijzen
            Me.lbl_prod_price1_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price1_excl)
            Me.lbl_prod_price2_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price2_excl)
            Me.lbl_prod_price3_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price3_excl)

            Me.lbl_prod_price1_original_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price1_excl)
            Me.lbl_prod_price2_original_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price2_excl)
            Me.lbl_prod_price3_original_excl.Text = Me.global_ws.convert_amount_to_credits(ln_prod_original_price3_excl)
        End If
    End Sub
End Class
