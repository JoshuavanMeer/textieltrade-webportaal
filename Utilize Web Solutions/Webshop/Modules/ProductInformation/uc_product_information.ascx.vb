﻿
Partial Class Webshop_Modules_ProductInformation_uc_product_information
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _ProductCode As String = ""
    Public Property product_code As String
        Get
            Return _ProductCode
        End Get
        Set(ByVal value As String)
            _ProductCode = value
            Me.Display_Product()
        End Set
    End Property

    Private _controls_added As Boolean = False
    Private uc_product_price As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    Private Sub Display_Product()
        'eerst gegevens ophalen
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Als de controls nog niet toegevoegd zijn, dan moet de usercontrol geladen worden
            If Not Me._controls_added Then
                uc_product_price = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_price.ascx")
            End If

            uc_product_price.set_property("product_code", Me.product_code)

            If Not Me._controls_added Then
                ph_product_price.Controls.Add(uc_product_price)
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.block_css = False
        Me.set_style_sheet("uc_product_information.css", Me)

        Dim pf_product_details As New ws_productdetails
        Dim dt_product_details As System.Data.DataTable = pf_product_details.get_product_details(_ProductCode, Me.global_ws.Language)

        ' Hier wordt de productcode beschikbaar gemaakt
        Me.lt_prod_code.Text = _ProductCode

        If dt_product_details.Rows.Count > 0 Then
            Me.lt_prod_weight.Text = Format(dt_product_details.Rows(0).Item("prod_weight"), "N3")

            ' Productcode verbergen wanneer dit aangegeven is in de webshop instellingen
            If Me.global_ws.get_webshop_setting("prod_code_hide") = True Then
                Me.ph_product_code.Visible = False
            End If
        End If

        Me.lbl_ff_desc1.Text = dt_product_details.Rows(0).Item("ff_desc1")
        Me.lbl_ff_desc2.Text = dt_product_details.Rows(0).Item("ff_desc2")
        Me.lbl_ff_desc3.Text = dt_product_details.Rows(0).Item("ff_desc3")
        Me.lbl_ff_desc4.Text = dt_product_details.Rows(0).Item("ff_desc4")
        Me.lbl_ff_desc5.Text = dt_product_details.Rows(0).Item("ff_desc5")
        Me.lbl_ff_desc6.Text = dt_product_details.Rows(0).Item("ff_desc6")

        Me.lbl_ff_desc7.Text = dt_product_details.Rows(0).Item("ff_desc7")
        Me.lbl_ff_desc8.Text = dt_product_details.Rows(0).Item("ff_desc8")
        Me.lbl_ff_desc9.Text = dt_product_details.Rows(0).Item("ff_desc9")
        Me.lbl_ff_desc10.Text = dt_product_details.Rows(0).Item("ff_desc10")


        Me.lbl_fl_desc1.Text = Utilize.Data.DataProcedures.GetValue("uws_free_list1", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code1"))
        Me.lbl_fl_desc2.Text = Utilize.Data.DataProcedures.GetValue("uws_free_list2", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code2"))
        Me.lbl_fl_desc3.Text = Utilize.Data.DataProcedures.GetValue("uws_free_list3", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code3"))
        Me.lbl_fl_desc4.Text = Utilize.Data.DataProcedures.GetValue("uws_free_list4", "fl_desc_" + Me.global_ws.Language, dt_product_details.Rows(0).Item("fl_code4"))

        Me.ph_free_text_1.Visible = Me.global_ws.get_webshop_setting("ff1_avail")
        Me.ph_free_text_2.Visible = Me.global_ws.get_webshop_setting("ff2_avail")
        Me.ph_free_text_3.Visible = Me.global_ws.get_webshop_setting("ff3_avail")
        Me.ph_free_text_4.Visible = Me.global_ws.get_webshop_setting("ff4_avail")
        Me.ph_free_text_5.Visible = Me.global_ws.get_webshop_setting("ff5_avail")
        Me.ph_free_text_6.Visible = Me.global_ws.get_webshop_setting("ff6_avail")

        Me.ph_free_text_7.Visible = Me.global_ws.get_webshop_setting("ff7_avail")
        Me.ph_free_text_8.Visible = Me.global_ws.get_webshop_setting("ff8_avail")
        Me.ph_free_text_9.Visible = Me.global_ws.get_webshop_setting("ff9_avail")
        Me.ph_free_text_10.Visible = Me.global_ws.get_webshop_setting("ff10_avail")


        Me.ph_free_list_1.Visible = Me.global_ws.get_webshop_setting("fl1_avail")
        Me.ph_free_list_2.Visible = Me.global_ws.get_webshop_setting("fl2_avail")
        Me.ph_free_list_3.Visible = Me.global_ws.get_webshop_setting("fl3_avail")
        Me.ph_free_list_4.Visible = Me.global_ws.get_webshop_setting("fl4_avail")

        ' Free text
        If Me.lbl_ff_desc1.Text = "" Then
            Me.label_free_text1.CssClass = "text empty"
            Me.label_free_text1.Parent.Visible = False
        End If

        If Me.lbl_ff_desc2.Text = "" Then
            Me.label_free_text2.CssClass = "text empty"
            Me.label_free_text2.Parent.Visible = False
        End If

        If Me.lbl_ff_desc3.Text = "" Then
            Me.label_free_text3.CssClass = "text empty"
            Me.label_free_text3.Parent.Visible = False
        End If

        If Me.lbl_ff_desc4.Text = "" Then
            Me.label_free_text4.CssClass = "text empty"
            Me.label_free_text4.Parent.Visible = False
        End If

        If Me.lbl_ff_desc5.Text = "" Then
            Me.label_free_text5.CssClass = "text empty"
            Me.label_free_text5.Parent.Visible = False
        End If

        If Me.lbl_ff_desc6.Text = "" Then
            Me.label_free_text6.CssClass = "text empty"
            Me.label_free_text6.Parent.Visible = False
        End If

        If Me.lbl_ff_desc7.Text = "" Then
            Me.label_free_text7.CssClass = "text empty"
            Me.label_free_text7.Parent.Visible = False
        End If

        If Me.lbl_ff_desc8.Text = "" Then
            Me.label_free_text8.CssClass = "text empty"
            Me.label_free_text8.Parent.Visible = False
        End If

        If Me.lbl_ff_desc9.Text = "" Then
            Me.label_free_text9.CssClass = "text empty"
            Me.label_free_text9.Parent.Visible = False
        End If

        If Me.lbl_ff_desc10.Text = "" Then
            Me.label_free_text10.CssClass = "text empty"
            Me.label_free_text10.Parent.Visible = False
        End If

        ' free list
        If Me.lbl_fl_desc1.Text = "" Then
            Me.label_free_list1.CssClass = "text empty"
            Me.label_free_list1.Parent.Visible = False
        End If

        If Me.lbl_fl_desc2.Text = "" Then
            Me.label_free_list2.CssClass = "text empty"
            Me.label_free_list2.Parent.Visible = False
        End If

        If Me.lbl_fl_desc3.Text = "" Then
            Me.label_free_list3.CssClass = "text empty"
            Me.label_free_list3.Parent.Visible = False
        End If

        If Me.lbl_fl_desc4.Text = "" Then
            Me.label_free_list4.CssClass = "text empty"
            Me.label_free_list4.Parent.Visible = False
        End If

        If Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_TRUSTPILOT") = True Then
            pnl_stars.Visible = True
            ' Get trustpilot rating number.
            Dim rating As Integer = 0
            Try
                rating = Math.Round(Utilize.Web.Solutions.OAuth.Trustpilot.TrustpilotFunctions.GetStars(product_code))
            Catch ex As Exception
                pnl_stars.Visible = False
            End Try

            If rating > 0 Then
                ' Set the correct images.
                Dim stars As image() = {img_star_1, img_star_2, img_star_3, img_star_4, img_star_5}
                For Each star In stars
                    If (Array.IndexOf(stars, star) + 1) <= rating Then
                        star.ImageUrl = ResolveUrl("~\Documents\Images\Stars\sprite_star.png")
                    Else
                        star.ImageUrl = ResolveUrl("~\Documents\Images\Stars\sprite_star_empty.png")
                    End If
                Next
            Else
                pnl_stars.Visible = False
            End If

            ' If trustpilot API credentials are not set, do not display the trustpilot blocks.
            Try
                Utilize.Web.Solutions.OAuth.Trustpilot.Authentication.trustpilotOAuth.Authenticate()
            Catch ex As Exception
                pnl_stars.Visible = False
            End Try
        End If

        ' Advanded product filters aka product configurator
        If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") = True Then
            Dim ll_product_is_main_of_set As Boolean = Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_part_of_set(Me._ProductCode)

            ' Don't show the product information if the product is a main product in a set.
            If Request.Url.ToString.ToLower.Contains("product_list.aspx") Then
                Me.Visible = Not ll_product_is_main_of_set
            End If

            ' Show the price but not the rest of the information for a main product in a set.
            If Request.Url.ToString.ToLower.Contains("product_details.aspx") Then
                If ll_product_is_main_of_set = True Then
                    Me.pnl_stars.Visible = False
                    Me.ph_product_price.Visible = False
                    Me.ph_product_code.Visible = False
                    Me.ph_product_weight.Visible = False
                    Me.ph_free_texts.Visible = False
                    Me.ph_free_lists.Visible = False
                End If
            End If
        End If

        ' If product list version 2 is used and product information
        ' is loaded in a product row element apply the product block
        ' field visibility preferences
        Dim lc_control As Control = Me.Parent.FindControl("div_uc_placeholder")
        If Not lc_control Is Nothing And global_ws.get_webshop_setting("use_prod_list_v2") Then
            If global_ws.get_webshop_setting("hide_prod_code_v2") Then
                Me.ph_product_code.Visible = False
            End If

            If global_ws.get_webshop_setting("hide_weight_list_v2") Then
                Me.ph_product_weight.Visible = False
            End If

            If global_ws.get_webshop_setting("hide_free_text_v2") Then
                Me.ph_free_texts.Visible = False
            End If

            If global_ws.get_webshop_setting("hide_free_list_v2") Then
                Me.ph_free_lists.Visible = False
            End If
        End If

        If Request.Url.ToString.ToLower.Contains("product_details.aspx") Then
            If global_ws.get_webshop_setting("use_prod_detail_v2") And global_ws.get_webshop_setting("hide_weight_v2") Then
                Me.ph_product_weight.Visible = False
            End If
        End If
    End Sub

End Class
