﻿Partial Class uc_order_product
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

#Region " Properties"
#Region " Eenheid properties"
    Private _unit_type As Utilize.Data.API.Webshop.product_manager.SaleUnit = Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal ' Eenheid voor het product
    Private _sales_quantity As Decimal = 0 'Standaard verkoop eenheid
    Private _unit_code As String = ""

    Public Property unit_code As String
        Get
            If Not _unit_code = "" Then
                Return _unit_code
            Else
                Return Utilize.Data.API.Webshop.product_manager.get_product_unit_default(_product_code)
            End If
        End Get

        Set(value As String)
            _unit_code = value
        End Set
    End Property

    Private _sales_unit As Boolean = False
    Public Property sales_unit As Boolean
        Get
            _sales_unit = _unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
            Return _sales_unit
        End Get
        Set(value As Boolean)
            _sales_unit = value
        End Set
    End Property
#End Region

    Private _group_code As String = ""
    Private _product_code As String = ""
    Private _size As String = ""
    Private _project_location As String = ""
    Private _enabled As Boolean = True
    Private _product_set_code As String = ""
    Private _product_set_product_codes As New List(Of String)

    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            ' Verwijder alle bestaande product codes
            _product_set_product_codes.Clear()
            ' Voeg het hoofdproduct toe aan de product codes voor de set
            product_set_product_codes = value

            ' Here we get the sales quantity and if it is greater than 0, another control is shown
            _group_code = Utilize.Data.DataProcedures.GetValue("uws_products", "shp_grp", _product_code)
            _sales_quantity = Utilize.Data.API.Webshop.product_manager.get_product_unit_quantity(product_code, Me.unit_code)
            _sales_quantity = _sales_quantity.ToString()

            Me.lbl_sale_unit.Text = _sales_quantity.ToString().TrimEnd("0")
            If Me.lbl_sale_unit.Text.EndsWith(",") Then Me.lbl_sale_unit.Text = Me.lbl_sale_unit.Text.Substring(0, Me.lbl_sale_unit.Text.Length - 1)

            Me.lbl_quantity_sale_unit.Text = _sales_quantity.ToString()

            ' De verkoop eenheden worden alleen toegepast wanneer we in de order entry of snel bestellen zitten
            Select Case True
                Case Request.Url.ToString().ToLower().Contains("orderentry.aspx")
                    _unit_type = Utilize.Data.API.Webshop.product_manager.get_product_unit_type(product_code)
                Case Request.Url.ToString().ToLower().Contains("product_details.aspx")
                    _unit_type = Utilize.Data.API.Webshop.product_manager.get_product_unit_type(product_code)
                Case Else
                    ' We zitten op een andere pagina, dus alleen verkoop eenheid of normaal tonen.
                    If _sales_quantity <> 1 Then
                        _unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Else
                        _unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    End If
            End Select
        End Set
    End Property

    Public Property size As String
        Get
            Return _size
        End Get
        Set(ByVal value As String)
            _size = value
        End Set
    End Property

    Public Property project_location As String
        Get
            Return _project_location
        End Get
        Set(value As String)
            _project_location = value
        End Set
    End Property

    Public Property Enabled As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal value As Boolean)
            _enabled = value
        End Set
    End Property

    Public WriteOnly Property textboxvalue As Decimal
        Set(ByVal value As Decimal)
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Me.txt_quantity_sale_unit.Text = value.ToString()

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "UpdateQuantity", "update_quantity('" + Me.txt_quantity_sale_unit.ClientID + "', '" + Me.lbl_sale_unit.ClientID + "', '" + Me.lbl_quantity_sale_unit.ClientID + "'); ", True)
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    For Each rptItem In Me.rpt_units.Items
                        CType(rptItem.FindControl("txt_quantity_unit"), textbox).Text = "0"
                    Next
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Me.txt_quantity_normal.Text = value
            End Select
        End Set
    End Property

    Public Property Value As Decimal
        Get
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Try
                        Return CDec(Me.txt_quantity_sale_unit.Text) * CDec(Me.lbl_sale_unit.Text)
                    Catch ex As Exception
                        Return 0
                    End Try
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    Try
                        Return CDec(CType(Me.rpt_units.Items(0).FindControl("txt_quantity_unit"), textbox).Text)
                    Catch ex As Exception
                        Return 0
                    End Try
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Try
                        Return CDec(Me.txt_quantity_normal.Text)
                    Catch ex As Exception
                        Return 0
                    End Try
                Case Else
                    Exit Select
            End Select

            Return 0
        End Get
        Set(ByVal value As Decimal)
            Me.lt_quantity_normal.Text = value.ToString()
            Me.txt_quantity_normal.Text = value.ToString()
            Me.lbl_quantity_normal.Text = value.ToString()

            If _sales_quantity > 0 Then
                Me.lt_quantity_sale_unit.Text = String.Format("{0:G29}", value / _sales_quantity)
                Me.txt_quantity_sale_unit.Text = String.Format("{0:G29}", value / _sales_quantity)
                Me.lbl_quantity_sale_unit.Text = String.Format("{0:G29}", value)
            End If
        End Set
    End Property

    Public Property AutoPostBack As Boolean
        Get
            Return Me.txt_quantity_sale_unit.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            Me.txt_quantity_sale_unit.AutoPostBack = value
            Me.txt_quantity_normal.AutoPostBack = value
        End Set
    End Property

    Public WriteOnly Property product_set_code As String
        Set(value As String)
            _product_set_code = value
        End Set
    End Property

    Public WriteOnly Property product_set_product_codes As String
        Set(value As String)
            If Not value = "NOTHING_SELECTED" And Not _product_set_product_codes.Contains(value) Then
                _product_set_product_codes.Add(value)
            End If
        End Set
    End Property

    Public WriteOnly Property remove_product_set_product_code As String
        Set(value As String)
            _product_set_product_codes.Remove(value)
        End Set
    End Property
#End Region

#Region " Page subs"
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ' Zet de stylesheet
        Me.set_style_sheet("uc_order_product.css", Me)
        Me.set_javascript("uc_order_product.js", Me)

        ' Controleer of de module product eenheden aan staat
        If Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_PROD_UNITS") And Not Me.IsPostBack() Then
            Dim dt_product_units As System.Data.DataTable = Utilize.Data.API.Webshop.product_manager.get_product_units(Me.product_code)

            Me.rpt_units.DataSource = dt_product_units
            Me.rpt_units.DataBind()
        End If

        Me.button_order.Text = global_trans.translate_button("button_order", "Bestellen", Me.global_ws.Language)
        Me.button_order1.Text = global_trans.translate_button("button_order", "Bestellen", Me.global_ws.Language)
        Me.button_order2.Text = global_trans.translate_button("button_order", "Bestellen", Me.global_ws.Language)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Controleer of de module product eenheden aan staat
        If Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_PROD_UNITS") Then
            Dim dv_product_units As System.Data.DataView = Utilize.Data.API.Webshop.product_manager.get_product_units(Me.product_code).DefaultView

            Me.rpt_units.DataSource = dv_product_units
            Me.rpt_units.DataBind()
        End If

        Me.block_css = False

        Dim ln_decimal_numbers As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals(_product_code)
        Dim ln_decimal_numbers_saleunit As Integer = Utilize.Data.API.Webshop.product_manager.get_product_decimals_saleunit(_product_code)
        ' UWS-370 "Fixed that shopping cart and single checkout are both valid to have autopostback"
        If (Request.Url.ToString.ToLower().Contains("shopping_cart.aspx") Or Request.Url.ToString.ToLower().Contains("single_checkout.aspx")) And Me.AutoPostBack Then
            'END UWS-370
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Me.img_min_normal.OnClientClick = "decrement('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_normal.ClientID + """); return false;"
                    Me.img_plus_normal.OnClientClick = "increment('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_normal.ClientID + """); return false;"

                    Me.img_min_normal.OnClientClick = "decrement('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_normal.ClientID + """); return false;"
                    Me.img_plus_normal.OnClientClick = "increment('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_normal.ClientID + """); return false;"
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Me.img_min.OnClientClick = "decrement('" + Me.txt_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_sale_unit.ClientID + """); return false;"
                    Me.img_plus.OnClientClick = "increment('" + Me.txt_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers.ToString() + "); __doPostBack(""" + Me.txt_quantity_sale_unit.ClientID + """); return false;"
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    Exit Select
                Case Else
                    Exit Select
            End Select
        Else
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Me.img_min_normal.OnClientClick = "decrement('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); return false;"
                    Me.img_plus_normal.OnClientClick = "increment('" + Me.txt_quantity_normal.ClientID + "', " + ln_decimal_numbers.ToString() + "); return false;"
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Me.img_min.OnClientClick = "decrement('" + Me.txt_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers.ToString() + ");update_quantity('" + Me.txt_quantity_sale_unit.ClientID + "', '" + Me.lbl_sale_unit.ClientID + "', '" + Me.lbl_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers_saleunit.ToString() + "); return false;"
                    Me.img_plus.OnClientClick = "increment('" + Me.txt_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers.ToString() + ");update_quantity('" + Me.txt_quantity_sale_unit.ClientID + "', '" + Me.lbl_sale_unit.ClientID + "', '" + Me.lbl_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers_saleunit.ToString() + "); return false;"
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    ' Create the javascript functionality for the buttons
                    For I As Integer = 0 To Me.rpt_units.Items.Count - 1
                        Dim txt_quantity As textbox = Me.rpt_units.Items(I).FindControl("txt_quantity_unit")
                        Dim dv_product_units As System.Data.DataView = Utilize.Data.API.Webshop.product_manager.get_product_units(Me.product_code).DefaultView
                        Dim lc_sale_unit As String = dv_product_units.Item(I)("unit_code")

                        If Me.global_ws.get_webshop_setting("lazy_load_products") Then
                            txt_quantity.Attributes.Add("data-prod-code", product_code)
                            txt_quantity.Attributes.Add("data-unit-code", lc_sale_unit)

                            'Dim img_min_unit As Utilize.Web.UI.WebControls.button = Me.rpt_units.Items(I).FindControl("img_min_unit")
                            'Dim img_plus_unit As Utilize.Web.UI.WebControls.button = Me.rpt_units.Items(I).FindControl("img_plus_unit")

                        End If

                        CType(Me.rpt_units.Items(I).FindControl("img_min_unit"), button).OnClientClick = "decrementzero('" + txt_quantity.ClientID + "', " + ln_decimal_numbers.ToString() + "); return false;"
                        CType(Me.rpt_units.Items(I).FindControl("img_plus_unit"), button).OnClientClick = "incrementzero('" + txt_quantity.ClientID + "', " + ln_decimal_numbers.ToString() + "); return false;"
                    Next
                Case Else
                    Exit Select
            End Select
        End If

        If Me.global_ws.get_webshop_setting("lazy_load_products") Or Me.global_ws.get_webshop_setting("use_async_order") Then
            Me.txt_quantity_normal.Attributes.Add("data-prod-code", product_code)
            Me.txt_quantity_sale_unit.Attributes.Add("data-prod-code", product_code)
        End If

        If Me.Visible Then
            ' Dit onderdeel is alleen zichtbaar als er een productcode beschikbaar is
            Me.Visible = Not Me._product_code = ""
        End If

        If Me.Visible And Me.global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            ' Alleen zichtbaar als de gebruiker hier rechten toe heeft
            Me.Visible = Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") = True
        End If

        If Me.Visible And Not Me.global_ws.user_information.user_logged_on Then
            ' De bestelknop is zichtbaar wanneer je niet bent ingelogd en de instelling bestellen wanneer niet ingelogd aan staat.
            Me.Visible = Me.global_ws.get_webshop_setting("order_not_logged_on")
        End If

        If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_row_ref") And Request.Url.ToString.ToLower.Contains("product_details.aspx") Then
            Me.ph_row_reference.Visible = True
            Me.txt_product_comment.Attributes.Add("placeholder", global_trans.translate_label("label_product_comment", "Opmerkingen", Me.global_ws.Language))
        End If

        If Me.Visible Then
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Me.ph_quantity_sale_unit.Visible = False
                    Me.ph_quantity_normal.Visible = True
                    Me.ph_quantity_product_unit.Visible = False
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Me.ph_quantity_sale_unit.Visible = True
                    Me.ph_quantity_normal.Visible = False
                    Me.ph_quantity_product_unit.Visible = False
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    Me.ph_quantity_sale_unit.Visible = False
                    Me.ph_quantity_normal.Visible = False
                    Me.ph_quantity_product_unit.Visible = True
                Case Else
                    Exit Select
            End Select
        End If

        If Me.Visible And Me._unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit Then
            Me.txt_quantity_normal.Style.Add("display", "none")
            Me.txt_quantity_sale_unit.Attributes.Add("onchange", "update_quantity('" + Me.txt_quantity_sale_unit.ClientID + "', '" + Me.lbl_sale_unit.ClientID + "', '" + Me.lbl_quantity_sale_unit.ClientID + "', " + ln_decimal_numbers.ToString() + ");")
        Else
            Me.txt_quantity_normal.Style.Remove("display")
        End If

        If Me.Visible Then
            If Me.button_order.Visible Then
                Me.button_order.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", product_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", product_code, "mainitemmatrix") = product_code)
            End If

            If Me.button_order1.Visible Then
                Me.button_order1.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", product_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", product_code, "mainitemmatrix") = product_code)
            End If

            If Me.button_order2.Visible Then
                Me.button_order2.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", product_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", product_code, "mainitemmatrix") = product_code)
            End If
        End If

        If Not _enabled Then
            Select Case _unit_type
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                    Me.ph_quantity_normal.Visible = True
                    Me.ph_quantity_sale_unit.Visible = False
                    Me.ph_quantity_product_unit.Visible = False
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                    Me.ph_quantity_normal.Visible = False
                    Me.ph_quantity_sale_unit.Visible = True
                    Me.ph_quantity_product_unit.Visible = False
                Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                    Me.ph_quantity_normal.Visible = False
                    Me.ph_quantity_sale_unit.Visible = True
                    Me.ph_quantity_product_unit.Visible = False
                Case Else
                    Exit Select
            End Select

            Me.txt_quantity_normal.Visible = False
            Me.txt_quantity_sale_unit.Visible = False
            Me.img_min.Visible = False
            Me.img_plus.Visible = False
            Me.img_min_normal.Visible = False
            Me.img_plus_normal.Visible = False

            Me.lt_quantity_normal.Visible = True
            Me.lt_quantity_sale_unit.Visible = True

            Me.lt_quantity_normal.Text = Me.txt_quantity_normal.Text
            Me.lt_quantity_sale_unit.Text = Me.txt_quantity_sale_unit.Text

            Me.button_order.Visible = False
            Me.button_order1.Visible = False
            Me.button_order2.Visible = False
        End If

        If Me.Visible And Me.IsPostBack And Me._unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit Then
            Me.lbl_quantity_sale_unit.Text = Me.txt_quantity_sale_unit.Text * Me._sales_quantity
        End If

        If Me.Visible And (Request.Url.ToString.ToLower.Contains("product_list.aspx") Or Request.Url.ToString.ToLower().Contains("product_search.aspx")) Then
            ' Als de sizeview module aan staat, dan moet de control onzichtbaar zijn
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview") = True Then
                Me.Visible = False
            End If
        End If

        If Me.Visible And Me.global_ws.get_webshop_setting("show_quantity") = True Then
            Me.set_style_sheet("uc_order_product_quantity.css", Me)
        End If

        ' When we are in the product list or product search page and the group code is filled
        ' Then check wether the products have to be grouped based on webshop settings in the overview
        ' If so, then hide the quantity boxes and set the order functionality to redirect to the product_details
        If Me.Visible _
                    And (Request.Url.ToString.ToLower.Contains("product_list.aspx") _
                    Or Request.Url.ToString.ToLower().Contains("product_search.aspx") Or Request.Url.ToString.ToLower().Contains("productlistwebmethods.aspx")) _
                    And Not Me.global_ws.get_webshop_setting("group_products") = 0 _
                    Then

            Me.ph_quantity.Visible = False
            Me.ph_quantity1.Visible = False

            Me.button_order.OnClientClick = "document.location='" + Me.ResolveClientUrl("/") + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", Me._product_code + Me.global_ws.Language, "prod_code + lng_code").substring(1) + "'; return false;"
            Me.button_order1.OnClientClick = "document.location='" + Me.ResolveClientUrl("/") + Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", Me._product_code + Me.global_ws.Language, "prod_code + lng_code").substring(1) + "'; return false;"
        End If


        ' Advanded product filters aka product configurator
        If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") = True Then
            Dim ll_products_is_main_part_of_set As Boolean = Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_part_of_set(Me.product_code)

            If Request.Url.ToString.ToLower.Contains("product_list.aspx") Then
                ' Don't show this control if the product is a main product in a product set.
                Me.ph_order.Visible = Not ll_products_is_main_part_of_set
            End If

            If Me.Visible And Request.Url.ToString.ToLower.Contains("product_details.aspx") And Me.ph_row_reference.Visible Then
                ' Don't show this control if the product is a main product in a product set.
                Me.ph_row_reference.Visible = Not ll_products_is_main_part_of_set
            End If

            If Me.Visible And Request.Url.ToString.ToLower().Contains("shopping_cart.aspx") Then
                ' Show the control if its a main product in a set.
                If Not ll_products_is_main_part_of_set Then
                    ' Don't show this control if the product is a product in a product set.
                    Me.ph_order.Visible = Not Utilize.Web.Solutions.Webshop.ws_procedures.check_if_product_is_part_of_set(Me.product_code, False)
                End If
            End If
        End If

        If Me.Visible Then
            ' Parse het aantal op n decimalen
            Me.txt_quantity_normal.Text = Decimal.Parse(txt_quantity_normal.Text).ToString("F" + ln_decimal_numbers.ToString())
            Me.txt_quantity_sale_unit.Text = Decimal.Parse(txt_quantity_sale_unit.Text).ToString("F" + ln_decimal_numbers.ToString())
            Me.lbl_quantity_sale_unit.Text = Decimal.Parse(lbl_quantity_sale_unit.Text).ToString("F" + ln_decimal_numbers_saleunit.ToString())

            If ln_decimal_numbers > 0 Then
                ' Om dezelfde grote te houden van het input veld, moeten het lettertype wat kleiner
                Dim ln_font_size As Integer = 11 - Math.Floor(ln_decimal_numbers / 2)

                Me.txt_quantity_normal.Attributes.Add("style", "font-size: " + ln_font_size.ToString() + "pt !important")
                Me.txt_quantity_sale_unit.Attributes.Add("style", "font-size: " + ln_font_size.ToString() + "pt !important")

                ' MaxLength is standaard 4, voeg voor elk decimaal en de komma 1 extra teken toe
                Me.txt_quantity_normal.MaxLength = 4 + (ln_decimal_numbers + 1)
                Me.txt_quantity_sale_unit.MaxLength = 4 + (ln_decimal_numbers + 1)
            End If
        End If

        ' change check to setting product group 1
        Dim ll_main_item_matrix = (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", _product_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", _product_code, "mainitemmatrix") = _product_code)
        If Me.Visible And Me.global_ws.get_webshop_setting("use_async_order") And Me.global_ws.get_webshop_setting("group_products") = Not 1 Then
            button_order1.OnClientClick = "return false"
            button_order.OnClientClick = "return false"
        End If
    End Sub
#End Region

#Region " Order functions"
    Public Function order_products() As Boolean
        Dim ll_resume As Boolean = True

        ' Normaal
        If _product_set_code = "" Then
            If ll_resume Then
                Select Case _unit_type
                    Case Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal
                        ll_resume = Me.order_default_products()
                    Case Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit
                        ll_resume = Me.order_default_products()
                    Case Utilize.Data.API.Webshop.product_manager.SaleUnit.ProductUnit
                        ll_resume = Me.order_unit_products()
                    Case Else
                        Exit Select
                End Select
            End If
        End If

        ' Als het product deel van een reeks is dan moeten meerdere producten toegevoegd worden.
        If Not _product_set_code = "" Then
            ll_resume = Me.order_product_set_products()
        End If

        Return ll_resume
    End Function

    Private Function order_product_set_products() As Boolean
        Dim ll_resume As Boolean = True

        ' Voeg de set met producten toe aan de winkelwagen
        If ll_resume Then
            ll_resume = Me.global_ws.shopping_cart.product_add_set_products(_product_set_product_codes, _product_set_code, Me.global_ws.Language, Me.global_ws.purchase_combination.user_id)

            If Not ll_resume Then
                ' Error melding tonen
                If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                End If
            End If
        End If

        Return ll_resume
    End Function

    Private Function order_default_products() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume And Me._unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.SaleUnit Then
            Try
                Dim ln_total As Integer = CInt(Me.txt_quantity_sale_unit.Text) * Decimal.Parse(Me.lbl_sale_unit.Text)

                ' Het gaat om verkoopeenheden dus we moeten een andere waarde gebruiken
                ll_resume = Me.global_ws.shopping_cart.product_add(_product_code, Me.txt_quantity_sale_unit.Text, _size, Me.unit_code, Me.txt_product_comment.Text, "", Me.global_ws.purchase_combination.user_id)
            Catch ex As Exception
                ' De aantallen kloppen niet, geef een foutmelding
                ' Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                ll_resume = False
            End Try


            If Not ll_resume Then
                ' Error melding tonen
                If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                End If
            End If
        End If

        If ll_resume And Me._unit_type = Utilize.Data.API.Webshop.product_manager.SaleUnit.Normal Then
            ' Het gaat niet om verkoopeenheden dus we gebruiken de standaard waarden
            ll_resume = Me.global_ws.shopping_cart.product_add(_product_code, Me.txt_quantity_normal.Text, _size, Me.unit_code, Me.txt_product_comment.Text, "", Me.global_ws.purchase_combination.user_id, lc_correlation_id:=Guid.NewGuid.ToString)

            If Not ll_resume Then
                ' Error melding tonen
                If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                End If
            End If
        End If

        If ll_resume And Not Me.project_location = "" Then
            Me.global_ws.shopping_cart.ubo_shopping_cart.field_set("uws_order_lines.project_location", Me._project_location)
        End If

        Return ll_resume
    End Function

    Private Function order_unit_products() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            For Each rpt_repeater_item As System.Web.UI.WebControls.RepeaterItem In rpt_units.Items
                Dim lc_unit_code As String = CType(rpt_repeater_item.FindControl("lt_unit_code"), literal).Text
                Dim lc_prod_code As String = Me.product_code

                Dim ln_unit_quantity As String = CType(rpt_repeater_item.FindControl("txt_quantity_unit"), textbox).Text
                Dim ln_sales_quantity As Integer = Utilize.Data.API.Webshop.product_manager.get_product_unit_quantity(lc_prod_code, lc_unit_code)

                If ll_resume And Not ln_unit_quantity = "0" And Not ln_unit_quantity = "-" Then
                    ll_resume = Me.global_ws.shopping_cart.product_add(_product_code, ln_unit_quantity, _size, lc_unit_code, Me.txt_product_comment.Text, "", Me.global_ws.purchase_combination.user_id)

                    If Not ll_resume Then
                        ' Error melding tonen
                        If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                        End If
                    End If
                End If

                If Not ll_resume Then
                    Exit For
                End If
            Next
        End If

        Return ll_resume
    End Function
#End Region

#Region "Button handlers"
    Protected Sub button_order_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_order.Click, button_order1.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Me.order_products()
        End If

        If ll_resume Then
            ' Als er in de webshop instellingen is aangegegeven dat je na het bestellen direct naar de winkelwagen gaat, dan omleiden.
            If Me.global_ws.get_webshop_setting("order_redirect") = True Then
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "$('#order_message').fadeIn().delay(1000).fadeOut();", True)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Button voor het bestellen van eenheden
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub button_order2_Click(sender As Object, e As EventArgs) Handles button_order2.Click
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Me.order_unit_products()
        End If

        If ll_resume Then
            ' Als er in de webshop instellingen is aangegegeven dat je na het bestellen direct naar de winkelwagen gaat, dan omleiden.
            If Me.global_ws.get_webshop_setting("order_redirect") = True Then
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "$('#order_message').fadeIn().delay(1000).fadeOut();", True)
            End If
        End If
    End Sub
#End Region

End Class
