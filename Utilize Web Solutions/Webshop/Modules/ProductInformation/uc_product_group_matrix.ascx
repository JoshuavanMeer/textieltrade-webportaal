﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_group_matrix.ascx.vb" Inherits="uc_product_group_matrix" %>

<div class="uc_product_group_matrix col-md-12" data-order-target="order-prod-control">
    <div class="row">
        <utilize:placeholder runat="server" ID="ph_group_products" Visible="false">
            <utilize:literal runat="server" ID="lt_group_desc"></utilize:literal>
            <utilize:repeater runat="server" ID="rpt_group_values">
                <ItemTemplate>
                    <utilize:literal runat="server" ID="lt_prod_code" Text='<%# DataBinder.Eval(Container.DataItem, "prod_code")%>' Visible="false"></utilize:literal>
                    <utilize:literal runat="server" ID="lt_sale_unit" Text='<%# DataBinder.Eval(Container.DataItem, "sale_unit")%>' Visible="false"></utilize:literal>
                        <div class="product_group row margin-bottom-5 table-bordered"> 
                            <div class="prod_code col-md-2 col-xs-4"><%# DataBinder.Eval(Container.DataItem, "prod_code")%></div>
                            <div class="prod_desc col-md-6 col-xs-4">
                                <span class="prod-desc">
                                    <%# DataBinder.Eval(Container.DataItem, "prod_desc")%>
                                </span><br />
                                <span class="grp-val">
                                    <%# DataBinder.Eval(Container.DataItem, "grp_val")%>
                                </span>
                            </div>
                            <div class="prod_price col-md-2 col-xs-6"><utilize:literal runat="server" ID="lt_prod_price"></utilize:literal></div>
                            <div class="prod_quantity col-md-3 col-xs-6">
                                <utilize:placeholder runat="server" ID="ph_order">
                                    <div class="product-quantity pull-right">
                                        <utilize:button runat="server" CssClass="increment quantity-button" ID="img_min_normal" Text="-" UseSubmitBehavior="false"></utilize:button>
                                        <utilize:textbox runat="server" ID="txt_order_quantity" MaxLength="4" Text="0" CssClass="quantity quantity-field" data-order-target="order-prod-quantity"></utilize:textbox>
                                        <utilize:button runat="server" CssClass="increment quantity-button" ID="img_plus_normal" Text="+" UseSubmitBehavior="false"></utilize:button>
                                    </div>
                                </utilize:placeholder>
                            </div>
                        </div>
                </ItemTemplate>
            </utilize:repeater>
        </utilize:placeholder>

        <utilize:panel runat="server" cssclass="table-responsive matrix" ID="pnl_group_product_matrix" Visible="false">

        </utilize:panel>
        <utilize:translatebutton runat="server" cssclass="add_to_basket btn-u btn-u-sea-shop btn-u-lg pull-right" ID="button_order" Text="Bestellen" CommandName="order" UseSubmitBehavior="true" data-order-target="order-prod-button"/>
    </div>
</div>