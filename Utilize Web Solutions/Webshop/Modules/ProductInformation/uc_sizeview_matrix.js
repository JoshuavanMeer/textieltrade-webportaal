﻿
function increment(fldId) {
    fld = document.getElementById(fldId);
    if (fld.className.indexOf("aspNetDisabled") == -1) {
        if (fld.value == '-') {
            fld.value = 1;
        }
        else {
            fld.value = parseFloat(fld.value) + 1;
        }

        fld.onchange()
    }
}

function decrement(fldId) {
    fld = document.getElementById(fldId);

    if (fld.className.indexOf("aspNetDisabled") == -1) {
        if (fld.value == '-' || fld.value == '1') {
            fld.value = "-";
        }
        else {
            fld.value = parseFloat(fld.value) - 1;
        }
        fld.onchange()
    }
}


var incrementCount = {
    onChange: function () {
        var counter = 0;
        for (var a = 0; a < this.row.inputs.length; a++) {
            if (isNaN(parseInt(this.row.inputs[a].value, 10))) {
                this.row.inputs[a].value = '-';
            } else {
                counter += parseInt(this.row.inputs[a].value, 10);
            }
        }
        this.row.output.innerHTML = counter;
    },
    //deze functie word aangeroepen wanneer de pagina volledig is geladen en opgebouwd
    start: function () {
        var table = $(".sizeview_matrix").get(0);

        if (table) {
            var rows = table.getElementsByTagName('tr');
            for (var a = 1; a < rows.length; a++) {
                rows[a].inputs = rows[a].getElementsByClassName('incrementcount');
                for (var b = 0; b < rows[a].inputs.length; b++) {
                    rows[a].inputs[b].row = rows[a];
                    rows[a].inputs[b].onmouseup =
                    rows[a].inputs[b].onchange = this.onChange;
                }
                rows[a].output = rows[a].getElementsByTagName('span')[0];

                try
                { rows[a].inputs[0].onchange(); }
                catch (err)
                { }
            }
        }
    },
    //initalisatie van de object nadat het document is opgebouwd
    init: function () {
        //locaal definieren maakt dit globaal voor de functies die hardcoded gemaakt worden in de volgende regels.
        var This = this;
        //dit maakt het volgende korte oplossing mogelijk op deze manier kunnen er meerdere instanties aangemaakt worden zonde vrees voor het ontdoen van bestaande of aanvullende scripts scripts.
        if (typeof (window.addEventListener) != 'undefined') {
            window.addEventListener('load', function () { This.start(); }, false);
        } else if (typeof (window.attachEvent) != 'undefined') {
            window.attachEvent('onload', function () { This.start() });
        }
    }
}; incrementCount.init();