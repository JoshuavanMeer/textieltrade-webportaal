﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_volume_discount.ascx.vb" Inherits="uc_volume_discount" %>
<div class="uc_volume_discount">
    <div class="table-responsive">
        <asp:Repeater runat="server" ID="rpt_discount">
            <HeaderTemplate>
                <table class="overview table">
                    <thead>
                        <tr>
                            <th class="quantity"><utilize:translateliteral runat="server" id="label_discount_quantity" Text="Aantal vanaf"></utilize:translateliteral></th>
                            <th class="amount">
                                <utilize:translateliteral runat="server" id="label_discount_price" Text="Prijs per stuk" Visible="false"></utilize:translateliteral>
                                <utilize:translateliteral runat="server" id="label_discount_percentage" Text="Kortingspercentage" Visible="false"></utilize:translateliteral>
                                <utilize:translateliteral runat="server" id="label_discount_amount" Text="Korting" Visible="false"></utilize:translateliteral>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="quantity"><utilize:literal ID="lt_quantity" runat="server" Text='<%#Decimal.Round(DataBinder.Eval(Container.DataItem, "vdv_qty"), 0) %>'></utilize:literal></td>
                    <td class="amount">
                        <utilize:literal ID="lt_value_price" runat="server" Text='<%# Format(DataBinder.Eval(Container.DataItem, "vdv_val"), "c")%>' Visible="false"></utilize:literal>
                        <utilize:literal ID="lt_value_percentage" runat="server" Text='<%# Decimal.Round(DataBinder.Eval(Container.DataItem, "vdv_val"), 2)%>' Visible="false"></utilize:literal><utilize:literal runat="server" ID="lt_percentage_sign" Text="%" Visible="false"></utilize:literal>
                        <utilize:literal ID="lt_value_discount" runat="server" Text='<%# Format(DataBinder.Eval(Container.DataItem, "vdv_val"), "c")%>' Visible="false"></utilize:literal>
                        <utilize:literal ID="lt_value_discount_price" runat="server" Visible="false"></utilize:literal>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>