﻿
Partial Class uc_volume_discount
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    Private dt_staffels As System.Data.DataTable = Nothing

    ' Type staffel - (1: Prijs 2: Percentage 3: Kortingsbedrag)
    Private staffels_type As Integer = 0
    Private staffels_id As String = ""

    ' Type korting - 1: Prijs van/voor 2: Alleen staffelprijs 3: Staffelweergave 4: Staffelweergave - alleen prijzen
    Private discount_type As Integer = 0

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_px_list")
        End If

        If ll_resume Then
            ll_resume = IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
        End If

        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
        If prices_not_visible Then
            ' Verberg prijzen wanneer het vinkje "Prijzen tonen" aan staat
            ll_resume = False
        End If

        If ll_resume Then
            Me.discount_type = Me.global_ws.get_webshop_setting("volume_discount_type")

            ll_resume = Me.discount_type > 2
        End If

        If ll_resume Then
            ll_resume = Not Me.global_ws.check_sales_action(Me.product_code)
        End If

        If ll_resume Then
            Dim lc_cust_id As String = IIf(Me.global_ws.user_information.user_logged_on, Me.global_ws.user_information.ubo_customer.field_get("rec_id"), "")
            Dim lc_pl_code As String = Me.global_ws.user_information.ubo_customer.field_get("pl_code")

            Me.dt_staffels = Me.global_ws.price_manager.get_staffels(_product_code, lc_cust_id, Utilize.Data.DataProcedures.GetValue("uws_products", "px_sell", product_code))

            ll_resume = Not dt_staffels Is Nothing
        End If

        If ll_resume Then
            Me.staffels_id = Me.global_ws.price_manager.staffels_id
            Me.staffels_type = Me.global_ws.price_manager.staffels_type
        End If

        If ll_resume Then
            Me.rpt_discount.DataSource = dt_staffels
            Me.rpt_discount.DataBind()

            ll_resume = Me.rpt_discount.Items.Count > 0

            If ll_resume And Me.rpt_discount.Items.Count = 1 Then
                ll_resume = Not CType(Me.rpt_discount.Items(0).FindControl("lt_quantity"), Utilize.Web.UI.WebControls.literal).Text = "1"
            End If

            ' Ruim altijd de staffels op!!!
            Me.global_ws.price_manager.dispose_staffels()
        End If

        If ll_resume And Me.discount_type = 3 And Me.staffels_type = 2 Then
            For I As Integer = 0 To Me.rpt_discount.Items.Count - 1
                ' Percentage
                CType(Me.rpt_discount.Items(I).FindControl("lt_percentage_sign"), Utilize.Web.UI.WebControls.literal).Visible = True
            Next
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_volume_discount.css", Me)
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub
    Protected Sub rpt_discount_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_discount.ItemCreated
        If Not Me.discount_type = 4 Then
            Select Case e.Item.ItemType
                Case UI.WebControls.ListItemType.Header
                    Select Case staffels_type
                        Case 1
                            ' Prijs
                            CType(e.Item.FindControl("label_discount_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case 2
                            ' Percentage
                            CType(e.Item.FindControl("label_discount_percentage"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case 3
                            ' Korting
                            CType(e.Item.FindControl("label_discount_amount"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case Else
                            Exit Select
                    End Select

                Case UI.WebControls.ListItemType.Footer
                    Exit Select
                Case UI.WebControls.ListItemType.Item
                    Exit Select
                Case UI.WebControls.ListItemType.AlternatingItem
                    Exit Select
                Case UI.WebControls.ListItemType.SelectedItem
                    Exit Select
                Case UI.WebControls.ListItemType.EditItem
                    Exit Select
                Case UI.WebControls.ListItemType.Separator
                    Exit Select
                Case UI.WebControls.ListItemType.Pager
                    Exit Select
                Case Else
                    Exit Select
            End Select

            Select Case e.Item.ItemType
                Case UI.WebControls.ListItemType.AlternatingItem
                    Select Case staffels_type
                        Case 1
                            ' Prijs
                            CType(e.Item.FindControl("lt_value_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case 2
                            ' Percentage
                            CType(e.Item.FindControl("lt_value_percentage"), Utilize.Web.UI.WebControls.literal).Visible = True
                            CType(e.Item.FindControl("lt_value_percentage"), Utilize.Web.UI.WebControls.literal).Text = True
                        Case 3
                            ' Korting
                            CType(e.Item.FindControl("lt_value_discount"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case Else
                            Exit Select
                    End Select

                Case UI.WebControls.ListItemType.Item
                    Select Case staffels_type
                        Case 1
                            ' Prijs
                            CType(e.Item.FindControl("lt_value_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case 2
                            ' Percentage
                            CType(e.Item.FindControl("lt_value_percentage"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case 3
                            ' Korting
                            CType(e.Item.FindControl("lt_value_discount"), Utilize.Web.UI.WebControls.literal).Visible = True
                        Case Else
                            Exit Select
                    End Select

                Case UI.WebControls.ListItemType.Header
                    Exit Select
                Case UI.WebControls.ListItemType.Footer
                    Exit Select
                Case UI.WebControls.ListItemType.SelectedItem
                    Exit Select
                Case UI.WebControls.ListItemType.EditItem
                    Exit Select
                Case UI.WebControls.ListItemType.Separator
                    Exit Select
                Case UI.WebControls.ListItemType.Pager
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End If

        If Me.discount_type = 4 Then
            Select Case e.Item.ItemType
                Case UI.WebControls.ListItemType.Header
                    ' Alleen een prijs
                    CType(e.Item.FindControl("label_discount_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                Case UI.WebControls.ListItemType.Footer
                    Exit Select
                Case UI.WebControls.ListItemType.Item
                    Exit Select
                Case UI.WebControls.ListItemType.AlternatingItem
                    Exit Select
                Case UI.WebControls.ListItemType.SelectedItem
                    Exit Select
                Case UI.WebControls.ListItemType.EditItem
                    Exit Select
                Case UI.WebControls.ListItemType.Separator
                    Exit Select
                Case UI.WebControls.ListItemType.Pager
                    Exit Select
                Case Else
                    Exit Select
            End Select

            If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
                ' Dan alleen prijzen tonen
                Select Case staffels_type
                    Case 1
                        CType(e.Item.FindControl("lt_value_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                    Case 2
                        Dim ln_product_price As Decimal = Me.global_ws.get_product_price_default(Me.product_code)
                        Dim ln_new_price As Decimal = (ln_product_price / 100 * (100 - e.Item.DataItem("vdv_val")))

                        CType(e.Item.FindControl("lt_value_discount_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                        CType(e.Item.FindControl("lt_value_discount_price"), Utilize.Web.UI.WebControls.literal).Text = Format(ln_new_price, "c")
                    Case 3
                        Dim ln_product_price As Decimal = Me.global_ws.get_product_price_default(Me.product_code)
                        Dim ln_new_price As Decimal = ln_product_price - e.Item.DataItem("vdv_val")

                        CType(e.Item.FindControl("lt_value_discount_price"), Utilize.Web.UI.WebControls.literal).Visible = True
                        CType(e.Item.FindControl("lt_value_discount_price"), Utilize.Web.UI.WebControls.literal).Text = Format(ln_new_price, "c")
                    Case Else
                        Exit Select
                End Select
            End If
        End If
    End Sub
End Class
