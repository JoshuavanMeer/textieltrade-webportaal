﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_favorites.ascx.vb" Inherits="uc_product_favorites" %>
<div class="row">
    <div class="uc_product_favorites col-sm-12 col-md-12 margin-top-20">
        <utilize:translatebutton ID="button_add_to_favorites" runat="server" Text="Voeg toe aan mijn favorieten" cssclass="uc_product_favorites_add btn-u btn-u-sea-shop btn-u-lg"/>
        <utilize:translatebutton ID="button_remove_from_favorites" runat="server" Text="Verwijder van mijn favorieten" cssclass="uc_product_favorites_remove btn-u btn-u-sea-shop btn-u-lg"/>
    </div>
</div>
