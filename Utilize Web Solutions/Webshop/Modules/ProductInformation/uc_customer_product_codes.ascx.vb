﻿
Partial Class uc_customer_product_codes
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _read_only As Boolean = False
    Public Property read_only As Boolean
        Get
            Return _read_only
        End Get
        Set(ByVal value As Boolean)
            _read_only = value
        End Set
    End Property

    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    Protected Sub Page_Init1() Handles Me.Init
        Dim ll_resume As Boolean = True

        Me.block_css = False

        If ll_resume Then
            ll_resume = Me.global_ws.user_information.user_logged_on = True
        End If

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_prod")
        End If

        If ll_resume Then
            Me.txt_product_code.Attributes.Add("placeholder", global_trans.translate_label("label_customer_product_code", "Vul hier uw productcode in", Me.global_ws.Language))
            'Me.txt_product_code.watermark = global_trans.translate_label("label_customer_product_code", "Vul hier uw productcode in", Me.global_ws.Language)

            ' Zet de stylesheet
            Me.set_style_sheet("uc_customer_product_codes.css", Me)
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub

    Protected Sub button_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_save.Click
        Dim ubo_customer As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", Me.global_ws.user_information.user_customer_id, "rec_id")

        If ubo_customer.data_tables("uws_customers").data_row_count > 0 Then
            If ubo_customer.table_locate("uws_product_codes", "prod_code = '" + Me._product_code + "'") Then
                ' Sla de gewijzigde code op
                ubo_customer.field_set("uws_product_codes.prod_cust", Me.txt_product_code.Text.ToUpper())

                ubo_customer.table_update(Me.global_ws.user_information.user_id)
            Else
                ' Voeg de nieuwe code toe
                ubo_customer.table_insert_row("uws_product_codes", Me.global_ws.user_information.user_id)
                ubo_customer.field_set("uws_product_codes.prod_code", Me._product_code)
                ubo_customer.field_set("uws_product_codes.prod_cust", Me.txt_product_code.Text.ToUpper())

                ubo_customer.table_update(Me.global_ws.user_information.user_id)
            End If

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "product_code_saved", "alert('" + global_trans.translate_message("message_product_code_saved", "De vastgelegde productcode is bewaard.", Me.global_ws.Language) + "');", True)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ph_customer_text.Visible = Me._read_only
        Me.ph_customer_edit.Visible = Not Me._read_only

        Dim lc_product_code As String = Utilize.Data.DataProcedures.GetValue("uws_product_codes", "prod_cust", Me.global_ws.user_information.user_customer_id + Me._product_code, "cust_id + prod_code")
        Me.txt_product_code.Text = lc_product_code
        Me.lt_product_code.Text = lc_product_code

        If Me._read_only = True Then
            Me.Visible = Not Me.txt_product_code.Text = ""
        End If
    End Sub
End Class
