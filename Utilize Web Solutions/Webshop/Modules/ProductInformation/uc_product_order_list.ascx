﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_order_list.ascx.vb" Inherits="uc_product_order_list" %>
<div class="row">
    <div class="uc_product_order_list col-md-12">
        <utilize:translatebutton ID="button_add_to_order_list" runat="server" Text="Voeg toe aan mijn bestellijst" cssclass="add btn-u btn-u-sea-shop btn-u-lg"/>
        <utilize:translatebutton ID="button_remove_from_order_list" runat="server" Text="Verwijder van mijn bestellijst" cssclass="remove btn-u btn-u-sea-shop btn-u-lg"/>
    </div>
</div>
