﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_information.ascx.vb" Inherits="Webshop_Modules_ProductInformation_uc_product_information" %>

<div class="uc_product_information">
    <utilize:panel runat="server" ID="pnl_stars" CssClass="trustpilot-stars" Visible="false">
        <utilize:image runat="server" ID="img_star_1" />
        <utilize:image runat="server" ID="img_star_2" />
        <utilize:image runat="server" ID="img_star_3" />
        <utilize:image runat="server" ID="img_star_4" />
        <utilize:image runat="server" ID="img_star_5" />
    </utilize:panel>

    <utilize:placeholder runat="server" ID="ph_product_price"></utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_product_code">
        <div class="prod_code">
            <ul class="list-inline prod_code">
                <li class="text"><utilize:translatelabel runat="server" ID="label_product_code" Text="Productcode" CssClass="text"></utilize:translatelabel>:</li>
                <li class="value"><utilize:literal runat="server" ID="lt_prod_code"></utilize:literal></li>
            </ul>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_product_weight">
        <div class="prod_weight">
            <ul class="list-inline prod_weight">
                <li class="text"><utilize:translatelabel runat="server" ID="label_product_weight" Text="Gewicht" CssClass="text"></utilize:translatelabel>:</li>
                <li class="value"><utilize:literal runat="server" ID="lt_prod_weight"></utilize:literal></li>
            </ul>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_free_texts">
        <div class="free_text">
            <utilize:placeholder runat="server" ID="ph_free_text_1">
                    <ul class="list-inline text1">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text1" Text="Vrije tekst 1" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc1" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_2">
                    <ul class="list-inline text2">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text2" Text="Vrije tekst 2" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc2" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_3">
                    <ul class="list-inline text3">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text3" Text="Vrije tekst 3" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc3" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_4">
                    <ul class="list-inline text4">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text4" Text="Vrije tekst 4" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc4" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_5">
                    <ul class="list-inline text5">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text5" Text="Vrije tekst 5" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc5" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_6">
                    <ul class="list-inline text6">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text6" Text="Vrije tekst 6" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc6" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_7">
                    <ul class="list-inline text7">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text7" Text="Vrije tekst 7" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc7" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_8">
                    <ul class="list-inline text8">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text8" Text="Vrije tekst 8" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc8" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_9">
                    <ul class="list-inline text9">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text9" Text="Vrije tekst 9" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc9" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_text_10">
                    <ul class="list-inline text10">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_text10" Text="Vrije tekst 10" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_ff_desc10" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" id="ph_free_lists">
        <div class="free_list">
            <utilize:placeholder runat="server" ID="ph_free_list_1">
                    <ul class="list-inline list1">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_list1" Text="Vrije lijst 1" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_fl_desc1" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_list_2">
            
                    <ul class="list-inline list2">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_list2" Text="Vrije lijst 2" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_fl_desc2" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_list_3">
            
                    <ul class="list-inline list3">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_list3" Text="Vrije lijst 3" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_fl_desc3" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_free_list_4">
                    <ul class="list-inline list4">
                        <li class="text"><utilize:translatelabel runat="server" ID="label_free_list4" Text="Vrije lijst 4" CssClass="text"></utilize:translatelabel>:</li>
                        <li class="value"><utilize:label runat="server" ID="lbl_fl_desc4" CssClass="value"></utilize:label></li>
                    </ul>
            </utilize:placeholder>
        </div>
    </utilize:placeholder>
</div>