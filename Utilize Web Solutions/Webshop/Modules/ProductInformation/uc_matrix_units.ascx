﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_matrix_units.ascx.vb" Inherits="Webshop_Modules_ProductInformation_uc_matrix_units" %>
<div class="uc_matrix_units">
    <utilize:panel runat="server" ID="pnl_matrix_unit" Visible="false">
        <utilize:translatelabel runat ="server" ID="label_matrix_unit_choice" Text="Keuze uit: "></utilize:translatelabel>
        <utilize:dropdownlist runat="server" ID="ddl_unit_1" AutoPostBack="true" CssClass="form-control">
        </utilize:dropdownlist>
        <utilize:dropdownlist runat="server" ID="ddl_unit_2" AutoPostBack="true" CssClass="form-control">
        </utilize:dropdownlist>
        <br />
        <br />
    </utilize:panel>
</div>
