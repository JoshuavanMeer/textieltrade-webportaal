﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_sizeview_matrix.ascx.vb" Inherits="uc_sizeview_matrix" %>
<div class="uc_sizeview_matrix col-md-12">
    <div class="row">
        <h2><utilize:translatetitle runat="server" ID="title_size_view" Text="Kies uw gewenste kleuren en maten" /></h2>
    </div>
    <div class="row">
        <div class="table-responsive">
            <utilize:placeholder runat="server" ID="ph_sizes"></utilize:placeholder>
        </div>
    </div>
    <div class="row margin-bottom-10">
        <utilize:placeholder runat="server" id="ph_legend" Visible="False">
            <utilize:repeater runat="server" ID="rpt_legend">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-1 col-xs-1"><asp:Panel ID="pnl_stock_color" CssClass="stock_color" BackColor='<%# System.Drawing.ColorTranslator.FromHtml(DataBinder.Eval(Container.DataItem, "stock_color")) %>' runat="server"></asp:Panel></div>
                        <div class="col-md-5 col-xs-11"><asp:label ID="label_legend_desc" Text='<%# DataBinder.Eval(Container.DataItem, "stock_desc_" + Me.global_cms.Language)%>' runat="server"></asp:label></div>
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </utilize:placeholder>
    </div>
    <utilize:placeholder runat="server" ID="ph_row_reference" Visible="false">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <label><utilize:translatelabel runat="server" ID="label_product_comment" Text="Opmerkingen"></utilize:translatelabel></label>
                <utilize:textbox runat="server" CssClass="product_comment form-control" ID="txt_product_comment" MaxLength="200"></utilize:textbox>
            </div>
        </div>
    </utilize:placeholder>
    <div class="row">
        <utilize:translatebutton runat='server' ID="button_order" CssClass="btn-u btn-u-sea-shop btn-u-lg pull-right" Text="Bestellen" />
    </div>
</div>