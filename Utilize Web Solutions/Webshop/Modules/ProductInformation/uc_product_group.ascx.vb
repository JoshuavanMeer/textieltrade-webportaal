﻿Partial Class uc_product_group
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""
    Private _product_group As String = ""

    Protected _default_quantity As Integer = 1
    Public Property default_quantity() As Integer
        Get
            Return _default_quantity
        End Get
        Set(ByVal value As Integer)
            _default_quantity = value
        End Set
    End Property

    Public Property product_code() As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            Me.lt_label_desc1.Text = ""
            Me.lt_label_desc2.Text = ""

            Dim ll_resume As Boolean = True

            If ll_resume Then
                _product_code = value.ToUpper()

                ll_resume = Not _product_code = ""
            End If

            If ll_resume Then
                _product_group = Utilize.Data.DataProcedures.GetValue("uws_products", "shp_grp", _product_code)

                ll_resume = Not _product_group = ""
            End If

            If ll_resume Then
                Me.lt_label_desc1.Text = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "grp_desc1", _product_code + Me.global_ws.Language, "prod_code + lng_code")
                Me.lt_label_desc2.Text = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "grp_desc2", _product_code + Me.global_ws.Language, "prod_code + lng_code")

                If Not Me.Page.IsPostBack() Then
                    Me.fill_dropdownlist_1()
                    Me.dd_group_1.SelectedValue = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "grp_val1", _product_code + Me.global_ws.Language, "prod_code + lng_code")

                    Me.fill_dropdownlist_2(Me.dd_group_1.SelectedValue)
                    Me.dd_group_2.SelectedValue = _product_code
                End If
            End If

            Me.Visible = ll_resume
        End Set
    End Property

    Private dt_productgroup As System.Data.DataTable = Nothing
    Private qc_productgroup As New Utilize.Data.QueryCustomSelectClass

    Private Sub fill_dropdownlist_1()
        Dim qc_list1 As New Utilize.Data.QuerySelectClass
        qc_list1.select_fields = "distinct CASE WHEN ISNUMERIC(uws_products_tran.grp_val1) = 1 THEN CAST(replace(uws_products_tran.grp_val1, ',', '.') AS FLOAT) WHEN ISNUMERIC(LEFT(uws_products_tran.grp_val1,1)) = 0 THEN ASCII(LEFT(LOWER(uws_products_tran.grp_val1),1)) ELSE 2147483647 END , max(prod_prio) as prod_prio, uws_products_tran.grp_val1, uws_products_tran.grp_desc1, uws_products.prod_code "
        qc_list1.select_from = "uws_products, uws_products_tran"
        qc_list1.select_where = "uws_products.prod_code = uws_products_tran.prod_code and uws_products.shp_grp = @shp_grp and uws_products.prod_show = 1 and lng_code = @lng_code "
        qc_list1.select_where += "GROUP BY uws_products_tran.grp_val1, uws_products_tran.grp_desc1, uws_products.prod_code"
        qc_list1.select_order = "prod_prio desc, CASE WHEN ISNUMERIC(uws_products_tran.grp_val1) = 1 THEN CAST(replace(uws_products_tran.grp_val1, ',', '.') AS FLOAT) WHEN ISNUMERIC(LEFT(uws_products_tran.grp_val1,1)) = 0 THEN ASCII(LEFT(LOWER(uws_products_tran.grp_val1),1)) ELSE 2147483647 END "

        qc_list1.add_parameter("lng_code", Me.global_ws.Language)
        qc_list1.add_parameter("shp_grp", _product_group)

        Dim dt_data_table As System.Data.DataTable = qc_list1.execute_query()

        Dim lc_product_string As String = ""

        For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
            If Not lc_product_string.Contains("|" + dr_data_row.Item("grp_val1").ToString().Trim() + dr_data_row.Item("grp_desc1").ToString().Trim() + "|") Then
                lc_product_string = lc_product_string + "|" + dr_data_row.Item("grp_val1").ToString().Trim() + dr_data_row.Item("grp_desc1").ToString().Trim() + "|"
            Else
                dr_data_row.Delete()
            End If
        Next

        dt_data_table.AcceptChanges()

        Me.dd_group_1.DataSource = dt_data_table
        Me.dd_group_1.DataTextField = "grp_val1"
        Me.dd_group_1.DataValueField = "grp_val1"
        Me.dd_group_1.DataBind()
    End Sub
    Private Sub fill_dropdownlist_2(ByVal grp_val1 As String)
        Dim qc_list2 As New Utilize.Data.QuerySelectClass
        qc_list2.select_fields = "distinct CASE WHEN ISNUMERIC(uws_products_tran.grp_val2) = 1 THEN CAST(replace(uws_products_tran.grp_val2, ',', '.') AS FLOAT) WHEN ISNUMERIC(LEFT(uws_products_tran.grp_val2,1)) = 0 THEN ASCII(LEFT(LOWER(uws_products_tran.grp_val2),1)) ELSE 2147483647 END, uws_products.prod_prio, uws_products.prod_code, uws_products_tran.grp_val2, uws_products_tran.grp_desc2 "
        qc_list2.select_from = "uws_products, uws_products_tran"
        qc_list2.select_where = "uws_products.prod_code = uws_products_tran.prod_code and uws_products.shp_grp = @shp_grp "
        qc_list2.select_where += "and uws_products_tran.grp_val1 = @grp_val1 And uws_products.prod_show = 1 and lng_code = @lng_code "
        qc_list2.select_order = "prod_prio desc, CASE WHEN ISNUMERIC(uws_products_tran.grp_val2) = 1 THEN CAST(replace(uws_products_tran.grp_val2, ',', '.') AS FLOAT) WHEN ISNUMERIC(LEFT(uws_products_tran.grp_val2,1)) = 0 THEN ASCII(LEFT(LOWER(uws_products_tran.grp_val2),1)) ELSE 2147483647 END "

        qc_list2.add_parameter("lng_code", Me.global_ws.Language)
        qc_list2.add_parameter("shp_grp", _product_group)
        qc_list2.add_parameter("grp_val1", grp_val1)

        Dim dt_data_table As System.Data.DataTable = qc_list2.execute_query()

        Me.dd_group_2.DataSource = dt_data_table
        Me.dd_group_2.DataValueField = "prod_code"
        Me.dd_group_2.DataTextField = "grp_val2"
        Me.dd_group_2.DataBind()
    End Sub

    Protected Sub Page_Init() Handles Me.Init
        Me.set_style_sheet("uc_product_group.css", Me)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.Visible Then
            Me.Visible = Not Me.product_code = ""
        End If

        If Me.Visible Then
            Me.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group")
        End If

        If Me.Visible And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sizeview") = True Then
            ' Als sizeview actief is en het is een sizeview product, dan tonen we deze control niet.
            Me.Visible = Not Utilize.Data.DataProcedures.GetValue("uws_products", "sizebar_article", Me.product_code) = True
        End If

        If Me.Visible Then
            Me.ph_group_1.Visible = Me.dd_group_1.Items.Count > 1
            Me.ph_group_2.Visible = Me.dd_group_2.Items.Count > 1
        End If
    End Sub

    Function SetProperty(ByVal obj As Object, ByVal propertyName As String, ByVal val As Object) As Boolean
        Try
            ' get a reference to the PropertyInfo, exit if no property with that 
            ' name
            Dim pi As System.Reflection.PropertyInfo = obj.GetType().GetProperty _
                (propertyName)
            If pi Is Nothing Then Return False
            ' convert the value to the expected type
            val = Convert.ChangeType(val, pi.PropertyType)
            ' attempt the assignment
            pi.SetValue(obj, val, Nothing)
            Return True
        Catch
            Return False
        End Try
    End Function

    Protected Sub dd_group_1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_group_1.SelectedIndexChanged
        Me.fill_dropdownlist_2(Me.dd_group_1.SelectedValue)

        Dim lc_product_code As String = Me.dd_group_2.SelectedValue

        Dim lc_url As String = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", lc_product_code + Me.global_ws.Language, "prod_code + lng_code")

        Dim loUrlClass As New base_url_base(lc_url)
        loUrlClass.set_parameter("prod_code", Server.UrlEncode(lc_product_code))

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "GroupRedirect", "location.replace('" + loUrlClass.get_url() + "');", True)
    End Sub
    Protected Sub dd_group_2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_group_2.SelectedIndexChanged
        Dim lc_product_code As String = Me.dd_group_2.SelectedValue

        Dim lc_url As String = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "tgt_url", lc_product_code + Me.global_ws.Language, "prod_code + lng_code")

        Dim loUrlClass As New base_url_base(lc_url)
        loUrlClass.set_parameter("prod_code", Server.UrlEncode(lc_product_code))

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "GroupRedirect", "location.replace('" + loUrlClass.get_url() + "');", True)
    End Sub
End Class
