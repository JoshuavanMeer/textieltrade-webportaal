﻿Imports System.Data

Partial Class uc_product_documents
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ll_resume As Boolean = True
        Dim lc_prod_code As String = ""
        Dim dt_product_group_documents As DataTable = Nothing

        Me.set_style_sheet("uc_product_documents.css", Me)

        ' Controleer of de module product documenten aan staat
        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_doc")
        End If

        ' Ophalen van de parameters
        If ll_resume Then
            lc_prod_code = Me.get_page_parameter("prod_code")
        End If

        ' Load the product documents in the repeater
        If ll_resume Then
            Dim qsc_product_documents As New Utilize.Data.QuerySelectClass
            qsc_product_documents.select_fields = "crec_id, file_name, doc_desc"
            qsc_product_documents.select_from = "uws_product_doc"
            qsc_product_documents.select_where = "prod_code = @prod_code"
            qsc_product_documents.add_parameter("prod_code", lc_prod_code)

            Dim dt_data_table As DataTable = qsc_product_documents.execute_query()

            Me.rpt_product_documents.DataSource = dt_data_table
            Me.rpt_product_documents.DataBind()
        End If

        ' Ophalen van de product documenten
        If ll_resume Then
            ' Get the product group of the current product
            Dim lc_prod_group As String = Utilize.Data.DataProcedures.GetValue("uws_products", "prod_group", lc_prod_code)

            ' Ophalen van de product groep docmenten
            Dim qsc_product_group_documents As New Utilize.Data.QuerySelectClass
            qsc_product_group_documents.select_fields = "*"
            qsc_product_group_documents.select_from = "uws_prod_group_docs"

            ' Add the product group to the where statement
            qsc_product_group_documents.select_where = "(grp_code = @grp_code OR grp_code = '')"
            qsc_product_group_documents.add_parameter("grp_code", lc_prod_group)

            ' Add the product code string to the where statement
            qsc_product_group_documents.select_where += " AND (@prod_code LIKE prod_string OR prod_string = '')"
            qsc_product_group_documents.add_parameter("prod_code", lc_prod_code)

            ' Add the product code to the where statement
            qsc_product_group_documents.select_where += " AND (prod_code = @prod_code OR prod_code = '')"

            ' Sorteer op groepcode, product string, product code zodat het verwijderen obv de niveau's goed gaat
            qsc_product_group_documents.select_order = "grp_code, prod_string, prod_code"

            ' Get the product group documents and save them in a DataTable
            dt_product_group_documents = qsc_product_group_documents.execute_query()
        End If

        ' Verwijder niet te tonen product groep documenten
        ' Laden van de product groep documenten in de repeater
        If ll_resume Then
            ' Alleen als er product groep documenten zijn opgehaald
            If dt_product_group_documents IsNot Nothing Then
                Dim ll_delete_row As Boolean = False

                ' Loop door de product documenten
                For Each dr_product_group_document As DataRow In dt_product_group_documents.Rows
                    ' Delete the row or check if the next rows need to be deleted
                    If ll_delete_row Then
                        ' Verwijder het product document uit de datatable
                        dr_product_group_document.Delete()
                    Else
                        ' Als bij het document is aangegeven dat documenten van een hoger niveau niet weergegeven mogen worden
                        If dr_product_group_document.Item("ignore_top") Then
                            ' Opslaan variabele zodat bij de volgende regels de documenten worden verwijderd
                            ll_delete_row = True
                        End If

                        ' Check if the document exists
                        If String.IsNullOrEmpty(get_product_group_document_link(dr_product_group_document.Item("crec_id"))) Then
                            ' The document does not exist, delete the file
                            dr_product_group_document.Delete()
                        End If
                    End If
                Next

                ' Update de DataTable zodat de verwijderde documenten echt worden verwijderd
                dt_product_group_documents.AcceptChanges()
            End If

            ' Toon de product groep documenten
            Me.rpt_product_group_documents.DataSource = dt_product_group_documents
            Me.rpt_product_group_documents.DataBind()
        End If

        ' Toon de user control alleen als er product documenten of product group documenten zijn
        If ll_resume Then
            ll_resume = Me.rpt_product_documents.Items.Count > 0 Or Me.rpt_product_group_documents.Items.Count > 0
        End If

        Me.Visible = ll_resume
    End Sub

    Friend Function get_product_document_link(ByVal rec_id As String) As String
        Try
            Dim lc_file_name As String = Utilize.Data.DataProcedures.GetValue("uws_product_doc", "file_name", rec_id)

            If System.IO.File.Exists(Server.MapPath("~\" + lc_file_name)) Then
                Return Me.Page.ResolveUrl("~/" + lc_file_name.Replace("\", "/"))
            Else
                Return Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/account/opendocument.aspx?DocType=ProductDocument&DocNr=" + rec_id
            End If
        Catch ex As Exception
            ' Als bestandsnaam .. is dan de foutmelding: Kan geen voorlooptekens (..) gebruiken voor afsluiten boven de hoogste map.
        End Try

        Return "#"
    End Function

    Friend Function get_product_group_document_link(ByVal rec_id As String) As String
        Try
            ' Ophalen van de bestandsnaam van het document
            Dim lc_file_name As String = Utilize.Data.DataProcedures.GetValue("uws_prod_group_docs", "file_name", rec_id)

            ' De product documents worden of op de server opgeslagen, of - als deze vanuit exact worden geuploaded - in de database opgeslagen (in veld doc_content).
            ' Als product groep documenten worden geuploaded worden deze op de server opgeslagen in de map ..\Documents\ProductGroupDocuments.
            If System.IO.File.Exists(Server.MapPath("~\" + lc_file_name)) Then
                Return Me.Page.ResolveUrl("~/" + lc_file_name.Replace("\", "/"))
            End If
        Catch ex As Exception
            ' Als bestandsnaam .. is dan de foutmelding: Kan geen voorlooptekens (..) gebruiken voor afsluiten boven de hoogste map.
        End Try

        Return "#"
    End Function

End Class
