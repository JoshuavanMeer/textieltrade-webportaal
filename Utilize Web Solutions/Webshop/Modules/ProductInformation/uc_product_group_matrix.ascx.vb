﻿Partial Class uc_product_group_matrix
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Friend Class order_control
        Inherits Utilize.Web.UI.WebControls.placeholder

        ' Als het een sizeview kolom is, dan moet een textbox toegevoegd worden
        Public txt_quantity As New Utilize.Web.UI.WebControls.textbox
        Private button_add As New Utilize.Web.UI.WebControls.button
        Private button_subtract As New Utilize.Web.UI.WebControls.button

        Private _enabled As Boolean = True
        Public Property Enabled As Boolean
            Get
                Return _enabled
            End Get
            Set(value As Boolean)
                _enabled = value

                txt_quantity.Enabled = value
                button_add.Enabled = value
                button_subtract.Enabled = value
            End Set
        End Property

        Private Sub order_control_Init(sender As Object, e As EventArgs) Handles Me.Init
            button_add.Text = "+"
            button_add.CssClass = "inputbutton increment quantity-button quantity-min"

            button_subtract.Text = "-"
            button_subtract.CssClass = "inputbutton increment quantity-button quantity-min"

            Me.Controls.Add(button_subtract)
            Me.Controls.Add(txt_quantity)
            Me.Controls.Add(button_add)

            txt_quantity.Text = "-"
            txt_quantity.MaxLength = 4
            txt_quantity.CssClass = "quantity quantity-field incrementcount"

            button_add.OnClientClick = "increment('" + txt_quantity.ClientID + "'); return false;"
            button_subtract.OnClientClick = "decrement('" + txt_quantity.ClientID + "'); return false;"
        End Sub
    End Class

    Private _product_code As String = ""
    Private _product_group As String = ""

    Private _group_type As Utilize.Web.Solutions.Webshop.group_types = Webshop.group_types.none
    Public Property group_type As Utilize.Web.Solutions.Webshop.group_types
        Get
            Return _group_type
        End Get
        Set(value As Utilize.Web.Solutions.Webshop.group_types)
            _group_type = value
        End Set
    End Property

    Public Property product_code() As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            Dim ll_resume As Boolean = True

            If ll_resume Then
                _product_code = value.ToUpper()

                ll_resume = Not _product_code = ""
            End If

            If ll_resume Then
                _product_group = Utilize.Data.DataProcedures.GetValue("uws_products", "shp_grp", _product_code)

                ll_resume = Not _product_group = ""
            End If

            If ll_resume Then
                Select Case _group_type
                    Case Webshop.group_types.group_val1
                        Me.create_table()
                    Case Webshop.group_types.group_val2
                        Me.create_table()
                    Case Webshop.group_types.group_matrix
                        Me.create_matrix()
                    Case Webshop.group_types.none
                        Exit Select
                    Case Webshop.group_types.group
                        Exit Select
                    Case Else
                        Exit Select
                End Select
            End If
        End Set
    End Property

#Region " Group on value 1 or on value 2"
    Private Function get_values1() As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "distinct uws_products.prod_prio, uws_products_tran.grp_val1 as grp_val, uws_products_tran.prod_code, uws_products_tran.prod_desc, uws_products.sale_unit"
        qsc_select.select_from = "uws_products_tran"
        qsc_select.add_join("left outer join", "uws_products", "uws_products_tran.prod_code = uws_products.prod_code")
        qsc_select.select_where = "lng_code = @lng_code and uws_products_tran.shp_grp = @shp_grp and not uws_products_tran.grp_val1 = '' and uws_products.prod_show = 1"
        qsc_select.select_order = "uws_products.prod_prio desc, uws_products_tran.grp_val1"

        qsc_select.add_parameter("lng_code", Me.global_ws.Language)
        qsc_select.add_parameter("shp_grp", _product_group)

        Return qsc_select.execute_query
    End Function
    Private Function get_values2() As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "distinct uws_products.prod_prio, uws_products_tran.grp_val2 as grp_val, uws_products_tran.prod_code, uws_products_tran.prod_desc, uws_products.sale_unit"
        qsc_select.select_from = "uws_products_tran"
        qsc_select.add_join("left outer join", "uws_products", "uws_products_tran.prod_code = uws_products.prod_code")
        qsc_select.select_where = "lng_code = @lng_code and uws_products_tran.shp_grp = @shp_grp and not uws_products_tran.grp_val2 = '' and uws_products.prod_show = 1"
        qsc_select.select_order = "uws_products.prod_prio desc, uws_products_tran.grp_val2"

        qsc_select.add_parameter("lng_code", Me.global_ws.Language)
        qsc_select.add_parameter("shp_grp", _product_group)

        Return qsc_select.execute_query
    End Function
    Private Sub create_table()
        Dim llResume As Boolean = True

        If llResume Then
            llResume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group")
        End If

        If llResume Then
            Dim dt_rows As System.Data.DataTable = Nothing

            Select Case _group_type
                Case 0

                Case 1

                Case 2
                    Dim lc_group_desc As String = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "grp_desc1", Me.product_code + Me.global_ws.Language, "prod_code + lng_code")
                    If Not lc_group_desc.Trim() = "" Then
                        Me.lt_group_desc.Text = "<h2>" + lc_group_desc + "</h2>"
                    End If

                    dt_rows = Me.get_values1()
                Case 3
                    Dim lc_group_desc As String = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "grp_desc2", Me.product_code + Me.global_ws.Language, "prod_code + lng_code")
                    If Not lc_group_desc.Trim() = "" Then
                        Me.lt_group_desc.Text = "<h2>" + lc_group_desc + "</h2>"
                    End If

                    dt_rows = Me.get_values2()
                Case Webshop.group_types.group_matrix
                    Exit Select
                Case Else
                    Exit Select
            End Select

            If Not dt_rows Is Nothing Then
                Me.ph_group_products.Visible = True

                Me.set_style_sheet("uc_product_group_matrix.css", Me)
                Me.set_javascript("uc_product_group_matrix.js", Me)

                Me.rpt_group_values.DataSource = dt_rows
                Me.rpt_group_values.DataBind()

                ' Maak de oorspronkelijke bestelknop onzichtbaar
                _uc_order_product.Visible = False
            End If

            Me.Visible = Not dt_rows Is Nothing
        End If
    End Sub

    Private Function order_group_value() As Boolean
        Dim ll_resume As Boolean = True
        Dim ll_products_added As Boolean = False

        If ll_resume Then
            ' Eerst controleren of de waarden correct zijn
            For I As Integer = 0 To Me.rpt_group_values.Items.Count - 1
                Dim txt_order_quantity As textbox = Me.rpt_group_values.Items(I).FindControl("txt_order_quantity")
                Dim lt_sale_unit As literal = Me.rpt_group_values.Items(I).FindControl("lt_sale_unit")
                Dim ln_sale_unit As Decimal = Decimal.Parse(lt_sale_unit.Text)
                ln_sale_unit = IIf(ln_sale_unit = 0, 1, ln_sale_unit)

                Try
                    Dim ln_total As Decimal = Decimal.Parse(txt_order_quantity.Text) * ln_sale_unit

                    If ln_total > 0 Then
                        ll_products_added = True
                    End If
                Catch ex As Exception
                    ' De aantallen kloppen niet, geef een foutmelding
                    Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                    ll_resume = False

                    Exit For
                End Try

                If Not ll_resume Then
                    ' Error melding tonen
                    If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                    End If
                End If
            Next
        End If

        If ll_resume And ll_products_added Then
            ' Eerst controleren of de waarden correct zijn
            For I As Integer = 0 To Me.rpt_group_values.Items.Count - 1
                Dim txt_order_quantity As textbox = Me.rpt_group_values.Items(I).FindControl("txt_order_quantity")
                Dim lt_sale_unit As literal = Me.rpt_group_values.Items(I).FindControl("lt_sale_unit")
                Dim ln_sale_unit As Decimal = Decimal.Parse(lt_sale_unit.Text)
                ln_sale_unit = IIf(ln_sale_unit = 0, 1, ln_sale_unit)

                Dim lt_prod_code As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_code")

                If Not txt_order_quantity.Text = "0" And Not txt_order_quantity.Text = "-" Then
                    Try
                        Dim ln_total As Decimal = Decimal.Parse(txt_order_quantity.Text)

                        ' Het gaat om verkoopeenheden dus we moeten een andere waarde gebruiken
                        ll_resume = Me.global_ws.shopping_cart.product_add(lt_prod_code.Text, ln_total, "", Utilize.Data.API.Webshop.product_manager.get_product_unit_default(_product_code), "", "")
                    Catch ex As Exception
                        ' De aantallen kloppen niet, geef een foutmelding
                        Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                        ll_resume = False
                    End Try


                    If Not ll_resume Then
                        ' Error melding tonen
                        If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                        End If
                    End If
                End If
            Next
        End If

        Return ll_resume And ll_products_added
    End Function
#End Region

#Region " Matrix"
    Private dtMatrix As System.Data.DataTable

    Private Function get_matrix_products() As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "distinct uws_products.prod_prio, uws_products_tran.grp_val1 as grp_val1, uws_products_tran.grp_val2 as grp_val2, uws_products_tran.prod_code, uws_products_tran.prod_desc"
        qsc_select.select_from = "uws_products_tran"
        qsc_select.add_join("left outer join", "uws_products", "uws_products_tran.prod_code = uws_products.prod_code")
        qsc_select.select_where = "lng_code = @lng_code and uws_products_tran.shp_grp = @shp_grp and not uws_products_tran.grp_val1 = ''  and not uws_products_tran.grp_val2 = '' and uws_products.prod_show = 1"
        qsc_select.select_order = "uws_products.prod_prio desc, uws_products_tran.grp_val1, uws_products_tran.grp_val2"

        qsc_select.add_parameter("lng_code", Me.global_ws.Language)
        qsc_select.add_parameter("shp_grp", _product_group)

        Return qsc_select.execute_query
    End Function

    Private Sub create_matrix()
        dtMatrix = Me.get_matrix_products()

        If dtMatrix.Rows.Count > 0 Then
            Me.set_style_sheet("uc_product_group_matrix.css", Me)

            Me.pnl_group_product_matrix.Visible = True

            ' Haal de X en de Y as op
            Dim dt_x_values As System.Data.DataTable = dtMatrix.DefaultView.ToTable(True, "grp_val1")
            Dim dt_y_values As System.Data.DataTable = dtMatrix.DefaultView.ToTable(True, "grp_val2")

            ' Nieuwe HTML table
            Dim htTable As New System.Web.UI.HtmlControls.HtmlTable

            ' Nieuwe header regel
            Dim hrHeaderRow As New System.Web.UI.HtmlControls.HtmlTableRow

            ' Cell voor de linker bovenhoek
            Dim hcSpacingCell As New System.Web.UI.HtmlControls.HtmlTableCell

            ' Voeg de controls toe aan de row en de table
            hrHeaderRow.Cells.Add(hcSpacingCell)
            hrHeaderRow.Attributes.Add("class", "matrixheader")

            htTable.Rows.Add(hrHeaderRow)

            ' Loop door de X-As heen
            For Each drXRow As System.Data.DataRow In dt_x_values.Rows
                Dim hcHeaderCell As New System.Web.UI.HtmlControls.HtmlTableCell
                hcHeaderCell.InnerText = drXRow.Item("grp_val1")
                hcHeaderCell.Attributes.Add("class", "description-x")

                hrHeaderRow.Cells.Add(hcHeaderCell)
            Next

            '  Loop door de Y-As heen
            For Each drYRow As System.Data.DataRow In dt_y_values.Rows
                Dim hrTableRow As New System.Web.UI.HtmlControls.HtmlTableRow

                Dim hcTableCell As New System.Web.UI.HtmlControls.HtmlTableCell
                hcTableCell.InnerText = drYRow.Item("grp_val2")
                hcTableCell.Attributes.Add("class", "description-y")

                hrTableRow.Cells.Add(hcTableCell)

                htTable.Rows.Add(hrTableRow)

                For Each drXRow As System.Data.DataRow In dt_x_values.Rows
                    Dim hcCell As New HtmlTableCell
                    hrTableRow.Controls.Add(hcCell)

                    Dim drSelectRow As System.Data.DataRow() = dtMatrix.Select("grp_val1 = '" + drXRow.Item("grp_val1") + "' and grp_val2 = '" + drYRow.Item("grp_val2") + "'")

                    If drSelectRow.Length > 0 Then
                        Dim ucOrderProduct As New order_control
                        ucOrderProduct.ID = "oc" + drSelectRow(0).Item("prod_code")

                        hcCell.Controls.Add(ucOrderProduct)
                    Else
                        Dim ucOrderProduct As New order_control
                        ucOrderProduct.Enabled = False
                        hcCell.Controls.Add(ucOrderProduct)
                    End If
                Next
            Next


            Me.pnl_group_product_matrix.Controls.Add(htTable)
        End If
    End Sub

    Private Function order_matrix() As Boolean
        Dim ll_resume As Boolean = True
        Dim ll_products_added As Boolean = False

        If ll_resume Then
            ' Eerst controleren of de waarden correct zijn
            For Each drMatrixRow As System.Data.DataRow In dtMatrix.Rows
                Dim txt_order_quantity As textbox = CType(Me.ph_group_products.FindControl("oc" + drMatrixRow.Item("prod_code")), order_control).txt_quantity

                Dim ln_sale_unit As Decimal = Utilize.Data.DataProcedures.GetValue("uws_products", "sale_unit", drMatrixRow.Item("prod_code"))
                ln_sale_unit = IIf(ln_sale_unit = 0, 1, ln_sale_unit)

                Try
                    Dim ln_total As Decimal = Decimal.Parse(txt_order_quantity.Text) * ln_sale_unit

                    If ln_total > 0 Then
                        ll_products_added = True
                    End If
                Catch ex As Exception
                    ' De aantallen kloppen niet, geef een foutmelding
                    Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                    ll_resume = False

                    Exit For
                End Try

                If Not ll_resume Then
                    ' Error melding tonen
                    If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                    End If
                End If
            Next
        End If

        If ll_resume And ll_products_added Then
            ' Eerst controleren of de waarden correct zijn
            For Each drMatrixRow As System.Data.DataRow In dtMatrix.Rows
                Dim txt_order_quantity As textbox = CType(Me.ph_group_products.FindControl("oc" + drMatrixRow.Item("prod_code")), order_control).txt_quantity

                Dim ln_sale_unit As Decimal = Utilize.Data.DataProcedures.GetValue("uws_products", "sale_unit", drMatrixRow.Item("prod_code"))
                ln_sale_unit = IIf(ln_sale_unit = 0, 1, ln_sale_unit)

                If Not txt_order_quantity.Text = "0" And Not txt_order_quantity.Text = "-" Then
                    Try
                        Dim ln_total As Decimal = Decimal.Parse(txt_order_quantity.Text) * ln_sale_unit

                        ' Het gaat om verkoopeenheden dus we moeten een andere waarde gebruiken
                        ll_resume = Me.global_ws.shopping_cart.product_add(drMatrixRow.Item("prod_code"), ln_total, "", "", "", "", Me.global_ws.purchase_combination.user_id)
                    Catch ex As Exception
                        ' De aantallen kloppen niet, geef een foutmelding
                        Me.global_ws.shopping_cart.ubo_shopping_cart.set_error_message(global_trans.translate_message("message_invalid_quantity", "Er is een ongeldig aantal ingevuld.", Me.global_cms.Language))

                        ll_resume = False
                    End Try


                    If Not ll_resume Then
                        ' Error melding tonen
                        If String.IsNullOrEmpty(Me.global_ws.shopping_cart.error_message) Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.ubo_shopping_cart.error_message + "');", True)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + Me.global_ws.shopping_cart.error_message + "');", True)
                        End If
                    End If
                End If
            Next
        End If

        Return ll_resume And ll_products_added
    End Function
#End Region


    Private _uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control
    Public Property uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control
        Get
            Return _uc_order_product
        End Get
        Set(value As Utilize.Web.Solutions.Webshop.ws_user_control)
            _uc_order_product = value
        End Set
    End Property

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.Visible Then
            Me.Visible = Not Me.product_code = ""
        End If

        If Me.Visible Then
            Me.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group")
        End If

        If Me.Visible Then
            ' Als de gebruiker niet is ingelogd, dan mag deze alleen getoond worden wanneer er besteld mag worden wanneer je niet ingelogd bent
            Dim ll_order_not_logged_on As Boolean = Me.global_ws.get_webshop_setting("order_not_logged_on")

            If Not Me.global_ws.user_information.user_logged_on Then
                Me.Visible = ll_order_not_logged_on
            End If
        End If

        If Me.Visible And Me.global_ws.user_information.user_logged_on Then
            ' Het hele gedeelte is alleen zichtbaar wanneer de gebruiker rechten heeft om te bestellen
            Me.Visible = Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po") = True
        End If

        If Me.Visible Then

            Dim ll_async_order As Boolean = Me.global_ws.get_webshop_setting("use_async_order")
            Dim ll_lazy_loading As Boolean = Me.Visible And Me.global_ws.get_webshop_setting("use_async_order")
            Dim prices_not_visible As Boolean = ((Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on) Or (Me.global_ws.get_webshop_setting("prices_not_logged_on") And Not Me.global_ws.user_information.user_logged_on)

            If Not Me.IsPostBack() Then
                If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                    For I As Integer = 0 To Me.rpt_group_values.Items.Count - 1
                        Dim lt_prod_code As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_code")
                        Dim lt_prod_price As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_price")
                        Dim ln_prod_price As Decimal = Me.global_ws.get_product_price(lt_prod_code.Text, "")

                        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                            lt_prod_price.Text = Format(ln_prod_price, "c")
                        Else
                            lt_prod_price.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price).ToString()
                        End If
                        lt_prod_price.Visible = Not prices_not_visible
                    Next
                Else
                    For I As Integer = 0 To Me.rpt_group_values.Items.Count - 1
                        Dim lt_prod_code As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_code")
                        Dim lt_prod_price As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_price")
                        Dim ln_prod_price As Decimal = Me.global_ws.get_product_price_incl_vat(lt_prod_code.Text, "")

                        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                            lt_prod_price.Text = Format(ln_prod_price, "c")
                        Else
                            lt_prod_price.Text = Me.global_ws.convert_amount_to_credits(ln_prod_price).ToString()
                        End If
                        lt_prod_price.Visible = Not prices_not_visible
                    Next
                End If
            End If

            For I As Integer = 0 To Me.rpt_group_values.Items.Count - 1

                Dim lt_prod_code As literal = Me.rpt_group_values.Items(I).FindControl("lt_prod_code")
                Dim txt_order_quantity As textbox = Me.rpt_group_values.Items(I).FindControl("txt_order_quantity")
                Dim img_plus_normal As button = Me.rpt_group_values.Items(I).FindControl("img_plus_normal")
                Dim img_min_normal As button = Me.rpt_group_values.Items(I).FindControl("img_min_normal")

                img_min_normal.OnClientClick = "decrement('" + txt_order_quantity.ClientID + "'); return false;"
                img_plus_normal.OnClientClick = "increment('" + txt_order_quantity.ClientID + "'); return false;"

                ' Attach prod code as attribute to text element so it can be read out with js using async ordering
                If ll_lazy_loading Or ll_async_order Then
                    txt_order_quantity.Attributes.Add("data-prod-code", lt_prod_code.Text)
                End If

                ' Pass product code and set up event listeners for async ordering
                If ll_async_order Then
                    Me.button_order.OnClientClick = "return false;"
                End If
            Next

        End If

        If Me.Visible Then
            Select Case group_type
                Case Webshop.group_types.group_matrix And dtMatrix IsNot Nothing
                    Me.Visible = dtMatrix.Rows.Count > 0
                Case Else
                    Me.Visible = Me.rpt_group_values.Items.Count > 0
            End Select
        End If
    End Sub

    Protected Sub button_order_Click(sender As Object, e As EventArgs) Handles button_order.Click
        Dim ll_resume As Boolean = True

        Select Case _group_type
            Case Webshop.group_types.group_val1
                ll_resume = Me.order_group_value()
            Case Webshop.group_types.group_val2
                ll_resume = Me.order_group_value()
            Case Webshop.group_types.group_matrix
                ll_resume = Me.order_matrix()
            Case Webshop.group_types.none
                Exit Select
            Case Webshop.group_types.group
                Exit Select
            Case Else
                Exit Select
        End Select

        If ll_resume Then
            ' Als er in de webshop instellingen is aangegegeven dat je na het bestellen direct naar de winkelwagen gaat, dan omleiden.
            If Me.global_ws.get_webshop_setting("order_redirect") = True Then
                Response.Redirect("~/" + me.global_ws.language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "$('#order_message').fadeIn().delay(1000).fadeOut();", True)
            End If
        End If
    End Sub
End Class
