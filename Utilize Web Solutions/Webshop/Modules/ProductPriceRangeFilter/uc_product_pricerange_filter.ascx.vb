﻿
Partial Class uc_product_pricerange_filter
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Variabele die bepaalt of de control getoond moet worden
        Dim ll_resume As Boolean = Me.check_availability()

        If ll_resume Then
            Dim ll_show As Boolean = False

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_pricerange") Then
                ' Haal de waarden van het prijsbereik op
                Dim dt_data_table As System.Data.DataTable = Me.get_pricerange_values()

                ' Als er waarden gevonden zijn
                If dt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_list1.DataSource = dt_data_table
                    Me.rpt_free_list1.DataBind()
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_pricerange.Visible = False
                End If
            End If

            ll_resume = ll_show
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub

    Friend lc_url As String = ""

    ''' <summary>
    ''' Controleer hier of de control zichtbaar moet zijn of niet
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function check_availability() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            If Request.Url.ToString.Contains("product_list.aspx") Then
                lc_url = Request.RawUrl.ToString()
            Else
                lc_url = "~/Webshop/product_list.aspx"
            End If
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_product_pricerange_filter.css", Me)
        End If

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_pricerange")
        End If

        Return ll_resume
    End Function

    Friend Function get_url(ByVal li_range_start As String, ByVal li_range_end As String) As String
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lc_url)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("range_start", li_range_start)
        url_class.set_parameter("range_end", li_range_end)

        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Return lc_url
    End Function

    Private Function get_freelist_values(ByVal list_nr As Integer) As System.Data.DataTable
        Dim qsc_data_table As New Utilize.Data.QuerySelectClass
        qsc_data_table.select_fields = "*"
        qsc_data_table.select_from = "uws_free_list" + list_nr.ToString()
        qsc_data_table.select_order = "row_ord"

        Return qsc_data_table.execute_query()
    End Function

    Private Function get_pricerange_values() As System.Data.DataTable
        Dim qsc_data_table As New Utilize.Data.QuerySelectClass
        qsc_data_table.select_fields = "*"
        qsc_data_table.select_from = "uws_pricerange"
        qsc_data_table.select_order = "row_ord"

        Return qsc_data_table.execute_query()
    End Function

End Class
