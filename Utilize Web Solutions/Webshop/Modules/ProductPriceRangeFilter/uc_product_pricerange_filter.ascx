﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_pricerange_filter.ascx.vb" Inherits="uc_product_pricerange_filter" %>

<div class="uc_product_pricerange_filter">
    <utilize:placeholder runat="server" ID="ph_pricerange">
        <h2><utilize:translatetitle runat="server" ID="title_pricefilter" Text="Filter op prijs"></utilize:translatetitle></h2>
        <div class="free_list">
            <utilize:repeater runat="server" ID="rpt_free_list1">
                <ItemTemplate>
                    <a class="product" href="<%# Me.get_url(DataBinder.Eval(Container.DataItem, "range_start"), DataBinder.Eval(Container.DataItem, "range_end"))%>"><%# DataBinder.Eval(Container.DataItem, "range_desc")%></a>
                </ItemTemplate>
            </utilize:repeater>
        </div>
    </utilize:placeholder>
    
</div>