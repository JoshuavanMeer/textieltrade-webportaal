﻿
Partial Class uc_product_videos
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property product_code() As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.set_style_sheet("uc_product_videos.css", Me)

        ' licentie controle
        Dim ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_PROD_VIDEOS")

        If ll_resume Then
            Me.rpt_videos.DataSource = fetch_videos(_product_code)
            Me.rpt_videos.DataBind()
        End If

        If ll_resume Then
            ll_resume = Me.rpt_videos.Items.Count > 0
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub

    Private Function fetch_videos(ByVal prod_code As String) As System.Data.DataTable
        Dim cqs_videos As New Utilize.Data.QueryCustomSelectClass
        cqs_videos.QueryString = "select * from uws_product_videos where prod_code = @prod_code"
        cqs_videos.add_parameter("prod_code", prod_code)
        Return cqs_videos.execute_query(Me.global_ws.user_information.user_id)
    End Function

    Friend Function get_video_link(ByVal url As String) As String
        If Not url.ToLower().StartsWith("http") Then
            If FrameWork.FrameworkProcedures.GetRequestUrl().StartsWith("https") Then
                url = "https://www.youtube.com/embed/" + url
            Else
                url = "http://www.youtube.com/embed/" + url
            End If
        Else
            Dim lc_domain_protocol As String = "http"

            If FrameWork.FrameworkProcedures.GetRequestUrl().StartsWith("https") Then
                lc_domain_protocol = "https"
            End If

            url = url.Replace("http://", lc_domain_protocol + "://")
        End If

        Return url
    End Function

End Class
