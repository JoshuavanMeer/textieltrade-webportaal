﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_videos.ascx.vb" Inherits="uc_product_videos" %>
<div class="uc_product_videos">
    <h2><utilize:translatetitle runat="server" ID="title_youtube_videos" Text="Filmpjes van dit product"></utilize:translatetitle></h2>
    <div class="row">
        <utilize:repeater ID="rpt_videos" runat="server">
            <ItemTemplate>
                <div class="video-container col-md-6">
                    <h3 class="video-description">
                        <%# DataBinder.Eval(Container.DataItem, "video_desc") %>
                    </h3>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="youtube_player embed-responsive-item" src='<%# Me.get_video_link(DataBinder.Eval(Container.DataItem, "video_url")) %>' frameBorder="0" type="text/html" allowfullscreen></iframe>                   
                    </div>                    
                </div>
            </ItemTemplate>
        </utilize:repeater>
    </div>
</div>
    