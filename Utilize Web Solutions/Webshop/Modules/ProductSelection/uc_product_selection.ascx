﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_selection.ascx.vb" Inherits="uc_product_selection" %>
<utilize:panel ID="pnl_product_selection" CssClass="uc_product_selection" runat="server">
    <input type="hidden" class="hidden_item_size" value="<%= Me.item_size %>" />
    <div class="header-container">
        <h2><utilize:translatetitle runat='server' ID="title_products_selection" Text="Product selectie"></utilize:translatetitle></h2>
        <h5><utilize:hyperlink runat="server" ID="hl_show_more" text="Toon meer <i class='fa fa-arrow-right'></i>"></utilize:hyperlink></h5>
    </div>    
    <div class="clearfix"></div>
    <div class="row">
        <asp:Repeater runat="server" ID="rpt_products">
            <ItemTemplate>
               <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>

                <utilize:placeholder runat="server" ID="ph_clear_fix" Visible="false">
                    <div class="clearfix"></div>
                </utilize:placeholder>
                <utilize:placeholder runat="server" ID="ph_mobile_clearfix" Visible="false">
                    <div class="mobile-clearfix clearfix"></div>
                </utilize:placeholder>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <utilize:image runat="server" CssClass="label-img label-first" ID="img_label" />
</utilize:panel>