﻿
Partial Class uc_product_selection
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _item_count As Integer = 6

    Public Enum control_type
        list
        blocks
    End Enum

    Class button_ext
        Inherits Utilize.Web.UI.WebControls.button
        Private _prd_code As String
        Public Property prd_code() As String
            Get
                Return _prd_code
            End Get
            Set(ByVal value As String)
                _prd_code = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class

    ''' <summary>
    ''' The number of items shown in the salesactions
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property item_count As Integer
        Get
            Return _item_count
        End Get
        Set(ByVal value As Integer)
            _item_count = value
        End Set
    End Property

    Private _overview_type As control_type = control_type.blocks
    Public Property overview_type As control_type
        Get
            Return _overview_type
        End Get
        Set(value As control_type)
            _overview_type = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True

        Dim dt_data_table As System.Data.DataTable = Nothing
        Dim ws_product_filter As ws_productfilter = Nothing
        Dim qsc_prod_selection As Utilize.Data.QuerySelectClass = Nothing
        Dim ds_prod_selection As System.Data.DataTable = Nothing

        If ll_testing Then
            ws_product_filter = New ws_productfilter
            ws_product_filter.page_size = 4

            qsc_prod_selection = New Utilize.Data.QuerySelectClass
            qsc_prod_selection.select_fields = "*"
            qsc_prod_selection.select_from = "uws_prod_selection"
            qsc_prod_selection.select_where = "rec_id = @rec_id"
            qsc_prod_selection.add_parameter("rec_id", Me.block_id)

            ds_prod_selection = qsc_prod_selection.execute_query()

            If ds_prod_selection.Rows.Count > 0 Then
                ws_product_filter.category_code = ds_prod_selection.Rows(0).Item("prod_sel_cat")
            Else
                ll_testing = False
            End If

            If Not Me.global_ws.user_information.user_logged_on Then
                Me.pnl_product_selection.Visible = ((CType(Utilize.Data.DataProcedures.GetValue("uws_prod_selection", "show_type", Me.block_id, "rec_id"), Integer) = 0 Or _
                                                    CType(Utilize.Data.DataProcedures.GetValue("uws_prod_selection", "show_type", Me.block_id, "rec_id"), Integer) = 2) And _
                                                    CType(Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_show", ws_product_filter.category_code, "cat_code"), Boolean))
            Else
                Me.pnl_product_selection.Visible = ((CType(Utilize.Data.DataProcedures.GetValue("uws_prod_selection", "show_type", Me.block_id, "rec_id"), Integer) = 0 Or _
                                                    CType(Utilize.Data.DataProcedures.GetValue("uws_prod_selection", "show_type", Me.block_id, "rec_id"), Integer) = 1) And _
                                                    CType(Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_show", ws_product_filter.category_code, "cat_code"), Boolean))
            End If

            hl_show_more.NavigateUrl = ResolveUrl("~/" & Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", ws_product_filter.category_code + Me.global_ws.Language, "cat_code + lng_code"))
        End If

        If ll_testing Then
            ' Als de module landspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cnt_ass") = True Then
                Dim lc_ass_code As String = Utilize.Data.DataProcedures.GetValue("uws_countries", "ass_code", Me.global_ws.user_information.ubo_customer.field_get("cnt_code"))

                If Not Me.global_ws.user_information.ubo_customer.field_get("ass_code") = "" Then
                    ws_product_filter.extra_where += " and uws_products.prod_code not in (select prod_code from uws_cnt_ass_excl where ass_code = '@lc_ass_code')"
                    qsc_prod_selection.add_parameter("lc_ass_code", lc_ass_code)
                    ws_product_filter.extra_where += " and uws_products.prod_group not in (select grp_code from uws_cnt_ass_excl where ass_code = '@lc_ass_code')"
                    qsc_prod_selection.add_parameter("lc_ass_code", lc_ass_code)
                End If
            End If

            ws_product_filter.include_sub_categories = False

            dt_data_table = ws_product_filter.get_products(Me.global_ws.Language)

            ll_testing = dt_data_table.Rows.Count > 0
        End If

        If ll_testing Then
            Me.rpt_products.DataSource = dt_data_table
            Me.rpt_products.DataBind()
        End If

        If ll_testing Then
            Me.set_style_sheet("uc_product_selection.css", Me)
            Me.set_javascript("uc_product_selection.js", Me)
        End If

        If ll_testing Then
            title_products_selection.Text = ds_prod_selection.Rows(0).Item("title_" & Me.global_ws.Language)
            Dim url As String = ds_prod_selection.Rows(0).Item("prod_label_" & Me.global_ws.Language)
            img_label.ImageUrl = ResolveUrl("~/" & url)
            img_label.Visible = url IsNot ""
        End If

        Me.Visible = ll_testing
    End Sub

    Protected Sub orderbutton_handler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.global_ws.shopping_cart.product_add(CType(sender, button_ext).prd_code, 1, "", "", "")
    End Sub

    Private clearfixcount As Integer = 0
    Private mobile_clearfix As Integer = 0
    Protected Sub rpt_products_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_products.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then

            clearfixcount += item_size_int
            mobile_clearfix += 6

            If clearfixcount = 12 Then
                e.Item.FindControl("ph_clear_fix").Visible = True
                clearfixcount = 0
            End If

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clearfix").Visible = True
                mobile_clearfix = 0
            End If

            Select Case overview_type
                Case control_type.blocks
                    Dim uc_product_block As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_block.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)

                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
                    ph_product_row.Controls.Add(uc_product_block)
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType, Me.pnl_product_selection.ClientID, "show_blocks_custom('#" + Me.pnl_product_selection.ClientID + "');", True)

                Case control_type.list
                    Dim uc_product_row As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)

                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
                    ph_product_row.Controls.Add(uc_product_row)
                Case Else
                    Exit Select
            End Select
        End If
    End Sub
End Class

