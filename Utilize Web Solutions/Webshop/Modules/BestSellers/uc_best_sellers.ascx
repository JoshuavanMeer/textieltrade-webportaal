﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_best_sellers.ascx.vb" Inherits="uc_best_sellers" %>
<div class="uc_best_sellers">
    <h2><utilize:translatetitle runat="server" ID="title_best_sellers" Text="Bestsellers"></utilize:translatetitle></h2>
    <ul>
        <asp:Repeater runat="server" ID="rpt_best_sellers">
            <ItemTemplate>
                <li>
                    <utilize:hyperlink runat="server" ID="hl_product_details" NavigateUrl='<%# Me.ResolveCustomUrl("~/"+ DataBinder.Eval(Container.DataItem, "tgt_url")) %>'>
                        <utilize:label CssClass="row_number" ID="lbl_row_number" runat="server" Text='<%# Me.counter.tostring() + ".&nbsp;" %>'></utilize:label>
                        <utilize:label CssClass="prod_desc" ID="lbl_product_title" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "prod_desc" ) %>'></utilize:label>
                    </utilize:hyperlink>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
   