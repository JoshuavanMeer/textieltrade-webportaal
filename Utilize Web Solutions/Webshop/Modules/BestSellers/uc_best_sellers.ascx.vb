﻿
Partial Class uc_best_sellers
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _counter As Integer = 0
    Friend ReadOnly Property counter As Integer
        Get
            _counter += 1
            Return _counter
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Bouw de query op om de best sellers op te halen
        Dim qsc_best_sellers As New Utilize.Data.QuerySelectClass
        qsc_best_sellers.select_fields = "prod_desc, tgt_url"
        qsc_best_sellers.select_from = "uws_best_sellers"
        qsc_best_sellers.add_join("inner join", "uws_products_tran", "uws_best_sellers.prod_code = uws_products_tran.prod_code")
        qsc_best_sellers.select_where = "uws_products_tran.lng_code = @lng_code and uws_best_sellers.shop_code = @shop_code"
        qsc_best_sellers.select_order = "row_ord"

        qsc_best_sellers.add_parameter("lng_code", Me.global_ws.Language)
        qsc_best_sellers.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())
        ' Sla het resultaat op in de pagina
        Dim dt_data_table As System.Data.DataTable = qsc_best_sellers.execute_query()

        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Controleer of er regels gevonden zijn.
            ll_resume = dt_data_table.Rows.Count > 0

            If Not ll_resume Then
                Me.Visible = False
            End If
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_best_sellers.css", Me)

            Me.rpt_best_sellers.DataSource = dt_data_table
            Me.rpt_best_sellers.DataBind()
        End If
    End Sub
End Class
