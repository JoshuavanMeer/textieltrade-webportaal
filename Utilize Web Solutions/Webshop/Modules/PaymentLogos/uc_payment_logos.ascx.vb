﻿
Partial Class uc_payment_logos
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim llResume As Boolean = Me.Visible

        If llResume Then
            llResume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_pay_logo")
        End If

        Dim dt_data_table As System.Data.DataTable = Nothing

        If llResume Then
            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "uws_pay_logo"
            qsc_data_table.select_order = "row_ord"
            dt_data_table = qsc_data_table.execute_query()

            llResume = dt_data_table.Rows.Count > 0
        End If

        If llResume Then
            Me.set_style_sheet("uc_payment_logos.css", Me)

            Me.rpt_payment_logos.DataSource = dt_data_table
            Me.rpt_payment_logos.DataBind()
        End If

        Me.Visible = llResume
    End Sub
End Class
