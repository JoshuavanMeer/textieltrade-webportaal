﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_payment_logos.ascx.vb" Inherits="uc_payment_logos" %>
<div class="block uc_payment_logos">
    <h2><utilize:translatetitle runat="server" ID="title_payment_logos" Text="Betalingsmogelijkheden"></utilize:translatetitle></h2>
    <ul>
        <asp:Repeater runat="server" ID="rpt_payment_logos">
            <ItemTemplate>
                <li>
                    <utilize:image runat="server" ImageUrl='<%#"~/" + DataBinder.Eval(Container.DataItem, "logo_image")%>' ToolTip='<%#  DataBinder.Eval(Container.DataItem, "logo_desc_" + Me.global_cms.Language)%>' AlternateText='<%#  DataBinder.Eval(Container.DataItem, "logo_desc_" + Me.global_cms.Language)%>' />
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>