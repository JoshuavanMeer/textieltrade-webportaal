﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_customer_review.ascx.vb" Inherits="uc_customer_review" %>

<div class="uc_customer_review">
    <h2><utilize:translatetitle runat="server" ID="title_customer_review" Text="Klanten beoordelingen"></utilize:translatetitle></h2>
    <div class="reviewedwrapper">
    <utilize:placeholder runat="server"  ID="ph_reviews">
        <utilize:repeater runat="server" ID="rpt_reviews">
            <ItemTemplate>
                <div class="product-comment margin-bottom-40">
                    <div class="product-comment-in">
                        <i class="product-comment-icon icon-custom icon-lg rounded-x icon-line icon-user"></i>
                        <div class="product-comment-dtl">
                            <h4><utilize:literal runat='server' ID="lt_cust_name" Text='<%# DataBinder.Eval(Container.DataItem, "cust_name")%>'></utilize:literal></h4>
                            <utilize:label runat='server' ID="lt_cust_comments" CssClass="comments" Text='<%# DataBinder.Eval(Container.DataItem, "cust_comments").replace(chr(10), "<br />")%>'></utilize:label>
                            <ul class="list-inline product-ratings">
                                <li class="pull-right">
                                    <div class="star_wrapper">
                                        <input runat='server' id="star1" name="star1" type="radio" class="star" disabled="disabled"/>
                                        <input runat='server' id="star2" name="star1" type="radio" class="star" disabled="disabled"/>
                                        <input runat='server' id="star3" name="star1" type="radio" class="star" disabled="disabled"/>
                                        <input runat='server' id="star4" name="star1" type="radio" class="star" disabled="disabled"/>
                                        <input runat='server' id="star5" name="star1" type="radio" class="star" disabled="disabled"/>
                                    </div>
                                </li>    
                            </ul>
                        </div>
                    </div>    
                </div>
            </ItemTemplate>
        </utilize:repeater>
    </utilize:placeholder>


    <utilize:placeholder runat="server" id="ph_no_reviews"  Visible="false">
        <utilize:translatetext runat="server" ID="text_no_reviews_given" Text="Er zijn op dit moment nog geen beoordelingen door klanten geschreven voor dit product."></utilize:translatetext>
    </utilize:placeholder>

    <div class="buttons"><utilize:translatebutton runat="server" ID="button_give_review" Text="Plaats review" CssClass="btn-u btn-u-sea-shop btn-u-lg" /></div>
    </div>

    <utilize:panel runat="server" ID="pnl_review" cssclass="review_wrapper" Visible="false">
        <utilize:placeholder runat='server' ID="pnl_review_post">

            <div class="sky-form sky-changes-4">
                <fieldset>
                    <div class="margin-bottom-30">
                        <utilize:translatelabel runat="server" id="label_your_name" Text="Uw naam" CssClass="label-v2"></utilize:translatelabel><span class="color-red mandatory_sign"> *</span>
                        <label class="input">
                            <Utilize:TextBox ID="txt_customer_name" CssClass="text_give_your_name" runat="server"></Utilize:TextBox>
                        </label>
                    </div>    
                            
                    <div class="margin-bottom-30">
                        <utilize:translatelabel runat="server" id="label_give_your_review" Text="Geef uw beoordeling" CssClass="label-v2"></utilize:translatelabel><span class="color-red mandatory_sign"> *</span>
                        <label class="textarea">
                            <utilize:textarea id="txt_customer_comments" runat="server" CssClass="text_area_give_your_review" rows="7"></utilize:textarea>
                        </label>
                    </div>    
                </fieldset>    
                            
                <footer class="review-submit">
                    <utilize:translatelabel runat="server" id="label_your_rating" Text="Aantal sterren" CssClass="label-v2"></utilize:translatelabel>
                    <div class="star_wrapper2 stars-rating">
                            <input runat='server' id="star1" name="star1" type="radio" class="star"/>
                            <input runat='server' id="star2" name="star1" type="radio" class="star"/>
                            <input runat='server' id="star3" name="star1" type="radio" class="star"/>
                            <input runat='server' id="star4" name="star1" type="radio" class="star"/>
                            <input runat='server' id="star5" name="star1" type="radio" class="star"/>
                        </div>
                    <div class="buttons pull-right">
                        <utilize:translatebutton ID="button_send_review" CssClass="btn-u btn-u-sea-shop btn-u-sm pull-right" runat="server" Text="Plaats review" />
                    </div>
                </footer>
            </div>


            
            <utilize:translatemessage runat='server' ID="message_mandatory_fields_not_filled" Visible="false" Text="Een of meer verplichte velden zijn niet gevuld." CssClass="message"></utilize:translatemessage>

        </utilize:placeholder>
        <utilize:placeholder runat='server' ID="pnl_review_posted" Visible="false">
            <utilize:translatetext runat='server' ID="text_review_posted" Text="Uw review is verstuurd."></utilize:translatetext>
        </utilize:placeholder>
        <asp:HiddenField runat='server' ID="hih_review_visible" Value="" />
    </utilize:panel>
    </div>