﻿
Partial Class uc_customer_review
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(value As String)
            _product_code = value

            Me.DisplayReviews()
        End Set
    End Property

    Protected Sub DisplayReviews()
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not _product_code = ""

            If Not ll_resume Then
                Me.Visible = False
            End If
        End If

        If ll_resume Then
            Me.set_javascript("uc_customer_review.js", Me)
            Me.set_style_sheet("uc_customer_review.css", Me)
        End If

        If ll_resume Then

            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "uws_product_reviews"
            qsc_data_table.select_order = "prod_code"
            qsc_data_table.select_where = "prod_code = @prod_code and review_show = 1 and shop_code = @shop_code"
            qsc_data_table.add_parameter("prod_code", product_code)
            qsc_data_table.add_parameter("shop_code", Me.global_ws.shop.get_shop_code())

            Dim dt_data_table As System.Data.DataTable = qsc_data_table.execute_query()

            Me.rpt_reviews.DataSource = dt_data_table
            Me.rpt_reviews.DataBind()
        End If

        If ll_resume Then
            Me.ph_no_reviews.Visible = Me.rpt_reviews.Items.Count = 0
            Me.ph_reviews.Visible = Not Me.rpt_reviews.Items.Count = 0
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack() And Me.Visible Then
            Dim lc_script As String = "$('input[type=radio].star').rating();"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "review", lc_script, True)
        End If

        Me.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_review")

    End Sub

    Private Function check_mandatory_fields() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.txt_customer_name.Text = ""
        End If

        If ll_resume Then
            ll_resume = Not Me.txt_customer_comments.Text = ""
        End If

        Return ll_resume
    End Function
    Protected Sub button_send_review_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_send_review.Click
        Me.button_give_review.Visible = False
        Dim ll_resume As Boolean = True
        Dim ln_rating As Integer = 0

        If ll_resume Then
            ll_resume = Me.check_mandatory_fields()

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_resume Then
            Select Case True
                Case Me.star1.Checked
                    ln_rating = 1
                Case Me.star2.Checked
                    ln_rating = 2
                Case Me.star3.Checked
                    ln_rating = 3
                Case Me.star4.Checked
                    ln_rating = 4
                Case Me.star5.Checked
                    ln_rating = 5
                Case Else
                    Exit Select
            End Select

            ll_resume = Not ln_rating = 0

            If Not ll_resume Then
                Me.message_mandatory_fields_not_filled.Visible = True
            End If
        End If

        If ll_resume Then
            ' Voeg hier de review toe aan de database
            Dim ubo_bus_obj As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_product_reviews")
            ubo_bus_obj.table_insert_row("uws_product_reviews", Me.global_ws.user_information.user_id)
            ubo_bus_obj.field_set("prod_code", product_code)
            ubo_bus_obj.field_set("cust_name", Me.txt_customer_name.Text.Trim())
            ubo_bus_obj.field_set("cust_comments", Me.txt_customer_comments.Text.Trim())
            ubo_bus_obj.field_set("prod_rating", ln_rating)
            ubo_bus_obj.table_update(Me.global_ws.user_information.user_id)

            Dim body_customer As String = "Review van product: " + product_code + ".<br/>"
            body_customer &= "Waardering:" & ln_rating & "<br/>"
            body_customer &= "Naam van de klant:" & Me.txt_customer_name.Text & "<br/>"
            body_customer &= "Bericht:" & Me.txt_customer_comments.Text & "<br/>"


            Dim mail As New SendMail
            mail.mail_subject = "Klant beoordeling - product: " + product_code
            mail.mail_body = body_customer

            mail.send_mail()

            Me.pnl_review_post.Visible = False
            Me.pnl_review_posted.Visible = True
        End If
    End Sub

    Protected Sub rpt_reviews_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_reviews.ItemDataBound
        Dim ln_rate As Integer = e.Item.DataItem("prod_rating")

        Select Case ln_rate
            Case 1
                Dim lo_input As HtmlInputRadioButton = e.Item.FindControl("star1")
                lo_input.Checked = True
            Case 2
                Dim lo_input As HtmlInputRadioButton = e.Item.FindControl("star2")
                lo_input.Checked = True
            Case 3
                Dim lo_input As HtmlInputRadioButton = e.Item.FindControl("star3")
                lo_input.Checked = True
            Case 4
                Dim lo_input As HtmlInputRadioButton = e.Item.FindControl("star4")
                lo_input.Checked = True
            Case 5
                Dim lo_input As HtmlInputRadioButton = e.Item.FindControl("star5")
                lo_input.Checked = True
            Case Else
                Exit Select
        End Select
    End Sub

    Private Sub button_give_review_Click(sender As Object, e As EventArgs) Handles button_give_review.Click
        Me.pnl_review.Visible = True
        Me.button_give_review.Visible = False
    End Sub
End Class
