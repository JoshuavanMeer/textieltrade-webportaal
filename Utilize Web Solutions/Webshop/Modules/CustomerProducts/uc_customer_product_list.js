﻿function setOverview() {
    if (sessionStorage.getItem("view_state") == null) {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("onload_state") == "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("onload_state") == "list") {
                show_list();
            }
        }
    } else {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("view_state") == "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("view_state") == "list") {
                show_list();
            }
            if (sessionStorage.getItem("view_state") == "table") {
                show_table();
            }
        }
    }
}

function set_start() {
    $(".uc_customer_product_list .uc_placeholder").removeClass().addClass("uc_placeholder");

    $(".uc_customer_product_list .image-placeholder").removeClass().addClass("image-placeholder img-responsive");

    $(".uc_customer_product_list .image-block").css("display", "block");
    $(".uc_customer_product_list .item-border").css("display", "block");


    $(".uc_customer_product_list .image-block").removeClass().addClass("image-block");

    $(".uc_customer_product_list .information-block").removeClass().addClass("information-block");


    $(".uc_customer_product_list .item-border").removeClass().addClass("item-border");
    $(".uc_customer_product_list .containter-placeholder").removeClass().addClass("containter-placeholder");
    $(".uc_customer_product_list .row-placeholder").removeClass().addClass("row-placeholder");

    $(".uc_customer_product_list .list-only").css("display", "block");

    $(".fa-th").removeClass("active");
    $(".fa-bars").removeClass("active");
    $(".fa-th-list").removeClass("active");
}

function show_list() {
    set_start();
    var size = $(".uc_customer_product_list .hidden_item_size").val();
    $(".uc_customer_product_list .uc_placeholder").addClass("uc_product_row col-xs-6 col-md-12 col-sm-12");
    $(".uc_customer_product_list .uc_product_block").removeClass("col-xs-6 col-sm-6 uc_product_block " + size);

    $(".uc_customer_product_list .image-placeholder").addClass("sm-margin-bottom-20");

    $(".uc_customer_product_list .image-block").addClass("col-sm-4");

    $(".uc_customer_product_list .information-block").addClass("col-sm-8");


    $(".uc_customer_product_list .content-placeholder").addClass("content col-xs-12");
    $(".uc_customer_product_list .content-placeholder").removeClass("illustration-v2 row");

    $(".uc_customer_product_list .item-border").addClass("list-product-description product-description-brd margin-bottom-30");
    $(".uc_customer_product_list .containter-placeholder").addClass("container-fluid");
    $(".uc_customer_product_list .row-placeholder").addClass("row");

    $(".uc_customer_product_list .product_info_order").css("display", "none");

    $(".fa-th-list").addClass("active");

    sessionStorage.setItem("view_state", "list");
}

function show_blocks(mobile) {
    mobile = mobile || false;
    set_start();
    var size = $(".uc_customer_product_list .hidden_item_size").val();
    $(".uc_customer_product_list .uc_placeholder").addClass("col-xs-6 col-sm-6 uc_product_block " + size);

    $(".uc_customer_product_list .image-placeholder").addClass("full-width");

    $(".uc_customer_product_list .image-block").addClass("product-img product-img-brd");

    $(".uc_customer_product_list .information-block").addClass("product-description-brd margin-bottom-30");

    $(".uc_customer_product_list .content-placeholder").addClass("illustration-v2 col-xs-12");

    $(".uc_customer_product_list .list-only").css("display", "none");

    $(".fa-th").addClass("active");
    if (!mobile) {
        sessionStorage.setItem("view_state", "blocks");
    }
}

function show_table() {
    set_start();
    var size = $(".uc_customer_product_list .hidden_item_size").val();
    $(".uc_customer_product_list .uc_placeholder").addClass("uc_product_table");

    $(".uc_customer_product_list .item-border").css("display", "none")
    $(".uc_customer_product_list .uc_product_block").removeClass("uc_product_block " + size);
    $(".uc_customer_product_list .content-placeholder").removeClass("illustration-v2 row");

    $(".fa-bars").addClass("active");

    sessionStorage.setItem("view_state", "table");
}

function set_blocks() {
    sessionStorage.setItem("onload_state", "blocks");
    setOverview();
}

function set_list() {
    sessionStorage.setItem("onload_state", "list");
    setOverview();
}

$(window).resize(function () {
    if ($(window).width() < 991) {
        show_blocks(true);
    } else {
        setOverview();
    }
});