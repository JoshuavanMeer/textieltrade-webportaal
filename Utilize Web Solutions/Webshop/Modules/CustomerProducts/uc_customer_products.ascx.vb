﻿
Partial Class uc_customer_products
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private brand_code As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Dit blok is alleen zichtbaar wanneer de gebruiker is ingelogd
            ll_resume = Me.global_ws.user_information.user_logged_on
        End If

        If ll_resume Then
            ' Dit blok is alleen zichtbaar wanneer er klantspecifieke producten zijn gekoppeld bij de klant
            ll_resume = Me.global_ws.user_information.ubo_customer.data_tables("uws_customer_product").data_row_count > 0
        End If

        If ll_resume Then
            Me.link_customer_products.NavigateUrl = Me.Page.ResolveUrl("~/" + Me.global_ws.Language + "/9/customerproducts.aspx")

            Me.set_style_sheet("uc_customer_products.css", Me)
        End If

        Me.Visible = ll_resume
    End Sub
End Class
