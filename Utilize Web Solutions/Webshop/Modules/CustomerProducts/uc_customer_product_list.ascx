﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_customer_product_list.ascx.vb" Inherits="uc_customer_product_list" %>

<div class="uc_customer_product_list">
    <h1><utilize:translatetitle runat="server" id="title_customer_products" text="Uw producten"></utilize:translatetitle></h1>
    <utilize:placeholder runat="server" ID="ph_product_list">
        <utilize:translatetext runat="server" ID="text_customer_products_overview" text="Hieronder vindt u een overzicht van uw producten"></utilize:translatetext>

        <div class="row">
            <div class="col-md-12">
                <!-- Pager 1 -->
                <utilize:placeholder runat='server' ID="ph_pager1"> 
                    <div class="text-center pagingwrapper top-pagination">
                        <ul class="pagination">
                            <li><utilize:HyperLink ID="hl_pl_previous1" CssClass="previous" runat="server"></utilize:HyperLink></li>
                            <utilize:literal runat='server' ID="lt_pager_1"></utilize:literal>
                            <li><utilize:HyperLink ID="hl_pl_next1" CssClass="next" runat="server"></utilize:HyperLink></li>
                        </ul>
                    </div>
                </utilize:placeholder>

                <!-- Sort and pagesize 1 -->
                <div class="pages">            
                    <input type="hidden" runat="server" id="hidden_item_size" class="hidden_item_size" value=""/>                       
                    <ul class="list-inline clear-both">                
                        <li class="grid-list-icons margin-bottom-10 hidden-sm hidden-xs ">     
                            <utilize:label runat="server" ID="test"></utilize:label>               
                            <a onclick="show_list(); return false;"><i class="fa fa-th-list"></i></a>
                            <a onclick="show_blocks(); return false;"><i class="fa fa-th"></i></a>
                            <a onclick="show_table(); return false;"><i class="fa fa-bars"></i></a>
                        </li> 
                        <li class="sort-list-btn">
                            <h3><utilize:label runat="server" CssClass="page_size" ID="label_items_per_page1"></utilize:label></h3>
                            <div class="btn-group">
                                <utilize:dropdownlist runat='server' CssClass="cbo_items_per_page btn btn-default dropdown-toggle" ID="cbo_items_per_page1" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>
                        <li class="sort-list-btn">
                            <h3><utilize:label runat="server" CssClass="product_sort" ID="label_sort_by1"></utilize:label></h3>
                            <div class="btn-group">
                                <utilize:dropdownlist runat='server' CssClass="cbo_sort_by btn btn-default dropdown-toggle" ID="cbo_sort_by1" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>               
                    </ul>
                </div>
            </div>
        </div>

        <!-- content -->
        <div class="row">
            <div class="content content-placeholder">
                <div class="row">
                    <asp:Repeater runat="server" ID="rpt_product_list">
                        <ItemTemplate>
                            <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_clear_fix" Visible="false">
                                <div class="clearfix"></div>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_mobile_clear_fix" Visible="false">
                                <div class="mobile-clearfix"></div>
                            </utilize:placeholder>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>

        <!-- Bulk order -->
        <utilize:placeholder runat="server" id="ph_bulk_order">
            <div class="buttons">
                <utilize:translatebutton ID="button_bulk_order" CssClass="add_to_basket right" runat="server" Text="Bestellen" />
            </div>
        </utilize:placeholder>

        <div class="row">
             <div class="col-md-12">
                 <div class="prodcountwrapper text-center">
                    <span class="showing_count"><utilize:literal runat='server' ID="lt_product_count1"></utilize:literal></span>
                </div>   
                <div class="pages">
                    <ul class="list-inline clear-both">
                        <li class="sort-list-btn">
                            <div class="productpagesize">
                                <utilize:label runat="server" CssClass="page_size" ID="label_items_per_page2"></utilize:label><utilize:dropdownlist runat='server' CssClass="cbo_items_per_page" ID="cbo_items_per_page2" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>
                        <li class="sort-list-btn">
                            <div class="productsort">
                                <utilize:label runat="server" CssClass="product_sort" ID="label_sort_by2"></utilize:label><utilize:dropdownlist runat='server' CssClass="cbo_sort_by" ID="cbo_sort_by2" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <!-- Showing count -->
                   

                <!-- Pager 2 -->
                <utilize:placeholder runat='server' ID="ph_pager2">
                        <div class="text-center pagingwrapper bottom-pagination">
                        <ul class="pagination">
                            <li><utilize:HyperLink ID="hl_pl_previous2" CssClass="previous" runat="server"></utilize:HyperLink></li>
                            <utilize:literal runat='server' ID="lt_pager_2"></utilize:literal>
                            <li><utilize:HyperLink ID="hl_pl_next2" CssClass="next" runat="server"></utilize:HyperLink></li>
                        </ul>
                    </div>
                </utilize:placeholder>      
            </div>
        </div>

        <!-- Never shown -->
        <utilize:label runat='server' ID="lbl_raw_url" Visible='false'></utilize:label>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_no_items">
        <div class="no_products_found">
            <utilize:translatetext runat='server' ID="text_no_customer_products" Text="Er zijn geen producten beschikbaar."></utilize:translatetext>
        </div>
    </utilize:placeholder>
</div>