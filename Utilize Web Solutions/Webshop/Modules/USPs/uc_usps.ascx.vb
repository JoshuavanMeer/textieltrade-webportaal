﻿
Partial Class uc_usps
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim llResume As Boolean = Me.Visible

        Dim qsc_data_table As Utilize.Data.QuerySelectClass = Nothing
        Dim dt_data_table As System.Data.DataTable = Nothing

        If llResume Then
            Me.set_style_sheet("uc_usps.css", Me)

            llResume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_usps")
        End If

        If llResume Then
            qsc_data_table = New Utilize.Data.QuerySelectClass
            dt_data_table = New System.Data.DataTable
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "uws_usp"
            qsc_data_table.select_order = "row_ord"
            dt_data_table = qsc_data_table.execute_query()
        End If

        If llResume Then
            Me.rpt_usps.DataSource = dt_data_table
            Me.rpt_usps.DataBind()
        End If

        Me.Visible = llResume
    End Sub
End Class
