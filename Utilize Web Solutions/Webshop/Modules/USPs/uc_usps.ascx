﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_usps.ascx.vb" Inherits="uc_usps" %>

<div class="uc_usps">
    <h2><utilize:translatetitle runat="server" ID="title_usps" Text="USP's"></utilize:translatetitle></h2>
    <ul>
        <asp:Repeater runat="server" ID="rpt_usps">
            <ItemTemplate>
                <li>
                    <span class="usp_text"><i class="fa fa-check"></i><utilize:literal ID="Literal1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "usp_desc_" + Me.global_cms.Language)%>'></utilize:literal></span>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>