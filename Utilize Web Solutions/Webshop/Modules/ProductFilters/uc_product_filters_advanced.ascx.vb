﻿Imports System.Data

Partial Class uc_product_filters_advanced
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private pc_product_code As String = ""
    Private pc_product_set_code As String = ""
    Private pc_product_set_description As String = ""
    Private pc_language As String = Me.global_ws.Language

    Private pdt_current_product_specs As DataTable = Nothing
    Private pdt_product_specs As DataTable = Nothing

    Private pph_placeholder As placeholder = Nothing
    Private pls_added_specs As New List(Of String)()
    Private pls_spec_controls As New List(Of String)

    Private pl_controls_added As Boolean = False
    Private uc_order_product As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_product_set_price As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    ''' <summary>
    ''' De product code wordt gezet op de pagina waar de user control wordt geladen
    ''' </summary>
    ''' <returns></returns>
    Public Property product_code As String
        Get
            Return pc_product_code
        End Get
        Set(ByVal value As String)
            pc_product_code = value
        End Set
    End Property

    Private Sub add_user_controls()
        ' Als de controls nog niet toegevoegd zijn, dan moet de usercontrol geladen worden
        If Not Me.pl_controls_added Then
            uc_order_product = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
            uc_product_set_price = Me.LoadUserControl("~/Webshop/Modules/ProductFilters/uc_product_set_price.ascx")
        End If

        uc_order_product.set_property("product_code", Me.product_code)
        uc_order_product.set_property("product_set_code", pc_product_set_code)
        uc_product_set_price.set_property("product_code", Me.product_code)
        uc_product_set_price.set_property("product_set_code", pc_product_set_code)

        If Not Me.pl_controls_added Then
            Me.ph_order_product.Controls.Add(uc_order_product)
            Me.ph_product_set_price.Controls.Add(uc_product_set_price)

            Me.pl_controls_added = True
        End If
    End Sub

    ''' <summary>
    ''' Afhankelijk van het type moeten verschillende id's en velden worden toegekend en gebruikt
    ''' </summary>
    Private Enum ItemType As Integer
        Products = 1
        OptionalProducts = 2
        Specifications = 3
    End Enum

    'Friend Class SpecDropdownList
    '    Inherits dropdownlist

    '    Public Voorbeeld As String = ""
    'End Class

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_product_filters_advanced.css", Me)

        Dim ll_resume As Boolean = True

        ' Controleer of het huidige product een hoofdproduct in een producten reeks zit
        ' Ook worden de product reeks code en omschrijving in publieke variabelen opgeslagen
        If ll_resume Then
            ll_resume = Me.check_main_product_in_product_set()
        End If

        If ll_resume Then
            Me.add_user_controls()
        End If

        ' Het product is een hoofdproduct in een reeks
        If ll_resume Then
            ' Zet de omschrijving van de product set als titel
            Me.lt_title.Text = pc_product_set_description

            ' Hier worden de elementen toegevoegd aan de control
            Me.add_elements_to_the_page()
        End If

        ' Toon de control alleen als het hoofdproduct in een product reeks zit
        Me.Visible = ll_resume
    End Sub

    ''' <summary>
    ''' Controleer hier of het product een hoofdproduct in een producten reeks is
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function check_main_product_in_product_set() As Boolean
        ' Ophalen van de code en omschrijving van de reeks waar het product in zit
        Dim qsc_product_set_products As New Utilize.Data.QuerySelectClass
        qsc_product_set_products.select_fields = "set_code, set_desc_" + pc_language + " as set_desc"
        qsc_product_set_products.select_from = "uws_product_set_prod"
        qsc_product_set_products.select_where = "main_product = 1 and prod_code = @prod_code"
        qsc_product_set_products.add_parameter("prod_code", pc_product_code)
        qsc_product_set_products.add_join("left join", "uws_product_sets", "uws_product_set_prod.set_code = uws_product_sets.rec_id")

        Dim dt_product_set_products As New DataTable
        dt_product_set_products = qsc_product_set_products.execute_query()

        If dt_product_set_products.Rows.Count > 0 Then
            ' Opslaan van de product reeks code
            pc_product_set_code = dt_product_set_products.Rows(0).Item("set_code")
            ' Opslaan omschrijving van de product reeks
            pc_product_set_description = dt_product_set_products.Rows(0).Item("set_desc")
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Deze functie voegt alle elementen toe aan de pagina
    ''' </summary>
    Private Sub add_elements_to_the_page()
        ' Hier worden de producten toegevoegd aan de control

        write_output(DateTime.Now.ToString(), False)
        write_output("Product code: " + pc_product_code)
        write_output("Product set code: " + pc_product_set_code)
        write_output("Product set omschrijving: " + pc_product_set_code)
        write_output("Taal: " + pc_language)

        ' Haal de producten in de product reeks op
        Dim dt_product_set_products As DataTable = Me.get_product_in_product_set()

        ' Producten met dezelfde product set type worden gegroepeerd in een dropdown
        ' Hier worden de unieke product typen opgehaald
        Dim dt_distinct_product_types As DataTable = dt_product_set_products.DefaultView.ToTable(True, "prod_type")

        ' Controleer of de module: Specificaties beschikbaar is
        Dim ll_specifications_module_active As Boolean = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_spec")

        ' Variabelen voor het wel/niet tonen van de placeholder voor optinele producten
        Dim ll_optional_products_available As Boolean = False

        ' Loop door elke type heen
        Dim i As String = 0
        For Each dr_product_type As DataRow In dt_distinct_product_types.Rows
            i += 1
            ' Opslaan van de product reeks type
            Dim lc_product_type As String = dr_product_type.Item("prod_type")
            write_output("Product type [" + i.ToString + "]: " + lc_product_type)

            ' Filter alleen de producten van het huidige type
            Dim dr_products() As DataRow = dt_product_set_products.Select(String.Format("prod_type = '{0}' and prod_optional = 0", lc_product_type))

            ' Als er producten zijn gevonden
            If dr_products.Length > 0 Then
                Dim ll_main_product As Boolean = dr_products(0).Item("main_product")

                ' Voeg de producten toe aan de control
                pph_placeholder = Me.ph_product_set_products
                Me.create_items_and_add_to_control(lc_product_type, dr_products, ItemType.Products, ll_main_product)

                ' Hier worden de specificaties voor de verplichte producten toegevoegd
                If ll_specifications_module_active Then
                    Me.add_product_specifications(dr_products, ll_main_product)
                End If
            End If

            ' Filter alleen de producten van het huidige type die optioneel zijn
            Dim dr_optional_products() As DataRow = dt_product_set_products.Select(String.Format("prod_type = '{0}' and prod_optional = 1", lc_product_type))

            ' Als er optionele producten zijn gevonden
            If dr_optional_products.Length > 0 Then
                ll_optional_products_available = True

                ' Voeg de optionele producten toe aan de control
                pph_placeholder = Me.ph_product_set_optional_products
                Me.create_items_and_add_to_control(lc_product_type, dr_optional_products, ItemType.OptionalProducts)

                ' Hier worden de specificaties voor de optionele producten toegevoegd
                If ll_specifications_module_active Then
                    Me.add_product_specifications(dr_optional_products)
                End If
            End If
        Next

        ' Allleen tonen van de optionele placeholder als er optionele producten zijn
        Me.ph_product_set_optional_products.Visible = ll_optional_products_available
    End Sub

    ''' <summary>
    ''' Ophalen van de producten die in de product reeks zitten
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function get_product_in_product_set() As DataTable
        Dim dt_product_set_products As New DataTable
        Dim qsc_product_set_products As New Utilize.Data.QuerySelectClass

        ' Ophalen van de code van de reeks waar het product in zit
        qsc_product_set_products.select_fields = "uws_product_set_prod.*, uws_products_tran.prod_desc, uws_product_set_type.type_desc_" + pc_language + " as prod_type_desc, uws_products_tran.tgt_url"
        qsc_product_set_products.select_from = "uws_product_set_prod"
        qsc_product_set_products.select_where = "set_code = @set_code and uws_products_tran.lng_code = @lng_code and prod_show = 1"
        qsc_product_set_products.select_order = "main_product DESC, row_ord"

        qsc_product_set_products.add_parameter("set_code", pc_product_set_code)
        qsc_product_set_products.add_parameter("lng_code", pc_language)

        qsc_product_set_products.add_join("left join", "uws_products", "uws_product_set_prod.prod_code = uws_products.prod_code")
        qsc_product_set_products.add_join("left join", "uws_products_tran", "uws_product_set_prod.prod_code = uws_products_tran.prod_code")
        qsc_product_set_products.add_join("left join", "uws_product_set_type", "uws_product_set_prod.prod_type = uws_product_set_type.type_code")

        dt_product_set_products = qsc_product_set_products.execute_query()

        Return dt_product_set_products
    End Function

    ''' <summary>
    ''' Deze functie maakt de teksten en dropdown's aan voor de specificaties van de hoofdproducten, de producten in de product reeks en voor optionele producten
    ''' </summary>
    ''' <param name="product_type"></param>
    ''' <param name="dr"></param>
    ''' <param name="item_type"></param>
    ''' <param name="main_product"></param>
    Private Sub create_items_and_add_to_control(ByVal product_type As String, ByVal dr() As DataRow, ByVal item_type As ItemType, Optional ByVal main_product As Boolean = False)
        write_output("Add items to control")
        write_output("- prod_type: " + product_type)
        write_output("- item_type: " + item_type.ToString())
        write_output("- main_product: " + main_product.ToString())

        Dim ll_resume As Boolean = True

        Dim lc_description As String = ""
        Dim lc_text_field As String = "prod_desc"
        Dim lc_value_field As String = "prod_code"
        Dim lc_add_to_id As String = ""

        If ll_resume Then
            ' Type omzetten naar kleine letters en wijzig spaties naar underscores
            ' Het type wordt gebruikt als id voor het element
            lc_add_to_id = product_type.ToLower().Replace(" ", "_")

            Dim lc_description_field As String = "prod_type_desc"

            Select Case item_type
                Case ItemType.OptionalProducts
                    lc_add_to_id += "_optional"
                Case ItemType.Specifications
                    lc_add_to_id += "_spec"
                    lc_description_field = "spec_desc"
                    lc_text_field = "val_desc"
                    lc_value_field = "val_code"
                Case ItemType.Products
                    Exit Select
                Case Else
                    Exit Select
            End Select

            lc_description = dr(0).Item(lc_description_field)
        End If

        ' Als een product in een reeks een specificatie heeft en het volgende product heeft dezelfde dan moeten de volgende niet getoond worden
        If ll_resume Then
            If item_type = ItemType.Specifications Then
                ll_resume = Not pls_added_specs.Contains(product_type)

                If Not ll_resume Then
                    write_output("Specification is present in the control and is not added again")
                End If

                If ll_resume Then
                    ' Toevoegen product type of specificatie code aan de lijst zodat deze niet nogmaals wordt toegevoegd
                    pls_added_specs.Add(product_type)
                End If
            End If
        End If

        If ll_resume Then
            If item_type = ItemType.Products Then
                ll_resume = Not main_product
            End If
        End If

        If ll_resume Then
            ' Aanmaken van de elementen
            Dim id As String = "type_" + lc_add_to_id
            Dim hgc_span As HtmlGenericControl = Me.create_literal_in_span(id, lc_description)
            Dim ddl As dropdownlist = Me.create_dropdownlist(id, dr, lc_text_field, lc_value_field, main_product)
            'ddl.Voorbeeld = "Voorbeeld " + lc_value_field

            AddHandler ddl.SelectedIndexChanged, AddressOf ddl_SelectedIndexChanged

            ' Aanmaken van de lijst voor het huidige type
            Dim ul As New HtmlGenericControl("div")
            ul.ID = "ul_" + lc_add_to_id
            ul.Attributes.Add("class", "row")

            ' Toevoegen van de text items aan de lijst
            Dim li_text = Me.add_text_item_to_list(hgc_span)
            ul.Controls.Add(li_text)

            ' Toevoegen van de value items aan de lijst
            Dim li_value = Me.add_value_item_to_list(ddl)
            ul.Controls.Add(li_value)

            ' Toevoegen van de lijst aan de control
            pph_placeholder.Controls.Add(ul)

            If item_type = ItemType.Specifications Then
                pls_spec_controls.Add(ddl.ID)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Aanmaken van een literal in een span
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="text"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function create_literal_in_span(ByVal id As String, ByVal text As String) As HtmlGenericControl
        write_output("Create literal In span")
        write_output("- id: " + id)
        write_output("- text: " + text)

        Dim lt As New literal
        lt.Text = text + ":"

        Dim hgc_span As New HtmlGenericControl("span")
        hgc_span.ID = "label_" + id
        hgc_span.Attributes.Add("class", "text")
        hgc_span.Controls.Add(lt)

        Return hgc_span
    End Function

    ''' <summary>
    ''' Aanmaken van een dropdownlist met waarden uit een datarow collectie
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="products"></param>
    ''' <param name="text_field"></param>
    ''' <param name="value_field"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function create_dropdownlist(ByVal id As String, ByVal products() As DataRow, ByVal text_field As String, ByVal value_field As String, Optional ByVal main_product As Boolean = False) As dropdownlist
        write_output("Create dropdownlist")
        write_output("- id: " + id)

        Dim ll_optional_products As Boolean = id.EndsWith("_optional")

        Dim ddl As New dropdownlist

        ddl.ID = "cb_" + id
        ddl.AutoPostBack = True

        ' Staat op true als het een product specificatie van een hoofdproduct is
        ' Wordt gebruikt om in de ddl_SelectedIndexChanged naar een ander product te redirecten
        ddl.Attributes.Add("data-main-spec", main_product.ToString())
        ' Zodat de dropdown bootstrap layout krijgt
        ddl.Attributes.Add("class", "form-control")

        Dim lo_added_values As New List(Of String)()
        Dim i As Integer = 0
        Dim ll_product_selected As Boolean = False

        For Each dr As DataRow In products
            i += 1
            Dim lc_text As String = dr.Item(text_field).ToString.Trim()
            Dim lc_value As String = dr.Item(value_field).ToString.Trim().ToUpper()
            Dim lc_product_code As String = dr.Item("prod_code").ToString.Trim()
            Dim lc_tgt_url As String = dr.Item("tgt_url").ToString.Trim()
            Dim lc_spec_code As String = ""
            If id.EndsWith("_spec") Then
                lc_spec_code = dr.Item("spec_code").ToString()
            End If
            ddl.Attributes.Add("data-spec-code", lc_spec_code)

            write_output("Item [" + i.ToString() + "]")
            write_output("- text: " + lc_text)
            write_output("- value: " + lc_value)
            write_output("- prod_code: " + lc_product_code)

            Dim ll_resume As Boolean = Not lo_added_values.Contains(lc_value)

            If Not ll_resume Then
                write_output("Item is present in dropdownlist is not added again")
            End If

            If ll_resume Then
                write_output("Add listitem to dropdownlist")

                Dim li As New System.Web.UI.WebControls.ListItem

                li.Text = lc_text
                li.Value = dr.Item(value_field)
                li.Attributes.Add("data-prod-code", lc_product_code)
                li.Attributes.Add("data-spec-code", lc_spec_code)

                'If dr.Item("available") = "0" Then
                '    li.Attributes.Add("disabled", "disabled")
                'End If

                Dim ll_select_li As Boolean = False
                If String.IsNullOrEmpty(lc_spec_code) Then
                    If Not Me.check_enabled_product(lc_product_code) Then
                        li.Attributes.Add("disabled", "disabled")
                    ElseIf Not ll_optional_products Then
                        ll_select_li = True
                    End If
                Else
                    Me.check_enabled(li)

                    If lc_product_code = pc_product_code Then
                        write_output("Product code of spec is the same as product code of the main product")
                        ll_select_li = True
                    End If
                End If

                Dim lcSessionId As String = "ucpfa_" + pc_product_set_code.Trim() + "_" + ddl.ID

                If Not String.IsNullOrEmpty(Session(lcSessionId)) Then
                    write_output(" - ddl.ID: " + ddl.ID)
                    write_output(" - Session(" + lcSessionId + "): " + Session(lcSessionId))
                    write_output(" - li.value: " + li.Value)
                    If li.Value = Session(lcSessionId) Then
                        write_output("Selecting the list item")
                        If Not li.Attributes.Item("disabled") = "disabled" Then
                            ll_select_li = True
                        End If
                    Else
                        ll_select_li = False
                    End If
                End If

                If ll_select_li Then
                    If Not ll_product_selected Then
                        ' Selecteer automatisch een product
                        ll_product_selected = True
                        ll_select_li = False

                        ' Deselecteer de andere items in de dropdown
                        For j As Integer = 0 To ddl.Items.Count - 1
                            If ddl.Items(j).Selected Then
                                ddl.Items(j).Selected = False
                            End If
                        Next
                        li.Selected = True

                        ' Voeg hier de geselecteerde producten toe aan de uc_order_products
                        If Not main_product Then
                            uc_order_product.set_property("product_set_product_codes", lc_product_code)
                            uc_product_set_price.set_property("product_set_product_codes", lc_product_code)
                        End If
                    End If
                End If

                write_output("- selected: " + IIf(lc_product_code = pc_product_code, "True", "False"))
                ddl.Items.Add(li)
                lo_added_values.Add(lc_value)
            End If
        Next

        If ll_optional_products Then
            Dim li As New System.Web.UI.WebControls.ListItem

            li.Text = global_trans.translate_label("label_nothing_selected", "Niets geselecteerd", Me.global_ws.Language)
            li.Value = "NOTHING_SELECTED"

            If Not ll_product_selected Then
                ll_product_selected = True

                ' Deselecteer de andere items in de dropdown
                For j As Integer = 0 To ddl.Items.Count - 1
                    If ddl.Items(j).Selected Then
                        ddl.Items(j).Selected = False
                    End If
                Next
                li.Selected = True
            End If

            ddl.Items.Insert(0, li)
        End If

        Return ddl
    End Function

    ''' <summary>
    ''' Aanmaken van een text list item en toevoegen van een object
    ''' </summary>
    ''' <param name="hgc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function add_text_item_to_list(ByVal hgc As Object) As HtmlGenericControl
        Dim li As New HtmlGenericControl("div")

        li.Attributes.Add("class", "text col-md-3")
        li.Controls.Add(hgc)

        Return li
    End Function

    ''' <summary>
    ''' Aanmaken van een value list item en toevoegen van een object
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function add_value_item_to_list(ByVal ddl As Object) As HtmlGenericControl
        Dim li As New HtmlGenericControl("div")

        li.Attributes.Add("class", "value col-md-9")
        li.Controls.Add(ddl)

        Return li
    End Function

    ''' <summary>
    ''' Deze functie haalt alle product specificaties van de hoofdproducten op
    ''' </summary>
    ''' <param name="dr"></param>
    ''' <param name="main_product"></param>
    Private Sub add_product_specifications(ByVal dr() As DataRow, Optional ByVal main_product As Boolean = False)
        write_output("Add specifications")
        Dim lc_product_codes As String = ""
        Dim lc_product_type As String = ""

        ' Loop door de producten om alle specificaties van deze producten op te kunnen halen in de query
        Dim i As Integer = 0
        For Each row As DataRow In dr
            i += 1
            write_output("Product [" + i.ToString() + "]: " + row.Item("prod_code"))

            lc_product_codes += IIf(String.IsNullOrEmpty(lc_product_codes), "", ", ") + "'" + row.Item("prod_code").ToString.Trim() + "'"
            lc_product_type = row.Item("prod_type")
        Next
        write_output("- prod_type: " + lc_product_type)
        write_output("- main_product: " + main_product.ToString())

        ' Maak een nieuwe query class aan
        Dim qc_query_class As New Utilize.Data.QuerySelectClass

        ' Op te halen gegevens
        qc_query_class.select_fields = "UPPER(uws_prod_spec.spec_code) as spec_code, uws_prod_spec_val.val_code, uws_prod_spec.prod_code, uws_prod_spec.rec_id, uws_products_tran.tgt_url"
        qc_query_class.select_fields += ", uws_prod_spec_desc.spec_desc_" & pc_language & " AS spec_desc"
        qc_query_class.select_fields += ", uws_prod_spec_val.val_desc_" & pc_language & " AS val_desc "
        'qc_query_class.select_fields += ", isnull((select top 1 '1' from uws_prod_spec crs_prod_spec where prod_code = @main_prod_code and spec_code = uws_prod_spec.spec_code and spec_val = uws_prod_spec.spec_val), '0') as available "
        'qc_query_class.add_parameter("main_prod_code", pc_product_code)

        ' Hoofdtabel
        qc_query_class.select_from = "uws_prod_spec"

        ' Filter op product codes
        qc_query_class.select_where = "uws_prod_spec.prod_code In (" & lc_product_codes & ")"

        ' Filter op "Gebruik specificatie In product reeks"
        qc_query_class.select_where += " And uws_prod_spec.use_spec_in_set = 1"

        ' Specificaties in een product reeks mogen geen vrije waarde hebben
        ' Dit een vrije waarde geen specificatie code heeft en hiermee wordt gekeken welk product geselecteerd moet worden
        qc_query_class.select_where += " And uws_prod_spec.spec_free = 0"

        ' Alleen van artikelen die getoond worden op de webshop
        qc_query_class.select_where += " and uws_products.prod_show = 1"

        ' Twee tabellen erbij joinen
        qc_query_class.add_join("left outer join", "uws_prod_spec_desc", "uws_prod_spec_desc.spec_code = uws_prod_spec.spec_code")
        qc_query_class.add_join("left outer join", "uws_prod_spec_val", "uws_prod_spec_val.spec_code = uws_prod_spec.spec_code And uws_prod_spec_val.val_code = uws_prod_spec.spec_val")
        qc_query_class.add_join("left join", "uws_products", "uws_products.prod_code = uws_prod_spec.prod_code")
        qc_query_class.add_join("left join", "uws_products_tran", "uws_products_tran.prod_code = uws_prod_spec.prod_code And uws_products_tran.lng_code = @lng_code")
        qc_query_class.add_parameter("lng_code", Me.global_ws.Language)

        ' Zet de volgorde van de gegevens
        qc_query_class.select_order = "uws_prod_spec.row_ord"

        ' Ophalen van de gegevens en in een datatable zetten
        Dim dt_product_specs As DataTable = qc_query_class.execute_query()

        write_output("Aantal specificatie waarden: " + dt_product_specs.Rows.Count.ToString())

        ' Hier worden de unieke specificaties opgehaald
        Dim dt_distinct_product_specs As DataTable = dt_product_specs.DefaultView.ToTable(True, "spec_code")

        ' Loop door elke specificatie heen
        Dim j As Integer = 0
        For Each dr_specification As DataRow In dt_distinct_product_specs.Rows
            j += 1
            ' Opslaan van de specificatie code
            Dim lc_spec_code As String = dr_specification.Item("spec_code")
            write_output("Specificatie [" + j.ToString() + "]: " + lc_spec_code)

            ' Filter alleen de specificaties van het huidige type
            Dim dr_specifications_filtered() As DataRow = dt_product_specs.Select(String.Format("spec_code = '{0}'", lc_spec_code))

            ' Als er specificaties zijn gevonden
            If dr_specifications_filtered.Length > 0 Then
                ' Voeg de specificatie toe aan de control
                Me.create_items_and_add_to_control(lc_spec_code, dr_specifications_filtered, ItemType.Specifications, main_product)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Deze functie is gekoppeld aan de dropdownlists als handler en wordt aangeroepen als een nieuwe optie wordt geselecteerd. Als het een specificatie van een hoofdproduct is dan wordt een redirect naar dit product uitgevoerd
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub ddl_SelectedIndexChanged(sender As Object, e As EventArgs)
        write_output("Dropdown selected index changed")
        'Het product moet gewijzigd worden als een specificatie van de hoofdproducten gewijzigd wordt.

        Dim ddl As dropdownlist = DirectCast(sender, dropdownlist)
        Dim ll_main_product As Boolean = CBool(ddl.Attributes.Item("data-main-spec"))

        write_output("- id: " + ddl.ID)
        write_output("- selected value: " + ddl.SelectedValue)
        write_output("- data-main-spec: " + ll_main_product.ToString())

        Dim lcSessionId As String = "ucpfa_" + pc_product_set_code.Trim() + "_" + ddl.ID

        ' Als het geen hoofdproduct is
        If Not ll_main_product Then
            ' Ophalen van de oude product code
            Dim lc_prod_code_old As String = Session(lcSessionId)
            Dim lc_prod_code_new = ddl.SelectedValue

            If Not lc_prod_code_old = "" Then
                uc_order_product.set_property("remove_product_set_product_code", lc_prod_code_old)
                uc_product_set_price.set_property("remove_product_set_product_code", lc_prod_code_old)
            End If

            uc_order_product.set_property("product_set_product_codes", lc_prod_code_new)
            uc_product_set_price.set_property("product_set_product_codes", lc_prod_code_new)
        End If

        Session(lcSessionId) = ddl.SelectedValue

        ' Controleer of een specificatie van een hoofdproduct is gewijzigd
        ' Dan moet geredirect worden naar de details pagina van het product dat deze specificatie heeft
        If ll_main_product Then
            Dim lc_query_where As String = ""
            ' Loop door de opgehaalde specificaties
            For Each loString As String In pls_spec_controls
                Dim ddl_spec As dropdownlist = Me.FindControl(loString)
                Dim ll_main_spec As Boolean = CBool(ddl_spec.Attributes.Item("data-main-spec"))
                Dim lc_spec_code As String = ddl_spec.Attributes.Item("data-spec-code")

                write_output("- id: " + ddl_spec.ID)
                write_output("- selected value: " + ddl_spec.SelectedValue)
                write_output("- data-spec-code: " + lc_spec_code)
                write_output("- data-main-spec: " + ll_main_spec.ToString())

                ' Als de dropdown specificaties van een hoofdproduct bevat
                If ll_main_spec And Not String.IsNullOrEmpty(lc_spec_code) Then
                    If Not String.IsNullOrEmpty(lc_query_where) Then
                        lc_query_where += " AND "
                    End If
                    lc_query_where += String.Format("uws_products.prod_code IN (SELECT prod_code from uws_prod_spec WHERE uws_prod_spec.spec_code = '{0}'", lc_spec_code)
                    lc_query_where += String.Format(" AND uws_prod_spec.spec_val = '{0}' ==extra==)", ddl_spec.SelectedValue)
                    'lc_query_where += String.Format(" OR '{0}' NOT IN (SELECT spec_code from uws_prod_spec where prod_code = products.prod_code AND spec_code = '{0}'))", lc_spec_code)
                End If
            Next

            ' Ophalen van het artikel dat voldoet aan de specificaties
            Try
                Dim lc_tgt_url As String = Me.get_product_url_with_specifications(lc_query_where)
                write_output("- redirect url: " + lc_tgt_url)
                If Not String.IsNullOrEmpty(lc_tgt_url) Then
                    Response.Redirect(Me.ResolveUrl("~" + lc_tgt_url), True)
                End If
            Catch ex As Exception
                write_output("- ERROR: " + ex.Message.Replace(Chr(13), "<br/>"))
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Deze functie controleert of een listitem enabled of disabled moet worden, en voegt de disabled attribuut toe
    ''' </summary>
    Private Sub check_enabled(ByRef li As System.Web.UI.WebControls.ListItem)
        write_output("check_enabled")
        ' Scenario 1
        ' Specificaties huidige product:
        ' Maat: 200x200
        ' Kleur: groen
        ' Specificaties andere producten:
        ' 180x200, geel
        ' 180x200, zwart
        ' geen maat, zwart
        ' In de details van het huidige product:
        ' De maten dropdown:
        ' - 200x200 geselecteerd
        ' - 180x200 disabled
        ' De kleuren dropdown
        ' - groen geselecteerd
        ' - zwart disabled
        ' - geel disabled

        ' TODO: wat als een hoofdproduct geen maat/kleur specificatie heeft; tekst: "geen 'specificatie'" tonen of specificatie niet tonen

        Dim ll_resume As Boolean = True

        ' Controleer of de specificatie van het huidige product is
        ' Als dit zo is dan hoeft deze waarde niet gedisabled te worden
        Dim lc_prod_code As String = ""
        If ll_resume Then
            lc_prod_code = li.Attributes.Item("data-prod-code")
            write_output("- prod_code: " + lc_prod_code)

            ll_resume = Not lc_prod_code = pc_product_code

            If Not ll_resume Then
                write_output("Product code of spec is the same as product code of the main product")
            End If
        End If

        ' Ophalen van de specificatie code
        ' Alleen waarden met een specificatie code moeten evt. gedisabled te worden
        Dim lc_spec_code As String = ""
        If ll_resume Then
            lc_spec_code = li.Attributes.Item("data-spec-code")
            write_output("- spec_code: " + lc_spec_code)

            ll_resume = Not String.IsNullOrEmpty(lc_spec_code)

            If Not ll_resume Then
                write_output("The list item has no specification code")
            End If
        End If

        ' Ophalen van de waarde van de dropdown list item
        Dim lc_val_code As String = ""
        If ll_resume Then
            lc_val_code = li.Value
            write_output("- val_code: " + lc_val_code)

            ll_resume = Not String.IsNullOrEmpty(lc_val_code)

            If Not ll_resume Then
                write_output("The list item has no specification value")
            End If
        End If

        If ll_resume Then
            If Not Me.check_enabled_spec_value(lc_spec_code, lc_val_code) Then
                li.Attributes.Add("disabled", "disabled")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Deze functie controleert of het product enabled of disabled moet worden afhankelijk van de specificaties van het hoofdproduct
    ''' </summary>
    ''' <param name="prod_code"></param>
    ''' <returns></returns>
    Private Function check_enabled_product(ByVal prod_code As String) As Boolean
        write_output("Check product enabled")
        write_output("- prod_code: " + prod_code)

        Dim ll_resume As Boolean = True
        Dim ll_enable_product As Boolean = True

        If ll_resume Then
            ' Hier worden de specificaties opgehaald van het hoofdproduct
            If pdt_current_product_specs Is Nothing Then
                Dim qsc_current_product_specs As New Utilize.Data.QuerySelectClass

                qsc_current_product_specs.select_fields = "spec_code, spec_val"
                qsc_current_product_specs.select_from = "uws_prod_spec"
                qsc_current_product_specs.select_where = "prod_code = @prod_code"
                qsc_current_product_specs.select_where += " AND use_spec_in_set = 1"
                qsc_current_product_specs.select_where += " AND spec_free = 0"
                qsc_current_product_specs.add_parameter("prod_code", pc_product_code)

                pdt_current_product_specs = qsc_current_product_specs.execute_query()
            End If
        End If

        If ll_resume Then
            ll_resume = pdt_current_product_specs.Rows.Count > 0
        End If

        ' Hier worden de specificaties opgehaald van het huidige product
        Dim dt_this_product_specs As DataTable = Nothing
        If ll_resume Then
            Dim qsc_product_specs As New Utilize.Data.QuerySelectClass

            qsc_product_specs.select_fields = "spec_code, spec_val"
            qsc_product_specs.select_from = "uws_prod_spec"
            qsc_product_specs.select_where = "prod_code = @prod_code"
            qsc_product_specs.select_where += " AND use_spec_in_set = 1"
            qsc_product_specs.select_where += " AND spec_free = 0"
            qsc_product_specs.add_parameter("prod_code", prod_code)

            dt_this_product_specs = qsc_product_specs.execute_query()
        End If

        If ll_resume Then
            ll_resume = dt_this_product_specs.Rows.Count > 0
        End If

        If ll_resume Then
            write_output("Loop specs of main product " + pc_product_code)
            Dim ll_enable_product_spec As Boolean = False
            ' Hier wordt gekeken of de huidige val_code disabled of enabled moet worden
            For Each dr_current_spec As DataRow In pdt_current_product_specs.Rows
                ll_enable_product_spec = False

                Dim lc_spec_code As String = dr_current_spec.Item("spec_code")
                Dim lc_val_code As String = dr_current_spec.Item("spec_val")
                write_output("- spec_code: " + lc_spec_code)
                write_output("- val_code: " + lc_val_code)

                ' Controleer het product de specificatie heeft
                Dim dt_filtered_spec_code() As DataRow = dt_this_product_specs.Select(String.Format("spec_code = '{0}'", lc_spec_code))

                ' Zo ja, controleer dan of het product de waarde heeft
                If dt_filtered_spec_code.Length > 0 Then
                    Dim dt_filtered_spec_val() As DataRow = dt_this_product_specs.Select(String.Format("spec_code = '{0}' and spec_val = '{1}'", lc_spec_code, lc_val_code))

                    If dt_filtered_spec_val.Length > 0 Then
                        write_output("- dt_filtered_spec_val.Length > 0")
                        ll_enable_product_spec = True
                    End If
                Else
                    write_output("- NOT dt_filtered_spec_code.Length > 0")
                    ' Zo nee dan mag het product enabled worden
                    ll_enable_product_spec = True
                End If
                write_output("- enabled product spec: " + ll_enable_product_spec.ToString())
                If Not ll_enable_product_spec Then
                    ll_enable_product = False
                End If
            Next
        End If

        write_output("- enabled product: " + ll_enable_product.ToString())
        Return ll_enable_product
    End Function

    ''' <summary>
    ''' Deze functie controleert of een specificatie waarde enabled of disabled moet worden afhankelijk van het hoofdproduct
    ''' </summary>
    ''' <param name="spec_code"></param>
    ''' <param name="val_code"></param>
    ''' <returns></returns>
    Private Function check_enabled_spec_value(ByVal spec_code As String, ByVal val_code As String) As Boolean
        write_output("Check specification enabled")
        write_output("- spec_code: " + spec_code)
        write_output("- val_code: " + val_code)

        ' Hier worden de gegevens opgehaald.
        ' De list item moet disabled worden als de val_code niet voorkomt in specificaties van de lijst met hoofdproducten waar de specificaties hetzelfde zijn als het hoofdproducten
        ' Maat 180x200
        ' spec_code: MAAT
        ' val_code: 1820
        ' specs hoofdproduct
        ' spec_code: MAAT
        ' val_code: 2020
        ' spec_code: KLEUR
        ' val_code: GREEN

        ' Moet de maat 1820 disabled worden, ja
        ' Waarom? Omdat het huidige product kleur green heeft en geen een hoofdproduct kleur green heeft met maat 1820
        ' Wat hebben we daarvoor nodig
        ' ophalen alle maten (meegegeven spec_code) van hoofdproducten waar de specificaties van het hoofdproduct aanwezig zijn

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Hier worden de producten opgehaald uit de product reeks die de specificatie hebben '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If pdt_product_specs Is Nothing Then
            ' Maak een nieuwe query class aan
            Dim qc_query_class As New Utilize.Data.QuerySelectClass

            ' Op te halen gegevens
            qc_query_class.select_fields = "prod_code, spec_code, spec_val"

            qc_query_class.select_from = "uws_prod_spec"

            qc_query_class.select_where = " use_spec_in_set = 1"
            qc_query_class.select_where += " AND spec_free = 0"
            qc_query_class.select_where += " AND prod_code IN (SELECT prod_code FROM uws_product_set_prod WHERE set_code = @set_code AND main_product = 1)"

            qc_query_class.add_parameter("set_code", pc_product_set_code)

            ' Ophalen van de gegevens en in een datatable zetten
            pdt_product_specs = qc_query_class.execute_query()
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Hier wordt een filter gezet op de huidige specificatie '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim dr_products_with_spec_and_value() As DataRow = Nothing
        If pdt_product_specs.Rows.Count > 0 Then
            dr_products_with_spec_and_value = pdt_product_specs.Select(String.Format("spec_code = '{0}' and spec_val = '{1}'", spec_code, val_code))
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Hier worden de specificaties opgehaald van het huidige product '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If pdt_current_product_specs Is Nothing Then
            Dim qsc_current_product_specs As New Utilize.Data.QuerySelectClass

            qsc_current_product_specs.select_fields = "spec_code, spec_val"
            qsc_current_product_specs.select_from = "uws_prod_spec"
            qsc_current_product_specs.select_where = "prod_code = @prod_code"
            qsc_current_product_specs.select_where += " AND use_spec_in_set = 1"
            qsc_current_product_specs.select_where += " AND spec_free = 0"
            qsc_current_product_specs.add_parameter("prod_code", pc_product_code)

            pdt_current_product_specs = qsc_current_product_specs.execute_query()
        End If

        ' Hier wordt gekeken of de huidige val_code disabled of enabled moet worden
        Dim ll_enable_spec_value As Boolean = False
        For Each dr_product As DataRow In dr_products_with_spec_and_value
            Dim lc_prod_code As String = dr_product.Item("prod_code")

            For Each dr_current_product As DataRow In pdt_current_product_specs.Rows
                Dim lc_spec_code As String = dr_current_product.Item("spec_code")
                Dim lc_val_code As String = dr_current_product.Item("spec_val")
                Dim dt_product_specs_and_values() As DataRow = pdt_product_specs.Select(String.Format("prod_code = '{0}' and spec_code = '{1}' and spec_val = '{2}'", lc_prod_code, lc_spec_code, lc_val_code))

                If dt_product_specs_and_values.Length > 0 Then
                    ll_enable_spec_value = True
                End If
            Next
        Next

        Return ll_enable_spec_value
    End Function

    ''' <summary>
    ''' Deze functie haalt de product url op voor een redirect met de geselecteerde specificaties
    ''' </summary>
    ''' <param name="query_where"></param>
    ''' <returns></returns>
    Private Function get_product_url_with_specifications(ByVal query_where As String) As String
        write_output("Get product URL with the selected specifications")
        write_output("- query where: " + query_where)

        ' Filter op "Gebruik specificatie in product reeks"
        ' Specificaties in een product reeks mogen geen vrije waarde hebben
        query_where = query_where.Replace("==extra==", "AND uws_prod_spec.prod_code = uws_products.prod_code AND uws_prod_spec.use_spec_in_set = 1 AND uws_prod_spec.spec_free = 0")
        write_output("- query where: " + query_where)

        Dim lc_url As String = ""
        Dim dt_products As DataTable = Nothing

        ' Maak een nieuwe query class aan
        Dim qc_query_class As New Utilize.Data.QuerySelectClass

        ' Op te halen gegevens
        qc_query_class.select_fields = "uws_products_tran.tgt_url"

        ' Hoofdtabel
        qc_query_class.select_from = "uws_products"

        qc_query_class.select_where += query_where

        ' Twee tabellen erbij joinen
        qc_query_class.add_join("left join", "uws_products_tran", "uws_products_tran.prod_code = uws_products.prod_code AND uws_products_tran.lng_code = @lng_code")
        qc_query_class.add_parameter("lng_code", Me.global_ws.Language)

        ' Ophalen van de gegevens en in een datatable zetten
        dt_products = qc_query_class.execute_query()

        If dt_products IsNot Nothing Then
            If dt_products.Rows.Count > 0 Then
                lc_url = dt_products.Rows(0).Item("tgt_url")
            End If
        End If

        Return lc_url
    End Function

    ''' <summary>
    ''' Deze functie maakt een log bestand aan in de documents folder en schrijft log berichten weg
    ''' </summary>
    ''' <param name="message"></param>
    ''' <param name="append"></param>
    Private Sub write_output(ByVal message As String, Optional ByVal append As Boolean = True)
        Dim ll_utlz_fw_dev As Boolean = False
        Boolean.TryParse(Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("utlz_fw_dev"), ll_utlz_fw_dev)

        If ll_utlz_fw_dev Then
            ' Add the relative path of the file
            Dim lc_file_path As String = "~/Documents/Logs"

            ' Add the path of the solution to the file path
            lc_file_path = System.Web.HttpContext.Current.Server.MapPath(lc_file_path)

            ' Check if the directory exists
            If Not System.IO.Directory.Exists(lc_file_path) Then
                ' Create the directory
                System.IO.Directory.CreateDirectory(lc_file_path)
            End If

            ' Write the string to a new file
            lc_file_path = System.IO.Path.Combine(lc_file_path, "AdvancedFilters.txt")
            Using outputFile As New System.IO.StreamWriter(lc_file_path, append)
                outputFile.WriteLine(message)
            End Using
        End If
    End Sub
End Class
