﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_set_price.ascx.vb" Inherits="Webshop_Modules_ProductFilters_uc_product_set_price" %>

<div class="uc_product_set_price">
    <ul class="hidden">
        <li><utilize:label runat="server" ID="lbl_product_code"></utilize:label></li>
        <li><utilize:label runat="server" ID="lbl_product_set_code"></utilize:label></li>
        <li><utilize:label runat="server" ID="lbl_product_set_product_codes"></utilize:label></li>
    </ul>
    <div class="prod_price">
        <utilize:placeholder runat="server" ID="ph_prod_price1">
            <ul class="price list-inline shop-product-prices">
                <li><utilize:label runat="server" ID="lbl_prod_price1" CssClass="value actual"></utilize:label></li>
                <li><utilize:label runat="server" ID="lbl_prod_price1_original" CssClass="value original"></utilize:label></li>
            </ul>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prod_price2">
            <ul class="price1 list-inline shop-product-prices">
                <li><utilize:label runat="server" ID="lbl_prod_price2" CssClass="value actual"></utilize:label></li>
                <li><utilize:label runat="server" ID="lbl_prod_price2_original" CssClass="value original"></utilize:label></li>
            </ul>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_prod_price3">
            <ul class="price2 list-inline shop-product-prices">
                <li><utilize:label runat="server" ID="lbl_prod_price3" CssClass="value actual"></utilize:label></li>
                <li><utilize:label runat="server" ID="lbl_prod_price3_original" CssClass="value original"></utilize:label></li>
            </ul>
        </utilize:placeholder>
    </div>
</div>