﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_filters_advanced.ascx.vb" Inherits="uc_product_filters_advanced" %>

<div class="uc_product_filters_advanced">
    <utilize:placeholder runat="server" ID="ph_product_set_price"></utilize:placeholder>

    <h2><utilize:literal runat="server" ID="lt_title" text="Product filters - Advanced"></utilize:literal></h2>
    <utilize:placeholder runat="server" ID="ph_product_set_products">
	</utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_product_set_optional_products">
        <h3><utilize:label runat="server" ID="label_optional_product_filters" Text="Optioneel"></utilize:label></h3>
	</utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_order_product"></utilize:placeholder>
</div>