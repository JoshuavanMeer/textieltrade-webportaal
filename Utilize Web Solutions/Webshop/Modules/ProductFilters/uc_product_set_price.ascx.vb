﻿
Partial Class Webshop_Modules_ProductFilters_uc_product_set_price
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _sales_action As Boolean = False
    Private _product_code As String = ""
    Private _product_set_code As String = ""
    Private _product_set_product_codes As List(Of String) = New List(Of String)

    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
            Me.calculate_set_price()
        End Set
    End Property

   Public WriteOnly Property product_set_code As String
        Set(value As String)
            _product_set_code = value
        End Set
    End Property

    Public WriteOnly Property product_set_product_codes As String
        Set(value As String)
            If Not value = "NOTHING_SELECTED" And Not _product_set_product_codes.contains(value) Then 
                _product_set_product_codes.Add(value)
                Me.calculate_set_price()
            End If 
        End Set
    End Property

     Public WriteOnly Property remove_product_set_product_code As String
        Set(value As String)
            _product_set_product_codes.Remove(value)
            Me.calculate_set_price()
        End Set
    End Property

    ' Klantprijs product
    Private pn_prod_price1 As Decimal = 0
    Private pn_prod_price2 As Decimal = 0
    Private pn_prod_price3 As Decimal = 0

    ' Originele verkoopprijs product
    Private pn_prod_original_price1 As Decimal = 0
    Private pn_prod_original_price2 As Decimal = 0
    Private pn_prod_original_price3 As Decimal = 0

    ' Verkoopacties
    Private pl_sales_action_1 As Boolean = False
    Private pl_sales_action_2 As Boolean = False
    Private pl_sales_action_3 As Boolean = False

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.block_css = False
        Me.set_style_sheet("uc_product_set_price.css", Me)

        Me.calculate_set_price()
    End Sub

    Private sub calculate_sales_actions()
        Me._sales_action = global_ws.check_sales_action(Me._product_code)

        ' Bereken de verkoopacties
        pl_sales_action_1 = pn_prod_original_price1 > pn_prod_price1
        pl_sales_action_2 = pn_prod_original_price2 > pn_prod_price2
        pl_sales_action_3 = pn_prod_original_price3 > pn_prod_price3
    End sub

    Private sub convert_amounts_to_credits()
        ' Credits
        Me.lbl_prod_price1.Text = Me.global_ws.convert_amount_to_credits(pn_prod_price1)
        Me.lbl_prod_price2.Text = Me.global_ws.convert_amount_to_credits(pn_prod_price2)
        Me.lbl_prod_price3.Text = Me.global_ws.convert_amount_to_credits(pn_prod_price3)

        Me.lbl_prod_price1_original.Text = Me.global_ws.convert_amount_to_credits(pn_prod_original_price1)
        Me.lbl_prod_price2_original.Text = Me.global_ws.convert_amount_to_credits(pn_prod_original_price2)
        Me.lbl_prod_price3_original.Text = Me.global_ws.convert_amount_to_credits(pn_prod_original_price3)
    End sub

    Private sub format_prices()
        ' Standaard prijzen
        Me.lbl_prod_price1.Text = Format(pn_prod_price1, "c")
        Me.lbl_prod_price2.Text = Format(pn_prod_price2, "c")
        Me.lbl_prod_price3.Text = Format(pn_prod_price3, "c")

        Me.lbl_prod_price1_original.Text = Format(pn_prod_original_price1, "c")
        Me.lbl_prod_price2_original.Text = Format(pn_prod_original_price2, "c")
        Me.lbl_prod_price3_original.Text = Format(pn_prod_original_price3, "c")
    End sub

    Private sub add_sales_action_css_classes()
        ' Toevoegen CSS classes voor de verkoopacties
        If Me._sales_action And pl_sales_action_1 Then
            Me.lbl_prod_price1.CssClass = Me.lbl_prod_price1.CssClass + " salesaction_to"
            Me.lbl_prod_price1_original.CssClass = Me.lbl_prod_price1_original.CssClass + " salesaction_for"
        End If

        If Me._sales_action And pl_sales_action_2 Then
            Me.lbl_prod_price2.CssClass = Me.lbl_prod_price2.CssClass + " salesaction_to"
            Me.lbl_prod_price2_original.CssClass = Me.lbl_prod_price2_original.CssClass + " salesaction_for"
        End If

        If Me._sales_action And pl_sales_action_3 Then
            Me.lbl_prod_price3.CssClass = Me.lbl_prod_price3.CssClass + " salesaction_to"
            Me.lbl_prod_price3_original.CssClass = Me.lbl_prod_price3_original.CssClass + " salesaction_for"
        End If

        ' Toevoegen CSS classes als er geen verkoopacties zijn
        If Not Me._sales_action And Not pl_sales_action_1 Then
            Me.lbl_prod_price1_original.CssClass = "value original originaldefault"
            Me.lbl_prod_price1.CssClass = "value actual actualdefault"
        End If

        If Not Me._sales_action And Not pl_sales_action_2 Then
            Me.lbl_prod_price2_original.CssClass = "value original originaldefault"
            Me.lbl_prod_price2.CssClass = "value actual actualdefault"
        End If

        If Not Me._sales_action And Not pl_sales_action_3 Then
            Me.lbl_prod_price3_original.CssClass = "value original originaldefault"
            Me.lbl_prod_price3.CssClass = "value actual actualdefault"
        End If
    End sub

    Private sub set_prices_visibility()
        ' De prijzen mogen niet zichtbaar zijn indien de baliefunctie actief is, en het UserShowPrices attribuut in de sessie op false staat.
        Dim ll_prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        If ll_prices_not_visible Then
            Me.ph_prod_price1.Visible = False
            Me.ph_prod_price2.Visible = False
            Me.ph_prod_price3.Visible = False
        Else
            ' Of prijzen zichtbaar zijn is afhankelijk van een aantal instellingen
            Me.ph_prod_price1.Visible = IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
            Me.ph_prod_price2.Visible = Me.global_ws.get_webshop_setting("prod_price1_avail") And IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
            Me.ph_prod_price3.Visible = Me.global_ws.get_webshop_setting("prod_price2_avail") And IIf(Me.global_ws.user_information.user_logged_on, True, Not Me.global_ws.get_webshop_setting("prices_not_logged_on"))
        End If

        ' Als er extra prijzen gebruikt moeten worden en er is aangegeven dat de prijs niet getoond moet worden wanneer deze 0 is
        If Not Me.global_ws.get_webshop_setting("prod_price1_show") And Me.lbl_prod_price2.Visible Then
            If pn_prod_original_price1 = 0 Then
                Me.ph_prod_price2.Visible = False
            End If
        End If

        ' Als er extra prijzen gebruikt moeten worden en er is aangegeven dat de prijs niet getoond moet worden wanneer deze 0 is
        If Not Me.global_ws.get_webshop_setting("prod_price2_show") And Me.lbl_prod_price3.Visible Then
            If pn_prod_original_price2 = 0 Then
                Me.ph_prod_price3.Visible = False
            End If
        End If
    End sub
    Private sub calculate_set_price()
        ' Toon de set gegevens, verwijderen als ontwikkeling is afgerond
        me.lbl_product_code.text = _product_code
        me.lbl_product_set_code.text = _product_set_code
        me.lbl_product_set_product_codes.text = String.Join("<br/>",  _product_set_product_codes.toArray())

        Me.reset_prices()

        Dim pf_product_details As New ws_productdetails
        Dim dt_product_details As System.Data.DataTable = pf_product_details.get_product_details(me._product_code, Me.global_ws.Language)

        If dt_product_details.Rows.Count > 0 Then
            set_price_for_product(Me._product_code)

            For each prod_code As String in _product_set_product_codes
                set_price_for_product(prod_code)
            Next

            Me.calculate_sales_actions()

            If global_ws.price_mode = Webshop.price_mode.Credits Then
                Me.convert_amounts_to_credits()
            Else
                Me.format_prices()
            End If

            Me.add_sales_action_css_classes()

            Me.set_prices_visibility()
        End If
    End sub

    Private sub set_price_for_product(byval prod_code As string)
        If Me.global_ws.get_webshop_setting("price_type") = 1 Then
            ' Excl. BTW
            pn_prod_price1 += Me.global_ws.get_product_price(prod_code, "")
            pn_prod_price2 += Me.global_ws.get_product_price1(prod_code, "")
            pn_prod_price3 += Me.global_ws.get_product_price2(prod_code, "")

            pn_prod_original_price1 += Me.global_ws.get_product_price_default(prod_code)
            pn_prod_original_price2 += Me.global_ws.get_product_price1_default(prod_code)
            pn_prod_original_price3 += Me.global_ws.get_product_price2_default(prod_code)
        Else
            ' Incl. BTW
            pn_prod_price1 += Me.global_ws.get_product_price_incl_vat(prod_code, "")
            pn_prod_price2 += Me.global_ws.get_product_price1_incl_vat(prod_code, "")
            pn_prod_price3 += Me.global_ws.get_product_price2_incl_vat(prod_code, "")

            pn_prod_original_price1 += Me.global_ws.get_product_price_default_incl_vat(prod_code)
            pn_prod_original_price2 += Me.global_ws.get_product_price1_default_incl_vat(prod_code)
            pn_prod_original_price3 += Me.global_ws.get_product_price2_default_incl_vat(prod_code)
        End If
    End sub

    Private Sub reset_prices()
        ' Klantprijs product
        pn_prod_price1 = 0
        pn_prod_price2 = 0
        pn_prod_price3 = 0

        ' Originele verkoopprijs product
        pn_prod_original_price1 = 0
        pn_prod_original_price2 = 0
        pn_prod_original_price3 = 0

        ' Verkoopacties
        pl_sales_action_1 = False
        pl_sales_action_2 = False
        pl_sales_action_3 = False
    End Sub 

End Class
