﻿Partial Class uc_logon
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

#Region "properties (show_logout / show_login / user_type / additional_classname / display_title)"

    Public Property display_my_account() As Boolean
        Get
            Return Me.button_my_account.Visible
        End Get
        Set(ByVal value As Boolean)
            Me.button_my_account.Visible = value
        End Set
    End Property
#End Region
   
    Protected Sub button_logout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_logout.Click
        Dim ll_resume As Boolean = True

        ' Als er een oorspronkelijke id is, dan is de gebruiker namens een klant ingelogd.
        ' Sla deze id op in een variabele
        Dim lc_original_id As String = Me.global_ws.user_information.user_id_original

        ' Als de oorspronkelijke id niet bekend is, dan wordt de cookie ook leeggemaakt
        If ll_resume And lc_original_id = "" Then
            Me.cookie_write("")
        End If

        If ll_resume Then
            ' Probeer uit te loggen
            Me.global_ws.user_information.logout()
        End If

        If ll_resume And lc_original_id = "" Then
            Response.Redirect("~/home.aspx", True)
        End If

        If ll_resume And Not lc_original_id = "" Then
            ' Haal de gegevens op om opnieuw namens de oorspronkelijke gebruiker in te kunnen loggen.
            Dim lc_cust_id As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "hdr_id", lc_original_id)
            Dim lc_login_code As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_code", lc_original_id)
            Dim lc_password As String = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_password", lc_original_id)
            Dim ln_user_type As Integer = Utilize.Data.DataProcedures.GetValue("uws_customers", "type_code", lc_cust_id)

            ' Log in namens de oorspronkelijke gebruiker
            ll_resume = Me.global_ws.user_information.login(ln_user_type, lc_login_code, lc_password)

            If ll_resume Then
                ' Als de module inkoopcombinaties aan staat, ga dan terug naar home
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") Then
                    Response.Redirect("~/home.aspx", False)
                Else
                    Response.Redirect("~/" + Me.global_ws.Language + "/account/customersoverview.aspx", True)
                End If
            End If
        End If
    End Sub

    Protected Sub button_my_account_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_my_account.Click
        Response.Redirect("~/" + Me.global_ws.language + "/account/account.aspx", True)
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_javascript("uc_logon.js", Me)

        If Not Me.global_ws.user_information.user_logged_on Then
            Me.ph_logon.Visible = True
            Me.pan_logon.Controls.Add(loadcontrol("/Webshop/Modules/Login/uc_logon_page.ascx"))
        Else
            Me.ph_logout.Visible = True

            If Not Me.IsPostBack() Then
                Me.chk_account_counter.Checked = Me.global_ws.show_prices
            End If

            Dim ll_testing As Boolean = True

            If ll_testing Then
                ' Doe hier de licentie controle, dit houdt in dat we checken of de module actief is
                ll_testing = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter")

                If Not ll_testing Then
                    Me.label_show_prices.Visible = False
                End If
            End If

            Me.link_account_orders_overview.OnClientClick = "document.location='" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language.ToLower() + "/account/orders.aspx") + "'; return false;"
            Me.link_account_favorites.OnClientClick = "document.location='" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language.ToLower() + "/account/favorites.aspx") + "'; return false;"
            Me.link_account_order_entry.OnClientClick = "document.location='" + Me.ResolveCustomUrl("~/" + Me.global_ws.Language.ToLower() + "/account/orderentry.aspx") + "'; return false;"

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Then
                Me.chk_account_counter.Visible = True
            End If

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_order_entry") Then
                Me.link_account_order_entry.Visible = True
            Else
                Me.link_account_order_entry.Visible = False
            End If

            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True Then
                If Not Me.global_ws.purchase_combination.user_is_manager Then
                    Me.button_my_account.visible = False
                End If
            End If

            If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
                Me.button_my_account.Visible = False
            End If

        End If

        Me.set_style_sheet("uc_logon.css", Me)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.IsPostBack And Me.hih_logon_visible.Value = "visible" Then
            Me.pan_account.Style.Add("display", "block")
            Me.pan_logon.Style.Add("display", "block")
        End If

        ' Maak de gegevens op de juiste manier beschikbaar
        Me.ph_logon.Visible = (Not Me.global_ws.user_information.user_logged_on)
        Me.ph_logout.Visible = Me.global_ws.user_information.user_logged_on

        Me.ph_user.Visible = True
        Me.pnl_accountmenu.Visible = Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb")

        Me.lt_welcome.Text = Me.global_ws.user_information.user_full_name
    End Sub

    Private Function cookie_write(ByVal user_id As String) As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim lc_user_id As String = Utilize.FrameWork.Encryption.Encrypt(user_id)

            Utilize.FrameWork.FrameworkProcedures.WriteCookie("UserLoginId", lc_user_id)
        End If

        Return ll_resume
    End Function

    Protected Sub chk_account_counter_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_account_counter.CheckedChanged
        ' Maak prijzen wel of niet zichtbaar afhankelijk van of de 
        Me.global_ws.show_prices = Me.chk_account_counter.Checked

        ' Laad de pagina opnieuw
        Response.Redirect(FrameWork.FrameworkProcedures.GetRequestUrl(), True)
    End Sub
End Class