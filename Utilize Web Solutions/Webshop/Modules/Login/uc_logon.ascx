﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_logon.ascx.vb" Inherits="uc_logon" %>

<utilize:placeholder runat='server' ID="ph_logon" Visible="false">
    <utilize:panel runat="server" ID="pan_logon" CssClass="col-md-12 uc_logon">
    </utilize:panel>
</utilize:placeholder>

<utilize:placeholder runat='server' ID="ph_logout" Visible="false">
    <utilize:panel runat="server" ID="pan_account" CssClass="col-md-12 uc_logon margin-bottom-20">
        <div class="border logout_wrapper">
            <utilize:placeholder runat="server" ID="ph_user">
                <h2><utilize:translateliteral ID="label_login_welcome" runat='server' Visible="true" Text="Welkom"></utilize:translateliteral>&nbsp;<utilize:literal ID="lt_welcome" runat="server"></utilize:literal></h2>
            </utilize:placeholder>

            <utilize:panel runat="server" ID="pnl_accountmenu" CssClass="accountmenu">
                <div class="accountlinks">
                    <utilize:translatelinkbutton runat='server' ID="link_account_order_entry" Text="Snel bestellen"></utilize:translatelinkbutton><br />
                    <utilize:translatelinkbutton runat='server' ID="link_account_orders_overview" Text="Overzicht bestellingen"></utilize:translatelinkbutton><br />
                    <utilize:translatelinkbutton runat='server' ID="link_account_favorites" Text="Uw favoriete producten"></utilize:translatelinkbutton><br />
                </div>
                <div class="wrapper_show_prices">
                    <utilize:checkbox AutoPostBack="true" Visible="false" runat="server" ID="chk_account_counter" /><utilize:translatelabel ID="label_show_prices" runat="server" Text="Toon prijzen"></utilize:translatelabel>
                </div>
            </utilize:panel>

            <div class="row_logout">
                <utilize:translatebutton ID="button_logout" CssClass="sb_logout  btn-u btn-u-sea-shop col-md-6" Text="Uitloggen" runat="server" />
                <utilize:translatebutton ID="button_my_account" CssClass="sb_account  btn-u btn-u-sea-shop col-md-6" Text="Mijn account" runat="server" />
            </div>
        </div>
    </utilize:panel>
</utilize:placeholder>
<asp:HiddenField runat="server" ID="hih_logon_visible" Value="" />
