﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_logon_page.ascx.vb" Inherits="uc_logon_page" %>

<utilize:panel cssclass="uc_logon_page" runat="server" DefaultButton="button_login">
    <utilize:placeholder runat="server" ID="ph_alert">
        <div class="alert alert-warning" role="alert">
            <utilize:translatetext runat="server" ID="text_secure_page" Text="De pagina die u wilt openen is onderdeel van het beveiligde gedeelte van onze website. Hieronder kunt u inloggen om naar de gewenste pagina te gaan."></utilize:translatetext>
        </div>
    </utilize:placeholder>

    <h1><utilize:translatetitle runat="server" ID="title_login" Text="Inloggen"></utilize:translatetitle></h1>

    <div class="border margin-bottom-20">
        <section>
            <label class="input login-input">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <utilize:textbox ID="txt_logon_name" runat="server" CssClass="txt_name form-control"></utilize:textbox>
                </div>
            </label>
        </section>

        <section>
            <label class="input login-input no-border-top">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <utilize:textbox ID="txt_logon_password" TextMode="Password" runat="server" CssClass="txt_password form-control"></utilize:textbox>
                </div>
            </label>
        </section>
        <section>
            <utilize:translatebutton ID="button_login" UseSubmitBehavior="true" CssClass="btn-u btn-u-sea-shop btn-block margin-bottom-20" Text="Inloggen" runat="server" />
        </section>
        <section class="height-fix">
            <div class="row col-sm-12 margin-bottom-5">
                <div class="col-sm-6">
                    <span class="checkbox margin">
                        <utilize:checkbox runat="server" ID="chk_remember_me" class="sb_remember_me" name="chk_remember_me"/>
                        <label for="sb_remember_me">
                            <utilize:translateliteral ID="button_remember_me" Text="Onthoud wachtwoord" runat="server"></utilize:translateliteral>
                        </label>
                    </span>
                </div>
                <div class="col-sm-6 text-right pass_forgot">
                    <span class="checkbox sb_remember_me" style="margin-top: 0px!important">
                        <utilize:hyperlink runat="server" ID="link_forgot_password"></utilize:hyperlink>
                    </span>
                </div>
            </div>
        </section>
    </div>

    <utilize:placeholder runat="server" ID="ph_single_sign_on">
        <div class="single-sign-on">        
            <utilize:repeater runat="server" ID="rpt_external_logins">
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <utilize:button runat="server" ID="btn_external_login" CommandName="external_login" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "sso_code") %>' CssClass="btn-u btn-u-sea-shop btn-block margin-bottom-20"/>
                        </div>
                    </div>
                </ItemTemplate>
            </utilize:repeater>            
        </div>
    </utilize:placeholder>    
</utilize:panel>
