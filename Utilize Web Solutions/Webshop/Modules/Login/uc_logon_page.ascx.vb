﻿Imports System.Linq
Imports System.Web.UI.WebControls

Partial Class uc_logon_page
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control
    Private default_redirect_page As String
    Protected Sub button_login_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_login.Click
        Dim ll_resume As Boolean = True

        Try
            If ll_resume Then
                ' Probeer in te loggen
                ll_resume = Me.global_ws.user_information.login(0, Me.txt_logon_name.Text, Me.txt_logon_password.Text)

                If Not ll_resume Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "login_error", "alert('" + Me.global_ws.user_information.error_message + "'); ", True)
                End If
            End If

            If ll_resume Then
                ' Hier controleren we of een gebruiker geblokkeerd is of niet.
                ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers", "blocked", global_ws.user_information.user_customer_id))

                If Not ll_resume Then
                    Me.global_ws.user_information.logout()

                    Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
                End If
            End If

            If ll_resume Then
                ' Hier controleren we of een gebruiker geblokkeerd is of niet.
                ll_resume = Not CBool(Utilize.Data.DataProcedures.GetValue("uws_customers_users", "blocked", global_ws.user_information.user_id))

                If Not ll_resume Then
                    Me.global_ws.user_information.logout()

                    Dim loTranslateMessage_blcked As String = global_trans.translate_message("message_user_blocked", "Uw account is geblokkeerd, neem contact op.", Me.global_cms.Language)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "message_user_blocked", "alert('" & loTranslateMessage_blcked & "');", True)
                End If
            End If

            If ll_resume And Me.chk_remember_me.Checked Then
                'Writing the UserLoginId cookie
                Dim lc_user_id As String = Utilize.FrameWork.Encryption.Encrypt(Me.global_ws.user_information.user_id)
                'Setting the default value if setting is 0 = 10 days 10x24h
                Dim ld_validity_hours As Double = 240

                If Not Me.global_ws.get_webshop_setting("log_in_cookie_valid") = 0 Then
                    ld_validity_hours = Me.global_ws.get_webshop_setting("log_in_cookie_valid")
                End If
                Base.FrontEnd.frontend_page_base.WriteCookieUtc("UserLoginId", lc_user_id, ld_validity_hours)
            End If

            If ll_resume And Utilize.Data.API.Webshop.IntermediateServiceProcedures.get_setting("sync_cust_on_login") Then
                Dim task As System.Threading.Tasks.Task = Threading.Tasks.Task.Factory.StartNew(AddressOf update_account, Me.global_ws.user_information.ubo_customer)
            End If

            If ll_resume Then
                'Destroy the Categories helper Session, so a new will be created there
                If Not Session("UserCategoriesModel") Is Nothing Then
                    Session.Remove("UserCategoriesModel")
                End If
                If Not Cache.Get(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel") Is Nothing Then
                    Cache.Remove(Utilize.Data.API.Webshop.CategoriesHelper.CategoriesHelperUserId() & "_UserCategoriesModel")
                End If
            End If

            If ll_resume Then
                Dim lcUrlReferrer As String = Server.HtmlDecode(Me.get_page_parameter("RedirectUrl"))

                If String.IsNullOrEmpty(lcUrlReferrer) Then
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_purch_comb") = True Then
                        If Me.global_ws.purchase_combination.user_is_manager Then
                            Response.Redirect(Me.default_redirect_page, True)
                        End If
                    Else
                        If Utilize.Data.API.Webshop.DefaultDebtorHelper.user_has_default_debtor() Then
                            Me.default_redirect_page = "~/" + Me.global_ws.Language + "/webshop/product_list.aspx"
                        End If

                        Response.Redirect(Me.default_redirect_page, True)
                    End If
                Else
                    Response.Redirect(lcUrlReferrer, True)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub update_account(ByVal ubo_customer As Utilize.Data.BusinessObject)
        Dim lc_external_id As String = ubo_customer.field_get("av_nr")
        If Not String.IsNullOrEmpty(lc_external_id) Then
            Utilize.Data.API.Webshop.DebtorImport.LoginDebtorUpdate(ubo_customer, lc_external_id)
        End If
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_logon_page.css", Me)
        
        'UWS 1166 Get set the default redirect page 
        Me.default_redirect_page = "~/" + Me.global_ws.Language + "/account/account.aspx"
        If Me.global_ws.get_webshop_setting("redirect_home") Then
            Me.default_redirect_page = "~/home.aspx"
        End If
        'UWS-1146 END

        txt_logon_name.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_name", "Gebruikersnaam", Me.global_cms.Language))
        txt_logon_password.Attributes.Add("placeholder", Utilize.Web.Solutions.Translations.global_trans.translate_label("label_login_password", "Wachtwoord", Me.global_cms.Language))

        Me.link_forgot_password.Text = global_trans.translate_button("link_forgot_password", "Wachtwoord vergeten?", Me.global_cms.Language)
        Me.link_forgot_password.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/paymentprocess/request_password.aspx"

        Me.ph_alert.Visible = Not Me.get_page_parameter("redirecturl") = ""

        Me.ph_single_sign_on.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_sso")

        If ph_single_sign_on.Visible Then
            Dim qsc_single_sign_on_options As New Utilize.Data.QuerySelectClass
            With qsc_single_sign_on_options
                .select_fields = "*"
                .select_from = "uws_single_sign_on"
            End With
            Dim dt_single_sign_on As System.Data.DataTable = qsc_single_sign_on_options.execute_query

            If String.IsNullOrEmpty(Me.get_page_parameter("internal")) Then
                Dim dr_direct_login As System.Data.DataRow() = dt_single_sign_on.Select("use_as_login = 1")

                If dr_direct_login.Count > 0 Then
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_saml") Then
                        saml_login(dr_direct_login(0).Item("sso_code"))
                    End If
                End If
            End If

            Me.rpt_external_logins.DataSource = dt_single_sign_on
            Me.rpt_external_logins.DataBind()
        End If
    End Sub

    Private Sub uc_logon_page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.txt_logon_name.Focus()

        If Me.Page.IsPostBack() Then
            Me.txt_logon_password.Focus()
        End If
    End Sub

    Private Sub saml_login(id As String)
        Dim dr_sso_info As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow("uws_single_sign_on", id)

        Dim lc_endpoint As String = dr_sso_info.Item("saml_endpoint")
        Dim lc_entity_id As String = dr_sso_info.Item("saml_entity_id")
        Dim lc_redirect_url As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "Webshop/SingleSignOn/SamlAuthenticate.aspx?id=" + id.ToUpper

        Dim lo_request = New Saml.AuthRequest(lc_entity_id, lc_redirect_url)

        Dim lc_url As String = lo_request.GetRedirectUrl(lc_endpoint)

        Response.Redirect(lc_url)
    End Sub

    Private Sub rpt_external_logins_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_external_logins.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim lc_button_start_text As String = global_trans.translate_button("btn_ext_log_in", "Inloggen met {0}", Me.global_ws.Language)

            Dim btn_external_login As Utilize.Web.UI.WebControls.button = e.Item.FindControl("btn_external_login")
            btn_external_login.Text = String.Format(lc_button_start_text, e.Item.DataItem("sso_desc_" + Me.global_cms.Language))
        End If
    End Sub

    Private Sub rpt_external_logins_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rpt_external_logins.ItemCommand
        If e.CommandName = "external_login" Then
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_saml") Then
                saml_login(e.CommandArgument)
            End If
        End If
    End Sub
End Class
