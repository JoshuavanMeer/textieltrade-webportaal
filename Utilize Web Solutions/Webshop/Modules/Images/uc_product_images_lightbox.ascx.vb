﻿Imports System.Data

Partial Class uc_product_images_lightbox
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _product_code As String = ""

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property product_code() As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
        End Set
    End Property

    ' datatable aanmaken
    Private udt_product_data As System.Data.DataTable = Nothing

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' licentie controle
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_product_images_lightbox.css", Me)

            Me.set_style_sheet("~/_THMAssets/plugins/master-slider/quick-start/masterslider/style/masterslider.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/master-slider/quick-start/masterslider/skins/default/style.css", Me)

            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/jquery.fancybox.css", Me)
            Me.set_style_sheet("~/_THMAssets/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5", Me)

            Me.set_javascript("~/_THMAssets/js/plugins/master-slider.js", Me)
            Me.set_javascript("~/_THMAssets/plugins/master-slider/quick-start/masterslider/masterslider.min.js", Me)

            ' Set the fancybox javascript files
            Me.Page.Master.FindControl("ph_fancybox").Visible = True

            ' huidige taalcode ophalen
            Dim lc_language = Me.global_cms.Language

            ' Maak een nieuwe datatable aan
            Dim ws_productdetails As New ws_productdetails
            udt_product_data = ws_productdetails.get_product_details(product_code, Me.global_ws.Language)

            ' er mag maar een enkele product met deze code voorkomen
            Dim ll_resume_database = udt_product_data.Rows.Count > 0

            If ll_resume_database Then
                ' Bouw de lijst met afbeeldingen op
                image_list(Me._product_code)
            End If
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox", "jQuery(document).ready(function() {$('.fancybox').fancybox({openEffect  : 'fade', closeEffect : 'fade', prevEffect:  'fade', nextEffect:  'fade'});});", True)

        If ll_resume And Me.rpt_image_list.Items.Count > 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox1", "jQuery(document).ready(function () {MasterSliderShowcase2.initMasterSliderShowcase2();});", True)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox-button", "jQuery(document).ready(function() {$('.button-lightbox-zoom-in').click(function() {$('.ms-sl-selected .fancybox').click();});});", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LightBox-button", "jQuery(document).ready(function() {$('#enlarge-image').click(function() {$('#" + img_image.ClientID() + "').click();});});", True)
        End If
    End Sub

    Private Sub image_list(ByVal product_code As String)
        Dim ll_resume As Boolean = True

        ' datatable aanmaken
        If ll_resume Then
            Dim qsc_data_table As New Utilize.Data.QuerySelectClass
            qsc_data_table.select_fields = "*"
            qsc_data_table.select_from = "uws_product_images"
            qsc_data_table.select_order = "prod_code"
            qsc_data_table.select_where = "prod_code = @prod_code"
            qsc_data_table.add_parameter("prod_code", product_code)

            ' En laad de tabel
            Dim dt_data_table As System.Data.DataTable = qsc_data_table.execute_query()

            ' Voeg de hoofdafbeelding aan de tabel toe
            Dim dr_data_Row As System.Data.DataRow = dt_data_table.NewRow()
            ' Prepare clean not nullable options
            For Each column As System.Data.DataColumn In dt_data_table.Columns
                dr_data_Row.Item(column.ColumnName) = ""
            Next

            dr_data_Row.Item("rec_id") = Me.product_code
            dr_data_Row.Item("prod_code") = Me.product_code
            dr_data_Row.Item("image_desc_" + Me.global_ws.Language) = udt_product_data.Rows(0).Item("prod_desc")
            dr_data_Row.Item("thumbnail_normal") = udt_product_data.Rows(0).Item("thumbnail_normal")
            dr_data_Row.Item("thumbnail_large") = udt_product_data.Rows(0).Item("thumbnail_large")
            dr_data_Row.Item("image_normal") = udt_product_data.Rows(0).Item("image_normal")
            dr_data_Row.Item("image_large") = udt_product_data.Rows(0).Item("image_large")
            dr_data_Row.Item("image_small") = udt_product_data.Rows(0).Item("image_small")

            dt_data_table.Rows.InsertAt(dr_data_Row, 0)

            CheckImages(dt_data_table)

            Me.img_image.ImageUrl = Fingerprint.tag(Me.ResolveCustomUrl("~/" + dt_data_table.Rows(0).Item("image_large")))
            Me.img_image.AlternateText = dr_data_Row.Item("image_desc_" + Me.global_ws.Language).ToString().Replace("'", "").Replace("""", "")
            Me.img_fancybox.Attributes.Add("href", Me.ResolveCustomUrl("~/" + dt_data_table.Rows(0).Item("image_large")))

            Me.rpt_image_list.DataSource = dt_data_table
            Me.rpt_image_list.DataBind()

            Me.ph_image.Visible = Me.rpt_image_list.Items.Count = 1
            Me.rpt_image_list.Visible = Me.rpt_image_list.Items.Count > 1
        End If
    End Sub

    Private Sub CheckImages(ByRef dt As DataTable)
        Dim dt_copy As DataTable = dt.Copy()

        Dim ln_index As Integer = 0
        For Each dr As DataRow In dt_copy.Rows
            Dim dr_data_row As DataRow = dt.Rows(ln_index)

            dr_data_row.Item("thumbnail_normal") = Webshop.ws_images.get_image_path(dr_data_row.Item("thumbnail_normal"), "thumbnail_normal", Me)
            dr_data_row.Item("thumbnail_large") = Webshop.ws_images.get_image_path(dr_data_row.Item("thumbnail_large"), "thumbnail_large", Me)
            dr_data_row.Item("image_small") = Webshop.ws_images.get_image_path(dr_data_row.Item("image_small"), "image_small", Me)
            dr_data_row.Item("image_normal") = Webshop.ws_images.get_image_path(dr_data_row.Item("image_normal"), "image_normal", Me)
            dr_data_row.Item("image_large") = Webshop.ws_images.get_image_path(dr_data_row.Item("image_large"), "image_large", Me)

            ln_index += 1
        Next

        dt.AcceptChanges()
    End Sub

    Friend Function get_image_title(ByVal prod_code As String, ByVal prod_desc As String) As String
        If Me.global_ws.get_webshop_setting("prod_code_hide") = False Then
            Return prod_code.Trim() + " - " + prod_desc
        End If

        Return prod_desc
    End Function
End Class
