﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_images_lightbox.ascx.vb" Inherits="uc_product_images_lightbox" %>
<div class="uc_product_images_lightbox" id="gallery">
    <utilize:placeholder runat="server" ID="ph_image_list">
        <div class="ms-showcase2-template">
            <div class="master-slider ms-skin-default" id="masterslider">
                <utilize:repeater runat="server" ID="rpt_image_list">
                    <ItemTemplate>
                        <div class="ms-slide">
                            <img class="ms-brd fancybox" href="<%#Fingerprint.tag(Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "image_large")))%>" data-src="<%# Fingerprint.tag(Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "image_large")))%>" src="<%# Me.ResolveCustomUrl("~/DefaultImages/blank.gif")%>" alt="<%# DataBinder.Eval(Container.DataItem, "image_desc_" + Me.global_ws.Language).Replace("'", "").Replace("""", "") %>" data-fancybox-group="gallery" />
                            <img class="ms-thumb" src="<%# Fingerprint.tag(Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "image_normal")))%>" alt="<%# DataBinder.Eval(Container.DataItem, "image_desc_" + Me.global_ws.Language).Replace("'", "").Replace("""", "") %>" />
                        </div>
                    </ItemTemplate>
                </utilize:repeater>
                <button type="button" class="btn btn-lg button-lightbox-zoom-in" aria-label="Left Align">
                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_image">
        <a runat="server" id="img_fancybox" class="fancybox" rel="gallery4">
            <span><utilize:image runat="server" ID="img_image" CssClass="img-responsive full-width" /></span>
        </a>

        <button type="button" class="btn btn-lg button-lightbox-zoom-in single-image" aria-label="Left Align" id="enlarge-image">
            <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
        </button>
    </utilize:placeholder>
</div>