﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_products_relations_cross.ascx.vb" Inherits="uc_products_relations" %>

<utilize:placeholder runat="server" ID="ph_product_relations">
    <utilize:panel runat="server" ID="pnl_prod_rel_cross" class="uc_products_relations_cross">
        <input type="hidden" runat="server" id="hidden_item_size_relations" class="hidden_item_size" />

        <h2><utilize:translatetitle runat='server' ID="title_product_relations_cross" Text="Aanvullende producten"></utilize:translatetitle></h2>

         <div class="row">
            <div class="illustration-v2">
                <utilize:Repeater runat="server" ID="rpt_products">
                    <ItemTemplate>
                        <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>

                        <utilize:placeholder runat="server" ID="ph_clear_fix" Visible="false">
                                <div class="clearfix"></div>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_mobile_clear_fix" Visible="false">
                            <div class="mobile-clearfix"></div>
                        </utilize:placeholder>
                    </ItemTemplate>
                </utilize:Repeater>
            </div>
        </div>
    </utilize:panel>
    <div class="clearfix"></div>
</utilize:placeholder>