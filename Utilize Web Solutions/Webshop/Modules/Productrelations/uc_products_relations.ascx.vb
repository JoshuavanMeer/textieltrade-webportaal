﻿
Partial Class uc_products_relations
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Public Enum control_type
        blocks = 1
        list = 2
    End Enum

    Private _overview_type As control_type = control_type.blocks
    Public Property overview_type As control_type
        Get
            Return _overview_type
        End Get
        Set(value As control_type)
            _overview_type = value
        End Set
    End Property

    ' Obtaining the attribute product_code this is mandatory
    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            Me.DisplayRelations()
        End Set
    End Property

    Private dt_similar_products As System.Data.DataTable

    Protected Sub DisplayRelations()
        Me.set_style_sheet("uc_products_relations.css", Me)

        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.hidden_item_size_relations.Value = "col-md-" + Me.global_ws.get_webshop_setting("item_layout")
        End If

        If ll_resume Then
            ll_resume = Not _product_code = ""
        End If

        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_PROD_REL")

            If Not ll_resume Then
                Me.Visible = False
            End If
        End If

        If ll_resume Then
            Dim ws_product_filter As New ws_productfilter
            ws_product_filter.page_size = 10000
            ws_product_filter.extra_fields = "uws_prod_relations.rel_code"
            ws_product_filter.extra_join("full join", "uws_prod_relations", "uws_products.prod_code = uws_prod_relations.rel_code")
            ws_product_filter.extra_where += " and (uws_prod_relations.prod_code = '" + _product_code + "'"
             ws_product_filter.extra_where += " or (uws_products.prod_code <> '" + _product_code + "' and uws_prod_relations.prod_group in (select prod_group from uws_products where prod_code = '" + _product_code + "') ) )"
            ws_product_filter.extra_where += " and uws_prod_relations.rel_type = 5"
            ws_product_filter.include_sub_categories = False

            dt_similar_products = ws_product_filter.get_products(Me.global_ws.Language, "uws_products.prod_code")

            Me.rpt_products.DataSource = dt_similar_products
            Me.rpt_products.DataBind()

            ll_resume = Me.rpt_products.Items.Count > 0
        End If

        If Not ll_resume Then
            Me.ph_product_relations.Visible = False
        Else
            Me.ph_product_relations.Visible = True
        End If
    End Sub

    Private clearfixcount As Integer = 0
    Private mobile_clearfix As Integer = 0
    Protected Sub rpt_products_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_products.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            clearfixcount += CInt(Me.global_ws.get_webshop_setting("item_layout"))
            mobile_clearfix += 6

            If clearfixcount = 12 Then
                e.Item.FindControl("ph_clear_fix").Visible = True
                clearfixcount = 0
            End If

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clear_fix").Visible = True
                mobile_clearfix = 0
            End If

            Dim uc_product_block As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
            uc_product_block.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)

            Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
            ph_product_row.Controls.Add(uc_product_block)
        End If
    End Sub

    Private Sub uc_products_relations_Load(sender As Object, e As EventArgs) Handles Me.Load
        Select Case overview_type
            Case control_type.blocks
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType, Me.pnl_prod_rel.ClientID, "show_blocks_custom('#" + Me.pnl_prod_rel.ClientID + "');", True)
            Case control_type.list
            Case Else
                Exit Select
        End Select
    End Sub
End Class
