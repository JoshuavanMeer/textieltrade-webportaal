﻿Imports System.Net

Partial Class uc_company_logo
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Het bedrijfslogo is alleen zichtbaar wanneer een gebruiker is ingelogd.
            ll_resume = Me.global_ws.user_information.user_logged_on
        End If

        Dim lc_image_url As String = ""

        If ll_resume Then
            Me.set_style_sheet("uc_company_logo.css", Me)

            ' Ophalen afbeelding gekoppeld aan de klant bv. Document/CompanyLogos/logo.jpg
            lc_image_url = Utilize.Data.DataProcedures.GetValue("uws_customers", "logo", Me.global_ws.user_information.user_customer_id)

            ' Controleer of er een logo is vastgelegd
            ll_resume = Not String.IsNullOrEmpty(lc_image_url)
        End If

        ' Controleer of de url van de afbeelding te benaderen is (of de url aangemaakt is)
        If ll_resume Then
            ll_resume = check_image_url(Request.Url.GetLeftPart(UriPartial.Authority) + Me.ResolveCustomUrl("~/" + lc_image_url))
        End If

        ' Laden van de afbeelding in de pagina
        If ll_resume Then
            Me.company_logo.ImageUrl = Me.ResolveCustomUrl("~/" + lc_image_url)
        End If

        If Not ll_resume Then
            Me.Visible = False
        Else
            Me.Parent.FindControl("img_logo").Visible = False
        End If
    End Sub

    Public Function check_image_url(ByVal url As String) As Boolean
        Dim ll_resume As Boolean = True

        Try
            Dim request As HttpWebRequest = WebRequest.Create(url)
            request.Method = "HEAD"
            Dim response As HttpWebResponse = request.GetResponse()
            ll_resume = response.StatusCode = HttpStatusCode.OK
        Catch ex As Exception
            ll_resume = False
        End Try

        Return ll_resume
    End Function

End Class
