﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_freelist_filter.ascx.vb" Inherits="uc_product_freelist_filter" %>
<utilize:literal runat="server" ID="lt_url" Text="" Visible="false"></utilize:literal>

<div ID="divmain" runat="server" class="uc_product_freelist_filter">
    <utilize:placeholder runat="server" ID="ph_free_list1">
	<div class="free_list1">
        <h2><utilize:translatetitle runat="server" ID="title_free_list1" Text="Vrije lijst 1"></utilize:translatetitle></h2>
        <div class="free_list">
            <utilize:repeater runat="server" ID="rpt_free_list1">
                <ItemTemplate>
                    <div class="free_text_val">
                        <utilize:checkbox runat="server" ID="chk_free_list1" Text='<%# DataBinder.Eval(Container.DataItem, "fl_desc_" + Me.global_ws.Language)%>' />
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </div>
	</div>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_free_list2">
	<div class="free_list2">
        <h2><utilize:translatetitle runat="server" ID="title_free_list2" Text="Vrije lijst 2"></utilize:translatetitle></h2>
        <div class="free_list">
            <utilize:repeater runat="server" ID="rpt_free_list2">
                <ItemTemplate>
                    <div class="free_text_val">                    
                        <utilize:checkbox runat="server" ID="chk_free_list1" Text='<%# DataBinder.Eval(Container.DataItem, "fl_desc_" + Me.global_ws.Language)%>' />
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </div>
	</div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_free_list3">
	<div class="free_list3">
        <h2><utilize:translatetitle runat="server" ID="title_free_list3" Text="Vrije lijst 3"></utilize:translatetitle></h2>
        <div class="free_list">
            <utilize:repeater runat="server" ID="rpt_free_list3">
                <ItemTemplate>
                    <div class="free_text_val">
                        <utilize:checkbox runat="server" ID="chk_free_list1" Text='<%# DataBinder.Eval(Container.DataItem, "fl_desc_" + Me.global_ws.Language)%>' />
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </div>
	</div>
    </utilize:placeholder>

    <utilize:placeholder runat="server" ID="ph_free_list4">
	<div class="free_list4">
        <h2><utilize:translatetitle runat="server" ID="title_free_list4" Text="Vrije lijst 4"></utilize:translatetitle></h2>
        <div class="free_list">
            <utilize:repeater runat="server" ID="rpt_free_list4">
                <ItemTemplate>
                    <div class="free_text_val">
                        <utilize:checkbox runat="server" ID="chk_free_list1" Text='<%# DataBinder.Eval(Container.DataItem, "fl_desc_" + Me.global_ws.Language)%>' />
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </div>
	</div>
    </utilize:placeholder>
</div>