﻿Imports System.Linq

Partial Class uc_product_freelist_filter
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' Variabele die bepaalt of de control getoond moet worden
        Dim ll_resume As Boolean = Me.check_availability()

        If Not Me.IsPostBack() Then
            Me.lt_url.Text = Request.RawUrl.ToString()
        End If

        If ll_resume Then
            Dim ll_show As Boolean = False

            If ll_fl1_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freelist_values(1)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_list1.DataSource = udt_data_table
                    Me.rpt_free_list1.DataBind()
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_list1.Visible = False
                End If
            End If

            If ll_fl2_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freelist_values(2)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_list2.DataSource = udt_data_table
                    Me.rpt_free_list2.DataBind()
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_list2.Visible = False
                End If
            End If

            If ll_fl3_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freelist_values(3)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_list3.DataSource = udt_data_table
                    Me.rpt_free_list3.DataBind()
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_list3.Visible = False
                End If
            End If

            If ll_fl4_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freelist_values(4)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_list4.DataSource = udt_data_table
                    Me.rpt_free_list4.DataBind()
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_list4.Visible = False
                End If
            End If

            ll_resume = ll_show
        End If

        If Not ll_resume Then
            Me.Visible = False
        End If

        If ll_resume And Not Me.block_table = "" And Not Me.block_rec_id = "" Then
            Dim dr_temp_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

            If dr_temp_row.Item("freelist_advanced") Then
                Me.divmain.Attributes.Add("class", Me.divmain.Attributes.Item("class") + " advanced")
            End If
        End If
    End Sub
    Dim ll_fl1_avail As Boolean = False
    Dim ll_fl2_avail As Boolean = False
    Dim ll_fl3_avail As Boolean = False
    Dim ll_fl4_avail As Boolean = False

    Friend lc_url As String = ""

    ''' <summary>
    ''' Controleer hier of de control zichtbaar moet zijn of niet
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function check_availability() As Boolean
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim lc_cat_code As String = Me.get_page_parameter("cat_code")

            ll_resume = Not lc_cat_code = ""
        End If


        If ll_resume Then
            Dim lc_cat_code As String = Me.get_page_parameter("cat_code")

            ll_resume = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_type", lc_cat_code) = 2 Or Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_code", lc_cat_code, "cat_parent") = ""
        End If

        If ll_resume Then
            If Request.Url.ToString.Contains("product_list.aspx") Then
                lc_url = Request.RawUrl.ToString()
            Else
                lc_url = "~/Webshop/product_list.aspx"
            End If
            ' ll_resume = Request.Url.ToString.Contains("product_list.aspx")
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_product_freelist_filter.css", Me)
        End If

        If ll_resume Then
            ll_fl1_avail = Me.global_ws.get_webshop_setting("fl1_avail")
            ll_fl2_avail = Me.global_ws.get_webshop_setting("fl2_avail")
            ll_fl3_avail = Me.global_ws.get_webshop_setting("fl3_avail")
            ll_fl4_avail = Me.global_ws.get_webshop_setting("fl4_avail")

            ll_resume = ll_fl1_avail Or ll_fl2_avail Or ll_fl3_avail Or ll_fl4_avail
        End If

        If ll_resume Then
            Me.ph_free_list1.Visible = ll_fl1_avail
            Me.ph_free_list2.Visible = ll_fl2_avail
            Me.ph_free_list3.Visible = ll_fl3_avail
            Me.ph_free_list4.Visible = ll_fl4_avail
        End If

        Return ll_resume
    End Function

    Friend Function get_url(ByVal fl_field As String, ByVal fl_code As String) As String
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter(fl_field, fl_code)

        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Return lc_url
    End Function

    Private Function get_freelist_values(ByVal list_nr As Integer) As System.Data.DataTable
        Dim lc_cat_code As String = Me.get_page_parameter("cat_code")
        Dim lc_categories_string As String = "^" + lc_cat_code + "^" + Utilize.Web.Solutions.Webshop.ws_procedures.get_sub_categories(lc_cat_code)

        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_fields = "distinct uws_free_list" + list_nr.ToString() + ".*"
        qsc_select.select_from = "uws_free_list" + list_nr.ToString()

        qsc_select.add_join("left join", "uws_products", "uws_free_list" + list_nr.ToString() + ".fl_code = uws_products.fl_code" + list_nr.ToString())
        qsc_select.add_join("left join", "uws_categories_prod", "uws_products.prod_code = uws_categories_prod.prod_code")
        qsc_select.add_join("right join", "(select cat_code from uws_categories where @cat_string like '%^' + uws_categories.cat_code + '^%') as uws_categories", "uws_categories.cat_code = uws_categories_prod.cat_code")

        qsc_select.select_where = "uws_products.prod_show = 1"

        qsc_select.add_parameter("cat_string", lc_categories_string)

        ' Make the filter dynamic by adding where's according to the current page parameters
        If Me.global_ws.get_webshop_setting("filter_type") = 2 Then
            qsc_select.add_join("left join", "uws_products_tran", "uws_products.prod_code = uws_products_tran.prod_code")
            For ln_value As Integer = 1 To 4 Step 1
                If Not String.IsNullOrEmpty(Me.get_page_parameter("fl_code" + ln_value.ToString)) Then
                    qsc_select.select_where += " and uws_products_tran.prod_code in (select uws_products.prod_code from uws_products where uws_products.prod_show = 1 and uws_products.fl_code" + ln_value.ToString + " = @fl_code" + ln_value.ToString + ")"
                    qsc_select.add_parameter("fl_code" + ln_value.ToString, Me.get_page_parameter("fl_code" + ln_value.ToString))
                End If
            Next

            For ln_value As Integer = 1 To 10 Step 1
                If Not String.IsNullOrEmpty(Me.get_page_parameter("ff_desc" + ln_value.ToString)) Then
                    qsc_select.select_where += " and uws_products_tran.ff_desc" + ln_value.ToString + " = @ff_desc" + ln_value.ToString
                    qsc_select.add_parameter("ff_desc" + ln_value.ToString, Me.get_page_parameter("ff_desc" + ln_value.ToString))
                End If
            Next

            If Me.lt_url.Text.Contains("?") Then
                Dim dt_dictionary As New Dictionary(Of String, String)
                Dim lc_raw_url As String = Me.lt_url.Text.Split("?")(1)
                Dim lc_url() As String = lc_raw_url.Split("&")

                For ln_value As Integer = 1 To lc_url.Length - 1
                    Dim lc_text As String = lc_url(ln_value)
                    Dim ll_resume As Boolean = True

                    If String.IsNullOrEmpty(lc_text) Then
                        ll_resume = False
                    End If

                    If ll_resume Then
                        If Not lc_text.ToLower.Contains("page") And Not lc_text.ToLower.Contains("search") And Not lc_text.ToLower.Contains("sort") _
                            And Not lc_text.ToLower.Contains("language") And Not lc_text.ToLower.Contains("cat_parent") _
                            And Not lc_text.ToLower.Contains("ff_desc") And Not lc_text.ToLower.Contains("fl_code") Then

                            ' Check if the filter is selected
                            Dim lc_spec_code As String = lc_text.Split("=")(0).ToUpper()
                            Dim lc_spec_val As String = lc_text.Split("=")(1).ToUpper()

                            lc_spec_code = lc_spec_code.Replace("+", " ")
                            lc_spec_val = lc_spec_val.Replace("%20", " ")

                            ' Add the right spec_code and spec_val to the dictionary
                            dt_dictionary.Add(lc_spec_code, lc_spec_val)
                        End If
                    End If
                Next

                For Each entry In dt_dictionary
                    Dim key_var As String = entry.Key.Replace(" ", "")
                    Dim val_var As String = key_var + entry.Value.Replace(" ", "")
                    qsc_select.select_where += " and uws_products_tran.prod_code in (select prod_code from uws_prod_spec where (spec_code = @" + key_var + " and spec_val = @" + val_var + "))"
                    qsc_select.add_parameter(key_var, entry.Key)
                    qsc_select.add_parameter(val_var, entry.Value)
                Next
            End If
        End If

        Return qsc_select.execute_query()
    End Function

    Protected Sub rpt_free_text1_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_free_list1.ItemCreated, rpt_free_list2.ItemCreated, rpt_free_list3.ItemCreated, rpt_free_list4.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            ' Bepaal het nummer van de lijst
            Dim ln_list_nr As Integer = CInt(CType(sender, repeater).ID.Replace("rpt_free_list", ""))

            ' Referentie naar de filter en zet de checked
            Dim chk_filter As Utilize.Web.UI.WebControls.checkbox = e.Item.FindControl("chk_free_list1")

            Dim ll_active As Boolean
            If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                Dim lo_current_val As List(Of String) = Me.get_page_parameter("fl_code" + ln_list_nr.ToString()).Split("|").ToList

                ll_active = lo_current_val.Contains(e.Item.DataItem("fl_code"))
            Else
                ll_active = Me.get_page_parameter("fl_code" + ln_list_nr.ToString()) = e.Item.DataItem("fl_code")
            End If
            Dim lc_code_value As String = e.Item.DataItem("fl_code")

            If ll_active Then
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")

                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter("fl_code" + ln_list_nr.ToString())

                    url_class.set_parameter("fl_code" + ln_list_nr.ToString(), lc_current_val.Replace(lc_code_value + "|", ""))
                Else
                    url_class.set_parameter("fl_code" + ln_list_nr.ToString(), "")
                End If

                chk_filter.Checked = True
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            Else
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")
                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter("fl_code" + ln_list_nr.ToString())

                    url_class.set_parameter("fl_code" + ln_list_nr.ToString(), lc_current_val + lc_code_value + "|")
                Else
                    url_class.set_parameter("fl_code" + ln_list_nr.ToString(), Server.UrlEncode(lc_code_value))
                End If

                chk_filter.Checked = False
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            End If
        End If
    End Sub

End Class