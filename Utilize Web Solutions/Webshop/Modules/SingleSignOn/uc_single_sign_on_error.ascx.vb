﻿
Partial Class Webshop_Modules_SingleSignOn_uc_single_sign_on_error
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Sub Webshop_Modules_SingleSignOn_uc_single_sign_on_error_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_single_sign_on.css", Me)
        Dim lc_error_code As String = Me.get_page_parameter("errorcode")

        Dim lc_error_message As String = ""
        Select Case lc_error_code.ToUpper
            Case "BOOKING_ERROR"
                lc_error_message = global_trans.translate_text("text_booking_error", "U werkt niet in het juiste kantoor", Me.global_ws.Language)
        End Select
        lbl_error_message.Text = lc_error_message
    End Sub
End Class
