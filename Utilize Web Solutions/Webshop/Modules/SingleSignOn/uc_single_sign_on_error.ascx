﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_single_sign_on_error.ascx.vb" Inherits="Webshop_Modules_SingleSignOn_uc_single_sign_on_error" %>

<div class="uc_single_sign_on_error">
    <h1><utilize:translatetitle runat="server" ID="title_saml_error" Text="Er is iets fout gegaan bij de inlog"></utilize:translatetitle></h1>

    <utilize:literal runat="server" ID="lbl_error_message"></utilize:literal>
</div>