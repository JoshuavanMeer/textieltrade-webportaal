﻿Imports RestSharp
Imports Utilize.Elastic.Models
Imports System.Data
Imports System.Linq

Partial Class Webshop_Modules_ProductlistElastic_uc_product_list_elastic
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Sub Webshop_Modules_ProductlistElastic_uc_product_list_elastic_Init(sender As Object, e As EventArgs) Handles Me.Init

        Dim searchConfig = New ProductSearchRequest
        If Not String.IsNullOrEmpty(Me.get_page_parameter("cat_code")) Then
            searchConfig.Category = Me.get_page_parameter("cat_code")
        End If

        Dim filters As New List(Of String)

        Dim qsc_filters As New Utilize.Data.QuerySelectClass
        With qsc_filters
            .select_fields = "spec_code"
            .select_from = "uws_prod_spec_desc"
            .select_where = "spec_search = 1"
        End With

        Dim dt_filters As System.Data.DataTable = qsc_filters.execute_query
        If dt_filters.Rows.Count > 0 Then
            filters = dt_filters.AsEnumerable.Select(Function(x) x.Item("spec_code").ToString).ToList
        End If

        searchConfig.StringAggregations = filters

        Dim vuePageSizes As String = Utilize.FrameWork.FrameworkProcedures.ReadConfiguration("vue_page_sizes")
        Dim pageSizes = New String() {"32", "64", "96"}

        If Not String.IsNullOrEmpty(vuePageSizes) Then pageSizes = vuePageSizes.Split(",")

        Dim config = New JavaScriptConfig With {
            .CategoryName = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", Me.get_page_parameter("cat_code") + Me.global_ws.Language, "cat_code + lng_code"),
            .Language = Me.global_ws.Language.ToUpper,
            .From = 0,
            .Size = Convert.ToInt32(pageSizes.First),
            .BlockSize = Me.global_ws.get_webshop_setting("item_layout"),
            .Aggregations = True,
            .PageSizes = pageSizes
        }

        Dim translations As Object = New With {
            .label = New With {
                 .label_stock_indicator = global_trans.translate_label("label_stock_indicator", "", Me.global_ws.Language),
                 .label_showing_result = global_trans.translate_label("label_showing_results_elastic", "Product [1] van [2]", Me.global_ws.Language),
                 .label_product_code = global_trans.translate_label("label_product_code", "Productcode", Me.global_ws.Language),
                 .label_toggle_mobile_filters = global_trans.translate_label("label_toggle_mobile_filters", "Toon filters", Me.global_ws.Language),
                 .label_sales_action = global_trans.translate_label("label_sales_action", "Aanbieding", Me.global_ws.Language),
                 .label_no_products_found = global_trans.translate_label("label_no_products_found", "Er zijn geen producten gevonden", Me.global_ws.Language)
            }
        }

        If Not String.IsNullOrEmpty(Me.get_page_parameter("searchtext")) Then
            config.QueryString = Me.get_page_parameter("searchtext")
        End If

        Dim lc_list_code As String = Me.get_page_parameter("list_code")
        If Not String.IsNullOrEmpty(lc_list_code) Then
            Dim restrictionJson As String = Utilize.Data.DataProcedures.GetValue("uws_custom_prod_list", "restriction_json", lc_list_code)

            If Not String.IsNullOrEmpty(restrictionJson) Then
                Dim restriction = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Utilize.Data.API.Webshop.ProductListRestrictions)(restrictionJson)

                searchConfig.ExtraQueries.Add(GenerateListQuery(restriction.Field, restriction.Value))
            End If
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "initialize_elastic_list",
                                                "$(document).ready(function () { window.elastic = {" +
                                                "searchObject: " + Newtonsoft.Json.JsonConvert.SerializeObject(searchConfig) + "," +
                                                "config: " + Newtonsoft.Json.JsonConvert.SerializeObject(config) + "}});", True)

        ' Setup global variables that are used when vuejs mounts elastic search component
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupGlobalVueScope", "setupVueGlobalScope.addProperty('globalWs', 'language', 'NL');", True)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupElasticSearchModule", "setupVueGlobalScope.useModule('elasticSearch', {searchConfig:" + Newtonsoft.Json.JsonConvert.SerializeObject(searchConfig) + ", config: " + Newtonsoft.Json.JsonConvert.SerializeObject(config) + " });", True)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupElasticSearchTranslations", "setupVueGlobalScope.addTranslations(" + Newtonsoft.Json.JsonConvert.SerializeObject(translations) + ");", True)

    End Sub

    Private Function GenerateListQuery(field As String, value As String) As String
        If field.StartsWith("label-") Then
            Dim labelValue As String = field.Substring(6)
            Dim labelQuery As String = "{" +
                  """match"":  {" +
                    """labels"": """ + labelValue + """" +
                  "}" +
                "}"

            Return labelQuery
        End If

        Dim fieldType As Integer = Utilize.Data.DataProcedures.GetValue("utlz_dd_fields", "fld_type", field.Replace(".", ""), "dbf_name+fld_name")
        Dim fieldName As String = GetFieldName(field, fieldType)

        Dim actualValue As Object
        Select Case fieldType
            Case 1
                actualValue = """" + value + """"
            Case 5
                actualValue = (value = "1").ToString.ToLower
            Case Else
                actualValue = value
        End Select

        Dim query As String = "{" +
          """match"":  {" +
            """" + fieldName + """: " + actualValue.ToString +
          "}" +
        "}"

        Return query
    End Function

    Private Function GetFieldName(field As String, fieldtype As Integer) As String

        Dim table As String = field.Split(".")(0)
        Dim actualField As String = field.Split(".")(1)

        Select Case actualField.ToLower
            Case "prod_code"
                Return "id"
            Case "prod_desc"
                Return "shortDescription." + Me.global_ws.Language
            Case "prod_comm"
                Return "longDescription." + Me.global_ws.Language
            Case "prod_desc2"
                Return "subTitle." + Me.global_ws.Language
            Case "prod_price"
                Return "defaultPrice"
            Case Else
                Dim customFieldName As String = ""
                Select Case fieldtype
                    Case 1
                        customFieldName = "customStrings."
                    Case 2
                        customFieldName = "customDecimals."
                    Case 3
                        customFieldName = "customDateTimes."
                    Case 5
                        customFieldName = "customBooleans."
                    Case Else
                        customFieldName = "customStrings."
                End Select

                customFieldName += actualField

                Return customFieldName
        End Select

    End Function

    Private Class JavaScriptConfig
        Public Property CategoryName As String
        Public Property Language As String
        Public Property From As Integer
        Public Property Size As Integer
        Public Property QueryString As String
        Public Property BlockSize As Integer
        Public Property Aggregations As Boolean
        Public Property PageSizes As String()
    End Class
End Class
