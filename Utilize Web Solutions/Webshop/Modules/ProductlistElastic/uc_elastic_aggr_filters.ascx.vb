﻿
Partial Class Webshop_Modules_ProductlistElastic_uc_elastic_aggr_filters
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private Sub Webshop_Modules_ProductlistElastic_uc_elastic_aggr_filters_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim translations As Object = New With {
            .label = New With {
                 .label_filter_collapse_more = global_trans.translate_label("label_filter_collapse_more", "Toon meer", Me.global_ws.Language),
                 .label_filter_collapse_less = global_trans.translate_label("label_filter_collapse_less", "Toon minder", Me.global_ws.Language)
            }
        }

        ' Setup global variables that are used when vuejs mounts elastic search component
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupAggrFiltersModule", "setupVueGlobalScope.useModule('elasticAggrFilters');", True)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "setupAggrFilterTranslations", "setupVueGlobalScope.addTranslations(" + Newtonsoft.Json.JsonConvert.SerializeObject(translations) + ");", True)
    End Sub

End Class
