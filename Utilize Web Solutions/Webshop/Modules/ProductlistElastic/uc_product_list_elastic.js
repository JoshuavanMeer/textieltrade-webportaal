﻿function setOverview(id) {
    if (sessionStorage.getItem("view_state") === null) {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("onload_state") === "blocks") {
                show_blocks(id);
            }
            if (sessionStorage.getItem("onload_state") === "list") {
                show_list(id);
            }
        }
    } else {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("view_state") === "blocks") {
                show_blocks(id);
            }
            if (sessionStorage.getItem("view_state") === "list") {
                show_list(id);
            }
        }
    }
}
function set_start(id) {
    var element = id === undefined ? ".uc_product_list_elastic" : "#" + id;
    // Keep the overflow-title class if it was set
    var clss = " ";
    if ($(element +" .uc_placeholder").hasClass("overflow-title-1")) {
        clss = "overflow-title-1";
    } else if ($(element +" .uc_placeholder").hasClass("overflow-title-2")) {
        clss = "overflow-title-2";
    } else if ($(element +" .uc_placeholder").hasClass("overflow-title-3")) {
        clss = "overflow-title-3";
    } else if ($(element +" .uc_placeholder").hasClass("overflow-title-4")) {
        clss = "overflow-title-4";
    }

    if ($(element +" .uc_placeholder").hasClass("overflow-desc-1")) {
        clss += " overflow-desc-1";
    } else if ($(element +" .uc_placeholder").hasClass("overflow-desc-2")) {
        clss += " overflow-desc-2";
    } else if ($(element +" .uc_placeholder").hasClass("overflow-desc-3")) {
        clss += " overflow-desc-3";
    }

    if ($(element +" .uc_placeholder").hasClass("no-prod-code")) {
        clss += " no-prod-code";
    }

    $(element +"  .uc_placeholder").removeClass().addClass("uc_placeholder");

    if (clss !== " ") {
        $(element +" .uc_placeholder").addClass(clss);
    }

    $(element +"  .image-placeholder").removeClass().addClass("image-placeholder img-responsive");

    $(element +" .image-block").css("display", "block");
    $(element +" .item-border").css("display", "block");


    $(element +" .image-block").removeClass().addClass("image-block");
    $(element +" .information-block").removeClass().addClass("information-block");


    $(element +" .item-border").removeClass().addClass("item-border");
    $(element +" .containter-placeholder").removeClass().addClass("containter-placeholder");
    $(element +" .row-placeholder").removeClass().addClass("row-placeholder");

    $(element +" .list-only").css("display", "block");

    $(".fa-th").removeClass("active");
    $(".fa-bars").removeClass("active");
    $(".fa-th-list").removeClass("active");
}

function show_list(id) {
    var element = id === undefined ? ".uc_product_list_elastic" : "#" + id;
    set_start(id);
    var size = $(".uc_product_list_elastic .hidden_item_size").val();
    $(element + " .uc_placeholder").addClass("uc_product_row col-xs-6 col-md-12 col-sm-12");
    $(element + " .uc_product_block").removeClass("col-xs-6 col-sm-6 uc_product_block " + size);

    $(element + " .image-placeholder").addClass("sm-margin-bottom-20");
    $(element + " .image-block").addClass("col-sm-4");
    $(element + " .information-block").addClass("col-sm-8");


    $(element + " .content-placeholder").addClass("content col-xs-12");
    $(element + " .content-placeholder").removeClass("illustration-v2 row");

    $(element + " .item-border").addClass("list-product-description product-description-brd margin-bottom-30");
    $(element + " .containter-placeholder").addClass("container-fluid");
    $(element + " .row-placeholder").addClass("row");

    $(element + " .product_info_order").css("display", "none");

    $(".fa-th-list").addClass("active");

    sessionStorage.setItem("view_state", "list");
}

function show_blocks(mobile, id) {
    var element = id === undefined ? ".uc_product_list_elastic" : "#" + id;

    mobile = mobile || false;
    set_start();
    var size = $(".uc_product_list_elastic .hidden_item_size").val();
    $(element + " .uc_placeholder").addClass("col-xs-6 col-sm-6 uc_product_block " + size);
    $(element + " .image-placeholder").addClass("full-width");
    $(element + " .image-block").addClass("product-img product-img-brd");
    $(element + " .information-block").addClass("product-description-brd margin-bottom-30");
    $(element + " .content-placeholder").addClass("illustration-v2 col-xs-12");
    $(element + " .list-only").css("display", "none");
    $(".fa-th").addClass("active");
    if (!mobile) {
        sessionStorage.setItem("view_state", "blocks");
    }
}

function set_blocks() {
    sessionStorage.setItem("onload_state", "blocks");
    setOverview();
}

function set_list() {
    sessionStorage.setItem("onload_state", "list");
    setOverview();
}

$(window).resize(function () {
    if ($(window).width() < 991) {
        show_blocks(true);
    } else {
        setOverview();
    }
});