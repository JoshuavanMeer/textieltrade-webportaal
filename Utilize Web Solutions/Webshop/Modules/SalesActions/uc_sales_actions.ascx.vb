﻿
Partial Class uc_sales_actions
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Public Enum control_type
        blocks = 1
        list = 2
    End Enum

    Private _overview_type As control_type = control_type.blocks
    Public Property overview_type As control_type
        Get
            Return _overview_type
        End Get
        Set(value As control_type)
            _overview_type = value
        End Set
    End Property

    Private _products As System.Data.DataTable
    Private Const PageSize As Integer = 10000

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.set_style_sheet("uc_sales_actions.css", Me)

        Me.hidden_item_size.Value = "col-md-" + Me.item_size_int.ToString()

        Dim ll_resume As Boolean = True
        Dim dt_data_table As System.Data.DataTable = Nothing

        If Not Me.Page.IsPostBack() Then
            Me.lbl_raw_url.Text = Request.RawUrl.ToString()
        End If

        If ll_resume Then
            ' Haal hier op basis van de product filter class de aanbiedingen op
            Dim ws_product_filter As New ws_productfilter
            ws_product_filter.use_categories = False
            ws_product_filter.group_products = False
            ws_product_filter.page_size = PageSize

            If Me.block_id = "" Then
                ws_product_filter.extra_where = "and uws_products.prod_code in (select distinct prod_code from uws_sales_action_lin where hdr_id in (select top 1 rec_id from uws_sales_actions where GETDATE () >= sa_from and sa_till >= GETDATE () ORDER by sa_from desc)) "
            Else
                ws_product_filter.extra_where = "and uws_products.prod_code in (select distinct prod_code from uws_sales_action_lin where hdr_id in (select top 1 rec_id from uws_sales_actions where GETDATE () >= sa_from and sa_till >= GETDATE () and rec_id = '" + Me.block_id + "' ORDER by sa_from desc)) "
            End If

            If Not Me.global_ws.get_webshop_setting("lazy_load_products") Then
                dt_data_table = ws_product_filter.get_products(Me.global_ws.Language)

                ll_resume = dt_data_table.Rows.Count > 0

                If ll_resume Then
                    Me.rpt_products.DataSource = dt_data_table
                    Me.rpt_products.DataBind()
                End If
            Else
                _products = ws_product_filter.get_product_codes(Me.global_ws.Language, "prod_prio")
            End If
        End If

            If Not ll_resume Then
            Me.Visible = False
        End If
    End Sub

    Private _counter As Integer = 0
    Private mobile_clearfix As Integer = 0
    Protected Sub rpt_products_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_products.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            mobile_clearfix += 6

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clear_fix").Visible = True
                mobile_clearfix = 0
            End If

            Select Case overview_type
                Case control_type.blocks
                    Dim uc_product_block As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_block.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
                    uc_product_block.set_property("item_size_int", Me.item_size_int)

                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
                    ph_product_row.Controls.Add(uc_product_block)
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType, Me.pnl_sales_actions.ClientID, "show_blocks_custom('#" + Me.pnl_sales_actions.ClientID + "');", True)
                Case control_type.list
                    Dim uc_product_row As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
                    uc_product_row.set_property("item_size_int", Me.item_size_int)
                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")

                    ph_product_row.Controls.Add(uc_product_row)
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Private Sub uc_sales_actions_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.global_ws.get_webshop_setting("lazy_load_products") Then
            Dim li_products As New List(Of String)

            For Each product In _products.Rows
                li_products.Add(product.item("prod_code"))
            Next

            Dim lo_lazyLoad As New LazyLoadModel
            With lo_lazyLoad
                .Control = Me
                .LocationId = Me.sales_lazy_target.ClientID
                .MainElement = "#" + pnl_sales_actions.ClientID
                .Products = li_products
                .SpinnerId = Me.sales_lazy_load_spinner.ClientID
                .Url = Me.lbl_raw_url.Text.Substring(0, Me.lbl_raw_url.Text.LastIndexOf("/"))
                .PageSize = PageSize
                .RowAmount = Me.item_size_int
            End With

            LazyLoadHelper.InitializeLazyLoading(lo_lazyLoad)
        End If
    End Sub
End Class
