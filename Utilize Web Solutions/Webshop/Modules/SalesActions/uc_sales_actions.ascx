﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_sales_actions.ascx.vb" Inherits="uc_sales_actions" %>

<utilize:panel runat="server" ID="pnl_sales_actions" CssClass="uc_sales_actions margin-bottom-20">
    <h2><utilize:translatetitle runat="server" ID="title_sales_actions" Text="Aanbiedingen"></utilize:translatetitle></h2>

    <div class="row">
        <div class="illustration-v2">
            <input type="hidden" class="hidden_item_size" value="" runat="server" id="hidden_item_size"/>
            <utilize:label runat='server' ID="lbl_raw_url" Visible='false'></utilize:label>
            <div runat="server" id="sales_lazy_target"></div>
            <div runat="server" id="sales_lazy_load_spinner">
                <div class="loader-wrapper">
                    <div class="loader"></div>
                    <utilize:translatelabel runat="server" ID="label_loader_product_list" Text="Producten worden geladen..."></utilize:translatelabel>
                </div>
            </div>
            <asp:Repeater runat="server" ID="rpt_products">
                <ItemTemplate>
                        <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_clearfix" Visible="<%# me.clearfix() %>">
                        <div class="customclearfix"></div>
                    </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_mobile_clear_fix" Visible="false">
                        <div class="mobile-clearfix"></div>
                    </utilize:placeholder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</utilize:panel>