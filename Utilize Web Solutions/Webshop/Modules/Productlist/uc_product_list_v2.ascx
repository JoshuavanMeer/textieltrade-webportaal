﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_list_v2.ascx.vb" Inherits="uc_product_list_v2" %>

<utilize:panel ID="pnl_product_list" CssClass="uc_product_list_v2" runat="server">
    <h1><utilize:literal runat='server' ID="lt_current_category"></utilize:literal></h1>
    <utilize:placeholder runat="server" ID="ph_product_list">
        <div class="row">
             <div class="col-md-12">
                <!-- Sortingoptions -->
                <div>            
                    <input type="hidden" runat="server" id="hidden_item_size" class="hidden_item_size" value=""/>                    
                    <ul class="list-inline sorting-container">         
                        <li class="total-products-counter">
                            <span class="showing_count"><utilize:literal runat='server' ID="lt_product_count1"></utilize:literal></span>                            
                        </li>                        
                        <li class="items-per-page">
                            <utilize:label runat="server" CssClass="page_size" ID="label_items_per_page1"></utilize:label>
                            <div class="btn-group">
                                <utilize:dropdownlist runat='server' CssClass="cbo_items_per_page btn btn-default dropdown-toggle" ID="cbo_items_per_page1" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>
                        <li class="sort-by">
                            <utilize:label runat="server" CssClass="product_sort" ID="label_sort_by1"></utilize:label>
                            <div class="btn-group">
                                <utilize:dropdownlist runat='server' CssClass="cbo_sort_by btn btn-default dropdown-toggle" ID="cbo_sort_by1" AutoPostBack="true"></utilize:dropdownlist>
                            </div>
                        </li>
                        <li class="grid-list-icons hidden-sm hidden-xs ">     
                            <utilize:label runat="server" ID="test"></utilize:label>               
                            <a onclick="show_list(); return false;"><i class="fa fa-th-list"></i></a>
                            <a onclick="show_blocks(); return false;"><i class="fa fa-th"></i></a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div>

        <!-- content -->
        <div runat="server" ID="div_uc_product_list" class="row">
            <div class="content content-placeholder">
                <div class="row">
                    <div id="lazy-target"></div>
                    <div id="lazy-load-spinner">
                        <div class="loader-wrapper">
                            <div class="loader"></div>
                            <utilize:translatelabel runat="server" ID="label_loader_product_list" Text="Producten worden geladen..."></utilize:translatelabel>
                        </div>
                    </div>
                    <asp:Repeater runat="server" ID="rpt_product_list">
                        <ItemTemplate>
                            <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>

                            <utilize:placeholder runat="server" ID="ph_clear_fix" Visible="false">
                                <div class="clearfix"></div>
                            </utilize:placeholder>
                            <utilize:placeholder runat="server" ID="ph_mobile_clear_fix" Visible="false">
                                <div class="mobile-clearfix"></div>
                            </utilize:placeholder>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        
        <!-- Bulk order -->
        <utilize:placeholder runat="server" id="ph_bulk_order">
            <div class="buttons bulk-order">                
                <h3 class="bulk-order-title"><utilize:translatelabel runat="server" ID="label_bulk_order_desc" Text="Voeg alle producten uit dit overzicht toe aan uw winkelwagen"></utilize:translatelabel></h3>
                <utilize:translatebutton ID="button_bulk_order" CssClass="add_to_basket right btn-u btn-u-sea-shop" runat="server" Text="Bestellen"></utilize:translatebutton>
            </div>
        </utilize:placeholder>

        <!-- Pagination -->
        <div class="row">
             <div class="col-md-12">                 
                <utilize:placeholder runat='server' ID="ph_pager1"> 
                    <div class="text-center pagingwrapper" id="lower-paging">
                        <ul class="pagination">
                            <li class="li-previous">
                                <utilize:HyperLink ID="hl_pl_previous1" runat="server">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </utilize:HyperLink>
                            </li>
                            <utilize:literal runat='server' ID="lt_pager_1"></utilize:literal>
                            <li class="li-next">
                                <utilize:HyperLink ID="hl_pl_next1" runat="server">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </utilize:HyperLink>
                            </li>
                        </ul>
                    </div>
                </utilize:placeholder>                   
            </div>
        </div>

        <!-- Never shown -->
        <utilize:label runat='server' ID="lbl_raw_url" Visible='false'></utilize:label>
    </utilize:placeholder>

    <!-- no items text -->
    <utilize:placeholder runat="server" ID="ph_no_items">
        <div class="no_products_found">
            <utilize:translatetext runat='server' ID="text_no_products" Text="Er zijn geen producten gevonden."></utilize:translatetext>
        </div>
   
    </utilize:placeholder>
</utilize:panel>





