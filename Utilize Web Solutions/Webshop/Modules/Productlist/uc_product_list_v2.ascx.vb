﻿Imports System.Linq

Partial Class uc_product_list_v2
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _pf_product_filter As New ws_productfilter
    Private _page_nr As Integer = 0
    Private _page_item_count As Integer = 10

    Private _page_count As Integer = 0
    Private _page_nr_count As Integer = 8
    Private _page_nr_endcount As Integer = 2
    Private _sort_field As String = ""
    Private _bulk_order As Boolean = False
    Private _customer_product_codes As Boolean = False

    Private _search_text As String = ""

    Protected Friend _page_name As String = ""

    Private _page_range_1 As Integer = 0
    Private _page_range_2 As Integer = 0
    Private _page_range_3 As Integer = 0

    Private _product_codes As System.Data.DataTable

    ' Onderstaand object wordt ingelezen vanuit een sessie object welke gecreeerd wordt in Specifications/uc_product_spec_filter.ascx.vb
    ' Het sessie object is Session("ProductSpecifications")
    ' Deze wordt vanuit de sessie ingelezen wanneer er specificaties gezet worden
    Private _active_specifications As New System.Collections.Generic.List(Of KeyValuePair(Of String, String))

    Private Sub set_product_list()
        ' Bouw de product filter class op
        _pf_product_filter.category_code = Me.get_page_parameter("cat_code")
        _pf_product_filter.include_sub_categories = True

        Dim lc_search_text As String = _search_text
        Dim lc_range_start As String = Me.get_page_parameter("range_start")
        Dim lc_range_end As String = Me.get_page_parameter("range_end")

        If lc_range_end <> "" Then
            _pf_product_filter.price_range_start = Integer.Parse(lc_range_start)
            _pf_product_filter.price_range_end = Integer.Parse(lc_range_end)
        End If

        _pf_product_filter.prod_desc = lc_search_text
        _pf_product_filter.prod_code = lc_search_text
        _pf_product_filter.prod_desc2 = lc_search_text
        _pf_product_filter.prod_comm = lc_search_text
        _pf_product_filter.prod_keywords = lc_search_text
        _pf_product_filter.brand_code = Me.get_page_parameter("brand")

        If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
            For i As Integer = 1 To 4
                Dim lo_values As List(Of String) = Me.get_page_parameter("fl_code" + i.ToString).Split("|").ToList

                _pf_product_filter.add_free_list_code_list("fl_code" + i.ToString, lo_values)
            Next
        Else
            _pf_product_filter.fl_code1 = Me.get_page_parameter("fl_code1")
            _pf_product_filter.fl_code2 = Me.get_page_parameter("fl_code2")
            _pf_product_filter.fl_code3 = Me.get_page_parameter("fl_code3")
            _pf_product_filter.fl_code4 = Me.get_page_parameter("fl_code4")
        End If

        _pf_product_filter.fl_desc1 = Me.get_page_parameter("fl_desc1")
        _pf_product_filter.fl_desc1_type = Webshop.filter_type.contains
        _pf_product_filter.fl_desc2 = Me.get_page_parameter("fl_desc2")
        _pf_product_filter.fl_desc2_type = Webshop.filter_type.contains
        _pf_product_filter.fl_desc3 = Me.get_page_parameter("fl_desc3")
        _pf_product_filter.fl_desc3_type = Webshop.filter_type.contains
        _pf_product_filter.fl_desc4 = Me.get_page_parameter("fl_desc4")
        _pf_product_filter.fl_desc4_type = Webshop.filter_type.contains

        If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
            For i As Integer = 1 To 10
                Dim lo_values As List(Of String) = Me.get_page_parameter("ff_desc" + i.ToString).Split("|").ToList

                _pf_product_filter.add_free_text_list("ff_desc" + i.ToString, lo_values)
            Next
        Else
            _pf_product_filter.ff_desc1 = Me.get_page_parameter("ff_desc1")
            _pf_product_filter.ff_desc1_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc2 = Me.get_page_parameter("ff_desc2")
            _pf_product_filter.ff_desc2_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc3 = Me.get_page_parameter("ff_desc3")
            _pf_product_filter.ff_desc3_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc4 = Me.get_page_parameter("ff_desc4")
            _pf_product_filter.ff_desc4_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc5 = Me.get_page_parameter("ff_desc5")
            _pf_product_filter.ff_desc5_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc6 = Me.get_page_parameter("ff_desc6")
            _pf_product_filter.ff_desc6_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc7 = Me.get_page_parameter("ff_desc7")
            _pf_product_filter.ff_desc7_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc8 = Me.get_page_parameter("ff_desc8")
            _pf_product_filter.ff_desc8_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc9 = Me.get_page_parameter("ff_desc9")
            _pf_product_filter.ff_desc9_type = Webshop.filter_type.contains
            _pf_product_filter.ff_desc10 = Me.get_page_parameter("ff_desc10")
            _pf_product_filter.ff_desc10_type = Webshop.filter_type.contains
        End If

        ' Lees de gezette filters uit
        If Not Session("ProductSpecifications") Is Nothing Then
            _active_specifications = Session("ProductSpecifications")
            Session("ProductSpecifications") = Nothing
        End If

        For Each Pair As KeyValuePair(Of String, String) In _active_specifications
            If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                _pf_product_filter.add_specification_list(Pair.Key, Pair.Value.Split("|").ToList)
            Else
                _pf_product_filter.add_specification_free_desc(Pair.Key, Pair.Value)
            End If
        Next

        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_group") = True Then
            Dim ln_group_products As Integer = Me.global_ws.get_webshop_setting("group_products")
            Select Case ln_group_products
                Case 0
                    ' Geen groepering
                Case 1
                    ' Standaard groepering op shp_code
                    _pf_product_filter.group_products = Webshop.group_types.group
                    Me.div_uc_product_list.Attributes("class") += " clickthrough-btns"
                Case 2
                    ' Standaard groepering op waarde 1
                    _pf_product_filter.group_products = Webshop.group_types.group_val1
                    Me.div_uc_product_list.Attributes("class") += " clickthrough-btns"
                Case 3
                    ' Standaard groepering op waarde 2
                    _pf_product_filter.group_products = Webshop.group_types.group_val2
                    Me.div_uc_product_list.Attributes("class") += " clickthrough-btns"
                Case Else
                    Exit Select
            End Select

        End If

        ' In de webshop instellingen kun je aangeven of je alle producten in een lijst wil tonen
        ' Als deze aan staat, dan een maximale page size instellen.
        If Me.global_ws.get_webshop_setting("show_all_products") = True Then
            _page_item_count = 1000

            ' En het aantal resultaten per pagina heeft dan ook geen zin meer dus verbergen
            Me.label_items_per_page1.Visible = False
            Me.cbo_items_per_page1.Visible = False

            Me.ph_pager1.Visible = False
        End If

        _pf_product_filter.page_size = _page_item_count

        ' Probeer het paginanummer uit te lezen
        If Not Me.get_page_parameter("page") = "" Then
            Try
                _page_nr = CInt(Me.get_page_parameter("page"))
            Catch ex As Exception
                _page_nr = 0
            End Try
        End If

        ' Zet het paginanummer in de filter class
        _pf_product_filter.page_number = _page_nr

        Dim lc_sort_field As String = ""

        Dim lc_list_code As String = Me.get_page_parameter("list_code")
        If Not String.IsNullOrEmpty(lc_list_code) Then
            Dim restrictionJson As String = Utilize.Data.DataProcedures.GetValue("uws_custom_prod_list", "restriction_json", lc_list_code)

            If Not String.IsNullOrEmpty(restrictionJson) Then
                Dim restriction = Newtonsoft.Json.JsonConvert.DeserializeObject(Of Utilize.Data.API.Webshop.ProductListRestrictions)(restrictionJson)

                If Not restriction.Field.StartsWith("label-") Then
                    _pf_product_filter.extra_where += " and " + restriction.Field + " = " + "@restriction_value"
                    _pf_product_filter.AddParameter("restriction_value", restriction.Value)
                Else
                    _pf_product_filter.extra_where += " and uws_products.prod_code in (select prod_code from uws_product_labels where label_code = @label_code)"
                    _pf_product_filter.AddParameter("label_code", restriction.Field.Substring(6))
                End If
            End If
        End If

            Select Case _sort_field
            Case "prod_prio"
                lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"
            Case "prod_price_asc"
                lc_sort_field = "uws_products.prod_price asc, uws_products.prod_prio desc"
            Case "prod_price_desc"
                lc_sort_field = "uws_products.prod_price desc, uws_products.prod_prio desc"
            Case Else
                lc_sort_field = "uws_products.prod_prio desc, uws_products.prod_code"
        End Select

        ' Voer de query uit en bouw de repeater op
        If Not Me.global_ws.get_webshop_setting("lazy_load_products") Then
            Me.rpt_product_list.DataSource = _pf_product_filter.get_products(Me.global_ws.Language, lc_sort_field)
            Me.rpt_product_list.DataBind()
        Else
            _product_codes = _pf_product_filter.get_product_codes(Me.global_ws.Language, lc_sort_field)
        End If


        ' Sla het aantal pagina's op in een variabele
        _page_count = _pf_product_filter.page_count


        Me.ph_product_list.Visible = True
        Me.ph_no_items.Visible = False

        If Me.rpt_product_list.Items.Count = 0 Then
            Me.ph_product_list.Visible = True
            Me.ph_no_items.Visible = False
        End If
    End Sub
    Private Sub set_pager_text()
        ' Zorg hier dat het juiste aantal getoond wordt in de pager
        Dim ln_product_count As Integer = _pf_product_filter.product_count

        Dim ln_from As Integer = IIf(_page_nr = 0, 1, _page_nr * _page_item_count)
        Dim ln_till As Integer = IIf(_page_nr = 0, _page_item_count, (_page_nr * _page_item_count) + _page_item_count)

        If ln_till > ln_product_count Then
            ln_till = ln_product_count
        End If

        If ln_from > ln_till Then
            ln_from = ln_till
        End If

        Dim lc_product_text As String = global_trans.translate_label("label_showing_results_v2", "Product [1] van [2]", Me.global_ws.Language)
        lc_product_text = lc_product_text.Replace("[1]", ln_from.ToString() + " - " + ln_till.ToString())
        lc_product_text = lc_product_text.Replace("[2]", _pf_product_filter.product_count.ToString())

        Me.lt_product_count1.Text = lc_product_text

        Me.lt_current_category.Text = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "cat_desc", Me.get_page_parameter("cat_code") + Me.global_ws.Language, "cat_code + lng_code")

        Me.label_items_per_page1.Text = global_trans.translate_label("label_items_per_page_v2", "Toon", Me.global_cms.Language)

        Me.label_sort_by1.Text = global_trans.translate_label("label_sort_by", "Sorteer op", Me.global_cms.Language)
    End Sub

    Private Function get_page_start(ByVal page_nr As Integer) As Integer
        ' _page_nr => Huidige pagina
        ' _page_count => Aantal pagina's
        ' _page_nr_count => Aantal pagina's tonen
        ' _page_nr_endcount => Aantal pagina's op het eind

        Dim ln_page_start As Integer = (page_nr - System.Math.Floor(_page_nr_count / 2))

        If (ln_page_start + _page_nr_count) > _page_count Then
            ln_page_start = _page_count - _page_nr_count
        End If

        If ln_page_start < 0 Then
            ln_page_start = 0
        End If

        Return ln_page_start
    End Function

    Private Function get_page_end(ByVal page_nr As Integer) As Integer
        Dim ln_page_end As Integer = Me.get_page_start(page_nr) + _page_nr_count

        If ln_page_end > _page_count Then
            ln_page_end = _page_count
        End If

        Return ln_page_end
    End Function

    Private Sub set_pager_navigation(ByVal empty_page As Boolean)
        If Not empty_page Then
            Dim page_start As Integer = Me.get_page_start(_page_nr)
            Dim page_end As Integer = Me.get_page_end(_page_nr)

            Me.set_page_parameter("pagesize", Me._page_item_count)

            Dim lc_pager_string As String = ""

            ' Bouw hier de standaard pagina's op
            For page_iterations As Integer = page_start To page_end - 1
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", page_iterations)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Dim lc_css_class As String = ""
                If page_iterations = _page_nr Then
                    lc_css_class = "active"
                End If

                If page_iterations = page_start And Not page_iterations + 1 = 1 Then
                    Dim url_class_start As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                    url_class_start.set_parameter("pagesize", Me._page_item_count)
                    url_class_start.set_parameter("page", 0)
                    Dim lc_url_start As String = Me.ResolveCustomUrl(url_class_start.get_url())
                    If page_iterations = 1 Then
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_start + """>" + "1" + "</a></li>"
                    Else
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_start + """>" + "1" + "</a></li>" + "<li class=""" + lc_css_class + """><a href="""">" + "..." + "</a></li>"
                    End If
                End If

                lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url + """>" + (page_iterations + 1).ToString() + "</a></li>"

                If page_iterations = page_end - 1 And Not page_iterations = _page_count - 1 Then
                    Dim url_class_end As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                    url_class_end.set_parameter("pagesize", Me._page_item_count)
                    url_class_end.set_parameter("page", _page_count - 1)
                    Dim lc_url_end As String = Me.ResolveCustomUrl(url_class_end.get_url())
                    If page_iterations = _page_count - 2 Then
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href=""" + lc_url_end + """>" + _page_count.ToString() + "</a></li>"
                    Else
                        lc_pager_string += "<li class=""" + lc_css_class + """><a href="""">" + "..." + "</a></li>" + "<li class=""" + lc_css_class + """><a href=""" + lc_url_end + """>" + _page_count.ToString() + "</a></li>"
                    End If
                End If
            Next

            Me.lt_pager_1.Text = lc_pager_string

            If _page_nr > 0 Then
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", _page_nr - 1)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Me.hl_pl_previous1.NavigateUrl = lc_url
                Me.hl_pl_previous1.CssClass = "previous"

            Else
                Me.hl_pl_previous1.NavigateUrl = ""
                Me.hl_pl_previous1.CssClass = "previous hide_me"

            End If

            If _page_nr < (_page_count - 1) Then
                ' Dit is de url class waar je parameters kunt wijzigen en een nette url wordt gegenereerd
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
                url_class.set_parameter("pagesize", Me._page_item_count)
                url_class.set_parameter("page", _page_nr + 1)
                Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

                Me.hl_pl_next1.NavigateUrl = lc_url
                Me.hl_pl_next1.CssClass = "next"

            Else
                Me.hl_pl_next1.NavigateUrl = ""
                Me.hl_pl_next1.CssClass = "next hide_me"

            End If
        Else
            Me.hl_pl_previous1.CssClass = "previous hide_me"
            Me.hl_pl_next1.CssClass = "next hide_me"

        End If
        'als er maar 1 pagina aanwezig is hide de pager
        If _page_count = 1 Then
            Me.ph_pager1.Visible = False
        End If
    End Sub
    Private Sub set_pager_ranges()
        Dim dt_data_table As New System.Data.DataTable

        ' Maak een nieuwe datacolumn aan
        Dim dc_column As New System.Data.DataColumn
        dc_column.ColumnName = "page_size"
        dc_column.DataType = System.Type.GetType("System.Int32")

        dt_data_table.Columns.Add(dc_column)

        Dim dr_data_row5 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row5.Item("page_size") = _page_range_1
        dt_data_table.Rows.Add(dr_data_row5)

        Dim dr_data_row10 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row10.Item("page_size") = _page_range_2
        dt_data_table.Rows.Add(dr_data_row10)

        Dim dr_data_row15 As System.Data.DataRow = dt_data_table.NewRow()
        dr_data_row15.Item("page_size") = _page_range_3
        dt_data_table.Rows.Add(dr_data_row15)

        Me.cbo_items_per_page1.DataTextField = "page_size"
        Me.cbo_items_per_page1.DataValueField = "page_size"
        Me.cbo_items_per_page1.DataSource = dt_data_table
        Me.cbo_items_per_page1.DataBind()

        Try
            Me.cbo_items_per_page1.SelectedValue = Me.get_page_parameter("pagesize")

            _page_item_count = Me.get_page_parameter("pagesize")
        Catch ex As Exception
            _page_item_count = _page_range_1
        End Try
    End Sub
    Private Sub set_sort_by_items()
        Dim dt_data_table As New System.Data.DataTable

        ' Maak een nieuwe datacolumn aan
        Dim dc_field As New System.Data.DataColumn
        dc_field.ColumnName = "sort_field"
        dc_field.DataType = System.Type.GetType("System.String")

        dt_data_table.Columns.Add(dc_field)

        ' Maak een nieuwe datacolumn aan
        Dim dc_text As New System.Data.DataColumn
        dc_text.ColumnName = "sort_text"
        dc_text.DataType = System.Type.GetType("System.String")

        dt_data_table.Columns.Add(dc_text)

        Dim dr_data_row1 As System.Data.DataRow = Nothing
        dr_data_row1 = dt_data_table.NewRow()
        dr_data_row1.Item("sort_field") = "prod_prio"
        dr_data_row1.Item("sort_text") = global_trans.translate_label("label_sort_priority", "Standaard", Me.global_ws.Language)
        dt_data_table.Rows.Add(dr_data_row1)

        If Me.global_ws.get_webshop_setting("prices_not_logged_on") = False Or Me.global_ws.user_information.user_logged_on Then
            dr_data_row1 = dt_data_table.NewRow()
            dr_data_row1.Item("sort_field") = "prod_price_asc"
            dr_data_row1.Item("sort_text") = global_trans.translate_label("label_price_ascending", "Prijs - Oplopend", Me.global_ws.Language)
            dt_data_table.Rows.Add(dr_data_row1)

            dr_data_row1 = dt_data_table.NewRow()
            dr_data_row1.Item("sort_field") = "prod_price_desc"
            dr_data_row1.Item("sort_text") = global_trans.translate_label("label_price_descending", "Prijs - Aflopend", Me.global_ws.Language)
            dt_data_table.Rows.Add(dr_data_row1)
        End If

        Me.cbo_sort_by1.DataTextField = "sort_text"
        Me.cbo_sort_by1.DataValueField = "sort_field"
        Me.cbo_sort_by1.DataSource = dt_data_table
        Me.cbo_sort_by1.DataBind()

        Try
            Me.cbo_sort_by1.SelectedValue = Me.get_page_parameter("Sort")

            _sort_field = Me.get_page_parameter("Sort")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Dim ll_resume As Boolean = True

        If ll_resume Then
            Dim ln_minimum_characters = Me.global_ws.get_webshop_setting("search_count")

            If ln_minimum_characters < 0 Then
                ln_minimum_characters = 0
            End If

            _search_text = Me.get_page_parameter("searchtext")

            ll_resume = _search_text.Length >= ln_minimum_characters Or String.IsNullOrEmpty(_search_text)

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "search_text", "alert('" + global_trans.translate_message("message_search_text", "Uw zoekterm moet uit minimaal [1] tekens bestaan.", Me.global_ws.Language).Replace("[1]", ln_minimum_characters.ToString()) + "');", True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "search_reopen", "$('.uc_content_search').css({display: 'block'});", True)
            End If
        End If

        If ll_resume Then
            Try
                _page_range_1 = CInt(global_trans.translate_label("label_product_count_1", "15", Me.global_ws.Language))
            Catch ex As Exception
                _page_range_1 = 15
            End Try

            Try
                _page_range_2 = CInt(global_trans.translate_label("label_product_count_2", "30", Me.global_ws.Language))
            Catch ex As Exception
                _page_range_2 = 30
            End Try

            Try
                _page_range_3 = CInt(global_trans.translate_label("label_product_count_3", "45", Me.global_ws.Language))
            Catch ex As Exception
                _page_range_3 = 45
            End Try


            Me.set_javascript("uc_product_list_v2.js", Me)

            Me.hidden_item_size.Value = "col-md-" + Me.global_ws.get_webshop_setting("item_layout")

            If Me.global_ws.get_webshop_setting("product_list_type") = 2 Then
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "set_blocks", "set_blocks();", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "set_list", "set_list();", True)
            End If

            If Not Me.global_ws.user_information.user_logged_on Then
                ' De bestelknop is zichtbaar wanneer je niet bent ingelogd en de instelling bestellen wanneer niet ingelogd aan staat.
                Me.ph_bulk_order.Visible = Me.global_ws.get_webshop_setting("order_not_logged_on")
                ' Als je niet bent ingelogd, dan is deze onzichtbaar, anders is deze obv licentie
                Me._customer_product_codes = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_prod")
            End If

            If Me.button_bulk_order.Visible Then
                ' Toon de knop bulk order als deze zichtbaar is
                _bulk_order = Me.global_ws.get_webshop_setting("bulk_order")
                'Me.button_bulk_order.Visible = _bulk_order
                Me.ph_bulk_order.Visible = _bulk_order

                ' Voeg een class toe zodat we hierop de bestel knop per product blok kunnen stylen
                If _bulk_order Then
                    Me.div_uc_product_list.Attributes("class") += " bulk_order_enabled"
                End If
            End If

            ' Sla de url op in een onzichtbare label
            If Not Me.Page.IsPostBack() Then
                Me.lbl_raw_url.Text = Request.RawUrl.ToString()
            End If

            Me._page_name = Me.Page.AppRelativeVirtualPath

            Me.set_style_sheet("uc_product_list_v2.css", Me)

            If Not Request.QueryString.Count > 1 Then
                Me.set_pager_ranges()
                Me.set_sort_by_items()
                Me.set_product_list()
                Me.set_pager_text()
                Me.set_pager_navigation(False)
            Else
                Me.set_pager_ranges()
                Me.set_sort_by_items()
                Me.set_product_list()
                Me.set_pager_text()
                Me.set_pager_navigation(False)
            End If
        End If

        If Me._pf_product_filter.product_count = 0 Or ll_resume = False Then
            Me.ph_no_items.Visible = True
            Me.ph_product_list.Visible = False
        End If

    End Sub

    Protected Sub cbo_items_per_page1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_items_per_page1.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("pagesize", Me.cbo_items_per_page1.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub

    Protected Sub cbo_sort_by1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_sort_by1.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lbl_raw_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter("sort", Me.cbo_sort_by1.SelectedValue)
        Dim lc_url As String = Me.ResolveCustomUrl(url_class.get_url())

        Response.Redirect(lc_url, True)
    End Sub

    Private clearfixcount As Integer = 0
    Private mobile_clearfix As Integer = 0
    Protected Sub rpt_product_list_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_product_list.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then

            clearfixcount += CInt(Me.global_ws.get_webshop_setting("item_layout"))
            mobile_clearfix += 6

            If clearfixcount = 12 Then
                e.Item.FindControl("ph_clear_fix").Visible = True
                clearfixcount = 0
            End If

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clear_fix").Visible = True
                mobile_clearfix = 0
            End If

            Dim uc_product_row As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
            uc_product_row.ID = "uc_product_row"
            uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
            uc_product_row.set_property("bulk_order", Me._bulk_order)
            uc_product_row.set_property("customer_product_codes", Me._customer_product_codes)

            Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
            ph_product_row.Controls.Add(uc_product_row)
        End If
    End Sub

    Protected Sub button_bulk_order_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_bulk_order.Click
        ' Nu gaan we alle items in de repeater nalopen en de producten in het winkelwagentje doen
        Dim ll_resume As Boolean = True

        ' Eerst gaan we controleren of er correcte aantallen zijn ingevuld
        If ll_resume Then
            For Each rpt_item As UI.WebControls.RepeaterItem In rpt_product_list.Items
                Dim uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control = rpt_item.FindControl("uc_product_row").FindControl("uc_order_product")

                Dim lc_order_quantity As String = "1"

                '  Als het om verkoopeenheden gaat, dan moeten er een ander textbox uitgelezen worden
                If uc_order_product.get_property("sales_unit") = True Then
                    '  Als het om verkoopeenheden gaat
                    lc_order_quantity = CType(uc_order_product.FindControl("txt_quantity_sale_unit"), textbox).Text
                Else
                    '  Het gaat niet om verkoopeenheden
                    lc_order_quantity = CType(uc_order_product.FindControl("txt_quantity_normal"), textbox).Text
                End If

                Try
                    Dim li_quantity As Integer = CInt(lc_order_quantity)

                    ll_resume = li_quantity >= 0
                Catch ex As Exception
                    ll_resume = False
                End Try

                ' Als een waarde niet goed is ingevuld, dan houden we meteen op
                If Not ll_resume Then
                    Exit For
                End If
            Next

            If Not ll_resume Then
                ' Een of meer aantallen zijn niet goed ingevuld, melding tonen
                Dim lc_error_message As String = global_trans.translate_message("message_invalid_amounts", "Er is een ongeldige waarde ingevoerd", Me.global_cms.Language)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_error", "alert('" + lc_error_message + "'); ", True)
            End If
        End If

        If ll_resume Then
            ' Alle ingevulde aantallen zijn correct, voeg de items toe aan de winkelwagen
            For Each rpt_item As UI.WebControls.RepeaterItem In rpt_product_list.Items
                Dim uc_order_product As Utilize.Web.Solutions.Webshop.ws_user_control = rpt_item.FindControl("uc_product_row").FindControl("uc_order_product")

                Dim lc_product_code As String = uc_order_product.get_property("product_code")
                Dim lc_order_quantity As String = "1"

                '  Als het om verkoopeenheden gaat dan moet er een ander textbox uitgelezen worden
                If uc_order_product.get_property("sales_unit") = True Then
                    '  Als het om verkoopeenheden gaat
                    lc_order_quantity = CType(uc_order_product.FindControl("txt_quantity_sale_unit"), textbox).Text
                Else
                    '  Het gaat niet om verkoopeenheden
                    lc_order_quantity = CType(uc_order_product.FindControl("txt_quantity_normal"), textbox).Text
                End If

                Dim li_quantity As Integer = CInt(lc_order_quantity)

                If li_quantity > 0 Then
                    Me.global_ws.shopping_cart.product_add(lc_product_code, li_quantity, "", "", "")
                End If
            Next
        End If

        If ll_resume Then
            ' Als er in de webshop instellingen is aangegegeven dat je na het bestellen direct naar de winkelwagen gaat, dan omleiden.
            If Me.global_ws.get_webshop_setting("order_redirect") = True Then
                Response.Redirect("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx", True)
            Else
                ' Toon een melding dat de aantallen aan de winkelwagen zijn toegevoegd
                Dim lc_add_message As String = global_trans.translate_message("message_items_added", "De producten zijn aan de winkelwagen toegevoegd.", Me.global_cms.Language)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "order_message", "alert('" + lc_add_message + "'); ", True)
            End If
        End If

    End Sub

    Private Sub uc_product_list_v2_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.global_ws.get_webshop_setting("lazy_load_products") Then
            Dim li_products As New List(Of String)

            For Each row In _product_codes.Rows
                li_products.Add(row.item("prod_code"))
            Next

            Dim lo_lazyLoad As New LazyLoadModel
            With lo_lazyLoad
                .Control = Me
                .LocationId = "lazy-target"
                .MainElement = ""
                .Products = li_products
                .SpinnerId = "lazy-load-spinner"
                .Url = Me.lbl_raw_url.Text.Substring(0, Me.lbl_raw_url.Text.LastIndexOf("/"))
                .PageSize = _page_item_count
                .RowAmount = Me.global_ws.get_webshop_setting("item_layout")
            End With

            LazyLoadHelper.InitializeLazyLoading(lo_lazyLoad)
        End If
    End Sub
End Class

