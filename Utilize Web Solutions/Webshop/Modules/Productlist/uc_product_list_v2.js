﻿function setOverview() {
    if (sessionStorage.getItem("view_state") === null) {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("onload_state") === "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("onload_state") === "list") {
                show_list();
            }
        }
    } else {
        if ($(window).width() < 991) {
            show_blocks(true);
        } else {
            if (sessionStorage.getItem("view_state") === "blocks") {
                show_blocks();
            }
            if (sessionStorage.getItem("view_state") === "list") {
                show_list();
            }
        }
    }
}
function set_start() {
    // Keep the overflow-title class if it was set
    var clss = " ";
    if ($(".uc_placeholder").hasClass("overflow-title-1")) {
        clss = "overflow-title-1";
    } else if ($(".uc_placeholder").hasClass("overflow-title-2")) {
        clss = "overflow-title-2";
    } else if ($(".uc_placeholder").hasClass("overflow-title-3")) {
        clss = "overflow-title-3";
    } else if ($(".uc_placeholder").hasClass("overflow-title-4")) {
        clss = "overflow-title-4";
    }

    if ($(".uc_placeholder").hasClass("overflow-desc-1")) {
        clss += " overflow-desc-1";
    } else if ($(".uc_placeholder").hasClass("overflow-desc-2")) {
        clss += " overflow-desc-2";
    } else if ($(".uc_placeholder").hasClass("overflow-desc-3")) {
        clss += " overflow-desc-3";
    }

    if ($(".uc_placeholder").hasClass("no-prod-code")) {
        clss += " no-prod-code";
    }

    $(".uc_product_list_v2 .uc_placeholder").removeClass().addClass("uc_placeholder");

    if (clss !== " ") {
        $(".uc_placeholder").addClass(clss);
    }

    $(".uc_product_list_v2 .image-placeholder").removeClass().addClass("image-placeholder img-responsive");

    $(".uc_product_list_v2 .image-block").css("display", "block");
    $(".uc_product_list_v2 .item-border").css("display", "block");


    $(".uc_product_list_v2 .image-block").removeClass().addClass("image-block");

    $(".uc_product_list_v2 .information-block").removeClass().addClass("information-block");


    $(".uc_product_list_v2 .item-border").removeClass().addClass("item-border");
    $(".uc_product_list_v2 .containter-placeholder").removeClass().addClass("containter-placeholder");
    $(".uc_product_list_v2 .row-placeholder").removeClass().addClass("row-placeholder");

    $(".uc_product_list_v2 .list-only").css("display", "block");

    $(".fa-th").removeClass("active");
    $(".fa-bars").removeClass("active");
    $(".fa-th-list").removeClass("active");
}

function show_list() {
    set_start();
    var size = $(".uc_product_list_v2 .hidden_item_size").val();
    $(".uc_product_list_v2 .uc_placeholder").addClass("uc_product_row col-xs-6 col-md-12 col-sm-12");
    $(".uc_product_list_v2 .uc_product_block").removeClass("col-xs-6 col-sm-6 uc_product_block " + size);

    $(".uc_product_list_v2 .image-placeholder").addClass("sm-margin-bottom-20");

    $(".uc_product_list_v2 .image-block").addClass("col-sm-4");

    $(".uc_product_list_v2 .information-block").addClass("col-sm-8");


    $(".uc_product_list_v2 .content-placeholder").addClass("content col-xs-12");
    $(".uc_product_list_v2 .content-placeholder").removeClass("illustration-v2 row");

    $(".uc_product_list_v2 .item-border").addClass("list-product-description product-description-brd margin-bottom-30");
    $(".uc_product_list_v2 .containter-placeholder").addClass("container-fluid");
    $(".uc_product_list_v2 .row-placeholder").addClass("row");

    $(".uc_product_list_v2 .product_info_order").css("display", "none");

    $(".fa-th-list").addClass("active");

    sessionStorage.setItem("view_state", "list");
}

function show_blocks(mobile) {
    mobile = mobile || false;
    set_start();
    var size = $(".uc_product_list_v2 .hidden_item_size").val();
    $(".uc_product_list_v2 .uc_placeholder").addClass("col-xs-6 col-sm-6 uc_product_block " + size);

    $(".uc_product_list_v2 .image-placeholder").addClass("full-width");

    $(".uc_product_list_v2 .image-block").addClass("product-img product-img-brd");

    $(".uc_product_list_v2 .information-block").addClass("product-description-brd margin-bottom-30");

    $(".uc_product_list_v2 .content-placeholder").addClass("illustration-v2 col-xs-12");

    $(".uc_product_list_v2 .list-only").css("display", "none");

    $(".fa-th").addClass("active");
    if (!mobile) {
        sessionStorage.setItem("view_state", "blocks");
    }
}

function set_blocks() {
    sessionStorage.setItem("onload_state", "blocks");
    setOverview();
}

function set_list() {
    sessionStorage.setItem("onload_state", "list");
    setOverview();
}

$(window).resize(function () {
    if ($(window).width() < 991) {
        show_blocks(true);
    } else {
        setOverview();
    }
});