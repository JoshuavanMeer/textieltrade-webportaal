﻿
Imports System.Linq

Partial Class uc_product_freetext_filter
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Dim ll_ff1_avail As Boolean = False
    Dim ll_ff2_avail As Boolean = False
    Dim ll_ff3_avail As Boolean = False
    Dim ll_ff4_avail As Boolean = False
    Dim ll_ff5_avail As Boolean = False
    Dim ll_ff6_avail As Boolean = False
    Dim ll_ff7_avail As Boolean = False
    Dim ll_ff8_avail As Boolean = False
    Dim ll_ff9_avail As Boolean = False
    Dim ll_ff10_avail As Boolean = False

    Public Enum FilterTypes
        Checkboxes = 0
        Dropdownlist = 1
    End Enum

    Private _FilterType As FilterTypes = FilterTypes.Checkboxes
    Public Property FilterType As FilterTypes
        Get
            Return _FilterType
        End Get
        Set(value As FilterTypes)
            _FilterType = value
        End Set
    End Property

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.IsPostBack() Then
            Me.lt_url.Text = Request.RawUrl.ToString()
        End If

        ' Variabele die bepaalt of de control getoond moet worden
        Dim ll_resume As Boolean = Me.check_availability()

        If ll_resume Then
            Dim ll_show As Boolean = False

            If ll_ff1_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(1)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text1.DataSource = udt_data_table
                    Me.rpt_free_text1.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text1.DataValueField = "ff_desc1"
                    Me.cbo_free_text1.DataTextField = "ff_desc1"
                    Me.cbo_free_text1.DataSource = udt_data_table
                    Me.cbo_free_text1.DataBind()

                    Me.cbo_free_text1.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))

                    If Not Me.IsPostBack() Then
                        Me.cbo_free_text1.SelectedValue = Me.get_page_parameter("ff_desc1")
                    End If
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text1.Visible = False
                End If
            End If

            If ll_ff2_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(2)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text2.DataSource = udt_data_table
                    Me.rpt_free_text2.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text2.DataValueField = "ff_desc2"
                    Me.cbo_free_text2.DataTextField = "ff_desc2"
                    Me.cbo_free_text2.DataSource = udt_data_table
                    Me.cbo_free_text2.DataBind()

                    Me.cbo_free_text2.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text2.SelectedValue = Me.get_page_parameter("ff_desc2")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text2.Visible = False
                End If
            End If

            If ll_ff3_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(3)

                ' Als er waarden gevonden zijn
                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text3.DataSource = udt_data_table
                    Me.rpt_free_text3.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text3.DataValueField = "ff_desc3"
                    Me.cbo_free_text3.DataTextField = "ff_desc3"
                    Me.cbo_free_text3.DataSource = udt_data_table
                    Me.cbo_free_text3.DataBind()

                    Me.cbo_free_text3.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text3.SelectedValue = Me.get_page_parameter("ff_desc3")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text3.Visible = False
                End If
            End If

            If ll_ff4_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(4)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text4.DataSource = udt_data_table
                    Me.rpt_free_text4.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text4.DataValueField = "ff_desc4"
                    Me.cbo_free_text4.DataTextField = "ff_desc4"
                    Me.cbo_free_text4.DataSource = udt_data_table
                    Me.cbo_free_text4.DataBind()

                    Me.cbo_free_text4.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text4.SelectedValue = Me.get_page_parameter("ff_desc4")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text4.Visible = False
                End If
            End If

            If ll_ff5_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(5)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text5.DataSource = udt_data_table
                    Me.rpt_free_text5.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text5.DataValueField = "ff_desc5"
                    Me.cbo_free_text5.DataTextField = "ff_desc5"
                    Me.cbo_free_text5.DataSource = udt_data_table
                    Me.cbo_free_text5.DataBind()

                    Me.cbo_free_text5.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text5.SelectedValue = Me.get_page_parameter("ff_desc5")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text5.Visible = False
                End If
            End If

            If ll_ff6_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(6)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text6.DataSource = udt_data_table
                    Me.rpt_free_text6.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text6.DataValueField = "ff_desc6"
                    Me.cbo_free_text6.DataTextField = "ff_desc6"
                    Me.cbo_free_text6.DataSource = udt_data_table
                    Me.cbo_free_text6.DataBind()

                    Me.cbo_free_text6.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text6.SelectedValue = Me.get_page_parameter("ff_desc6")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text6.Visible = False
                End If
            End If

            If ll_ff7_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(7)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text7.DataSource = udt_data_table
                    Me.rpt_free_text7.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text7.DataValueField = "ff_desc7"
                    Me.cbo_free_text7.DataTextField = "ff_desc7"
                    Me.cbo_free_text7.DataSource = udt_data_table
                    Me.cbo_free_text7.DataBind()

                    Me.cbo_free_text7.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text7.SelectedValue = Me.get_page_parameter("ff_desc7")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text7.Visible = False
                End If
            End If

            If ll_ff8_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(8)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text8.DataSource = udt_data_table
                    Me.rpt_free_text8.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text8.DataValueField = "ff_desc8"
                    Me.cbo_free_text8.DataTextField = "ff_desc8"
                    Me.cbo_free_text8.DataSource = udt_data_table
                    Me.cbo_free_text8.DataBind()

                    Me.cbo_free_text8.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text8.SelectedValue = Me.get_page_parameter("ff_desc8")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text8.Visible = False
                End If
            End If

            If ll_ff9_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(9)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text9.DataSource = udt_data_table
                    Me.rpt_free_text9.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text9.DataValueField = "ff_desc9"
                    Me.cbo_free_text9.DataTextField = "ff_desc9"
                    Me.cbo_free_text9.DataSource = udt_data_table
                    Me.cbo_free_text9.DataBind()

                    Me.cbo_free_text9.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text9.SelectedValue = Me.get_page_parameter("ff_desc9")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text9.Visible = False
                End If
            End If

            If ll_ff10_avail Then
                ' Haal de waarden van de vrije lijst op
                Dim udt_data_table As System.Data.DataTable = Me.get_freetext_values(10)

                If udt_data_table.Rows.Count > 0 Then
                    ll_show = True

                    ' Vul de repeater
                    Me.rpt_free_text10.DataSource = udt_data_table
                    Me.rpt_free_text10.DataBind()

                    ' Vul de uitklaplijst
                    Me.cbo_free_text10.DataValueField = "ff_desc10"
                    Me.cbo_free_text10.DataTextField = "ff_desc10"
                    Me.cbo_free_text10.DataSource = udt_data_table
                    Me.cbo_free_text10.DataBind()

                    Me.cbo_free_text10.Items.Insert(0, New System.Web.UI.WebControls.ListItem(global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language), ""))
                    Me.cbo_free_text10.SelectedValue = Me.get_page_parameter("ff_desc10")
                Else
                    ' Geen regels gevonden, onzichtbaar maken
                    Me.ph_free_text10.Visible = False
                End If
            End If

            ll_resume = ll_show
        End If

        If Not ll_resume Then
            Me.Visible = False
        Else
            Me.set_style_sheet("uc_product_freetext_filter.css", Me)

            Me.divmain.Attributes.Add("class", Me.divmain.Attributes.Item("class") + " " + Me.GetFilterType())

            If Not Me.block_table = "" And Not Me.block_rec_id = "" Then
                Dim dr_temp_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

                If dr_temp_row.Item("freetext_advanced") Then
                    Me.divmain.Attributes.Add("class", Me.divmain.Attributes.Item("class") + " advanced")
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Controleer hier of de control zichtbaar moet zijn of niet
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function check_availability() As Boolean
        Dim ll_resume As Boolean = True

        If Request.Url.ToString.ToLower().Contains("product_list.aspx") Then
            If ll_resume Then
                Dim lc_cat_code As String = Me.get_page_parameter("cat_code")

                ll_resume = Not lc_cat_code = ""
            End If

            If ll_resume Then
                Dim lc_cat_code As String = Me.get_page_parameter("cat_code")

                ll_resume = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_type", lc_cat_code) = 2 Or Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_code", lc_cat_code, "cat_parent") = ""
            End If
        End If

        If ll_resume Then
            ll_ff1_avail = Me.global_ws.get_webshop_setting("ff1_filter_avail")
            ll_ff2_avail = Me.global_ws.get_webshop_setting("ff2_filter_avail")
            ll_ff3_avail = Me.global_ws.get_webshop_setting("ff3_filter_avail")
            ll_ff4_avail = Me.global_ws.get_webshop_setting("ff4_filter_avail")
            ll_ff5_avail = Me.global_ws.get_webshop_setting("ff5_filter_avail")
            ll_ff6_avail = Me.global_ws.get_webshop_setting("ff6_filter_avail")
            ll_ff7_avail = Me.global_ws.get_webshop_setting("ff7_filter_avail")
            ll_ff8_avail = Me.global_ws.get_webshop_setting("ff8_filter_avail")
            ll_ff9_avail = Me.global_ws.get_webshop_setting("ff9_filter_avail")
            ll_ff10_avail = Me.global_ws.get_webshop_setting("ff10_filter_avail")

            ll_resume = ll_ff1_avail Or ll_ff2_avail Or ll_ff3_avail Or ll_ff4_avail Or ll_ff5_avail Or ll_ff6_avail Or ll_ff7_avail Or ll_ff8_avail Or ll_ff9_avail Or ll_ff10_avail
        End If

        If ll_resume Then
            Me.ph_free_text1.Visible = ll_ff1_avail
            Me.ph_free_text2.Visible = ll_ff2_avail
            Me.ph_free_text3.Visible = ll_ff3_avail
            Me.ph_free_text4.Visible = ll_ff4_avail
            Me.ph_free_text5.Visible = ll_ff5_avail
            Me.ph_free_text6.Visible = ll_ff6_avail

            Me.ph_free_text7.Visible = ll_ff7_avail
            Me.ph_free_text8.Visible = ll_ff8_avail
            Me.ph_free_text9.Visible = ll_ff9_avail
            Me.ph_free_text10.Visible = ll_ff10_avail
        End If

        Return ll_resume
    End Function

    Friend Function get_url(ByVal ff_field As String, ByVal ff_desc As String) As String
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
        url_class.set_parameter("page", "0")
        url_class.set_parameter(ff_field, ff_desc)

        Return Me.ResolveCustomUrl(url_class.get_url())
    End Function

    Private Function get_freetext_values(ByVal list_nr As Integer) As System.Data.DataTable
        Dim qsc_select As New Utilize.Data.QuerySelectClass
        qsc_select.select_from = "uws_products_tran"
        qsc_select.select_fields = "distinct CASE WHEN ISNUMERIC(REPLACE(ff_desc" + list_nr.ToString() + ", ',', '.')) = 1 Then CAST(REPLACE(ff_desc" + list_nr.ToString() + ", ',', '.') As FLOAT) When ISNUMERIC(LEFT(ff_desc" + list_nr.ToString() + ",1)) = 0 Then ASCII(LEFT(LOWER(ff_desc" + list_nr.ToString() + "),1)) Else 2147483647 End, ff_desc" + list_nr.ToString()
        qsc_select.select_where = " lng_code = @lng_code And prod_code In (Select prod_code from uws_products where prod_show = 1) "

        Select Case True
            Case Request.Url.ToString.ToLower.Contains("product_list.aspx")
                qsc_select.select_where += "And prod_code In (Select prod_code from uws_categories_prod where cat_code = @cat_code) "
                qsc_select.add_parameter("cat_code", Me.get_page_parameter("cat_code"))
            Case Request.Url.ToString.ToLower.Contains("customer_products.aspx")
                qsc_select.select_where += "And prod_code In (Select prod_code from uws_customer_product where cust_id = @cust_id) "
                qsc_select.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)
            Case Request.Url.ToString.ToLower.Contains("customer_user_products.aspx")
                qsc_select.select_where += "And prod_code In (Select prod_code from uws_user_products where user_id = @user_id) "
                qsc_select.add_parameter("user_id", Me.global_ws.purchase_combination.user_id)
        End Select


        qsc_select.select_where += " And ff_desc" + list_nr.ToString() + " <> ''"
        qsc_select.select_order = "CASE WHEN ISNUMERIC(REPLACE(ff_desc" + list_nr.ToString() + ", ',', '.')) = 1 THEN CAST(REPLACE(ff_desc" + list_nr.ToString() + ", ',', '.') AS FLOAT) WHEN ISNUMERIC(LEFT(ff_desc" + list_nr.ToString() + ",1)) = 0 THEN ASCII(LEFT(LOWER(ff_desc" + list_nr.ToString() + "),1)) ELSE 2147483647 END "

        qsc_select.add_parameter("lng_code", Me.global_ws.Language)

        ' set dynamic filters
        If Me.global_ws.get_webshop_setting("filter_type") = 2 Then
            For ln_value As Integer = 1 To 10 Step 1
                If Not String.IsNullOrEmpty(Me.get_page_parameter("ff_desc" + ln_value.ToString)) Then
                    qsc_select.select_where += " and uws_products_tran.ff_desc" + ln_value.ToString + " = @ff_desc" + ln_value.ToString
                    qsc_select.add_parameter("ff_desc" + ln_value.ToString, Me.get_page_parameter("ff_desc" + ln_value.ToString))
                End If
            Next

            For ln_value As Integer = 1 To 4 Step 1
                If Not String.IsNullOrEmpty(Me.get_page_parameter("fl_code" + ln_value.ToString)) Then
                    qsc_select.select_where += " and uws_products_tran.prod_code in (select uws_products.prod_code from uws_products where uws_products.prod_show = 1 and uws_products.fl_code" + ln_value.ToString + " = @fl_code" + ln_value.ToString + ")"
                    qsc_select.add_parameter("fl_code" + ln_value.ToString, Me.get_page_parameter("fl_code" + ln_value.ToString))
                End If
            Next

            If Me.lt_url.Text.Contains("?") Then
                Dim dt_dictionary As New Dictionary(Of String, String)
                Dim lc_raw_url As String = Me.lt_url.Text.Split("?")(1)
                Dim lc_url() As String = lc_raw_url.Split("&")

                For ln_value As Integer = 1 To lc_url.Length - 1
                    Dim lc_text As String = lc_url(ln_value)
                    Dim ll_resume As Boolean = True

                    If String.IsNullOrEmpty(lc_text) Then
                        ll_resume = False
                    End If

                    If ll_resume Then
                        If Not lc_text.ToLower.Contains("page") And Not lc_text.ToLower.Contains("search") And Not lc_text.ToLower.Contains("sort") _
                            And Not lc_text.ToLower.Contains("language") And Not lc_text.ToLower.Contains("cat_parent") _
                            And Not lc_text.ToLower.Contains("ff_desc") And Not lc_text.ToLower.Contains("fl_code") Then

                            ' Check if the filter is selected
                            Dim lc_spec_code As String = lc_text.Split("=")(0).ToUpper()
                            Dim lc_spec_val As String = lc_text.Split("=")(1).ToUpper()

                            lc_spec_code = lc_spec_code.Replace("+", " ")
                            lc_spec_val = lc_spec_val.Replace("%20", " ")

                            ' Add the right spec_code and spec_val to the dictionary
                            dt_dictionary.Add(lc_spec_code, lc_spec_val)
                        End If
                    End If
                Next

                For Each entry In dt_dictionary
                    Dim key_var As String = entry.Key.Replace(" ", "")
                    Dim val_var As String = key_var + entry.Value.Replace(" ", "")
                    qsc_select.select_where += " and uws_products_tran.prod_code in (select prod_code from uws_prod_spec where (spec_code = @" + key_var + " and spec_val = @" + val_var + "))"
                    qsc_select.add_parameter(key_var, entry.Key)
                    qsc_select.add_parameter(val_var, entry.Value)
                Next
            End If
        End If

        Return qsc_select.execute_query()
    End Function

    Protected Sub rpt_free_text1_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_free_text1.ItemCreated, rpt_free_text2.ItemCreated, rpt_free_text3.ItemCreated, rpt_free_text4.ItemCreated, rpt_free_text5.ItemCreated, rpt_free_text6.ItemCreated, rpt_free_text7.ItemCreated, rpt_free_text8.ItemCreated, rpt_free_text9.ItemCreated, rpt_free_text10.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.Item Or e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Then
            ' Bepaal het nummer van de lijst
            Dim ln_list_nr As Integer = CInt(CType(sender, repeater).ID.Replace("rpt_free_text", ""))

            ' Referentie naar de filter en zet de checked
            Dim chk_filter As Utilize.Web.UI.WebControls.checkbox = e.Item.FindControl("chk_free_text1")

            Dim ll_active As Boolean

            If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                Dim lo_current_val As List(Of String) = Me.get_page_parameter("ff_desc" + ln_list_nr.ToString()).ToLower.Split("|").ToList

                ll_active = lo_current_val.Contains(e.Item.DataItem("ff_desc" + ln_list_nr.ToString()).ToLower)
            Else
                ll_active = Me.get_page_parameter("ff_desc" + ln_list_nr.ToString()).ToLower = e.Item.DataItem("ff_desc" + ln_list_nr.ToString()).ToLower
            End If
            Dim lc_desc_value As String = e.Item.DataItem("ff_desc" + ln_list_nr.ToString())

            If ll_active Then
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")
                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter("ff_desc" + ln_list_nr.ToString())

                    url_class.set_parameter("ff_desc" + ln_list_nr.ToString(), lc_current_val.Replace(lc_desc_value + "|", ""))
                Else
                    url_class.set_parameter("ff_desc" + ln_list_nr.ToString(), "")
                End If

                chk_filter.Checked = True
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            Else
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
                url_class.set_parameter("page", "0")

                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter("ff_desc" + ln_list_nr.ToString())

                    url_class.set_parameter("ff_desc" + ln_list_nr.ToString(), lc_current_val + lc_desc_value + "|")
                Else
                    url_class.set_parameter("ff_desc" + ln_list_nr.ToString(), Server.UrlEncode(lc_desc_value))
                End If

                chk_filter.Checked = False
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            End If
        End If
    End Sub

    Protected Sub cbo_free_text_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_free_text1.SelectedIndexChanged, cbo_free_text2.SelectedIndexChanged, cbo_free_text3.SelectedIndexChanged, cbo_free_text4.SelectedIndexChanged, cbo_free_text5.SelectedIndexChanged, cbo_free_text6.SelectedIndexChanged, cbo_free_text7.SelectedIndexChanged, cbo_free_text8.SelectedIndexChanged, cbo_free_text9.SelectedIndexChanged, cbo_free_text10.SelectedIndexChanged
        Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)
        url_class.set_parameter("page", "0")

        For ln_list_nr As Integer = 1 To 10
            Dim cbo_free_text As dropdownlist = Me.FindControl("cbo_free_text" + ln_list_nr.ToString())

            If cbo_free_text.SelectedValue = "" Then
                url_class.remove_parameter("ff_desc" + ln_list_nr.ToString())
            Else
                url_class.set_parameter("ff_desc" + ln_list_nr.ToString(), Server.UrlEncode(cbo_free_text.SelectedValue))
            End If
        Next

        Dim lc_url As String = url_class.get_url()

        For ln_list_nr As Integer = 1 To 10
            Dim cbo_free_text As dropdownlist = Me.FindControl("cbo_free_text" + ln_list_nr.ToString())

            If Not cbo_free_text.SelectedValue = "" Then
                lc_url = lc_url.Replace("ff_desc" + ln_list_nr.ToString() + "=" + Server.UrlEncode(cbo_free_text.SelectedValue).ToLower(), "ff_desc" + ln_list_nr.ToString() + "=" + Server.UrlEncode(cbo_free_text.SelectedValue))
            End If
        Next

        Response.Redirect(lc_url, True)
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Me.rpt_free_text1.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text2.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text3.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text4.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text5.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text6.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text7.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text8.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text9.Visible = Me._FilterType = FilterTypes.Checkboxes
        Me.rpt_free_text10.Visible = Me._FilterType = FilterTypes.Checkboxes

        Me.cbo_free_text1.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text2.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text3.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text4.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text5.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text6.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text7.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text8.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text9.Visible = Me._FilterType = FilterTypes.Dropdownlist
        Me.cbo_free_text10.Visible = Me._FilterType = FilterTypes.Dropdownlist
    End Sub

    Friend Function GetToggle(ByVal list_nr As Integer) As String
        Dim lcReturnValue As String = ""

        If Not Me.get_page_parameter("ff_desc" + list_nr.ToString()) = "" Then
            lcReturnValue = "in"
        Else
            If Me._FilterType = FilterTypes.Dropdownlist Then
                lcReturnValue = "in"
            End If

        End If

        Return lcReturnValue
    End Function

    Friend Function GetFilterType() As String
        Dim lcReturnValue As String = ""

        If FilterType = FilterTypes.Checkboxes Then
            lcReturnValue = "filterlist-checkbox"
        Else
            lcReturnValue = "filterlist-dropdown"
        End If

        Return lcReturnValue
    End Function
End Class
