﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_freetext_filter.ascx.vb" Inherits="uc_product_freetext_filter" %>
<utilize:literal runat="server" ID="lt_url" Text="" Visible="false"></utilize:literal>

<div ID="divmain" runat="server" class="uc_product_freetext_filter filter-by-block">
    <h2><utilize:translatetitle runat="server" ID="title_free_text_filters" Text="Filters"></utilize:translatetitle></h2>

    <div class="row">
        <utilize:placeholder runat="server" ID="ph_free_text1">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_1_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_1_div" href="#filter_div_1">
                                    <utilize:translatelabel runat="server" ID="label_free_text1" Text="Vrije tekst 1" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_1" class="panel-collapse collapse <%=Me.GetToggle(1)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text1">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc1")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>
                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text1" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text2">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_2_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_2_div" href="#filter_div_2">
                                    <utilize:translatelabel runat="server" ID="label_free_text2" Text="Vrije tekst 2" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_2" class="panel-collapse collapse <%=Me.GetToggle(2)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text2">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc2")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text2" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text3">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_3_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_3_div" href="#filter_div_3">
                                    <utilize:translatelabel runat="server" ID="label_free_text3" Text="Vrije tekst 3" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_3" class="panel-collapse collapse <%=Me.GetToggle(3)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text3">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc3")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text3" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>    

        <utilize:placeholder runat="server" ID="ph_free_text4">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_4_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_4_div" href="#filter_div_4">
                                    <utilize:translatelabel runat="server" ID="label_free_text4" Text="Vrije tekst 4" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_4" class="panel-collapse collapse <%=Me.GetToggle(4)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text4">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc4")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text4" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text5">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_5_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_5_div" href="#filter_div_5">
                                    <utilize:translatelabel runat="server" ID="label_free_text5" Text="Vrije tekst 5" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_5" class="panel-collapse collapse <%=Me.GetToggle(5)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text5">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc5")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text5" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_free_text6">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_6_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_6_div" href="#filter_div_6">
                                    <utilize:translatelabel runat="server" ID="label_free_text6" Text="Vrije tekst 6" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_6" class="panel-collapse collapse <%=Me.GetToggle(6)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text6">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc6")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text6" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text7">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_7_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_7_div" href="#filter_div_7">
                                    <utilize:translatelabel runat="server" ID="label_free_text7" Text="Vrije tekst 7" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_7" class="panel-collapse collapse <%=Me.GetToggle(7)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text7">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc7")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text7" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text8">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_8_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_8_div" href="#filter_div_8">
                                    <utilize:translatelabel runat="server" ID="label_free_text8" Text="Vrije tekst 8" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_8" class="panel-collapse collapse <%=Me.GetToggle(8)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text8">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc8")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text8" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
        <utilize:placeholder runat="server" ID="ph_free_text9">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_9_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_9_div" href="#filter_div_9">
                                    <utilize:translatelabel runat="server" ID="label_free_text9" Text="Vrije tekst 9" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_9" class="panel-collapse collapse <%=Me.GetToggle(9)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text9">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc9")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text9" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>

        <utilize:placeholder runat="server" ID="ph_free_text10">
            <div class="<%=Me.item_size.ToString()%>">
                <div class="free_text panel-group" id="free_list_10_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#free_list_10_div" href="#filter_div_10">
                                    <utilize:translatelabel runat="server" ID="label_free_text10" Text="Vrije tekst 10" CssClass="text panel-title"></utilize:translatelabel>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="filter_div_10" class="panel-collapse collapse <%=Me.GetToggle(10)%>">
                            <div class="panel-body">
                                <utilize:repeater runat="server" ID="rpt_free_text10">
                                    <ItemTemplate>
                                        <label class="fieldcontrol text-left">
                                            <span class="check-box">
                                                <utilize:checkbox runat="server" ID="chk_free_text1" Text='<%# DataBinder.Eval(Container.DataItem, "ff_desc10")%>' />
                                            </span>
                                        </label>
                                    </ItemTemplate>
                                </utilize:repeater>

                                <utilize:dropdownlist runat="server" autopostback="true" ID="cbo_free_text10" CssClass="form-control"></utilize:dropdownlist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </utilize:placeholder>
    </div>
</div>