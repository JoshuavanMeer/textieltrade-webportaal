﻿
Partial Class uc_product_tabs
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _controls_added As Boolean = False
    Private _product_code As String = ""

    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            DisplayTabs()
        End Set
    End Property

    Private uc_product_specifications As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_related_products As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    Protected Sub DisplayTabs()
        Dim lc_relations_title As String = global_trans.translate_title("title_product_relations", "Product relaties", Me.global_ws.Language)
        Dim lc_specifications_title As String = global_trans.translate_title("title_specifications", "Specificaties", Me.global_ws.Language)

        ' Als de controls nog niet toegevoegd zijn, dan moet de usercontrol geladen worden
        If Not Me._controls_added Then
            uc_related_products = Me.LoadUserControl("~/Webshop/Modules/Productrelations/uc_products_relations.ascx")
            uc_product_specifications = Me.LoadUserControl("~/Webshop/Modules/Specifications/uc_product_specs.ascx")

            Me.pgf_tabs.add_page(lc_specifications_title).Controls.Add(uc_product_specifications)
            Me.pgf_tabs.add_page(lc_relations_title).Controls.Add(uc_related_products)

            _controls_added = True
        End If

        uc_product_specifications.set_property("product_code", _product_code)
        uc_related_products.set_property("product_code", _product_code)

        Me.pgf_tabs.get_page(lc_specifications_title).page_button.Visible = CType(uc_product_specifications.FindControl("rpt_specs"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0
        Me.pgf_tabs.get_page(lc_relations_title).page_button.Visible = CType(uc_related_products.FindControl("rpt_products"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0

        Me.pgf_tabs.get_page(lc_specifications_title).page_panel.Visible = CType(uc_product_specifications.FindControl("rpt_specs"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0
        Me.pgf_tabs.get_page(lc_relations_title).page_panel.Visible = CType(uc_related_products.FindControl("rpt_products"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0

        If Not Me.IsPostBack() Then
            Select Case True
                Case CType(uc_product_specifications.FindControl("rpt_specs"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0
                    pgf_tabs.page_index = 0
                Case CType(uc_related_products.FindControl("rpt_products"), Utilize.Web.UI.WebControls.repeater).Items.Count > 0
                    pgf_tabs.page_index = 1
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Me.set_javascript("uc_product_tabs.js", Me)
        Me.set_style_sheet("uc_product_tabs.css", Me)
    End Sub
End Class
