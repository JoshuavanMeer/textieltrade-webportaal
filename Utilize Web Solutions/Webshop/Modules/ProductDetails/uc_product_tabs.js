(
    function ($) {
        $.pageframe =
        {
            // Initialisatie van de pageframe
            initialize_pageframes: function (pageframe_id, buttons_css) {
                // Zoek de pageframe op op basis van de id en sla het pageframe op in de variabele.
                var pgf_pageframe = $("[id$=" + pageframe_id + "]");

                // Zoek het buttons panel op, op basis van de class naam
                var pgf_page_buttons = pgf_pageframe.find('.' + buttons_css);
                var pagenumber = 0
                var currentpage = pgf_pageframe.find('.pageframe_index');

                // Loop door de buttons heen
                pgf_page_buttons.children().each
    		    (
    		        function (index) {
    		            pagenumber += 1
    		            // Referentie naar de button
    		            var pgf_button = $(this);

    		            // Voeg een click handler toe aan de pagina
    		            $(this).click(function () {
    		                $.pageframe.activate_page(pageframe_id, pgf_button[0].id, buttons_css);
    		                return false;
    		            });
    		        }
                );
            }
    	    ,

            // Activate a page based on
            activate_page: function (pageframe_id, pagebutton_id, buttons_css) {
                // Zoek de pageframe op op basis van de id en sla het pageframe op in de variabele.
                var pgf_pageframe = $("[id$=" + pageframe_id + "]");

                // Zoek het buttons panel op, op basis van de class naam
                var pgf_page_buttons = pgf_pageframe.find('.' + buttons_css);
                var ln_teller = 0

                // Loop door de buttons heen
                pgf_page_buttons.children().each
    		    (
    		        function (index) {
    		            // Referentie naar de button
    		            var pgf_button = $(this);

    		            pgf_button.removeClass();

    		            // Sla de id van de button op in een variabel;e
    		            var pgf_button_id = pgf_button[0].id

    		            if (pgf_button_id == pagebutton_id) {
    		                pgf_button.addClass('page_button_active');
    		                $('#' + pgf_button_id.replace('pgb', 'pgp')).css("display", "block");
    		                pgf_pageframe.find('.page_index').val(ln_teller);
    		            }
    		            else {
    		                pgf_button.addClass('page_button');
    		                $('#' + pgf_button_id.replace('pgb', 'pgp')).css("display", "none");
    		            }

    		            ln_teller += 1
    		        }
                );
            }
        }

    }
)(jQuery);



