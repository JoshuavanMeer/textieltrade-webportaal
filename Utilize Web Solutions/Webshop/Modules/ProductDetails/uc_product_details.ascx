﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_details.ascx.vb" Inherits="uc_product_details" %>

<%@ Register Src="~/Webshop/Modules/Images/uc_product_images_lightbox.ascx" tagname="uc_product_images_lightbox" tagprefix="uc" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_product_group.ascx" tagname="uc_product_group" tagprefix="uc3" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_product_favorites.ascx" tagname="uc_product_favorites" tagprefix="uc4" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_product_order_list.ascx" tagname="uc_product_order_list" tagprefix="uc10" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_product_stock.ascx" tagname="uc_product_stock" tagprefix="uc5" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_volume_discount.ascx" tagname="uc_volume_discount" tagprefix="uc6" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_sizeview_matrix.ascx" tagname="uc_sizeview_matrix" tagprefix="uc1" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_sizeview_group.ascx" tagname="uc_sizeview_group" tagprefix="uc7" %>
<%@ Register Src="~/Webshop/Modules/ProductInformation/uc_customer_product_codes.ascx" tagname="uc_customer_product_codes" tagprefix="uc8" %>
<%@ Register Src="~/Webshop/Modules/Videos/uc_product_videos.ascx" tagname="uc_product_videos" tagprefix="uc9" %>
<%@ Register Src="~/Webshop/Modules/PurchaseCombinations/uc_select_project_location.ascx" tagname="uc_select_project_location" tagprefix="uc2" %>
<%@ Register src="~/Webshop/TearSheet/uc_tearsheet.ascx" tagname="uc_tearsheet" tagprefix="uc11" %>

<div class="productdetails">
    <asp:HiddenField runat='server' ID="hih_product_code" />
    <asp:HiddenField runat='server' ID="hih_size" />

    <utilize:placeholder runat="server" ID="ph_product_details" Visible="true">
        <div class="shop-product margin-bottom-10">
            <div class="productcontent row">
                <div class="col-sm-6 col-md-6 image">
                    <utilize:placeholder runat="server" ID="ph_product_images_lightbox"></utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_sales_action" Visible="false">
                        <div class="shop-rgba-red rgba-banner"> 
                            <utilize:translatelabel runat="server" ID="label_sales_action" Text="Aanbieding"></utilize:translatelabel>
                        </div>
                    </utilize:placeholder>
                </div>
                <div class="col-sm-6 col-md-6 specs">
                    <utilize:placeholder runat="server" ID="ph_shop_product_heading">
                        <div class="shop-product-heading">
                            <utilize:image runat="server" ID="img_brand" Visible="false" CssClass="brand-image"/>

                            <h1><utilize:literal ID="lt_article_title" runat="server"></utilize:literal></h1>
                            <utilize:placeholder runat="server" ID="ph_prod_desc2" Visible="false">
                                <h4><utilize:literal ID="lt_article_title2" runat="server"></utilize:literal></h4>
                            </utilize:placeholder>
                        </div>
                    </utilize:placeholder>
                    <div class="productspecs">
                        <uc10:uc_product_order_list ID="uc_product_order_list1" runat="server" />
                        <utilize:placeholder runat="server" ID="ph_product_information"></utilize:placeholder>
                        <uc5:uc_product_stock ID="uc_product_stock1" runat="server" />
                        <uc6:uc_volume_discount ID="uc_volume_discount1" runat="server" />
                        <uc3:uc_product_group ID="uc_product_group1" runat="server" />
                        <uc7:uc_sizeview_group ID="uc_sizeview_group1" runat="server" />
                        <uc2:uc_select_project_location ID="uc_select_project_location1" runat="server" />
                        <utilize:placeholder runat="server" ID="ph_matrix"></utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_order_product"></utilize:placeholder>
                        <uc4:uc_product_favorites ID="uc_product_favorites1" runat="server" />
                        <uc11:uc_tearsheet ID="uc_tearsheet" runat="server" />
                    </div>
                </div>
                <div class="col-md-6 product-filters-advanced pull-right">
                    <utilize:placeholder runat="server" ID="ph_product_filters_advanced"></utilize:placeholder>
                </div>
            </div>
        </div>

        <utilize:placeholder runat="server" ID="ph_product_group_matrix"></utilize:placeholder>

        <uc9:uc_product_videos ID="uc_product_videos" runat="server" />

        <uc8:uc_customer_product_codes ID="uc_customer_product_codes" runat="server" />
                    
        <div class="productcontent">
            <utilize:placeholder runat="server" ID="ph_product_description">
                <h2><utilize:translatetitle runat="server" ID="title_product_description" Text="Product omschrijving"></utilize:translatetitle></h2>
                <p><utilize:literal ID="lt_prod_spec" runat="server"></utilize:literal></p>
            </utilize:placeholder>
        </div>

        <utilize:placeholder runat="server" ID="ph_product_document"></utilize:placeholder>
        <uc1:uc_sizeview_matrix ID="uc_sizeview_matrix1" runat="server" />
        <utilize:panel runat="server" ID="pnl_prod_document" CssClass="productdocument" Visible="false">
            <utilize:translatelink runat='server' ID="link_download_product_document" Text="Download hier uw product document"></utilize:translatelink>
        </utilize:panel>

        <utilize:panel runat="server" ID="pnl_trustpilot_reviews" visible="false">
            <h2><utilize:translatetitle runat="server" ID="title_trustpilot_review" Text="Trustpilot Beoordelingen"></utilize:translatetitle></h2>
            <utilize:repeater runat="server" ID="rpt_trustpilot_reviews">
                <ItemTemplate>
                    <div class="trustpilot-review">
                        <utilize:hyperlink runat="server" ID="hl_review" NavigateUrl="#" Target="_blank">
                            <h3><%# DataBinder.Eval(Container.DataItem.consumer, "name") %></h3>
                        </utilize:hyperlink>
                        <utilize:panel runat="server" ID="pnl_stars" CssClass="trustpilot-stars">
                            <utilize:image runat="server" ID="img_star_1" />
                            <utilize:image runat="server" ID="img_star_2" />
                            <utilize:image runat="server" ID="img_star_3" />
                            <utilize:image runat="server" ID="img_star_4" />
                            <utilize:image runat="server" ID="img_star_5" />
                        </utilize:panel>
                        <p><%# Container.DataItem.content %></p>
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </utilize:panel>

        <utilize:literal runat="server" ID="lt_trustpilot_errors" Visible="false" />
    
        <utilize:placeholder runat="server" ID="ph_extra_information"></utilize:placeholder>
    </utilize:placeholder>
    <utilize:placeholder runat="server" ID="ph_product_not_found" Visible='false'>
        <h1><utilize:translatetitle runat="server" ID="title_product_not_found" Text="Het product is niet gevonden"></utilize:translatetitle></h1>
        <utilize:translatetext runat="server" ID="text_product_not_found" Text="De pagina van het product is niet gevonden."></utilize:translatetext>
    </utilize:placeholder>
   
</div>
