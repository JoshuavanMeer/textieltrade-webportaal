﻿
Partial Class uc_product_details
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _controls_added As Boolean = False
    Private _product_code As String = ""
    Private _group_type As Utilize.Web.Solutions.Webshop.group_types = Me.global_ws.get_webshop_setting("group_products_det")

    Private udt_product As System.Data.DataTable = Nothing

    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value
            Me.block_id = value

            If product_code = "" Then
                _product_code = "!@ASECUTILIZE!!!!#######"
            End If

            If Not product_code = "" Then
                Me.uc_product_stock1.product_code = _product_code
                Me.uc_product_order_list1.product_code = _product_code
                Me.uc_product_group1.product_code = _product_code
                Me.uc_volume_discount1.product_code = _product_code
                Me.uc_sizeview_group1.product_code = _product_code
                Me.uc_customer_product_codes.product_code = _product_code
                Me.uc_product_videos.product_code = _product_code
                Me.uc_product_favorites1.product_code = _product_code
                Me.uc_tearsheet.ProductCode = _product_code

                ' De matrix is voor de verschillende producten altijd hetzelfde.
                Me.uc_sizeview_matrix1.product_code = _product_code

                ' Toon de productgegevens.
                Me.Display_Product()
            End If
        End Set
    End Property

    Private _size As String = ""
    Public Property size As String
        Get
            Return _size
        End Get
        Set(ByVal value As String)
            _size = value

            ' Zet de property van de order product en de voorraad
            Me.uc_order_product.set_property("size", _size)
            Me.uc_product_stock1.size = _size
        End Set
    End Property

    ''' <summary>
    ''' Used by one of the child processes to update the product_code here and subsequential to others (as the setting contains functions)
    ''' </summary>
    ''' <param name="prod_code">make product_code this code</param>
    ''' <returns>Success = true otherwise false</returns>
    Public Function SetProductCode(ByVal prod_code As String) As Boolean

        product_code = prod_code

        Dim e As New EventArgs
        OnPreRender(e)

        Return product_code = prod_code
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.object_code = "uws_products"

        If Not Me.IsPostBack() Then
            product_code = Me.get_page_parameter("prod_code")
        Else
            If product_code = "" Then
                product_code = Request.Form(Me.hih_product_code.UniqueID)
            End If

            size = Request.Form(Me.hih_size.UniqueID)
        End If

        If udt_product.Rows.Count > 0 Then
            Me.uc_order_product.set_property("project_location", Me.uc_select_project_location1.project_location)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ll_resume = Not Me.udt_product.Rows.Count = 0
        End If

        If ll_resume Then
            Dim dr_data_row As System.Data.DataRow = udt_product.Rows(0)

            Me.lt_article_title.Text = dr_data_row.Item("prod_desc")

            If Me.global_ws.get_webshop_setting("prod_show_subtitle") And Not dr_data_row.Item("prod_desc2") = "" Then
                Me.lt_article_title2.Text = dr_data_row.Item("prod_desc2")
                Me.ph_prod_desc2.Visible = True
            End If

            Me.lt_prod_spec.Text = dr_data_row.Item("prod_comm")

            ' Ton de omschrijving alleen wanneer deze is ingevuld
            Me.ph_product_description.Visible = Not Me.lt_prod_spec.Text.Trim() = ""

            If Not dr_data_row.Item("prod_document") = "" Then
                pnl_prod_document.Visible = True

                Me.link_download_product_document.NavigateUrl = Me.ResolveCustomUrl("~/" + dr_data_row.Item("prod_document"))
                Me.link_download_product_document.Target = "_blank"
            End If
        End If

        Me.hih_product_code.Value = product_code
        Me.hih_size.Value = size

        If Me.Visible Then
            Me.set_style_sheet("uc_product_details.css", Me)
        End If

        Me.ph_order_product.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", product_code) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", product_code, "mainitemmatrix") = product_code)
    End Sub

    Private uc_product_images_lightbox As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_order_product As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_matrix_units As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_product_group_matrix As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    Private uc_product_information As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_product_specifications As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_related_products_crossell As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_related_products_upsell As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_customer_review As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_product_documents As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_locater As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing
    Private uc_product_filters_advanced As Utilize.Web.Solutions.Base.base_usercontrol_base = Nothing

    ' product tonen
    Private Sub Display_Product()
        'eerst gegevens ophalen
        Dim ll_resume As Boolean = True

        ' huidige taalcode ophalen
        Dim lc_language = Me.global_cms.Language

        If ll_resume Then
            Dim ws_productdetails As New ws_productdetails
            udt_product = ws_productdetails.get_product_details(_product_code, Me.global_ws.Language)

            ll_resume = udt_product.Rows.Count > 0
        End If

        If ll_resume Then
            ' Als de controls nog niet toegevoegd zijn, dan moet de usercontrol geladen worden
            If Not Me._controls_added Then
                uc_product_images_lightbox = Me.LoadUserControl("~/Webshop/Modules/Images/uc_product_images_lightbox.ascx")
                uc_order_product = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
                uc_matrix_units = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_matrix_units.ascx")
                uc_product_information = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_information.ascx")

                uc_customer_review = Me.LoadUserControl("~/Webshop/Modules/Review/uc_customer_review.ascx")
                uc_related_products_crossell = Me.LoadUserControl("~/Webshop/Modules/Productrelations/uc_products_relations_cross.ascx")
                uc_related_products_upsell = Me.LoadUserControl("~/Webshop/Modules/Productrelations/uc_products_relations.ascx")
                uc_product_specifications = Me.LoadUserControl("~/Webshop/Modules/Specifications/uc_product_specs.ascx")
                uc_product_documents = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_documents.ascx")
                uc_product_group_matrix = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_group_matrix.ascx")
                uc_product_filters_advanced = Me.LoadUserControl("~/Webshop/Modules/ProductFilters/uc_product_filters_advanced.ascx")

                uc_related_products_crossell.set_property("product_code", product_code)
                uc_related_products_upsell.set_property("product_code", product_code)

                uc_product_specifications.set_property("product_code", product_code)
            End If


            'Voeg hier de product group/matrix control toe
            If Not _group_type = Webshop.group_types.none Then
                uc_product_group_matrix.ID = "uc_product_group_matrix"
                uc_product_group_matrix.set_property("uc_order_product", uc_order_product)
                uc_product_group_matrix.set_property("group_type", _group_type)
                uc_product_group_matrix.set_property("product_code", Me.product_code)

                If Not Me._controls_added Then
                    ph_product_group_matrix.Controls.Add(uc_product_group_matrix)
                End If
            End If

            uc_product_images_lightbox.set_property("product_code", product_code)
            uc_customer_review.set_property("product_code", product_code)
            uc_order_product.set_property("product_code", Me.product_code)
            uc_order_product.set_property("size", Me.size)
            uc_matrix_units.set_property("product_code", Me.product_code)
            uc_matrix_units.set_property("size", Me.size)
            uc_product_information.set_property("product_code", Me.product_code)
            uc_product_filters_advanced.set_property("product_code", Me.product_code)

            If Not Me._controls_added Then
                ph_product_images_lightbox.Controls.Add(uc_product_images_lightbox)
                ph_product_document.Controls.Add(uc_product_documents)
                ph_order_product.Controls.Add(uc_order_product)
                ph_matrix.Controls.Add(uc_matrix_units)
                ph_product_information.Controls.Add(uc_product_information)
                ph_product_details.Controls.Add(uc_customer_review)

                ph_extra_information.Controls.Add(uc_related_products_crossell)
                ph_extra_information.Controls.Add(uc_related_products_upsell)

                ph_extra_information.Controls.Add(uc_product_specifications)

                ' Als de module 'Product filters - Geavanceerd' aan staat
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") Then
                    ' Toevoegen van de user control geavanceerde filters
                    ph_product_filters_advanced.Controls.Add(uc_product_filters_advanced)
                    ' Als de user control zichtbaar is
                    If uc_product_filters_advanced.Visible Then
                        ' Niet tonen van de bestel knop, deze wordt dan in de user control getoond
                        Me.uc_order_product.Visible = False
                        Me.ph_shop_product_heading.Visible = False
                    End If
                End If

                Me._controls_added = True
            End If
        End If

        If Not ll_resume Then
            Me.ph_product_details.Visible = False
            Me.ph_product_not_found.Visible = True
        End If
    End Sub


    Protected Sub rpt_trustpilot_reviews_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_trustpilot_reviews.ItemCreated
        ' Bouw de URL op naar de review.
        CType(e.Item.FindControl("hl_review"), hyperlink).NavigateUrl = "https://" & If(Me.global_cms.Language.ToLower.Contains("nl"), "nl.", "") & "trustpilot.com/review/" & Utilize.Data.DataProcedures.GetValue("uws_settings", "tp_business_name", "UWS_SETTINGS") & "/" & e.Item.DataItem.id

        ' Haal de sterren op en zet de images.
        Dim stars As image() = {e.Item.FindControl("img_star_1"), e.Item.FindControl("img_star_2"), e.Item.FindControl("img_star_3"), e.Item.FindControl("img_star_4"), e.Item.FindControl("img_star_5")}
        Dim rating As Integer = e.Item.DataItem.stars
        For Each star In stars
            If (Array.IndexOf(stars, star) + 1) <= rating Then
                star.ImageUrl = ResolveUrl("~\Documents\Images\Stars\sprite_star.png")
            Else
                star.ImageUrl = ResolveUrl("~\Documents\Images\Stars\sprite_star_empty.png")
            End If
        Next
    End Sub
End Class
