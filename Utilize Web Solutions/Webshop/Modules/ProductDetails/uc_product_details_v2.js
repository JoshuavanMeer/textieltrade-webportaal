﻿function setup_product_details() {
    setup_prod_desc();
    structure_product_specs();
}

function setup_prod_desc() {

    var prodDetails = $('.productdetails .shop-product');
    var prodDescContainer = prodDetails.find($('.read-more-container'));
    var readMoreBtn = prodDetails.find($('.read-more-btn > a'));
    var readLessBtn = prodDetails.find($('.read-less-btn > a'));
    var readMoreBtnsContainer = prodDetails.find($('.read-more-btns-container'));
    var maxHeight = 100;

    init();

    function init() {
        // check if the description container is larger than 3 lines
        if (prodDescContainer.height() > maxHeight) {

            prodDescContainer.addClass('expandable');

            if (window.sessionStorage.getItem('expanded') === 'open') {
                open_prod_desc(prodDescContainer);
            } else {
                close_prod_desc(prodDescContainer);
            }

            readMoreBtn.click(function () {
                open_prod_desc(prodDescContainer);
            });
            readLessBtn.click(function () {
                close_prod_desc(prodDescContainer);
            });

        } else {
            readMoreBtnsContainer.addClass('hidden');
        }
    }

    function open_prod_desc(elem) {
        elem.addClass('open');
        elem.removeClass('closed');
        set_prod_desc_cache('open');
    }

    function close_prod_desc(elem) {
        elem.addClass('closed');
        elem.removeClass('open');
        set_prod_desc_cache('closed');
    }

    // Stores if the prod desc is opened or closed to act on postbacks
    function set_prod_desc_cache(state) {
        window.sessionStorage.setItem('expanded', state);
    }
}

// fires if reload is not a postback
function clear_prod_desc_cache() {
    window.sessionStorage.removeItem('expanded');
    window.sessionStorage.removeItem('opened_tab_id');
}

// This function alters the appearance of the product specs
// depending on how many product specs by grouping them in seperate
// two columns for better readability above a certain amount of items
function structure_product_specs() {

    if (!$('.uc_product_details_v2 .uc_product_specs').length) return false;

    var minListCount = 6;
    var length = ($('.uc_product_details_v2 .uc_product_specs').children().length - 1);
    var firstHalf = Math.ceil(length / 2) + 1;
    var secondHalf = length - firstHalf + 1;
    var children = $('.uc_product_details_v2 .uc_product_specs').children();
    if (length > minListCount) {
        children.slice(1, firstHalf).wrapAll("<div class='left-column'></div>");
        children.slice(firstHalf, length + 1).wrapAll("<div class='right-column'></div>");
    }
}


// This function handles the product information tabs
// It checks if tabs or tab content should be shown,
// and attaches event handlers so we can retrieve 
// and update current state using session storage
function manage_tabs() {
       
    var active_tab_id = window.sessionStorage.getItem('opened_tab_id');
    var total_tabs = $('.prod-information-tabs .tab-pane > *').length;

    init();    

    function init() {   
        
        // if there is no tabcontent available, hide all tabs and content
        if (!total_tabs) {
            $('.prod-information-tabs').addClass('hidden');
            return false;
        }

        // If only one tabcontent is available hide all tabs
        if (total_tabs === 1) {
            $('.prod-information-tabs .nav.nav-tabs').addClass('hidden');
        }

        $('.prod-information-tabs .tab-pane').each(function () {
            if ($(this).children().length) {

                var id = $(this)[0].id;
                total_tabs++;                

                // if an active tab is set in sessionstorage
                if (active_tab_id) {
                    show_active_tab(active_tab_id);
                }
                // set an active tab once if none in session storage
                else if (!active_tab_id) {
                    show_active_tab(id);
                    active_tab_id = id;
                }
                // reveal tab if it contains a user control
                $(this).toggleClass('hidden');
                $('a[href="#' + id + '"]').parent().toggleClass('hidden');
            }            
        });  

        // only attach tabs events if more than one tab is available
        if (total_tabs > 1) handle_clicks();
    }
    
    // if tab is visible, add a click handler so we can store 
    // and retrieve the currently opened tab from sessionstorage
    function handle_clicks() {
        $('.prod-information-tabs .nav-tabs > li').each(function () {
            if (!$(this).hasClass('hidden')) {
                var id = $(this).children('a').attr('href').substring(1);
                $(this).click(function () {
                    window.sessionStorage.setItem('opened_tab_id', id);
                });
            }           
        });
    }

    function show_active_tab(id) {
        active_tab_id = id; 
        $('#' + id).addClass('active in');
        $('a[href="#' + id + '"]').parent().addClass('active');
    }        
}