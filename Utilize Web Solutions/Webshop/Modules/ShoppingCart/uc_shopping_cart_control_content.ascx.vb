﻿
Imports System.Web.UI.WebControls

Partial Class uc_shopping_cart_content
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private price_type As Integer = 0
    ' 0 = Excl BTW
    ' 1 = Incl BTW

    ''' <summary>
    ''' This function limits the number of rows to 25 of the parsed datatable
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTop25(ByVal dt As System.Data.DataTable) As System.Data.DataTable
        Dim _table As System.Data.DataTable

        _table = dt.Clone()
        For i As Integer = 1 To 25
            If i - 1 >= dt.Rows.Count Then
                Exit For
            Else
                _table.ImportRow(dt.Rows(i - 1))
            End If
        Next

        Return _table
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.price_type = Me.global_ws.get_webshop_setting("price_type")

        Me.set_style_sheet("uc_shopping_cart_control_content.css", Me)

        Me.rpt_shopping_cart.DataSource = Me.GetTop25(Me.global_ws.shopping_cart.product_table.ToTable())
        Me.rpt_shopping_cart.DataBind()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.rpt_shopping_cart.DataSource = Me.GetTop25(Me.global_ws.shopping_cart.product_table.ToTable())
        Me.rpt_shopping_cart.DataBind()
    End Sub

    Protected Function get_image(ByVal prod_code As String) As String
        Dim qsc_product As New Utilize.Data.QuerySelectClass
        qsc_product.select_fields = "image_small"
        qsc_product.select_from = "uws_products"
        qsc_product.select_where = "prod_code = @prod_code"
        qsc_product.add_parameter("@prod_code", prod_code)

        Dim dt_image As System.Data.DataTable = qsc_product.execute_query()

        Return Webshop.ws_images.get_image_path(dt_image.Rows(0).Item("image_small"), "image_small", Me)

    End Function

    Protected Sub rpt_shopping_cart_ItemCommand(source As Object, e As UI.WebControls.RepeaterCommandEventArgs) Handles rpt_shopping_cart.ItemCommand
        Select Case e.CommandName.ToLower()
            Case "delete_row"
                ' Als de linkbutton voor een regel verwijderen wordt aangeklikt
                Dim lc_rec_id As String = e.CommandArgument

                ' Verwijder de geselecteerde regel
                Me.global_ws.shopping_cart.product_delete_by_id(lc_rec_id)

                ' En bouw de repeater opnieuw op
                Me.rpt_shopping_cart.DataBind()
            Case Else
                Exit Select
        End Select
    End Sub

    Private Sub rpt_shopping_cart_ItemCreated(sender As Object, e As RepeaterItemEventArgs) Handles rpt_shopping_cart.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lt_prod_price As Literal = e.Item.FindControl("lt_prod_price")

            If Not global_ws.price_mode = Webshop.price_mode.Credits Then
                ' Standaard prijzen tonen
                If Me.price_type = 1 Then
                    Dim ln_prod_price_incl_vat As Decimal = e.Item.DataItem("prod_price")

                    lt_prod_price.Text = Format(e.Item.DataItem("prod_price"), "c")
                Else
                    Dim ln_prod_price_incl_vat As Decimal = e.Item.DataItem("prod_price") + (e.Item.DataItem("vat_amt") / e.Item.DataItem("ord_qty"))
                    lt_prod_price.Text = Format(ln_prod_price_incl_vat, "c")
                End If

                Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on
                lt_prod_price.Visible = Not prices_not_visible
            Else
                ' Credits tonen
                If Me.price_type = 1 Then
                    'Excl
                    lt_prod_price.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("prod_price"))
                Else
                    'Incl
                    lt_prod_price.Text = global_ws.convert_amount_to_credits(e.Item.DataItem("prod_price")) + global_ws.convert_amount_to_credits(e.Item.DataItem("vat_amt")) / e.Item.DataItem("ord_qty")
                End If
            End If

            Dim lt_rec_id As Literal = e.Item.FindControl("lt_rec_id")
            lt_rec_id.Text = e.Item.DataItem("rec_id")
            Dim lt_product_code As Literal = e.Item.FindControl("lt_product_code")
            lt_product_code.Text = e.Item.DataItem("prod_code")

            Dim img_prod_img As System.Web.UI.HtmlControls.HtmlImage = e.Item.FindControl("img_prod_img")
            img_prod_img.Src = Me.get_image(e.Item.DataItem("prod_code"))

            ' Advanded product filters aka product configurator
            Dim btn_delete As Button = e.Item.FindControl("ib_delete")
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_prod_filters") = True And Not String.IsNullOrEmpty(e.Item.DataItem("set_code")) Then
                If Not String.IsNullOrEmpty(e.Item.DataItem("hdr_id_set")) Then
                    Dim li As HtmlGenericControl = e.Item.FindControl("li_shopping")
                    li.Attributes.Add("class", "cart-content shopping-li part-of-set")
                    btn_delete.Visible = False

                    Dim ph_quantity_price As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_quantity_price")
                    ph_quantity_price.Visible = False
                End If
            End If

            If Me.global_ws.get_webshop_setting("use_async_order") Then
                btn_delete.OnClientClick = "return false;"
                btn_delete.Attributes.Add("data-rec-id", e.Item.DataItem("rec_id"))
            End If

            btn_delete.CommandArgument = e.Item.DataItem("rec_id")

            Dim lt_prod_desc As Literal = e.Item.FindControl("lt_prod_desc")
            lt_prod_desc.Text = Utilize.Data.DataProcedures.GetValue("uws_products_tran", "prod_desc", e.Item.DataItem("prod_code") + Me.global_ws.Language, "prod_code + lng_code")
        End If
    End Sub
End Class
