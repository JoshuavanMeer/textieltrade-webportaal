﻿Partial Class uc_shopping_cart_control
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.ph_shopping_cart_content.Controls.Add(Me.LoadUserControl("~/Webshop/Modules/ShoppingCart/uc_shopping_cart_control_content.ascx"))

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim prices_not_visible As Boolean = (Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_counter") Or Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights")) And Not Me.global_ws.show_prices And Me.global_ws.user_information.user_logged_on

        'Zet de link van de winkelwagen button
        Me.link_shopping_cart.NavigateUrl = Me.ResolveCustomUrl("~/") + Me.global_ws.Language + "/webshop/paymentprocess/shopping_cart.aspx"

        If Me.Visible And global_ws.user_information.user_logged_on And Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_user_rights") Then
            ' Alleen zichtbaar als de gebruiker hier rechten toe heeft
            Me.Visible = Not Me.global_ws.user_information.ubo_customer.field_get("uws_customers_users.decl_po")
        End If

        ' Wanneer de user niet is ingelogd moet gecontroleerd worden of is ingesteld of de winkelwagen weergeven mag worden wanneer de user niet is ingelogd
        If Me.Visible And Not global_ws.user_information.user_logged_on Then
            Me.Visible = Me.global_ws.get_webshop_setting("shpcart_unlogged")
        End If

        Me.lt_product_count.Text = String.Format("{0:G29}", Me.global_ws.shopping_cart.product_count)
        Me.lt_badge_product_count.Text = String.Format("{0:G29}", Me.global_ws.shopping_cart.product_count)

        Select Case global_ws.price_mode
            Case Webshop.price_mode.Credits
                Me.ph_credits_available.Visible = True
                Me.lt_credits_left.Text = Format(global_ws.purchase_combination.credits_available, "N0")
            Case Webshop.price_mode.Money
                'Me.ph_amounts.Visible = True
                Me.ph_prod_count.Visible = True
                Me.ph_amount_available.Visible = True And prices_not_visible = False
                Me.lt_amount_left.Text = Format(global_ws.purchase_combination.amount_available, "c")
            Case Webshop.price_mode.Unlimited
                'Me.ph_amounts.Visible = True
                Me.ph_prod_count.Visible = True
                Me.ph_amount_available.Visible = False
            Case Else
                Exit Select
        End Select
        If Not global_ws.price_mode = Webshop.price_mode.Credits Then
            If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                Me.cart_total_amount.Text = Format(Me.global_ws.shopping_cart.order_subtotal_excl_vat, "c")
            Else
                Me.cart_total_amount.Text = Format(Me.global_ws.shopping_cart.order_subtotal_incl_vat, "c")
            End If
        Else

            If Me.global_ws.get_webshop_setting("price_type") = 1 Then
                Me.cart_total_amount.Text = Format(Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.order_subtotal_excl_vat), "N0")
            Else
                Me.cart_total_amount.Text = Format(Me.global_ws.convert_amount_to_credits(Me.global_ws.shopping_cart.order_subtotal_incl_vat), "N0")
            End If
        End If


        Me.set_style_sheet("uc_shopping_cart_control.css", Me)

        If Me.global_ws.get_webshop_setting("show_cart_content") Then
            Me.ph_shopping_cart_content.Visible = True
            Me.ph_content_li.Visible = True
            If Me.IsPostBack() Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "shopping_cart_scrollbar", "$(document).ready(function() {$(""#shopping_cart_content_ul"").mCustomScrollbar();});", True)
            End If
        End If

        If prices_not_visible Then
            Me.cart_total_amount.Visible = False
            Me.label_credit_count.Visible = False
            Me.lt_credits_left.Visible = False
        Else
            Me.cart_total_amount.Visible = True
            Me.label_credit_count.Visible = True
            Me.lt_credits_left.Visible = True
        End If

        Me.hl_shopping_cart.NavigateUrl = Me.ResolveCustomUrl("~/" + Me.global_ws.Language + "/Webshop/PaymentProcess/shopping_cart.aspx")
    End Sub
End Class