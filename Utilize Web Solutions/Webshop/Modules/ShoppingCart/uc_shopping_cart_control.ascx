﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_shopping_cart_control.ascx.vb" Inherits="uc_shopping_cart_control" %>

<ul class="uc_shopping_cart_control list-inline shop-badge badge-lists badge-icons pull-right">
    <li class="shopping-cart-btn">
        <utilize:hyperlink runat="server" ID="link_shopping_cart"><i class="fa fa-shopping-cart"></i></utilize:hyperlink>
        <utilize:translatelabel runat="server" ID="label_total_amount" Text="Bedrag" CssClass="label_amount"></utilize:translatelabel>

        <span class="cart_total_quantity shopping_cart_badge">
            <utilize:literal runat="server" ID="lt_badge_product_count"></utilize:literal>
        </span>
        <span class="cart_total_amount">
             <utilize:literal runat="server" ID="cart_total_amount"></utilize:literal>
        </span>

        <ul class='list-unstyled badge-open' id="header_shopping_cart">
            <utilize:placeholder runat="server" ID="ph_prod_count" Visible="false">
                <li class="subtotal shopping-li no-border-bottom">
                    <div class="col-md-12">
                        <utilize:translatelabel ID="label_product_count" runat="server" Text="Aantal producten in winkelwagen"></utilize:translatelabel>
                        <span class="product_count pull-right subtotal-cost">
                            <utilize:literal runat="server" ID="lt_product_count"></utilize:literal></span>
                    </div>
                </li>
            </utilize:placeholder>
            <utilize:placeholder runat="server" ID="ph_content_li" Visible="false">
                <li class="content_li">
                    <utilize:placeholder runat="server" ID="ph_shopping_cart_content" Visible="false">
                    </utilize:placeholder>
                </li>
            </utilize:placeholder>

            <li class="subtotal shopping-li">
                <div class="overflow-h margin-bottom-10">
                    <div class="shoppingcart_wrapper">
                        <utilize:placeholder runat="server" ID="ph_credits_available" Visible="false">
                            <br />
                            <span class="product_amount_text">
                                <utilize:translatelabel ID="label_credit_count" runat="server" Text="Aantal beschikbare credits"></utilize:translatelabel></span>&nbsp;<span class="product_amount"><b><utilize:literal runat="server" ID="lt_credits_left"></utilize:literal></b></span>
                        </utilize:placeholder>
                        <utilize:placeholder runat="server" ID="ph_amount_available" Visible="false">
                            <span class="product_amount_text">
                                <utilize:translatelabel ID="label_amount_available" runat="server" Text="Bedrag beschibaar"></utilize:translatelabel></span>&nbsp;<span class="product_amount"><b><utilize:literal runat="server" ID="lt_amount_left"></utilize:literal></b></span>
                        </utilize:placeholder>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <asp:HyperLink ID="hl_shopping_cart" runat="server" CssClass="btn-u btn-u-sea-shop btn-block">
                            <utilize:translateliteral runat="server" ID="label_show_basket" Text="Winkelwagen"></utilize:translateliteral></asp:HyperLink>
                    </div>
                </div>
            </li>

            <utilize:placeholder runat="server" ID="ph_shopping_cart_scroll" Visible="false">
            </utilize:placeholder>
        </ul>
    </li>
</ul>


