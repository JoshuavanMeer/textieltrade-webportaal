﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_shopping_cart_control_content.ascx.vb" Inherits="uc_shopping_cart_content" %>
<ul class="mCustomScrollbar" data-mcs-theme="minimal-dark" id="shopping_cart_content_ul"> 
    <utilize:repeater runat="server" id="rpt_shopping_cart">
        <ItemTemplate>
            <li class="cart-content shopping-product-li list-inline zone-product-block" runat="server" id="li_shopping">
                <utilize:literal runat='server' ID="lt_rec_id" visible="false"></utilize:literal>
                <utilize:literal runat='server' ID="lt_product_code" visible="false"></utilize:literal>

                <img runat="server" ID="img_prod_img" class="mCSB_img_loaded" />
                <utilize:button runat="server" ID="ib_delete" CssClass="close" CommandName="delete_row" text="x"/>     

                <div class="overflow-h">
                    <span><utilize:literal ID="lt_prod_desc" runat="server"></utilize:literal></span>
                    <utilize:placeholder runat="server" ID="ph_quantity_price">
                        <small><utilize:literal ID="lt_ord_qty" runat="server" Text='<%# String.Format("{0:G29}", DataBinder.Eval(Container.DataItem, "ord_qty")) %>'></utilize:literal> x <utilize:literal runat="server" ID="lt_prod_price"></utilize:literal></small>
                    </utilize:placeholder>
                </div>
            </li>
        </ItemTemplate>
    </utilize:repeater>
</ul>