﻿
Partial Class uc_categories_overview
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private cat_code As String = ""
    Private clearfix_sm As Integer = 0
    Private clearfix_md As Integer = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True
        Dim lc_shop_code As String = ""
        Dim ll_category_avail As Boolean = False
        Dim dt_data_table As System.Data.DataTable = Nothing

        If ll_resume Then
            If Me.global_ws.get_webshop_setting("cat_tiles_advanced") Then
                Dim lc_css As String = Me.pnl_categories_overview.CssClass

                If Not Me.global_ws.get_webshop_setting("cat_row_break") = 0 Then
                    lc_css += " overflow-title-" + Me.global_ws.get_webshop_setting("cat_row_break").ToString()
                End If

                If Me.global_ws.get_webshop_setting("cat_tiles_advanced") Then
                    lc_css += " advanced"

                    If Me.global_ws.get_webshop_setting("cat_tiles_set_backgr") Then
                        lc_css += " text-bg"
                    End If

                    If Me.global_ws.get_webshop_setting("cat_tiles_border") Then
                        lc_css += " border"
                    End If

                    If Me.global_ws.get_webshop_setting("cat_tiles_mob_list") Then
                        lc_css += " mobile-list-view"
                    End If

                    If Me.global_ws.get_webshop_setting("cat_tiles_align") = 2 Then
                        lc_css += " align-left"
                    ElseIf Me.global_ws.get_webshop_setting("cat_tiles_align") = 3 Then
                        lc_css += " align-right"
                    End If
                End If

                Me.pnl_categories_overview.CssClass = lc_css
                Me.set_style_sheet("uc_categories_overview_adv_css.aspx", Me)
            End If
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_categories_overview.css", Me)

            If Me.block_id = "" Then
                If cat_code = "" Then
                    cat_code = Me.get_page_parameter("cat_code")
                End If
            Else
                cat_code = block_id
            End If
        End If

        If ll_resume Then
            Dim userCategoriesModel As New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = cat_code,
                    .GetAllCategories = False
            }
            dt_data_table = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)
        End If

        Me.rpt_categories.DataSource = dt_data_table
        Me.rpt_categories.DataBind()

        If ll_resume Then
            ' Haal de bovenliggende categorie op
            Dim lc_parent_cat As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)

            ' Zorg dat de link opgebouwd wordt
            Dim lc_us_code As String = Utilize.Data.DataProcedures.GetValue("ucm_structure_blocks", "us_code", lc_parent_cat, "cat_block_id")
        End If

        ' Only show the categories block menu when categories are available
        If ll_resume Then
            ll_resume = dt_data_table.Rows.Count > 0
        End If

        Me.Visible = ll_resume
    End Sub

    Protected Sub rpt_categories_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_categories.ItemCreated
        clearfix_sm += 1
        If clearfix_sm = 2 Then
            clearfix_sm = 0
            Dim clearfixsm As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_clearfix_sm")
            clearfixsm.Visible = True
        End If

        clearfix_md += CInt(Me.item_size_int)

        If clearfix_md = 12 Then
            clearfix_md = 0

            Dim clearfixsm As Utilize.Web.UI.WebControls.placeholder = e.Item.FindControl("ph_clearfix_md")
            clearfixsm.Visible = True
        End If
    End Sub
End Class
