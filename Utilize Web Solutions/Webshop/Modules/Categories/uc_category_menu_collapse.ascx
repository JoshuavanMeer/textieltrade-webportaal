﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_category_menu_collapse.ascx.vb" Inherits="uc_category_menu_collapse" %>

<div class="mobile-category">
    <utilize:panel runat="server" id="pnl_menu_flyout" class="uc_category_menu_collapse collapse navbar-collapse mega-menu navbar-responsive-collapse panel-width">
        <div class="panel-group" id="accordion">
            <div class="panel panel-borderset">
                <div class="mobile-heading panel-heading">
                    <h2 class="panel-title">
                            <utilize:translatetitle runat="server" ID="title_categories" Text="Categorieën"></utilize:translatetitle>
                    </h2>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body mobile-body">
                        <utilize:placeholder runat="server" ID="ph_categories">

                        </utilize:placeholder>
                    </div>
                 
                </div>
            </div>
        </div>
    </utilize:panel>
</div>