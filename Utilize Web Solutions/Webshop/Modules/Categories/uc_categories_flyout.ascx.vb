﻿Imports System.Data

Partial Class uc_categories_flyout
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private cat_code As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_categories_flyout.css", Me)

            If cat_code = "" Then
                cat_code = Me.get_page_parameter("cat_code")
            End If

            Dim qsc_categories As New Utilize.Data.QuerySelectClass
            qsc_categories.select_fields = "uws_categories_tran.cat_code, uws_categories_tran.cat_desc, uws_categories_tran.tgt_url"
            qsc_categories.select_from = "uws_categories"
            qsc_categories.add_join("inner join", "uws_categories_tran", "uws_categories_tran.cat_code = uws_categories.cat_code")

            qsc_categories.select_where = "lng_code = @lng_code and uws_categories.cat_parent = @cat_parent and cat_show = 1"
            qsc_categories.select_order = "row_ord"

            qsc_categories.add_parameter("lng_code", Me.global_ws.Language)
            qsc_categories.add_parameter("cat_parent", "")

            ' shop_code is empty for the main shop
            ' shop_code is present for a focus shop
            ' category_available true means all categories must be show (both main and focus shop)
            ' category_available false means only categories of the focus shop is shown
            Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
            Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
            If ll_category_avail Then
                qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
            Else
                qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
            End If
            qsc_categories.add_parameter("shop_code", lc_shop_code)

            ' Als de module Gebruikersspecifieke producten aan staat, dan moet deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_webs_usr_prd") Then
                qsc_categories.select_where = qsc_categories.select_where + " and uws_categories.cat_code in (select cat_code from uws_categories_prod where prod_code in (select prod_code from uws_user_products where user_id = @user_id))"
                If Not String.IsNullOrEmpty(Me.global_ws.user_information.user_id_original) Then
                    qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id_original)
                Else
                    qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id)
                End If
            End If

            Dim dt_data_table As System.Data.DataTable = qsc_categories.execute_query()

            ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
            If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
                Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

                If Not String.IsNullOrEmpty(Me.global_ws.user_information.ubo_customer.field_get("ass_code")) Then
                    ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True Then
                        Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                        If Not lc_category_string = "" Then
                            For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                                If Not lc_category_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                                    dr_data_row.Delete()
                                End If
                            Next

                            dt_data_table.AcceptChanges()
                        End If
                    End If

                    ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                        Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)

                        For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                            If lc_category_exclusion_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                                dr_data_row.Delete()
                            End If
                        Next
                        dt_data_table.AcceptChanges()
                    End If
                End If
            End If

            If dt_data_table.Rows.Count = 0 Then
                Dim lc_cat_parent As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)
                qsc_categories = New Utilize.Data.QuerySelectClass
                qsc_categories.select_fields = "uws_categories_tran.cat_code, uws_categories_tran.cat_desc, uws_categories_tran.tgt_url"
                qsc_categories.select_from = "uws_categories"
                qsc_categories.add_join("inner join", "uws_categories_tran", "uws_categories_tran.cat_code = uws_categories.cat_code")
                qsc_categories.select_where = "lng_code = @lng_code and uws_categories.cat_parent = @cat_parent and cat_show = 1"

                ' shop_code is empty for the main shop
                ' shop_code is present for a focus shop
                ' category_available true means all categories must be show (both main and focus shop)
                ' category_available false means only categories of the focus shop is shown
                If ll_category_avail Then
                    qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
                Else
                    qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
                End If
                qsc_categories.add_parameter("shop_code", lc_shop_code)

                ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
                    Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

                    If Not Me.global_ws.user_information.ubo_customer.field_get("ass_code") = "" Then
                        Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                        If Not lc_category_string = "" Then
                            qsc_categories.select_where = " and '" + lc_category_string + "' like '%^' + uws_categories.cat_code + '^%')"
                        End If
                    End If
                End If

                qsc_categories.select_order = "row_ord"

                qsc_categories.add_parameter("lng_code", Me.global_ws.Language)
                qsc_categories.add_parameter("cat_parent", lc_cat_parent)

                ' Als de module Gebruikersspecifieke producten aan staat, dan moet deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_webs_usr_prd") Then
                    qsc_categories.select_where = qsc_categories.select_where + " and uws_categories.cat_code in (select cat_code from uws_categories_prod where prod_code in (select prod_code from uws_user_products where user_id = @user_id))"
                    If Not String.IsNullOrEmpty(Me.global_ws.user_information.user_id_original) Then
                        qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id_original)
                    Else
                        qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id)
                    End If
                End If

                dt_data_table = qsc_categories.execute_query()

                ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
                    Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

                    ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                    If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                        Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)

                        For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                            If lc_category_exclusion_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                                dr_data_row.Delete()
                            End If
                        Next
                        dt_data_table.AcceptChanges()
                    End If
                End If
            End If

            createMenu(dt_data_table)
        End If

    End Sub

    Private Function getCategories(ByVal lc_cat_code As String) As DataTable
        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        qsc_categories.select_fields = "uws_categories_tran.cat_code, uws_categories_tran.cat_desc, uws_categories_tran.tgt_url"
        qsc_categories.select_from = "uws_categories"
        qsc_categories.add_join("inner join", "uws_categories_tran", "uws_categories_tran.cat_code = uws_categories.cat_code")
        qsc_categories.select_where = "lng_code = @lng_code and uws_categories.cat_parent = @cat_parent and cat_show = 1"
        qsc_categories.select_order = "row_ord"

        qsc_categories.add_parameter("lng_code", Me.global_ws.Language)
        qsc_categories.add_parameter("cat_parent", lc_cat_code)

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        ' Als de module Gebruikersspecifieke producten aan staat, dan moet deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_webs_usr_prd") Then
            qsc_categories.select_where = qsc_categories.select_where + " and uws_categories.cat_code in (select cat_code from uws_categories_prod where prod_code in (select prod_code from uws_user_products where user_id = @user_id))"
            If Not String.IsNullOrEmpty(Me.global_ws.user_information.user_id_original) Then
                qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id_original)
            Else
                qsc_categories.add_parameter("user_id", Me.global_ws.user_information.user_id)
            End If
        End If

        Dim dt_data_table As System.Data.DataTable = qsc_categories.execute_query()

        ' Als de module klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
        If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass") = True Then
            Dim lc_ass_code As String = Me.global_ws.user_information.ubo_customer.field_get("ass_code")

            If Not String.IsNullOrEmpty(Me.global_ws.user_information.ubo_customer.field_get("ass_code")) Then
                ' Als de module categorie toekenning in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_cat") = True Then
                    Dim lc_category_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_string(lc_ass_code)

                    If Not lc_category_string = "" Then
                        For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                            If Not lc_category_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                                dr_data_row.Delete()
                            End If
                        Next

                        dt_data_table.AcceptChanges()
                    End If
                End If

                ' Als de module categorie uitsluitingen in klantspecifieke assortimenten aan staat, dan moeten deze toegepast worden
                If Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_cust_ass_exc") = True Then
                    Dim lc_category_exclusion_string As String = Utilize.Web.Solutions.Webshop.ws_procedures.get_category_exclusion_string(lc_ass_code)

                    For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                        If lc_category_exclusion_string.Contains("^" + dr_data_row.Item("cat_code") + "^") Then
                            dr_data_row.Delete()
                        End If
                    Next
                    dt_data_table.AcceptChanges()
                End If
            End If
        End If


        Return dt_data_table

    End Function

    Private Sub createMenu(ByVal dt_categories As DataTable)

        Dim lo_list As New HtmlGenericControl("ul")
        lo_list.Attributes.Add("class", "level1")


        Dim li_counter = 1
        For Each dr_category As DataRow In dt_categories.Rows
            Dim lo_li As New HtmlGenericControl("li")

            Dim dt_sub_categories As DataTable = Me.getCategories(dr_category("cat_code"))

            If dt_sub_categories.Rows.Count > 0 Then
                Dim lo_ul_sub As New HtmlGenericControl("ul")
                lo_ul_sub.Attributes.Add("class", "level2")
                For Each dr_subcat As DataRow In dt_sub_categories.Rows
                    Dim lo_li_subcat As New HtmlGenericControl("li")
                    Dim lo_a_subcat As New HtmlGenericControl("a")
                    lo_a_subcat.InnerText = dr_subcat("cat_desc")
                    lo_a_subcat.Attributes.Add("href", Me.ResolveCustomUrl("~/" + dr_subcat.Item("tgt_url")))
                    lo_li_subcat.Controls.Add(lo_a_subcat)
                    lo_ul_sub.Controls.Add(lo_li_subcat)
                Next
                lo_li.Controls.Add(lo_ul_sub)
            End If


            Dim lo_a As New HtmlGenericControl("a")
            lo_a.InnerText = dr_category("cat_desc")
            lo_a.Attributes.Add("href", Me.ResolveCustomUrl("~/" + dr_category.Item("tgt_url")))
            lo_li.Controls.Add(lo_a)
            lo_list.Controls.Add(lo_li)

            If li_counter < dt_categories.Rows.Count Then
                lo_list.Controls.Add(get_seperator())
            End If
            li_counter += 1
        Next

        Me.ph_menu_cat.Controls.Add(lo_list)

    End Sub
    ' Dit wordt een menu van links naar rechts
    ' Niveau 1 zijn de menuopties
    ' Als je daarop wijst, dan moet dit uitklappen, net als paul simons.
    ' Om dit te testen, kun je de website structuur blok even weghalen en dit blok toevoegen.

    Private Function get_seperator() As HtmlGenericControl
        Dim lo_seperator = New HtmlGenericControl("li")
        lo_seperator.Attributes.Add("class", "seperator")
        lo_seperator.InnerText = "^"
        Return lo_seperator
    End Function
End Class
