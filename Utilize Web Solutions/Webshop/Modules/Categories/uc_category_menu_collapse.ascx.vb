﻿
Partial Class uc_category_menu_collapse
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private cat_code As String = ""
    Private _category_string As String = "^"
    Private use_advanced_styling As Boolean = False
    ' Used to only show the categories block menu when categories are available
    Private _number_of_categories As Integer = 0

    ''' <summary>
    ''' Sub om een string van bovenliggende categorieën te maken
    ''' </summary>
    ''' <param name="cat_code"></param>
    ''' <remarks></remarks>
    Private Sub create_category_string(ByVal cat_code As String)
        If Not String.IsNullOrEmpty(cat_code) Then
            _category_string += cat_code + "^"

            Dim lc_parent As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)
            If Not String.IsNullOrEmpty(lc_parent) Then
                create_category_string(lc_parent)
            End If
        End If
    End Sub

    Private _level_count As Integer = 5
    Public Property level_count As Integer
        Get
            Return _level_count
        End Get
        Set(value As Integer)
            _level_count = value
        End Set
    End Property

    Private _level As Integer = 1
    Private sub_menu_counter As Integer = 0

    Private Sub create_category_uls(ByVal cat_code As String, ByVal targetcontrol As System.Web.UI.Control)
        If _level <= _level_count Then
            ' Haal de categorieën op.                        
            Dim userCategoriesModel As New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = cat_code,
                    .GetAllCategories = False
            }
            Dim dt_data_Table As System.Data.DataTable = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)

            ' controle voor het toevoegen van de class dropdown
            If _level > 1 Then
                If CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Item("class") = "active" Then
                    If dt_data_Table.Rows.Count > 0 Then
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown active")
                    Else
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width active")
                    End If
                Else
                    If dt_data_Table.Rows.Count > 0 Then
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown")
                    Else
                        CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width")
                    End If
                End If
            End If

            ' Used to only show the categories block menu when categories are available
            _number_of_categories += dt_data_table.Rows.Count

            ' Als er resultaten gevonden zijn...
            If dt_data_table.Rows.Count > 0 Then
                Dim ln_level_number As Integer = _level.ToString()

                ' Maak een nieuwe ul aan
                Dim lo_ul As New HtmlGenericControl("ul")
                ' Zet de classname en zorg dat we het niveau ook weten 
                lo_ul.Attributes.Add("class", "level" + _level.ToString())

                If ln_level_number = 1 Then
                    lo_ul.Attributes.Add("class", "nav navbar-nav")
                End If

                If ln_level_number > 1 Then
                    If _category_string.ToUpper.Contains("^" + cat_code.ToUpper + "^") Then
                        lo_ul.Attributes.Add("class", "dropdown-menu active")
                    Else
                        lo_ul.Attributes.Add("class", "dropdown-menu inactive")
                    End If
                End If

                For Each dr_data_row As System.Data.DataRow In dt_data_table.Rows
                    Dim lo_li As New HtmlGenericControl("li")

                    Dim hl_hyperlink As New Utilize.Web.UI.WebControls.hyperlink

                    hl_hyperlink.NavigateUrl = Me.ResolveCustomUrl("~/" + dr_data_row.Item("tgt_url"))
                    hl_hyperlink.Text = dr_data_row.Item("cat_desc_tran")

                    ' code
                    If _category_string.ToUpper.Contains("^" + dr_data_row.Item("cat_code").ToUpper + "^") Then
                        lo_li.Attributes.Add("class", "active")
                    End If

                    lo_li.Controls.Add(hl_hyperlink)

                    _level += 1

                    If Me.use_advanced_styling Then
                        If _category_string.ToUpper.Contains("^" + dr_data_row.Item("cat_code").ToUpper + "^") Then
                            Me.create_category_uls(dr_data_row.Item("cat_code"), lo_li)
                        End If
                    Else
                        Me.create_category_uls(dr_data_row.Item("cat_code"), lo_li)
                    End If

                    _level -= 1

                    lo_ul.Controls.Add(lo_li)

                Next
                'check of the ul een active bevat
                If TypeOf targetcontrol Is System.Web.UI.HtmlControls.HtmlGenericControl Then
                    For Each Control In lo_ul.Controls
                        If Control.attributes.item("class") = "li-width active" Then
                            CType(targetcontrol, System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("class", "li-width dropdown active")
                        End If
                    Next
                End If

                targetcontrol.Controls.Add(lo_ul)
            Else
                If cat_code.Trim() = "" Then
                    Me.Visible = False
                End If
            End If
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume And Not Me.block_table = "" And Not Me.block_rec_id = "" Then
            Dim dr_temp_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

            If dr_temp_row.Item("catmen_coll_advanced") Then
                Me.use_advanced_styling = True
                pnl_menu_flyout.Attributes.Add("class", pnl_menu_flyout.Attributes.Item("class") + " advanced")
            End If
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_category_menu_collapse.css", Me)

            If cat_code = "" Then
                cat_code = Me.get_page_parameter("cat_code")

                Me.create_category_string(cat_code)
            End If

            ' Bouw hier de gegevens op
            Me.create_category_uls(Me.block_id, Me.ph_categories)
        End If

        If ll_resume Then
            ' Hier maken we een button voor de navbar aan
            Dim uc_navbar_button As Base.base_usercontrol_base = LoadUserControl("~/CMS/Modules/Header/uc_navbar_button.ascx")
            uc_navbar_button.set_property("datatarget", ".uc_category_menu_collapse")
            uc_navbar_button.set_property("cssclass", "category_menu_icon")
            uc_navbar_button.set_property("icon", "fa fa-asterisk")

            Me.Page.Master.FindControl("uc_header").FindControl("ph_nav_buttons").Controls.Add(uc_navbar_button)
        End If

        Me.Visible = ll_resume

        ' Only show the categories block menu when categories are available
        If Me.Visible Then
            Me.Visible = _number_of_categories > 0
        End If
    End Sub
End Class
