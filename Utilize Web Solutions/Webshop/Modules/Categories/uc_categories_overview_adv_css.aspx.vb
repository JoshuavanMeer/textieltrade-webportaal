﻿
Partial Class Styles_PageTemplate
    Inherits System.Web.UI.Page

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim ll_resume As Boolean = True

        If ll_resume Then
            'Add the styles as set in the advanced styling options
            Dim qsc_settings As New Utilize.Data.QuerySelectClass With {.select_fields = "*", .select_from = "uws_settings"}
            Dim dr_data_row As System.Data.DataRow = qsc_settings.execute_query().Rows(0)
            Dim lc_style_string As New StringBuilder

            lc_style_string.Append(".uc_categories_overview.advanced.text-bg .category-name").AppendLine()
            lc_style_string.Append("{").AppendLine()
            lc_style_string.Append("background-color: " + dr_data_row.Item("cat_tiles_backgr_col").ToString + " !important;").AppendLine()
            lc_style_string.Append("color: " + dr_data_row.Item("cat_tiles_font_col").ToString + " !important;").AppendLine()
            lc_style_string.Append("}").AppendLine()

            lc_style_string.ToString()

            'Turn the css into a style sheet
            Response.Clear()
            Response.ContentType = "text/css"
            Response.Write(lc_style_string)
            Response.End()
        End If
    End Sub
End Class
