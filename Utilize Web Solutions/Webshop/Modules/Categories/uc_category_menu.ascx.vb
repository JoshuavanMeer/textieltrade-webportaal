﻿
Partial Class uc_category_menu
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private cat_code As String = ""

    Public Overloads Property block_id As String
        Get
            Return cat_code
        End Get
        Set(ByVal value As String)
            cat_code = value
        End Set
    End Property
    Private Function get_category_description() As String
        Dim lc_return_value As String = ""

        Dim qsc_categories As New Utilize.Data.QuerySelectClass
        qsc_categories.select_fields = "uws_categories_tran.cat_desc"
        qsc_categories.select_from = "uws_categories"
        qsc_categories.select_order = "row_ord"
        qsc_categories.add_join("inner join", "uws_categories_tran", "uws_categories.cat_code = uws_categories_tran.cat_code")
        qsc_categories.select_where = "uws_categories_tran.cat_code = @cat_code and uws_categories_tran.lng_code = @lng_code and cat_show = 1"

        qsc_categories.add_parameter("cat_code", cat_code)
        qsc_categories.add_parameter("lng_code", Me.global_ws.Language)

        ' shop_code is empty for the main shop
        ' shop_code is present for a focus shop
        ' category_available true means all categories must be show (both main and focus shop)
        ' category_available false means only categories of the focus shop is shown
        Dim lc_shop_code As String = Me.global_ws.shop.get_shop_code()
        Dim ll_category_avail As Boolean = Utilize.Data.DataProcedures.GetValue("uws_shops", "category_avail", lc_shop_code)
        If ll_category_avail Then
            qsc_categories.select_where += " and (uws_categories.shop_code = @shop_code or uws_categories.shop_code = '')"
        Else
            qsc_categories.select_where += " and uws_categories.shop_code = @shop_code"
        End If
        qsc_categories.add_parameter("shop_code", lc_shop_code)

        Dim dt_data_Table As System.Data.DataTable = qsc_categories.execute_query()

        If dt_data_Table.Rows.Count > 0 Then
            lc_return_value = dt_data_Table.Rows(0).Item("cat_desc")
        Else
            lc_return_value = global_trans.translate_title("title_categories", "Assortiment", Me.global_ws.Language)
        End If

        Return lc_return_value
    End Function

    ' Voor het inspringen van een categorie menu is de parameter CATEGORY_MENU_TYPE beschikbaar
    ' 1 is standaard menu
    ' 2 is inspringen
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        Me.set_style_sheet("uc_category_menu.css", Me)

        If cat_code = "" Then
            cat_code = Me.get_page_parameter("cat_code")
        End If

        Dim userCategoriesModel As New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = cat_code,
                    .GetAllCategories = False
            }
            Dim dt_data_Table As System.Data.DataTable = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)

        If dt_data_table.Rows.Count = 0 Then
            Dim lc_cat_parent As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)
            userCategoriesModel = New Utilize.Data.API.Webshop.UserCategoriesModel With {
                    .Language = Me.global_ws.Language,
                    .ShopCode = Me.global_ws.shop.get_shop_code,
                    .UserId = Me.global_ws.user_information.user_id,
                    .UserLoggedOn = Me.global_ws.user_information.user_logged_on,
                    .UserInformation = Me.global_ws.user_information.ubo_customer,
                    .CatParent = lc_cat_parent,
                    .GetAllCategories = False
            }
            dt_data_Table = Utilize.Data.API.Webshop.CategoriesHelper.getUserCategories(userCategoriesModel)
        End If

        Me.rpt_categories.DataSource = dt_data_Table
        Me.rpt_categories.DataBind()

        If ll_resume Then
            Me.title_categories.Text = Me.get_category_description()
        End If

        If ll_resume Then
            ' Hier maken we een button voor de navbar aan
            Dim uc_navbar_button As Base.base_usercontrol_base = LoadUserControl("~/CMS/Modules/Header/uc_navbar_button.ascx")
            uc_navbar_button.set_property("datatarget", ".uc_category_menu")
            uc_navbar_button.set_property("cssclass", "category_menu_icon")
            uc_navbar_button.set_property("icon", "glyphicon glyphicon-asterisk")

            Me.Page.Master.FindControl("uc_header").FindControl("ph_nav_buttons").Controls.Add(uc_navbar_button)
        End If

        If ll_resume Then
            '  Als de bovenliggende categorie niet leeg is:
            Me.link_level_up.Visible = Not cat_code.Trim() = ""

            If Not cat_code = "" Then
                ' Haal de bovenliggende categorie op
                Dim lc_parent_cat As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)
                Dim lc_category_url As String = Utilize.Data.DataProcedures.GetValue("uws_categories_tran", "tgt_url", lc_parent_cat + Me.global_ws.Language, "cat_code + lng_code")

                If String.IsNullOrEmpty(lc_category_url) Then
                    lc_category_url = "/" + Me.global_ws.Language + "/webshop/product_list.aspx"
                End If

                Me.lt_level_up.Text = global_trans.translate_label("link_level_up", "Een niveau omhoog", Me.global_ws.Language)

                Me.link_level_up.NavigateUrl = Me.ResolveCustomUrl("~/" + lc_category_url)
            End If
        End If

        Me.Visible = ll_resume


        If ll_resume And Not Me.block_table = "" And Not Me.block_rec_id = "" Then
            Dim dr_temp_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

            If dr_temp_row.Item("cat_menu_advanced") Then
                ph_category_menu.CssClass += " advanced"
            End If
        End If
    End Sub
End Class
