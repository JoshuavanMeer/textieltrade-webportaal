﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_category_menu.ascx.vb" Inherits="uc_category_menu" %>

<div class="mobile-category">
    <utilize:panel runat="server" id="ph_category_menu" cssclass="uc_category_menu collapse navbar-collapse mega-menu navbar-responsive-collapse panel-width"> 
        <div class="panel-group" id="accordion">
            <div class="panel panel-borderset">
                <div class="mobile-heading panel-heading">
                    <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"><utilize:translatetitle runat="server" ID="title_categories" Text="Categorieën"></utilize:translatetitle></a>
                    </h2>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body mobile-body">
                        <ul class="nav navbar-nav">
                            <utilize:repeater runat='server' ID="rpt_categories">
                                <ItemTemplate>
                                    <li class="li-width"><asp:HyperLink runat="server" ID="hl_products_overview" NavigateUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "tgt_url"))%>'><utilize:literal ID="hl_categories" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "cat_desc_tran")%>'></utilize:literal></asp:HyperLink></li>
                                </ItemTemplate>
                            </utilize:repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <utilize:hyperlink runat="server" ID="link_level_up">
            <span class="glyphicon glyphicon-arrow-left"></span>
            <utilize:literal runat="server" ID="lt_level_up"></utilize:literal>
        </utilize:hyperlink>
    </utilize:panel>
</div>