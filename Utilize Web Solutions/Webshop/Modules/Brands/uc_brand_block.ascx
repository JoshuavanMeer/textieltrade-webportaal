﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_brand_block.ascx.vb" Inherits="uc_brand_block" %>

<utilize:panel ID="pnl_brands_block" CssClass="uc_brand_block margin-bottom-20" runat="server">
    <div class="owl-carousel-v2 owl-carousel-style-v1 margin-bottom-50">
        <utilize:placeholder runat="server" ID="ph_brands_title">
            <h2><utilize:translatetitle runat="server" id="title_brand_block" text="Merken overzicht"></utilize:translatetitle></h2>
        </utilize:placeholder>

        <div class="owl-slider-v2">
            <utilize:repeater runat='server' ID="rpt_brands">
                <ItemTemplate>
                    <div class="item">
                        <a href="<%# Me.ResolveCustomUrl("~/") + Me.global_ws.Language.ToLower() + "/webshop/product_search.aspx?brand=" + DataBinder.Eval(Container.DataItem, "brand_code")%>">
                            <utilize:image runat='server' ID="img_brand_image" ImageUrl='<%# Me.ResolveCustomUrl("~/" + DataBinder.Eval(Container.DataItem, "brand_image"))%>'/>
                            <p><utilize:literal ID="hl_brands" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "brand_desc_" + Me.global_ws.Language)%>'></utilize:literal></p>
                        </a>
                    </div>
                </ItemTemplate>
            </utilize:repeater>
        </div>
    </div>
</utilize:panel>