﻿
Partial Class uc_brand_block
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private brand_code As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            Me.set_style_sheet("uc_brand_block.css", Me)

            If Me.block_id = "" Then
                If brand_code = "" Then
                    brand_code = Me.get_page_parameter("brand_code")
                End If
            Else
                brand_code = block_id
            End If

            Dim qsc_brands As New Utilize.Data.QuerySelectClass
            qsc_brands.select_fields = "uws_brands.brand_code, uws_brands.brand_desc_" + Me.global_ws.Language + ", CASE WHEN uws_brands.brand_logo = '' THEN 'Documents/ProductImages/not-available-category.jpg' ELSE uws_brands.brand_logo END as brand_image "
            qsc_brands.select_from = "uws_brands"
            qsc_brands.select_where = "slider_hide = 0"

            qsc_brands.add_parameter("lng_code", Me.global_ws.Language)

            Dim dt_data_table As System.Data.DataTable = qsc_brands.execute_query()

            Me.rpt_brands.DataSource = dt_data_table
            Me.rpt_brands.DataBind()
        End If

        If ll_resume Then
            ' Haal de bovenliggende categorie op
            Dim lc_parent_cat As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", brand_code)

            ' Zorg dat de link opgebouwd wordt
            Dim lc_us_code As String = Utilize.Data.DataProcedures.GetValue("ucm_structure_blocks", "us_code", lc_parent_cat, "cat_block_id")
        End If

        Me.Visible = ll_resume

        If ll_resume And Me.Page.IsPostBack() Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "brands_slider", "jQuery(document).ready(function () {OwlCarousel.initOwlCarousel();});", True)
        End If
    End Sub
End Class
