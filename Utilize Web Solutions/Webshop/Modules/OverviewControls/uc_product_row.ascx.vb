﻿
Partial Class uc_product_row
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _dr_product_row As System.Data.DataRow
    Public Property product_row As System.Data.DataRow
        Get
            Return _dr_product_row
        End Get
        Set(value As System.Data.DataRow)
            _dr_product_row = value
        End Set
    End Property

    Private _customer_product_codes As Boolean = False
    Public Property customer_product_codes As Boolean
        Get
            Return _customer_product_codes
        End Get
        Set(value As Boolean)
            _customer_product_codes = value
        End Set
    End Property

    Private _bulk_order As Boolean = False
    Public Property bulk_order As Boolean
        Get
            Return _bulk_order
        End Get
        Set(value As Boolean)
            _bulk_order = value
        End Set
    End Property

    Protected Sub Page_Init1(sender As Object, e As EventArgs) Handles Me.Init
        Me.object_code = "uws_products"
        Me.block_id = _dr_product_row.Item("prod_code")

        Me.set_style_sheet("uc_product_row.css", Me)

        Me.Hyperlink1.NavigateUrl = Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url"))

        Me.imgprod.Alt = Me.product_row.Item("prod_desc").replace("'", "").replace("""", "")

        Me.imgprod.Src = Fingerprint.tag(Webshop.ws_images.get_image_path(Me.product_row.Item("image_small"), "image_small", Me))

        Me.Hyperlink2.NavigateUrl = Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url"))
        Me.Hyperlink2.Text = Me.product_row.Item("prod_desc")

        Me.uc_customer_product_codes1.product_code = Me.product_row.Item("prod_code")

        Me.span_prod_desc2.InnerText = Me.product_row.Item("prod_desc2")

        If Not String.IsNullOrEmpty(Me.product_row.Item("prod_comm_short")) Then
            Dim span_prod_comm As New System.Web.UI.HtmlControls.HtmlGenericControl("span")
            span_prod_comm.InnerText = Me.product_row.Item("prod_comm_short")

            Dim break As New System.Web.UI.HtmlControls.HtmlGenericControl("br")

            Dim link_read_more As New Utilize.Web.Solutions.Translations.translatelink
            link_read_more.ID = "link_read_more"
            link_read_more.NavigateUrl = Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url"))
            link_read_more.Text = "Lees meer..."
            link_read_more.CssClass = "readmore"

            Me.p_prod_com_short.Controls.Add(span_prod_comm)
            Me.p_prod_com_short.Controls.Add(break)
            Me.p_prod_com_short.Controls.Add(link_read_more)
        End If

        If Not String.IsNullOrEmpty(_dr_product_row.Item("grp_name")) And Me.global_ws.get_webshop_setting("GROUP_PRODUCTS") = 1 Or Not String.IsNullOrEmpty(_dr_product_row.Item("grp_name")) And Me.global_ws.get_webshop_setting("GROUP_PRODUCTS") = 2 Or Not String.IsNullOrEmpty(_dr_product_row.Item("grp_name")) And Me.global_ws.get_webshop_setting("GROUP_PRODUCTS") = 3 Then
            Hyperlink2.Text = _dr_product_row.Item("grp_name")
        Else
            Hyperlink2.Text = _dr_product_row.Item("prod_desc")
        End If

        ph_prod_desc2.Visible = Me.global_ws.get_webshop_setting("prod_show_subtitle") And Not _dr_product_row.Item("prod_desc2") = ""
        ph_prod_desc2_table.Visible = Me.global_ws.get_webshop_setting("prod_show_subtitle") And Not _dr_product_row.Item("prod_desc2") = ""

        ' Voeg hier de order product control toe
        Dim uc_order_product As Utilize.Web.Solutions.Base.base_usercontrol_base = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
            uc_order_product.ID = "uc_order_product"
            uc_order_product.set_property("product_code", _dr_product_row.Item("prod_code"))

            Dim uc_order_product2 As Utilize.Web.Solutions.Base.base_usercontrol_base = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_order_product.ascx")
            uc_order_product2.ID = "uc_order_product2"
            uc_order_product2.set_property("product_code", _dr_product_row.Item("prod_code"))

            ph_order_product.Controls.Add(uc_order_product)
            ph_order_product2.Controls.Add(uc_order_product2)

            ' Maak de klantspecifieke productcodes zichtbaar/onzichtbaar
            ph_customer_product_codes.Visible = Me._customer_product_codes

        If Me._bulk_order Then
            ' Als systeem bestellen aan staat, dan willen we de bestellen knop niet tonen in het productenoverzicht
            ' Ook moeten de aantallen op 0 staan
            uc_order_product.FindControl("button_order").Visible = False ' Sale unit order button
            uc_order_product.FindControl("button_order1").Visible = False ' Standaard order button

            ' Check if the control is found
            If uc_order_product.FindControl("txt_order_qty") IsNot Nothing Then
                CType(uc_order_product.FindControl("txt_order_qty"), textbox).Text = "0"
            End If
        End If

        If global_ws.get_webshop_setting("use_prod_list_v2") And global_ws.get_webshop_setting("hide_short_desc_v2") Then
            Me.item_comm_short.Visible = False
            Me.ph_prod_desc2.Visible = False
        End If

        If global_ws.get_webshop_setting("use_prod_list_v2") And Not global_ws.get_webshop_setting("title_v2") = "0" Then
            Me.div_uc_placeholder.Attributes.Add("class", Me.div_uc_placeholder.Attributes.Item("class") + " overflow-title-" + global_ws.get_webshop_setting("title_v2").ToString())
        End If

        If global_ws.get_webshop_setting("use_prod_list_v2") And Not global_ws.get_webshop_setting("desc_v2") = "0" Then
            Me.div_uc_placeholder.Attributes.Add("class", Me.div_uc_placeholder.Attributes.Item("class") + " overflow-desc-" + global_ws.get_webshop_setting("desc_v2").ToString())
        End If

        If global_ws.get_webshop_setting("use_prod_list_v2") And global_ws.get_webshop_setting("hide_prod_code_v2") Then
            Me.div_uc_placeholder.Attributes.Add("class", Me.div_uc_placeholder.Attributes.Item("class") + " no-prod-code")
        End If

        'Voeg hier de product information control toe
        Dim uc_product_information As Utilize.Web.Solutions.Base.base_usercontrol_base = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_information.ascx")
        uc_product_information.set_property("product_code", _dr_product_row.Item("prod_code"))
        uc_product_information.ID = "uc_product_information"

        ph_product_information.Controls.Add(uc_product_information)

        Dim _sales_action As Boolean = False

        _sales_action = global_ws.check_sales_action(_dr_product_row.Item("prod_code"))
        Me.ph_sales_action.Visible = _sales_action

        Dim ln_group_products As Integer = Me.global_ws.get_webshop_setting("group_products")
        Select Case ln_group_products
            Case 0
                Me.ph_product_group_matrix.Visible = False
            Case Utilize.Web.Solutions.Webshop.group_types.group
                Me.ph_product_group_matrix.Visible = False
            Case Utilize.Web.Solutions.Webshop.group_types.group_val1
                Me.ph_product_group_matrix.Visible = True

                'Voeg hier de product matrix control toe
                Dim uc_product_group_matrix As Utilize.Web.Solutions.Base.base_usercontrol_base = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_group_matrix.ascx")
                uc_product_group_matrix.ID = "uc_product_group_matrix"
                ph_product_group_matrix.Controls.Add(uc_product_group_matrix)

                uc_product_group_matrix.set_property("uc_order_product", uc_order_product)
                uc_product_group_matrix.set_property("group_type", 1)
                uc_product_group_matrix.set_property("product_code", _dr_product_row.Item("prod_code"))

                Me.ph_product_group_matrix.Visible = True
            Case Utilize.Web.Solutions.Webshop.group_types.group_val2
                'Voeg hier de product information control toe
                Dim uc_product_group_matrix As Utilize.Web.Solutions.Base.base_usercontrol_base = Me.LoadUserControl("~/Webshop/Modules/ProductInformation/uc_product_group_matrix.ascx")
                uc_product_group_matrix.ID = "uc_product_group_matrix"
                ph_product_group_matrix.Controls.Add(uc_product_group_matrix)

                uc_product_group_matrix.set_property("uc_order_product", uc_order_product)
                uc_product_group_matrix.set_property("group_type", 2)
                uc_product_group_matrix.set_property("product_code", _dr_product_row.Item("prod_code"))

                Me.ph_product_group_matrix.Visible = True
            Case Else
                Exit Select
        End Select

        If global_ws.get_webshop_setting("use_prod_list_v2") And global_ws.get_webshop_setting("hide_order_button_v2") Then
            Me.ph_order_product.Visible = False
            Me.ph_order_product2.Visible = False
        Else
            Me.ph_order_product.Visible = Not (Utilize.FrameWork.LicenseManager.CheckModule("UTLZ_WS_E_TEXTILE") And Utilize.Data.DataProcedures.GetValue("uws_products", "ismainitemmatrix", _dr_product_row.Item("prod_code")) And Utilize.Data.DataProcedures.GetValue("uws_products", "mainitemmatrix", _dr_product_row.Item("prod_code"), "mainitemmatrix") = _dr_product_row.Item("prod_code"))
        End If
    End Sub
End Class
