﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_row.ascx.vb" Inherits="uc_product_row" %>
<%@ Register Src="~/webshop/modules/ProductInformation/uc_customer_product_codes.ascx" tagname="uc_customer_product_codes" tagprefix="uc1" %>

<div ID="div_uc_placeholder" runat="server" class="uc_product_row uc_placeholder">
    <div class="table-view">
        <div class="row">
            <div class="item-code-table col-md-2 col-sm-2 col-xs-4">
                <div class="prod_code">
                    <ul class="list-inline prod_code">
                        <li class="value"><span class="prod_code_target"></span></li>
                    </ul>
                </div>
            </div>
            <div class="item-desc1-table col-md-2 col-sm-2 col-xs-4">
                <div class="row"><asp:hyperlink ID="Hyperlink3" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url"))%>'><%# Me.product_row.Item("prod_desc")%></asp:hyperlink></div>
                <utilize:placeholder runat='server' id="ph_prod_desc2_table"><div class="row"><span class="gender text-uppercase"><%# Me.product_row.Item("prod_desc2")%></span></div></utilize:placeholder>
            </div>            
            <div class="item-comm-short-table col-md-4 col-sm-6 col-xs-12">
                <%# Me.product_row.Item("prod_comm_short") %>
                <utilize:translatelink ID="link_read_more_table" CssClass="readmore" runat="server" NavigateUrl='<%# Me.ResolveCustomUrl("~" + Me.product_row.Item("tgt_url")) %>' Text="Lees meer..." Visible='<%# Not Me.product_row.Item("prod_comm_short") = ""%>'></utilize:translatelink>
            </div>
            <div class="item-price-table col-md-2 col-sm-2 col-xs-5">
                <div class="prod_price">        
                    <ul class="price list-inline shop-product-prices">
                        <li><span class="value actual actualdefault target-actualdefault"></span></li>
                        <li><span class="value original originaldefault target-originaldefault"></span></li>
                    </ul>        
                </div>
            </div>
            <div class="item-order-product-table col-md-2 col-sm-6 col-xs-7">
                <utilize:placeholder runat="server" ID="ph_order_product2"></utilize:placeholder>
            </div>
        </div>
    </div>

    <div class="list-product-description product-description-brd margin-bottom-30 item-border" >
        <div class="container-fluid containter-placeholder">
            <div class="row row-placeholder">
                <div class="col-sm-4 image-block" >
                    <asp:hyperlink ID="Hyperlink1" runat="server"><img runat="server" id="imgprod" class="image-placeholder img-responsive sm-margin-bottom-20" /></asp:hyperlink>
                    <utilize:placeholder runat="server" ID="ph_sales_action" Visible="false">
                        <div class="shop-rgba-red rgba-banner"> 
                            <utilize:translatelabel runat="server" ID="label_sales_action" Text="Aanbieding"></utilize:translatelabel>
                        </div>
                    </utilize:placeholder>
                </div> 

                <div class="col-sm-8 product-description information-block">
                    <div class="overflow-h margin-bottom-5">
                        <div class="table-block1">
                            <h4 class="title-price"><asp:hyperlink ID="Hyperlink2" runat="server"></asp:hyperlink></h4>
                            <utilize:placeholder runat='server' id="ph_prod_desc2"><span class="gender text-uppercase" runat="server" id="span_prod_desc2"></span></utilize:placeholder> 
                        
                            <div class="margin-bottom-10">
                                <utilize:placeholder runat="server" ID="ph_product_information"></utilize:placeholder>
                            </div>
                        </div>

                        <span ID="item_comm_short" runat="server" class="gender list-only">    
                            <p class="margin-bottom-20" runat="server" id="p_prod_com_short"></p>
                        </span>
                        <div>
                            <utilize:placeholder runat="server" ID="ph_order_product"></utilize:placeholder>
                        </div>
                        <div class="list-only">
                            
                            <utilize:placeholder runat='server' ID="ph_customer_product_codes" Visible="false">
                                <div>
                                    <uc1:uc_customer_product_codes ID="uc_customer_product_codes1" runat="server"/>
                                </div>
                            </utilize:placeholder>
                        </div>
                    </div>    
                </div>
                <div class="col-sm-12 list-only">
                    <utilize:placeholder runat="server" ID="ph_product_group_matrix" Visible="false">
                    </utilize:placeholder>
                </div>
            </div>
        </div>
    </div>
</div>