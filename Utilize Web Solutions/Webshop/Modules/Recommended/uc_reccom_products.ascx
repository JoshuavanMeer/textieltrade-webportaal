﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_reccom_products.ascx.vb" Inherits="uc_reccom_products" %>
<utilize:panel ID="pnl_reccom_products" CssClass="uc_reccom_products" runat="server">    
        <h2><utilize:translatetitle runat='server' ID="title_recommended_products" Text="Aanraders"></utilize:translatetitle></h2>
        <div class="row">
            <input type="hidden" class="hidden_item_size" value="" runat="server" id="hidden_item_size"/>
            <utilize:label runat='server' ID="lbl_raw_url" Visible='false'></utilize:label>
            <div runat="server" id="recommended_lazy_target"></div>
            <div runat="server" id="recommended_lazy_load_spinner">
                <div class="loader-wrapper">
                    <div class="loader"></div>
                    <utilize:translatelabel runat="server" ID="label_loader_product_list" Text="Producten worden geladen..."></utilize:translatelabel>
                </div>
            </div>
            <asp:Repeater runat="server" ID="rpt_products">
                <ItemTemplate>
                   <utilize:placeholder runat="server" ID="ph_product_row"></utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_clear_fix" Visible="false">
                                <div class="clearfix"></div>
                            </utilize:placeholder>
                    <utilize:placeholder runat="server" ID="ph_mobile_clearfix" Visible="false">
                        <div class="mobile-clearfix"></div>
                    </utilize:placeholder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
</utilize:panel>