﻿
Partial Class uc_reccom_products
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private _item_count As Integer = 6

    Public Enum control_type
        list
        blocks
    End Enum

    Class button_ext
        Inherits Utilize.Web.UI.WebControls.button
        Private _prd_code As String
        Public Property prd_code() As String
            Get
                Return _prd_code
            End Get
            Set(ByVal value As String)
                _prd_code = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class

    ''' <summary>
    ''' The number of items shown in the salesactions
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property item_count As Integer
        Get
            Return _item_count
        End Get
        Set(ByVal value As Integer)
            _item_count = value
        End Set
    End Property

    Private _overview_type As control_type = control_type.blocks
    Public Property overview_type As control_type
        Get
            Return _overview_type
        End Get
        Set(value As control_type)
            _overview_type = value
        End Set
    End Property

    Private _products As System.Data.DataTable

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True
        Dim dt_data_table As System.Data.DataTable = Nothing

        If Not Me.Page.IsPostBack() Then
            Me.lbl_raw_url.Text = Request.RawUrl.ToString()
        End If

        Me.hidden_item_size.Value = "col-md-" + Me.item_size_int.ToString()

        Dim ws_product_filter As New ws_productfilter
        ws_product_filter.page_size = 10000

        If ll_testing Then
            ws_product_filter.extra_join("full join", "uws_prod_reccom", "uws_products.prod_code = uws_prod_reccom.prod_code")
            ws_product_filter.extra_where = " and uws_prod_reccom.hdr_id = '" + Me.block_id + "' "
            ws_product_filter.include_sub_categories = False

            If Not Me.global_ws.get_webshop_setting("lazy_load_products") Then
                dt_data_table = ws_product_filter.get_products(Me.global_ws.Language, "uws_prod_reccom.row_ord")
                ll_testing = dt_data_table.Rows.Count > 0
            Else
                _products = ws_product_filter.get_product_codes(Me.global_ws.Language, "uws_prod_reccom.row_ord")
                ll_testing = _products.Rows.Count > 0
            End If
        End If

        If ll_testing And Not Me.global_ws.get_webshop_setting("lazy_load_products") Then
            Me.rpt_products.DataSource = dt_data_table
            Me.rpt_products.DataBind()
        End If

        If ll_testing Then
            Me.set_style_sheet("uc_reccom_products.css", Me)
        End If

        Me.Visible = ll_testing
    End Sub

    Protected Sub orderbutton_handler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.global_ws.shopping_cart.product_add(CType(sender, button_ext).prd_code, 1, "", "", "")
    End Sub

    Private mobile_clearfix As Integer = 0
    Private clearfixcount As Integer = 0
    Protected Sub rpt_products_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_products.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then

            clearfixcount += Me.item_size_int
            mobile_clearfix += 6

            If clearfixcount = 12 Then
                e.Item.FindControl("ph_clear_fix").Visible = True
                clearfixcount = 0
            End If

            If mobile_clearfix = 12 Then
                e.Item.FindControl("ph_mobile_clearfix").Visible = True
                mobile_clearfix = 0
            End If

            Select Case overview_type
                Case control_type.blocks
                    Dim uc_product_block As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_block.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
                    uc_product_block.set_property("item_size_int", Me.item_size_int)

                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")
                    ph_product_row.Controls.Add(uc_product_block)
                    ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script" + Me.pnl_reccom_products.ClientID, "show_blocks_custom('#" + Me.pnl_reccom_products.ClientID + "');", True)
                Case control_type.list
                    Dim uc_product_row As Utilize.Web.Solutions.Webshop.ws_user_control = LoadUserControl("~/Webshop/Modules/OverviewControls/uc_product_row.ascx")
                    uc_product_row.set_property("product_row", CType(e.Item.DataItem, System.Data.DataRowView).Row)
                    uc_product_row.set_property("item_size_int", Me.item_size_int)
                    Dim ph_product_row As placeholder = e.Item.FindControl("ph_product_row")

                    ph_product_row.Controls.Add(uc_product_row)
                Case Else
                    Exit Select
            End Select
        End If
    End Sub

    Private Sub uc_reccom_products_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.global_ws.get_webshop_setting("lazy_load_products") Then
            Dim li_products As New List(Of String)

            For Each product In _products.Rows
                li_products.Add(product.item("prod_code"))
            Next

            Dim lc_url As String = Me.lbl_raw_url.Text.Substring(0, Me.lbl_raw_url.Text.LastIndexOf("/"))

            If String.IsNullOrEmpty(lc_url) Then
                lc_url = "webshop"
            End If

            Dim lo_lazyLoad As New LazyLoadModel
            With lo_lazyLoad
                .Control = Me
                .LocationId = Me.recommended_lazy_target.ClientID
                .MainElement = "#" + pnl_reccom_products.ClientID
                .Products = li_products
                .SpinnerId = Me.recommended_lazy_load_spinner.ClientID
                .Url = lc_url
                .PageSize = 10000
                .RowAmount = Me.item_size_int
            End With

            LazyLoadHelper.InitializeLazyLoading(lo_lazyLoad)
        End If
    End Sub
End Class

