﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_spec_filter.ascx.vb" Inherits="uc_product_spec_filter" %>
<utilize:literal runat="server" ID="lt_url" Text="" Visible="false"></utilize:literal>

<div ID="divmain" runat="server" class="uc_product_spec_filter">
    <utilize:repeater runat="server" ID="rpt_specifications" pager_enabled="false" EnableViewState="false">
        <ItemTemplate> 
                <h2><utilize:literal runat="server" ID="title_specification" Text='<%# Container.DataItem.specDesc %>'></utilize:literal></h2>

                <div class="spec_desc">
                    <utilize:repeater runat="server" ID="rpt_specification_values">
                        <ItemTemplate>
                            <div class="spec_val">
                                <utilize:checkbox runat="server" ID="chk_filter" Text='<%# Container.DataItem.specVal%>' />
                            </div>
                        </ItemTemplate>
                    </utilize:repeater>
                </div>
        </ItemTemplate>
    </utilize:repeater>
</div>
