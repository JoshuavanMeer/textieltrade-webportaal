﻿
Partial Class uc_product_specs
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    ' Obtaining the attribute product_code this is mandatory
    Private _product_code As String = ""
    Public Property product_code As String
        Get
            Return _product_code
        End Get
        Set(ByVal value As String)
            _product_code = value

            Me.DisplaySpecifications()
        End Set
    End Property


    Protected Sub DisplaySpecifications()
        Dim ll_resume As Boolean = True

        ' datatable aanmaken
        Dim udt_product_specs As System.Data.DataTable = Nothing

        ' licentie controle
        If ll_resume Then
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_spec") = True
        End If

        If ll_resume Then
            ll_resume = Not Me.get_page_parameter("prod_code") = ""
        End If

        If ll_resume Then
            Me.set_style_sheet("uc_product_specs.css", Me)

            ' huidige taalcode ophalen
            Dim lc_language = Me.global_cms.Language

            ' Maak een nieuwe datatable aan
            udt_product_specs = New System.Data.DataTable

            ' Maak een nieuwe query class aan.
            Dim qc_query_class As New Utilize.Data.QuerySelectClass

            ' hoofdtabel
            qc_query_class.select_from = "UWS_PROD_SPEC"

            'externe filter is op product code
            qc_query_class.select_where = "UWS_PROD_SPEC.prod_code = @prod_code "
            qc_query_class.add_parameter("prod_code", Me.get_page_parameter("prod_code"))

            ' Twee tabellen erbij joinen
            qc_query_class.add_join("left outer join", "UWS_PROD_SPEC_DESC", "UWS_PROD_SPEC.spec_code = UWS_PROD_SPEC_DESC.spec_code")
            qc_query_class.add_join("left outer join", "UWS_PROD_SPEC_VAL", "UWS_PROD_SPEC.spec_code = UWS_PROD_SPEC_VAL.spec_code AND UWS_PROD_SPEC.spec_val = UWS_PROD_SPEC_VAL.val_code")

            ' Twee waardes opvragen
            qc_query_class.select_fields = "UWS_PROD_SPEC_DESC.SPEC_DESC_" & lc_language & " AS SPEC_DESC , "
            qc_query_class.select_fields &= "CASE WHEN uws_prod_spec.spec_free = 1 THEN uws_prod_spec.spec_fval ELSE UWS_PROD_SPEC_VAL.VAL_DESC_" & lc_language & " END AS VAL_DESC "
            qc_query_class.select_order = "uws_prod_spec.row_ord"

            udt_product_specs = qc_query_class.execute_query()

            ll_resume = udt_product_specs.Rows.Count > 0
        End If

        If ll_resume Then
            ' Bind the datasource met de product specificaties tabel
            Me.rpt_specs.DataSource = udt_product_specs
            Me.rpt_specs.DataBind()

            ll_resume = Me.rpt_specs.Items.Count > 0
        End If
    End Sub

    Private Sub uc_product_specs_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.Visible Then
            Me.Visible = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_spec") = True
        End If

        If Me.Visible Then
            Me.Visible = Me.rpt_specs.Items.Count > 0
        End If
    End Sub
End Class
