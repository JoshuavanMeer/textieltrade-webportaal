﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_product_specs.ascx.vb" Inherits="uc_product_specs" %>
<utilize:placeholder ID="uc_product_specs_container" runat="server">
    <div class="uc_product_specs">
        <h2><utilize:translatetitle runat="server" ID="title_specifications" Text="Product specificaties"></utilize:translatetitle></h2>
        <utilize:Repeater runat="server" ID="rpt_specs">
            <ItemTemplate>
                <div class="line">
                    <span class="spec_desc"><%#DataBinder.Eval(Container.DataItem, "spec_desc")%></span>
                    <span class="spec_value"><%#DataBinder.Eval(Container.DataItem, "val_desc")%></span>
                </div>
            </ItemTemplate>
        </utilize:Repeater>
    </div>
</utilize:placeholder>
