﻿Imports System.Linq
Imports System.Web.DynamicData

Partial Class uc_product_spec_filter
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    ' Made a datadictionary to add values from the url
    Private dt_dictionary As New Dictionary(Of String, String)

    ' Made a datadictionary to add the products to check if they have other filters specifications
    Private dt_product_dictionary As New List(Of String)

    ' Made a datadictionary to add the specifications of the products, to make it possible to make a dynamic filter
    Private dt_specifications_dictionary As New Dictionary(Of String, String)
    ' BESPOKE: PROP-36 added private variable to hold available filters
    Private _dt_avail_filters As New System.Data.DataTable

    Private _sub_category_string As String = ""
    Private _has_child_categories As Boolean = True
    Private _cat_code As String = ""

    Private Sub create_category_string(ByVal cat_code As String)
        Dim row As System.Data.DataRow
        _sub_category_string &= ",'" & cat_code & "'"

        Dim qc_subcategories As New Utilize.Data.QuerySelectClass
        qc_subcategories.select_from = "uws_categories"
        qc_subcategories.select_order = "row_ord"
        qc_subcategories.select_fields = "cat_code"
        qc_subcategories.select_where = " cat_parent = @current_cat"

        qc_subcategories.add_parameter("current_cat", cat_code)

        Dim dt_subcategories As System.Data.DataTable = qc_subcategories.execute_query()

        For Each row In dt_subcategories.Rows
            create_category_string(row.Item("cat_code"))
        Next row
    End Sub

    Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_testing As Boolean = True
        _cat_code = Me.get_page_parameter("cat_code")

        ' Set URL in lt_url
        If Not Me.IsPostBack Then
            Me.lt_url.Text = Request.RawUrl.ToString()
        End If

        If Me.lt_url.Text.Contains("?") Then
            Dim lc_raw_url As String = Me.lt_url.Text.Split("?")(1)
            Dim lc_url() As String = lc_raw_url.Split("&")

            For ln_value As Integer = 1 To lc_url.Length - 1
                Dim lc_text As String = lc_url(ln_value)
                Dim ll_resume As Boolean = True

                If String.IsNullOrEmpty(lc_text) Then
                    ll_resume = False
                End If

                If ll_resume Then
                    If Not lc_text.ToLower.Contains("page") And Not lc_text.ToLower.Contains("search") And Not lc_text.ToLower.Contains("sort") _
                        And Not lc_text.ToLower.Contains("language") And Not lc_text.ToLower.Contains("cat_parent") Then
                        ' Check if the filter is selected
                        Dim lc_spec_code As String = lc_text.Split("=")(0).ToUpper()
                        Dim lc_spec_val As String = lc_text.Split("=")(1)

                        lc_spec_code = lc_spec_code.Replace("+", " ")
                        lc_spec_val = lc_spec_val.Replace("%20", " ")

                        ' Add the right spec_code and spec_val to the dictionary
                        dt_dictionary.Add(lc_spec_code, lc_spec_val)
                    End If
                End If
            Next
        End If

        Me.create_category_string(_cat_code)
        Dim lc_dynamic_category_string As String = _sub_category_string.Substring(1)

        Dim qsc_avail_filters As New Utilize.Data.QuerySelectClass
        ' Added fields needed to allow all logic to be based of this query
        qsc_avail_filters.select_fields = "uws_prod_spec.spec_code, spec_desc_" + Me.global_ws.Language + " as spec_desc,"
        qsc_avail_filters.select_fields += "CASE WHEN uws_prod_spec.spec_free = 1"
        qsc_avail_filters.select_fields += "   THEN uws_prod_spec.spec_fval "
        qsc_avail_filters.select_fields += "   ELSE (select val_desc_" + Me.global_ws.Language + " as spec_val from uws_prod_spec_val where val_code = uws_prod_spec.spec_val) "
        qsc_avail_filters.select_fields += "END as spec_val,"
        qsc_avail_filters.select_fields += "CASE WHEN uws_prod_spec.spec_free = 1"
        qsc_avail_filters.select_fields += "   THEN uws_prod_spec.spec_fval "
        qsc_avail_filters.select_fields += "   ELSE uws_prod_spec.spec_val "
        qsc_avail_filters.select_fields += "END as spec_val_code,"
        qsc_avail_filters.select_fields += "CASE WHEN uws_prod_spec.spec_free = 1"
        qsc_avail_filters.select_fields += "   THEN uws_prod_spec.row_ord "
        qsc_avail_filters.select_fields += "   ELSE (select row_ord as row_ord from uws_prod_spec_val where val_code = uws_prod_spec.spec_val) "
        qsc_avail_filters.select_fields += "END as row_ord"

        qsc_avail_filters.select_from = "uws_prod_spec"
        qsc_avail_filters.add_join("left join", "uws_products", "uws_prod_spec.prod_code = uws_products.prod_code")
        qsc_avail_filters.add_join("left join", "uws_categories_prod", "uws_prod_spec.prod_code = uws_categories_prod.prod_code")
        ' Added join to get prod_spec_desc
        qsc_avail_filters.add_join("left join", "uws_prod_spec_desc", "uws_prod_spec.spec_code = uws_prod_spec_desc.spec_code")
        qsc_avail_filters.add_join("left join", "uws_products_tran", "uws_products.prod_code = uws_products_tran.prod_code")
        qsc_avail_filters.select_where = "uws_products.prod_show = 1 and "

        If Not Request.Url.ToString.Contains("product_search.aspx") Then
            qsc_avail_filters.select_where += "uws_categories_prod.cat_code in (" + lc_dynamic_category_string + ")"
        Else
            qsc_avail_filters.select_where += "(uws_products.prod_code like @searchtext or "
            qsc_avail_filters.select_where += "uws_products_tran.prod_desc like @searchtext) and uws_products_tran.lng_code = @lng_code"
            qsc_avail_filters.add_parameter("searchtext", Me.get_page_parameter("searchtext").Replace("+", " ") + "%")
            qsc_avail_filters.add_parameter("lng_code", Me.global_ws.Language)
        End If

        If Me.global_ws.get_webshop_setting("filter_type") = 2 Then
            If dt_dictionary.Count > 0 Then
                For Each entry In dt_dictionary
                    If entry.Key.ToLower.Contains("ff_desc") Then
                        qsc_avail_filters.select_where += " AND uws_products_tran." + entry.Key.ToLower + " = @" + entry.Key
                        qsc_avail_filters.add_parameter(entry.Key, entry.Value.Replace("+", " "))
                    ElseIf entry.Key.ToLower.Contains("fl_code") Then
                        qsc_avail_filters.select_where += " AND uws_products." + entry.Key + " = @" + entry.Key
                        qsc_avail_filters.add_parameter(entry.Key, entry.Value)
                    Else
                        Dim key_var As String = entry.Key.Replace(" ", "")
                        ' small bugfix for weird characters
                        Dim val_var As String = key_var + entry.Value.Replace(" ", "").Replace(".", "").Replace(",", "")
                        qsc_avail_filters.select_where += " AND uws_products.prod_code in (select prod_code from uws_prod_spec where (spec_code = @" + key_var + " and (spec_val = @" + val_var + " or spec_fval = @" + val_var + "))) "
                        qsc_avail_filters.add_parameter(key_var, entry.Key)
                        qsc_avail_filters.add_parameter(val_var, entry.Value)
                    End If
                Next
            End If
        End If

        qsc_avail_filters.select_where += " AND uws_prod_spec_desc.spec_search = 1"

        _dt_avail_filters = qsc_avail_filters.execute_query()
        ll_testing = Not _dt_avail_filters.Rows.Count = 0

        ' Don't check cat code on search page
        If ll_testing And Not Request.Url.ToString.Contains("product_search.aspx") Then
            ll_testing = Not _cat_code = ""
        End If

        ' Don't search for cat childs on the search page
        If ll_testing And Not Request.Url.ToString.Contains("product_search.aspx") Then
            _has_child_categories = Not Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_code", _cat_code, "cat_parent") = ""

            If _has_child_categories Then
                ll_testing = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_type", _cat_code) = 2
            End If
        Else
            ' If user is on search page set this variable to false
            _has_child_categories = False
        End If

        If ll_testing Then
            ' Removed query for selecting different specs
            Dim lo_filter_type = From filter In _dt_avail_filters.Rows Select New With {Key .specCode = filter.item("spec_code"), .specDesc = filter.item("spec_desc")} Distinct.ToList

            ' get category order
            Dim lc_spec_code_order As String() = get_spec_code_order()

            If Not lc_spec_code_order Is Nothing Then
                lo_filter_type = lo_filter_type.OrderBy(Function(x)
                                                            Dim index As Integer = Array.IndexOf(lc_spec_code_order, x.specCode.ToString.ToUpper)
                                                            If index = -1 Then Return Integer.MaxValue
                                                            Return index
                                                        End Function).ToList
            End If

            Me.rpt_specifications.DataSource = lo_filter_type
            Me.rpt_specifications.DataBind()

            ll_testing = Me.rpt_specifications.Items.Count > 0
        End If

        If ll_testing Then
            Me.set_style_sheet("uc_product_spec_filter.css", Me)
        End If

        If ll_testing Then
            Me.Visible = ll_testing
        End If

        If ll_testing And Not Me.block_table = "" And Not Me.block_rec_id = "" Then
            Dim dr_temp_row As System.Data.DataRow = Utilize.Data.DataProcedures.GetDataRow(Me.block_table, Me.block_rec_id)

            If dr_temp_row.Item("spec_filter_advanced") Then
                Me.divmain.Attributes.Add("class", Me.divmain.Attributes.Item("class") + " advanced")
            End If
        End If
    End Sub

    Private Function get_spec_code_order() As String()
        If String.IsNullOrEmpty(_cat_code) Then
            Return Nothing
        End If

        Dim lc_split_string As String = get_spec_order_string(_cat_code).ToUpper

        If String.IsNullOrEmpty(lc_split_string) Then
            Return Nothing
        Else
            Return lc_split_string.Split(";")
        End If
    End Function

    Private Function get_spec_order_string(cat_code As String) As String
        Dim lc_split_string As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "specification_order", cat_code)

        If String.IsNullOrEmpty(lc_split_string) Then
            Dim lc_cat_parent As String = Utilize.Data.DataProcedures.GetValue("uws_categories", "cat_parent", cat_code)

            If Not String.IsNullOrEmpty(lc_cat_parent) Then
                lc_split_string = get_spec_order_string(lc_cat_parent)
            End If
        End If

        Return lc_split_string
    End Function

    Protected Sub rpt_specifications_ItemCreated(sender As Object, e As UI.WebControls.RepeaterItemEventArgs) Handles rpt_specifications.ItemCreated
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            ' Voeg een handler toe voor het aanmaken van de items
            Dim ll_show_products As Boolean = False
            Dim rpt_specification_values As repeater = e.Item.FindControl("rpt_specification_values")
            AddHandler rpt_specification_values.ItemCreated, AddressOf Me.rpt_specification_values_itemcreated

            ' BESPOKE: PROP-61
            ' Removed query for getting spec values

            Dim lo_datasource = From filter In _dt_avail_filters.Rows Where filter.item("spec_code") = e.Item.DataItem.specCode Select New With {.specCode = filter.item("spec_code"), Key .specValCode = filter.item("spec_val_code"), .specVal = filter.item("spec_val"), .rowOrd = filter.item("row_ord")} Distinct.ToList

            Dim sorting As SpecValueFilterSorting = Me.global_ws.get_webshop_setting("spec_val_sorting")

            Select Case sorting
                Case SpecValueFilterSorting.Alphanumeric
                    ' Get all the words from the specifications and order them so they can be used in the sorting of the datasource
                    Dim lo_words As List(Of String) = (From item In lo_datasource Order By GetSortWord(item.specVal) Descending Select GetSortWord(item.specVal).ToLower.Trim Distinct).ToList

                    ' Sort the datasource
                    lo_datasource = lo_datasource.OrderBy(Function(x) GetSortOrder(x.specVal.ToString, SortOrderLocation.First, lo_words)) _
                                                .ThenBy(Function(x) GetSortOrder(x.specVal.ToString, SortOrderLocation.Last, lo_words)) _
                                                .ThenBy(Function(x) x.specVal) _
                                                .ToList()
                Case SpecValueFilterSorting.RowOrd
                    ' Sort the datasource
                    lo_datasource = lo_datasource.OrderBy(Function(x) x.rowOrd) _
                                                .ToList()
            End Select


            rpt_specification_values.DataSource = lo_datasource
            ' END BESPOKE
            rpt_specification_values.DataBind()

        End If
    End Sub

    Private Enum SpecValueFilterSorting
        Alphanumeric = 0
        RowOrd = 1
    End Enum

    Private Function GetSortWord(specVal As String) As String
        ' Get all numeric values from the specVal
        Dim numericValues = Regex.Split(specVal, "\D+")

        For Each value In numericValues
            If Not String.IsNullOrEmpty(value) Then
                If specVal.StartsWith(value) Then
                    ' If the specVal starts with the number return everything after 
                    Return specVal.Substring(value.Count)
                ElseIf specVal.EndsWith(value) Then
                    ' If the specVal ends with the number return everything before it
                    Return specVal.Substring(0, specVal.Count - (value.Count + 1))
                End If
            End If
        Next

        Return specVal
    End Function

    Private Function GetSortOrder(specVal As String, position As SortOrderLocation, words As List(Of String)) As Decimal
        Dim numericValues = Regex.Split(specVal, "\D+")

        If numericValues.Count = 0 Then
            Return specVal
        End If

        For Each value In numericValues
            If Not String.IsNullOrEmpty(value) Then
                If specVal.StartsWith(value) Then
                    If position = SortOrderLocation.First Then
                        Return Convert.ToDecimal(value.Replace(",", "."))
                    ElseIf position = SortOrderLocation.Last Then
                        Return Integer.MaxValue - 1 - words.IndexOf(specVal.Substring(value.Count).ToLower)
                    End If
                ElseIf specVal.EndsWith(value) Then
                    If position = SortOrderLocation.First Then
                        Return Integer.MaxValue - 1 - words.IndexOf(specVal.Substring(0, specVal.Count - (value.Count + 1)).ToLower)
                    ElseIf position = SortOrderLocation.Last Then
                        Return Convert.ToDecimal(value.Replace(",", "."))
                    End If
                End If
            End If
        Next

        Return Integer.MaxValue
    End Function

    Private Enum SortOrderLocation As Integer
        First = 0
        Last = 1
    End Enum

    Protected Sub rpt_specification_values_itemcreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = UI.WebControls.ListItemType.AlternatingItem Or e.Item.ItemType = UI.WebControls.ListItemType.Item Then
            Dim rpt_specification_values As repeater = CType(sender, repeater)
            Dim rpt_specifications As System.Web.UI.WebControls.RepeaterItem = rpt_specification_values.Parent()

            ' De specificatie en de waarde
            ' BESPOKE PROP-61:
            ' Changed values to the fields of the annonymous type
            Dim lc_spec_code As String = rpt_specifications.DataItem.specCode
            Dim lc_spec_code_url_style As String = lc_spec_code.Replace(" ", "+")
            Dim lc_spec_val As String = e.Item.DataItem.specValCode
            ' END BESPOKE

            ' Controleer of de pagina parameter de gelijk is aan de specificatie waarde
            Dim ll_active As Boolean

            If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                Dim lo_current_filters As List(Of String) = Me.get_page_parameter(lc_spec_code_url_style).Split("|").ToList

                ll_active = lo_current_filters.Contains(lc_spec_val) And Not String.IsNullOrEmpty(lc_spec_val.Trim)
            Else
                ll_active = Me.get_page_parameter(lc_spec_code_url_style) = lc_spec_val And Not String.IsNullOrEmpty(lc_spec_val.Trim)
            End If

            Dim chk_filter As checkbox = e.Item.FindControl("chk_filter")

            If ll_active Then
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)

                url_class.set_parameter("page", "0")

                ' Multi select filter
                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter(lc_spec_code_url_style)
                    url_class.set_parameter(lc_spec_code_url_style, lc_current_val.Replace(lc_spec_val + "|", ""))

                    Dim lo_spec = New KeyValuePair(Of String, String)(lc_spec_code, lc_current_val)

                    Me._active_specifications = (From spec In Me._active_specifications Where Not spec.Key = lc_spec_code Select spec).ToList
                    Me._active_specifications.Add(lo_spec)
                Else
                    Dim lo_spec = New KeyValuePair(Of String, String)(lc_spec_code, url_class.get_parameter(lc_spec_code_url_style))

                    url_class.set_parameter(lc_spec_code_url_style, "")

                    ' Voeg de geselecteerde specificatie toe aan de lijst
                    If Not Me._active_specifications.Contains(lo_spec) Then
                        Me._active_specifications.Add(lo_spec)
                    End If
                End If


                chk_filter.Checked = True
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")

                Session("ProductSpecifications") = Me._active_specifications
            Else
                Dim url_class As New Utilize.Web.Solutions.base_url_base(Me.lt_url.Text)

                url_class.set_parameter("page", "0")
                If Me.global_ws.get_webshop_setting("filter_type") = 1 Then
                    Dim lc_current_val As String = Me.get_page_parameter(lc_spec_code_url_style)
                    url_class.set_parameter(lc_spec_code_url_style, lc_current_val + lc_spec_val + "|")
                Else
                    url_class.set_parameter(lc_spec_code_url_style, lc_spec_val)
                End If

                chk_filter.Checked = False
                chk_filter.Attributes.Add("onclick", "document.location='" + Me.ResolveCustomUrl(url_class.get_url()) + "'")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Dit is een lijst met daarin alle geselecteerde specificaties.
    ''' Deze wordt in de sessie opgeslagen zodat we die in de uc_product_list.ascx en de uc_product_blocks.ascx kunnen gebruiken om te filteren
    ''' </summary>
    ''' <remarks></remarks>
    Private _active_specifications As New System.Collections.Generic.List(Of KeyValuePair(Of String, String))
End Class