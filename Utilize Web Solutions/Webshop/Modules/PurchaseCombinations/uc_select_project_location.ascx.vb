﻿Imports System.Data

Partial Class uc_select_employee
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control


    Public ReadOnly Property project_location As String
        Get
            Try
                If Not Request.Form(dd_project_location.UniqueID) Is Nothing Then
                    Return Request.Form(dd_project_location.UniqueID)
                End If
            Catch ex As Exception

            End Try

            Return Me.dd_project_location.SelectedValue
        End Get
    End Property

    Protected Sub Page_Init() Handles Me.Init
        Dim ll_visible As Boolean = True

        If ll_visible Then
            ' Module controle
            ll_visible = Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")
        End If

        If ll_visible Then
            ' Alleen zichtbaar wanneer de gebruiker is ingelogd
            ll_visible = Me.global_ws.user_information.user_logged_on
        End If

        If ll_visible Then
            ll_visible = Me.global_ws.user_information.ubo_customer.field_get("uws_customers.ploc_enab") = True
        End If

        If ll_visible Then
            Me.set_style_sheet("uc_select_project_location.css", Me)

            Dim qsc_select As New Utilize.Data.QuerySelectClass
            qsc_select.select_fields = "*"
            qsc_select.select_from = "uws_project_location"
            qsc_select.select_where = "cust_id = @cust_id and blocked = 0"
            qsc_select.add_parameter("cust_id", Me.global_ws.user_information.user_customer_id)

            Dim dt_data_table As System.Data.DataTable = qsc_select.execute_query()

            Me.Visible = dt_data_table.Rows.Count > 0

            Me.dd_project_location.DataSource = dt_data_table
            Me.dd_project_location.DataValueField = "rec_id"
            Me.dd_project_location.DataTextField = "location_name"
            Me.dd_project_location.DataBind()

            dd_project_location.Items.Insert(0,
                                            New System.Web.UI.WebControls.ListItem With
                                            {
                                               .Text = global_trans.translate_label("label_not_selected", "Niet geselecteerd", Me.global_ws.Language),
                                               .Value = ""
                                            })
        End If

        If Not ll_visible Then
            Me.Visible = False
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

    End Sub
End Class

