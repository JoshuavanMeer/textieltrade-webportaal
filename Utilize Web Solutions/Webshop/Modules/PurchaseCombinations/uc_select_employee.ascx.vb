﻿Imports System.Data

Partial Class uc_select_employee
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Protected Sub Page_Init() Handles Me.Init
        Dim ll_visible As Boolean = True

        If ll_visible Then
            ' Module controle
            ll_visible = Utilize.Data.DataProcedures.CheckModule("utlz_ws_purch_comb")
        End If

        If ll_visible Then
            ' Alleen zichtbaar wanneer de gebruiker is ingelogd
            ll_visible = Me.global_ws.user_information.user_logged_on
        End If

        If ll_visible Then
            Me.set_style_sheet("uc_select_employee.css", Me)

            If Not Me.IsPostBack Then
                'Haal de dochterbedrijven op en het moederbedrijf
                Dim qsc_companies As New Utilize.Data.QuerySelectClass
                qsc_companies.select_fields = "*"
                qsc_companies.select_from = "uws_customers"
                qsc_companies.select_order = "bus_name"
                qsc_companies.select_where = "rec_id = @rec_id "
                qsc_companies.select_where += " or rec_id in (SELECT rec_id FROM uws_customers WHERE main_company = @rec_id) "
                qsc_companies.add_parameter("rec_id", Me.global_ws.user_information.user_customer_id)

                Dim dt_companies As System.Data.DataTable = qsc_companies.execute_query()

                Me.ddl_select_company.DataSource = dt_companies
                Me.ddl_select_company.DataBind()
                Me.ddl_select_company.SelectedValue = Me.global_ws.purchase_combination.company_id

                ' Vraag de medewerkers op die behoren bij het geselecteerde bedrijf op.
                Dim qsc_customer_users As New Utilize.Data.QuerySelectClass()
                qsc_customer_users.select_fields = "rec_id, lst_name + ',' + fst_name as full_name"
                qsc_customer_users.select_from = "uws_customers_users"
                qsc_customer_users.select_where = "hdr_id = @hdr_id and blocked = 0"
                qsc_customer_users.add_parameter("hdr_id", Me.ddl_select_company.SelectedValue)
                qsc_customer_users.select_order = "lst_name"

                Dim dt_customer_users As System.Data.DataTable = qsc_customer_users.execute_query()

                Me.ddl_select_employee.DataSource = dt_customer_users
                Me.ddl_select_employee.DataBind()
                Me.ddl_select_employee.SelectedValue = Me.global_ws.purchase_combination.user_id

                Me.ph_companies.Visible = False
            End If

            ' Toon de selecteer bedrijf niet als er maar 1 bedrijf is
            If global_ws.purchase_combination.user_is_manager Then
                Me.ph_companies.Visible = ddl_select_company.Items.Count > 1
            Else
                Me.Visible = False
            End If
        End If

        If ll_visible And Not Me.IsPostBack() Then
            ' Sla de url op in de onzichtbare literal
            Me.lt_raw_url.Text = Request.RawUrl.ToString().ToLower()
        End If

        If Not ll_visible Then
            Me.Visible = False
        End If
    End Sub

    Protected Sub ddl_select_company_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_select_company.SelectedIndexChanged
        Dim qsc_customer_users As New Utilize.Data.QuerySelectClass
        qsc_customer_users.select_fields = "rec_id, lst_name + ', ' + fst_name as full_name"
        qsc_customer_users.select_from = "uws_customers_users"
        qsc_customer_users.select_where = "hdr_id = @hdr_id "
        qsc_customer_users.select_where += "or hdr_id in(SELECT rec_id FROM uws_customers WHERE MAIN_COMPANY = @hdr_id)"
        qsc_customer_users.add_parameter("hdr_id", Me.ddl_select_company.SelectedValue)
        qsc_customer_users.select_order = "lst_name"

        Dim dt_customer_users As System.Data.DataTable = qsc_customer_users.execute_query()

        Me.global_ws.purchase_combination.company_id = Me.ddl_select_company.SelectedValue
        Me.login_user(Me.global_ws.purchase_combination.company_id)

        Me.ddl_select_employee.DataSource = dt_customer_users
        Me.ddl_select_employee.DataBind()
        Me.ddl_select_employee.SelectedIndex() = 0

        Me.global_ws.purchase_combination.user_id = Me.ddl_select_employee.SelectedValue

    End Sub
    Protected Sub ddl_select_employee_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_select_employee.SelectedIndexChanged
        Me.global_ws.purchase_combination.company_id = Me.ddl_select_company.SelectedValue
        Me.global_ws.purchase_combination.user_id = Me.ddl_select_employee.SelectedValue

        If Me.global_ws.user_information.user_id_original = "" Then
            Me.global_ws.user_information.user_id_original = Me.global_ws.user_information.user_id
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "user_selected", "alert('" + global_trans.translate_message("message_user_selected", "De gebruiker is geselecteerd", Me.global_ws.Language) + "'); document.location='" + Me.ResolveCustomUrl("~/") + "home.aspx';", True)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Try
            Me.ddl_select_company.SelectedValue = Me.global_ws.purchase_combination.company_id
            Me.ddl_select_employee.SelectedValue = Me.global_ws.purchase_combination.user_id
        Catch ex As Exception

        End Try
    End Sub

    Private Function login_user(ByVal company_id As String) As Boolean
        ' We loggen altijd in onder het emailadres wat in de website instellingen is vasttgelegd.
        Dim lc_login_code As String = global_cms.get_cms_setting("eml_from_address").ToString().Trim()

        ' Sla hieronder een aantal gegevens op
        Dim lc_cust_id As String = company_id
        Dim lc_password As String = Guid.NewGuid().ToString()
        Dim lc_rec_id As String = ""

        Dim ll_resume As Boolean = True

        ' Declareer het business objects
        Dim ubo_customer As Utilize.Data.BusinessObject = Nothing

        If ll_resume Then
            ' Zet hier de id van de oorspronkelijk ingelogde gebruiker
            Me.global_ws.user_information.user_id_original = Me.global_ws.user_information.user_id
        End If

        If ll_resume Then
            ' Als de logincode niet gevonden is, dan maken we een nieuwe webshop gebruiker aan
            ubo_customer = Utilize.Data.DataProcedures.CreateRecordObject("uws_customers", lc_cust_id, "rec_id")
            ubo_customer.table_insert_row("uws_customers_users", Me.global_ws.user_information.user_id)

            ' Sla de sleutelwaarde van de nieuw aangemaakte gebruiker op
            lc_rec_id = ubo_customer.field_get("uws_customers_users.rec_id")

            ' Zet de velden
            ubo_customer.field_set("uws_customers_users.fst_name", Me.global_ws.user_information.user_fst_name)
            ubo_customer.field_set("uws_customers_users.infix", Me.global_ws.user_information.user_infix)
            ubo_customer.field_set("uws_customers_users.lst_name", Me.global_ws.user_information.user_lst_name)

            ubo_customer.field_set("uws_customers_users.email_address", lc_login_code)
            ubo_customer.field_set("uws_customers_users.login_code", lc_login_code)
            ubo_customer.field_set("uws_customers_users.login_password", lc_password)
            ubo_customer.field_set("uws_customers_users.type_code", 1)
            ubo_customer.field_set("uws_customers_users.user_manager", 1)

            ' Sla de gebruiker op
            ll_resume = ubo_customer.table_update(Me.global_ws.user_information.user_id)

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "login_message", "alert('" + ubo_customer.error_message.Replace("'", "\'") + "');", True)
            End If
        End If

        If ll_resume Then
            ' Haal het wachtwoord van de gebruiker op
            lc_login_code = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_code", lc_login_code + lc_cust_id, "email_address + hdr_id")
            lc_password = Utilize.Data.DataProcedures.GetValue("uws_customers_users", "login_password", lc_login_code + lc_cust_id, "email_address + hdr_id")

            ' Login onder de juiste gebruikers gegevens
            ll_resume = Me.global_ws.user_information.login(1, lc_login_code, lc_password)

            If Not ll_resume Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "login_message", "alert('" + Me.global_ws.user_information.error_message.Replace("'", "\'") + "');", True)
            End If
        End If

        If ll_resume Then
            ' We verwijderen de gebruiker meteen weer want we willen niet allemaal zwevende gebruikers hebben
            ubo_customer.move_first("uws_customers_users")

            If ubo_customer.table_locate("uws_customers_users", "rec_id = '" + lc_rec_id + "'") Then
                ubo_customer.table_delete_row("uws_customers_users", False, Me.global_ws.user_information.user_customer_id)
                ubo_customer.table_update(Me.global_ws.user_information.user_id)
            End If
        End If

        If ll_resume Then
            ' En maak een nieuwe winkelwagen aan
            Me.global_ws.shopping_cart.create_shopping_cart()

            Response.Redirect("~/home.aspx", False)
        End If

        Return True
    End Function
End Class

