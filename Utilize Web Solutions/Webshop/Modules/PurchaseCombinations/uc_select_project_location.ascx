﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_select_project_location.ascx.vb" Inherits="uc_select_employee" %>

<div class="uc_select_project_location">
    <table>
        <tr>
            <td class="text">
                <utilize:translatelabel runat="server" ID="label_select_project_locatie" Text="Kies project locatie"></utilize:translatelabel>
            </td>
            <td class="value">
                <utilize:dropdownlist runat="server" ID="dd_project_location"></utilize:dropdownlist>
            </td>
        </tr>
    </table>
</div>