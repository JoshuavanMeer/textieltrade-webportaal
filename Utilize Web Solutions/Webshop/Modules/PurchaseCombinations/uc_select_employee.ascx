﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uc_select_employee.ascx.vb" Inherits="uc_select_employee" %>
<utilize:literal runat="server" ID="lt_raw_url" Visible="false"></utilize:literal>

<div class="uc_select_employee margin-bottom-10">
    <h2><utilize:translatetitle runat='server' ID="title_select_employee" Text="Medewerker selecteren"></utilize:translatetitle></h2>
    <utilize:placeholder runat="server" ID="ph_companies">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <utilize:translatelabel runat="server" ID="label_select_company" Text="Selecteer bedrijf"></utilize:translatelabel>
            </div>
            <div class="col-md-12">
                <utilize:dropdownlist ID="ddl_select_company" CssClass="form-control" runat="server" AutoPostBack="true" DataTextField="bus_name" DataValueField="rec_id"></utilize:dropdownlist>
            </div>
        </div>
    </utilize:placeholder>
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <utilize:translatelabel runat="server" ID="label_select_employee" Text="Selecteer medewerker"></utilize:translatelabel>
        </div>
        <div class="col-md-12">
            <utilize:dropdownlist ID="ddl_select_employee" CssClass="form-control" runat="server" AutoPostBack="true" DataTextField="full_name" DataValueField="rec_id"></utilize:dropdownlist>
        </div>
    </div>
</div>
