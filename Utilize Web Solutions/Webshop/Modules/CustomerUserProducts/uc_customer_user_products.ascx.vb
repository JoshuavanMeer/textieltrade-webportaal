﻿
Partial Class uc_customer_user_products
    Inherits Utilize.Web.Solutions.Webshop.ws_user_control

    Private brand_code As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim ll_resume As Boolean = True

        If ll_resume Then
            ' Dit blok is alleen zichtbaar wanneer de module Gebruikersspecifieke producten actief is
            ll_resume = Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_webs_usr_prd")
        End If        

        If ll_resume Then
            ' Dit blok is alleen zichtbaar wanneer de gebruiker is ingelogd
            ll_resume = Me.global_ws.user_information.user_logged_on
        End If

        If ll_resume Then
            ' Dit blok is alleen zichtbaar wanneer er gebruikersspecifieke producten zijn gekoppeld bij de webshopgebruiker
            Dim user_id As String = Me.global_ws.purchase_combination.user_id

            If String.IsNullOrEmpty(user_id) Then
                user_id = Me.global_ws.user_information.user_id 
            End If

           ll_resume = (Utilize.Data.DataProcedures.GetValue("uws_user_products", "user_id", user_id) = user_id) 
        End If

        If ll_resume Then
            Me.link_customer_user_products.NavigateUrl = Me.Page.ResolveUrl("~/" + Me.global_ws.Language + "/9/customeruserproducts.aspx")

            Me.set_style_sheet("uc_customer_user_products.css", Me)
        End If

        Me.Visible = ll_resume
    End Sub
End Class
