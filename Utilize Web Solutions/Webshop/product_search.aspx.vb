﻿

<Serializable()>
Public Class product_search
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("product_src_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_search_results", "Zoekresultaten", Me.global_cms.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        ' Maak een nieuwe tekst blok aan.
        Dim uc_block As Base.base_usercontrol_base

        If Not Utilize.FrameWork.LicenseManager.CheckModule("utlz_ws_productlist") Then
            If global_ws.get_webshop_setting("use_prod_list_v2") Then
                uc_block = LoadUserControl("~/Webshop/Modules/ProductList/uc_product_list_v2.ascx")
            Else
                uc_block = LoadUserControl("~/Webshop/Modules/ProductList/uc_product_list.ascx")
            End If
        Else
            uc_block = LoadUserControl("~/Webshop/Modules/ProductListElastic/uc_product_list_elastic.ascx")
        End If

        ph_target_block.Controls.Add(uc_block)

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack() Then
            Session("ProductPage") = Request.RawUrl.ToString()
        End If
    End Sub
End Class
