﻿
Partial Class Webshop_SingleSignOn_SamlError
    Inherits CMSPageTemplate

    Protected Sub Page_PreInit1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.cms_template_code = Me.global_ws.get_webshop_setting("login_template")

        ' Zet hier de titel van de pagina
        Dim lc_title As String = Me.global_cms.get_cms_setting("us_website_title")
        Me.Page.Title = IIf(lc_title = "", "", lc_title + " : ") + global_trans.translate_title("title_saml_error", "Foutieve inlog", Me.global_cms.Language)
    End Sub

    Protected Sub Page_InitComplete1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim ph_target_block As Utilize.Web.UI.WebControls.placeholder = Me.cms_template_zone

        ' Maak een nieuwe tekst blok aan.
        Dim uc_block As Base.base_usercontrol_base = LoadUserControl("~/Webshop/Modules/SingleSignOn/uc_single_sign_on_error.ascx")
        ph_target_block.Controls.Add(uc_block)
    End Sub
End Class
