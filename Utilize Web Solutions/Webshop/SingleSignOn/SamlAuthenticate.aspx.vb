﻿
Partial Class Webshop_Services_SamlAuthenticate
    Inherits System.Web.UI.Page

    Private Sub Webshop_Services_SamlAuthenticate_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ll_testing As Boolean = True
        Dim lc_certificate As String = ""
        Dim lc_saml_resp As String = ""
        Dim lc_id As String = Request.QueryString("id")

        Dim dr_sso As System.Data.DataRow = Nothing
        If ll_testing Then
            If Not String.IsNullOrEmpty(lc_id) Then
                dr_sso = Utilize.Data.DataProcedures.GetDataRow("uws_single_sign_on", lc_id)
            End If

            ll_testing = dr_sso IsNot Nothing
        End If

        If ll_testing Then
            lc_certificate = dr_sso.Item("saml_certificate")

            ll_testing = Not String.IsNullOrEmpty(lc_certificate)
        End If

        Dim lo_saml_response As Saml.Response = Nothing
        If ll_testing Then
            lo_saml_response = New Saml.Response(lc_certificate)
            lo_saml_response.LoadXmlFromBase64(Request.Form("SAMLResponse"))

            ll_testing = lo_saml_response.IsValid
        End If

        If ll_testing Then
            Dim lc_debtor = dr_sso.Item("main_debtor")
            Dim lc_email As String = lo_saml_response.GetEmail
            Dim lc_first_name As String = lo_saml_response.GetFirstName
            Dim lc_last_name As String = lo_saml_response.GetLastName
            Dim lc_login_code As String = lo_saml_response.GetEmail

            Dim ubo_customer As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateBusinessObject("uws_customers_users")

            If Not ubo_customer.table_locate("uws_customers_users", "email_address = '" + lc_email + "' and hdr_id = '" + lc_debtor + "'") Then
                ubo_customer.table_insert_row("uws_customers_users", "SamlAuthenticate")
                ubo_customer.field_set("uws_customers_users.hdr_id", lc_debtor)
            End If

            ubo_customer.field_set("uws_customers_users.email_address", lc_email)
            ubo_customer.field_set("uws_customers_users.fst_name", lc_first_name)
            ubo_customer.field_set("uws_customers_users.lst_name", lc_last_name)
            ubo_customer.field_set("uws_customers_users.login_code", lc_login_code)
            Dim lc_password As String = System.Guid.NewGuid.ToString("n")
            ubo_customer.field_set("uws_customers_users.login_password", lc_password)

            ubo_customer.table_update("SamlAuthenticate")

            Utilize.Web.Solutions.Webshop.ws_procedures.get_global_ws().user_information.login(Webshop.user_type.Customer, lc_email, lc_password)

            Response.Redirect(Me.ResolveClientUrl("~/home.aspx"))
        End If
    End Sub
End Class
