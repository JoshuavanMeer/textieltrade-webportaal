﻿
Partial Class AssortmentFeedDownload
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim loProductFeed As New ProductExportFeed
        Dim sbProductFeed As StringBuilder = loProductFeed.GenerateFeed()

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("Content-Disposition", "attachment;filename=assortiment.csv")

        Response.Write(sbProductFeed.ToString())
        Response.End()
    End Sub
End Class
