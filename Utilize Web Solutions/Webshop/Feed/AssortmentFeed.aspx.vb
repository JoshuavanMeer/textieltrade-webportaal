﻿Imports System.IO

Partial Class Bespoke_Account_CreateCSVFeed
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim loProductFeed As New ProductExportFeed
        Dim sbProductFeed As StringBuilder = loProductFeed.GenerateFeed()

        Me.lt_product_feed.Text = sbProductFeed.ToString()

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.Write(sbProductFeed.ToString())
        Response.End()
    End Sub
End Class
