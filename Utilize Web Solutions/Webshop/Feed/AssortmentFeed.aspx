﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssortmentFeed.aspx.vb" Inherits="Bespoke_Account_CreateCSVFeed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <utilize:literal runat="server" ID="lt_product_feed"></utilize:literal>
    </div>
    </form>
</body>
</html>
