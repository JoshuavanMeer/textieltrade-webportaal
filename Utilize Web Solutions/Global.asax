﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup

        ' Logging
        Dim configFile As String = ConfigurationManager.AppSettings.Get("log4net.Config")
        If Not String.IsNullOrEmpty(configFile) Then
            configFile = Environment.ExpandEnvironmentVariables(configFile)
            log4net.Config.XmlConfigurator.Configure(New System.IO.FileInfo(configFile))
            'log4net.Util.LogLog.InternalDebugging = True
        End If
        RouteTable.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", defaults:=
                                         New With {.id = System.Web.Http.RouteParameter.Optional})
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_PreSendRequestHeaders(ByVal sender As Object, ByVal e As EventArgs)
        Response.Headers.Set("Server", "Utilize. step by step")
    End Sub

    ' Register the error in the error log
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Get current exception 
        Dim lastErrorException As HttpException = Server.GetLastError()

        If Not lastErrorException.GetHttpCode() = 404 And Not lastErrorException.Message = "File does not exist." Then
            ' Get the logged in user, if not availabel create user name
            Dim AdminUserName As String = Utilize.Web.UI.SessionManager.Login.AdminUserName
            If String.IsNullOrEmpty(AdminUserName) Then
                AdminUserName = "GlobalApplicationError"
            End If

            ' Create the business object
            Dim ubo_err_log As Utilize.Data.BusinessObject = Utilize.Data.DataProcedures.CreateRecordObject("utlz_err_log", "TEST", "rec_id")

            ' Insert a new row
            ubo_err_log.table_insert_row("utlz_error_log", AdminUserName)

            ' Set the details
            ubo_err_log.field_set("err_date", System.DateTime.Now())
            ubo_err_log.field_set("err_msg", lastErrorException.Message)
            ubo_err_log.field_set("err_details", lastErrorException.ToString())
            ' Logging
            Dim loExceptionA As Byte() = Utilize.Data.GenericProcedures.SerializeObjectToByteArray(lastErrorException)
            ubo_err_log.field_set("err_exception", loExceptionA)

            ' Save the information
            ubo_err_log.table_update(AdminUserName)

            ' Release the error object
            ubo_err_log = Nothing
        End If
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode is set to InProc in the Web.config file. 
        ' If SessionThen mode Is Set To StateServer or SQLServer, the event is not raised.
    End Sub
</script>